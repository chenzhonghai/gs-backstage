<?php
/**
 * 易优CMS
 * ============================================================================
 * 版权所有 2016-2028 海南赞赞网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.eyoucms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 */
use app\common\model\Weapp as WeappModel;

$globalConfig = tpCache('global');
$author = '<em style="font-size: 12px;">'.$globalConfig['web_name'].'</em>';
$name = '低调插件_eyou';
$row = WeappModel::get(array('code' => 'ClearCopyright'));
$config = json_decode($row->data, true);
$ishide = $config['captcha']['clear_copyright']['is_hide'];
$script = !empty($ishide) && $ishide == 1 ? '<script>$(".bDiv tbody tr").each(function(){ var name_old = $(this).children(":first").children().text(); if(name_old.replace(/\s/g, "") == "'.$name.'"){ $(this).remove(); } }); $(".plug-item-content").each(function(){ var name_new = $(this).children(".plug-item-top").children(".plug-text").children(".plug-text-title").children().text(); if(name_new.replace(/\s/g, "") == "'.$name.'"){ $(this).remove(); } });</script>' : '';

return array(
    'code' => 'ClearCopyright', // 插件标识
    'name' => $name, // 插件名称
    'version' => 'v1.0.7', // 插件版本号
    'min_version' => 'v1.2.0', // CMS最低版本支持
    'author' => $author, // 开发者
    'description' => '低调是强大应有的素质'.$script, // 插件描述
    'litpic'    => '/weapp/ClearCopyright/ClearCopyright.jpg',
    'scene' => '0',  // 使用场景 0 PC+手机 1 手机 2 PC
    'permission' => array(),
);