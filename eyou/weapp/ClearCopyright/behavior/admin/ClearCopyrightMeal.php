<?php

namespace weapp\ClearCopyright\behavior\admin;

use think\Config;
use app\common\model\Weapp;

/**
 * 行为扩展
 */
class ClearCopyrightMeal
{
    protected static $actionName;
    protected static $controllerName;
    protected static $moduleName;
    protected static $method;

    /**
     * 构造方法
     * @param Request $request Request对象
     * @access public
     */
    public function __construct()
    {
        !isset(self::$moduleName) && self::$moduleName = request()->module();
        !isset(self::$controllerName) && self::$controllerName = request()->controller();
        !isset(self::$actionName) && self::$actionName = request()->action();
        !isset(self::$method) && self::$method = strtoupper(request()->method());
    }

    /**
     * 模块初始化
     * @param array $params 传入参数
     * @access public
     */
    public function moduleInit(&$params)
    {
		// 获取插件信息
		$weapp = Weapp::get(array('code' => 'ClearCopyright'));
		// 获取插件配置信息
		$row = json_decode($weapp->data, true);
		if (!empty($row)) {
		    foreach ($row['captcha'] as $key => $val) {
				if ('clear_copyright' == $key && 1 == $val['is_on']) {
					$diestr = array();
					$dierep = array();
					$die_file = CORE_PATH.'db/driver/Driver.php';
					array_push($diestr, '\'shop_open\'=>0', '$iseyKey=>-1', '$tmpMeal=>0');
					array_push($dierep, '\'shop_open\' => 1', '$iseyKey => 0', '$tmpMeal => 2');
					$die_body = file_get_contents($die_file, LOCK_SH);
					file_put_contents($die_file, str_replace( $diestr, $dierep, $die_body ), LOCK_EX);
					
					$ecostr = array();
					$ecorep = array();
					$eco_file = APP_PATH.'/admin/controller/Encodes.php';
					array_push($ecostr, ';tpCache(', ';getUsersConfigData(');
					array_push($ecorep, ';if($ClearCopyright)tpCache(', ';if($ClearCopyright)getUsersConfigData(');
					$eco_body = file_get_contents($eco_file, LOCK_SH);
					file_put_contents($eco_file, str_replace( $ecostr, $ecorep, $eco_body ), LOCK_EX);
					
					$tmpMeal = 'cGhwX3NlcnZpY2VtZWFs';
					$tmpMeal = base64_decode($tmpMeal);
					$langRow = \think\Db::name('language')->order('id asc')->select();
					foreach ($langRow as $key => $val) {
					    tpCache('php', [$tmpMeal=>2], $val['mark']);
					}
					
					$logstr = array();
					$logrep = array();
					$log_file = APP_PATH.'/common/logic/FunctionLogic.php';
					array_push($logstr, 'die(', '"error";$');
					array_push($logrep, '@(', '"error";if($ClearCopyright)$');
					$log_body = file_get_contents($log_file, LOCK_SH);
					file_put_contents($log_file, str_replace( $logstr, $logrep, $log_body ), LOCK_EX);
				}
			}
		}
    }
}