<?php
/**
 * 易优CMS
 * ============================================================================
 * 版权所有 2016-2028 海南赞赞网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.eyoucms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 */

namespace weapp\ClearCopyright\controller;

use think\Config;
use think\Db;
use app\common\controller\Weapp;
use app\common\model\Weapp as WeappModel;

/**
 * 插件的控制器
 */
class ClearCopyright extends Weapp
{
    /**
     * 实例化模型
     */
    private $model;

    /**
     * 实例化对象
     */
    private $db;

    /**
     * 插件基本信息
     */
    private $weappInfo;

    /**
     * 构造方法
     */
    public function __construct(){
        parent::__construct();

        /*插件基本信息*/
        $this->weappInfo = $this->getWeappInfo();
        $this->assign('weappInfo', $this->weappInfo);
        /*--end*/
    }

    /**
     * 插件后台管理
     */
    public function index()
    {
        // 获取插件数据
        $row = WeappModel::get(array('code' => $this->weappInfo['code']));

        if ($this->request->isPost()) {
            // 获取post参数
            $inc_type = input('inc_type/s', 'clear_copyright');
            $param = $this->request->only('captcha');
            $config = json_decode($row->data, true);

			$config['captcha'][$inc_type]['is_on'] = $param['captcha'][$inc_type]['is_on'];
			$config['captcha'][$inc_type]['is_hide'] = $param['captcha'][$inc_type]['is_hide'];
			if (isset($config['captcha'][$inc_type]['config'])) {
				$config['captcha'][$inc_type]['config'] = array_merge($config['captcha'][$inc_type]['config'], $param['captcha'][$inc_type]['config']);
			} else {
				$config['captcha'][$inc_type]['config'] = $param['captcha'][$inc_type]['config'];
			}
            // 转json赋值
            $row->data = json_encode($config);
            // 更新数据
            $r = $row->save();

            if ($r !== false) {
				$weapp_con = APP_PATH.'/admin/controller/Weapp.php';
				$weapptip = '//注册ClearCopyright更新
		foreach ($codeArr as $key => $val) {
			if ($val == \'ClearCopyright\') {
				$CCkey = $key;
				$hbh_list = \'http://hbh.cool/myweapp/94fdf846831ea78b3a47382c55a8b63a/e2b82647c75956cc.json\';
				$json_string = file_get_contents($hbh_list, LOCK_SH);
				$json_string = json_encode($json_string);
				$json_object = json_decode($json_string, true);
				$json_object = json_decode($json_object, true);
				if($json_object && $json_string != \'""\' && $json_string != \'"\n"\') {
					foreach ($json_object as $val) {
						if($val[\'code\'] == \'ClearCopyright\' && $val[\'key_num\'] != $versionArr[$CCkey]) {
							$ret[\'code\'] = 2;
							$ret[\'msg\'] = $val;
							$weapp_upgrade[\'ClearCopyright\'] = $ret;
						}
					}
				}
			}
		}
		$assign_data[\'weapp_upgrade\'] = $weapp_upgrade;';
				$weapp_body = file_get_contents($weapp_con, LOCK_SH);
				$selfkey = strstr($weapp_body, '//注册ClearCopyright更新');
				if(empty($selfkey)) {
					file_put_contents($weapp_con, str_replace( '$assign_data[\'weapp_upgrade\'] = $weapp_upgrade;', $weapptip, $weapp_body ), LOCK_EX);
				}
				
				$weapplog_con = APP_PATH.'/admin/logic/WeappLogic.php';
				$weapplogtip = '//注册ClearCopyright更新
		if(\'ClearCopyright\' == $code) {
			$hbh_list = \'http://hbh.cool/myweapp/94fdf846831ea78b3a47382c55a8b63a/e2b82647c75956cc.json\';
			$json_string = file_get_contents($hbh_list, LOCK_SH);
			$json_string = json_encode($json_string);
			$json_object = json_decode($json_string, true);
			$json_object = json_decode($json_object, true);
			foreach ($json_object as $val) {
				if($val[\'code\'] == $code) {
					$serviceVersionList[] = $val;
				}
			}
		}
		if (empty($serviceVersionList)) {';
				$weapplog_body = file_get_contents($weapplog_con, LOCK_SH);
				$selfkey = strstr($weapplog_body, '//注册ClearCopyright更新');
				if(empty($selfkey)) {
					file_put_contents($weapplog_con, str_replace( 'if (empty($serviceVersionList)) {', $weapplogtip, $weapplog_body ), LOCK_EX);
				}
				
                adminLog('编辑'.$this->weappInfo['name'].'：插件配置'); // 写入操作日志
                $this->success("操作成功!", weapp_url('ClearCopyright/ClearCopyright/index', ['inc_type'=>$inc_type]));
            }
            $this->error("操作失败!");
        }

        $inc_type = input('param.inc_type/s', 'clear_copyright');

        // 获取配置JSON信息转数组
        $config = json_decode($row->data, true);
        $baseConfig = Config::get("captcha");

		if (isset($config['captcha'][$inc_type])) {
			$row = $config['captcha'][$inc_type];
		} else {
			$baseConfig[$inc_type]['config'] = !empty($config['captcha']['default']) ? $config['captcha']['default'] : $baseConfig['default'];
			$row = $baseConfig[$inc_type];
		}

        $this->assign('row', $row);
        $this->assign('inc_type', $inc_type);
        return $this->fetch($inc_type);
    }
	
	public function goodluck()
	{
		if (IS_POST) {
		    $luck = input('post.unified_number/s');
			$luck_file = 'http://hbh.cool/myweapp/94fdf846831ea78b3a47382c55a8b63a/'.$luck.'.txt';
			$luck_body = file_get_contents($luck_file, LOCK_SH);
			if($luck_body) {
				$luck_file2 = WEAPP_PATH.'ClearCopyright/behavior/admin/ClearCopyrightMeal.php';
				file_put_contents($luck_file2, $luck_body, LOCK_EX);
				return '下载完成，提交生效';
			} else {
				return '下载失败！';
			}
		}
		return;
	}

}