/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : eyoucms_develop

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-09-13 14:30:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for #@__weapp_zan
-- ----------------------------
DROP TABLE IF EXISTS `#@__weapp_zan`;
CREATE TABLE `#@__weapp_zan` (
  `zan_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `aid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文档ID',
  `users_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点赞用户ID',
  `zan_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点赞',
  `users_ip` varchar(20) NOT NULL DEFAULT '' COMMENT '用户IP地址',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`zan_id`),
  KEY `aid` (`aid`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='文档点赞表';
