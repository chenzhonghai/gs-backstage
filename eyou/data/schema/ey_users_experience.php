<?php 
return array (
  'id' => 
  array (
    'name' => 'id',
    'type' => 'int(11)',
    'notnull' => false,
    'default' => NULL,
    'primary' => true,
    'autoinc' => true,
  ),
  'occupation' => 
  array (
    'name' => 'occupation',
    'type' => 'text',
    'notnull' => false,
    'default' => NULL,
    'primary' => false,
    'autoinc' => false,
  ),
  'education' => 
  array (
    'name' => 'education',
    'type' => 'text',
    'notnull' => false,
    'default' => NULL,
    'primary' => false,
    'autoinc' => false,
  ),
  'industry' => 
  array (
    'name' => 'industry',
    'type' => 'text',
    'notnull' => false,
    'default' => NULL,
    'primary' => false,
    'autoinc' => false,
  ),
  'users_id' => 
  array (
    'name' => 'users_id',
    'type' => 'int(11)',
    'notnull' => false,
    'default' => NULL,
    'primary' => false,
    'autoinc' => false,
  ),
  'sex' => 
  array (
    'name' => 'sex',
    'type' => 'varchar(50)',
    'notnull' => false,
    'default' => NULL,
    'primary' => false,
    'autoinc' => false,
  ),
  'introduce' => 
  array (
    'name' => 'introduce',
    'type' => 'varchar(255)',
    'notnull' => false,
    'default' => NULL,
    'primary' => false,
    'autoinc' => false,
  ),
  'authentication' => 
  array (
    'name' => 'authentication',
    'type' => 'int(1)',
    'notnull' => false,
    'default' => '0',
    'primary' => false,
    'autoinc' => false,
  ),
  'add_time' => 
  array (
    'name' => 'add_time',
    'type' => 'int(11)',
    'notnull' => false,
    'default' => NULL,
    'primary' => false,
    'autoinc' => false,
  ),
  'background_map' => 
  array (
    'name' => 'background_map',
    'type' => 'varchar(255)',
    'notnull' => false,
    'default' => 'https://st0.dancf.com/csc/740/topics/0/20210226-111127-6da2.jpg?x-oss-process=image/resize,w_385/sharpen,100/interlace,1,image/format,webp',
    'primary' => false,
    'autoinc' => false,
  ),
  'personal_description' => 
  array (
    'name' => 'personal_description',
    'type' => 'varchar(255)',
    'notnull' => false,
    'default' => NULL,
    'primary' => false,
    'autoinc' => false,
  ),
);