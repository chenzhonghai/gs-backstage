<?php if (!defined('THINK_PATH')) exit(); /*a:7:{s:43:"./application/admin/template/shop/index.htm";i:1642406897;s:57:"/mnt/api/api/application/admin/template/public/layout.htm";i:1640913882;s:60:"/mnt/api/api/application/admin/template/public/theme_css.htm";i:1640913882;s:53:"/mnt/api/api/application/admin/template/shop/left.htm";i:1640913887;s:57:"/mnt/api/api/application/admin/template/shop/shop_bar.htm";i:1640913887;s:55:"/mnt/api/api/application/admin/template/public/page.htm";i:1640913882;s:57:"/mnt/api/api/application/admin/template/public/footer.htm";i:1640913882;}*/ ?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link href="/api/public/plugins/layui/css/layui.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/main.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/page.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/font/css/font-awesome.min.css" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="/api/public/static/admin/font/css/font-awesome-ie7.min.css">
<![endif]-->
<script type="text/javascript">
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
    var module_name = "<?php echo MODULE_NAME; ?>";
    var GetUploadify_url = "<?php echo url('Uploadify/upload'); ?>";
    var __root_dir__ = "/api";
    var __lang__ = "<?php echo $admin_lang; ?>";
</script>  
<link href="/api/public/static/admin/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="/api/public/static/admin/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
<!-- <link type="text/css" rel="stylesheet" href="/api/public/plugins/tags_input/css/jquery.tagsinput.css?v=<?php echo $version; ?>"> -->
<style type="text/css">html, body { overflow: visible;}</style>
<link href="/api/public/static/admin/css/diy_style.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css" />

<!-- 官方内置样式表，升级会覆盖变动，请勿修改，否则后果自负 -->

<style type="text/css">
	/*左侧收缩图标*/
	#foldSidebar i { font-size: 24px;color:<?php echo $global['web_theme_color']; ?>; }
    /*左侧菜单*/
    .eycms_cont_left{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl dd a:hover,.eycms_cont_left dl dd a.on,.eycms_cont_left dl dt.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl dd a:active{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist dd{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl.jslist dd a:hover,.eycms_cont_left dl.jslist dd a.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist:hover{ background:<?php echo $global['web_assist_color']; ?>; }
    /*栏目操作*/
    .cate-dropup .cate-dropup-con a:hover{ background-color: <?php echo $global['web_theme_color']; ?>; }
    /*按钮*/
    a.ncap-btn-green { background-color: <?php echo $global['web_theme_color']; ?>; }
    a:hover.ncap-btn-green { background-color: <?php echo $global['web_assist_color']; ?>; }
    .flexigrid .sDiv2 .btn:hover { background-color: <?php echo $global['web_theme_color']; ?>; }
    .flexigrid .mDiv .fbutton div.add{background-color: <?php echo $global['web_theme_color']; ?>; border: none;}
    .flexigrid .mDiv .fbutton div.add:hover{ background-color: <?php echo $global['web_assist_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds{color:<?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds:hover{ background-color: <?php echo $global['web_theme_color']; ?>;}
    /*选项卡字体*/
    .tab-base a.current,
    .tab-base a:hover.current { border-bottom: solid 2px <?php echo $global['web_theme_color']; ?> !important;color: <?php echo $global['web_theme_color']; ?> !important;}
    .addartbtn input.btn:hover{ border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>; }
    .addartbtn input.btn.selected{ color: <?php echo $global['web_theme_color']; ?>;border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>;}
	/*会员导航*/
	.member-nav-group .member-nav-item .btn.selected{background: <?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;box-shadow: -1px 0 0 0 <?php echo $global['web_theme_color']; ?>;}
	.member-nav-group .member-nav-item:first-child .btn.selected{border-left: 1px solid <?php echo $global['web_theme_color']; ?> !important;}
	/*搜索按钮图标*/
	.flexigrid .sDiv2 .fa-search{}
        
    /* 商品订单列表标题 */
   .flexigrid .mDiv .ftitle h3 {border-left: 3px solid <?php echo $global['web_theme_color']; ?>;} 
	
    /*分页*/
    .pagination > .active > a, .pagination > .active > a:focus, 
	.pagination > .active > a:hover, 
	.pagination > .active > span, 
	.pagination > .active > span:focus, 
	.pagination > .active > span:hover { border-color: <?php echo $global['web_theme_color']; ?>;color:<?php echo $global['web_theme_color']; ?>; }
    
    .layui-form-onswitch {border-color: <?php echo $global['web_theme_color']; ?> !important;background-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-enable.selected { background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-disable.selected {background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    input[type="text"]:focus,
    input[type="text"]:hover,
    input[type="text"]:active,
    input[type="password"]:focus,
    input[type="password"]:hover,
    input[type="password"]:active,
    textarea:hover,
    textarea:focus,
    textarea:active { border-color:<?php echo hex2rgba($global['web_theme_color'],0.8); ?>;box-shadow: 0 0 0 2px <?php echo hex2rgba($global['web_theme_color'],0.15); ?> !important;}
    .input-file-show:hover .type-file-button {background-color:<?php echo $global['web_theme_color']; ?> !important; }
    .input-file-show:hover {border-color: <?php echo $global['web_theme_color']; ?> !important;box-shadow: 0 0 5px <?php echo hex2rgba($global['web_theme_color'],0.5); ?> !important;}
    .input-file-show:hover span.show a,
    .input-file-show span.show a:hover { color: <?php echo $global['web_theme_color']; ?> !important;}
    .input-file-show:hover .type-file-button {background-color: <?php echo $global['web_theme_color']; ?> !important; }
    .color_z { color: <?php echo $global['web_theme_color']; ?> !important;}
    a.imgupload{
        background-color: <?php echo $global['web_theme_color']; ?> !important;
        border-color: <?php echo $global['web_theme_color']; ?> !important;
    }
    /*专题节点按钮*/
    .ncap-form-default .special-add{background-color:<?php echo $global['web_theme_color']; ?>;border-color:<?php echo $global['web_theme_color']; ?>;}
    .ncap-form-default .special-add:hover{background-color:<?php echo $global['web_assist_color']; ?>;border-color:<?php echo $global['web_assist_color']; ?>;}
    
    /*更多功能标题*/
    .on-off_panel .title::before {background-color:<?php echo $global['web_theme_color']; ?>;}

</style>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.js"></script>
<!-- <script type="text/javascript" src="/api/public/plugins/tags_input/js/jquery.tagsinput.js?v=<?php echo $version; ?>"></script> -->
<script type="text/javascript" src="/api/public/static/admin/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/api/public/plugins/layer-v3.1.0/layer.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/admin.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/common.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/api/public/plugins/layui/layui.js"></script>
<script src="/api/public/static/admin/js/myFormValidate.js"></script>
<script src="/api/public/static/admin/js/myAjax2.js?v=<?php echo $version; ?>"></script>
<script src="/api/public/static/admin/js/global.js?v=<?php echo $version; ?>"></script>
</head>

<body class="bodystyle" style="overflow-y: scroll; cursor: default; -moz-user-select: inherit;min-width:auto;">
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="sidebar-second ">
    <ul>
        <li class="sidebar-second-title">商城中心</li>
        <li>
            <a <?php if('Statistics' == CONTROLLER_NAME): ?>class="active"<?php endif; ?> href='<?php echo url("Statistics/index"); ?>'>数据统计</a>
        </li>
        <li>
            <a <?php if(('ShopService' == CONTROLLER_NAME) OR ('Shop' == CONTROLLER_NAME and in_array(ACTION_NAME, ['index', 'order_details']))): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/index"); ?>'>所有订单</a>
        </li>
        <li>
            <a <?php if('ShopProduct' == CONTROLLER_NAME and in_array(ACTION_NAME, ['index', 'add', 'edit'])): ?>class="active"<?php endif; ?> href='<?php echo url("ShopProduct/index"); ?>'>商品管理</a>
        </li>
        <li>
            <a <?php if('ShopProduct' == CONTROLLER_NAME and in_array(ACTION_NAME, ['attrlist_index', 'attribute_index'])): ?>class="active"<?php endif; ?> href='<?php echo url("ShopProduct/attrlist_index"); ?>'>商品参数</a>
        </li>
        <?php 
            $shop_open_spec = getUsersConfigData('shop.shop_open_spec');
         if($shop_open_spec == '1'): ?>
        <li>
            <a <?php if('Shop' == CONTROLLER_NAME and in_array(ACTION_NAME, ['spec_index'])): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/spec_index"); ?>'>商品规格</a>
        </li>
        <?php endif; ?>
        <li>
            <a <?php if('Shop' == CONTROLLER_NAME and 'conf' == ACTION_NAME): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/conf"); ?>'>商城配置</a>
        </li>
        <?php if($php_servicemeal >= 2): ?>
        <li>
            <a <?php if('ShopComment' == CONTROLLER_NAME): ?>class="active"<?php endif; ?> href='<?php echo url("ShopComment/comment_index"); ?>'>商品评价</a>
        </li>
        <li>
            <a <?php if((in_array(CONTROLLER_NAME, ['Sharp','Coupon'])) OR ('Shop' == CONTROLLER_NAME and in_array(ACTION_NAME, ['market_index']))): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/market_index"); ?>'>营销功能</a>
        </li>
        <?php endif; ?>
    </ul>
</div>
<div class="page" style="min-width:auto;margin-left:98px;">
    <div class="fixed-bar">
    <div class="item-title">
        <!-- <a class="back" href="<?php echo url("Shop/index"); ?>" title="返回列表"><i class="fa fa-angle-double-left"></i>返回</a></a> -->
        <ul class="tab-base nc-row">
            <li>
                <a <?php if('index' == ACTION_NAME and '' == \think\Request::instance()->param('order_status')): ?>class="current"<?php endif; ?> href='<?php echo url("Shop/index"); ?>'>
                    <span>全部</span>
                </a>
            </li>

            <li>
                <a <?php if(\think\Request::instance()->param('order_status') == '10'): ?>class="current"<?php endif; ?> href='<?php echo url("Shop/index", ["order_status"=>10]); ?>'>
                    <span>待付款</span>
                </a>
            </li>

            <li>
                <a <?php if(\think\Request::instance()->param('order_status') == '1'): ?>class="current"<?php endif; ?> href='<?php echo url("Shop/index", ["order_status"=>1]); ?>' href="<?php echo url('Shop/conf'); ?>">
                    <span>待发货</span>
                </a>
            </li>

            <li>
                <a <?php if(\think\Request::instance()->param('order_status') == '2'): ?>class="current"<?php endif; ?> href='<?php echo url("Shop/index", ["order_status"=>2]); ?>' href="<?php echo url('Shop/conf'); ?>">
                    <span>已发货</span>
                </a>
            </li>

            <li>
                <a <?php if(\think\Request::instance()->param('order_status') == '3'): ?>class="current"<?php endif; ?> href='<?php echo url("Shop/index", ["order_status"=>3]); ?>' href="<?php echo url('Shop/conf'); ?>">
                    <span>已完成</span>
                </a>
            </li>
            
            <?php if($php_servicemeal > 1): ?>
            <li>
                <a <?php if('ShopService' == CONTROLLER_NAME): ?>class="current"<?php endif; ?> href='<?php echo url("ShopService/after_service"); ?>'>
                    <span>售后</span>
                </a>
            </li>
            <?php endif; ?>
            
        </ul>
    </div>
</div>
    <div class="flexigrid">
        <div class="mDiv">
            <div class="ftitle">
                <?php if('index' == ACTION_NAME and '' == \think\Request::instance()->param('order_status')): ?><h3>全部列表</h3><?php endif; if(\think\Request::instance()->param('order_status') == '10'): ?><h3>待付款列表</h3><?php endif; if(\think\Request::instance()->param('order_status') == '1'): ?><h3>待发货列表</h3><?php endif; if(\think\Request::instance()->param('order_status') == '2'): ?><h3>已发货列表</h3><?php endif; if(\think\Request::instance()->param('order_status') == '3'): ?><h3>已完成列表</h3><?php endif; ?>
                <h5>(共<?php echo $pager->totalRows; ?>条数据)</h5>
            </div>
            <form class="navbar-form form-inline" id="postForm" action="<?php echo url('Shop/index'); ?>" method="get" onsubmit="layer_loading('正在处理');">
                <?php echo (isset($searchform['hidden']) && ($searchform['hidden'] !== '')?$searchform['hidden']:''); ?>
                <div class="sDiv">
                    <div class="fl">
                        <!-- #weapp_ExportOrder_btn# -->
                    </div>
                    <div class="sDiv2">
                        <input type="text" name="add_time_begin" id="add_time_begin" autocomplete="off" value="<?php echo \think\Request::instance()->param('add_time_begin'); ?>" class="qsbox" placeholder="起始日期">
                    </div>
                    &nbsp;至&nbsp;
                    <div class="sDiv2">
                        <input type="text" name="add_time_end" id="add_time_end" autocomplete="off" value="<?php echo \think\Request::instance()->param('add_time_end'); ?>" class="qsbox" placeholder="结束日期">
                    </div>
                    <div class="sDiv2">
                        <input type="text" size="50" name="order_code" value="<?php echo \think\Request::instance()->param('keywords'); ?>" class="qsbox" style="width: 200px;" placeholder="搜索订单号...">
                        <input type="submit" class="btn" value="搜索">
						<i class="fa fa-search"></i>
                    </div>

                </div>
            </form>
        </div>
        <div class="hDiv">
            <div class="hDivBox">
                <table cellspacing="0" cellpadding="0" style="width: 100%">
                    <thead>
                    <tr>
                        <th class="sign w40" axis="col0">
                            <div class="tc"><input type="checkbox" autocomplete="off" class="checkAll"></div>
                        </th>
						<th align="center" abbr="article_title" axis="col3" class="w60">
						    <div class="tc">缩略图</div>
						</th>
                        <th abbr="article_title" axis="col3" class="">
                            <div class="text-l10">订单号</div>
                        </th>
                        <th abbr="article_time" axis="col6" class="w100">
                            <div class="tc">用户名</div>
                        </th>
                        
                       <th abbr="article_time" axis="col6" class="w100">
                            <div class="tc">支付方式</div>
                        </th>
                        
                        <th abbr="article_time" axis="col6" class="w100">
                            <div class="tc">订单金额<span class="hint" data-hint="当前列表订单总金额：￥<?php echo (isset($total_money) && ($total_money !== '')?$total_money:'0.00'); ?>"><i class="fa fa-jpy ml5"></i></span></div>
                        </th>
                        <!--<th abbr="article_time" axis="col6" class="w100">
                            <div class="tc">支付方式</div>
                        </th>-->
                        <th abbr="article_time" axis="col6" class="w100">
                            <div class="tc">订单状态</div>
                        </th>
                        <th abbr="article_time" axis="col6" class="w160">
                            <div class="tc">下单时间</div>
                        </th>
                        <!--<th abbr="article_time" axis="col6" class="w160">
                            <div class="tc">支付时间</div>
                        </th>-->
                        <th axis="col1" class="w160">
                            <div class="tc">操作</div>
                        </th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="bDiv" style="height: auto;">
            <div id="flexigrid" cellpadding="0" cellspacing="0" border="0">
                <table style="width: 100%">
                    <tbody>
                    <?php if(empty($list) || (($list instanceof \think\Collection || $list instanceof \think\Paginator ) && $list->isEmpty())): ?>
                        <tr>
                            <td class="no-data" align="center" axis="col0" colspan="50">
                                <i class="fa fa-exclamation-circle"></i>没有符合条件的记录
                            </td>
                        </tr>
                    <?php else: if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                        <tr>
                            <td class="sign">
                                <div class="w40 tc"><input type="checkbox" autocomplete="off" name="ids[]" value="<?php echo $vo['order_id']; ?>"></div>
                            </td>
							<td class="w60">
							    <div class="tc">
                                    <a href="<?php echo $vo['arcurl']; ?>" target="_blank"><img width="60" height="60" src="<?php echo $vo['litpic']; ?>"></a>
							    </div>
							</td>
                            <td class="" style="width: 100%;">
                                <div class="tl text-l10">
                                    <a href="javascript:void(0);" data-href="<?php echo url('Shop/order_details',array('order_id'=>$vo['order_id'])); ?>" onclick="openFullframe(this, '订单详情', '100%', '100%');"><?php echo $vo['order_code']; ?></a>
                                </div>
                            </td>
                            <td class="">
                                <div class="w100 tc">
                                     <a href="javascript:void(0);" data-href="<?php echo url('Member/users_edit',array('id'=>$vo['users_id'])); ?>" onclick="openFullframe(this, '用户资料', '100%', '100%');"><?php echo $vo['username']; ?></a>
                                </div>
                            </td>
                            
                           <td class="">
                                <div class="w100 tc">
                                    <?php if($vo['payment_method'] == 0): ?>
                                    在线支付
                                    <?php endif; if($vo['payment_method'] == 1): ?>
                                    货到付款
                                    <?php endif; if($vo['payment_method'] == 2): ?>
                                   银联支付
                                    <?php endif; ?>
                                </div>
                            </td>
                            <td class="">
                                <div class="w100 tc">
                                    ￥<?php echo $vo['order_amount']; ?>
                                </div>
                            </td>
                            <!--
                            <td class="">
                                <div class="w100 tc">
                                    <?php if($vo['payment_method'] == '1'): ?>
                                        <?php echo $vo['pay_name']; else: ?>
                                        <?php echo (isset($pay_method_arr[$vo['pay_name']]) && ($pay_method_arr[$vo['pay_name']] !== '')?$pay_method_arr[$vo['pay_name']]:'未支付'); endif; ?>
                                </div>
                            </td>
                            -->
                            <td class="">
                                <div class="w100 tc">
                                    <?php echo (isset($admin_order_status_arr[$vo['order_status']]) && ($admin_order_status_arr[$vo['order_status']] !== '')?$admin_order_status_arr[$vo['order_status']]:''); ?>
                                </div>
                            </td>
                            <td class="">
                                <div class="w160 tc">
                                    <?php echo MyDate('Y-m-d H:i:s',$vo['add_time']); ?>
                                </div>
                            </td>
                            <!--
                            <td class="">
                                <div class="w160 tc">
                                    <?php if(empty($vo['pay_time']) || (($vo['pay_time'] instanceof \think\Collection || $vo['pay_time'] instanceof \think\Paginator ) && $vo['pay_time']->isEmpty())): ?>
                                        ————————
                                    <?php else: ?>
                                        <?php echo MyDate('Y-m-d H:i:s',$vo['pay_time']); endif; ?>
                                </div>
                            </td>
                            -->
                            <td class="operation">
                                <div class="w160 tc">
                                    <a href="javascript:void(0);" data-href="<?php echo url('Shop/order_details',array('order_id'=>$vo['order_id'])); ?>" class="btn blue" onclick="openFullframe(this, '订单详情', '100%', '100%');">详情</a>
                                    
                                      
                                    
                                    <?php if($vo['order_status'] == '0'): ?>
                                        <!-- 订单未付款时出现 -->
										<i></i>
                                        <a href="JavaScript:void(0);" onclick="OrderMark('yfk','<?php echo $vo['order_id']; ?>','<?php echo $vo['users_id']; ?>');" class="btn blue">
                                            付款
                                        </a>
										<i></i>
                                        <a href="JavaScript:void(0);" onclick="PromptChangePrice('<?php echo $vo['order_id']; ?>', '<?php echo $vo['order_amount']; ?>');" class="btn blue">
                                            改价
                                        </a>
                                        <script type="text/javascript">
                                            // 弹出改价输入框
                                            function PromptChangePrice(order_id, order_amount) {
                                                layer.prompt({
                                                    title: false, 
                                                    formType: 3,
                                                    id: 'BulkSetPrice',
                                                    btn: ['确定', '关闭'],
                                                    closeBtn: 0,
                                                    success: function(layero, index) {
                                                        $("#BulkSetPrice").find('input').attr('placeholder', '请输入应付金额');
                                                        $("#BulkSetPrice").find('input').attr('value', order_amount);
                                                        $("#BulkSetPrice").find('input').attr('onkeyup', "this.value=this.value.replace(/[^\\d.]/g,'')");
                                                        $("#BulkSetPrice").find('input').attr('onpaste', "this.value=this.value.replace(/[^\\d.]/g,'')");
                                                        var msg = '<span style="color: red;">改价后让用户重新进入订单列表点击支付</span>';
                                                        $("#BulkSetPrice").append(msg);
                                                        // 修改订单金额后为保证支付金额准确有效<br/>请让用户刷新订单列表页后重新点击支付
                                                    }
                                                }, function(price, index) {
                                                    if (0 < price) {
                                                        layer.close(index);
                                                        OrderChangePrice(order_id, order_amount, price);
                                                    } else {
                                                        layer.msg('应付金额不允许为0', {time: 1500});
                                                    }
                                                });
                                            }

                                            // 提交改价数据并追加一条订单操作记录
                                            function OrderChangePrice(OrderID, OrderAmount, Price) {
                                                if (0 >= Price) layer.msg('应付金额不允许为0', {time: 1500});
                                                layer_loading('正在处理');
                                                $.ajax({
                                                    type : 'post',
                                                    url  : "<?php echo url('Shop/order_change_price'); ?>",
                                                    data : {
                                                        order_id: OrderID,
                                                        order_amount_old: OrderAmount,
                                                        order_amount: Price,
                                                        _ajax: 1
                                                    },
                                                    dataType : 'json',
                                                    success : function(res) {
                                                        layer.closeAll();
                                                        if (1 == res.code) {
                                                            layer.msg(res.msg, {time: 1500}, function() {
                                                                window.location.reload();
                                                            });
                                                        } else {
                                                            layer.alert(res.msg, {title: false, closeBtn: 0}, function() {
                                                                window.location.reload();
                                                            });
                                                        }
                                                    }
                                                });
                                            }
                                        </script>
                                    <?php endif; if($vo['order_status'] == '1'): ?>
                                        <!-- 订单待发货时出现 -->
										<i></i>
                                        <a href="JavaScript:void(0);" data-url="<?php echo url('Shop/order_send', ['order_id'=>$vo['order_id']]); ?>" onclick="OrderSend(this);" class="btn blue">
                                            发货
                                        </a>
										<i></i>
                                        <a href="JavaScript:void(0);" class="btn blue" title="买家提醒商家发货次数">
                                            提醒
                                            <?php if(!(empty($LogData[$vo['order_id']]['action_count']) || (($LogData[$vo['order_id']]['action_count'] instanceof \think\Collection || $LogData[$vo['order_id']]['action_count'] instanceof \think\Paginator ) && $LogData[$vo['order_id']]['action_count']->isEmpty()))): ?>
                                                <em class="num"><?php echo $LogData[$vo['order_id']]['action_count']; ?></em>
                                            <?php endif; ?>
                                        </a>
                                    <?php endif; if($vo['pay_name'] == 'voucher'): ?>
                            			<i></i>
                                        <a href="javascript:void(0);" data-href="<?php echo url('Shop/voucher',array('id'=>$vo['order_id'])); ?>" class="btn blue" onclick="openFullframe(this, '订单详情', '100%', '100%');">查看支付凭证</a>
                                        <?php endif; if($vo['order_status'] == '2'): ?>
                                        <!-- 订单已发货时出现 -->
                                        
										<i></i>
                                        <a href="JavaScript:void(0);" onclick="OrderMark('ysh','<?php echo $vo['order_id']; ?>','<?php echo $vo['users_id']; ?>');" class="btn blue">
                                            完成
                                        </a>
										<i></i>
                                        <a href="JavaScript:void(0);" onclick="OrderMark('ddbz','<?php echo $vo['order_id']; ?>','<?php echo $vo['users_id']; ?>','<?php echo $vo['admin_note']; ?>', '<?php echo MyDate("Y-m-d H:i:s",$vo['update_time']); ?>');" class="btn blue">
                                            备注
                                            <?php if(!(empty($vo['admin_note']) || (($vo['admin_note'] instanceof \think\Collection || $vo['admin_note'] instanceof \think\Paginator ) && $vo['admin_note']->isEmpty()))): ?>
                                                <em class="num" style="z-index: 9999; width: 8px; height: 8px; line-height: 8px; right: -2px; top: -2px;"></em>
                                            <?php endif; ?>
                                            <input type="hidden" id="beizhu-url" value="<?php echo url('Shop/update_note'); ?>">
                                        </a>
                                    <?php endif; if(-1 == $vo['order_status'] or 3 == $vo['order_status']): ?>
                                        <!-- 订单取消或取消过期时出现 -->
										<i></i>
                                        <a href="JavaScript:void(0);" onclick="OrderMark('ddbz','<?php echo $vo['order_id']; ?>','<?php echo $vo['users_id']; ?>','<?php echo $vo['admin_note']; ?>', '<?php echo MyDate("Y-m-d H:i:s",$vo['update_time']); ?>');" class="btn blue">
                                            备注
                                            <?php if(!(empty($vo['admin_note']) || (($vo['admin_note'] instanceof \think\Collection || $vo['admin_note'] instanceof \think\Paginator ) && $vo['admin_note']->isEmpty()))): ?>
                                                <em class="num" style="z-index: 9999; width: 8px; height: 8px; line-height: 8px; right: -2px; top: -2px;"></em>
                                            <?php endif; ?>
                                            <input type="hidden" id="beizhu-url" value="<?php echo url('Shop/update_note'); ?>">
                                        </a>
										<i></i>
                                        <a href="JavaScript:void(0);" onclick="OrderMark('ddsc','<?php echo $vo['order_id']; ?>','<?php echo $vo['users_id']; ?>');" class="btn blue">
                                            删除
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; endif; else: echo "" ;endif; endif; ?>
                    </tbody>
                </table>
            </div>
            <div class="iDiv" style="display: none;"></div>
        </div>
        <div class="tDiv">
            <div class="tDiv2">
                <div class="fbutton checkboxall">
                    <input type="checkbox" autocomplete="off" class="checkAll">
                </div>
                <div class="fbutton">
                    <a onclick="batch_del(this, 'ids');" data-url="<?php echo url('Shop/order_del'); ?>" class="layui-btn layui-btn-primary">
                            <span>批量删除</span>
                    </a>
                </div>
                <div class="fbuttonr">
    <div class="pages">
       <?php echo $page; ?>
    </div>
</div>
<div class="fbuttonr">
    <div class="total">
        <h5>共有<?php echo $pager->totalRows; ?>条,每页显示
            <select name="pagesize" style="width: 60px;" onchange="ey_selectPagesize(this);">
                <option <?php if($pager->listRows == 20): ?> selected <?php endif; ?> value="20">20</option>
                <option <?php if($pager->listRows == 50): ?> selected <?php endif; ?> value="50">50</option>
                <option <?php if($pager->listRows == 100): ?> selected <?php endif; ?> value="100">100</option>
                <option <?php if($pager->listRows == 200): ?> selected <?php endif; ?> value="200">200</option>
            </select>
        </h5>
    </div>
</div>
            </div>
            <div style="clear:both"></div>
        </div>
        <!--分页位置-->
    </div>
</div>
<script>
    
    $(function(){
        $('input[name*=ids]').click(function(){
            if ($('input[name*=ids]').length == $('input[name*=ids]:checked').length) {
                $('.checkAll').prop('checked','checked');
            } else {
                $('.checkAll').prop('checked', false);
            }
        });
        $('input[type=checkbox].checkAll').click(function(){
            $('input[type=checkbox]').prop('checked',this.checked);
        });
    });

    layui.use('laydate', function(){
        var laydate = layui.laydate;
        //执行一个laydate实例
        laydate.render({
            elem: '#add_time_begin' //指定元素
        });
        laydate.render({
            elem: '#add_time_end' //指定元素
        });
    });

    $(document).ready(function(){
        // 表格行点击选中切换
        $('#flexigrid > table>tbody >tr').click(function(){
            $(this).toggleClass('trSelected');
        });

        // 点击刷新数据
        $('.fa-refresh').click(function(){
            location.href = location.href;
        });

        <?php if($is_syn_theme_shop == '1'): ?>
            syn_theme_shop();
        <?php endif; ?>
        function syn_theme_shop()
        {
            layer_loading('订单初始化');
            // 确定
            $.ajax({
                type : 'get',
                url : "<?php echo url('Shop/ajax_syn_theme_shop'); ?>",
                data : {_ajax:1},
                dataType : 'json',
                success : function(res){
                    layer.closeAll();
                    if(res.code == 1){
                        layer.msg(res.msg, {icon: 1, time: 1000});
                    }else{
                        layer.alert(res.msg, {icon: 2, title:false}, function(){
                            window.location.reload();
                        });
                    }
                },
                error: function(e){
                    layer.closeAll();
                    layer.alert(ey_unknown_error, {icon: 2, title:false}, function(){
                        window.location.reload();
                    });
                }
            })
        }
    });

    function OrderQueryStatus(){
        $('#postForm').submit();
    }

    function OrderSend(obj){
        var url = $(obj).attr('data-url');

        // iframe窗
        var iframes = layer.open({
            type: 2,
            title: '订单发货详情',
            fixed: true, //不固定
            shadeClose: false,
            shade: 0.3,
            area: ['100%', '100%'],
            content: url
        });
        layer.full(iframes);
    }

    // 订单操作
    function OrderMark(status_name, order_id, users_id, admin_note, update_time) {
        if('yfk' == status_name){
            var msg = '确认订单已付款？';
        }else if('ysh' == status_name){
            var msg = '确认订单已收货？';
        }else if('gbdd' == status_name){
            var msg = '确认关闭订单？';
        }else if('ddbz' == status_name){
            layer.prompt({
                formType: 2,
                value: admin_note,
                title: false,
                closeBtn: 0,
                id: 'AdminNote',
                area: ['300px', '100px'],
                success: function(layero, index) {
                    if (admin_note) {
                        var msg = '<br/><span style="color: #999; font-size: 12px;">最后更新：'+update_time+'</span>';
                        $("#AdminNote").append(msg);
                    }
                }
            }, function(value, index, elem) {
                UpNote(order_id,value);
                layer.close(index);
            });
            return false;
        }else if('ddsc' == status_name){
            var msg = '确认删除订单？';
        }

        layer.confirm(msg, {
            title: false,
            closeBtn: 0,
            btn: ['确定','取消'],
        },function(){
            $.ajax({
                url: "<?php echo url('Shop/order_mark_status'); ?>",
                data: {order_id:order_id,status_name:status_name,users_id:users_id, _ajax:1},
                type:'post',
                dataType:'json',
                success:function(res){
                    layer.closeAll();
                    if (1 == res.code) {
                        layer.msg(res.msg, {time: 1500},function(){
                            window.location.reload();
                        });
                    }else{
                        layer.msg(res.msg, {time: 1500});
                    }
                }
            });
        },function(index){
            layer.closeAll(index);
        });
    }

    function UpNote(order_id,admin_note){
        $.ajax({
            url: "<?php echo url('Shop/update_note'); ?>",
            data: {order_id:order_id,admin_note:admin_note, _ajax:1},
            type:'post',
            dataType:'json',
            success:function(res){
                layer.closeAll();
                if ('1' == res.code) {
                    layer.msg(res.msg, {time: 1500},function(){
                        window.location.reload();
                    });
                }else{
                    layer.msg(res.msg, {time: 1500});
                }
            }
        });
    }
</script>

<br/>
<div id="goTop">
    <a href="JavaScript:void(0);" id="btntop">
        <i class="fa fa-angle-up"></i>
    </a>
    <a href="JavaScript:void(0);" id="btnbottom">
        <i class="fa fa-angle-down"></i>
    </a>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#think_page_trace_open').css('z-index', 99999);
    });
</script>
</body>
</html>