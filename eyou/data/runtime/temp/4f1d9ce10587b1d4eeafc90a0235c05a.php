<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:59:"./application/admin/template/shop_product/attrlist_edit.htm";i:1640913889;s:57:"/mnt/api/api/application/admin/template/public/layout.htm";i:1640913882;s:60:"/mnt/api/api/application/admin/template/public/theme_css.htm";i:1640913882;s:57:"/mnt/api/api/application/admin/template/public/footer.htm";i:1640913882;}*/ ?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link href="/api/public/plugins/layui/css/layui.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/main.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/page.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/font/css/font-awesome.min.css" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="/api/public/static/admin/font/css/font-awesome-ie7.min.css">
<![endif]-->
<script type="text/javascript">
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
    var module_name = "<?php echo MODULE_NAME; ?>";
    var GetUploadify_url = "<?php echo url('Uploadify/upload'); ?>";
    var __root_dir__ = "/api";
    var __lang__ = "<?php echo $admin_lang; ?>";
</script>  
<link href="/api/public/static/admin/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="/api/public/static/admin/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
<!-- <link type="text/css" rel="stylesheet" href="/api/public/plugins/tags_input/css/jquery.tagsinput.css?v=<?php echo $version; ?>"> -->
<style type="text/css">html, body { overflow: visible;}</style>
<link href="/api/public/static/admin/css/diy_style.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css" />

<!-- 官方内置样式表，升级会覆盖变动，请勿修改，否则后果自负 -->

<style type="text/css">
	/*左侧收缩图标*/
	#foldSidebar i { font-size: 24px;color:<?php echo $global['web_theme_color']; ?>; }
    /*左侧菜单*/
    .eycms_cont_left{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl dd a:hover,.eycms_cont_left dl dd a.on,.eycms_cont_left dl dt.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl dd a:active{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist dd{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl.jslist dd a:hover,.eycms_cont_left dl.jslist dd a.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist:hover{ background:<?php echo $global['web_assist_color']; ?>; }
    /*栏目操作*/
    .cate-dropup .cate-dropup-con a:hover{ background-color: <?php echo $global['web_theme_color']; ?>; }
    /*按钮*/
    a.ncap-btn-green { background-color: <?php echo $global['web_theme_color']; ?>; }
    a:hover.ncap-btn-green { background-color: <?php echo $global['web_assist_color']; ?>; }
    .flexigrid .sDiv2 .btn:hover { background-color: <?php echo $global['web_theme_color']; ?>; }
    .flexigrid .mDiv .fbutton div.add{background-color: <?php echo $global['web_theme_color']; ?>; border: none;}
    .flexigrid .mDiv .fbutton div.add:hover{ background-color: <?php echo $global['web_assist_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds{color:<?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds:hover{ background-color: <?php echo $global['web_theme_color']; ?>;}
    /*选项卡字体*/
    .tab-base a.current,
    .tab-base a:hover.current { border-bottom: solid 2px <?php echo $global['web_theme_color']; ?> !important;color: <?php echo $global['web_theme_color']; ?> !important;}
    .addartbtn input.btn:hover{ border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>; }
    .addartbtn input.btn.selected{ color: <?php echo $global['web_theme_color']; ?>;border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>;}
	/*会员导航*/
	.member-nav-group .member-nav-item .btn.selected{background: <?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;box-shadow: -1px 0 0 0 <?php echo $global['web_theme_color']; ?>;}
	.member-nav-group .member-nav-item:first-child .btn.selected{border-left: 1px solid <?php echo $global['web_theme_color']; ?> !important;}
	/*搜索按钮图标*/
	.flexigrid .sDiv2 .fa-search{}
        
    /* 商品订单列表标题 */
   .flexigrid .mDiv .ftitle h3 {border-left: 3px solid <?php echo $global['web_theme_color']; ?>;} 
	
    /*分页*/
    .pagination > .active > a, .pagination > .active > a:focus, 
	.pagination > .active > a:hover, 
	.pagination > .active > span, 
	.pagination > .active > span:focus, 
	.pagination > .active > span:hover { border-color: <?php echo $global['web_theme_color']; ?>;color:<?php echo $global['web_theme_color']; ?>; }
    
    .layui-form-onswitch {border-color: <?php echo $global['web_theme_color']; ?> !important;background-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-enable.selected { background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-disable.selected {background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    input[type="text"]:focus,
    input[type="text"]:hover,
    input[type="text"]:active,
    input[type="password"]:focus,
    input[type="password"]:hover,
    input[type="password"]:active,
    textarea:hover,
    textarea:focus,
    textarea:active { border-color:<?php echo hex2rgba($global['web_theme_color'],0.8); ?>;box-shadow: 0 0 0 2px <?php echo hex2rgba($global['web_theme_color'],0.15); ?> !important;}
    .input-file-show:hover .type-file-button {background-color:<?php echo $global['web_theme_color']; ?> !important; }
    .input-file-show:hover {border-color: <?php echo $global['web_theme_color']; ?> !important;box-shadow: 0 0 5px <?php echo hex2rgba($global['web_theme_color'],0.5); ?> !important;}
    .input-file-show:hover span.show a,
    .input-file-show span.show a:hover { color: <?php echo $global['web_theme_color']; ?> !important;}
    .input-file-show:hover .type-file-button {background-color: <?php echo $global['web_theme_color']; ?> !important; }
    .color_z { color: <?php echo $global['web_theme_color']; ?> !important;}
    a.imgupload{
        background-color: <?php echo $global['web_theme_color']; ?> !important;
        border-color: <?php echo $global['web_theme_color']; ?> !important;
    }
    /*专题节点按钮*/
    .ncap-form-default .special-add{background-color:<?php echo $global['web_theme_color']; ?>;border-color:<?php echo $global['web_theme_color']; ?>;}
    .ncap-form-default .special-add:hover{background-color:<?php echo $global['web_assist_color']; ?>;border-color:<?php echo $global['web_assist_color']; ?>;}
    
    /*更多功能标题*/
    .on-off_panel .title::before {background-color:<?php echo $global['web_theme_color']; ?>;}

</style>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.js"></script>
<!-- <script type="text/javascript" src="/api/public/plugins/tags_input/js/jquery.tagsinput.js?v=<?php echo $version; ?>"></script> -->
<script type="text/javascript" src="/api/public/static/admin/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/api/public/plugins/layer-v3.1.0/layer.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/admin.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/common.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/api/public/plugins/layui/layui.js"></script>
<script src="/api/public/static/admin/js/myFormValidate.js"></script>
<script src="/api/public/static/admin/js/myAjax2.js?v=<?php echo $version; ?>"></script>
<script src="/api/public/static/admin/js/global.js?v=<?php echo $version; ?>"></script>
</head>
<body style="background-color: #FFF; overflow: auto;">
<div id="toolTipLayer" style="position: absolute; z-index: 9999; display: none; visibility: visible; left: 95px; top: 573px;"></div>
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="page" style="box-shadow:none;">
    <form class="form-horizontal" id="PostForm">
        <input type="hidden" name="list_id" value="<?php echo $list['list_id']; ?>">
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label for="ac_name"><em>*</em>参数名称</label>
                </dt>
                <dd class="opt">
                    <input type="text" name="list_name" value="<?php echo $list['list_name']; ?>" id="list_name" class="input-txt" autocomplete="off">
                    <span class="err" id="err_attr_name" style="color: #F00; display: none;"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="ac_name">参数描述</label>
                </dt>
                <dd class="opt">
                    <input type="text" name="desc" value="<?php echo $list['desc']; ?>" id="desc" class="input-txt" autocomplete="off">
                    <p class="notic"></p>
                </dd>
            </dl>
            
          <dl class="row">

                <dt class="tit">

                    <label for="title"><em>*</em>所属栏目</label>

                </dt>

                <dd class="opt"> 

                    <select name="aid" id="aid">

                        <option value="0">请选择品牌…</option>
                        
                         <?php if(is_array($arc) || $arc instanceof \think\Collection || $arc instanceof \think\Paginator): if( count($arc)==0 ) : echo "" ;else: foreach($arc as $k=>$vo): ?>
                            <option value="<?php echo $vo['id']; ?>" <?php if($list['aid'] == $vo['id']): ?>selected<?php endif; ?>><?php echo $vo['typename']; ?></option></option>
                         <?php endforeach; endif; else: echo "" ;endif; ?>
                    </select>

                    <span class="err"></span>

                </dd>

            </dl>
            
            
            

            <dl class="row">
                <dt class="tit">
                    <label for="attr_input_type">参数值列表</label>
                </dt>
                <dd class="opt">
                    <a href="JavaScript:void(0);" class="ncap-btn ncap-btn-green" onclick="Add();">新增</a>
                    <div class="" style="margin-top: 15px;width: 850px;">
                        <div class="flexigrid">
                            <div class="hDiv">
                                <div class="hDivBox">
                                    <table cellspacing="0" cellpadding="0" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th abbr="article_title" axis="col3" class="">
                                                    <div style="text-align: left; padding-left: 10px;" class="">参数名称</div>
                                                </th>
                                                
                                                <th abbr="article_time" axis="col4" class="w150">
                                                    <div class="tc">参数类型</div>
                                                </th>

                                                <th abbr="article_time" axis="col4" class="w250">
                                                    <div class="tc" id="PromptText">参数可选值</div>
                                                </th>

                                                <th abbr="article_time" axis="col4" class="w100">
                                                    <div class="tc" >排序</div>
                                                </th>

                                                <th abbr="article_time" axis="col6" class="w60">
                                                    <div class="tc">操作</div>
                                                </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="bDiv" style="height: auto;">
                                    <div id="flexigrid" cellpadding="0" cellspacing="0" border="0">
                                        <table style="width: 100%;">
                                            <tbody id="Template">
                                            <?php if(empty($list['attr']) || (($list['attr'] instanceof \think\Collection || $list['attr'] instanceof \think\Paginator ) && $list['attr']->isEmpty())): ?>
                                                <tr id="empty_tr">
                                                    <td class="no-data" align="center" axis="col0" colspan="50">
                                                        <i class="fa fa-exclamation-circle"></i>没有符合条件的记录
                                                    </td>
                                                </tr>
                                            <?php else: if(is_array($list['attr']) || $list['attr'] instanceof \think\Collection || $list['attr'] instanceof \think\Paginator): if( count($list['attr'])==0 ) : echo "" ;else: foreach($list['attr'] as $k=>$vo): ?>
                                                    <tr class="tr" id="tr_<?php echo $k; ?>">
                                                        <td style="width: 100%">
                                                            <div style="">
                                                                <input type="text" name="attr_name[]" value="<?php echo $vo['attr_name']; ?>" style="width: 90%;" autocomplete="off">
                                                                <input type="hidden" name="attr_id[]" value="<?php echo $vo['attr_id']; ?>">
                                                            </div>
                                                        </td>

                                                        <td>
                                                            <div class="w150 tc">
                                                                <select name="attr_input_type[]" class="w100" data-id="OptContent_<?php echo $k; ?>" onchange="ChangeDisplay(this);">
                                                                    <option value="0" <?php if($vo['attr_input_type'] == '0'): ?> selected="true" <?php endif; ?>>手工录入</option>
                                                                    <option value="1" <?php if($vo['attr_input_type'] == '1'): ?> selected="true" <?php endif; ?>>下拉选择</option>
                                                                </select>
                                                            </div>
                                                        </td>

                                                        <td>
                                                            <div class="w250 tc" id="OptContent_<?php echo $k; ?>">
                                                                <?php if(1 == $vo['attr_input_type']): ?>
                                                                    <textarea rows="5" cols="30" name="attr_values[]" style="height: 60px;" placeholder="一行代表一个可选值"><?php echo $vo['attr_values']; ?></textarea>
                                                                <?php else: ?>
                                                                    ——————
                                                                    <input type="hidden" name="attr_values[]" value="<?php echo $vo['attr_values']; ?>" class="w160">
                                                                <?php endif; ?>
                                                            </div>
                                                        </td>

                                                        <td>
                                                            <div class="w100 tc">
                                                                <input type="text" name="attr_sort_order[]" style="width: 60px;text-align: center;" value="<?php echo $vo['sort_order']; ?>" >
                                                            </div>
                                                        </td>

                                                        <td class="">
                                                            <div class="w60 tc">
                                                                <a class="btn red" href="javascript:void(0);" data-id="tr_<?php echo $k; ?>" onclick="DelHtml(this)">删除</a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; endif; else: echo "" ;endif; endif; ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </dd>
            </dl>
        </div>
        <div class="ncap-form-default">
            <div class="bot">
                <a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" onclick="SubmitForm(this);" data-url="<?php echo url('ShopProduct/attrlist_edit', ['_ajax'=>1]); ?>">确认提交</a>
            </div>
        </div> 
    </form>
</div>

<script type="text/javascript">

    // tr数,取唯一标识
    var tr_id = $('#Template .tr').length;

    function Add() {
        if (0 == tr_id){
            $('#empty_tr').hide();
        }
        var AddHtml = '';
        AddHtml += 
            '<tr class="tr" id="tr_'+tr_id+'">'+
                '<td style="width: 100%">'+
                    '<div style="">'+
                        '<input type="text" name="attr_name[]" style="width: 90%;" autocomplete="off">'+
                        '<input type="hidden" name="attr_id[]">'+
                    '</div>'+
                '</td>'+

                '<td>'+
                    '<div class="w150 tc">'+
                        '<select name="attr_input_type[]" class="w100" data-id="OptContent_'+tr_id+'" onchange="ChangeDisplay(this);">'+
                            '<option value="0">手工录入</option>'+
                            '<option value="1">下拉选择</option>' +
                        '</select>'+
                    '</div>'+
                '</td>'+

                '<td>'+
                    '<div class="w250 tc" id="OptContent_'+tr_id+'">'+
                        '——————<input type="hidden" name="attr_values[]" autocomplete="off" class="w160">'+
                    '</div>'+
                '</td>'+

                '<td>'+
                    '<div class="w100 tc">'+
                        '<input type="text" name="attr_sort_order[]" style="width: 60px;text-align: center;" value="100" >'+
                    '</div>'+
                '</td>'+

                '<td class="">'+
                    '<div class="w60 tc">'+
                        '<a class="btn red" href="javascript:void(0);" data-id="tr_'+tr_id+'" onclick="DelHtml(this)">删除</a>'+
                    '</div>'+
                '</td>'+
            '</tr>';
        $('#Template').append(AddHtml);
        tr_id++;
    }

    // 删除未保存的数据
    function DelHtml(obj){
        $('#'+$(obj).attr('data-id')).remove();
        if (0 == $('.tr').length) $('#empty_tr').show();
    }

    // 字段切换，字段可选值框跟着改变
    function ChangeDisplay(obj) {
        var value = $(obj).val();
        if (1 == value){
            //多行文本
            var html = '<textarea rows="5" cols="30" name="attr_values[]" style="height: 60px;" placeholder="一行代表一个可选值"></textarea>';
        } else{
            var html = '——————<input type="hidden" name="attr_values[]" autocomplete="off" class="w160">';
        }
        $('#'+$(obj).attr('data-id')).empty().html(html);
    }

    // 添加新增数据
    function SubmitForm(obj) {
        var parentObj = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引

        if($.trim($('input[name=list_name]').val()) == ''){
            showErrorMsg('参数名称不能为空！');
            $('input[name=list_name]').focus();
            return false;
        }

        layer_loading('正在处理');
        $.ajax({
            url : $(obj).attr('data-url'),
            data: $('#PostForm').serialize(),
            type: 'post',
            dataType: 'json',
            success: function(res) {
                layer.closeAll();
                if (res.code == 1) {
                    var _parent = parent;
                    _parent.layer.close(parentObj);
                    _parent.layer.msg(res.msg, {icon: 1, shade: 0.3, time: 1000}, function(){
                        _parent.window.location.reload();
                    });
                } else {
                    showErrorAlert(res.msg);
                }
            },
            error: function(e) {
                layer.closeAll();
                showErrorAlert(e.responseText);
            }
        })
    }
</script>
<br/>
<div id="goTop">
    <a href="JavaScript:void(0);" id="btntop">
        <i class="fa fa-angle-up"></i>
    </a>
    <a href="JavaScript:void(0);" id="btnbottom">
        <i class="fa fa-angle-down"></i>
    </a>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#think_page_trace_open').css('z-index', 99999);
    });
</script>
</body>
</html>