<?php if (!defined('THINK_PATH')) exit(); /*a:7:{s:51:"./application/admin/template/member/money_index.htm";i:1640913878;s:57:"/mnt/api/api/application/admin/template/public/layout.htm";i:1640913882;s:60:"/mnt/api/api/application/admin/template/public/theme_css.htm";i:1640913882;s:54:"/mnt/api/api/application/admin/template/member/bar.htm";i:1640913877;s:60:"/mnt/api/api/application/admin/template/member/order_bar.htm";i:1640913878;s:55:"/mnt/api/api/application/admin/template/public/page.htm";i:1640913882;s:57:"/mnt/api/api/application/admin/template/public/footer.htm";i:1640913882;}*/ ?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link href="/api/public/plugins/layui/css/layui.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/main.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/page.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/font/css/font-awesome.min.css" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="/api/public/static/admin/font/css/font-awesome-ie7.min.css">
<![endif]-->
<script type="text/javascript">
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
    var module_name = "<?php echo MODULE_NAME; ?>";
    var GetUploadify_url = "<?php echo url('Uploadify/upload'); ?>";
    var __root_dir__ = "/api";
    var __lang__ = "<?php echo $admin_lang; ?>";
</script>  
<link href="/api/public/static/admin/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="/api/public/static/admin/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
<!-- <link type="text/css" rel="stylesheet" href="/api/public/plugins/tags_input/css/jquery.tagsinput.css?v=<?php echo $version; ?>"> -->
<style type="text/css">html, body { overflow: visible;}</style>
<link href="/api/public/static/admin/css/diy_style.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css" />

<!-- 官方内置样式表，升级会覆盖变动，请勿修改，否则后果自负 -->

<style type="text/css">
	/*左侧收缩图标*/
	#foldSidebar i { font-size: 24px;color:<?php echo $global['web_theme_color']; ?>; }
    /*左侧菜单*/
    .eycms_cont_left{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl dd a:hover,.eycms_cont_left dl dd a.on,.eycms_cont_left dl dt.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl dd a:active{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist dd{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl.jslist dd a:hover,.eycms_cont_left dl.jslist dd a.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist:hover{ background:<?php echo $global['web_assist_color']; ?>; }
    /*栏目操作*/
    .cate-dropup .cate-dropup-con a:hover{ background-color: <?php echo $global['web_theme_color']; ?>; }
    /*按钮*/
    a.ncap-btn-green { background-color: <?php echo $global['web_theme_color']; ?>; }
    a:hover.ncap-btn-green { background-color: <?php echo $global['web_assist_color']; ?>; }
    .flexigrid .sDiv2 .btn:hover { background-color: <?php echo $global['web_theme_color']; ?>; }
    .flexigrid .mDiv .fbutton div.add{background-color: <?php echo $global['web_theme_color']; ?>; border: none;}
    .flexigrid .mDiv .fbutton div.add:hover{ background-color: <?php echo $global['web_assist_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds{color:<?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds:hover{ background-color: <?php echo $global['web_theme_color']; ?>;}
    /*选项卡字体*/
    .tab-base a.current,
    .tab-base a:hover.current { border-bottom: solid 2px <?php echo $global['web_theme_color']; ?> !important;color: <?php echo $global['web_theme_color']; ?> !important;}
    .addartbtn input.btn:hover{ border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>; }
    .addartbtn input.btn.selected{ color: <?php echo $global['web_theme_color']; ?>;border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>;}
	/*会员导航*/
	.member-nav-group .member-nav-item .btn.selected{background: <?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;box-shadow: -1px 0 0 0 <?php echo $global['web_theme_color']; ?>;}
	.member-nav-group .member-nav-item:first-child .btn.selected{border-left: 1px solid <?php echo $global['web_theme_color']; ?> !important;}
	/*搜索按钮图标*/
	.flexigrid .sDiv2 .fa-search{}
        
    /* 商品订单列表标题 */
   .flexigrid .mDiv .ftitle h3 {border-left: 3px solid <?php echo $global['web_theme_color']; ?>;} 
	
    /*分页*/
    .pagination > .active > a, .pagination > .active > a:focus, 
	.pagination > .active > a:hover, 
	.pagination > .active > span, 
	.pagination > .active > span:focus, 
	.pagination > .active > span:hover { border-color: <?php echo $global['web_theme_color']; ?>;color:<?php echo $global['web_theme_color']; ?>; }
    
    .layui-form-onswitch {border-color: <?php echo $global['web_theme_color']; ?> !important;background-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-enable.selected { background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-disable.selected {background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    input[type="text"]:focus,
    input[type="text"]:hover,
    input[type="text"]:active,
    input[type="password"]:focus,
    input[type="password"]:hover,
    input[type="password"]:active,
    textarea:hover,
    textarea:focus,
    textarea:active { border-color:<?php echo hex2rgba($global['web_theme_color'],0.8); ?>;box-shadow: 0 0 0 2px <?php echo hex2rgba($global['web_theme_color'],0.15); ?> !important;}
    .input-file-show:hover .type-file-button {background-color:<?php echo $global['web_theme_color']; ?> !important; }
    .input-file-show:hover {border-color: <?php echo $global['web_theme_color']; ?> !important;box-shadow: 0 0 5px <?php echo hex2rgba($global['web_theme_color'],0.5); ?> !important;}
    .input-file-show:hover span.show a,
    .input-file-show span.show a:hover { color: <?php echo $global['web_theme_color']; ?> !important;}
    .input-file-show:hover .type-file-button {background-color: <?php echo $global['web_theme_color']; ?> !important; }
    .color_z { color: <?php echo $global['web_theme_color']; ?> !important;}
    a.imgupload{
        background-color: <?php echo $global['web_theme_color']; ?> !important;
        border-color: <?php echo $global['web_theme_color']; ?> !important;
    }
    /*专题节点按钮*/
    .ncap-form-default .special-add{background-color:<?php echo $global['web_theme_color']; ?>;border-color:<?php echo $global['web_theme_color']; ?>;}
    .ncap-form-default .special-add:hover{background-color:<?php echo $global['web_assist_color']; ?>;border-color:<?php echo $global['web_assist_color']; ?>;}
    
    /*更多功能标题*/
    .on-off_panel .title::before {background-color:<?php echo $global['web_theme_color']; ?>;}

</style>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.js"></script>
<!-- <script type="text/javascript" src="/api/public/plugins/tags_input/js/jquery.tagsinput.js?v=<?php echo $version; ?>"></script> -->
<script type="text/javascript" src="/api/public/static/admin/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/api/public/plugins/layer-v3.1.0/layer.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/admin.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/common.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/api/public/plugins/layui/layui.js"></script>
<script src="/api/public/static/admin/js/myFormValidate.js"></script>
<script src="/api/public/static/admin/js/myAjax2.js?v=<?php echo $version; ?>"></script>
<script src="/api/public/static/admin/js/global.js?v=<?php echo $version; ?>"></script>
</head>

<body class="bodystyle" style="overflow-y: scroll; cursor: default; -moz-user-select: inherit;">
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="page">
        <div class="fixed-bar">
        <div class="item-title">
            <ul class="tab-base nc-row">
                <?php if(is_check_access('Member@users_index') == '1'): ?>
                    <li>
                        <a href="<?php echo url('Member/users_index'); ?>" <?php if(in_array(ACTION_NAME, ['users_index','level_index','attr_index','users_config'])): ?>class="current"<?php endif; ?>>
                            <span>会员列表</span>
                        </a>
                    </li>
                <?php endif; if(is_check_access('Member@money_index') == '1'): if(1 == $userConfig['pay_open']): ?>
                        <li>
                            <a href="<?php echo url('Member/money_index'); ?>" <?php if(in_array(ACTION_NAME, ['money_index', 'money_edit','media_index','upgrade_index'])): ?>class="current"<?php endif; ?>>
                                <span>订单管理</span>
                            </a>
                        </li>
                    <?php endif; endif; if(is_check_access('UsersRelease@conf') == '1'): if(1 == $userConfig['users_open_release']): ?>
                        <li>
                            <a href="<?php echo url('UsersRelease/conf'); ?>" <?php if(in_array(CONTROLLER_NAME, ['UsersRelease'])): ?>class="current"<?php endif; ?>>
                                <span>会员投稿</span>
                            </a>
                        </li>
                    <?php endif; endif; if($php_servicemeal > 1): if(is_check_access('UsersScore@conf') == '1'): ?>
                    <li>
                        <a href="<?php echo url('UsersScore/conf'); ?>" <?php if(in_array(CONTROLLER_NAME, ['UsersScore'])): ?>class="current"<?php endif; ?>>
                            <span>积分管理</span>
                        </a>
                    </li>
                    <?php endif; endif; ?>
            </ul>
        </div>
    </div>
    <div class="flexigrid">
        <div class="mDiv">
            <div class="ftitle">
                    <div class="member-nav-group">
        <?php if(is_check_access('Member@money_index') == '1'): ?>
            <label class="member-nav-item">
                <input type="button" class="btn <?php if(!in_array(\think\Request::instance()->action(), ['money_index'])): ?>current<?php else: ?>selected<?php endif; ?>" value="余额充值" onclick="window.location.href='<?php echo url("Member/money_index"); ?>';">
            </label>
        <?php endif; if(is_check_access('Level@index') == '1'): if(1 == $userConfig['level_member_upgrade']): if(is_check_access('Level@upgrade_index') == '1'): ?>
                    <label class="member-nav-item">
                        <input type="button" class="btn <?php if(!in_array(\think\Request::instance()->action(), ['upgrade_index'])): ?>current<?php else: ?>selected<?php endif; ?>" value="会员升级" onclick="window.location.href='<?php echo url("Level/upgrade_index"); ?>';">
                    </label>
                <?php endif; endif; endif; if(is_check_access('Member@media_index') == '1'): if(!empty($channeltype_row[5]['status'])): if(is_check_access('Member@media_index') == '1'): ?>
                    <label class="member-nav-item">
                        <input type="button" class="btn <?php if(!in_array(\think\Request::instance()->action(), ['media_index'])): ?>current<?php else: ?>selected<?php endif; ?>" value="视频订单" onclick="window.location.href='<?php echo url("Member/media_index"); ?>';">
                    </label>
                <?php endif; endif; endif; if(is_check_access('Member@article_index') == '1'): if(!empty($channeltype_row[1]['status']) && !empty($channelRow['data']['is_article_pay'])): ?>
                <label class="member-nav-item">
                    <input type="button" class="btn <?php if(!in_array(\think\Request::instance()->action(), ['article_index'])): ?>current<?php else: ?>selected<?php endif; ?>" value="文章订单" onclick="window.location.href='<?php echo url("Member/article_index"); ?>';">
                </label>
            <?php endif; endif; ?>
    </div>

            </div>
           
            <form class="navbar-form form-inline" id="postForm" action="<?php echo url('Member/money_index'); ?>" method="get" onsubmit="layer_loading('正在处理');">
                <?php echo (isset($searchform['hidden']) && ($searchform['hidden'] !== '')?$searchform['hidden']:''); ?>
                <div class="sDiv">
                    <div class="fl">
                        <!-- #weapp_ExportOrder_btn# -->
                    </div>
                    <div class="sDiv2">
                        <select name="status" class="select" style="margin:0px 5px;">
                            <option value="">--订单状态--</option>
                            <?php if(is_array($pay_status_arr) || $pay_status_arr instanceof \think\Collection || $pay_status_arr instanceof \think\Paginator): $i = 0; $__LIST__ = $pay_status_arr;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                            <option value="<?php echo $key; ?>" <?php if(\think\Request::instance()->param('status') == $key): ?> selected <?php endif; ?>><?php echo $vo; ?></option>
                            <?php endforeach; endif; else: echo "" ;endif; ?>
                        </select>
                    </div>
                    <div class="sDiv2">
                        <input type="text" name="add_time_begin" id="add_time_begin" value="<?php echo \think\Request::instance()->param('add_time_begin'); ?>" class="qsbox" autocomplete="off" placeholder="起始日期">
                    </div>
                    &nbsp;至&nbsp;
                    <div class="sDiv2">
                        <input type="text" name="add_time_end" id="add_time_end" value="<?php echo \think\Request::instance()->param('add_time_end'); ?>" class="qsbox" autocomplete="off" placeholder="结束日期">
                    </div>
                    <div class="sDiv2" style="margin-right: 6px;">
                        <input type="text" size="30" name="keywords" value="<?php echo \think\Request::instance()->param('keywords'); ?>" class="qsbox" placeholder="搜索订单号...">
                        <input type="submit" class="btn" value="搜索">
						<i class="fa fa-search"></i>
                    </div>
                </div>
            </form>
        </div>
        <div class="hDiv">
            <div class="hDivBox">
                <table cellspacing="0" cellpadding="0" style="width: 100%">
                    <thead>
                    <tr>
                        <th class="sign w40" axis="col0">
                            <div class="tc"><input type="checkbox" autocomplete="off" class="checkAll"></div>
                        </th>
                        <th abbr="ac_id" axis="col4">
                            <div class="text-l10">订单号</div>
                        </th>
                        <th abbr="ac_id" axis="col4" class="w100">
                            <div class="tc">充值金额<span class="hint" data-hint="当前列表订单总金额：￥<?php echo (isset($total_money) && ($total_money !== '')?$total_money:'0.00'); ?>"><i class="fa fa-jpy ml5"></i></span></div>
                        </th>
                        <th abbr="article_title" axis="col3" class="w150">
                            <div class="tc">用户名</div>
                        </th>
                        <th abbr="ac_id" axis="col4" class="w150">
                            <div class="tc">充值时间</div>
                        </th>
                        <th abbr="ac_id" axis="col4" class="w100">
                            <div class="tc">支付方式</div>
                        </th>
                        <th abbr="ac_id" axis="col4" class="w100">
                            <div class="tc">状态</div>
                        </th>
                        <th abbr="ac_id" axis="col4" class="w120">
                            <div class="tc">操作</div>
                        </th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="bDiv" style="height: auto;">
            <div id="flexigrid" cellpadding="0" cellspacing="0" border="0">
                <table style="width: 100%">
                    <tbody>
                    <?php if(empty($list) || (($list instanceof \think\Collection || $list instanceof \think\Paginator ) && $list->isEmpty())): ?>
                        <tr>
                            <td class="no-data" align="center" axis="col0" colspan="50">
                                <i class="fa fa-exclamation-circle"></i>没有符合条件的记录
                            </td>
                        </tr>
                    <?php else: if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $k=>$vo): ?>
                        <tr>
                            <td class="sign">
                                <div class="w40 tc"><input type="checkbox" autocomplete="off" name="ids[]" value="<?php echo $vo['moneyid']; ?>"></div>
                            </td>
                            <td style="width: 100%">
                                <div class="text-l10">
                                    <?php echo $vo['order_number']; ?>
                                </div>
                            </td>
                            <td>
                                <div class="w100 tc">
                                    ￥<?php echo $vo['money']; ?>
                                </div>
                            </td>
                            <td class="sort">
                                <div class="w150 tc">
                                    <a href="javascript:void(0);" data-href="<?php echo url('Member/users_edit', ['id'=>$vo['users_id'],'from'=>'money_index']); ?>" onclick="openFullframe(this, '会员中心 - 用户基本资料');"><?php echo $vo['username']; ?></a>
                                </div>
                            </td>
                            <td>
                                <div class="w150 tc">
                                    <?php echo date('Y-m-d H:i:s',$vo['add_time']); ?>
                                </div>
                            </td>
                            <td class="">
                                <div class="tc w100">
                                <?php if(!(empty($pay_method_arr[$vo['pay_method']]) || (($pay_method_arr[$vo['pay_method']] instanceof \think\Collection || $pay_method_arr[$vo['pay_method']] instanceof \think\Paginator ) && $pay_method_arr[$vo['pay_method']]->isEmpty()))): ?>
                                    <?php echo $pay_method_arr[$vo['pay_method']]; else: ?>
                                    ————
                                <?php endif; ?>
                                </div>
                            </td>
                            <td class="">
                                <div class="tc w100">
                                <?php echo $pay_status_arr[$vo['status']]; ?>
                                </div>
                            </td>
                            <td class="operation">
                                <div class="tc w120">
                                    <?php if($vo['status'] == 1): ?>
                                        <a class="btn blue"  href="javascript:void(0);" data-url="<?php echo url('Member/money_mark_order'); ?>" data-moneyid="<?php echo $vo['moneyid']; ?>" data-title="手工充值" data-status="<?php echo $vo['status']; ?>" data-username="<?php echo $vo['username']; ?>" data-money="<?php echo $vo['money']; ?>" onClick="handle(this);">充值</a>
										<i></i>
                                    <?php elseif($vo['status'] == 3): ?>
                                        <a class="btn blue"  href="javascript:void(0);" data-url="<?php echo url('Member/money_mark_order'); ?>" data-moneyid="<?php echo $vo['moneyid']; ?>" data-title="撤销充值" data-status="<?php echo $vo['status']; ?>" data-username="<?php echo $vo['username']; ?>" data-money="<?php echo $vo['money']; ?>" onClick="handle(this);">撤销</a>
										<i></i>
									<?php else: ?>
										<a class="btn grey"  href="javascript:void(0);" data-moneyid="<?php echo $vo['moneyid']; ?>" data-title="撤销充值" data-status="<?php echo $vo['status']; ?>" data-username="<?php echo $vo['username']; ?>" data-money="<?php echo $vo['money']; ?>">撤销</a>
                                        <i></i>
									<?php endif; if(is_check_access(CONTROLLER_NAME.'@money_del') == '1'): ?>
                                        <a class="btn red"  href="javascript:void(0);" data-url="<?php echo url('Member/money_del'); ?>" data-id="<?php echo $vo['moneyid']; ?>" onClick="delfun(this);">删除</a>
                                    <?php endif; ?>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; endif; else: echo "" ;endif; endif; ?>
                    </tbody>
                </table>
            </div>
            <div class="iDiv" style="display: none;"></div>
        </div>
        <div class="tDiv">
            <div class="tDiv2">
                <div class="fbutton checkboxall">
                    <input type="checkbox" autocomplete="off" class="checkAll">
                </div>
                <div class="fbutton">
                    <a onclick="batch_del(this, 'ids');" data-url="<?php echo url('Member/money_del'); ?>" class="layui-btn layui-btn-primary">
                            <span>批量删除</span>
                    </a>
                </div>
                <div class="fbuttonr">
    <div class="pages">
       <?php echo $page; ?>
    </div>
</div>
<div class="fbuttonr">
    <div class="total">
        <h5>共有<?php echo $pager->totalRows; ?>条,每页显示
            <select name="pagesize" style="width: 60px;" onchange="ey_selectPagesize(this);">
                <option <?php if($pager->listRows == 20): ?> selected <?php endif; ?> value="20">20</option>
                <option <?php if($pager->listRows == 50): ?> selected <?php endif; ?> value="50">50</option>
                <option <?php if($pager->listRows == 100): ?> selected <?php endif; ?> value="100">100</option>
                <option <?php if($pager->listRows == 200): ?> selected <?php endif; ?> value="200">200</option>
            </select>
        </h5>
    </div>
</div>
            </div>
            <div style="clear:both"></div>
        </div>
		
    </div>
</div> 
<script>
    $(function(){
        $('input[name*=ids]').click(function(){
            if ($('input[name*=ids]').length == $('input[name*=ids]:checked').length) {
                $('.checkAll').prop('checked','checked');
            } else {
                $('.checkAll').prop('checked', false);
            }
        });
        $('input[type=checkbox].checkAll').click(function(){
            $('input[type=checkbox]').prop('checked',this.checked);
        });
    });

    layui.use('laydate', function(){
        var laydate = layui.laydate;
        //执行一个laydate实例
        laydate.render({
            elem: '#add_time_begin' //指定元素
        });
        laydate.render({
            elem: '#add_time_end' //指定元素
        });
    });

    $(document).ready(function(){
        // 表格行点击选中切换
        $('#flexigrid > table>tbody >tr').click(function(){
            $(this).toggleClass('trSelected');
        });

        // 点击刷新数据
        $('.fa-refresh').click(function(){
            location.href = location.href;
        });

        $('#postForm select[name=status]').change(function(){
            $('#postForm').submit();
        });
    });

    // 订单处理
    function handle(obj){
        var msg = '';
        var status = $(obj).attr('data-status');
        var username = $(obj).attr('data-username');
        var money = $(obj).attr('data-money');
        if (1 == status) {
            msg = "将为【<font color='red'>"+username+"</font>】账户充值<font color='red'>￥"+money+"元</font>，确认执行？";
        } else if (3 == status) {
            msg = "将扣除【<font color='red'>"+username+"</font>】账户余额<font color='red'>￥"+money+"元</font>，确认执行？";
        }

        layer.confirm(msg, {
            title: false,//$(obj).data('title'),
            btn: ['确定','取消'] //按钮
        }, function(){
            layer_loading('正在处理');
            // 确定
            $.ajax({
                type : 'post',
                url : $(obj).attr('data-url'),
                data : {moneyid:$(obj).attr('data-moneyid'), _ajax:1},
                dataType : 'json',
                success : function(res){
                    layer.closeAll();
                    if(res.code == 1){
                        layer.msg(res.msg, {icon: 1, time:1000}, function(){
                            window.location.reload();
                        });
                    }else{
                        layer.alert(res.msg, {icon: 2, title:false});
                    }
                },
                error:function(){
                    layer.closeAll();
                    layer.alert(ey_unknown_error, {icon: 2, title:false});
                }
            })
        }, function(index){
            layer.closeAll();
        });
        return false;
    }
</script>

<br/>
<div id="goTop">
    <a href="JavaScript:void(0);" id="btntop">
        <i class="fa fa-angle-up"></i>
    </a>
    <a href="JavaScript:void(0);" id="btnbottom">
        <i class="fa fa-angle-down"></i>
    </a>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#think_page_trace_open').css('z-index', 99999);
    });
</script>
</body>
</html>