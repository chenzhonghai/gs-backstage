<?php if (!defined('THINK_PATH')) exit(); /*a:6:{s:60:"./application/admin/template/shop_product/attrlist_index.htm";i:1640913889;s:57:"/mnt/api/api/application/admin/template/public/layout.htm";i:1640913882;s:60:"/mnt/api/api/application/admin/template/public/theme_css.htm";i:1640913882;s:53:"/mnt/api/api/application/admin/template/shop/left.htm";i:1640913887;s:55:"/mnt/api/api/application/admin/template/public/page.htm";i:1640913882;s:57:"/mnt/api/api/application/admin/template/public/footer.htm";i:1640913882;}*/ ?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link href="/api/public/plugins/layui/css/layui.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/main.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/page.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/font/css/font-awesome.min.css" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="/api/public/static/admin/font/css/font-awesome-ie7.min.css">
<![endif]-->
<script type="text/javascript">
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
    var module_name = "<?php echo MODULE_NAME; ?>";
    var GetUploadify_url = "<?php echo url('Uploadify/upload'); ?>";
    var __root_dir__ = "/api";
    var __lang__ = "<?php echo $admin_lang; ?>";
</script>  
<link href="/api/public/static/admin/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="/api/public/static/admin/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
<!-- <link type="text/css" rel="stylesheet" href="/api/public/plugins/tags_input/css/jquery.tagsinput.css?v=<?php echo $version; ?>"> -->
<style type="text/css">html, body { overflow: visible;}</style>
<link href="/api/public/static/admin/css/diy_style.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css" />

<!-- 官方内置样式表，升级会覆盖变动，请勿修改，否则后果自负 -->

<style type="text/css">
	/*左侧收缩图标*/
	#foldSidebar i { font-size: 24px;color:<?php echo $global['web_theme_color']; ?>; }
    /*左侧菜单*/
    .eycms_cont_left{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl dd a:hover,.eycms_cont_left dl dd a.on,.eycms_cont_left dl dt.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl dd a:active{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist dd{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl.jslist dd a:hover,.eycms_cont_left dl.jslist dd a.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist:hover{ background:<?php echo $global['web_assist_color']; ?>; }
    /*栏目操作*/
    .cate-dropup .cate-dropup-con a:hover{ background-color: <?php echo $global['web_theme_color']; ?>; }
    /*按钮*/
    a.ncap-btn-green { background-color: <?php echo $global['web_theme_color']; ?>; }
    a:hover.ncap-btn-green { background-color: <?php echo $global['web_assist_color']; ?>; }
    .flexigrid .sDiv2 .btn:hover { background-color: <?php echo $global['web_theme_color']; ?>; }
    .flexigrid .mDiv .fbutton div.add{background-color: <?php echo $global['web_theme_color']; ?>; border: none;}
    .flexigrid .mDiv .fbutton div.add:hover{ background-color: <?php echo $global['web_assist_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds{color:<?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds:hover{ background-color: <?php echo $global['web_theme_color']; ?>;}
    /*选项卡字体*/
    .tab-base a.current,
    .tab-base a:hover.current { border-bottom: solid 2px <?php echo $global['web_theme_color']; ?> !important;color: <?php echo $global['web_theme_color']; ?> !important;}
    .addartbtn input.btn:hover{ border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>; }
    .addartbtn input.btn.selected{ color: <?php echo $global['web_theme_color']; ?>;border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>;}
	/*会员导航*/
	.member-nav-group .member-nav-item .btn.selected{background: <?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;box-shadow: -1px 0 0 0 <?php echo $global['web_theme_color']; ?>;}
	.member-nav-group .member-nav-item:first-child .btn.selected{border-left: 1px solid <?php echo $global['web_theme_color']; ?> !important;}
	/*搜索按钮图标*/
	.flexigrid .sDiv2 .fa-search{}
        
    /* 商品订单列表标题 */
   .flexigrid .mDiv .ftitle h3 {border-left: 3px solid <?php echo $global['web_theme_color']; ?>;} 
	
    /*分页*/
    .pagination > .active > a, .pagination > .active > a:focus, 
	.pagination > .active > a:hover, 
	.pagination > .active > span, 
	.pagination > .active > span:focus, 
	.pagination > .active > span:hover { border-color: <?php echo $global['web_theme_color']; ?>;color:<?php echo $global['web_theme_color']; ?>; }
    
    .layui-form-onswitch {border-color: <?php echo $global['web_theme_color']; ?> !important;background-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-enable.selected { background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-disable.selected {background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    input[type="text"]:focus,
    input[type="text"]:hover,
    input[type="text"]:active,
    input[type="password"]:focus,
    input[type="password"]:hover,
    input[type="password"]:active,
    textarea:hover,
    textarea:focus,
    textarea:active { border-color:<?php echo hex2rgba($global['web_theme_color'],0.8); ?>;box-shadow: 0 0 0 2px <?php echo hex2rgba($global['web_theme_color'],0.15); ?> !important;}
    .input-file-show:hover .type-file-button {background-color:<?php echo $global['web_theme_color']; ?> !important; }
    .input-file-show:hover {border-color: <?php echo $global['web_theme_color']; ?> !important;box-shadow: 0 0 5px <?php echo hex2rgba($global['web_theme_color'],0.5); ?> !important;}
    .input-file-show:hover span.show a,
    .input-file-show span.show a:hover { color: <?php echo $global['web_theme_color']; ?> !important;}
    .input-file-show:hover .type-file-button {background-color: <?php echo $global['web_theme_color']; ?> !important; }
    .color_z { color: <?php echo $global['web_theme_color']; ?> !important;}
    a.imgupload{
        background-color: <?php echo $global['web_theme_color']; ?> !important;
        border-color: <?php echo $global['web_theme_color']; ?> !important;
    }
    /*专题节点按钮*/
    .ncap-form-default .special-add{background-color:<?php echo $global['web_theme_color']; ?>;border-color:<?php echo $global['web_theme_color']; ?>;}
    .ncap-form-default .special-add:hover{background-color:<?php echo $global['web_assist_color']; ?>;border-color:<?php echo $global['web_assist_color']; ?>;}
    
    /*更多功能标题*/
    .on-off_panel .title::before {background-color:<?php echo $global['web_theme_color']; ?>;}

</style>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.js"></script>
<!-- <script type="text/javascript" src="/api/public/plugins/tags_input/js/jquery.tagsinput.js?v=<?php echo $version; ?>"></script> -->
<script type="text/javascript" src="/api/public/static/admin/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/api/public/plugins/layer-v3.1.0/layer.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/admin.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/common.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/api/public/plugins/layui/layui.js"></script>
<script src="/api/public/static/admin/js/myFormValidate.js"></script>
<script src="/api/public/static/admin/js/myAjax2.js?v=<?php echo $version; ?>"></script>
<script src="/api/public/static/admin/js/global.js?v=<?php echo $version; ?>"></script>
</head>

<body style="overflow: auto; cursor: default; -moz-user-select: inherit;background-color:#F4F4F4; padding: 20px; ">
    <div id="append_parent"></div>
    <div id="ajaxwaitid"></div>
    <?php if(empty($oldinlet) || (($oldinlet instanceof \think\Collection || $oldinlet instanceof \think\Paginator ) && $oldinlet->isEmpty())): ?>
        <div class="sidebar-second ">
    <ul>
        <li class="sidebar-second-title">商城中心</li>
        <li>
            <a <?php if('Statistics' == CONTROLLER_NAME): ?>class="active"<?php endif; ?> href='<?php echo url("Statistics/index"); ?>'>数据统计</a>
        </li>
        <li>
            <a <?php if(('ShopService' == CONTROLLER_NAME) OR ('Shop' == CONTROLLER_NAME and in_array(ACTION_NAME, ['index', 'order_details']))): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/index"); ?>'>所有订单</a>
        </li>
        <li>
            <a <?php if('ShopProduct' == CONTROLLER_NAME and in_array(ACTION_NAME, ['index', 'add', 'edit'])): ?>class="active"<?php endif; ?> href='<?php echo url("ShopProduct/index"); ?>'>商品管理</a>
        </li>
        <li>
            <a <?php if('ShopProduct' == CONTROLLER_NAME and in_array(ACTION_NAME, ['attrlist_index', 'attribute_index'])): ?>class="active"<?php endif; ?> href='<?php echo url("ShopProduct/attrlist_index"); ?>'>商品参数</a>
        </li>
        <?php 
            $shop_open_spec = getUsersConfigData('shop.shop_open_spec');
         if($shop_open_spec == '1'): ?>
        <li>
            <a <?php if('Shop' == CONTROLLER_NAME and in_array(ACTION_NAME, ['spec_index'])): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/spec_index"); ?>'>商品规格</a>
        </li>
        <?php endif; ?>
        <li>
            <a <?php if('Shop' == CONTROLLER_NAME and 'conf' == ACTION_NAME): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/conf"); ?>'>商城配置</a>
        </li>
        <?php if($php_servicemeal >= 2): ?>
        <li>
            <a <?php if('ShopComment' == CONTROLLER_NAME): ?>class="active"<?php endif; ?> href='<?php echo url("ShopComment/comment_index"); ?>'>商品评价</a>
        </li>
        <li>
            <a <?php if((in_array(CONTROLLER_NAME, ['Sharp','Coupon'])) OR ('Shop' == CONTROLLER_NAME and in_array(ACTION_NAME, ['market_index']))): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/market_index"); ?>'>营销功能</a>
        </li>
        <?php endif; ?>
    </ul>
</div>
    <?php endif; ?>
    <div class="page" <?php if(empty($oldinlet) || (($oldinlet instanceof \think\Collection || $oldinlet instanceof \think\Paginator ) && $oldinlet->isEmpty())): ?>style="min-width:auto;margin-left:98px;"<?php endif; ?>>
    <div class="flexigrid">
        <div class="mDiv">
            
            <form id="searchForm" class="navbar-form form-inline" action="<?php echo url('ShopProduct/attrlist_index'); ?>" method="get" onsubmit="layer_loading('正在处理');">
                <?php echo (isset($searchform['hidden']) && ($searchform['hidden'] !== '')?$searchform['hidden']:''); ?>
                <div class="ftitle">
                    <div class="sDiv2">
                        <input type="text" size="30" name="keywords" value="<?php echo \think\Request::instance()->param('keywords'); ?>" class="qsbox" placeholder="名称搜索...">
                        <input type="hidden" name="oldinlet" value="<?php echo $oldinlet; ?>">
                        <input type="submit" class="btn" value="搜索">
                        <i class="fa fa-search"></i>
                    </div>
                </div>
            </form>
        </div>

        <div class="hDiv">
            <div class="hDivBox">
                <table cellspacing="0" cellpadding="0" style="width: 100%">
                    <thead>
                    <tr>
                        <th class="sign w40" axis="col0">
                            <div class="tc"><input type="checkbox" autocomplete="off" class="checkAll"></div>
                        </th>
                        <th class="sign w10 none" axis="col0">
                            <div class="tc"></div>
                        </th>
                        <th abbr="article_time" axis="col6" class="w200">
                            <div class="text-l10">参数名称</div>
                        </th>
                        <th abbr="article_title" axis="col3">
                            <div class="tl text-l10">备注描述</div>
                        </th>
                        <th abbr="article_time" axis="col6" class="w120">
                            <div class="tc">参数值数量</div>
                        </th>
                        <th abbr="article_time" axis="col6" class="w100">
                            <div class="tc">状态</div>
                        </th>
                        <th axis="col1" class="w120">
                            <div class="tc">操作</div>
                        </th>
                        <th abbr="article_time" axis="col6" class="w60">
                            <div class="tc">排序</div>
                        </th>
                       
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="bDiv" style="height: auto;">
            <form id="PostForm">
                <div id="flexigrid" cellpadding="0" cellspacing="0" border="0">
                    <table style="width: 100%;">
                        <tbody>
                        <?php if(empty($list) || (($list instanceof \think\Collection || $list instanceof \think\Paginator ) && $list->isEmpty())): ?>
                            <tr>
                                <td class="no-data" align="center" axis="col0" colspan="50">
                                    <i class="fa fa-exclamation-circle"></i>没有符合条件的记录
                                </td>
                            </tr>
                        <?php else: if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $k = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($k % 2 );++$k;?>
                            <tr class="tr">
                                <td class="sign">
                                    <div class="tc w40"><input type="checkbox" autocomplete="off" name="ids[]" value="<?php echo $vo['list_id']; ?>"></div>
                                </td>
                                <td class="sign none">
                                    <div class="tc w10"><input type="hidden" name="list_id[]" value="<?php echo $vo['list_id']; ?>"></div>
                                </td>

                                <td>
                                    <div class="w200 ml10">
                                        <input type="text" name="list_name[]" value="<?php echo $vo['list_name']; ?>">
                                    </div>
                                </td>

                                <td align="left" style="width: 100%">
                                    <div class="tl" >
                                        <input type="text" name="desc[]" value="<?php echo $vo['desc']; ?>" style="width: 95%">
                                    </div>
                                </td>

                                <td>
                                    <div class="w120 tc" style="white-space: nowrap;">
                                        <?php echo $vo['attr_count']; ?>
                                    </div>
                                </td>

                                <td>
                                    <div class="w100 tc">
                                        <?php if($vo['status'] == 1): ?>
                                            <span class="yes" onclick="ProductStatus('shop_product_attrlist', 'list_id', '<?php echo $vo['list_id']; ?>', 'status', this);">
                                                <i class="fa fa-check-circle"></i>正常
                                            </span>
                                        <?php else: ?>
                                            <span class="no" onclick="ProductStatus('shop_product_attrlist', 'list_id', '<?php echo $vo['list_id']; ?>', 'status', this);">
                                                <i class="fa fa-ban"></i>停用
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </td>

                                <td class="operation">
                                    <div class="w120 tc">
                                        <?php if(is_check_access(CONTROLLER_NAME.'@attrlist_edit') == '1'): ?>
                                             <a href="javascript:void(0);" data-url="<?php echo url('ShopProduct/attrlist_edit',array('list_id'=>$vo['list_id'])); ?>" class="btn blue" onclick="AttrBbuteEdit(this)">编辑</a>
                                        <?php endif; if(is_check_access(CONTROLLER_NAME.'@attrlist_del') == '1'): ?>
                                            <i></i>
                                            <a class="btn red"  href="javascript:void(0);" data-url="<?php echo url('ShopProduct/attrlist_del'); ?>" data-id="<?php echo $vo['list_id']; ?>" onclick="DelFind(this);">删除</a>
                                        <?php endif; ?>
                                    </div>
                                </td>

                                <td class="sort">
                                    <div class="w60 tc">
                                        <?php if(is_check_access(CONTROLLER_NAME.'@attrlist_edit') == '1'): ?>
                                        <input style="text-align: left;" name="sort_order[]" type="text" onchange="changeTableVal('shop_product_attrlist','list_id','<?php echo $vo['list_id']; ?>','sort_order',this);"  size="4"  value="<?php echo $vo['sort_order']; ?>" />
                                        <?php else: ?>
                                            <?php echo $vo['sort_order']; endif; ?>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; endif; else: echo "" ;endif; endif; ?>
                        </tbody>
                    </table>
                    <div id='Template'></div>
                </div>
            </form>
            <div class="iDiv" style="display: none;"></div>
        </div>
        <div class="tDiv">
            <div class="tDiv2">
                <div class="fbutton checkboxall">
                    <input type="checkbox" autocomplete="off" class="checkAll">
                </div>

                <?php if(is_check_access(CONTROLLER_NAME.'@attrlist_del') == '1'): ?>
                    <div class="fbutton">
                        <a onclick="DelBatch(this, 'ids');" data-url="<?php echo url('ShopProduct/attrlist_del'); ?>" class="layui-btn layui-btn-primary">
                            <span>批量删除</span>
                        </a>
                    </div>
                <?php endif; if(is_check_access(CONTROLLER_NAME.'@attrlist_add') == '1'): ?>
                    <div class="fbutton">
                        <a href="javascript:void(0);" onclick="AttrBbuteAdd(this);" data-url="<?php echo url('ShopProduct/attrlist_add'); ?>" class="layui-btn layui-btn-primary">
                            <span class="red">新增参数</span>
                        </a>
                    </div>

                    <div class="fbutton">
                        <a href="javascript:void(0);" data-url="<?php echo url('ShopProduct/attrlist_save', ['oldinlet'=>$oldinlet, '_ajax'=>1]); ?>" onclick="AddAttrListData(this);" class="layui-btn layui-btn-primary">
                            <span class="">保存</span>
                        </a>
                    </div>
                <?php endif; ?>
                <a href="JavaScript:void(0);" onclick="click_to_eyou_1575506523('https://www.eyoucms.com/plus/view.php?aid=10108','attribute 商品参数列表标签调用')" style="font-size: 12px;padding-left: 10px;position:absolute;top: 18px">标签教程？</a>
                <div class="fbuttonr">
    <div class="pages">
       <?php echo $page; ?>
    </div>
</div>
<div class="fbuttonr">
    <div class="total">
        <h5>共有<?php echo $pager->totalRows; ?>条,每页显示
            <select name="pagesize" style="width: 60px;" onchange="ey_selectPagesize(this);">
                <option <?php if($pager->listRows == 20): ?> selected <?php endif; ?> value="20">20</option>
                <option <?php if($pager->listRows == 50): ?> selected <?php endif; ?> value="50">50</option>
                <option <?php if($pager->listRows == 100): ?> selected <?php endif; ?> value="100">100</option>
                <option <?php if($pager->listRows == 200): ?> selected <?php endif; ?> value="200">200</option>
            </select>
        </h5>
    </div>
</div>
            </div>
            <div style="clear:both"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('input[name*=ids]').click(function(){
            if ($('input[name*=ids]').length == $('input[name*=ids]:checked').length) {
                $('.checkAll').prop('checked','checked');
            } else {
                $('.checkAll').prop('checked', false);
            }
        });
        $('input[type=checkbox].checkAll').click(function(){
            $('input[type=checkbox]').prop('checked',this.checked);
        });
    });

    $(document).ready(function(){
        // 表格行点击选中切换
        $('#flexigrid > table>tbody >tr').click(function(){
            $(this).toggleClass('trSelected');
        });
        // 点击刷新数据
        $('.fa-refresh').click(function(){
            location.href = location.href;
        });
        $('#searchForm select[name=typeid]').change(function(){
            $('#searchForm').submit();
        });
    });

    // 提交
    function AddAttrListData(obj){
        layer_loading('正在处理');
        $.ajax({
            type : 'post',
            url : $(obj).attr('data-url'),
            data : $('#PostForm').serialize(),
            dataType : 'json',
            success : function(res){
                layer.closeAll();
                if(res.code == 1){
                    layer.msg(res.msg, {icon: 1, time:1000},function(){
                        window.location.reload();
                    });
                }else{
                    showErrorAlert(res.msg);
                }
            }
        })
    }

    // 生成html
    function AddAttrList(){
        // tr数,取唯一标识
        var SerialNum = $('.tr').length;
        var AddHtml = [];

        AddHtml += 
        [
            '<tr class="tr" id="tr_'+SerialNum+'">'+
                '<td class="sign">'+
                    '<div class="tc w40"></div>'+
                '</td>'+

                '<td class="sign none">'+
                    '<div class="tc w10"><input type="hidden" name="list_id[]"></div>'+
                '</td>'+

                '<td>'+
                    '<div class="w200">'+
                        '<input type="text" name="list_name[]">'+
                    '</div>'+
                '</td>'+

                '<td align="left" style="width: 100%">'+
                    '<div class="tl">'+
                        '<input type="text" name="desc[]" style="width: 95%">'+
                    '</div>'+
                '</td>'+

                '<td>'+
                    '<div class="w120 tc" style="white-space: nowrap;">0</div>'+
                '</td>'+

                '<td>'+
                    '<div class="w120 tc" style="white-space: nowrap;"><span class="putaway">正常</span></div>'+
                '</td>'+

                '<td class="operation">'+
                    '<div class="w180 tc">'+
                        '<a class="btn" title="添加参数名称保存后可编辑">参数值管理</a>'+
                        '<i></i>'+
                        '<a class="btn red" href="javascript:void(0);" data-id="tr_'+SerialNum+'" onclick="DelHtml(this)">删除</a>'+
                    '</div>'+
                '</td>'+

                '<td class="sort">'+
                    '<div class="w60 tc">'+
                        '<input type="text" name="sort_order[]" value="100">'+
                    '</div>'+
                '</td>'+
            '</tr>'
        ];
        $('.no-data').hide();
        $('#Template').append(AddHtml);
    }

    // 删除未保存的级别
    function DelHtml(obj){
        $('#'+$(obj).attr('data-id')).remove();
    }

    // 删除
    function DelFind(obj){
        layer.confirm('此操作不可恢复，确认彻底删除？', {
            title: false,
            closeBtn: 0,
            btn: ['确定','取消']
        }, function(){
            layer_loading('正在处理');
            // 确定
            $.ajax({
                type : 'post',
                url : $(obj).attr('data-url'),
                data : {del_id:$(obj).attr('data-id'), _ajax:1},
                dataType : 'json',
                success : function(res){
                    layer.closeAll();
                    if(res.code == 1){
                        layer.msg(res.msg, {icon: 1, time: 1000}, function(){
                            window.location.reload();
                        });
                    }else{
                        showErrorAlert(res.msg);
                    }
                },
                error:function(e){
                    layer.closeAll();
                    showErrorAlert(e.responseText);
                }
            })
        }, function(index){
            layer.close(index);
        });
        return false;
    }

    /**
     * 批量删除提交
     */
    function DelBatch(obj, name) {
        var a = [];
        $('input[name^='+name+']').each(function(i,o){
            if($(o).is(':checked')){
                a.push($(o).val());
            }
        })
        if(a.length == 0){
            showErrorAlert('请至少选择一项');
            return;
        }
        // 删除按钮
        layer.confirm('此操作不可恢复，确认批量彻底删除？', {
            title: false,
            closeBtn: 0,
            btn: ['确定', '取消']
        }, function () {
            layer_loading('正在处理');
            $.ajax({
                type: "POST",
                url: $(obj).attr('data-url'),
                data: {del_id:a, _ajax:1},
                dataType: 'json',
                success: function (res) {
                    layer.closeAll();
                    if(res.code == 1){
                        layer.msg(res.msg, {icon: 1, time: 1000}, function(){
                            window.location.reload();
                        });
                    }else{
                        showErrorAlert(res.msg);
                    }
                },
                error:function(e){
                    layer.closeAll();
                    showErrorAlert(e.responseText);
                }
            });
        }, function (index) {
            layer.closeAll(index);
        });
    }
    // 添加参数值
    function AttrBbuteAdd(obj) {
        var url = $(obj).data('url');
        layer.open({
            type: 2,
            title: '新增参数',
            area: ['80%', '90%'],
            content: url
        });
    }
    // 添加参数值
    function AttrBbuteEdit(obj) {
        var url = $(obj).data('url');
        console.log(url)
        layer.open({
            type: 2,
            title: '编辑参数',
            area: ['80%', '90%'],
            content: url
        });
    }
</script>

<br/>
<div id="goTop">
    <a href="JavaScript:void(0);" id="btntop">
        <i class="fa fa-angle-up"></i>
    </a>
    <a href="JavaScript:void(0);" id="btnbottom">
        <i class="fa fa-angle-down"></i>
    </a>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#think_page_trace_open').css('z-index', 99999);
    });
</script>
</body>
</html>