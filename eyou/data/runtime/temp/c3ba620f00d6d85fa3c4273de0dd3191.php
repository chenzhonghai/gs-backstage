<?php if (!defined('THINK_PATH')) exit(); /*a:6:{s:37:"./template/pc/users/users_welcome.htm";i:1622776750;s:51:"/mnt/api/api/template/pc/users/skin/css/diy_css.htm";i:1622085090;s:47:"/mnt/api/api/template/pc/users/users_header.htm";i:1622085092;s:45:"/mnt/api/api/template/pc/users/users_left.htm";i:1622085092;s:52:"./public/static/template/users_v2/users_leftmenu.htm";i:1616142504;s:47:"/mnt/api/api/template/pc/users/users_footer.htm";i:1622085092;}*/ ?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8" />
    <title>会员中心-<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_name"); echo $__VALUE__; ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <link href="<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_cmspath"); echo $__VALUE__; ?>/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/css/basic.css","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/css/eyoucms.css","","",""); echo $__VALUE__; ?>
    <!-- 新样式 2020-11-25 -->
    <?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/css/element/index.css","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/css/e-user.css","","",""); echo $__VALUE__; ?>
    
<!-- 官方内置样式表，升级会覆盖变动，请勿修改，否则后果自负 -->

<style>
/*
<?php echo $theme_color; ?>
<?php echo hex2rgba($theme_color,0.8); ?> */
    .ey-container .ey-nav ul .active a {
        border-left: <?php echo $theme_color; ?> 3px solid;
        background-color:<?php echo hex2rgba($theme_color,0.1); ?>;
        color:<?php echo $theme_color; ?>;
    }
    .ey-container .ey-nav ul li a:hover {
        color: <?php echo $theme_color; ?>;
    }
    .ey-container .user-box .user-box-text{
        background-color:<?php echo $theme_color; ?>;
    }
    .ey-container .user-box-r .user-box-top .user-top-r .more {
        color:<?php echo $theme_color; ?>;
    }
    .ey-container .user-box-r .user-box-bottom .data-info .link a {
        color: <?php echo $theme_color; ?>;
    }
    .ey-container .column-title .column-name {
        color: <?php echo $theme_color; ?>;
    }
    a:hover {
        color: <?php echo $theme_color; ?>;
    }
    .el-button:focus, .el-button:hover {
        color: <?php echo $theme_color; ?>;
        border-color: <?php echo hex2rgba($theme_color,0.2); ?>;
        background-color:<?php echo hex2rgba($theme_color,0.1); ?>;
    }
    .el-button--primary {
        background-color: <?php echo $theme_color; ?>;
        border-color: <?php echo $theme_color; ?>;
    }
    .ey-container .item-from-row .err a {
        color: <?php echo $theme_color; ?>;
    }
    .ey-container .checkbox-label .checkbox:checked+.check-mark {
        background-color: <?php echo $theme_color; ?>;
        border: 1px solid <?php echo $theme_color; ?>;
    }
    .ey-container .checkbox-label .checkbox:checked+.check-mark:after {
        background:<?php echo $theme_color; ?>;
    }
    .ey-container .radio-label .radio:checked+.check-mark {
        border: 1px solid <?php echo $theme_color; ?>;
        background-color: <?php echo $theme_color; ?>;
    }
    .ey-container .radio-label .radio:checked+.check-mark:after {
        background: <?php echo $theme_color; ?>;
    }
    .el-button--primary.is-plain {
        color: <?php echo $theme_color; ?>;
        border-color: <?php echo hex2rgba($theme_color,0.2); ?>;
        background-color:<?php echo hex2rgba($theme_color,0.1); ?>;
    }
    .el-button--primary.is-plain:focus, .el-button--primary.is-plain:hover {
        background: <?php echo $theme_color; ?>;
        border-color: <?php echo $theme_color; ?>;
    }
    .el-button--primary:hover, .el-button--primary:focus {
        background: <?php echo hex2rgba($theme_color,0.9); ?>;
        border-color: <?php echo hex2rgba($theme_color,0.9); ?>;
        color: #fff;
    }
    .el-input-number__decrease:hover, .el-input-number__increase:hover {
        color: <?php echo $theme_color; ?>;
    }
    .el-input-number__decrease:hover:not(.is-disabled)~.el-input .el-input__inner:not(.is-disabled), .el-input-number__increase:hover:not(.is-disabled)~.el-input .el-input__inner:not(.is-disabled) {
        border-color: <?php echo $theme_color; ?>;
    }
    .ey-container .address-con .address-item.cur {
        border: 1px solid <?php echo hex2rgba($theme_color,0.6); ?>;
    }
    .ey-container .address-con .address-item.cur:before {
        color: <?php echo $theme_color; ?>;
        border-top: 50px solid <?php echo $theme_color; ?>;
    }
    .ey-container .ey-con .shop-oper .shop-oper-l .el-button.active {
        color: #FFF;
        background-color: <?php echo $theme_color; ?>;
        border-color: <?php echo $theme_color; ?>;
    }
    .ey-container .goods-con .goods-item .goods-item-r .view a {
        color: <?php echo $theme_color; ?>;
    }
    .ey-container div.dataTables_paginate .paginate_button.active>a, .ey-containerdiv.dataTables_paginate .paginate_button.active>a:focus, .ey-containerdiv.dataTables_paginate .paginate_button.active>a:hover {
        background: <?php echo $theme_color; ?> !important;
        border-color: <?php echo $theme_color; ?> !important;
    }
    .ey-container .pagination>li>a:focus, .ey-container .pagination>li>a:hover, .ey-container .pagination>li>span:focus, .ey-container .pagination>li>span:hover {
        color:<?php echo $theme_color; ?>;
    }
    .ey-container .pagination>li>a, .ey-container .pagination>li>span {
        color:<?php echo $theme_color; ?>;
    }
    .el-button--danger {
        background-color: <?php echo $theme_color; ?>;
        border-color: <?php echo $theme_color; ?>;
    }
    .recharge .pc-vip-list.active {
        border: <?php echo $theme_color; ?> 2px solid;
        /*background-color:<?php echo hex2rgba($theme_color,0.1); ?>;*/
    }
    .recharge .pc-vip-list:hover {
    	border: <?php echo $theme_color; ?> 2px solid;
    }
    .recharge .pc-vip-list .icon-recomd {
    	background: <?php echo $theme_color; ?>;
    }
    .el-input.is-active .el-input__inner, .el-input__inner:focus {
        border-color: <?php echo $theme_color; ?>;
    }
    .ey-container .address-con .selected .address-item:before {
        color:<?php echo $theme_color; ?>;
        border-top: 50px solid <?php echo $theme_color; ?>;
    }
    .ey-container .address-con .selected .address-item {
        border: 1px solid <?php echo hex2rgba($theme_color,0.4); ?>;
    }
    .ey-container .pay-type .payTag a {
        color: <?php echo $theme_color; ?>;
        border-bottom: 1px solid <?php echo $theme_color; ?>;
    }
    .ey-container .pay-type .pay-con .pay-type-item.active {
        border-color: <?php echo hex2rgba($theme_color,0.4); ?>;
    }
    .ey-container .pay-type .pay-con .pay-type-item.active:before {
        color: <?php echo $theme_color; ?>;
        border-bottom: 30px solid <?php echo $theme_color; ?>;
    }
    .ey-header-nav .nav li ul li a:hover {
        background: <?php echo $theme_color; ?>;
    }
    .btn-primary {
        background-color: <?php echo $theme_color; ?>;
    }
    .btn-primary.focus, .btn-primary:focus, .btn-primary:hover {
        border-color: <?php echo $theme_color; ?>;
        background-color: <?php echo $theme_color; ?>;
    }
    .btn-primary.active.focus, .btn-primary.active:focus, .btn-primary.active:hover, .btn-primary:active.focus, .btn-primary:active:focus, .btn-primary:active:hover, .open>.btn-primary.dropdown-toggle.focus, .open>.btn-primary.dropdown-toggle:focus, .open>.btn-primary.dropdown-toggle:hover {
    	border-color:<?php echo $theme_color; ?>;
    	background-color:<?php echo $theme_color; ?>;
    }
    .login-link a {
        color: <?php echo $theme_color; ?>;
    }
    .register_index .login_link .login_link_register {
        color: <?php echo $theme_color; ?>;
    }

</style>

<script type="text/javascript">
    var __root_dir__ = "/api";
    var __version__ = "<?php echo $version; ?>";
    var __lang__ = "<?php echo $home_lang; ?>";
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
</script>

    <?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/static/common/js/jquery.min.js","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/js/bootstrap.min.js","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/plugins/layer-v3.1.0/layer.js","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/js/global.js","","",""); echo $__VALUE__; ?>
</head>

<body class="centre">
<!-- 头部 -->
<!-- 头部信息 -->
<div class="ey-header">
    <div class="ey-logo">
        <a href="<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_cmsurl"); echo $__VALUE__; ?>"><img src="<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_logo"); echo $__VALUE__; ?>" /></a>
    </div>
    <div class="ey-header-nav w1200">
        <ul class="nav nav-menu nav-inline">
            <li><a href="<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_cmsurl"); echo $__VALUE__; ?>"> 首页 </a></li>
            <?php  if(isset($ui_typeid) && !empty($ui_typeid)) : $typeid = $ui_typeid; else: $typeid = ""; endif; if(empty($typeid) && isset($channelartlist["id"]) && !empty($channelartlist["id"])) : $typeid = intval($channelartlist["id"]); endif;  if(isset($ui_row) && !empty($ui_row)) : $row = $ui_row; else: $row = 60; endif; $tagChannel = new \think\template\taglib\eyou\TagChannel; $_result = $tagChannel->getChannel($typeid, "top", "", ""); if(is_array($_result) || $_result instanceof \think\Collection || $_result instanceof \think\Paginator): $i = 0; $e = 1;$__LIST__ = is_array($_result) ? array_slice($_result,0, $row, true) : $_result->slice(0, $row, true); if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$field): $field["typename"] = text_msubstr($field["typename"], 0, 100, false); $__LIST__[$key] = $_result[$key] = $field;$i= intval($key) + 1;$mod = ($i % 2 ); ?>
            <li>
                <a href="<?php echo $field['typeurl']; ?>"> <?php echo $field['typename']; if(!(empty($field['children']) || (($field['children'] instanceof \think\Collection || $field['children'] instanceof \think\Paginator ) && $field['children']->isEmpty()))): ?><i class="fa fa-angle-down margin-small-left"></i><?php endif; ?></a>
                <?php if(!(empty($field['children']) || (($field['children'] instanceof \think\Collection || $field['children'] instanceof \think\Paginator ) && $field['children']->isEmpty()))): ?>
                <ul class="drop-menu">
                    <?php  if(isset($ui_typeid) && !empty($ui_typeid)) : $typeid = $ui_typeid; else: $typeid = ""; endif; if(empty($typeid) && isset($channelartlist["id"]) && !empty($channelartlist["id"])) : $typeid = intval($channelartlist["id"]); endif;  if(isset($ui_row) && !empty($ui_row)) : $row = $ui_row; else: $row = 100; endif;if(is_array($field['children']) || $field['children'] instanceof \think\Collection || $field['children'] instanceof \think\Paginator): $i = 0; $e = 1;$__LIST__ = is_array($field['children']) ? array_slice($field['children'],0,100, true) : $field['children']->slice(0,100, true); if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$field2): $field2["typename"] = text_msubstr($field2["typename"], 0, 100, false); $__LIST__[$key] = $_result[$key] = $field2;$i= intval($key) + 1;$mod = ($i % 2 ); ?>
                    <li><a href="<?php echo $field2['typeurl']; ?>"><?php echo $field2['typename']; ?></a></li>
                    <?php ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $field2 = []; ?>
                </ul>
                <?php endif; ?>
            
            </li>
            <?php ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $field = []; ?>
         </ul>
    </div>
    <div class="ey-header-r">
        <?php if($php_servicemeal >= '1'): ?>
        <div class="right-item user-news">
            <a href="<?php echo url('user/UsersNotice/index'); ?>">
                <?php if($unread_num > '0'): ?>
                <span class="num" id="users_unread_num"><?php echo $unread_num; ?></span>
                <?php endif; ?>
                <span class="icon"><i class="el-icon-bell"></i></span>
            </a>
        </div>
        <?php endif; ?>
        <div class="right-item user-photo">
            <img src="<?php echo get_head_pic($users['head_pic']); ?>"/>
            <div class="user-drop">
                <ul>
                    <li><a href="<?php  $tagDiyurl = new \think\template\taglib\eyou\TagDiyurl; $__VALUE__ = $tagDiyurl->getDiyurl("", "user/Users/index", "", "", "", "", "", "", "ey_active"); echo $__VALUE__; ?>">个人中心</a></li>
                    <?php if(getUsersConfigData('shop.shop_open') == 1): ?>
                    <li><a href="<?php  $tagDiyurl = new \think\template\taglib\eyou\TagDiyurl; $__VALUE__ = $tagDiyurl->getDiyurl("", "user/Shop/shop_centre", "", "", "", "", "", "", "ey_active"); echo $__VALUE__; ?>">我的订单</a></li>
                    <?php endif; ?>
                    <li><a href="<?php  $tagDiyurl = new \think\template\taglib\eyou\TagDiyurl; $__VALUE__ = $tagDiyurl->getDiyurl("", "user/Users/collection_index", "", "", "", "", "", "", "ey_active"); echo $__VALUE__; ?>">我的收藏</a></li>
                    <li><a href="<?php  $tagDiyurl = new \think\template\taglib\eyou\TagDiyurl; $__VALUE__ = $tagDiyurl->getDiyurl("", "user/Users/logout", "", "", "", "", "", "", "ey_active"); echo $__VALUE__; ?>">安全退出</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- 头部信息结束 -->

<script type="text/javascript">
    //头像下拉
    $(".user-photo").mouseover(function(){
       $(".user-drop").show();
    });
    $(".user-photo").mouseout(function(){
       $(".user-drop").hide();
    });

    var GetUploadify_url = "<?php echo url('Uploadify/upload'); ?>";
</script>
<!-- 头部结束 -->

<div class="ey-body-bg">
    <div class="ey-body">
        <div class="ey-container w1200">
            <!-- 侧边 -->
            <div class="ey-nav fl">
    <div class="sidebar-box">
        <ul> 
    <li>
        <div class="title">会员中心</div>
    </li>
    <?php  $tagUsermenu = new \think\template\taglib\eyou\TagUsermenu; $_result = $tagUsermenu->getUsermenu("active", ""); if(is_array($_result) || $_result instanceof \think\Collection || $_result instanceof \think\Paginator): $i = 0; $e = 1; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$field): $i= intval($key) + 1;$mod = ($i % 2 ); ?>
    <li class="<?php echo $field['currentstyle']; ?>">
        <a href="<?php echo $field['url']; ?>"><?php echo $field['title']; ?></a>
    </li>
    <?php ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $field = []; ?>
</ul>

<?php if(isset($usersConfig['shop_open']) && $usersConfig['shop_open'] == 1 && $php_servicemeal > 1): ?>
<ul>
    <li>
        <div class="title">商城中心</div>
    </li>
    <li <?php if(MODULE_NAME == 'user' && CONTROLLER_NAME == 'Shop' && ACTION_NAME == 'shop_cart_list'): ?> class="active" <?php endif; ?>>
        <a href="<?php echo url("user/Shop/shop_cart_list","",true,false,null,null,null);?>">购物车</a>
    </li>
    <li <?php if(MODULE_NAME == 'user' && CONTROLLER_NAME == 'Shop' && ACTION_NAME == 'shop_address_list'): ?> class="active" <?php endif; ?>>
        <a href="<?php echo url("user/Shop/shop_address_list","",true,false,null,null,null);?>">收货地址</a>
    </li>
    <li <?php if(MODULE_NAME == 'user' && (CONTROLLER_NAME == 'Shop' && in_array(ACTION_NAME, ['shop_centre', 'shop_order_details', 'after_service', 'after_service_details', 'after_service_apply']) || (CONTROLLER_NAME == 'ShopComment' && in_array(ACTION_NAME, ['product'])))): ?> class="active" <?php endif; ?>>
        <a href="<?php echo url("user/Shop/shop_centre","",true,false,null,null,null);?>">我的订单</a>
    </li>
    <?php if($php_servicemeal >= '2'): ?>
    <li <?php if(MODULE_NAME == 'user' && CONTROLLER_NAME == 'ShopComment' && in_array(ACTION_NAME, ['index'])): ?> class="active" <?php endif; ?>>
        <a href="<?php echo url("user/ShopComment/index","",true,false,null,null,null);?>"> 我的评价</a>
    </li>
    <?php endif; ?>
</ul>
<?php endif; if(isset($usersConfig['users_open_release']) && $usersConfig['users_open_release'] == 1): ?>
<ul>
    <li>
        <div class="title">投稿中心</div>
    </li>
    <li <?php if(MODULE_NAME == 'user' && CONTROLLER_NAME == 'UsersRelease' && ACTION_NAME == 'release_centre'): ?> class="active" <?php endif; ?>>
        <a href="<?php echo url('user/UsersRelease/release_centre', ['list'=>1]); ?>">投稿列表</a>
    </li>
    <li <?php if(MODULE_NAME == 'user' && CONTROLLER_NAME == 'UsersRelease' && (ACTION_NAME == 'article_add' || ACTION_NAME == 'article_edit')): ?> class="active" <?php endif; ?>>
        <a href="<?php echo url("user/UsersRelease/article_add","",true,false,null,null,null);?>">我要投稿</a>
    </li>
</ul>
<?php endif; ?>
    </div>
</div>
            <!-- 侧边结束 -->
            <!-- 右侧 -->
            <div class="ey-con fr" >
                <!-- 数据统计 -->
                <div class="main-bg user-box clearfix">
                    <div class="user-box-l fl">
                        <div class="user-box-text">
                            <span class="user-photo"><img src="<?php echo get_head_pic($users['head_pic']); ?>" /></span>
                            <p class="name"><?php echo $users['nickname']; ?></p>
                            <p>你好，欢迎来到<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_name"); echo $__VALUE__; ?></p>
                        </div>
                        <div class="user-box-vip">
                            <?php if($users['level'] > '1'): ?>
                                <a class="open-vip" href="<?php echo url("user/Level/level_centre","",true,false,null,null,null);?>">升级会员</a>
                            <?php else: ?>
                                <a class="open-vip" href="<?php echo url("user/Level/level_centre","",true,false,null,null,null);?>">开通会员</a>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="user-box-r fl">
                        <div class="user-box-top fl">
                            <div class="user-top-l fl">
                                <span class="name-id"><?php echo $users['nickname']; ?> <em><?php echo $users['level_name']; ?></em><?php switch($users['level']): case "1": ?> <img src="<?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/images/person-vip1.png","","",""); echo $__VALUE__; ?>"> <?php break; case "2": ?> <img src="<?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/images/person-vip2.png","","",""); echo $__VALUE__; ?>"> <?php break; case "3": ?> <img src="<?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/images/person-vip3.png","","",""); echo $__VALUE__; ?>">  <?php break; endswitch; ?></span>
                                <span class="user-type ml10">
                                    <?php if($users['is_mobile'] == '1'): ?>
                                    <i class="el-icon-mobile-phone" title="手机认证"></i>
                                    <?php endif; if($users['is_email'] == '1'): ?>
                                    <i class="el-icon-message" title="邮箱认证"></i>
                                    <?php endif; ?>
                                </span>
                            </div>
                            <div class="user-top-r fr">
                                <a class="more" href="<?php echo url("user/Users/info","",true,false,null,null,null);?>">修改个人信息</a>
                            </div>
                        </div>
                        <div class="user-box-bottom fl">
                            <div class="data-info">
                                <span class="num"><?php echo $users['users_money']; ?></span>
                                <span class="title">余额</span>
                                <span class="link"><a href="<?php echo url("user/Pay/pay_account_recharge","",true,false,null,null,null);?>">去充值</a></span>
                            </div>
                            <div class="data-info">
                                <span class="num"><?php echo $others['collect_num']; ?></span>
                                <span class="title">收藏</span>
                                <span class="link"><a href="<?php echo url("user/Users/collection_index","",true,false,null,null,null);?>">查看</a></span>
                            </div>
                            <?php if($php_servicemeal > '1'): ?>
                            <div class="data-info">
                                <span class="num" id="users_scores"><?php echo $users['scores']; ?></span>
                                <span class="title"><?php echo $score_name; ?></span>
                                <span class="link" id="user_signin">
                                    <?php if(isset($others['signin_conf']) && isset($others['signin_conf']['score_signin_status']) && $others['signin_conf']['score_signin_status'] == 1): if(!$others['signin_info']): ?>
                                            <a href="javascript:void(0);" data-url="<?php echo url('api/Ajax/signin_save'); ?>" onclick="userSignin(this);">签到</a>
                                        <?php else: ?>
                                            已签到
                                        <?php endif; else: ?>
                                        &nbsp;
                                    <?php endif; ?>
                                </span>
                            </div>
                            <?php endif; ?>
                            <div class="data-info">
                                <span class="num"><?php echo $others['footprint_num']; ?></span>
                                <span class="title">足迹</span>
                                <span class="link"><a href="<?php echo url("user/Users/footprint_index","",true,false,null,null,null);?>">查看</a></span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 数据统计 end-->

                <?php if(isset($usersConfig['shop_open']) && $usersConfig['shop_open'] == 1 && $php_servicemeal > 1): ?>
                <!-- 订单统计 -->
                <div class=" main-bg">
                    <div class="column-title2">
                        <div class="column-name2">我的订单</div>
                    </div>
                    <div class="el-main">
                        <div class="index-pay mt20">
                            <?php  $tagSporderlist = new \think\template\taglib\eyou\TagSporderlist; $_result = $tagSporderlist->getSpstatus();if(!empty($_result) || (($_result instanceof \think\Collection || $_result instanceof \think\Paginator ) && $_result->isEmpty())): $field3 = $_result; ?>
                                <div class="pay-item">
                                    <div class="pay-item-l">全部订单</div>
                                    <div class="pay-item-r"><a href="<?php echo url("user/Shop/shop_centre","",true,false,null,null,null);?>"><?php echo $field3['All']; ?></a></div>
                                </div>
                                <div class="pay-item">
                                    <div class="pay-item-l">待付款</div>
                                    <div class="pay-item-r"><a href="<?php echo url("user/Shop/shop_centre","select_status=dzf",true,false,null,null,null);?>"><?php echo $field3['PendingPayment']; ?></a></div>
                                </div>
                                <div class="pay-item">
                                    <div class="pay-item-l">待收货</div>
                                    <div class="pay-item-r"><a href="<?php echo url("user/Shop/shop_centre","select_status=2",true,false,null,null,null);?>"><?php echo $field3['PendingReceipt']; ?></a></div>
                                </div>
                                <div class="pay-item">
                                    <div class="pay-item-l">已完成</div>
                                    <div class="pay-item-r"><a href="<?php echo url("user/Shop/shop_centre","select_status=3",true,false,null,null,null);?>"><?php echo $field3['Completed']; ?></a></div>
                                </div>
                                <?php if($php_servicemeal > '1'): ?>
                                    <div class="pay-item">
                                        <div class="pay-item-l">售后</div>
                                        <div class="pay-item-r"><a href="<?php echo url("user/Shop/after_service","",true,false,null,null,null);?>"><?php echo $field3['AfterService']; ?></a></div>
                                    </div>
                                <?php endif; if($php_servicemeal >= '2'): ?>
                                    <div class="pay-item">
                                        <div class="pay-item-l">评价</div>
                                        <div class="pay-item-r"><a href="<?php echo url("user/ShopComment/index","",true,false,null,null,null);?>"><?php echo $field3['CommentList']; ?></a></div>
                                    </div>
                                <?php endif; endif; $field3 = []; ?>
                        </div>
                    </div>
                </div>
                <!-- 订单统计 end-->
                <?php endif; ?>
                
                <!-- 其他模块入口 -->
                <div class="main-bg">
                    <div class="column-title2">
                        <div class="column-name2">其他模块</div>
                    </div>
                    <div class="el-main">
                        <div class="index-plugs">
                            <?php if(isset($usersConfig['level_member_upgrade']) && $usersConfig['level_member_upgrade'] == 1): ?>
                            <div class="plugs-item">
                                <div class="plugs-item-l"><img src="<?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/images/user-vip-100x100.png","","",""); echo $__VALUE__; ?>" /></div>
                                <?php if($users['level'] > '1'): ?>
                                <div class="plugs-item-r"><a href="<?php echo url("user/Level/level_centre","",true,false,null,null,null);?>">升级会员</a></div>
                                <?php else: ?>
                                <div class="plugs-item-r"><a href="<?php echo url("user/Level/level_centre","",true,false,null,null,null);?>">开通会员</a></div>
                                <?php endif; ?>
                                <div class="plugs-item-r"><a href="<?php echo url("user/Level/level_centre","",true,false,null,null,null);?>">开通会员</a></div>
                                
                            </div>
                            <?php endif; if(!(empty($part_channel['download']['status']) || (($part_channel['download']['status'] instanceof \think\Collection || $part_channel['download']['status'] instanceof \think\Paginator ) && $part_channel['download']['status']->isEmpty()))): ?>
                            <div class="plugs-item">
                                <div class="plugs-item-l"><img src="<?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/images/user-down-100x100.png","","",""); echo $__VALUE__; ?>" /></div>
                                <div class="plugs-item-r"><a href="<?php echo url("user/Download/index","",true,false,null,null,null);?>">我的下载</a></div>
                            </div>
                            <?php endif; if(isset($part_channel['article']['data']['is_article_pay']) && $part_channel['article']['data']['is_article_pay'] == 1): ?>
                            <div class="plugs-item">
                                <div class="plugs-item-l"><img src="<?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/images/user-video-100x100.png","","",""); echo $__VALUE__; ?>" /></div>
                                <div class="plugs-item-r"><a href="<?php echo url("user/Users/article_index","",true,false,null,null,null);?>">我的文章</a></div>
                            </div>
                            <?php endif; if($php_servicemeal > '1'): if(!(empty($part_channel['media']['status']) || (($part_channel['media']['status'] instanceof \think\Collection || $part_channel['media']['status'] instanceof \think\Paginator ) && $part_channel['media']['status']->isEmpty()))): ?>
                                <div class="plugs-item">
                                    <div class="plugs-item-l"><img src="<?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/images/user-video-100x100.png","","",""); echo $__VALUE__; ?>" /></div>
                                    <div class="plugs-item-r"><a href="<?php echo url("user/Users/media_index","",true,false,null,null,null);?>">我的视频</a></div>
                                </div>
                                <?php endif; endif; if($php_servicemeal >= '2'): if(!(empty($part_channel['ask']['status']) || (($part_channel['ask']['status'] instanceof \think\Collection || $part_channel['ask']['status'] instanceof \think\Paginator ) && $part_channel['ask']['status']->isEmpty()))): ?>
                                <div class="plugs-item">
                                    <div class="plugs-item-l"><img src="<?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/images/user-video-100x100.png","","",""); echo $__VALUE__; ?>" /></div>
                                    <div class="plugs-item-r"><a href="<?php echo url("user/Ask/ask_index","",true,false,null,null,null);?>">我的问答</a></div>
                                </div>
                                <?php endif; endif; if(!(empty($others['weapp_menu_info']) || (($others['weapp_menu_info'] instanceof \think\Collection || $others['weapp_menu_info'] instanceof \think\Paginator ) && $others['weapp_menu_info']->isEmpty()))): if(is_array($others['weapp_menu_info']) || $others['weapp_menu_info'] instanceof \think\Collection || $others['weapp_menu_info'] instanceof \think\Paginator): $i = 0; $__LIST__ = $others['weapp_menu_info'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                                    <div class="plugs-item">
                                        <div class="plugs-item-l"><img src="<?php echo ROOT_DIR; ?><?php echo $vo['litpic']; ?>" /></div>
                                        <div class="plugs-item-r"><a href="<?php echo url($vo['mca']); ?>"><?php echo $vo['title']; ?></a></div>
                                    </div>
                                <?php endforeach; endif; else: echo "" ;endif; endif; ?>
                        </div>
                    </div>
                </div>  
                <!-- 其他模块入口 end-->

            </div>
            <!-- 右侧结束 -->
        </div>
    </div>
</div>

<script type="text/javascript">
    // 签到
    function userSignin(obj) {
        layer_loading('正在处理');
        $.ajax({
            type: "POST",
            url: $(obj).attr('data-url'),
            data: {_ajax:1},
            dataType: 'json',
            success: function (res) {
                layer.closeAll();
                if(res.code == 1){
                    layer.msg(res.msg, {icon: 6, time:1500});
                    $("#user_signin").html("已签到");
                    $("#users_scores").html(res.data.scores);
                }else{
                    showErrorAlert(res.msg);
                }
            },
            error:function(e){
                layer.closeAll();
                showErrorAlert(e.responseText);
            }
        });
    }
</script>

<!-- 底部 -->
	<div class="ey-footer">
    <footer class="container">
        <p><?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_copyright"); echo $__VALUE__; ?></p>
    </footer>
</body>
</html>