<?php if (!defined('THINK_PATH')) exit(); /*a:5:{s:48:"./application/admin/template/shop/spec_index.htm";i:1640913888;s:57:"/mnt/api/api/application/admin/template/public/layout.htm";i:1640913882;s:60:"/mnt/api/api/application/admin/template/public/theme_css.htm";i:1640913882;s:53:"/mnt/api/api/application/admin/template/shop/left.htm";i:1640913887;s:57:"/mnt/api/api/application/admin/template/public/footer.htm";i:1640913882;}*/ ?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link href="/api/public/plugins/layui/css/layui.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/main.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/page.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/font/css/font-awesome.min.css" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="/api/public/static/admin/font/css/font-awesome-ie7.min.css">
<![endif]-->
<script type="text/javascript">
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
    var module_name = "<?php echo MODULE_NAME; ?>";
    var GetUploadify_url = "<?php echo url('Uploadify/upload'); ?>";
    var __root_dir__ = "/api";
    var __lang__ = "<?php echo $admin_lang; ?>";
</script>  
<link href="/api/public/static/admin/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="/api/public/static/admin/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
<!-- <link type="text/css" rel="stylesheet" href="/api/public/plugins/tags_input/css/jquery.tagsinput.css?v=<?php echo $version; ?>"> -->
<style type="text/css">html, body { overflow: visible;}</style>
<link href="/api/public/static/admin/css/diy_style.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css" />

<!-- 官方内置样式表，升级会覆盖变动，请勿修改，否则后果自负 -->

<style type="text/css">
	/*左侧收缩图标*/
	#foldSidebar i { font-size: 24px;color:<?php echo $global['web_theme_color']; ?>; }
    /*左侧菜单*/
    .eycms_cont_left{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl dd a:hover,.eycms_cont_left dl dd a.on,.eycms_cont_left dl dt.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl dd a:active{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist dd{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl.jslist dd a:hover,.eycms_cont_left dl.jslist dd a.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist:hover{ background:<?php echo $global['web_assist_color']; ?>; }
    /*栏目操作*/
    .cate-dropup .cate-dropup-con a:hover{ background-color: <?php echo $global['web_theme_color']; ?>; }
    /*按钮*/
    a.ncap-btn-green { background-color: <?php echo $global['web_theme_color']; ?>; }
    a:hover.ncap-btn-green { background-color: <?php echo $global['web_assist_color']; ?>; }
    .flexigrid .sDiv2 .btn:hover { background-color: <?php echo $global['web_theme_color']; ?>; }
    .flexigrid .mDiv .fbutton div.add{background-color: <?php echo $global['web_theme_color']; ?>; border: none;}
    .flexigrid .mDiv .fbutton div.add:hover{ background-color: <?php echo $global['web_assist_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds{color:<?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds:hover{ background-color: <?php echo $global['web_theme_color']; ?>;}
    /*选项卡字体*/
    .tab-base a.current,
    .tab-base a:hover.current { border-bottom: solid 2px <?php echo $global['web_theme_color']; ?> !important;color: <?php echo $global['web_theme_color']; ?> !important;}
    .addartbtn input.btn:hover{ border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>; }
    .addartbtn input.btn.selected{ color: <?php echo $global['web_theme_color']; ?>;border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>;}
	/*会员导航*/
	.member-nav-group .member-nav-item .btn.selected{background: <?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;box-shadow: -1px 0 0 0 <?php echo $global['web_theme_color']; ?>;}
	.member-nav-group .member-nav-item:first-child .btn.selected{border-left: 1px solid <?php echo $global['web_theme_color']; ?> !important;}
	/*搜索按钮图标*/
	.flexigrid .sDiv2 .fa-search{}
        
    /* 商品订单列表标题 */
   .flexigrid .mDiv .ftitle h3 {border-left: 3px solid <?php echo $global['web_theme_color']; ?>;} 
	
    /*分页*/
    .pagination > .active > a, .pagination > .active > a:focus, 
	.pagination > .active > a:hover, 
	.pagination > .active > span, 
	.pagination > .active > span:focus, 
	.pagination > .active > span:hover { border-color: <?php echo $global['web_theme_color']; ?>;color:<?php echo $global['web_theme_color']; ?>; }
    
    .layui-form-onswitch {border-color: <?php echo $global['web_theme_color']; ?> !important;background-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-enable.selected { background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-disable.selected {background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    input[type="text"]:focus,
    input[type="text"]:hover,
    input[type="text"]:active,
    input[type="password"]:focus,
    input[type="password"]:hover,
    input[type="password"]:active,
    textarea:hover,
    textarea:focus,
    textarea:active { border-color:<?php echo hex2rgba($global['web_theme_color'],0.8); ?>;box-shadow: 0 0 0 2px <?php echo hex2rgba($global['web_theme_color'],0.15); ?> !important;}
    .input-file-show:hover .type-file-button {background-color:<?php echo $global['web_theme_color']; ?> !important; }
    .input-file-show:hover {border-color: <?php echo $global['web_theme_color']; ?> !important;box-shadow: 0 0 5px <?php echo hex2rgba($global['web_theme_color'],0.5); ?> !important;}
    .input-file-show:hover span.show a,
    .input-file-show span.show a:hover { color: <?php echo $global['web_theme_color']; ?> !important;}
    .input-file-show:hover .type-file-button {background-color: <?php echo $global['web_theme_color']; ?> !important; }
    .color_z { color: <?php echo $global['web_theme_color']; ?> !important;}
    a.imgupload{
        background-color: <?php echo $global['web_theme_color']; ?> !important;
        border-color: <?php echo $global['web_theme_color']; ?> !important;
    }
    /*专题节点按钮*/
    .ncap-form-default .special-add{background-color:<?php echo $global['web_theme_color']; ?>;border-color:<?php echo $global['web_theme_color']; ?>;}
    .ncap-form-default .special-add:hover{background-color:<?php echo $global['web_assist_color']; ?>;border-color:<?php echo $global['web_assist_color']; ?>;}
    
    /*更多功能标题*/
    .on-off_panel .title::before {background-color:<?php echo $global['web_theme_color']; ?>;}

</style>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.js"></script>
<!-- <script type="text/javascript" src="/api/public/plugins/tags_input/js/jquery.tagsinput.js?v=<?php echo $version; ?>"></script> -->
<script type="text/javascript" src="/api/public/static/admin/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/api/public/plugins/layer-v3.1.0/layer.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/admin.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/common.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/api/public/plugins/layui/layui.js"></script>
<script src="/api/public/static/admin/js/myFormValidate.js"></script>
<script src="/api/public/static/admin/js/myAjax2.js?v=<?php echo $version; ?>"></script>
<script src="/api/public/static/admin/js/global.js?v=<?php echo $version; ?>"></script>
</head>

<body class="bodystyle" style="overflow-y: scroll; cursor: default; -moz-user-select: inherit;min-width:auto;">
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="sidebar-second ">
    <ul>
        <li class="sidebar-second-title">商城中心</li>
        <li>
            <a <?php if('Statistics' == CONTROLLER_NAME): ?>class="active"<?php endif; ?> href='<?php echo url("Statistics/index"); ?>'>数据统计</a>
        </li>
        <li>
            <a <?php if(('ShopService' == CONTROLLER_NAME) OR ('Shop' == CONTROLLER_NAME and in_array(ACTION_NAME, ['index', 'order_details']))): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/index"); ?>'>所有订单</a>
        </li>
        <li>
            <a <?php if('ShopProduct' == CONTROLLER_NAME and in_array(ACTION_NAME, ['index', 'add', 'edit'])): ?>class="active"<?php endif; ?> href='<?php echo url("ShopProduct/index"); ?>'>商品管理</a>
        </li>
        <li>
            <a <?php if('ShopProduct' == CONTROLLER_NAME and in_array(ACTION_NAME, ['attrlist_index', 'attribute_index'])): ?>class="active"<?php endif; ?> href='<?php echo url("ShopProduct/attrlist_index"); ?>'>商品参数</a>
        </li>
        <?php 
            $shop_open_spec = getUsersConfigData('shop.shop_open_spec');
         if($shop_open_spec == '1'): ?>
        <li>
            <a <?php if('Shop' == CONTROLLER_NAME and in_array(ACTION_NAME, ['spec_index'])): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/spec_index"); ?>'>商品规格</a>
        </li>
        <?php endif; ?>
        <li>
            <a <?php if('Shop' == CONTROLLER_NAME and 'conf' == ACTION_NAME): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/conf"); ?>'>商城配置</a>
        </li>
        <?php if($php_servicemeal >= 2): ?>
        <li>
            <a <?php if('ShopComment' == CONTROLLER_NAME): ?>class="active"<?php endif; ?> href='<?php echo url("ShopComment/comment_index"); ?>'>商品评价</a>
        </li>
        <li>
            <a <?php if((in_array(CONTROLLER_NAME, ['Sharp','Coupon'])) OR ('Shop' == CONTROLLER_NAME and in_array(ACTION_NAME, ['market_index']))): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/market_index"); ?>'>营销功能</a>
        </li>
        <?php endif; ?>
    </ul>
</div>
<div class="page" style="min-width:auto;margin-left:98px;">
    <div id="explanation" class="explanation" style="color: rgb(44, 188, 163); background-color: rgb(237, 251, 248); padding-right: 0px; height: 100%;">
        <div id="checkZoom" class="title"><i class="fa fa-lightbulb-o"></i>
            <h4 title="提示相关设置操作时应注意的要点">提示</h4>
            <span title="收起提示" id="explanationZoom" style="display: block;"></span>
        </div>
        <ul>
            <li>1、新增规格模板，方便管理员快捷选择商品规格复制到商品发布页里</li>
            <li>2、在规格库里编辑，修改，删除商品规格模板并<i style="color: red;">不影响</i>已发布的商品里的规格值</li>
            <li>3、如果编辑商品规格时需要和规格库里规格模板一致，可以点击规格值右侧的刷新按钮 <i class="fa fa-refresh"></i> 同步</li>
        </ul>
    </div>

    <div class="flexigrid">
        <div class="hDiv">
            <div class="hDivBox">
                <table cellspacing="0" cellpadding="0" style="width: 100%">
                    <thead>
                    <tr>
                        <th class="sign w10" axis="col0">
                            <div class="tc"></div>
                        </th>
                        <th abbr="article_time" axis="col4" class="w180">
                            <div class="text-l10">规格名称</div>
                        </th>
                        <th abbr="ac_id" axis="col4">
                            <div class="text-l10">规格值</div>
                        </th>
                        <th abbr="article_time" axis="col6" class="w100">
                            <div class="tc">是否同步</div>
                        </th>
                        <th abbr="article_time" axis="col6" class="w60">
                            <div class="tc">排序</div>
                        </th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="bDiv" style="height: auto;">
            <form id="PostForm">
                <div id="flexigrid" cellpadding="0" cellspacing="0" border="0">
                    <table style="width: 100%">
                        <input type="hidden" name="mark_preset_ids" id='mark_preset_ids'>
                        <input type="hidden" name="mark_mark_ids" id='mark_mark_ids'>
                        <tbody id='Template'>
                            <?php if(is_array($info) || $info instanceof \think\Collection || $info instanceof \think\Paginator): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                                <tr class="tr" id="tr_<?php echo $vo['preset_mark_id']; ?>">
                                    <td class="sign">
                                        <div class="w10 tc">
                                            <input type="hidden" name="preset_old[]" value="<?php echo $vo['preset_mark_id']; ?>">
                                        </div>
                                    </td>

                                    <td class="">
                                        <div class="w180 tc preset-bt3" style="text-align: left;">
                                            <span>
                                                <input type="text" name="preset_name_old_<?php echo $vo['preset_mark_id']; ?>[]" placeholder="请输入规格名称" onchange="MarkMarkId('<?php echo $vo['preset_mark_id']; ?>');" value="<?php echo $vo['preset_name']; ?>">
                                                <em data-id="tr_<?php echo $vo['preset_mark_id']; ?>" onclick="DelSpecTpl(this, '<?php echo $vo['preset_mark_id']; ?>')"><i style="position: absolute;right: 4px;" class="fa fa-times-circle" title="关闭"></i></em>
                                            </span>
                                        </div>
                                    </td>

                                    <td style="width: 100%">
                                        <div class="preset-bt3" style="text-align: left;" id="tr_td_input_old_<?php echo $vo['preset_mark_id']; ?>">
                                                <?php if(is_array($vo['preset_value']) || $vo['preset_value'] instanceof \think\Collection || $vo['preset_value'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vo['preset_value'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$value): $mod = ($i % 2 );++$i;?>
                                                    <span>
                                                        <input type="hidden" name="preset_id_old_<?php echo $vo['preset_mark_id']; ?>[]" value="<?php echo $value['preset_id']; ?>">
                                                        <input type="text" name="preset_value_old_<?php echo $vo['preset_mark_id']; ?>[]" placeholder="请输入规格值" onchange="MarkPresetId('<?php echo $value['preset_id']; ?>');" value="<?php echo $value['preset_value']; ?>"><em onclick="DelSpecTplValue(this, '<?php echo $value['preset_id']; ?>', '<?php echo $vo['preset_mark_id']; ?>')"><i  class="fa fa-times-circle" title="关闭"></i></em>
                                                    </span>
                                                <?php endforeach; endif; else: echo "" ;endif; ?>
                                                <span style="background:#4fc0e8; float: right; color: #fff; border-radius: 4px; padding:0px 6px  0px 20px; position: relative; cursor: pointer;" class="red" data-id="tr_td_input_old_<?php echo $vo['preset_mark_id']; ?>" onclick="AddSpecTplValue(this,'<?php echo $vo['preset_mark_id']; ?>','old');">
                                                    <i class="layui-icon layui-icon-addition" style="position:absolute; left: 0; top:0px; margin: 0 3px; color: #fff"></i>添加规格值
                                                </span>
                                        </div>
                                    </td>
                                    <td class="sort">
                                        <div class="w100 tc">
                                            <label class="label">
                                                <input class="check" type="checkbox" <?php if($vo['spec_sync'] == '1'): ?> checked <?php endif; ?> value="1" name="spec_sync_<?php echo $vo['preset_mark_id']; ?>" onmouseover="layer_tips = layer.tips('勾选后，修改规格名称或规格值，会自动同步更新已发布的商品规格', this, {time:100000});" onmouseout="layer.close(layer_tips);">
                                            </label>
                                        </div>
                                    </td>
                                    <td class="sort">
                                        <div class="w60 tc">
                                            <input type="text" size="4" name="sort_order_<?php echo $vo['preset_mark_id']; ?>" value="<?php echo $vo['sort_order']; ?>">
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; endif; else: echo "" ;endif; ?>    
                        </tbody>
                    </table>
                </div>
            </form>
            <div class="iDiv" style="display: none;"></div>
        </div>
        <div class="tDiv">
            <div class="tDiv2">
                <div class="fbutton">
                    <a href="JavaScript:void(0);" onclick="SaveSpecTpl(this);" class="layui-btn layui-btn-primary">
                            <span>保存</span>
                    </a>
                </div>
                <div class="fbutton">
                    <a href="JavaScript:void(0);" onclick="AddSpecTpl(this);" class="layui-btn layui-btn-primary">
                         <input type="hidden" id="PresetMarkId" value="<?php echo $PresetMarkId; ?>">
                         <span class="red">新增规格</span>
                     </a>
                </div>
            </div>
            <div style="clear:both"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        // 表格行点击选中切换
        $('#flexigrid > table>tbody >tr').click(function(){
            $(this).toggleClass('trSelected');
        });

        // 点击刷新数据
        $('.fa-refresh').click(function(){
            location.href = location.href;
        });
    });

    // 追加规格模板
    function AddSpecTpl(obj){
        var PresetMarkId = $('#PresetMarkId').val();
        // 预设值标记ID，规格名称唯一标识
        if (!PresetMarkId || 0 == PresetMarkId) {
            // 标记ID出错则提示
            layer.alert('数据出错，请刷新重试~', {icon: 2, title:false});
        }
        // 拼装html
        var AddHtml = 
        [
            '<tr class="tr" id="tr_'+PresetMarkId+'">'+
                '<td class="sign">'+
                    '<div class="w10 tc">'+
                        '<input type="hidden" name="preset_new[]" value="'+PresetMarkId+'">'+
                        '<input type="hidden" name="preset_id_'+PresetMarkId+'[]">'+
                    '</div>'+
                '</td>'+

                '<td class="">'+
                    '<div class="w180 tc preset-bt3" style="text-align: left;">'+
                        '<span>'+
                        '<input type="text" name="preset_name_'+PresetMarkId+'[]" placeholder="请输入规格名称"><em data-id="tr_'+PresetMarkId+'" onclick="DelSpecTpl(this);"><i style="position: absolute;right: 4px;" class="fa fa-times-circle" title="关闭"></i></em>'+
                        '</span>'+
                    '</div>'+
                '</td>'+

                '<td style="width: 100%">'+
                    '<div class="preset-bt3" id="tr_td_input_'+PresetMarkId+'">'+
                            '<span>'+
                                '<input type="text" name="preset_value_'+PresetMarkId+'[]" placeholder="请输入规格值">'+
                                '<em onclick="DelSpecTplValue(this)"><i class="fa fa-times-circle" title="关闭"></i></em>'+
                            '</span>'+

                            '<span style="background:#4fc0e8; float:right; color: #fff; border-radius: 4px; padding:0px 6px  0px 20px; position: relative;" class="red" data-id="tr_td_input_'+PresetMarkId+'" onclick="AddSpecTplValue(this, '+PresetMarkId+');"><i class="layui-icon layui-icon-addition" style="position:absolute;left: 0; top:0px;margin: 0 3px; color: #fff"></i>添加规格值'+
                            '</span>'+
                    '</div>'+
                '</td>'+

                '<td class="sort">'+
                    '<div class="w100 tc">'+
                        '<label class="label">'+
                            '<input class="check" type="checkbox" value="0" disabled>'+
                        '</label>'+
                    '</div>'+
                '</td>'+

                '<td class="sort">'+
                    '<div class="w60 tc">'+
                        '<input type="text" name="sort_order_'+PresetMarkId+'" size="4" value="100">'+
                    '</div>'+
                '</td>'+
            '</tr>'
        ];

        // 追加规格名称规格值框架
        $('#Template').append(AddHtml);
        // 更新标记ID数
        PresetMarkId++;
        $('#PresetMarkId').val(PresetMarkId);
    }

    // 追加规格值
    function AddSpecTplValue(obj, PresetMarkId, type){
        var tr_td_input_id = $(obj).attr('data-id');
        var AddValue = [
            '<span>'
        ];
        if ('old' == type) {
            AddValue += [
                '<input type="hidden" name="preset_id_old_'+PresetMarkId+'[]">'+
                '<input type="text" name="preset_value_old_'+PresetMarkId+'[]" placeholder="请输入规格值">'+
                '<em onclick="DelSpecTplValue(this)"><i class="fa fa-times-circle" title="关闭"></i></em>'
            ];
        }else{
            AddValue += [
                '<input type="text" name="preset_value_'+PresetMarkId+'[]" placeholder="请输入规格值">'+
                '<em onclick="DelSpecTplValue(this)"><i class="fa fa-times-circle" title="关闭"></i></em>'
                
            ];
        }
        AddValue += [
            '</span>'
        ];
        $('#'+tr_td_input_id).append(AddValue);
    }

    // 删除规格模板
    function DelSpecTpl(obj, preset_mark_id){
        layer.confirm('<font color="red">此操作将和规格值一起删除</font>，确认删除规格名称？', {
            title: false,
            btn: ['确定', '取消']
        }, function () {
            layer_loading('正在处理');
            if (preset_mark_id) {
                var url = "<?php echo url('Shop/spec_delete'); ?>";
                $.ajax({
                    type : 'post',
                    url  : url,
                    data : {preset_mark_id:preset_mark_id,_ajax:1},
                    dataType : 'json',
                    success : function(data){
                        layer.closeAll();
                        if(0 == data.code){
                            layer.alert(data.msg, {icon: 2, title:false});
                        }
                    }
                });
            }
            // 确定
            $('#'+$(obj).attr('data-id')).remove();
            layer.closeAll();
            layer.msg('操作成功！', {icon: 1, time: 1500});
            // 调用父级方法，更新预设规格下拉框的信息
            // if (preset_mark_id) parent.UpPresetSpecData(preset_mark_id);
        }, function (index) {
            // 取消
            layer.closeAll(index);
        });
    }

    // 删除指定规格值
    function DelSpecTplValue(obj, preset_id, preset_mark_id){
        layer_loading('正在处理');
        if (preset_id) {
            var url = "<?php echo url('Shop/spec_delete'); ?>";
            $.ajax({
                type : 'post',
                url  : url,
                data : {preset_id:preset_id,_ajax:1},
                dataType : 'json',
                success : function(data){
                    layer.closeAll();
                    if(0 == data.code){
                        layer.alert(data.msg, {icon: 2, title:false});
                    } else {
                        layer.msg(data.msg, {icon: 1, time:1000});
                        $(obj).parent().remove();
                        // parent.RefreshPresetValue(preset_id, preset_mark_id);
                    }
                }
            });
        } else {
            layer.closeAll();
            layer.msg('删除成功！', {icon: 1, time:1000});
            $(obj).parent().remove();
        }
    }

    // 保存规格信息
    function SaveSpecTpl(obj){
        layer_loading('正在处理');
        var mark_preset_ids = $('#mark_preset_ids').val();
        var mark_mark_ids = $('#mark_mark_ids').val();
        var url = "<?php echo url('Shop/spec_template', ['_ajax'=>1]); ?>";
        $.ajax({
            type : 'post',
            url  : url,
            data : $('#PostForm').serialize(),
            dataType : 'json',
            success : function(data){
                layer.closeAll();
                if (data.code == 1) {
                    // 关闭自身
                    // parent.UpPresetSpecData(null, mark_preset_ids, mark_mark_ids);
                    layer.closeAll();
                    layer.msg(data.msg, {icon: 1, time: 1500}, function() {
                        window.location.reload();
                    });
                    // 调用父级方法，更新预设规格下拉框的信息
                } else {
                    layer.alert(data.msg, {icon: 2, title: false});
                }
            }
        });
    }

    // 标记预设规格值ID
    function MarkPresetId(preset_id) {
        var mark_preset_ids = $('#mark_preset_ids').val();
        if (mark_preset_ids) {
            var mark_preset = mark_preset_ids.split(',');
            var is_mark = mark_preset.indexOf(preset_id);
            if (-1 == is_mark) {
                $('#mark_preset_ids').val(mark_preset_ids+','+preset_id);
            }
        }else{
            $('#mark_preset_ids').val(preset_id);
        }
    }

    // 标记预设规格名称ID
    function MarkMarkId(mark_id) {
        var mark_mark_ids = $('#mark_mark_ids').val();
        if (mark_mark_ids) {
            var mark_mark = mark_mark_ids.split(',');
            var is_mark = mark_mark.indexOf(mark_id);
            if (-1 == is_mark) {
                $('#mark_mark_ids').val(mark_mark_ids+','+mark_id);
            }
        }else{
            $('#mark_mark_ids').val(mark_id);
        }
    }
</script>
<br/>
<div id="goTop">
    <a href="JavaScript:void(0);" id="btntop">
        <i class="fa fa-angle-up"></i>
    </a>
    <a href="JavaScript:void(0);" id="btnbottom">
        <i class="fa fa-angle-down"></i>
    </a>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#think_page_trace_open').css('z-index', 99999);
    });
</script>
</body>
</html>