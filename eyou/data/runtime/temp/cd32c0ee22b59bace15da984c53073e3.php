<?php if (!defined('THINK_PATH')) exit(); /*a:6:{s:38:"./template/pc/users/release_centre.htm";i:1622085078;s:48:"/mnt/eyou/template/pc/users/skin/css/diy_css.htm";i:1622085090;s:44:"/mnt/eyou/template/pc/users/users_header.htm";i:1622085092;s:42:"/mnt/eyou/template/pc/users/users_left.htm";i:1622085092;s:52:"./public/static/template/users_v2/users_leftmenu.htm";i:1616142504;s:44:"/mnt/eyou/template/pc/users/users_footer.htm";i:1622085092;}*/ ?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8" />
    <title><?php echo $MenuTitle; ?>-<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_name"); echo $__VALUE__; ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <link href="<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_cmspath"); echo $__VALUE__; ?>/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/css/bootstrap.min.css","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/css/basic.css","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/css/eyoucms.css","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/css/shop.css","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/css/tb_style.css","","",""); echo $__VALUE__; ?>
    <!-- 新样式 2020-11-25 -->
    <?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/css/element/index.css","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/css/e-user.css","","",""); echo $__VALUE__; ?>
    
<!-- 官方内置样式表，升级会覆盖变动，请勿修改，否则后果自负 -->

<style>
/*
<?php echo $theme_color; ?>
<?php echo hex2rgba($theme_color,0.8); ?> */
    .ey-container .ey-nav ul .active a {
        border-left: <?php echo $theme_color; ?> 3px solid;
        background-color:<?php echo hex2rgba($theme_color,0.1); ?>;
        color:<?php echo $theme_color; ?>;
    }
    .ey-container .ey-nav ul li a:hover {
        color: <?php echo $theme_color; ?>;
    }
    .ey-container .user-box .user-box-text{
        background-color:<?php echo $theme_color; ?>;
    }
    .ey-container .user-box-r .user-box-top .user-top-r .more {
        color:<?php echo $theme_color; ?>;
    }
    .ey-container .user-box-r .user-box-bottom .data-info .link a {
        color: <?php echo $theme_color; ?>;
    }
    .ey-container .column-title .column-name {
        color: <?php echo $theme_color; ?>;
    }
    a:hover {
        color: <?php echo $theme_color; ?>;
    }
    .el-button:focus, .el-button:hover {
        color: <?php echo $theme_color; ?>;
        border-color: <?php echo hex2rgba($theme_color,0.2); ?>;
        background-color:<?php echo hex2rgba($theme_color,0.1); ?>;
    }
    .el-button--primary {
        background-color: <?php echo $theme_color; ?>;
        border-color: <?php echo $theme_color; ?>;
    }
    .ey-container .item-from-row .err a {
        color: <?php echo $theme_color; ?>;
    }
    .ey-container .checkbox-label .checkbox:checked+.check-mark {
        background-color: <?php echo $theme_color; ?>;
        border: 1px solid <?php echo $theme_color; ?>;
    }
    .ey-container .checkbox-label .checkbox:checked+.check-mark:after {
        background:<?php echo $theme_color; ?>;
    }
    .ey-container .radio-label .radio:checked+.check-mark {
        border: 1px solid <?php echo $theme_color; ?>;
        background-color: <?php echo $theme_color; ?>;
    }
    .ey-container .radio-label .radio:checked+.check-mark:after {
        background: <?php echo $theme_color; ?>;
    }
    .el-button--primary.is-plain {
        color: <?php echo $theme_color; ?>;
        border-color: <?php echo hex2rgba($theme_color,0.2); ?>;
        background-color:<?php echo hex2rgba($theme_color,0.1); ?>;
    }
    .el-button--primary.is-plain:focus, .el-button--primary.is-plain:hover {
        background: <?php echo $theme_color; ?>;
        border-color: <?php echo $theme_color; ?>;
    }
    .el-button--primary:hover, .el-button--primary:focus {
        background: <?php echo hex2rgba($theme_color,0.9); ?>;
        border-color: <?php echo hex2rgba($theme_color,0.9); ?>;
        color: #fff;
    }
    .el-input-number__decrease:hover, .el-input-number__increase:hover {
        color: <?php echo $theme_color; ?>;
    }
    .el-input-number__decrease:hover:not(.is-disabled)~.el-input .el-input__inner:not(.is-disabled), .el-input-number__increase:hover:not(.is-disabled)~.el-input .el-input__inner:not(.is-disabled) {
        border-color: <?php echo $theme_color; ?>;
    }
    .ey-container .address-con .address-item.cur {
        border: 1px solid <?php echo hex2rgba($theme_color,0.6); ?>;
    }
    .ey-container .address-con .address-item.cur:before {
        color: <?php echo $theme_color; ?>;
        border-top: 50px solid <?php echo $theme_color; ?>;
    }
    .ey-container .ey-con .shop-oper .shop-oper-l .el-button.active {
        color: #FFF;
        background-color: <?php echo $theme_color; ?>;
        border-color: <?php echo $theme_color; ?>;
    }
    .ey-container .goods-con .goods-item .goods-item-r .view a {
        color: <?php echo $theme_color; ?>;
    }
    .ey-container div.dataTables_paginate .paginate_button.active>a, .ey-containerdiv.dataTables_paginate .paginate_button.active>a:focus, .ey-containerdiv.dataTables_paginate .paginate_button.active>a:hover {
        background: <?php echo $theme_color; ?> !important;
        border-color: <?php echo $theme_color; ?> !important;
    }
    .ey-container .pagination>li>a:focus, .ey-container .pagination>li>a:hover, .ey-container .pagination>li>span:focus, .ey-container .pagination>li>span:hover {
        color:<?php echo $theme_color; ?>;
    }
    .ey-container .pagination>li>a, .ey-container .pagination>li>span {
        color:<?php echo $theme_color; ?>;
    }
    .el-button--danger {
        background-color: <?php echo $theme_color; ?>;
        border-color: <?php echo $theme_color; ?>;
    }
    .recharge .pc-vip-list.active {
        border: <?php echo $theme_color; ?> 2px solid;
        /*background-color:<?php echo hex2rgba($theme_color,0.1); ?>;*/
    }
    .recharge .pc-vip-list:hover {
    	border: <?php echo $theme_color; ?> 2px solid;
    }
    .recharge .pc-vip-list .icon-recomd {
    	background: <?php echo $theme_color; ?>;
    }
    .el-input.is-active .el-input__inner, .el-input__inner:focus {
        border-color: <?php echo $theme_color; ?>;
    }
    .ey-container .address-con .selected .address-item:before {
        color:<?php echo $theme_color; ?>;
        border-top: 50px solid <?php echo $theme_color; ?>;
    }
    .ey-container .address-con .selected .address-item {
        border: 1px solid <?php echo hex2rgba($theme_color,0.4); ?>;
    }
    .ey-container .pay-type .payTag a {
        color: <?php echo $theme_color; ?>;
        border-bottom: 1px solid <?php echo $theme_color; ?>;
    }
    .ey-container .pay-type .pay-con .pay-type-item.active {
        border-color: <?php echo hex2rgba($theme_color,0.4); ?>;
    }
    .ey-container .pay-type .pay-con .pay-type-item.active:before {
        color: <?php echo $theme_color; ?>;
        border-bottom: 30px solid <?php echo $theme_color; ?>;
    }
    .ey-header-nav .nav li ul li a:hover {
        background: <?php echo $theme_color; ?>;
    }
    .btn-primary {
        background-color: <?php echo $theme_color; ?>;
    }
    .btn-primary.focus, .btn-primary:focus, .btn-primary:hover {
        border-color: <?php echo $theme_color; ?>;
        background-color: <?php echo $theme_color; ?>;
    }
    .btn-primary.active.focus, .btn-primary.active:focus, .btn-primary.active:hover, .btn-primary:active.focus, .btn-primary:active:focus, .btn-primary:active:hover, .open>.btn-primary.dropdown-toggle.focus, .open>.btn-primary.dropdown-toggle:focus, .open>.btn-primary.dropdown-toggle:hover {
    	border-color:<?php echo $theme_color; ?>;
    	background-color:<?php echo $theme_color; ?>;
    }
    .login-link a {
        color: <?php echo $theme_color; ?>;
    }
    .register_index .login_link .login_link_register {
        color: <?php echo $theme_color; ?>;
    }

</style>

<script type="text/javascript">
    var __root_dir__ = "";
    var __version__ = "<?php echo $version; ?>";
    var __lang__ = "<?php echo $home_lang; ?>";
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
</script>
    
    <?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/static/common/js/jquery.min.js","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/plugins/layer-v3.1.0/layer.js","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/static/common/js/tag_global.js","","",""); echo $__VALUE__; ?>
</head>

<body class="centre shop">
<!-- 头部 -->
<!-- 头部信息 -->
<div class="ey-header">
    <div class="ey-logo">
        <a href="<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_cmsurl"); echo $__VALUE__; ?>"><img src="<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_logo"); echo $__VALUE__; ?>" /></a>
    </div>
    <div class="ey-header-nav w1200">
        <ul class="nav nav-menu nav-inline">
            <li><a href="<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_cmsurl"); echo $__VALUE__; ?>"> 首页 </a></li>
            <?php  if(isset($ui_typeid) && !empty($ui_typeid)) : $typeid = $ui_typeid; else: $typeid = ""; endif; if(empty($typeid) && isset($channelartlist["id"]) && !empty($channelartlist["id"])) : $typeid = intval($channelartlist["id"]); endif;  if(isset($ui_row) && !empty($ui_row)) : $row = $ui_row; else: $row = 60; endif; $tagChannel = new \think\template\taglib\eyou\TagChannel; $_result = $tagChannel->getChannel($typeid, "top", "", ""); if(is_array($_result) || $_result instanceof \think\Collection || $_result instanceof \think\Paginator): $i = 0; $e = 1;$__LIST__ = is_array($_result) ? array_slice($_result,0, $row, true) : $_result->slice(0, $row, true); if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$field): $field["typename"] = text_msubstr($field["typename"], 0, 100, false); $__LIST__[$key] = $_result[$key] = $field;$i= intval($key) + 1;$mod = ($i % 2 ); ?>
            <li>
                <a href="<?php echo $field['typeurl']; ?>"> <?php echo $field['typename']; if(!(empty($field['children']) || (($field['children'] instanceof \think\Collection || $field['children'] instanceof \think\Paginator ) && $field['children']->isEmpty()))): ?><i class="fa fa-angle-down margin-small-left"></i><?php endif; ?></a>
                <?php if(!(empty($field['children']) || (($field['children'] instanceof \think\Collection || $field['children'] instanceof \think\Paginator ) && $field['children']->isEmpty()))): ?>
                <ul class="drop-menu">
                    <?php  if(isset($ui_typeid) && !empty($ui_typeid)) : $typeid = $ui_typeid; else: $typeid = ""; endif; if(empty($typeid) && isset($channelartlist["id"]) && !empty($channelartlist["id"])) : $typeid = intval($channelartlist["id"]); endif;  if(isset($ui_row) && !empty($ui_row)) : $row = $ui_row; else: $row = 100; endif;if(is_array($field['children']) || $field['children'] instanceof \think\Collection || $field['children'] instanceof \think\Paginator): $i = 0; $e = 1;$__LIST__ = is_array($field['children']) ? array_slice($field['children'],0,100, true) : $field['children']->slice(0,100, true); if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$field2): $field2["typename"] = text_msubstr($field2["typename"], 0, 100, false); $__LIST__[$key] = $_result[$key] = $field2;$i= intval($key) + 1;$mod = ($i % 2 ); ?>
                    <li><a href="<?php echo $field2['typeurl']; ?>"><?php echo $field2['typename']; ?></a></li>
                    <?php ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $field2 = []; ?>
                </ul>
                <?php endif; ?>
            
            </li>
            <?php ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $field = []; ?>
         </ul>
    </div>
    <div class="ey-header-r">
        <?php if($php_servicemeal >= '1'): ?>
        <div class="right-item user-news">
            <a href="<?php echo url('user/UsersNotice/index'); ?>">
                <?php if($unread_num > '0'): ?>
                <span class="num" id="users_unread_num"><?php echo $unread_num; ?></span>
                <?php endif; ?>
                <span class="icon"><i class="el-icon-bell"></i></span>
            </a>
        </div>
        <?php endif; ?>
        <div class="right-item user-photo">
            <img src="<?php echo get_head_pic($users['head_pic']); ?>"/>
            <div class="user-drop">
                <ul>
                    <li><a href="<?php  $tagDiyurl = new \think\template\taglib\eyou\TagDiyurl; $__VALUE__ = $tagDiyurl->getDiyurl("", "user/Users/index", "", "", "", "", "", "", "ey_active"); echo $__VALUE__; ?>">个人中心</a></li>
                    <?php if(getUsersConfigData('shop.shop_open') == 1): ?>
                    <li><a href="<?php  $tagDiyurl = new \think\template\taglib\eyou\TagDiyurl; $__VALUE__ = $tagDiyurl->getDiyurl("", "user/Shop/shop_centre", "", "", "", "", "", "", "ey_active"); echo $__VALUE__; ?>">我的订单</a></li>
                    <?php endif; ?>
                    <li><a href="<?php  $tagDiyurl = new \think\template\taglib\eyou\TagDiyurl; $__VALUE__ = $tagDiyurl->getDiyurl("", "user/Users/collection_index", "", "", "", "", "", "", "ey_active"); echo $__VALUE__; ?>">我的收藏</a></li>
                    <li><a href="<?php  $tagDiyurl = new \think\template\taglib\eyou\TagDiyurl; $__VALUE__ = $tagDiyurl->getDiyurl("", "user/Users/logout", "", "", "", "", "", "", "ey_active"); echo $__VALUE__; ?>">安全退出</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- 头部信息结束 -->

<script type="text/javascript">
    //头像下拉
    $(".user-photo").mouseover(function(){
       $(".user-drop").show();
    });
    $(".user-photo").mouseout(function(){
       $(".user-drop").hide();
    });

    var GetUploadify_url = "<?php echo url('Uploadify/upload'); ?>";
</script>
<!-- END -->
<div class="ey-body-bg">
    <div class="ey-body">
        <div class="ey-container w1200">
			<!-- 侧边 -->
			<div class="ey-nav fl">
    <div class="sidebar-box">
        <ul> 
    <li>
        <div class="title">会员中心</div>
    </li>
    <?php  $tagUsermenu = new \think\template\taglib\eyou\TagUsermenu; $_result = $tagUsermenu->getUsermenu("active", ""); if(is_array($_result) || $_result instanceof \think\Collection || $_result instanceof \think\Paginator): $i = 0; $e = 1; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$field): $i= intval($key) + 1;$mod = ($i % 2 ); ?>
    <li class="<?php echo $field['currentstyle']; ?>">
        <a href="<?php echo $field['url']; ?>"><?php echo $field['title']; ?></a>
    </li>
    <?php ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $field = []; ?>
</ul>

<?php if(isset($usersConfig['shop_open']) && $usersConfig['shop_open'] == 1 && $php_servicemeal > 1): ?>
<ul>
    <li>
        <div class="title">商城中心</div>
    </li>
    <li <?php if(MODULE_NAME == 'user' && CONTROLLER_NAME == 'Shop' && ACTION_NAME == 'shop_cart_list'): ?> class="active" <?php endif; ?>>
        <a href="<?php echo url("user/Shop/shop_cart_list","",true,false,null,null,null);?>">购物车</a>
    </li>
    <li <?php if(MODULE_NAME == 'user' && CONTROLLER_NAME == 'Shop' && ACTION_NAME == 'shop_address_list'): ?> class="active" <?php endif; ?>>
        <a href="<?php echo url("user/Shop/shop_address_list","",true,false,null,null,null);?>">收货地址</a>
    </li>
    <li <?php if(MODULE_NAME == 'user' && (CONTROLLER_NAME == 'Shop' && in_array(ACTION_NAME, ['shop_centre', 'shop_order_details', 'after_service', 'after_service_details', 'after_service_apply']) || (CONTROLLER_NAME == 'ShopComment' && in_array(ACTION_NAME, ['product'])))): ?> class="active" <?php endif; ?>>
        <a href="<?php echo url("user/Shop/shop_centre","",true,false,null,null,null);?>">我的订单</a>
    </li>
    <?php if($php_servicemeal >= '2'): ?>
    <li <?php if(MODULE_NAME == 'user' && CONTROLLER_NAME == 'ShopComment' && in_array(ACTION_NAME, ['index'])): ?> class="active" <?php endif; ?>>
        <a href="<?php echo url("user/ShopComment/index","",true,false,null,null,null);?>"> 我的评价</a>
    </li>
    <?php endif; ?>
</ul>
<?php endif; if(isset($usersConfig['users_open_release']) && $usersConfig['users_open_release'] == 1): ?>
<ul>
    <li>
        <div class="title">投稿中心</div>
    </li>
    <li <?php if(MODULE_NAME == 'user' && CONTROLLER_NAME == 'UsersRelease' && ACTION_NAME == 'release_centre'): ?> class="active" <?php endif; ?>>
        <a href="<?php echo url('user/UsersRelease/release_centre', ['list'=>1]); ?>">投稿列表</a>
    </li>
    <li <?php if(MODULE_NAME == 'user' && CONTROLLER_NAME == 'UsersRelease' && (ACTION_NAME == 'article_add' || ACTION_NAME == 'article_edit')): ?> class="active" <?php endif; ?>>
        <a href="<?php echo url("user/UsersRelease/article_add","",true,false,null,null,null);?>">我要投稿</a>
    </li>
</ul>
<?php endif; ?>
    </div>
</div>
			<!-- END -->
			<!-- 中部 -->
			<div class="ey-con fr" >
				<div class="el-main main-bg">
					<!-- 顶部导航栏 -->
					<div class="column-title mb20">
						<div class="column-name"><?php echo $MenuTitle; ?></div>
					</div>
					<!-- END -->

					<div class="el-table el-table--fit el-table--enable-row-hover el-table--enable-row-transition mt20" style="width: 100%;">

						<table >
							<thead>
								<tr>
									<th style="width:40px;">
										<div class="cell tc">
											<label class="checkbox-label release-checkbox">
												<span> &nbsp; </span>
												<input class="checkbox" type="checkbox" onclick="javascript:$('input[name*=ids]').prop('checked',this.checked);">
												<span class="check-mark"></span>
											</label>
										</div>
									</th>
									<th style="width:320px ;"><div class="cell">文章标题</div></th>
									<th style="width: 100px;"><div class="cell tc">所属栏目</div></th>
									<th style="width: 200px;"><div class="cell tc">投稿时间</div></th>
									<th style="width: 100px;"><div class="cell tc">审核状态</div></th>
									<th style="width: 150px;"><div class="cell tc">操作</div></th>
								</tr>
							</thead>
							<tbody>
							<?php if(empty($eyou['field']['data']) || (($eyou['field']['data'] instanceof \think\Collection || $eyou['field']['data'] instanceof \think\Paginator ) && $eyou['field']['data']->isEmpty())): ?>
								<tr>
									<td  align="center" axis="col0" colspan="50">
										<div class="cell pt50 pb50 tc">
                                           <span class="mb10"><img id='litpic_img'  src="/public/static/common/images/null-data.png"/></span>
                                         </div>
									</td>
								</tr>
							<?php else: if(is_array($eyou['field']['data']) || $eyou['field']['data'] instanceof \think\Collection || $eyou['field']['data'] instanceof \think\Paginator): $i = 0; $e = 1; $__LIST__ = $eyou['field']['data'];if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$vo): $i= intval($key) + 1;$mod = ($i % 2 ); ?>    
								<tr>
									<td>
										<div class="cell tc">
											<label class="checkbox-label release-checkbox">
												<span> &nbsp; </span>
												<input class="checkbox" type="checkbox" name="aids[]" value="<?php echo $vo['aid']; ?>">
												<span class="check-mark"></span>
											</label>
											
										</div>
									</td>
									<td><div class="cell">
										<a  href="javascript:void(0);" data-arurl="<?php echo $vo['arcurl']; ?>" onclick="ArcUrl(this);">
											<?php echo $vo['title']; ?>
										</a></div>
									</td>
									<td>
										<div class="cell tc"><?php echo $vo['typename']; ?></div>
									</td>
									<td>
										<div class="cell tc"><?php echo date('Y-m-d H:i:s',$vo['add_time']); ?></div>
									</td>
									<td>
										<div class="cell tc"><?php echo (isset($home_article_arcrank[$vo['arcrank']]) && ($home_article_arcrank[$vo['arcrank']] !== '')?$home_article_arcrank[$vo['arcrank']]:''); ?></div>
									</td>
									<td>
										<div class="cell tc">
											<a href="javascript:void(0);" class="mr10" data-editurl="<?php echo $vo['editurl']; ?>" onclick="EditData(this);">
												编辑
											</a>
											<a href="javascript:void(0);" class="" style="margin-right: 0;" data-id='<?php echo $vo['aid']; ?>' data-url="<?php echo $eyou['field']['delurl']; ?>" onclick="DelData(this);">
												删除
											</a>
										</div>
									</td>
								</tr>
								<?php echo isset($vo["ey_1563185380"])?$vo["ey_1563185380"]:""; ?><?php echo (1 == $e && isset($vo["ey_1563185376"]))?$vo["ey_1563185376"]:""; ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $vo = []; endif; ?>
								<tr>
									<td >
										<label class="checkbox-label release-checkbox ml10">
											<span> &nbsp; </span>
											<input class="checkbox" type="checkbox" onclick="javascript:$('input[name*=ids]').prop('checked',this.checked);">
											<span class="check-mark"></span>
										</label>
									</td>
									<td >
										<a class="el-button el-button--default el-button--small" href="javascript:void(0);" onclick="BatchDelData(this, 'aids');" data-url="<?php echo $eyou['field']['delurl']; ?>">
											批量删除
										</a> 
									</td>
								</tr>
							</tbody>
						</table> 
					</div>
					<div style="text-align: center;"><?php echo $page; ?></div>
				</div>
			<!-- END -->
			</div>
           
        </div>
    </div>
</div>
<script type="text/javascript">
    // 内容查看
    function ArcUrl(obj) {
        window.open($(obj).attr('data-arurl'));
    }
    // 编辑内容
    function EditData(obj) {
        window.location.href = $(obj).attr('data-editurl');
    }
    // 单个删除发布的内容
    function DelData(obj) {
        layer.confirm('是否删除该投稿？', {
            title: false,
            btn: ['是','否'] //按钮
        }, function(){
            layer_loading('正在处理');
            // 确定
            $.ajax({
                type: "POST",
                url: $(obj).attr('data-url'),
                data: {del_id:$(obj).attr('data-id')},
                dataType: 'json',
                success: function (res) {
                    layer.closeAll();
                    if(res.code == 1){
                        layer.msg(res.msg, {icon: 1});
                        window.location.reload();
                    }else{
                        showErrorAlert(res.msg);
                    }
                },
                error:function(e){
                    layer.closeAll();
                    showErrorAlert(e.responseText);
                }
            });
        }, function(index){
            layer.close(index);
        });
    }
    // 批量删除发布的内容
    function BatchDelData(obj, name) {
        var a = [];
        $('input[name^='+name+']').each(function(i,o){
            if($(o).is(':checked')){
                a.push($(o).val());
            }
        })
        if(a.length == 0){
            showErrorAlert('请至少选择一项！');
            return;
        }
        // 删除按钮
        layer.confirm('确认批量彻底删除？', {
            title: false,
            btn: ['确定', '取消'] //按钮
        }, function () {
            layer_loading('正在处理');
            $.ajax({
                type: "POST",
                url: $(obj).attr('data-url'),
                data: {del_id:a},
                dataType: 'json',
                success: function (res) {
                    layer.closeAll();
                    if(res.code == 1){
                        layer.msg(res.msg, {icon: 1});
                        window.location.reload();
                    }else{
                        showErrorAlert(res.msg);
                    }
                },
                error:function(e){
                    layer.closeAll();
                    showErrorAlert(e.responseText);
                }
            });
        }, function (index) {
            layer.closeAll(index);
        });
    }
</script>
<!-- 底部 -->
	<div class="ey-footer">
    <footer class="container">
        <p><?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_copyright"); echo $__VALUE__; ?></p>
    </footer>
</body>
</html>
<!-- END -->