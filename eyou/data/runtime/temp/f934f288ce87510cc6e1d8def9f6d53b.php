<?php if (!defined('THINK_PATH')) exit(); /*a:5:{s:43:"./application/admin/template/coupon/add.htm";i:1640913865;s:57:"/mnt/api/api/application/admin/template/public/layout.htm";i:1640913882;s:60:"/mnt/api/api/application/admin/template/public/theme_css.htm";i:1640913882;s:53:"/mnt/api/api/application/admin/template/shop/left.htm";i:1640913887;s:57:"/mnt/api/api/application/admin/template/public/footer.htm";i:1640913882;}*/ ?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link href="/api/public/plugins/layui/css/layui.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/main.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/page.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/font/css/font-awesome.min.css" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="/api/public/static/admin/font/css/font-awesome-ie7.min.css">
<![endif]-->
<script type="text/javascript">
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
    var module_name = "<?php echo MODULE_NAME; ?>";
    var GetUploadify_url = "<?php echo url('Uploadify/upload'); ?>";
    var __root_dir__ = "/api";
    var __lang__ = "<?php echo $admin_lang; ?>";
</script>  
<link href="/api/public/static/admin/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="/api/public/static/admin/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
<!-- <link type="text/css" rel="stylesheet" href="/api/public/plugins/tags_input/css/jquery.tagsinput.css?v=<?php echo $version; ?>"> -->
<style type="text/css">html, body { overflow: visible;}</style>
<link href="/api/public/static/admin/css/diy_style.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css" />

<!-- 官方内置样式表，升级会覆盖变动，请勿修改，否则后果自负 -->

<style type="text/css">
	/*左侧收缩图标*/
	#foldSidebar i { font-size: 24px;color:<?php echo $global['web_theme_color']; ?>; }
    /*左侧菜单*/
    .eycms_cont_left{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl dd a:hover,.eycms_cont_left dl dd a.on,.eycms_cont_left dl dt.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl dd a:active{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist dd{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl.jslist dd a:hover,.eycms_cont_left dl.jslist dd a.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist:hover{ background:<?php echo $global['web_assist_color']; ?>; }
    /*栏目操作*/
    .cate-dropup .cate-dropup-con a:hover{ background-color: <?php echo $global['web_theme_color']; ?>; }
    /*按钮*/
    a.ncap-btn-green { background-color: <?php echo $global['web_theme_color']; ?>; }
    a:hover.ncap-btn-green { background-color: <?php echo $global['web_assist_color']; ?>; }
    .flexigrid .sDiv2 .btn:hover { background-color: <?php echo $global['web_theme_color']; ?>; }
    .flexigrid .mDiv .fbutton div.add{background-color: <?php echo $global['web_theme_color']; ?>; border: none;}
    .flexigrid .mDiv .fbutton div.add:hover{ background-color: <?php echo $global['web_assist_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds{color:<?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds:hover{ background-color: <?php echo $global['web_theme_color']; ?>;}
    /*选项卡字体*/
    .tab-base a.current,
    .tab-base a:hover.current { border-bottom: solid 2px <?php echo $global['web_theme_color']; ?> !important;color: <?php echo $global['web_theme_color']; ?> !important;}
    .addartbtn input.btn:hover{ border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>; }
    .addartbtn input.btn.selected{ color: <?php echo $global['web_theme_color']; ?>;border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>;}
	/*会员导航*/
	.member-nav-group .member-nav-item .btn.selected{background: <?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;box-shadow: -1px 0 0 0 <?php echo $global['web_theme_color']; ?>;}
	.member-nav-group .member-nav-item:first-child .btn.selected{border-left: 1px solid <?php echo $global['web_theme_color']; ?> !important;}
	/*搜索按钮图标*/
	.flexigrid .sDiv2 .fa-search{}
        
    /* 商品订单列表标题 */
   .flexigrid .mDiv .ftitle h3 {border-left: 3px solid <?php echo $global['web_theme_color']; ?>;} 
	
    /*分页*/
    .pagination > .active > a, .pagination > .active > a:focus, 
	.pagination > .active > a:hover, 
	.pagination > .active > span, 
	.pagination > .active > span:focus, 
	.pagination > .active > span:hover { border-color: <?php echo $global['web_theme_color']; ?>;color:<?php echo $global['web_theme_color']; ?>; }
    
    .layui-form-onswitch {border-color: <?php echo $global['web_theme_color']; ?> !important;background-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-enable.selected { background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-disable.selected {background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    input[type="text"]:focus,
    input[type="text"]:hover,
    input[type="text"]:active,
    input[type="password"]:focus,
    input[type="password"]:hover,
    input[type="password"]:active,
    textarea:hover,
    textarea:focus,
    textarea:active { border-color:<?php echo hex2rgba($global['web_theme_color'],0.8); ?>;box-shadow: 0 0 0 2px <?php echo hex2rgba($global['web_theme_color'],0.15); ?> !important;}
    .input-file-show:hover .type-file-button {background-color:<?php echo $global['web_theme_color']; ?> !important; }
    .input-file-show:hover {border-color: <?php echo $global['web_theme_color']; ?> !important;box-shadow: 0 0 5px <?php echo hex2rgba($global['web_theme_color'],0.5); ?> !important;}
    .input-file-show:hover span.show a,
    .input-file-show span.show a:hover { color: <?php echo $global['web_theme_color']; ?> !important;}
    .input-file-show:hover .type-file-button {background-color: <?php echo $global['web_theme_color']; ?> !important; }
    .color_z { color: <?php echo $global['web_theme_color']; ?> !important;}
    a.imgupload{
        background-color: <?php echo $global['web_theme_color']; ?> !important;
        border-color: <?php echo $global['web_theme_color']; ?> !important;
    }
    /*专题节点按钮*/
    .ncap-form-default .special-add{background-color:<?php echo $global['web_theme_color']; ?>;border-color:<?php echo $global['web_theme_color']; ?>;}
    .ncap-form-default .special-add:hover{background-color:<?php echo $global['web_assist_color']; ?>;border-color:<?php echo $global['web_assist_color']; ?>;}
    
    /*更多功能标题*/
    .on-off_panel .title::before {background-color:<?php echo $global['web_theme_color']; ?>;}

</style>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.js"></script>
<!-- <script type="text/javascript" src="/api/public/plugins/tags_input/js/jquery.tagsinput.js?v=<?php echo $version; ?>"></script> -->
<script type="text/javascript" src="/api/public/static/admin/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/api/public/plugins/layer-v3.1.0/layer.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/admin.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/common.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/api/public/plugins/layui/layui.js"></script>
<script src="/api/public/static/admin/js/myFormValidate.js"></script>
<script src="/api/public/static/admin/js/myAjax2.js?v=<?php echo $version; ?>"></script>
<script src="/api/public/static/admin/js/global.js?v=<?php echo $version; ?>"></script>
</head>
<script type="text/javascript" src="/api/public/plugins/laydate/laydate.js"></script>
<script type="text/javascript" src="/api/public/plugins/colpick/js/colpick.js"></script>
<link href="/api/public/plugins/colpick/css/colpick.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
#coupon_color {
    /*margin:0;*/
    /*padding:0;*/
    border:solid 1px #dc143c;
    width:70px;
    height:20px;
    border-right:40px solid green;
    /*line-height:20px;*/
} 
</style>

<body class="bodystyle" style="overflow-y: scroll; cursor: default; -moz-user-select: inherit;">
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="sidebar-second ">
    <ul>
        <li class="sidebar-second-title">商城中心</li>
        <li>
            <a <?php if('Statistics' == CONTROLLER_NAME): ?>class="active"<?php endif; ?> href='<?php echo url("Statistics/index"); ?>'>数据统计</a>
        </li>
        <li>
            <a <?php if(('ShopService' == CONTROLLER_NAME) OR ('Shop' == CONTROLLER_NAME and in_array(ACTION_NAME, ['index', 'order_details']))): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/index"); ?>'>所有订单</a>
        </li>
        <li>
            <a <?php if('ShopProduct' == CONTROLLER_NAME and in_array(ACTION_NAME, ['index', 'add', 'edit'])): ?>class="active"<?php endif; ?> href='<?php echo url("ShopProduct/index"); ?>'>商品管理</a>
        </li>
        <li>
            <a <?php if('ShopProduct' == CONTROLLER_NAME and in_array(ACTION_NAME, ['attrlist_index', 'attribute_index'])): ?>class="active"<?php endif; ?> href='<?php echo url("ShopProduct/attrlist_index"); ?>'>商品参数</a>
        </li>
        <?php 
            $shop_open_spec = getUsersConfigData('shop.shop_open_spec');
         if($shop_open_spec == '1'): ?>
        <li>
            <a <?php if('Shop' == CONTROLLER_NAME and in_array(ACTION_NAME, ['spec_index'])): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/spec_index"); ?>'>商品规格</a>
        </li>
        <?php endif; ?>
        <li>
            <a <?php if('Shop' == CONTROLLER_NAME and 'conf' == ACTION_NAME): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/conf"); ?>'>商城配置</a>
        </li>
        <?php if($php_servicemeal >= 2): ?>
        <li>
            <a <?php if('ShopComment' == CONTROLLER_NAME): ?>class="active"<?php endif; ?> href='<?php echo url("ShopComment/comment_index"); ?>'>商品评价</a>
        </li>
        <li>
            <a <?php if((in_array(CONTROLLER_NAME, ['Sharp','Coupon'])) OR ('Shop' == CONTROLLER_NAME and in_array(ACTION_NAME, ['market_index']))): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/market_index"); ?>'>营销功能</a>
        </li>
        <?php endif; ?>
    </ul>
</div>
<div class="page" style="margin-left:98px;">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="<?php echo url('Coupon/index'); ?>" title="返回列表"><i class="fa fa-angle-double-left"></i>返回</a>
            <ul class="tab-base nc-row">
                <li><a href="javascript:void(0);" class="tab current"><span>新增优惠券</span></a></li>
            </ul>
        </div>
    </div>
    <form class="form-horizontal" id="postForm" action="<?php echo url('Coupon/add'); ?>" method="post">
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label for="coupon_name"><em>*</em>优惠券名称</label>
                </dt>
                <dd class="opt">
                    <input type="text" name="coupon_name" id="coupon_name" class="input-txt">
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="coupon_name"><em>*</em>优惠券颜色</label>
                </dt>
                <dd class="opt">
                    <input type="text" name="coupon_color" value="#dc143c" id="coupon_color" style="border-color: #dc143c;" />
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="coupon_type"><em>*</em>可使用商品</label>
                </dt>
                <dd class="opt">
                    <label><input type="radio" name="coupon_type" value="1" checked="checked" />&nbsp;全站通用</label>
                    &nbsp; &nbsp;
                    <label><input type="radio" name="coupon_type" value="2" />&nbsp;指定商品</label>
                    &nbsp; &nbsp;
                    <label><input type="radio" name="coupon_type" value="3" />&nbsp;指定分类</label>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="coupon_form"><em>*</em>优惠券类型</label>
                </dt>
                <dd class="opt">
                    <label class="yh-jm"><input type="radio" name="coupon_form" value="1" checked="checked"/>&nbsp;满减券</label>
                     <label class="yh-zk"><input type="radio" name="coupon_form" value="0"/>&nbsp;折扣</label>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row" style="display: none;">
                <dt class="tit">
                    <label for="product_id"><em>*</em>商品集合</label>
                </dt>
                <dd class="opt">
                    <a href="JavaScript:void(0);" onclick="Product();" class="ncap-btn ncap-btn-green">选择商品</a>
                    <input type="hidden" name="product_id" id="product_id">
                    <input type="hidden" id="SelectProductID">
                    <span style="color: red; display: none;">已选 <a href="JavaScript:void(0);" id="product_id_num">0</a> 商品</span>
                </dd>
            </dl>
            <script type="text/javascript">
                $( function() {
                    $('#product_id_num').click( function() {
                        var product_ids = $('#product_id').val();
                        var url = "<?php echo url('Coupon/select_product'); ?>";
                        url = url + '&product_ids=' + product_ids;
                        //iframe窗
                        layer.open({
                            type: 2,
                            title: '已选商品列表',
                            shadeClose: false,
                            shade: 0.3,
                            area: ['80%', '90%'],
                            content: url
                        });
                    });
                });

                function Product() {
                    var url = "<?php echo url('Coupon/select_product'); ?>";
                    //iframe窗
                    layer.open({
                        type: 2,
                        title: '请选择商品',
                        shadeClose: false,
                        shade: 0.3,
                        area: ['80%', '90%'],
                        content: url
                    });
                }
            </script>

            <dl class="row" style="display: none;">
                <dt class="tit">
                    <label for="arctype_id"><em>*</em>商品分类</label>
                </dt>
                <dd class="opt">
                    <a href="JavaScript:void(0);" onclick="Arctype();" class="ncap-btn ncap-btn-green">选择分类</a>
                    <input type="hidden" name="arctype_id" id="arctype_id" class="input-txt">
                    <input type="hidden" id="SelectArctypeID">
                    <span style="color: red; display: none;">已选 <a href="JavaScript:void(0);" id="arctype_id_num">0</a> 分类</span>
                </dd>
            </dl>
            <script type="text/javascript">
                $( function() {
                    $('#arctype_id_num').click( function() {
                        var arctype_ids = $('#arctype_id').val();
                        var url = "<?php echo url('Coupon/select_arctype'); ?>";
                        url = url + '&arctype_ids=' + arctype_ids;
                        //iframe窗
                        layer.open({
                            type: 2,
                            title: '已选分类列表',
                            shadeClose: false,
                            shade: 0.3,
                            area: ['80%', '90%'],
                            content: url
                        });
                    });
                });

                function Arctype() {
                    var url = "<?php echo url('Coupon/select_arctype'); ?>";
                    //iframe窗
                    layer.open({
                        type: 2,
                        title: '请选择分类',
                        shadeClose: false,
                        shade: 0.3,
                        area: ['80%', '90%'],
                        content: url
                    });
                }
            </script>
            <dl class="row">
                <dt class="tit">
                    <label for="coupon_price"><em>*</em>优惠规则</label>
                    <!--减免金额-->
                </dt>
                    <dd class="opt yh-opt">
                        满
                        <input type="text" name="conditions_use" id="conditions_use" class="input-txt" onpaste="this.value=this.value.replace(/[^\d.]/g, '');" onkeyup="this.value=this.value.replace(/[^\d.]/g, '');" style="width: 80px !important;">
                        元，减
                        <input type="text" name="coupon_price" id="coupon_price" class="input-txt" onpaste="this.value=this.value.replace(/[^\d.]/g, '');" onkeyup="this.value=this.value.replace(/[^\d.]/g, '');" style="width: 80px !important;">
                        元
                        <!--<div style="height:10px;"></div>-->
                        <!--<label style="margin-right: 20px;">折扣</label>-->
                        <!--<input type="text" name="conditions_use" id="conditions_use" class="input-txt" onpaste="this.value=this.value.replace(/[^\d.]/g, '');" onkeyup="this.value=this.value.replace(/[^\d.]/g, '');" style="width: 125px !important;">-->
                        <!-- %-->
                        <p class="notic"></p>
                    </dd>
            </dl>
            <script>
                $(function(){
                    $('.yh-jm').click(function(){
                        var jm_html = [
                                '满'
                                +'<input type="text" name="conditions_use" id="conditions_use" class="input-txt" onpaste="this.value=this.value.replace(/[^\d.]/g, "");" onkeyup="this.value=this.value.replace(/[^\d.]/g, "");" style="width: 80px !important;">'
                                +'元，减'
                                +'<input type="text" name="coupon_price" id="coupon_price" class="input-txt" onpaste="this.value=this.value.replace(/[^\d.]/g, "");" onkeyup="this.value=this.value.replace(/[^\d.]/g, "");" style="width: 80px !important;">'
                                +'元'
                            ];
                        $('.yh-opt').html(jm_html);
                    });
                    $('.yh-zk').click(function(){
                        var jm_html = [
                                '<label style="margin-right: 20px;">折扣</label>'
                                +'<input type="text" name="conditions_use" id="conditions_use" class="input-txt" onpaste="this.value=this.value.replace(/[^\d.]/g, "");" onkeyup="this.value=this.value.replace(/[^\d.]/g, "");" style="width: 125px !important;">'
                                +'%'
                                +'<div style="display:none;">减'
                                +'<input type="text" name="coupon_price" id="coupon_price" class="input-txt" onpaste="this.value=this.value.replace(/[^\d.]/g, "");" value="1" onkeyup="this.value=this.value.replace(/[^\d.]/g, "");" style="width: 80px !important;">'
                                +'元</div>'
                            ];
                        $('.yh-opt').html(jm_html);
                    });
                    $(document).ready(function(){
                        var val=$('input:radio[name="coupon_form"]:checked').val();
                        if(val){
                            if(val == 1){
                                var jm_html = [
                                    '满'
                                    +'<input type="text" name="conditions_use" value="<?php echo $info['conditions_use']; ?>" id="conditions_use" class="input-txt" onpaste="this.value=this.value.replace(/[^\d.]/g, "");" onkeyup="this.value=this.value.replace(/[^\d.]/g, "");" style="width: 80px !important;">'
                                    +'元，减'
                                    +'<input type="text" name="coupon_price"  value="<?php echo $info['coupon_price']; ?>" id="coupon_price" class="input-txt" onpaste="this.value=this.value.replace(/[^\d.]/g, "");" onkeyup="this.value=this.value.replace(/[^\d.]/g, "");" style="width: 80px !important;">'
                                    +'元'
                                ];
                                $('.yh-opt').html(jm_html);
                            }
                            if(val == 0){
                                var jm_html = [
                                    '<label style="margin-right: 20px;">折扣</label>'
                                    +'<input type="text" name="conditions_use"  value="<?php echo $info['conditions_use']; ?>" id="conditions_use" class="input-txt" onpaste="this.value=this.value.replace(/[^\d.]/g, "");" onkeyup="this.value=this.value.replace(/[^\d.]/g, "");" style="width: 125px !important;">'
                                    +'%'
                                    +'<div style="display:none;">减'
                                    +'<input type="text" name="coupon_price"  value="<?php echo $info['coupon_price']; ?>" id="coupon_price" class="input-txt" onpaste="this.value=this.value.replace(/[^\d.]/g, "");" value="1" onkeyup="this.value=this.value.replace(/[^\d.]/g, "");" style="width: 80px !important;">'
                                    +'元</div>'
                                ];
                                $('.yh-opt').html(jm_html);
                            }
                        }
                    })
                });
            </script>
            <dl class="row">
                <dt class="tit">
                    <label for="coupon_stock"><em>*</em>库存</label>
                </dt>
                <dd class="opt">
                    <input type="text" name="coupon_stock" id="coupon_stock" class="input-txt" onpaste="this.value=this.value.replace(/[^\d.]/g, '');" onkeyup="this.value=this.value.replace(/[^\d.]/g, '');" style="width: 100px !important;">&nbsp;张
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row none" >
                <dt class="tit">
                    <label for="redeem_points">兑换所需积分</label>
                </dt>
                <dd class="opt">
                    <input type="text" name="redeem_points" id="redeem_points" value="0" class="input-txt" onpaste="this.value=this.value.replace(/[^\d.]/g, '');" onkeyup="this.value=this.value.replace(/[^\d.]/g, '');">
                    <p class="notic">兑换优惠券需要的积分，为0时免费领取</p>
                </dd>
            </dl>
            <dl class="row none">
                <dt class="tit">
                    <label for="redeem_authority">领取权限</label>
                </dt>
                <dd class="opt">
                    <?php if(is_array($users_level) || $users_level instanceof \think\Collection || $users_level instanceof \think\Paginator): $i = 0; $__LIST__ = $users_level;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                        <label><input type="checkbox" name="level_id[]" value="<?php echo $vo['level_id']; ?>"><?php echo $vo['level_name']; ?></label>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                    <p class="notic">未勾选时，所有用户都可以领取</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="use_type"><em>*</em>使用期限</label>
                </dt>
                <dd class="opt">
                    <label><input type="radio" name="use_type" value="1" checked="checked" onclick="chooseUseType(this);" />&nbsp;固定时间内有效</label>
                    <input type="text" autocomplete="off" name="use_start_time" id="use_start_time" value="<?php echo $start_date; ?>" class="input-txt" style="width: 130px !important;" />-
                    <input type="text" autocomplete="off" name="use_end_time" id="use_end_time" value="<?php echo $end_date; ?>" class="input-txt" style="width: 130px !important;" />
                    <div style="height:10px;"></div>
                    <label><input type="radio" name="use_type" value="2" onclick="chooseUseType(this);" />&nbsp;领到当日开始</label>
                    <input type="text" id="valid_days_2" value="30" disabled="disabled"  class="input-txt" onpaste="this.value=this.value.replace(/[^\d.]/g, '');" onkeyup="this.value=this.value.replace(/[^\d.]/g, '');" style="width: 50px !important;">&nbsp;天内有效
                    <div style="height:10px;"></div>
                    <label><input type="radio" name="use_type" value="3" onclick="chooseUseType(this);" />&nbsp;领到次日开始</label>
                    <input type="text" id="valid_days_3" value="30" disabled="disabled"  class="input-txt" onpaste="this.value=this.value.replace(/[^\d.]/g, '');" onkeyup="this.value=this.value.replace(/[^\d.]/g, '');" style="width: 50px !important;">&nbsp;天内有效
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="start_date"><em>*</em>发放日期</label>
                </dt>
                <dd class="opt">
                    <input type="text" autocomplete="off" name="start_date" id="start_date" value="<?php echo $start_date; ?>" class="input-txt">
                    <p class="notic">优惠券开始发放时间</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="end_date"><em>*</em>结束日期</label>
                </dt>
                <dd class="opt">
                    <input type="text" autocomplete="off" name="end_date" id="end_date" value="" class="input-txt">
                    <p class="notic">优惠券发放结束时间</p>
                </dd>
            </dl>
            
            <div class="bot">
                <input type="hidden" name="valid_days" id="valid_days">
                <a href="JavaScript:void(0);" onclick="checkForm();" class="ncap-btn-big ncap-btn-green" id="submitBtn">确认提交</a>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        // 颜色选择
        $('#coupon_color').colpick({
            flat:false,
            layout:'rgbhex',
            submit:0,
            colorScheme:'light',
            color:$('#coupon_color').val(),
            onChange:function(hsb,hex,rgb,el,bySetColor) {
                $(el).css('border-color','#'+hex);
                // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
                if(!bySetColor) $(el).val('#'+hex);
            }
        }).keyup(function(){
            $(this).colpickSetColor('#'+this.value);
        });
    });

    function chooseUseType(obj){
        var val = $(obj).val();
        if ( 1 == val ){
            $("#use_start_time").attr('disabled',false);
            $("#use_end_time").attr('disabled',false);
            $("#valid_days_2").attr('disabled',true);
            $("#valid_days_3").attr('disabled',true);
        }else if( 2 == val ){
            $("#valid_days_2").attr('disabled',false);
            $("#use_start_time").attr('disabled',true);
            $("#use_end_time").attr('disabled',true);
            $("#valid_days_3").attr('disabled',true);
        }else if( 3 == val ){
            $("#valid_days_3").attr('disabled',false);
            $("#use_start_time").attr('disabled',true);
            $("#use_end_time").attr('disabled',true);
            $("#valid_days_2").attr('disabled',true);
        }
    }

    $(function () {
        $('#start_date').layDate();
        $('#end_date').layDate();
        $('#use_start_time').layDate();
        $('#use_end_time').layDate();

        $('input[name=coupon_type]').click(function() {
            if (3 == $(this).val()) {
                $('#arctype_id').parent().parent().show();
                $('#product_id').parent().parent().hide();
            } else if (2 == $(this).val()) {
                $('#product_id').parent().parent().show();
                $('#arctype_id').parent().parent().hide();
            } else {
                $('#product_id').parent().parent().hide();
                $('#arctype_id').parent().parent().hide();
            }
        });
    });

    var parentObj = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引

    function checkForm(){
        if ($('#coupon_name').val() == '') {
            showErrorMsg('请填写优惠券名称');
            $('#coupon_name').focus();
            return false;
        }
        // if ($('#conditions_use').val() == '') {
        //     showErrorMsg('请填写优惠规则-使用条件');
        //     $('#conditions_use').focus();
        //     return false;
        // }
        // if ($('#coupon_price').val() == '') {
        //     showErrorMsg('请填写优惠规则-减免金额');
        //     $('#coupon_price').focus();
        //     return false;
        // }

        if ($('#coupon_stock').val() == '') {
            showErrorMsg('请填写库存');
            $('#coupon_stock').focus();
            return false;
        }
        var use_type = $("input[name='use_type']:checked").val();

        if ( 1 == use_type ) {
            if ($('#use_start_time').val() == '') {
                showErrorMsg('请填写优惠券有效开始时间');
                $('#use_start_time').focus();
                return false;
            }
            if ($('#use_end_time').val() == '') {
                showErrorMsg('请填写优惠券有效结束时间');
                $('#use_end_time').focus();
                return false;
            }
        }else if ( 2 == use_type ) {
            var valid_days_2 = $('#valid_days_2').val();
            if ( valid_days_2 == '') {
                showErrorMsg('请填写优惠券有效天数');
                $('#valid_days_2').focus();
                return false;
            }
            $("#valid_days").val(valid_days_2)
        }else if ( 3 == use_type ) {
            var valid_days_3 = $('#valid_days_3').val();
            if ( valid_days_3 == '') {
                showErrorMsg('请填写优惠券有效天数');
                $('#valid_days_3').focus();
                return false;
            }
            $("#valid_days").val(valid_days_3)
        }

        if ($('#start_date').val() == '') {
            showErrorMsg('请选择优惠券发放日期');
            $('#start_date').focus();
            return false;
        }

        if ($('#end_date').val() == '') {
            showErrorMsg('请选择优惠券结束日期');
            $('#end_date').focus();
            return false;
        }
        layer_loading('正在处理');
        $.ajax({
            type : 'post',
            url : "<?php echo url('Coupon/add', ['_ajax'=>1]); ?>",
            data : $('#postForm').serialize(),
            dataType : 'json',
            success : function(res){
                layer.closeAll();
                if(res.code == 1){
                    layer.msg(res.msg, {icon: 1, shade: 0.3, time: 1000}, function(){
                        window.location.href = res.url;
                    });
                }else{
                    showErrorMsg(res.msg);
                }
            },
            error: function(e){
                layer.closeAll();
                showErrorAlert(e.responseText);
            }
        });
    }
</script>

<br/>
<div id="goTop">
    <a href="JavaScript:void(0);" id="btntop">
        <i class="fa fa-angle-up"></i>
    </a>
    <a href="JavaScript:void(0);" id="btnbottom">
        <i class="fa fa-angle-down"></i>
    </a>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#think_page_trace_open').css('z-index', 99999);
    });
</script>
</body>
</html>