<?php if (!defined('THINK_PATH')) exit(); /*a:5:{s:47:"./application/admin/template/delivery/index.htm";i:1642403435;s:57:"/mnt/api/api/application/admin/template/public/layout.htm";i:1640913882;s:60:"/mnt/api/api/application/admin/template/public/theme_css.htm";i:1640913882;s:55:"/mnt/api/api/application/admin/template/public/page.htm";i:1640913882;s:57:"/mnt/api/api/application/admin/template/public/footer.htm";i:1640913882;}*/ ?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link href="/api/public/plugins/layui/css/layui.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/main.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/page.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/font/css/font-awesome.min.css" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="/api/public/static/admin/font/css/font-awesome-ie7.min.css">
<![endif]-->
<script type="text/javascript">
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
    var module_name = "<?php echo MODULE_NAME; ?>";
    var GetUploadify_url = "<?php echo url('Uploadify/upload'); ?>";
    var __root_dir__ = "/api";
    var __lang__ = "<?php echo $admin_lang; ?>";
</script>  
<link href="/api/public/static/admin/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="/api/public/static/admin/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
<!-- <link type="text/css" rel="stylesheet" href="/api/public/plugins/tags_input/css/jquery.tagsinput.css?v=<?php echo $version; ?>"> -->
<style type="text/css">html, body { overflow: visible;}</style>
<link href="/api/public/static/admin/css/diy_style.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css" />

<!-- 官方内置样式表，升级会覆盖变动，请勿修改，否则后果自负 -->

<style type="text/css">
	/*左侧收缩图标*/
	#foldSidebar i { font-size: 24px;color:<?php echo $global['web_theme_color']; ?>; }
    /*左侧菜单*/
    .eycms_cont_left{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl dd a:hover,.eycms_cont_left dl dd a.on,.eycms_cont_left dl dt.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl dd a:active{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist dd{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl.jslist dd a:hover,.eycms_cont_left dl.jslist dd a.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist:hover{ background:<?php echo $global['web_assist_color']; ?>; }
    /*栏目操作*/
    .cate-dropup .cate-dropup-con a:hover{ background-color: <?php echo $global['web_theme_color']; ?>; }
    /*按钮*/
    a.ncap-btn-green { background-color: <?php echo $global['web_theme_color']; ?>; }
    a:hover.ncap-btn-green { background-color: <?php echo $global['web_assist_color']; ?>; }
    .flexigrid .sDiv2 .btn:hover { background-color: <?php echo $global['web_theme_color']; ?>; }
    .flexigrid .mDiv .fbutton div.add{background-color: <?php echo $global['web_theme_color']; ?>; border: none;}
    .flexigrid .mDiv .fbutton div.add:hover{ background-color: <?php echo $global['web_assist_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds{color:<?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds:hover{ background-color: <?php echo $global['web_theme_color']; ?>;}
    /*选项卡字体*/
    .tab-base a.current,
    .tab-base a:hover.current { border-bottom: solid 2px <?php echo $global['web_theme_color']; ?> !important;color: <?php echo $global['web_theme_color']; ?> !important;}
    .addartbtn input.btn:hover{ border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>; }
    .addartbtn input.btn.selected{ color: <?php echo $global['web_theme_color']; ?>;border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>;}
	/*会员导航*/
	.member-nav-group .member-nav-item .btn.selected{background: <?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;box-shadow: -1px 0 0 0 <?php echo $global['web_theme_color']; ?>;}
	.member-nav-group .member-nav-item:first-child .btn.selected{border-left: 1px solid <?php echo $global['web_theme_color']; ?> !important;}
	/*搜索按钮图标*/
	.flexigrid .sDiv2 .fa-search{}
        
    /* 商品订单列表标题 */
   .flexigrid .mDiv .ftitle h3 {border-left: 3px solid <?php echo $global['web_theme_color']; ?>;} 
	
    /*分页*/
    .pagination > .active > a, .pagination > .active > a:focus, 
	.pagination > .active > a:hover, 
	.pagination > .active > span, 
	.pagination > .active > span:focus, 
	.pagination > .active > span:hover { border-color: <?php echo $global['web_theme_color']; ?>;color:<?php echo $global['web_theme_color']; ?>; }
    
    .layui-form-onswitch {border-color: <?php echo $global['web_theme_color']; ?> !important;background-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-enable.selected { background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-disable.selected {background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    input[type="text"]:focus,
    input[type="text"]:hover,
    input[type="text"]:active,
    input[type="password"]:focus,
    input[type="password"]:hover,
    input[type="password"]:active,
    textarea:hover,
    textarea:focus,
    textarea:active { border-color:<?php echo hex2rgba($global['web_theme_color'],0.8); ?>;box-shadow: 0 0 0 2px <?php echo hex2rgba($global['web_theme_color'],0.15); ?> !important;}
    .input-file-show:hover .type-file-button {background-color:<?php echo $global['web_theme_color']; ?> !important; }
    .input-file-show:hover {border-color: <?php echo $global['web_theme_color']; ?> !important;box-shadow: 0 0 5px <?php echo hex2rgba($global['web_theme_color'],0.5); ?> !important;}
    .input-file-show:hover span.show a,
    .input-file-show span.show a:hover { color: <?php echo $global['web_theme_color']; ?> !important;}
    .input-file-show:hover .type-file-button {background-color: <?php echo $global['web_theme_color']; ?> !important; }
    .color_z { color: <?php echo $global['web_theme_color']; ?> !important;}
    a.imgupload{
        background-color: <?php echo $global['web_theme_color']; ?> !important;
        border-color: <?php echo $global['web_theme_color']; ?> !important;
    }
    /*专题节点按钮*/
    .ncap-form-default .special-add{background-color:<?php echo $global['web_theme_color']; ?>;border-color:<?php echo $global['web_theme_color']; ?>;}
    .ncap-form-default .special-add:hover{background-color:<?php echo $global['web_assist_color']; ?>;border-color:<?php echo $global['web_assist_color']; ?>;}
    
    /*更多功能标题*/
    .on-off_panel .title::before {background-color:<?php echo $global['web_theme_color']; ?>;}

</style>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.js"></script>
<!-- <script type="text/javascript" src="/api/public/plugins/tags_input/js/jquery.tagsinput.js?v=<?php echo $version; ?>"></script> -->
<script type="text/javascript" src="/api/public/static/admin/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/api/public/plugins/layer-v3.1.0/layer.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/admin.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/common.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/api/public/plugins/layui/layui.js"></script>
<script src="/api/public/static/admin/js/myFormValidate.js"></script>
<script src="/api/public/static/admin/js/myAjax2.js?v=<?php echo $version; ?>"></script>
<script src="/api/public/static/admin/js/global.js?v=<?php echo $version; ?>"></script>
</head>

<body class="bodystyle" style="cursor: default; -moz-user-select: inherit;">
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="page">
    <div class="flexigrid">
        <div class="hDiv">
            <div class="hDivBox">
                <table cellspacing="0" cellpadding="0" style="width: 100%">
                    <thead>
                        <tr>
                            <?php if($main_lang == $admin_lang): ?>
                                <th class="sign w40" axis="col0">
                                    <div class="tc"><input type="checkbox" class="checkAll"></div>
                                </th>
                            <?php endif; ?>

                            <th abbr="article_title" axis="col3" class="w60">
                                <div class="tc">ID</div>
                            </th>
                           <th abbr="article_title" axis="col3">
                                <div class="tl text-l10" style="width: 100%">订单编号</div>
                            </th>
                             <th abbr="article_title" axis="col3" class="w120">
                                <div class="tc">产品名称</div>
                            </th>
                            <th abbr="article_title" axis="col3" class="w120">
                                <div class="tc">产品图片</div>
                            </th>
                            <th abbr="article_title" axis="col3" class="w120">
                                <div class="tc">提交用户</div>
                            </th>
                           <th abbr="article_title" axis="col3" class="w120">
                                <div class="tc">状态</div>
                            </th>
                            <th abbr="article_title" axis="col3" class="w120">
                                <div class="tc">提交时间</div>
                            </th>
                           <th axis="col1" class="w160">
                                 <div class="tc">操作</div>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>

        <div class="bDiv" style="height: auto;">
            <div id="flexigrid" cellpadding="0" cellspacing="0" border="0">
                <table style="width: 100%">
                    <tbody>
                    <?php if(empty($pro) || (($pro instanceof \think\Collection || $pro instanceof \think\Paginator ) && $pro->isEmpty())): ?>
                        <tr>
                            <td class="no-data" align="center" axis="col0" colspan="50">
                                <i class="fa fa-exclamation-circle"></i>没有符合条件的记录
                            </td>
                        </tr>
                    <?php else: if(is_array($pro) || $pro instanceof \think\Collection || $pro instanceof \think\Paginator): if( count($pro)==0 ) : echo "" ;else: foreach($pro as $k=>$vo): ?>
                            <tr>
                                <?php if($main_lang == $admin_lang): ?>
                                <td class="sign">
                                    <div class="w40 tc"> <input type="checkbox" name="ids[]" value="<?php echo $vo['id']; ?>"> </div>
                                </td>
                                <?php endif; ?>
                                <td>
                                    <div class="tc w60"> <?php echo $vo['id']; ?> </div>
                                </td>
                                 <td align="left" style="width:100%;">
                                    <div class="tl text-l10">
                                      <?php echo $vo['numbers']; ?>
                                    </div>
                                </td>

                                <td align="left">
                                   <div class="tc w120"> <?php echo $vo['titles']; ?> </div>
                                </td>
                                <td>
                                    <div class="tc w120">
                                        <ul class="adpic">
                                            <li>
                                                <img src="<?php echo $vo['litpic']; ?>" data-img-src="<?php echo $vo['litpic']; ?>">
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                                <td>
                                    <div class="tc w120"> <?php echo $vo['name']; ?> </div>
                                </td>
                                 <td>
                                    <div class="tc w120"> 
                                    <?php if($vo['status'] == 0): ?>
                                    未回交期
                                    <?php endif; if($vo['status'] == 1): ?>
                                    已回交期
                                    <?php endif; ?>
                                    </div>
                                </td>
                                <td>
                                    <div class="tc w120"><?php echo date('Y-m-d',$vo['add_time']); ?> </div>
                                </td>
                                
                              <td class="operation">
                                <div class="w160 tc">
                                    <?php if($vo['status'] == 1): ?>
                                     <a class="btn blue">已回复</a>
                                    <i></i>
                                    <?php endif; if($vo['status'] == 0): ?>
                                    <a href="<?php echo url('Delivery/edit',array('id'=>$vo['id'])); ?>" class="btn blue">回复</a>
                                    <i></i>
                                    <?php endif; ?>
                                </div>
                            </td>
                                
                            </tr>
                        <?php endforeach; endif; else: echo "" ;endif; endif; ?>
                    </tbody>
                </table>
            </div>
            <div class="iDiv" style="display: none;"></div>
        </div>

        <div class="tDiv">
            <div class="tDiv2">
                <?php if($main_lang == $admin_lang): ?>
                    <div class="fbutton checkboxall"> <input type="checkbox" class="checkAll"> </div>
                    <?php if(is_check_access(CONTROLLER_NAME.'@del') == '1'): ?>
                        <div class="fbutton">
                            <a onclick="batch_del(this, 'ids');" data-url="<?php echo url('AdPosition/del'); ?>" class="layui-btn layui-btn-primary">批量删除</a>
                        </div>
                    <?php endif; endif; ?>
                <!-- 分页 -->
                <div class="fbuttonr">
    <div class="pages">
       <?php echo $page; ?>
    </div>
</div>
<div class="fbuttonr">
    <div class="total">
        <h5>共有<?php echo $pager->totalRows; ?>条,每页显示
            <select name="pagesize" style="width: 60px;" onchange="ey_selectPagesize(this);">
                <option <?php if($pager->listRows == 20): ?> selected <?php endif; ?> value="20">20</option>
                <option <?php if($pager->listRows == 50): ?> selected <?php endif; ?> value="50">50</option>
                <option <?php if($pager->listRows == 100): ?> selected <?php endif; ?> value="100">100</option>
                <option <?php if($pager->listRows == 200): ?> selected <?php endif; ?> value="200">200</option>
            </select>
        </h5>
    </div>
</div>
            </div>
            <div style="clear:both"></div>
        </div>
    </div>
</div>

<div id="hi-img-pop">
    <div href="javascript:;" class="hi-close"></div>
    <img src="" alt="">
</div>
<style>
    /* 图片弹窗 */
        
        #hi-img-pop {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 1000;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.5);
            text-align: center;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        
        #hi-img-pop * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        
        #hi-img-pop .hi-close {
            position: fixed;
            top: 86%;
            left: 50%;
            margin-left: -25px;
            width: 50px;
            height: 50px;
            color: #fff;
            background: #fff;
            border-radius: 50%;
            transition: .5s;
            cursor: pointer;
        }
        
        #hi-img-pop .hi-close:after,
        #hi-img-pop .hi-close:before {
            content: "";
            position: absolute;
            bottom: 24px;
            left: 10px;
            width: 30px;
            height: 2px;
            background: #333;
            transition: .5s;
        }
        
        #hi-img-pop .hi-close:hover {
            background: #019dee;
        }
        
        #hi-img-pop .hi-close:hover:after,
        #hi-img-pop .hi-close:hover:before {
            background: #fff;
        }
        
        #hi-img-pop .hi-close:after {
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg);
        }
        
        #hi-img-pop .hi-close:before {
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg);
        }
        
        #hi-img-pop img {
            position: fixed;
            left: 50%;
            top: 50%;
            max-width: 90%;
            max-height: 70%;
            -webkit-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
        }
</style>


<script type="text/javascript">
    $(function() {
        $('input[name*=ids]').click(function() {
            if ($('input[name*=ids]').length == $('input[name*=ids]:checked').length) {
                $('.checkAll').prop('checked', 'checked');
            } else {
                $('.checkAll').prop('checked', false);
            }
        });
        $('input[type=checkbox].checkAll').click(function() {
            $('input[type=checkbox]').prop('checked', this.checked);
        });
    });
    
    $(document).ready(function() {
        // 表格行点击选中切换
        $('#flexigrid > table>tbody >tr').click(function() {
            $(this).toggleClass('trSelected');
        });

        // 点击刷新数据
        $('.fa-refresh').click(function() {
            location.href = location.href;
        });

        $('#searchForm select[name=type]').change(function(){
            $('#searchForm').submit();
        });
    });
    
    function samplefun(obj){
        var title = $(obj).attr('data-typename');
        layer.confirm('请确认要驳回？', {
            title: false,
            btn: ['确定','取消'] //按钮
        }, function(){
            alert($(obj).attr('data-url'));
            layer_loading('正在处理');
            // 确定
            $.ajax({
                type : 'post',
                url : $(obj).attr('data-url'),
                data : {del_id:$(obj).attr('data-id'),_ajax:1},
                dataType : 'json',
                success : function(data){
                    layer.closeAll();
                    if(data.code == 1){
                        layer.msg(data.msg, {icon: 1});
                        window.location.reload();
                        // $('tr[data-id="'+$(obj).attr('data-id')+'"]').remove();
                    }else{
                        layer.alert(data.msg, {icon: 2, title:false});  //alert(data);
                    }
                }
            })
        }, function(index){
            layer.close(index);
        });
        return false;
    }
    
     function examinefun(obj){
        var title = $(obj).attr('data-typename');
        layer.confirm('请确认审核？', {
            title: false,
            btn: ['确定','取消'] //按钮
        }, function(){
            alert($(obj).attr('data-url'));
            layer_loading('正在处理');
            // 确定
            $.ajax({
                type : 'post',
                url : $(obj).attr('data-url'),
                data : {del_id:$(obj).attr('data-id'),_ajax:1},
                dataType : 'json',
                success : function(data){
                    layer.closeAll();
                    if(data.code == 1){
                        layer.msg(data.msg, {icon: 1});
                        window.location.reload();
                        // $('tr[data-id="'+$(obj).attr('data-id')+'"]').remove();
                    }else{
                        layer.alert(data.msg, {icon: 2, title:false});  //alert(data);
                    }
                }
            })
        }, function(index){
            layer.close(index);
        });
        return false;
    }
    
    function HiImgPop(obj) {
        var oImgBox = $("#hi-img-pop"); //弹窗
        var oClose = oImgBox.find(".hi-close"); //关闭按钮
        var oImg = oImgBox.find("img"); //图片标签
        // 点击显示弹窗，设置图片src
        obj.each(function() {
            $(this).click(function() {
                oImgBox.stop().fadeIn();
                oImg.attr("src", $(this).attr("data-img-src"));
            });
        });
        // 点击关闭弹窗
        oClose.click(function() {
            oImgBox.stop().fadeOut();
        });
    }
    // 调用
    HiImgPop($(".adpic li img"));


</script>

<br/>
<div id="goTop">
    <a href="JavaScript:void(0);" id="btntop">
        <i class="fa fa-angle-up"></i>
    </a>
    <a href="JavaScript:void(0);" id="btnbottom">
        <i class="fa fa-angle-down"></i>
    </a>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#think_page_trace_open').css('z-index', 99999);
    });
</script>
</body>
</html>