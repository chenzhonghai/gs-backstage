<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:50:"./application/admin/template/member/users_edit.htm";i:1640913879;s:57:"/mnt/api/api/application/admin/template/public/layout.htm";i:1640913882;s:60:"/mnt/api/api/application/admin/template/public/theme_css.htm";i:1640913882;s:57:"/mnt/api/api/application/admin/template/public/footer.htm";i:1640913882;}*/ ?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link href="/api/public/plugins/layui/css/layui.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/main.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/page.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/font/css/font-awesome.min.css" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="/api/public/static/admin/font/css/font-awesome-ie7.min.css">
<![endif]-->
<script type="text/javascript">
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
    var module_name = "<?php echo MODULE_NAME; ?>";
    var GetUploadify_url = "<?php echo url('Uploadify/upload'); ?>";
    var __root_dir__ = "/api";
    var __lang__ = "<?php echo $admin_lang; ?>";
</script>  
<link href="/api/public/static/admin/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="/api/public/static/admin/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
<!-- <link type="text/css" rel="stylesheet" href="/api/public/plugins/tags_input/css/jquery.tagsinput.css?v=<?php echo $version; ?>"> -->
<style type="text/css">html, body { overflow: visible;}</style>
<link href="/api/public/static/admin/css/diy_style.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css" />

<!-- 官方内置样式表，升级会覆盖变动，请勿修改，否则后果自负 -->

<style type="text/css">
	/*左侧收缩图标*/
	#foldSidebar i { font-size: 24px;color:<?php echo $global['web_theme_color']; ?>; }
    /*左侧菜单*/
    .eycms_cont_left{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl dd a:hover,.eycms_cont_left dl dd a.on,.eycms_cont_left dl dt.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl dd a:active{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist dd{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl.jslist dd a:hover,.eycms_cont_left dl.jslist dd a.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist:hover{ background:<?php echo $global['web_assist_color']; ?>; }
    /*栏目操作*/
    .cate-dropup .cate-dropup-con a:hover{ background-color: <?php echo $global['web_theme_color']; ?>; }
    /*按钮*/
    a.ncap-btn-green { background-color: <?php echo $global['web_theme_color']; ?>; }
    a:hover.ncap-btn-green { background-color: <?php echo $global['web_assist_color']; ?>; }
    .flexigrid .sDiv2 .btn:hover { background-color: <?php echo $global['web_theme_color']; ?>; }
    .flexigrid .mDiv .fbutton div.add{background-color: <?php echo $global['web_theme_color']; ?>; border: none;}
    .flexigrid .mDiv .fbutton div.add:hover{ background-color: <?php echo $global['web_assist_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds{color:<?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds:hover{ background-color: <?php echo $global['web_theme_color']; ?>;}
    /*选项卡字体*/
    .tab-base a.current,
    .tab-base a:hover.current { border-bottom: solid 2px <?php echo $global['web_theme_color']; ?> !important;color: <?php echo $global['web_theme_color']; ?> !important;}
    .addartbtn input.btn:hover{ border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>; }
    .addartbtn input.btn.selected{ color: <?php echo $global['web_theme_color']; ?>;border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>;}
	/*会员导航*/
	.member-nav-group .member-nav-item .btn.selected{background: <?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;box-shadow: -1px 0 0 0 <?php echo $global['web_theme_color']; ?>;}
	.member-nav-group .member-nav-item:first-child .btn.selected{border-left: 1px solid <?php echo $global['web_theme_color']; ?> !important;}
	/*搜索按钮图标*/
	.flexigrid .sDiv2 .fa-search{}
        
    /* 商品订单列表标题 */
   .flexigrid .mDiv .ftitle h3 {border-left: 3px solid <?php echo $global['web_theme_color']; ?>;} 
	
    /*分页*/
    .pagination > .active > a, .pagination > .active > a:focus, 
	.pagination > .active > a:hover, 
	.pagination > .active > span, 
	.pagination > .active > span:focus, 
	.pagination > .active > span:hover { border-color: <?php echo $global['web_theme_color']; ?>;color:<?php echo $global['web_theme_color']; ?>; }
    
    .layui-form-onswitch {border-color: <?php echo $global['web_theme_color']; ?> !important;background-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-enable.selected { background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-disable.selected {background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    input[type="text"]:focus,
    input[type="text"]:hover,
    input[type="text"]:active,
    input[type="password"]:focus,
    input[type="password"]:hover,
    input[type="password"]:active,
    textarea:hover,
    textarea:focus,
    textarea:active { border-color:<?php echo hex2rgba($global['web_theme_color'],0.8); ?>;box-shadow: 0 0 0 2px <?php echo hex2rgba($global['web_theme_color'],0.15); ?> !important;}
    .input-file-show:hover .type-file-button {background-color:<?php echo $global['web_theme_color']; ?> !important; }
    .input-file-show:hover {border-color: <?php echo $global['web_theme_color']; ?> !important;box-shadow: 0 0 5px <?php echo hex2rgba($global['web_theme_color'],0.5); ?> !important;}
    .input-file-show:hover span.show a,
    .input-file-show span.show a:hover { color: <?php echo $global['web_theme_color']; ?> !important;}
    .input-file-show:hover .type-file-button {background-color: <?php echo $global['web_theme_color']; ?> !important; }
    .color_z { color: <?php echo $global['web_theme_color']; ?> !important;}
    a.imgupload{
        background-color: <?php echo $global['web_theme_color']; ?> !important;
        border-color: <?php echo $global['web_theme_color']; ?> !important;
    }
    /*专题节点按钮*/
    .ncap-form-default .special-add{background-color:<?php echo $global['web_theme_color']; ?>;border-color:<?php echo $global['web_theme_color']; ?>;}
    .ncap-form-default .special-add:hover{background-color:<?php echo $global['web_assist_color']; ?>;border-color:<?php echo $global['web_assist_color']; ?>;}
    
    /*更多功能标题*/
    .on-off_panel .title::before {background-color:<?php echo $global['web_theme_color']; ?>;}

</style>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.js"></script>
<!-- <script type="text/javascript" src="/api/public/plugins/tags_input/js/jquery.tagsinput.js?v=<?php echo $version; ?>"></script> -->
<script type="text/javascript" src="/api/public/static/admin/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/api/public/plugins/layer-v3.1.0/layer.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/admin.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/common.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/api/public/plugins/layui/layui.js"></script>
<script src="/api/public/static/admin/js/myFormValidate.js"></script>
<script src="/api/public/static/admin/js/myAjax2.js?v=<?php echo $version; ?>"></script>
<script src="/api/public/static/admin/js/global.js?v=<?php echo $version; ?>"></script>
</head>

<body class="bodystyle" style="overflow-y: scroll;">
<div id="toolTipLayer" style="position: absolute; z-index: 9999; display: none; visibility: visible; left: 95px; top: 573px;"></div>
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="<?php echo url('Member/users_index'); ?>" title="返回列表"><i class="fa fa-angle-double-left"></i>返回</a>
            <ul class="tab-base nc-row">
                <li><a href="javascript:void(0);" class="current"><span>编辑会员</span></a></li>
            </ul>
        </div>
    </div>
    <form class="form-horizontal" id="postForm" action="<?php echo url('Member/users_edit'); ?>" method="post">
        <div class="ncap-form-default">
            <dl class="row"><dt class="tit"><label><b>基本资料</b></label></dt></dl>
            <dl class="row">
                <dt class="tit">
                    <label for="username">会员头像</label>
                </dt>
                <dd class="opt">
                    <div class="txpic" onClick="GetUploadify(1,'','allimg','head_pic_call_back');">
                        <input type="hidden" name="head_pic" id="head_pic" value="<?php echo $info['head_pic']; ?>" />
                        <img id="img_head_pic" src="<?php echo get_head_pic($info['head_pic']); ?>" />
                        <em>更换头像</em>
                    </div>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="username">用&nbsp;&nbsp;户&nbsp;&nbsp;名</label>
                </dt>
                <dd class="opt">
                    <?php echo $info['username']; ?>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="nickname">会员昵称</label>
                </dt>
                <dd class="opt">
                    <input type="text" name="nickname" id="nickname" value="<?php echo $info['nickname']; ?>" class="input-txt">
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="password">登录密码</label>
                </dt>
                <dd class="opt">
                    <input type="text" name="password" id="password" autocomplete="off" class="input-txt" placeholder="不修改留空">
                </dd>
            </dl>
            <dl class="row <?php if(empty($userConfig['level_member_upgrade'])): ?> none <?php endif; ?> ">
                <dt class="tit">
                    <label for="level">会员级别</label>
                </dt>
                <dd class="opt">
                    <select name="level" id="level" onchange="IsOpenDays(this);">
                        <?php if(is_array($level) || $level instanceof \think\Collection || $level instanceof \think\Paginator): $i = 0; $__LIST__ = $level;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                            <option value="<?php echo $vo['level_id']; ?>" <?php if($info['level'] == $vo['level_id']): ?>selected<?php endif; ?>><?php echo $vo['level_name']; ?></option>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                    </select>
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>

            <?php if(1 == $userConfig['level_member_upgrade']): ?>
                <dl class="row" <?php if($info['level'] == '1'): ?>style="display: none;"<?php endif; ?> id='users_days'>
                    <dt class="tit">
                        <label for="users_days"><em>*</em>会员天数</label>
                    </dt>
                    <dd class="opt">
                        <input type="text" name="level_maturity_days_up" id="level_maturity_days" value="<?php echo (isset($info['level_maturity_days_new']) && ($info['level_maturity_days_new'] !== '')?$info['level_maturity_days_new']:'0'); ?>" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/\[^0-9]/g,''));" maxlength="10">
                        <span class="err"></span>
                        <p class="notic">请填写会员有效期天数，不填写则会默认为注册会员！</p>
                    </dd>
                </dl>
                <input type="hidden" name="level_maturity_days_new" value="<?php echo $info['level_maturity_days_new']; ?>">
                <input type="hidden" name="level_maturity_days_old" value="<?php echo $info['level_maturity_days']; ?>">
            <?php endif; ?>
            <input type="hidden" id="level_member_upgrade" value="<?php echo (isset($userConfig['level_member_upgrade']) && ($userConfig['level_member_upgrade'] !== '')?$userConfig['level_member_upgrade']:'0'); ?>">

            <?php if(1 == $userConfig['pay_open']): ?>
            <dl class="row">
                <dt class="tit">
                    <label for="users_money">账户余额</label>
                </dt>
                <dd class="opt">
                    <input type="text" name="users_money" id="users_money" value="<?php echo (isset($info['users_money']) && ($info['users_money'] !== '')?$info['users_money']:'0.00'); ?>" onkeyup='this.value=this.value.replace(/[^\d.]/g,"");' onpaste='this.value=this.value.replace(/[^\d.]/g,"")' maxlength="10">&nbsp;元
                    <p class="notic"></p>
                </dd>
            </dl>
            <?php endif; if($php_servicemeal > 1): ?>
            <dl class="row">
                <dt class="tit">
                    <label for="coin">会员<?php echo (isset($scoreCofing['score_name']) && ($scoreCofing['score_name'] !== '')?$scoreCofing['score_name']:'积分'); ?></label>
                </dt>
                <dd class="opt">
                    <input type="text" name="scores" id="scores" value="<?php echo (isset($info['scores']) && ($info['scores'] !== '')?$info['scores']:'0'); ?>" onkeyup='this.value=this.value.replace(/[^\d]/g,"");' onpaste='this.value=this.value.replace(/[^\d]/g,"")' maxlength="10">
                    <p class="notic"></p>
                </dd>
            </dl>
            <?php endif; ?>
            <dl class="row">
                <dt class="tit">
                    <label>是否激活</label>
                </dt>
                <dd class="opt">
                    <div class="onoff">
                        <label for="is_activation1" class="cb-enable <?php if($info['is_activation'] == 1): ?>selected<?php endif; ?>">是</label>
                        <label for="is_activation0" class="cb-disable <?php if($info['is_activation'] == 0): ?>selected<?php endif; ?>">否</label>
                        <input id="is_activation1" name="is_activation" value="1" type="radio" <?php if($info['is_activation'] == 1): ?> checked="checked"<?php endif; ?>>
                        <input id="is_activation0" name="is_activation" value="0" type="radio" <?php if($info['is_activation'] == 0): ?> checked="checked"<?php endif; ?>>
                    </div>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="reg_time">注册日期</label>
                </dt>
                <dd class="opt">
                    <?php echo date('Y-m-d H:i:s',$info['reg_time']); ?>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="last_login">最后登录</label>
                </dt>
                <dd class="opt">
                    <?php echo date('Y-m-d H:i:s',$info['last_login']); ?>
                </dd>
            </dl>
            <?php if(!(empty($users_para) || (($users_para instanceof \think\Collection || $users_para instanceof \think\Paginator ) && $users_para->isEmpty()))): ?>
            <dl class="row"><dt class="tit"><label><b>更多资料</b></label></dt></dl>
           <dl class="row">
                <dt class="tit">
                    <label for="email">邮箱</label>
                </dt>
                <dd class="opt">
                    <input type="text" name="email" id="email" value="<?php echo $info['email']; ?>" class="input-txt">
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="password">手机号</label>
                </dt>
                <dd class="opt">
                    <input type="text" name="mobile" id="mobile" autocomplete="off" class="input-txt" value="<?php echo $info['mobile']; ?>" placeholder="不修改留空">
                </dd>
            </dl>
            <?php endif; ?>
            <dl class="row">
                <dt class="tit">
                    <label for="last_login">快捷操作</label>
                </dt>
                <dd class="opt">
                    <a href="<?php echo url('Member/syn_users_login', ['users_id'=>$info['users_id'], 'mca'=>'user/Users/index']); ?>" target="_blank">登录到此用户会员中心</a>
                </dd>
            </dl>
            <div class="bot">
                <input type="hidden" name="users_id" value="<?php echo $info['users_id']; ?>">
                <a href="JavaScript:void(0);" onclick="checkForm();" class="ncap-btn-big ncap-btn-green" id="submitBtn">确认提交</a>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    function IsOpenDays(obj){
        if (1 == $('#level_member_upgrade').val()) {
            var level_id = $(obj).val();
            if (1 == level_id) {
                $('#users_days').css('display','none');
            }else{
                $('#users_days').css('display','');
                $('#level_maturity_days').css('border-color','red').focus();
            }
        }
    }

    var parentObj = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
    // 判断输入框是否为空
    function checkForm(){
        if (1 == $('#level_member_upgrade').val()) {
            if (1 != $('#level').val() && (!$('#level_maturity_days').val() || 0 == $('#level_maturity_days').val())){
                showErrorMsg('请填写会员有效期天数！');
                $('#level_maturity_days').focus();
                return false;
            }
        }

        layer_loading('正在处理');
        $.ajax({
            type : 'post',
            url : "<?php echo url('Member/users_edit', ['_ajax'=>1]); ?>",
            data : $('#postForm').serialize(),
            dataType : 'json',
            success : function(res){
                layer.closeAll();
                if(res.code == 1){
                    layer.msg(res.msg, {icon: 1, time:1000},function(){
                        window.location.reload();
                    });
                }else{
                    showErrorMsg(res.msg);
                }
            },
            error: function(e){
                layer.closeAll();
                showErrorAlert(e.responseText);
            }
        });
    }

    function head_pic_call_back(fileurl_tmp)
    {
      $("#head_pic").val(fileurl_tmp);
      $("#img_head_pic").attr('src', fileurl_tmp);
    }
</script>

<br/>
<div id="goTop">
    <a href="JavaScript:void(0);" id="btntop">
        <i class="fa fa-angle-up"></i>
    </a>
    <a href="JavaScript:void(0);" id="btnbottom">
        <i class="fa fa-angle-down"></i>
    </a>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#think_page_trace_open').css('z-index', 99999);
    });
</script>
</body>
</html>