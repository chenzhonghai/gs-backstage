<?php if (!defined('THINK_PATH')) exit(); /*a:5:{s:54:"./application/admin/template/coupon/select_product.htm";i:1640913866;s:57:"/mnt/api/api/application/admin/template/public/layout.htm";i:1640913882;s:60:"/mnt/api/api/application/admin/template/public/theme_css.htm";i:1640913882;s:55:"/mnt/api/api/application/admin/template/public/page.htm";i:1640913882;s:57:"/mnt/api/api/application/admin/template/public/footer.htm";i:1640913882;}*/ ?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link href="/api/public/plugins/layui/css/layui.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/main.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/page.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/font/css/font-awesome.min.css" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="/api/public/static/admin/font/css/font-awesome-ie7.min.css">
<![endif]-->
<script type="text/javascript">
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
    var module_name = "<?php echo MODULE_NAME; ?>";
    var GetUploadify_url = "<?php echo url('Uploadify/upload'); ?>";
    var __root_dir__ = "/api";
    var __lang__ = "<?php echo $admin_lang; ?>";
</script>  
<link href="/api/public/static/admin/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="/api/public/static/admin/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
<!-- <link type="text/css" rel="stylesheet" href="/api/public/plugins/tags_input/css/jquery.tagsinput.css?v=<?php echo $version; ?>"> -->
<style type="text/css">html, body { overflow: visible;}</style>
<link href="/api/public/static/admin/css/diy_style.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css" />

<!-- 官方内置样式表，升级会覆盖变动，请勿修改，否则后果自负 -->

<style type="text/css">
	/*左侧收缩图标*/
	#foldSidebar i { font-size: 24px;color:<?php echo $global['web_theme_color']; ?>; }
    /*左侧菜单*/
    .eycms_cont_left{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl dd a:hover,.eycms_cont_left dl dd a.on,.eycms_cont_left dl dt.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl dd a:active{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist dd{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl.jslist dd a:hover,.eycms_cont_left dl.jslist dd a.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist:hover{ background:<?php echo $global['web_assist_color']; ?>; }
    /*栏目操作*/
    .cate-dropup .cate-dropup-con a:hover{ background-color: <?php echo $global['web_theme_color']; ?>; }
    /*按钮*/
    a.ncap-btn-green { background-color: <?php echo $global['web_theme_color']; ?>; }
    a:hover.ncap-btn-green { background-color: <?php echo $global['web_assist_color']; ?>; }
    .flexigrid .sDiv2 .btn:hover { background-color: <?php echo $global['web_theme_color']; ?>; }
    .flexigrid .mDiv .fbutton div.add{background-color: <?php echo $global['web_theme_color']; ?>; border: none;}
    .flexigrid .mDiv .fbutton div.add:hover{ background-color: <?php echo $global['web_assist_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds{color:<?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds:hover{ background-color: <?php echo $global['web_theme_color']; ?>;}
    /*选项卡字体*/
    .tab-base a.current,
    .tab-base a:hover.current { border-bottom: solid 2px <?php echo $global['web_theme_color']; ?> !important;color: <?php echo $global['web_theme_color']; ?> !important;}
    .addartbtn input.btn:hover{ border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>; }
    .addartbtn input.btn.selected{ color: <?php echo $global['web_theme_color']; ?>;border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>;}
	/*会员导航*/
	.member-nav-group .member-nav-item .btn.selected{background: <?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;box-shadow: -1px 0 0 0 <?php echo $global['web_theme_color']; ?>;}
	.member-nav-group .member-nav-item:first-child .btn.selected{border-left: 1px solid <?php echo $global['web_theme_color']; ?> !important;}
	/*搜索按钮图标*/
	.flexigrid .sDiv2 .fa-search{}
        
    /* 商品订单列表标题 */
   .flexigrid .mDiv .ftitle h3 {border-left: 3px solid <?php echo $global['web_theme_color']; ?>;} 
	
    /*分页*/
    .pagination > .active > a, .pagination > .active > a:focus, 
	.pagination > .active > a:hover, 
	.pagination > .active > span, 
	.pagination > .active > span:focus, 
	.pagination > .active > span:hover { border-color: <?php echo $global['web_theme_color']; ?>;color:<?php echo $global['web_theme_color']; ?>; }
    
    .layui-form-onswitch {border-color: <?php echo $global['web_theme_color']; ?> !important;background-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-enable.selected { background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-disable.selected {background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    input[type="text"]:focus,
    input[type="text"]:hover,
    input[type="text"]:active,
    input[type="password"]:focus,
    input[type="password"]:hover,
    input[type="password"]:active,
    textarea:hover,
    textarea:focus,
    textarea:active { border-color:<?php echo hex2rgba($global['web_theme_color'],0.8); ?>;box-shadow: 0 0 0 2px <?php echo hex2rgba($global['web_theme_color'],0.15); ?> !important;}
    .input-file-show:hover .type-file-button {background-color:<?php echo $global['web_theme_color']; ?> !important; }
    .input-file-show:hover {border-color: <?php echo $global['web_theme_color']; ?> !important;box-shadow: 0 0 5px <?php echo hex2rgba($global['web_theme_color'],0.5); ?> !important;}
    .input-file-show:hover span.show a,
    .input-file-show span.show a:hover { color: <?php echo $global['web_theme_color']; ?> !important;}
    .input-file-show:hover .type-file-button {background-color: <?php echo $global['web_theme_color']; ?> !important; }
    .color_z { color: <?php echo $global['web_theme_color']; ?> !important;}
    a.imgupload{
        background-color: <?php echo $global['web_theme_color']; ?> !important;
        border-color: <?php echo $global['web_theme_color']; ?> !important;
    }
    /*专题节点按钮*/
    .ncap-form-default .special-add{background-color:<?php echo $global['web_theme_color']; ?>;border-color:<?php echo $global['web_theme_color']; ?>;}
    .ncap-form-default .special-add:hover{background-color:<?php echo $global['web_assist_color']; ?>;border-color:<?php echo $global['web_assist_color']; ?>;}
    
    /*更多功能标题*/
    .on-off_panel .title::before {background-color:<?php echo $global['web_theme_color']; ?>;}

</style>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.js"></script>
<!-- <script type="text/javascript" src="/api/public/plugins/tags_input/js/jquery.tagsinput.js?v=<?php echo $version; ?>"></script> -->
<script type="text/javascript" src="/api/public/static/admin/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/api/public/plugins/layer-v3.1.0/layer.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/admin.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/common.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/api/public/plugins/layui/layui.js"></script>
<script src="/api/public/static/admin/js/myFormValidate.js"></script>
<script src="/api/public/static/admin/js/myAjax2.js?v=<?php echo $version; ?>"></script>
<script src="/api/public/static/admin/js/global.js?v=<?php echo $version; ?>"></script>
</head>
<body style="overflow: auto; cursor: default; -moz-user-select: inherit;background-color:#F4F4F4; padding: 15px; ">
<style type="text/css">
    .roundlabel {
        display: inline-block;
        border: solid 1px #D02626;
        color: #D02626;
        padding: 0px 8px;
        font-size: 12px;
        border-radius: 5px;
    }
</style>
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="page" >
    <div class="flexigrid">
        <div class="mDiv">
            <div class="ftitle">
                <h3>商品列表</h3>
                <h5>(共<?php echo $pager->totalRows; ?>条数据<span style="display: none;">，已选择<span id="ProductNum"></span>个</span>)</h5>
            </div>
            <!-- <div title="刷新数据" class="pReload"><i class="fa fa-refresh"></i></div> -->
            <form class="navbar-form form-inline" id="postForm" action="<?php echo url('Coupon/select_product'); ?>" method="get" onsubmit="layer_loading('正在处理');">
                <?php echo (isset($searchform['hidden']) && ($searchform['hidden'] !== '')?$searchform['hidden']:''); ?>
                <div class="sDiv">
                    <div class="sDiv2">  
                        <select name="typeid" class="select">
                            <option value="">--所有商品--</option>
                            <?php echo $arctype_html; ?>
                        </select>
                    </div>
                    <div class="sDiv2">
                        <input type="hidden" name="typeid" id="typeid" value="<?php echo (\think\Request::instance()->param('typeid') ?: ''); ?>">
                        <input type="text" size="50" name="keywords" value="<?php echo \think\Request::instance()->param('keywords'); ?>" class="qsbox" placeholder="搜索商品名称...">
                        <input type="submit" class="btn" value="搜索">
                        <i class="fa fa-search"></i>
                    </div>
                </div>
            </form>
        </div>
        <div class="hDiv">
            <div class="hDivBox">
                <table cellspacing="0" cellpadding="0" style="width: 100%">
                    <thead>
                    <tr>
                        <th class="sign w40" axis="col0">
                            <div class="tc"><input type="checkbox" class="checkAll" onclick="AllProductSelect(this);"></div>
                        </th>
                        <th align="center" abbr="article_title" axis="col3" class="w60">
                            <div class="tc">封面图</div>
                        </th>
                        <th align="left" abbr="article_title" axis="col3" class="">
                            <div style="text-align: left; padding-left: 10px;" class="">商品标题</div>
                        </th>
                        <th abbr="article_time" axis="col6" class="w150">
                            <div class="tc">商品分类</div>
                        </th>
                        <th abbr="article_time" axis="col6" class="w120">
                            <div class="tc">商品价格(元)</div>
                        </th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="bDiv" style="height: auto;">
            <div id="flexigrid" cellpadding="0" cellspacing="0" border="0">
                <table style="width: 100%">
                    <tbody>
                    <?php if(empty($list) || (($list instanceof \think\Collection || $list instanceof \think\Paginator ) && $list->isEmpty())): ?>
                        <tr>
                            <td class="no-data" align="center" axis="col0" colspan="50">
                                <i class="fa fa-exclamation-circle"></i>没有符合条件的记录
                            </td>
                        </tr>
                    <?php else: if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                        <tr>
                            <td class="sign">
                                <div class="w40 tc">
                                    <input type="checkbox" name="ids[]" value="<?php echo $vo['product_id']; ?>" onclick="Select(this);">
                                </div>
                            </td>
                            <td class="w60">
                                <div class="tc">
                                    <img width="60" height="60" src="<?php echo get_default_pic($vo['litpic']); ?>">
                                </div>
                            </td>
                            <td class="" style="width: 100%;">
                                <div class="tl" style="padding-left: 10px;">
                                    <a href="<?php echo $vo['arcurl']; ?>" target="_blank"><?php echo $vo['title']; ?></a>
                                </div>
                            </td>
                            <td class="">
                                <div class="w150 tc"><?php echo $vo['typename']; ?></div>
                            </td>
                            <td class="">
                                <div class="w120 tc"><?php echo $vo['users_price']; ?></div>
                            </td>
                        </tr>
                        <?php endforeach; endif; else: echo "" ;endif; endif; ?>
                    </tbody>
                </table>
            </div>
            <div class="iDiv" style="display: none;"></div>
        </div>
        <div class="tDiv">
            <div class="tDiv2">
                <div class="fbutton checkboxall">
                    <input type="checkbox" class="checkAll" onclick="AllProductSelect(this);">
                </div>
                <div class="fbutton">
                    <a onclick="SaveData();" class="layui-btn layui-btn-primary">
                        <span>选择</span>
                    </a>
                </div>
                <div class="fbuttonr">
    <div class="pages">
       <?php echo $page; ?>
    </div>
</div>
<div class="fbuttonr">
    <div class="total">
        <h5>共有<?php echo $pager->totalRows; ?>条,每页显示
            <select name="pagesize" style="width: 60px;" onchange="ey_selectPagesize(this);">
                <option <?php if($pager->listRows == 20): ?> selected <?php endif; ?> value="20">20</option>
                <option <?php if($pager->listRows == 50): ?> selected <?php endif; ?> value="50">50</option>
                <option <?php if($pager->listRows == 100): ?> selected <?php endif; ?> value="100">100</option>
                <option <?php if($pager->listRows == 200): ?> selected <?php endif; ?> value="200">200</option>
            </select>
        </h5>
    </div>
</div>
            </div>
            <div style="clear:both"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var parentObj = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
    $(document).ready(function(){
        $('input[name*=ids]').click(function(){
            if ($('input[name*=ids]').length == $('input[name*=ids]:checked').length) {
                $('.checkAll').prop('checked','checked');
            } else {
                $('.checkAll').prop('checked', false);
            }
        });
        $('input[type=checkbox].checkAll').click(function(){
            $('input[type=checkbox]').prop('checked',this.checked);
        });

        $('#postForm select[name=typeid]').change(function(){
            $('#postForm').submit();
        });

        // 表格行点击选中切换
        $('#flexigrid > table>tbody >tr').click(function(){
            $(this).toggleClass('trSelected');
        });

        // 设置已选项
        var SelectProductID = parent.$('#SelectProductID').val();
        var TotalRows = <?php echo (isset($pager->totalRows) && ($pager->totalRows !== '')?$pager->totalRows:'0'); ?>;
        if (SelectProductID) {
            SelectProductID = SelectProductID.split(',');
            if (SelectProductID) {
                $('input[name*=ids]').each(function(i, o){
                    if (($.inArray($(o).val(), SelectProductID) >= 0)) {
                        $(o).prop('checked', 'checked');
                    }
                });
                if ($('input[name*=ids]').length == $('input[name*=ids]:checked').length) {
                    $('.checkAll').prop('checked','checked');
                } else {
                    $('.checkAll').prop('checked', false);
                }
            }
        }

        // 设置全选按钮，设置显示已选数量
        var ProductIdNum = parent.$('#SelectProductID').val();
        if (ProductIdNum) {
            ProductIdNum = ProductIdNum.split(',').length;
        } else {
            ProductIdNum = 0;
        }
        var TotalRows = <?php echo (isset($pager->totalRows) && ($pager->totalRows !== '')?$pager->totalRows:'0'); ?>;
        if (Number(TotalRows) >= Number(ProductIdNum) && 0 != ProductIdNum) {
            $('#ProductNum').html(ProductIdNum).parent().show();
        } else if (0 >= ProductIdNum) {
            $('#ProductNum').html(0).parent().hide();
        }
    });

    // 单页单选
    function Select(obj) {
        if($(obj).is(':checked')) {
            var SelectProductID = parent.$('#SelectProductID').val();
            if (SelectProductID) {
                SelectProductID = SelectProductID + ',' + $(obj).val();
            } else {
                SelectProductID = $(obj).val();
            }
            SelectProductID = SelectProductID.split(',');
            if (0 <= SelectProductID.length) {
                $('#ProductNum').html(SelectProductID.length).parent().show();
            } else {
                $('#ProductNum').html(0).parent().hide();
            }
            SelectProductID = SelectProductID.join(",");
            parent.$('#SelectProductID').val(SelectProductID);
        } else {
            var SelectProductID = parent.$('#SelectProductID').val();
            if (SelectProductID) {
                SelectProductID = SelectProductID.split(',');
                for (var i = 0; i < SelectProductID.length; i++) {
                    if (SelectProductID[i] == $(obj).val()) {
                        SelectProductID.splice(i, 1);
                    }
                }
                if (0 <= SelectProductID.length) {
                    $('#ProductNum').html(SelectProductID.length).parent().show();
                } else {
                    $('#ProductNum').html(0).parent().hide();
                }
                SelectProductID = SelectProductID.join(",");
                parent.$('#SelectProductID').val(SelectProductID);
            }
        }
    }

    // 全选所有商品
    function AllProductSelect(obj) {
        var product_ids = "<?php echo (\think\Request::instance()->param('product_ids') ?: ''); ?>";
        var keywords = "<?php echo (\think\Request::instance()->param('keywords') ?: ''); ?>";
        var typeid = "<?php echo (\think\Request::instance()->param('typeid') ?: '0'); ?>";

        $.ajax({
            url : "<?php echo url('Coupon/ajax_get_product_id'); ?>",
            data: {_ajax: 1, product_ids: product_ids, keywords: keywords, typeid: typeid},
            type: "POST",
            dataType: 'json',
            success: function (res) {
                layer.closeAll();
                if (res.code == 1) {
                    var TotalRows = <?php echo (isset($pager->totalRows) && ($pager->totalRows !== '')?$pager->totalRows:'0'); ?>;
                    var ProductIdNum = parent.$('#SelectProductID').val();
                    if (ProductIdNum) {
                        ProductIdNum = ProductIdNum.split(',').length;
                    } else {
                        ProductIdNum = 0;
                    }
                    if ('none' == $('#ProductNum').parent().css('display') || TotalRows != ProductIdNum) {
                        $('#ProductNum').html(res.data.ProductNum).parent().show();
                        parent.$('#SelectProductID').val(res.data.ProductIDS);
                    } else {
                        $('#ProductNum').html(0).parent().hide();
                        parent.$('#SelectProductID').val('');
                    }
                } else {
                    showErrorAlert(res.msg);
                }
            },
            error:function(e){
                layer.closeAll();
                showErrorAlert(e.responseText);
            }
        });
    }

    // 选择确认数据
    function SaveData() {
        var SelectProductIDNew = parent.$('#SelectProductID').val();
        parent.$('#product_id').val(SelectProductIDNew);
        if (SelectProductIDNew) {
            parent.$('#product_id_num').html(SelectProductIDNew.split(",").length).parent().show();
        } else {
            parent.$('#product_id_num').parent().hide();
        }
        parent.layer.close(parentObj);
    }
</script>

<br/>
<div id="goTop">
    <a href="JavaScript:void(0);" id="btntop">
        <i class="fa fa-angle-up"></i>
    </a>
    <a href="JavaScript:void(0);" id="btnbottom">
        <i class="fa fa-angle-down"></i>
    </a>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#think_page_trace_open').css('z-index', 99999);
    });
</script>
</body>
</html>