<?php if (!defined('THINK_PATH')) exit(); /*a:6:{s:52:"./application/admin/template/member/users_config.htm";i:1640913879;s:57:"/mnt/api/api/application/admin/template/public/layout.htm";i:1640913882;s:60:"/mnt/api/api/application/admin/template/public/theme_css.htm";i:1640913882;s:54:"/mnt/api/api/application/admin/template/member/bar.htm";i:1640913877;s:60:"/mnt/api/api/application/admin/template/member/users_bar.htm";i:1640913878;s:57:"/mnt/api/api/application/admin/template/public/footer.htm";i:1640913882;}*/ ?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link href="/api/public/plugins/layui/css/layui.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/main.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/page.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/font/css/font-awesome.min.css" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="/api/public/static/admin/font/css/font-awesome-ie7.min.css">
<![endif]-->
<script type="text/javascript">
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
    var module_name = "<?php echo MODULE_NAME; ?>";
    var GetUploadify_url = "<?php echo url('Uploadify/upload'); ?>";
    var __root_dir__ = "/api";
    var __lang__ = "<?php echo $admin_lang; ?>";
</script>  
<link href="/api/public/static/admin/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="/api/public/static/admin/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
<!-- <link type="text/css" rel="stylesheet" href="/api/public/plugins/tags_input/css/jquery.tagsinput.css?v=<?php echo $version; ?>"> -->
<style type="text/css">html, body { overflow: visible;}</style>
<link href="/api/public/static/admin/css/diy_style.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css" />

<!-- 官方内置样式表，升级会覆盖变动，请勿修改，否则后果自负 -->

<style type="text/css">
	/*左侧收缩图标*/
	#foldSidebar i { font-size: 24px;color:<?php echo $global['web_theme_color']; ?>; }
    /*左侧菜单*/
    .eycms_cont_left{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl dd a:hover,.eycms_cont_left dl dd a.on,.eycms_cont_left dl dt.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl dd a:active{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist dd{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl.jslist dd a:hover,.eycms_cont_left dl.jslist dd a.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist:hover{ background:<?php echo $global['web_assist_color']; ?>; }
    /*栏目操作*/
    .cate-dropup .cate-dropup-con a:hover{ background-color: <?php echo $global['web_theme_color']; ?>; }
    /*按钮*/
    a.ncap-btn-green { background-color: <?php echo $global['web_theme_color']; ?>; }
    a:hover.ncap-btn-green { background-color: <?php echo $global['web_assist_color']; ?>; }
    .flexigrid .sDiv2 .btn:hover { background-color: <?php echo $global['web_theme_color']; ?>; }
    .flexigrid .mDiv .fbutton div.add{background-color: <?php echo $global['web_theme_color']; ?>; border: none;}
    .flexigrid .mDiv .fbutton div.add:hover{ background-color: <?php echo $global['web_assist_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds{color:<?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds:hover{ background-color: <?php echo $global['web_theme_color']; ?>;}
    /*选项卡字体*/
    .tab-base a.current,
    .tab-base a:hover.current { border-bottom: solid 2px <?php echo $global['web_theme_color']; ?> !important;color: <?php echo $global['web_theme_color']; ?> !important;}
    .addartbtn input.btn:hover{ border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>; }
    .addartbtn input.btn.selected{ color: <?php echo $global['web_theme_color']; ?>;border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>;}
	/*会员导航*/
	.member-nav-group .member-nav-item .btn.selected{background: <?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;box-shadow: -1px 0 0 0 <?php echo $global['web_theme_color']; ?>;}
	.member-nav-group .member-nav-item:first-child .btn.selected{border-left: 1px solid <?php echo $global['web_theme_color']; ?> !important;}
	/*搜索按钮图标*/
	.flexigrid .sDiv2 .fa-search{}
        
    /* 商品订单列表标题 */
   .flexigrid .mDiv .ftitle h3 {border-left: 3px solid <?php echo $global['web_theme_color']; ?>;} 
	
    /*分页*/
    .pagination > .active > a, .pagination > .active > a:focus, 
	.pagination > .active > a:hover, 
	.pagination > .active > span, 
	.pagination > .active > span:focus, 
	.pagination > .active > span:hover { border-color: <?php echo $global['web_theme_color']; ?>;color:<?php echo $global['web_theme_color']; ?>; }
    
    .layui-form-onswitch {border-color: <?php echo $global['web_theme_color']; ?> !important;background-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-enable.selected { background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-disable.selected {background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    input[type="text"]:focus,
    input[type="text"]:hover,
    input[type="text"]:active,
    input[type="password"]:focus,
    input[type="password"]:hover,
    input[type="password"]:active,
    textarea:hover,
    textarea:focus,
    textarea:active { border-color:<?php echo hex2rgba($global['web_theme_color'],0.8); ?>;box-shadow: 0 0 0 2px <?php echo hex2rgba($global['web_theme_color'],0.15); ?> !important;}
    .input-file-show:hover .type-file-button {background-color:<?php echo $global['web_theme_color']; ?> !important; }
    .input-file-show:hover {border-color: <?php echo $global['web_theme_color']; ?> !important;box-shadow: 0 0 5px <?php echo hex2rgba($global['web_theme_color'],0.5); ?> !important;}
    .input-file-show:hover span.show a,
    .input-file-show span.show a:hover { color: <?php echo $global['web_theme_color']; ?> !important;}
    .input-file-show:hover .type-file-button {background-color: <?php echo $global['web_theme_color']; ?> !important; }
    .color_z { color: <?php echo $global['web_theme_color']; ?> !important;}
    a.imgupload{
        background-color: <?php echo $global['web_theme_color']; ?> !important;
        border-color: <?php echo $global['web_theme_color']; ?> !important;
    }
    /*专题节点按钮*/
    .ncap-form-default .special-add{background-color:<?php echo $global['web_theme_color']; ?>;border-color:<?php echo $global['web_theme_color']; ?>;}
    .ncap-form-default .special-add:hover{background-color:<?php echo $global['web_assist_color']; ?>;border-color:<?php echo $global['web_assist_color']; ?>;}
    
    /*更多功能标题*/
    .on-off_panel .title::before {background-color:<?php echo $global['web_theme_color']; ?>;}

</style>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.js"></script>
<!-- <script type="text/javascript" src="/api/public/plugins/tags_input/js/jquery.tagsinput.js?v=<?php echo $version; ?>"></script> -->
<script type="text/javascript" src="/api/public/static/admin/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/api/public/plugins/layer-v3.1.0/layer.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/admin.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/common.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/api/public/plugins/layui/layui.js"></script>
<script src="/api/public/static/admin/js/myFormValidate.js"></script>
<script src="/api/public/static/admin/js/myAjax2.js?v=<?php echo $version; ?>"></script>
<script src="/api/public/static/admin/js/global.js?v=<?php echo $version; ?>"></script>
</head>
<script src="/api/public/static/admin/js/users_upgrade.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/plugins/colpick/js/colpick.js"></script>
<link href="/api/public/plugins/colpick/css/colpick.css" rel="stylesheet" type="text/css"/>
<body class="bodystyle" style="overflow-y: scroll; cursor: default; -moz-user-select: inherit;">
<style type="text/css">
#picker {
    /*margin:0;*/
    /*padding:0;*/
    border:solid 1px <?php echo (isset($info['theme_color']) && ($info['theme_color'] !== '')?$info['theme_color']:'#ff6565'); ?>;
    width:70px;
    height:20px;
    border-right:40px solid green;
    /*line-height:20px;*/
} 
</style>
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="page">
        <div class="fixed-bar">
        <div class="item-title">
            <ul class="tab-base nc-row">
                <?php if(is_check_access('Member@users_index') == '1'): ?>
                    <li>
                        <a href="<?php echo url('Member/users_index'); ?>" <?php if(in_array(ACTION_NAME, ['users_index','level_index','attr_index','users_config'])): ?>class="current"<?php endif; ?>>
                            <span>会员列表</span>
                        </a>
                    </li>
                <?php endif; if(is_check_access('Member@money_index') == '1'): if(1 == $userConfig['pay_open']): ?>
                        <li>
                            <a href="<?php echo url('Member/money_index'); ?>" <?php if(in_array(ACTION_NAME, ['money_index', 'money_edit','media_index','upgrade_index'])): ?>class="current"<?php endif; ?>>
                                <span>订单管理</span>
                            </a>
                        </li>
                    <?php endif; endif; if(is_check_access('UsersRelease@conf') == '1'): if(1 == $userConfig['users_open_release']): ?>
                        <li>
                            <a href="<?php echo url('UsersRelease/conf'); ?>" <?php if(in_array(CONTROLLER_NAME, ['UsersRelease'])): ?>class="current"<?php endif; ?>>
                                <span>会员投稿</span>
                            </a>
                        </li>
                    <?php endif; endif; if($php_servicemeal > 1): if(is_check_access('UsersScore@conf') == '1'): ?>
                    <li>
                        <a href="<?php echo url('UsersScore/conf'); ?>" <?php if(in_array(CONTROLLER_NAME, ['UsersScore'])): ?>class="current"<?php endif; ?>>
                            <span>积分管理</span>
                        </a>
                    </li>
                    <?php endif; endif; ?>
            </ul>
        </div>
    </div>
    <div class="flexigrid">
        <div class="mDiv">
            <form class="navbar-form form-inline" action="<?php echo url('Member/level_index'); ?>" method="get" onsubmit="layer_loading('正在处理');">
                <?php echo (isset($searchform['hidden']) && ($searchform['hidden'] !== '')?$searchform['hidden']:''); ?>
            <div class="ftitle">
                     <div class="member-nav-group">
        <?php if(is_check_access('Member@users_index') == '1'): ?>
           <label class="member-nav-item">
                <input type="button" class="btn <?php if(!in_array(\think\Request::instance()->action(), ['users_index','users_add','users_edit'])): ?>current<?php else: ?>selected<?php endif; ?>" value="会员列表" onclick="window.location.href='<?php echo url("Member/users_index"); ?>';">
           </label>
        <?php endif; if(is_check_access('Member@level_index') == '1'): ?>
            <label class="member-nav-item">
                <input type="button" class="btn <?php if(!in_array(\think\Request::instance()->action(), ['level_index','level_add','level_edit'])): ?>current<?php else: ?>selected<?php endif; ?>" value="会员级别" onclick="window.location.href='<?php echo url("Member/level_index"); ?>';">
            </label>
        <?php endif; if(is_check_access('Member@attr_index') == '1'): ?>
            <label class="member-nav-item">
                <input type="button" class="btn <?php if(!in_array(\think\Request::instance()->action(), ['attr_index','attr_add','attr_edit'])): ?>current<?php else: ?>selected<?php endif; ?>" value="会员属性" onclick="window.location.href='<?php echo url("Member/attr_index"); ?>';">
            </label>
        <?php endif; if(is_check_access('Member@users_config') == '1'): ?>
            <label class="member-nav-item">
                <input type="button" class="btn <?php if(!in_array(\think\Request::instance()->action(), ['users_config'])): ?>current<?php else: ?>selected<?php endif; ?>" value="功能配置" onclick="window.location.href='<?php echo url("Member/users_config"); ?>';">
            </label>
        <?php endif; ?>
    </div>

            </div>
            </form>
        </div>
        
        <form class="form-horizontal" id="postForm" action="<?php echo url('Member/users_config'); ?>" method="post">
            <div class="hDiv htitx">
                <div class="hDivBox">
                    <table cellspacing="0" cellpadding="0" style="width: 100%">
                        <thead>
                        <tr>
                            <th class="sign w10" axis="col0">
                                <div class="tc"></div>
                            </th>
                            <th abbr="article_title" axis="col3" class="w10">
                                <div class="tc">注册设置</div>
                            </th>
                            <th abbr="ac_id" axis="col4">
                                <div class=""></div>
                            </th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>

            <!-- config/users -->
            <div class="ncap-form-default">
                <dl class="row">
                    <dt class="tit">
                        <label for="uname">会员中心</label>
                    </dt>
                    <dd class="opt">
                        <label>
                            <input type="radio" name="users[users_open_register]" value="0" <?php if(!isset($info['users_open_register']) || empty($info['users_open_register'])): ?>checked="checked"<?php endif; ?>/>开启
                        </label>
                        &nbsp;&nbsp;&nbsp;
                        <label>
                            <input type="radio" name="users[users_open_register]" value="1" <?php if($info['users_open_register'] == 1): ?>checked="checked"<?php endif; ?>/>关闭
                        </label>
                        <span class="notic2 hui">关闭则自动隐藏账户注册/登录的入口</span>
                    </dd>
                </dl>

                <dl class="row">
                    <dt class="tit">
                        <label for="uname">开启注册</label>
                    </dt>
                    <dd class="opt">
                        <label>
                            <input type="radio" name="users[users_open_reg]" value="0" <?php if(!isset($info['users_open_reg']) || empty($info['users_open_reg'])): ?>checked="checked"<?php endif; ?>/>开启
                        </label>
                        &nbsp;&nbsp;&nbsp;
                        <label>
                            <input type="radio" name="users[users_open_reg]" value="1" <?php if($info['users_open_reg'] == 1): ?>checked="checked"<?php endif; ?>/>关闭
                        </label>
                        <span class="notic2 hui">关闭则自动隐藏账户注册的入口</span>
                    </dd>
                </dl>

                <dl class="row">
                    <dt class="tit">
                        <label for="username">注册验证</label>
                    </dt>
                    <dd class="opt" style="line-height: 30px;">
                        <label>
                            <input type="radio" name="users[users_verification]" value="0" <?php if(!isset($info['users_verification']) || empty($info['users_verification'])): ?>checked="checked"<?php endif; ?>/>不验证
                            <span class="notic2 hui" style="padding-left: 23px;">注册会员后，可直接登录，无需验证</span>
                        </label>
                        <br/>
                        <label>
                            <input type="radio" name="users[users_verification]" value="1" <?php if($info['users_verification'] == 1): ?>checked="checked"<?php endif; ?>/>后台激活
                            <span class="notic2 hui">注册会员后，需后台审核激活后才能登录</span>
                        </label>
                        <br/>
                        <label>
                            <input type="radio" name="users[users_verification]" value="2" onclick="email(1);" <?php if($info['users_verification'] == 2): ?>checked="checked"<?php endif; ?>/>邮件验证
                            <span class="notic2 hui">注册会员中，发送邮箱验证码校验才能注册</span>
                        </label>
                        <br/>
                        <label>
                            <input type="radio" name="users[users_verification]" value="3" onclick="mobile(1);" <?php if($info['users_verification'] == 3): ?>checked="checked"<?php endif; ?>/>手机验证
                            <span class="notic2 hui">注册会员中，发送手机验证码校验才能注册</span>
                        </label>
                    </dd>
                </dl>

                <dl class="row">
                    <dt class="tit">
                        <label for="username">找回密码</label>
                    </dt>
                    <dd class="opt" style="line-height: 30px;">
                        <label>
                            <input type="radio" name="users[users_retrieve_password]" value="1" onclick="email(2, this);" <?php if(isset($info['users_retrieve_password']) && $info['users_retrieve_password'] == 1): ?>checked="checked"<?php endif; ?>/>邮件验证
                        </label>
                        &nbsp;&nbsp;&nbsp;
                        <label>
                            <input type="radio" name="users[users_retrieve_password]" value="2" onclick="mobile(2, this);" <?php if(isset($info['users_retrieve_password']) && $info['users_retrieve_password'] == 2): ?>checked="checked"<?php endif; ?>/>手机验证
                        </label>
                        <span class="notic2 hui">会员找回密码时的验证方法</span>
                    </dd>
                </dl>

                <dl class="row">
                    <dt class="tit">
                        <label for="username">禁止注册用户名</label>
                    </dt>
                    <dd class="opt" style="line-height: 30px;">
                        <textarea rows="5" cols="60" name="users[users_reg_notallow]" style="height:60px;"><?php echo (isset($info['users_reg_notallow']) && ($info['users_reg_notallow'] !== '')?$info['users_reg_notallow']:'www,bbs,ftp,mail,user,users,admin,administrator,eyoucms'); ?></textarea>
                        <p class="notic">前台注册时禁止注册的用户名列表，以逗号(,)分隔开</p>
                    </dd>
                </dl>
            </div>
            <!-- config/theme -->
            <div class="hDiv htitx">
                <div class="hDivBox">
                    <table cellspacing="0" cellpadding="0" style="width: 100%">
                        <thead>
                        <tr>
                            <th class="sign w10" axis="col0">
                                <div class="tc"></div>
                            </th>
                            <th abbr="article_title" axis="col3" class="w10">
                                <div class="tc">前台风格设置</div>
                            </th>
                            <th abbr="ac_id" axis="col4">
                                <div class=""></div>
                            </th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="ncap-form-default">
                <dl class="row">
                    <dt class="tit">标签调用</dt>
                    <dd class="opt">
                        <a href="javascript:void(0);" onclick="tag_call('web_users_switch');" class="ncap-btn ncap-btn-green">查看教程</a>
                    </dd>
                </dl>
                <dl class="row">
                    <dt class="tit">
                        <label for="uname"><?php if($usersTplVersion == 'v2'): ?>PC左侧菜单<?php else: ?>左侧菜单<?php endif; ?></label>
                    </dt>
                    <dd class="opt">
                        <a href="javascript:void(0);" onclick="menu_index();" class="ncap-btn ncap-btn-green">管理</a>
                    </dd>
                </dl>
                <?php if($usersTplVersion == 'v2'): ?>
                <dl class="row">
                    <dt class="tit">
                        <label for="uname">WAP底部菜单</label>
                    </dt>
                    <dd class="opt">
                        <a href="javascript:void(0);" onclick="bottom_menu_index();" class="ncap-btn ncap-btn-green">管理</a>
                    </dd>
                </dl>
                <?php endif; ?>
                <dl class="row">
                    <dt class="tit">
                        <label for="uname">主题颜色</label>
                    </dt>
                    <dd class="opt">
                        <input type="text" name="theme[theme_color]" value="<?php echo (isset($info['theme_color']) && ($info['theme_color'] !== '')?$info['theme_color']:'#ff6565'); ?>" id="picker" style="border-color: <?php echo (isset($info['theme_color']) && ($info['theme_color'] !== '')?$info['theme_color']:'#ff6565'); ?>;" />
                    </dd>
                </dl>
                <dl class="row">
                    <dt class="tit">
                        <label for="web_users_tpl_theme">模板风格</label>
                    </dt>
                    <dd class="opt">
                        <select name="web[web_users_tpl_theme]">
                            <option value="">默认风格</option>
                            <?php if(is_array($tpl_theme_list) || $tpl_theme_list instanceof \think\Collection || $tpl_theme_list instanceof \think\Paginator): $i = 0; $__LIST__ = $tpl_theme_list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;if(!in_array(($vo), explode(',',"users"))): ?>
                                <option value="<?php echo $vo; ?>" <?php if($web_users_tpl_theme == $vo): ?>selected<?php endif; ?>><?php echo $vo; ?></option>
                                <?php endif; endforeach; endif; else: echo "" ;endif; ?>
                        </select>
                        &nbsp;<a href="JavaScript:void(0);" onclick="click_to_eyou_1575506523('https://www.eyoucms.com/plus/view.php?aid=11017','如何制作可切换的会员中心模板？')" style="font-size: 12px;padding-left: 38px;position:absolute;top: 18px;">查看教程？</a>
                        <p class="notic"></p>
                    </dd>
                </dl>
            </div>

            <div class="ncap-form-default">
                <dl class="row">
                    <div class="bot">
                        <a href="JavaScript:void(0);" onclick="usersset();" class="ncap-btn-big ncap-btn-green" id="submitBtn">确认提交</a>
                    </div>
                </dl>
            </div>

        </form>
    </div>
</div>
<script>
    $(document).ready(function(){
        // 表格行点击选中切换
        $('#flexigrid > table>tbody >tr').click(function(){
            $(this).toggleClass('trSelected');
        });

        // 点击刷新数据
        $('.fa-refresh').click(function(){
            location.href = location.href;
        });

        $('input[name="oauth[oauth_open]"]').click(function(){
            var oauth_open = $(this).val();
            if (1 == oauth_open) {
                $('#oauth_list').show();
            } else {
                $('#oauth_list').hide();
            }
        });

        // 颜色选择
        $('#picker').colpick({
            flat:false,
            layout:'rgbhex',
            submit:0,
            colorScheme:'light',
            color:$('#picker').val(),
            onChange:function(hsb,hex,rgb,el,bySetColor) {
                $(el).css('border-color','#'+hex);
                // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
                if(!bySetColor) $(el).val('#'+hex);
            }
        }).keyup(function(){
            $(this).colpickSetColor('#'+this.value);
        });
    });

    function email(source, obj) {
        $.ajax({
            url: "<?php echo url('Member/ajax_users_config_email'); ?>",
            type: 'GET',
            dataType: 'JSON',
            data: {_ajax:1},
            success: function(res){
                if (res.code == 0) {
                    if (1 == source) {
                        $("input[name='users[users_verification]'][value=0]").attr("checked", "checked");
                        layer.alert(res.msg, {icon: 2, title:false});
                        return false;
                    } else {
                        $(obj).removeAttr('checked');
                        layer.alert(res.msg, {icon: 2, title:false});
                        return false;
                    }
                }
            },
            error: function(e){
                showErrorMsg(ey_unknown_error);
                $(obj).removeAttr('checked');
                return false;
            }
        });
    }

    function mobile(source, obj) {
        $.ajax({
            url: "<?php echo url('Member/ajax_users_config_mobile'); ?>",
            type: 'GET',
            dataType: 'JSON',
            data: {_ajax:1},
            success: function(res){
                if (res.code == 0) {
                    if (1 == source) {
                        $("input[name='users[users_verification]'][value=0]").attr("checked", "checked");
                        layer.alert(res.msg, {icon: 2, title:false});
                        return false;
                    } else {
                        $(obj).removeAttr('checked');
                        layer.alert(res.msg, {icon: 2, title:false});
                        return false;
                    }
                }
            },
            error: function(e){
                showErrorMsg(ey_unknown_error);
                $(obj).removeAttr('checked');
                return false;
            }
        });
    }

    function set_oauth_config(obj)
    {
        var title = $(obj).data('title');
        var oauth = $(obj).data('oauth');
        var url = "<?php echo url('Member/ajax_set_oauth_config'); ?>";
        if (url.indexOf('?') > -1) {
            url += '&';
        } else {
            url += '?';
        }
        url += 'oauth='+oauth;
        //iframe窗
        layer.open({
            type: 2,
            title: title,
            fixed: true, //不固定
            shadeClose: false,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: ['80%', '80%'],
            content: url
        });
    }

    function menu_index()
    {
        var url = "<?php echo url('Member/ajax_menu_index'); ?>";
        //iframe窗
        layer.open({
            type: 2,
            title: '前台会员中心左侧菜单',
            fixed: true, //不固定
            shadeClose: false,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: ['90%', '90%'],
            content: url
        });
    }

    function bottom_menu_index()
    {
        var url = "<?php echo url('Member/ajax_bottom_menu_index'); ?>";
        //iframe窗
        layer.open({
            type: 2,
            title: '前台会员中心手机端底部菜单',
            fixed: true, //不固定
            shadeClose: false,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: ['90%', '90%'],
            content: url
        });
    }

    function usersset(){
        layer_loading('正在处理');
        $('#postForm').submit();
    }
    function tag_call(name)
    {
        $.ajax({
            type: "POST",
            url: "<?php echo url('System/ajax_tag_call', ['_ajax'=>1]); ?>",
            data: {name:name},
            dataType: 'json',
            success: function (res) {
                if(res.code == 1){
                    //询问框
                    var confirm = layer.confirm(res.data.msg, {
                            title: false,
                            area: ['70%','80%'],
                            btn: ['关闭'] //按钮

                        }, function(){
                            layer.close(confirm);
                        }
                    );
                }else{
                    layer.alert(res.msg, {icon: 2, title:false});
                }
            },
            error:function(){
                layer.alert(ey_unknown_error, {icon: 2, title:false});
            }
        });
    }
</script>

<br/>
<div id="goTop">
    <a href="JavaScript:void(0);" id="btntop">
        <i class="fa fa-angle-up"></i>
    </a>
    <a href="JavaScript:void(0);" id="btnbottom">
        <i class="fa fa-angle-down"></i>
    </a>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#think_page_trace_open').css('z-index', 99999);
    });
</script>
</body>
</html>