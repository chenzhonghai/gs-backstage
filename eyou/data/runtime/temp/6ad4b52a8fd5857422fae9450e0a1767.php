<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:37:"./template/plugins/ask/pc/details.htm";i:1634542387;s:48:"/mnt/eyou/template/plugins/ask/pc/ask_header.htm";i:1634539380;s:47:"/mnt/eyou/template/plugins/ask/pc/hotsearch.htm";i:1634539380;s:45:"/mnt/eyou/template/plugins/ask/pc/hotpost.htm";i:1634539380;}*/ ?>
<!DOCTYPE html>
<html>
<head> 
    <title><?php echo $eyou['field']['info']['ask_title']; ?>-问答中心</title>
    <meta name="renderer" content="webkit" /> 
    <meta charset="utf-8" /> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=0,minimal-ui" /> 
    <link href="<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_cmspath"); echo $__VALUE__; ?>/favicon.ico" rel="shortcut icon" type="image/x-icon" /> 
    <?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("skin/css/eyoucms.css","","","ask"); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("skin/css/ask.css","","","ask"); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/static/common/js/jquery.min.js","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/plugins/layer-v3.1.0/layer.js","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("skin/js/ask.js","","","ask"); echo $__VALUE__; ?>
</head>

<body> 
<!-- 头部 -->
<nav class="navbar navbar-default met-nav navbar-fixed-top" role="navigation">
    <div class="ey-head">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-sm-6 logo">
                    <ul class="list-none">
                        <li><a href="<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_cmsurl"); echo $__VALUE__; ?>" class="ey-logo"><img src="<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_logo"); echo $__VALUE__; ?>" style="max-height: 60px;" /></a></li>
                        <li>问答中心</li>
                    </ul>
                </div>
                <div class="col-xs-6 col-sm-6 user-info">
                    <ol class="breadcrumb pull-right">
                        <?php if($users['users_id'] == '0'): ?>
                        <li><a href="<?php echo url("user/Users/login","",true,false,null,null,null);?>" title="登录">登录</a></li>
                        <li><a href="<?php echo url("user/Users/reg","",true,false,null,null,null);?>" title="注册">注册</a></li>
                        <?php else: ?>
                        <li><a href="<?php echo url("user/Users/centre","",true,false,null,null,null);?>"><?php echo $users['nickname']; ?></a></li>
                        <li><a href="<?php echo url("user/Users/logout","",true,false,null,null,null);?>" title="退出登录">退出登录</a></li>
                        <?php endif; ?>
                    </ol>
                    <button type="button" class="hamburger animated fadeInLeft is-closed" data-toggle="offcanvas">
                    <span class="hamb-top"></span>
                    <span class="hamb-middle"></span>
                    <span class="hamb-bottom"></span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</nav>
<!-- END -->
<main class="mian-body container">
    <aside class="aside-left col-md-9">
        <!-- 栏目导航、搜索 -->
        <?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("skin/js/bootstrap-hover-dropdown.min.js","","","ask"); echo $__VALUE__; ?>
<div class="sbox">
    <!-- 搜索 -->
    <div class="search">
        <form action="" method="get" name="search" id="search" onsubmit="return formSubmit();">
            <input type="hidden" name="ct" value="search">
            <input class="searchinput" name="q" placeholder="输入问题关键字" autocomplete="off" value="<?php echo $eyou['field']['SearchName']; ?>">
            <span class="search-icon" id="search-bottom" onclick="formSubmit();"></span>
            <div class="search-result">
            </div>
            <div class="hot-list" style="display: none;" id="search_keywords_list">
            </div>
        </form>
    </div>
    <!-- END -->

    <!-- 栏目导航 -->
    <ul class="nav navbar-nav navbar-right navlist" id="asktypes">
        <li class="nav-item">
            <a href="<?php echo $eyou['field']['NewDateUrl']; ?>" title="问答首页" class="link <?php if($eyou['field']['IsTypeId'] == '0'): ?>active<?php endif; ?>">问答首页</a>
        </li>

        <?php if(is_array($eyou['field']['TypeData']) || $eyou['field']['TypeData'] instanceof \think\Collection || $eyou['field']['TypeData'] instanceof \think\Paginator): $i = 0; $e = 1; $__LIST__ = $eyou['field']['TypeData'];if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$vo): $i= intval($key) + 1;$mod = ($i % 2 ); ?>
        <li class="margin-left-0 nav-item">
            <a class="link <?php if($eyou['field']['IsTypeId'] == $vo['type_id']): ?>active<?php endif; ?>" href="<?php echo $vo['Url']; ?>" data-hover="dropdown" aria-expanded="false"><?php echo $vo['type_name']; if(!(empty($vo['SubType']) || (($vo['SubType'] instanceof \think\Collection || $vo['SubType'] instanceof \think\Paginator ) && $vo['SubType']->isEmpty()))): ?>
                    <span class="caret fa-angle-down"></span>
                <?php endif; ?>
            </a>
            <?php if(!(empty($vo['SubType']) || (($vo['SubType'] instanceof \think\Collection || $vo['SubType'] instanceof \think\Paginator ) && $vo['SubType']->isEmpty()))): ?>
                <ul class="dropdown-menu dropdown-menu-right bullet" role="menu">
                    <?php if(is_array($vo['SubType']) || $vo['SubType'] instanceof \think\Collection || $vo['SubType'] instanceof \think\Paginator): $i = 0; $e = 1; $__LIST__ = $vo['SubType'];if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$st): $i= intval($key) + 1;$mod = ($i % 2 ); ?>
                        <li><a href="<?php echo $st['Url']; ?>"><?php echo $st['type_name']; ?></a></li>
                    <?php echo isset($st["ey_1563185380"])?$st["ey_1563185380"]:""; ?><?php echo (1 == $e && isset($st["ey_1563185376"]))?$st["ey_1563185376"]:""; ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $st = []; ?>
                </ul>
            <?php endif; ?>
        </li>
        <?php echo isset($vo["ey_1563185380"])?$vo["ey_1563185380"]:""; ?><?php echo (1 == $e && isset($vo["ey_1563185376"]))?$vo["ey_1563185376"]:""; ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $vo = []; ?>
    </ul>
    <!-- END -->
    <div class="clearfix"></div>
</div>
<div class=" blank10"></div>

<script type="text/javascript" charset="utf-8">
    $(function(){ 
        $('#search input[name=q]').click(function(){
            if ($('#search input[name=q]').val() == '') {
                $('#hot_keywords_list').show();
            }
        });
    }); 

    // 搜索提交
    function formSubmit()
    {
        var q = $('#search input[name=q]').val();
        var url = '<?php echo $eyou['field']['NewDateUrl']; ?>';
        if (q.length > 0) {
            url = url + '&search_name=' + q;
        }
        window.location.href = url;
        return false;
    }
</script>
        <!-- END -->

        <!-- 问题详情 -->
        <div class="wenda">
            <h3 class="wt-head <?php if($eyou['field']['info']['status'] == '1'): ?>solved<?php else: ?>no_solve<?php endif; ?>"><?php echo $eyou['field']['info']['ask_title']; ?></h3>
            <div class="wt-desc">
                <?php echo $eyou['field']['info']['content']; ?>
            </div>
            <div class="wt-bucong">
                <div class="wt-opt">
                    <span>
                        <?php echo $eyou['field']['info']['add_time']; ?>
                            <span style="margin:0 5px"> | </span>
                        <?php echo $eyou['field']['info']['click']; ?>人阅读
                        <a href="javascript:void(0);" id="ey_1595557091" onclick="ey_1595557091(this);" data-html="已收藏">加入收藏</a>
                        （收藏数：<span id="ey_collection_num"></span>次）
                        <?php echo hookexec('Collection/Collection/show'); if(!(empty($users['admin_id']) || (($users['admin_id'] instanceof \think\Collection || $users['admin_id'] instanceof \think\Paginator ) && $users['admin_id']->isEmpty()))): ?>
                            <span style="margin:0 5px"> | </span>
                            <a href="<?php echo $eyou['field']['EditAskUrl']; ?>">编辑</a>
                            <span style="margin:0 5px"> | </span>
                            <a data-url="<?php echo $eyou['field']['DelAnswerUrl']; ?>" onclick="DataDel(this, '<?php echo $eyou['field']['info']['ask_id']; ?>', 1)" class="secend-huifu-btn">删除</a>
                        <?php else: if(!(empty($eyou['field']['IsUsers']) || (($eyou['field']['IsUsers'] instanceof \think\Collection || $eyou['field']['IsUsers'] instanceof \think\Paginator ) && $eyou['field']['IsUsers']->isEmpty()))): ?>
                                <span style="margin:0 5px"> | </span>
                                <a href="<?php echo $eyou['field']['EditAskUrl']; ?>">编辑</a>
                            <?php endif; endif; if($AdminParentId == '0'): ?>
                            <!-- AdminParentId=0表示创始人 -->
                            <?php if($eyou['field']['info']['is_review'] == '0'): ?>
                                <!-- is_review=0表示未审核 -->
                                <span id='<?php echo $eyou['field']['info']['ask_id']; ?>_Ask'>
                                    <span style="margin:0 10px"> | </span>
                                    <span data-url='<?php echo $eyou['field']['ReviewAskUrl']; ?>' onclick="ReviewAsk(this, '<?php echo $eyou['field']['info']['ask_id']; ?>');" class="secend-huifu-btn" style="cursor: pointer; color: red;" title="该问题未审核，可点击审核，仅创始人可操作">审核</span>
                                </span>
                            <?php endif; endif; ?>
                    </span>
                </div>
            </div>
        </div>
        <!-- END -->

        <!-- 最佳答案 -->
        <?php if(!(empty($eyou['field']['BestAnswer']) || (($eyou['field']['BestAnswer'] instanceof \think\Collection || $eyou['field']['BestAnswer'] instanceof \think\Paginator ) && $eyou['field']['BestAnswer']->isEmpty()))): ?>
        <div class="wenti-huifu">
            <div class="huifu-head"><div><strong>最佳答案</strong></div></div>
            <ul class="huifu-list" id="BestAnswer">
                <div>
                    <?php if(is_array($eyou['field']['BestAnswer']) || $eyou['field']['BestAnswer'] instanceof \think\Collection || $eyou['field']['BestAnswer'] instanceof \think\Paginator): $i = 0; $e = 1; $__LIST__ = $eyou['field']['BestAnswer'];if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$vo): $i= intval($key) + 1;$mod = ($i % 2 ); ?>
                    <li class="huifu-li" id="ul_div_li_<?php echo $vo['answer_id']; ?>">
                        <div class="huifu-user">
                            <a class="user_card" href="javascript:void(0);">
                                <img src="<?php echo $vo['head_pic']; ?>" style="width:40px;height:40px;border-radius:100%">
                            </a>
                            <a href="javascript:void(0);">
                                <div class="user-nice">
                                    <p class="nice" style="line-height: initial;margin-bottom: 7px"><?php echo $vo['username']; ?></p>
                                    <p class="jianjie" style="margin-top: -5px"></p>
                                </div>
                            </a>
                            <div style="flex-grow:1"></div>
                            <div class="dianzan-bnt" data-url="<?php echo $eyou['field']['ClickLikeUrl']; ?>" data-is_like='' onclick="ClickLike(this, '<?php echo $vo['ask_id']; ?>', '<?php echo $vo['answer_id']; ?>')">
                                <span id="click_like_<?php echo $vo['answer_id']; ?>"><?php echo (isset($vo['AnswerLike']['LikeNum']) && ($vo['AnswerLike']['LikeNum'] !== '')?$vo['AnswerLike']['LikeNum']:'0'); ?></span>
                                <i class="fa fa-thumbs-up" style="margin-left:5px;"></i>
                            </div>
                        </div>
                        <div class="weiguan-people" id="click_like_name_<?php echo $vo['answer_id']; ?>">
                            <span id="click_like_span_<?php echo $vo['answer_id']; ?>">
                                <?php echo $vo['AnswerLike']['LikeName']; ?>
                            </span>
                            <span id="is_show_<?php echo $vo['answer_id']; ?>" <?php if(empty($vo['AnswerLike']['LikeNum']) || (($vo['AnswerLike']['LikeNum'] instanceof \think\Collection || $vo['AnswerLike']['LikeNum'] instanceof \think\Paginator ) && $vo['AnswerLike']['LikeNum']->isEmpty())): ?> style="display: none;" <?php endif; ?>>
                                等<font id="askanswer_click_like_<?php echo $vo['answer_id']; ?>"><?php echo $vo['AnswerLike']['LikeNum']; ?></font>人赞同
                            </span>
                        </div>
                        <div class="huifu-text-box">
                            <div class="huifu-content resetEditer">
                                <?php echo $vo['content']; ?>
                            </div>
                            <div class="huifu-time">
                                <span><?php echo $vo['add_time']; ?></span>
                                <?php if(!(empty($users['admin_id']) || (($users['admin_id'] instanceof \think\Collection || $users['admin_id'] instanceof \think\Paginator ) && $users['admin_id']->isEmpty()))): ?>
                                    <span style="float:right">
                                        <a class="a" href="<?php echo $eyou['field']['EditAnswer']; ?>&answer_id=<?php echo $vo['answer_id']; ?>">编辑</a>
                                        <span style="margin:0 5px;">|</span>
                                        <a class="a" data-url="<?php echo $eyou['field']['DelAnswerUrl']; ?>" onclick="DataDel(this, '<?php echo $vo['answer_id']; ?>', 3)">删除</a>
                                    </span>
                                <?php else: if($vo['users_id'] == $users['users_id']): ?>
                                        <span style="float:right">
                                            <a class="a" href="<?php echo $eyou['field']['EditAnswer']; ?>&answer_id=<?php echo $vo['answer_id']; ?>">编辑</a>
                                        </span>
                                    <?php endif; endif; ?>
                            </div>
                            <div class="secend-huifu">
                                <ul class="secend-ul">
                                    <div id="<?php echo $vo['answer_id']; ?>_ReplyContainer">
                                        <?php if(is_array($vo['AnswerSubData']) || $vo['AnswerSubData'] instanceof \think\Collection || $vo['AnswerSubData'] instanceof \think\Paginator): $i = 0; $e = 1; $__LIST__ = $vo['AnswerSubData'];if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$vo_sub): $i= intval($key) + 1;$mod = ($i % 2 ); ?>
                                        <li class="secend-li" id="<?php echo $vo_sub['answer_id']; ?>_answer_li">
                                            <div class="head-secend">
                                                <a href="javascript:void(0);">
                                                    <img src="<?php echo $vo_sub['head_pic']; ?>" style="width:30px;height:30px;border-radius:100%;margin-right: 16px;">
                                                </a>
                                                <strong><?php echo $vo_sub['username']; ?></strong>
                                                <span style="margin:0 10px"> | </span>
                                                <span><?php echo $vo_sub['add_time']; ?></span>
                                                <div style="flex-grow:1"></div>

                                                <?php if($AdminParentId == '0'): ?>
                                                    <!-- AdminParentId=0表示创始人 -->
                                                    <?php if($vo_sub['is_review'] == '0'): ?>
                                                        <!-- is_review=0表示未审核 -->
                                                        <span id='<?php echo $vo_sub['answer_id']; ?>_Review'>
                                                            <span data-url='<?php echo $eyou['field']['ReviewCommentUrl']; ?>' onclick="Review(this, '<?php echo $vo_sub['answer_id']; ?>', 0)" class="secend-huifu-btn" style="cursor: pointer; color: red;" title="该评论未审核，可点击审核，仅创始人可操作">审核</span>
                                                            <span style="margin:0 10px"> | </span>
                                                        </span>
                                                    <?php endif; endif; ?>

                                                <span onclick="replyUser('<?php echo $vo_sub['answer_pid']; ?>','<?php echo $vo_sub['users_id']; ?>','<?php echo $vo_sub['username']; ?>','<?php echo $vo_sub['answer_id']; ?>')" class="secend-huifu-btn" style="cursor: pointer;">回复</span>
                                                <?php if(!(empty($users['admin_id']) || (($users['admin_id'] instanceof \think\Collection || $users['admin_id'] instanceof \think\Paginator ) && $users['admin_id']->isEmpty()))): ?>
                                                    &nbsp;&nbsp;|&nbsp;&nbsp;
                                                    <a data-url="<?php echo $eyou['field']['DelAnswerUrl']; ?>" onclick="DataDel(this, '<?php echo $vo_sub['answer_id']; ?>', 2)" class="secend-huifu-btn" style="cursor: pointer; color:red;">删除</a>
                                                <?php else: if($vo_sub['users_id'] == $users['users_id']): ?>
                                                    &nbsp;&nbsp;|&nbsp;&nbsp;
                                                    <a data-url="<?php echo $eyou['field']['DelAnswerUrl']; ?>" onclick="DataDel(this, '<?php echo $vo_sub['answer_id']; ?>', 2)" class="secend-huifu-btn" style="cursor: pointer; color:red;">删除</a>
                                                    <?php endif; endif; ?>
                                            </div>
                                            <div class="secend-huifu-text">
                                                <?php if(empty($vo_sub['at_users_id']) || (($vo_sub['at_users_id'] instanceof \think\Collection || $vo_sub['at_users_id'] instanceof \think\Paginator ) && $vo_sub['at_users_id']->isEmpty())): ?>
                                                    <?php echo $vo_sub['content']; else: ?>
                                                    回复 @<?php echo $vo_sub['at_usersname']; ?>: &nbsp; <?php echo $vo_sub['content']; endif; ?>
                                            </div>
                                        </li>
                                        <?php echo isset($vo_sub["ey_1563185380"])?$vo_sub["ey_1563185380"]:""; ?><?php echo (1 == $e && isset($vo_sub["ey_1563185376"]))?$vo_sub["ey_1563185376"]:""; ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $vo_sub = []; ?>
                                    </div>

                                    <!--分页 begin-->
                                    <a href="javascript:void(0);" data-url="<?php echo $eyou['field']['ShowCommentUrl']; ?>" onclick="ShowComment(this, '<?php echo $vo['answer_id']; ?>', 0)" data-firstRow="<?php echo $eyou['field']['firstRow']; ?>" data-listRows="<?php echo $eyou['field']['listRows']; ?>" <?php if(empty($vo['AnswerSubData']) || (($vo['AnswerSubData'] instanceof \think\Collection || $vo['AnswerSubData'] instanceof \think\Paginator ) && $vo['AnswerSubData']->isEmpty())): ?>style="display: none;"<?php endif; ?>>查看更多</a>
                                    <!--分页 end-->

                                    <div class="huifu-area">
                                        <div id="<?php echo $vo['answer_id']; ?>_init_ta">
                                            <div class="pro_rela">
                                                <textarea style="height: 42px" onfocus="initReply('<?php echo $vo['answer_id']; ?>');" id="<?php echo $vo['answer_id']; ?>_contentInput" name="content" rows="1" class="pl_text" maxlength="300" deftips="^-^我来说两句......" placeholder="^-^我来说两句......"></textarea>
                                                <?php if(empty($users['users_id']) || (($users['users_id'] instanceof \think\Collection || $users['users_id'] instanceof \think\Paginator ) && $users['users_id']->isEmpty())): ?>
                                                <p class="login_tips1">请先
                                                    <a class="org_font" href="<?php echo url('user/Users/login'); ?>">登录</a>
                                                    <em>·</em>
                                                    <a class="org_font" href="<?php echo url('user/Users/reg'); ?>">注册</a>
                                                </p>
                                                <?php endif; ?>
                                            </div>
                                        </div>

                                        <form id="<?php echo $vo['answer_id']; ?>_replyForm">
                                            <input type="hidden" id="answer_id" value="<?php echo $vo['answer_id']; ?>" name="answer_id">
                                            <input type="hidden" name="type_id" value="<?php echo $eyou['field']['info']['type_id']; ?>">
                                            <input type="hidden" id="<?php echo $vo['answer_id']; ?>_at_users_id" value="" name="at_users_id">
                                            <input type="hidden" id="<?php echo $vo['answer_id']; ?>_at_answer_id" value="" name="at_answer_id">
                                            <div id="<?php echo $vo['answer_id']; ?>_ta" style="display: none;">
                                                <p>
                                                    <textarea sdtyle="height: 70px" id="<?php echo $vo['answer_id']; ?>_i_contentInput" name="content" rows="3" class="pl_text" onfocus="focusTextArea(this);" onblur="blurTextArea(this);" onkeyup="dealInputContentAndSize(this);" maxlength="300" deftips="^-^我来说两句......" placeholder="^-^我来说两句......"></textarea>
                                                </p>
                                                <p class="mar_t10 text_r">
                                                    <span class="jiu_font  float_l" id="<?php echo $vo['answer_id']; ?>_errorMsg" style="color: red;"></span>
                                                    <a href="javascript:void(0);" class="mar_r10" onclick="unReplyUser('<?php echo $vo['answer_id']; ?>');">取消</a>
                                                    <span value="评论" class="editreplaybtn" data-url="<?php echo $eyou['field']['AddAnswerUrl']; ?>" onclick="reply('<?php echo $vo['answer_id']; ?>', this);" style="cursor: pointer">评论</span>
                                                </p>
                                            </div>
                                        </form>
                                    </div>
                                </ul>
                            </div>
                        <input type="hidden" id="<?php echo $vo['answer_id']; ?>_page" value="1">
                    </li>
                    <?php echo isset($vo["ey_1563185380"])?$vo["ey_1563185380"]:""; ?><?php echo (1 == $e && isset($vo["ey_1563185376"]))?$vo["ey_1563185376"]:""; ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $vo = []; ?>
                </div>
            </ul>
        </div>
        <?php endif; ?>
        <!-- END -->

        <!-- 回答 -->
        <?php if(empty($eyou['field']['AnswerData']) || (($eyou['field']['AnswerData'] instanceof \think\Collection || $eyou['field']['AnswerData'] instanceof \think\Paginator ) && $eyou['field']['AnswerData']->isEmpty())): ?>
            <div class="huifu-edit">
                <div class="huifu-null" > <img src="/template/plugins/ask/pc/skin/images/kong_pic2.png" alt="">
                    <p style="margin-top: 10px;">暂时还没回答，等你发挥</p>
                </div>
            </div>
        <?php else: ?>
            <div class="wenti-huifu" id='comment'>
                <div class="huifu-head">
                    <div>
                        <strong>回答</strong>
                        <span style="margin:0 10px;color:#E5E5E5">|</span>
                        <span style="font-size: 14px;color:#999">共
                            <span id="detail_comment_count_DIV"><?php echo $eyou['field']['AnswerCount']; ?></span> 个
                        </span>
                    </div>
                    <div class="hf-head-flr">
                        <span class="hf-head-flr-span" id="orderName" style="color:#406080;font-size:14px;cursor: pointer;">
                            <a href="javascript:void(0);" data-url="<?php echo $eyou['field']['AnswerLikeNum']; ?>" data-sort_order='<?php echo $eyou['field']['SortOrder']; ?>' onclick="AnswerLike(this);">
                                按点赞量排序
                            </a>
                        </span>
                        <i class="icon iconfont icon-down" style="font-size: 12px;color: #999;display: inline-block;margin-left: 5px;"></i>
                    </div>
                </div>

                <ul class="huifu-list" id="AnswerData">
                    <div>
                        <?php if(is_array($eyou['field']['AnswerData']) || $eyou['field']['AnswerData'] instanceof \think\Collection || $eyou['field']['AnswerData'] instanceof \think\Paginator): $i = 0; $e = 1; $__LIST__ = $eyou['field']['AnswerData'];if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$vo): $i= intval($key) + 1;$mod = ($i % 2 ); ?>
                        <li class="huifu-li" id="ul_div_li_<?php echo $vo['answer_id']; ?>">
                            <div class="huifu-user">
                                <a class="user_card" href="javascript:void(0);">
                                    <img src="<?php echo $vo['head_pic']; ?>" style="width:40px;height:40px;border-radius:100%">
                                </a>
                                <a href="javascript:void(0);">
                                    <div class="user-nice">
                                        <p class="nice" style="line-height: initial;margin-bottom: 7px"><?php echo $vo['username']; ?></p>
                                        <p class="jianjie" style="margin-top: -5px"></p>
                                    </div>
                                </a>
                                <div style="flex-grow:1"></div>
                                <div class="dianzan-bnt" data-url="<?php echo $eyou['field']['ClickLikeUrl']; ?>" data-is_like='' onclick="ClickLike(this, '<?php echo $vo['ask_id']; ?>', '<?php echo $vo['answer_id']; ?>')">
                                    <span id="click_like_<?php echo $vo['answer_id']; ?>"><?php echo (isset($vo['AnswerLike']['LikeNum']) && ($vo['AnswerLike']['LikeNum'] !== '')?$vo['AnswerLike']['LikeNum']:'0'); ?></span>
                                    <i class="fa fa-thumbs-up" style="margin-left:5px;"></i>
                                </div>
                            </div>
                            <div class="weiguan-people" id="click_like_name_<?php echo $vo['answer_id']; ?>">
                                <span id="click_like_span_<?php echo $vo['answer_id']; ?>">
                                    <?php echo $vo['AnswerLike']['LikeName']; ?>
                                </span>
                                <span id="is_show_<?php echo $vo['answer_id']; ?>" <?php if(empty($vo['AnswerLike']['LikeNum']) || (($vo['AnswerLike']['LikeNum'] instanceof \think\Collection || $vo['AnswerLike']['LikeNum'] instanceof \think\Paginator ) && $vo['AnswerLike']['LikeNum']->isEmpty())): ?> style="display: none;" <?php endif; ?>>
                                    等<font id="askanswer_click_like_<?php echo $vo['answer_id']; ?>"><?php echo $vo['AnswerLike']['LikeNum']; ?></font>人赞同
                                </span>
                            </div>
                            <div class="huifu-text-box">
                                <div class="huifu-content resetEditer">
                                    <?php echo $vo['content']; ?>
                                </div>
                                <div class="huifu-time">
                                    <span><?php echo $vo['add_time']; ?></span>
                                    <?php if(!(empty($users['admin_id']) || (($users['admin_id'] instanceof \think\Collection || $users['admin_id'] instanceof \think\Paginator ) && $users['admin_id']->isEmpty()))): ?>
                                        <span style="float:right">
                                            <?php if($AdminParentId == '0'): ?>
                                                <!-- AdminParentId=0表示创始人 -->
                                                <?php if($vo['is_review'] == '0'): ?>
                                                    <!-- is_review=0表示未审核 -->
                                                    <span id='<?php echo $vo['answer_id']; ?>_Review'>
                                                        <span data-url='<?php echo $eyou['field']['ReviewCommentUrl']; ?>' onclick="Review(this, '<?php echo $vo['answer_id']; ?>', 1)" class="secend-huifu-btn" style="cursor: pointer; color: red;" title="该评论未审核，可点击审核，仅创始人可操作">审核</span>
                                                        <span style="margin:0 5px"> | </span>
                                                    </span>
                                                <?php endif; endif; ?>
                                            <a class="a" href="<?php echo $eyou['field']['EditAnswer']; ?>&answer_id=<?php echo $vo['answer_id']; ?>">编辑</a>
                                            <span style="margin:0 5px;">|</span>
                                            <a class="a" data-url="<?php echo $eyou['field']['DelAnswerUrl']; ?>" onclick="DataDel(this, '<?php echo $vo['answer_id']; ?>', 3)">删除</a>
                                            <span id='<?php echo $vo['answer_id']; ?>_Best' <?php if($vo['is_review'] == '0'): ?>style="display: none;"<?php endif; ?>>
                                                <span style="margin:0 5px;">|</span>
                                                <a class="a" data-url="<?php echo $eyou['field']['BestAnswerUrl']; ?>" style="color: red" onclick="BestAnswer(this, '<?php echo $vo['answer_id']; ?>', '0')">采纳最佳答案</a>
                                            </span>
                                        </span>
                                    <?php else: if($eyou['field']['info']['users_id'] == $users['users_id']): ?>
                                            <span style="float:right">
                                                <span style="margin:0 5px;">|</span>
                                                <a class="a" data-url="<?php echo $eyou['field']['BestAnswerUrl']; ?>" style="color: red" onclick="BestAnswer(this, '<?php echo $vo['answer_id']; ?>', '<?php echo $eyou['field']['info']['users_id']; ?>')">采纳最佳答案</a>
                                            </span>
                                        <?php endif; if($vo['users_id'] == $users['users_id']): ?>
                                            <span style="float:right">
                                                <a class="a" href="<?php echo $eyou['field']['EditAnswer']; ?>&answer_id=<?php echo $vo['answer_id']; ?>">编辑</a>
                                            </span>
                                        <?php endif; endif; ?>
                                </div>
                                <div class="secend-huifu">
                                    <ul class="secend-ul">
                                        <div id="<?php echo $vo['answer_id']; ?>_ReplyContainer">
                                            <?php if(is_array($vo['AnswerSubData']) || $vo['AnswerSubData'] instanceof \think\Collection || $vo['AnswerSubData'] instanceof \think\Paginator): $i = 0; $e = 1; $__LIST__ = $vo['AnswerSubData'];if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$vo_sub): $i= intval($key) + 1;$mod = ($i % 2 ); ?>
                                            <li class="secend-li" id="<?php echo $vo_sub['answer_id']; ?>_answer_li">
                                                <div class="head-secend">
                                                    <a href="javascript:void(0);">
                                                        <img src="<?php echo $vo_sub['head_pic']; ?>" style="width:30px;height:30px;border-radius:100%;margin-right: 16px;">
                                                    </a>
                                                    <strong><?php echo $vo_sub['username']; ?></strong>
                                                    <span style="margin:0 10px"> | </span>
                                                    <span><?php echo $vo_sub['add_time']; ?></span>
                                                    <div style="flex-grow:1"></div>

                                                    <?php if($AdminParentId == '0'): ?>
                                                        <!-- AdminParentId=0表示创始人 -->
                                                        <?php if($vo_sub['is_review'] == '0'): ?>
                                                            <!-- is_review=0表示未审核 -->
                                                            <span id='<?php echo $vo_sub['answer_id']; ?>_Review'>
                                                                <span data-url='<?php echo $eyou['field']['ReviewCommentUrl']; ?>' onclick="Review(this, '<?php echo $vo_sub['answer_id']; ?>', 0)" class="secend-huifu-btn" style="cursor: pointer; color: red;" title="该评论未审核，可点击审核，仅创始人可操作">审核</span>
                                                                <span style="margin:0 10px"> | </span>
                                                            </span>
                                                        <?php endif; endif; ?>

                                                    <span onclick="replyUser('<?php echo $vo_sub['answer_pid']; ?>','<?php echo $vo_sub['users_id']; ?>','<?php echo $vo_sub['username']; ?>','<?php echo $vo_sub['answer_id']; ?>')" class="secend-huifu-btn" style="cursor: pointer;">回复</span>
                                                    <?php if(!(empty($users['admin_id']) || (($users['admin_id'] instanceof \think\Collection || $users['admin_id'] instanceof \think\Paginator ) && $users['admin_id']->isEmpty()))): ?>
                                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                                        <a data-url="<?php echo $eyou['field']['DelAnswerUrl']; ?>" onclick="DataDel(this, '<?php echo $vo_sub['answer_id']; ?>', 2)" class="secend-huifu-btn" style="cursor: pointer; color:red;">删除</a>
                                                    <?php else: if($vo_sub['users_id'] == $users['users_id']): ?>
                                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                                        <a data-url="<?php echo $eyou['field']['DelAnswerUrl']; ?>" onclick="DataDel(this, '<?php echo $vo_sub['answer_id']; ?>', 2)" class="secend-huifu-btn" style="cursor: pointer; color:red;">删除</a>
                                                        <?php endif; endif; ?>
                                                </div>
                                                <div class="secend-huifu-text">
                                                    <?php if(empty($vo_sub['at_users_id']) || (($vo_sub['at_users_id'] instanceof \think\Collection || $vo_sub['at_users_id'] instanceof \think\Paginator ) && $vo_sub['at_users_id']->isEmpty())): ?>
                                                        <?php echo $vo_sub['content']; else: ?>
                                                        回复 @<?php echo $vo_sub['at_usersname']; ?>: &nbsp; <?php echo $vo_sub['content']; endif; ?>
                                                </div>
                                            </li>
                                            <?php echo isset($vo_sub["ey_1563185380"])?$vo_sub["ey_1563185380"]:""; ?><?php echo (1 == $e && isset($vo_sub["ey_1563185376"]))?$vo_sub["ey_1563185376"]:""; ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $vo_sub = []; ?>
                                        </div>
                                        
                                        <!--分页 begin-->
                                        <a href="javascript:void(0);" data-url="<?php echo $eyou['field']['ShowCommentUrl']; ?>" onclick="ShowComment(this, '<?php echo $vo['answer_id']; ?>', 1)" data-firstRow="<?php echo $eyou['field']['firstRow']; ?>" data-listRows="<?php echo $eyou['field']['listRows']; ?>" <?php if(empty($vo['AnswerSubData']) || (($vo['AnswerSubData'] instanceof \think\Collection || $vo['AnswerSubData'] instanceof \think\Paginator ) && $vo['AnswerSubData']->isEmpty())): ?>style="display: none;"<?php endif; ?>>查看更多</a>
                                        <!--分页 end-->

                                        <div class="huifu-area">
                                            <div id="<?php echo $vo['answer_id']; ?>_init_ta">
                                                <div class="pro_rela">
                                                    <textarea style="height: 42px" onfocus="initReply('<?php echo $vo['answer_id']; ?>');" id="<?php echo $vo['answer_id']; ?>_contentInput" name="content" rows="1" class="pl_text" maxlength="300" deftips="^-^我来说两句......" placeholder="^-^我来说两句......"></textarea>
                                                    <?php if(empty($users['users_id']) || (($users['users_id'] instanceof \think\Collection || $users['users_id'] instanceof \think\Paginator ) && $users['users_id']->isEmpty())): ?>
                                                    <p class="login_tips1">请先
                                                        <a class="org_font" href="<?php echo url('user/Users/login'); ?>">登录</a>
                                                        <em>·</em>
                                                        <a class="org_font" href="<?php echo url('user/Users/reg'); ?>">注册</a>
                                                    </p>
                                                    <?php endif; ?>
                                                </div>
                                            </div>

                                            <form id="<?php echo $vo['answer_id']; ?>_replyForm">
                                                <input type="hidden" id="answer_id" value="<?php echo $vo['answer_id']; ?>" name="answer_id">
                                                <input type="hidden" name="type_id" value="<?php echo $eyou['field']['info']['type_id']; ?>">
                                                <input type="hidden" id="<?php echo $vo['answer_id']; ?>_at_users_id" value="" name="at_users_id">
                                                <input type="hidden" id="<?php echo $vo['answer_id']; ?>_at_answer_id" value="" name="at_answer_id">
                                                <div id="<?php echo $vo['answer_id']; ?>_ta" style="display: none;">
                                                    <p>
                                                        <textarea sdtyle="height: 70px" id="<?php echo $vo['answer_id']; ?>_i_contentInput" name="content" rows="3" class="pl_text" onfocus="focusTextArea(this);" onblur="blurTextArea(this);" onkeyup="dealInputContentAndSize(this);" maxlength="300" deftips="^-^我来说两句......" placeholder="^-^我来说两句......"></textarea>
                                                    </p>
                                                    <p class="mar_t10 text_r">
                                                        <span class="jiu_font  float_l" id="<?php echo $vo['answer_id']; ?>_errorMsg" style="color: red;"></span>
                                                        <a href="javascript:;" class="mar_r10" onclick="unReplyUser('<?php echo $vo['answer_id']; ?>');">取消</a>
                                                        <span value="评论" class="editreplaybtn" data-url="<?php echo $eyou['field']['AddAnswerUrl']; ?>" onclick="reply('<?php echo $vo['answer_id']; ?>', this);" style="cursor: pointer">评论</span>
                                                    </p>
                                                </div>
                                            </form>
                                        </div>
                                    </ul>
                                </div>
                            <input type="hidden" id="<?php echo $vo['answer_id']; ?>_page" value="1">
                        </li>
                        <?php echo isset($vo["ey_1563185380"])?$vo["ey_1563185380"]:""; ?><?php echo (1 == $e && isset($vo["ey_1563185376"]))?$vo["ey_1563185376"]:""; ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $vo = []; ?>
                    </div>
                </ul>
            </div>
        <?php endif; ?>
            <div class="huifu-edit" id='hedit'>
                <div class="huifu-user">
                    <a href="javascript:void(0);">
                        <?php if(empty($users['users_id']) || (($users['users_id'] instanceof \think\Collection || $users['users_id'] instanceof \think\Paginator ) && $users['users_id']->isEmpty())): ?>
                            <img src="/template/plugins/ask/pc/skin/images/kong_pic2.png" style="width:40px;height:40px;border-radius:100%">
                        <?php else: ?>
                            <img src="<?php echo $users['head_pic']; ?>" style="width:40px;height:40px;border-radius:100%">
                        <?php endif; ?>
                    </a>
                    <div class="user-nice">
                        <?php if(empty($users['users_id']) || (($users['users_id'] instanceof \think\Collection || $users['users_id'] instanceof \think\Paginator ) && $users['users_id']->isEmpty())): ?>
                            <p class="nice">游客</p>
                        <?php else: ?>
                            <p class="nice"><?php echo $users['nickname']; ?></p>
                        <?php endif; ?>
                    </div>
                </div>
                <form id="commentForm" method="post" action="<?php echo $eyou['field']['AddAnswerUrl']; ?>">
                    <input type="hidden" name="m" value="plugins">
                    <input type="hidden" name="c" value="Ask">
                    <input type="hidden" name="a" value="ajax_add_answer">
                    <input type="hidden" name="type_id" value="<?php echo $eyou['field']['info']['type_id']; ?>">

                    <?php if(empty($users['users_id']) || (($users['users_id'] instanceof \think\Collection || $users['users_id'] instanceof \think\Paginator ) && $users['users_id']->isEmpty())): ?>
                    <div id="container" class="edui-default" style="">
                        <div id="edui1" class="edui-editor edui-default" style="text-indent: 0.5em; height:40px; z-index: 5; border:1px solid #ff6600; padding-top: 6px;">^-^我来说两句，请先
                            <a href="<?php echo url('user/Users/login'); ?>" style="color: red;">登录</a>
                            <em>·</em>
                            <a href="<?php echo url('user/Users/reg'); ?>" style="color: red;">注册</a>
                        </div>
                    </div>
                    <?php else: ?>
                    <script type="text/javascript">
                        window.UEDITOR_HOME_URL = "/public/plugins/Ueditor/";
                    </script> 
                    <?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/plugins/Ueditor/ueditor.config.js","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/plugins/Ueditor/ueditor.all.min.js","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/plugins/Ueditor/lang/zh-cn/zh-cn.js","","",""); echo $__VALUE__; ?>
                    <!-- HTML文本 start -->
                    <div class="row">
                        <div class="col-xs-9">
                            <textarea class="span12 ckeditor" id="ask_content" name="ask_content"></textarea>
                        </div>
                    </div>
                    <script type="text/javascript">
                        UE.getEditor('ask_content',{
                            serverUrl :"<?php echo url('user/Uploadify/index',array('savepath'=>'weapp')); ?>",
                            zIndex: 999,
                            initialFrameWidth: "135%", //初化宽度
                            initialFrameHeight: 300, //初化高度            
                            focus: false, //初始化时，是否让编辑器获得焦点true或false
                            maximumWords: 99999,
                            removeFormatAttributes: 'class,style,lang,width,height,align,hspace,valign',//允许的最大字符数 'fullscreen',
                            pasteplain:false, //是否默认为纯文本粘贴。false为不使用纯文本粘贴，true为使用纯文本粘贴
                            autoHeightEnabled: false,
                            toolbars: [["forecolor", "backcolor", "removeformat", "|", "simpleupload", "unlink"]],
                        });

                        //必须在提交前渲染编辑器；
                        function ask_content() {
                            //判断编辑模式状态:0表示【源代码】HTML视图；1是【设计】视图,即可见即所得；-1表示不可用
                            if(UE.getEditor("ask_content").queryCommandState('source') != 0) {
                                UE.getEditor("ask_content").execCommand('source'); //切换到【设计】视图
                            }
                        }
                    </script>
                    <?php endif; ?>
                    <div class="edit-opt">
                        <span class="editbutton" id="CommentButtonDiv" style="cursor: pointer;" data-url="<?php echo $eyou['field']['AddAnswerUrl']; ?>" onclick="return answer_submit(this);">发表回答</span>
                    </div>
                </form>
            </div>
        <!-- END -->
    </aside>

    <aside class="aside-right col-md-3">
        <div class="wt-opti">
            <!-- 个人信息 -->
            <div class="people-info">
                <a class="user_card" href="javascript:void(0);">
                    <img src="<?php echo $eyou['field']['info']['head_pic']; ?>" style="width:40px;height:40px;border-radius:100%;cursor: pointer">
                </a>
                <div class="people-info-a">
                    <h3><?php echo $eyou['field']['info']['nickname']; ?></h3>
                    <div></div>
                </div>
            </div>
            <div class="people-btn">
                <button class="act tw" onclick="window.location.href='#hedit';">
                    <i class="fa fa-plus"></i>写回答
                </button>
            </div>
        </div>
        <!-- END -->

        <!-- 热门帖子 -->
        <div class="hot-users" id="userRecommend">
    <!-- 显示热门帖子 -->
    <h3 class="block-head">
        <i class="fa fa-trophy" style="margin-right:10px;font-size: 18px"></i>热门帖子
        <div class="hot-sw">
            <span class="listType" style="margin-right:10px;cursor: pointer" data-type="week">周榜</span>
            <font style="color: #e5e5e5">|</font>
            <span class="act listType" style="margin-left:10px;cursor: pointer" data-type="total">总榜</span>
        </div>
    </h3>
    <!-- END -->

    <!-- 周榜 -->
    <ul id="weekList" style="display: none;">
        <?php if(is_array($eyou['field']['WeekList']) || $eyou['field']['WeekList'] instanceof \think\Collection || $eyou['field']['WeekList'] instanceof \think\Paginator): $i = 0; $e = 1; $__LIST__ = $eyou['field']['WeekList'];if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$week): $i= intval($key) + 1;$mod = ($i % 2 ); ?>
        <a href="<?php echo $week['AskUrl']; ?>" target="_blank">
            <li>
                <div class="hot-user-avter"><?php echo $key+1; ?></div>
                <div class="hot-user-info">
                    <div class="hot-nice"><?php echo $week['ask_title']; ?></div>
                </div>
            </li>
        </a>
        <?php echo isset($week["ey_1563185380"])?$week["ey_1563185380"]:""; ?><?php echo (1 == $e && isset($week["ey_1563185376"]))?$week["ey_1563185376"]:""; ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $week = []; ?>
    </ul>
    <!-- END -->

    <!-- 总榜 -->
    <ul id="totalList">
        <?php if(is_array($eyou['field']['TotalList']) || $eyou['field']['TotalList'] instanceof \think\Collection || $eyou['field']['TotalList'] instanceof \think\Paginator): $i = 0; $e = 1; $__LIST__ = $eyou['field']['TotalList'];if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$total): $i= intval($key) + 1;$mod = ($i % 2 ); ?>
        <a href="<?php echo $total['AskUrl']; ?>" target="_blank">
            <li>
                <div class="hot-user-avter"><?php echo $key+1; ?></div>
                <div class="hot-user-info">
                    <div class="hot-nice"><?php echo $total['ask_title']; ?></div>
                </div>
            </li>
        </a>
        <?php echo isset($total["ey_1563185380"])?$total["ey_1563185380"]:""; ?><?php echo (1 == $e && isset($total["ey_1563185376"]))?$total["ey_1563185376"]:""; ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $total = []; ?>
    </ul>
    <!-- END -->
</div>

<script type="text/javascript">
    $(function() {
        // 排行榜
        $('.listType').hover(function() {
            $('.hot-sw').find('span').removeClass('act');
            $(this).addClass('act');
            var type = $(this).data('type');
            if ('week' == type) {
                $('#weekList').show();
                $('#totalList').hide();
            } else {
                $('#weekList').hide();
                $('#totalList').show();
            }
        });
    });
</script>
        <!-- END -->
    </aside>
    
</main>
</body>
</html>