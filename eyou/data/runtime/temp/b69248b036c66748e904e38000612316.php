<?php if (!defined('THINK_PATH')) exit(); /*a:6:{s:49:"./application/admin/template/users_score/conf.htm";i:1641297257;s:57:"/mnt/api/api/application/admin/template/public/layout.htm";i:1640913882;s:60:"/mnt/api/api/application/admin/template/public/theme_css.htm";i:1640913882;s:54:"/mnt/api/api/application/admin/template/member/bar.htm";i:1640913877;s:59:"/mnt/api/api/application/admin/template/users_score/bar.htm";i:1640913897;s:57:"/mnt/api/api/application/admin/template/public/footer.htm";i:1640913882;}*/ ?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link href="/api/public/plugins/layui/css/layui.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/main.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/page.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/font/css/font-awesome.min.css" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="/api/public/static/admin/font/css/font-awesome-ie7.min.css">
<![endif]-->
<script type="text/javascript">
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
    var module_name = "<?php echo MODULE_NAME; ?>";
    var GetUploadify_url = "<?php echo url('Uploadify/upload'); ?>";
    var __root_dir__ = "/api";
    var __lang__ = "<?php echo $admin_lang; ?>";
</script>  
<link href="/api/public/static/admin/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="/api/public/static/admin/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
<!-- <link type="text/css" rel="stylesheet" href="/api/public/plugins/tags_input/css/jquery.tagsinput.css?v=<?php echo $version; ?>"> -->
<style type="text/css">html, body { overflow: visible;}</style>
<link href="/api/public/static/admin/css/diy_style.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css" />

<!-- 官方内置样式表，升级会覆盖变动，请勿修改，否则后果自负 -->

<style type="text/css">
	/*左侧收缩图标*/
	#foldSidebar i { font-size: 24px;color:<?php echo $global['web_theme_color']; ?>; }
    /*左侧菜单*/
    .eycms_cont_left{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl dd a:hover,.eycms_cont_left dl dd a.on,.eycms_cont_left dl dt.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl dd a:active{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist dd{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl.jslist dd a:hover,.eycms_cont_left dl.jslist dd a.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist:hover{ background:<?php echo $global['web_assist_color']; ?>; }
    /*栏目操作*/
    .cate-dropup .cate-dropup-con a:hover{ background-color: <?php echo $global['web_theme_color']; ?>; }
    /*按钮*/
    a.ncap-btn-green { background-color: <?php echo $global['web_theme_color']; ?>; }
    a:hover.ncap-btn-green { background-color: <?php echo $global['web_assist_color']; ?>; }
    .flexigrid .sDiv2 .btn:hover { background-color: <?php echo $global['web_theme_color']; ?>; }
    .flexigrid .mDiv .fbutton div.add{background-color: <?php echo $global['web_theme_color']; ?>; border: none;}
    .flexigrid .mDiv .fbutton div.add:hover{ background-color: <?php echo $global['web_assist_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds{color:<?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds:hover{ background-color: <?php echo $global['web_theme_color']; ?>;}
    /*选项卡字体*/
    .tab-base a.current,
    .tab-base a:hover.current { border-bottom: solid 2px <?php echo $global['web_theme_color']; ?> !important;color: <?php echo $global['web_theme_color']; ?> !important;}
    .addartbtn input.btn:hover{ border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>; }
    .addartbtn input.btn.selected{ color: <?php echo $global['web_theme_color']; ?>;border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>;}
	/*会员导航*/
	.member-nav-group .member-nav-item .btn.selected{background: <?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;box-shadow: -1px 0 0 0 <?php echo $global['web_theme_color']; ?>;}
	.member-nav-group .member-nav-item:first-child .btn.selected{border-left: 1px solid <?php echo $global['web_theme_color']; ?> !important;}
	/*搜索按钮图标*/
	.flexigrid .sDiv2 .fa-search{}
        
    /* 商品订单列表标题 */
   .flexigrid .mDiv .ftitle h3 {border-left: 3px solid <?php echo $global['web_theme_color']; ?>;} 
	
    /*分页*/
    .pagination > .active > a, .pagination > .active > a:focus, 
	.pagination > .active > a:hover, 
	.pagination > .active > span, 
	.pagination > .active > span:focus, 
	.pagination > .active > span:hover { border-color: <?php echo $global['web_theme_color']; ?>;color:<?php echo $global['web_theme_color']; ?>; }
    
    .layui-form-onswitch {border-color: <?php echo $global['web_theme_color']; ?> !important;background-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-enable.selected { background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-disable.selected {background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    input[type="text"]:focus,
    input[type="text"]:hover,
    input[type="text"]:active,
    input[type="password"]:focus,
    input[type="password"]:hover,
    input[type="password"]:active,
    textarea:hover,
    textarea:focus,
    textarea:active { border-color:<?php echo hex2rgba($global['web_theme_color'],0.8); ?>;box-shadow: 0 0 0 2px <?php echo hex2rgba($global['web_theme_color'],0.15); ?> !important;}
    .input-file-show:hover .type-file-button {background-color:<?php echo $global['web_theme_color']; ?> !important; }
    .input-file-show:hover {border-color: <?php echo $global['web_theme_color']; ?> !important;box-shadow: 0 0 5px <?php echo hex2rgba($global['web_theme_color'],0.5); ?> !important;}
    .input-file-show:hover span.show a,
    .input-file-show span.show a:hover { color: <?php echo $global['web_theme_color']; ?> !important;}
    .input-file-show:hover .type-file-button {background-color: <?php echo $global['web_theme_color']; ?> !important; }
    .color_z { color: <?php echo $global['web_theme_color']; ?> !important;}
    a.imgupload{
        background-color: <?php echo $global['web_theme_color']; ?> !important;
        border-color: <?php echo $global['web_theme_color']; ?> !important;
    }
    /*专题节点按钮*/
    .ncap-form-default .special-add{background-color:<?php echo $global['web_theme_color']; ?>;border-color:<?php echo $global['web_theme_color']; ?>;}
    .ncap-form-default .special-add:hover{background-color:<?php echo $global['web_assist_color']; ?>;border-color:<?php echo $global['web_assist_color']; ?>;}
    
    /*更多功能标题*/
    .on-off_panel .title::before {background-color:<?php echo $global['web_theme_color']; ?>;}

</style>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.js"></script>
<!-- <script type="text/javascript" src="/api/public/plugins/tags_input/js/jquery.tagsinput.js?v=<?php echo $version; ?>"></script> -->
<script type="text/javascript" src="/api/public/static/admin/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/api/public/plugins/layer-v3.1.0/layer.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/admin.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/common.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/api/public/plugins/layui/layui.js"></script>
<script src="/api/public/static/admin/js/myFormValidate.js"></script>
<script src="/api/public/static/admin/js/myAjax2.js?v=<?php echo $version; ?>"></script>
<script src="/api/public/static/admin/js/global.js?v=<?php echo $version; ?>"></script>
</head>

<body class="bodystyle" style="overflow-y: scroll; cursor: default; -moz-user-select: inherit;">

<div id="append_parent"></div>

<div id="ajaxwaitid"></div>

<div class="page">

        <div class="fixed-bar">
        <div class="item-title">
            <ul class="tab-base nc-row">
                <?php if(is_check_access('Member@users_index') == '1'): ?>
                    <li>
                        <a href="<?php echo url('Member/users_index'); ?>" <?php if(in_array(ACTION_NAME, ['users_index','level_index','attr_index','users_config'])): ?>class="current"<?php endif; ?>>
                            <span>会员列表</span>
                        </a>
                    </li>
                <?php endif; if(is_check_access('Member@money_index') == '1'): if(1 == $userConfig['pay_open']): ?>
                        <li>
                            <a href="<?php echo url('Member/money_index'); ?>" <?php if(in_array(ACTION_NAME, ['money_index', 'money_edit','media_index','upgrade_index'])): ?>class="current"<?php endif; ?>>
                                <span>订单管理</span>
                            </a>
                        </li>
                    <?php endif; endif; if(is_check_access('UsersRelease@conf') == '1'): if(1 == $userConfig['users_open_release']): ?>
                        <li>
                            <a href="<?php echo url('UsersRelease/conf'); ?>" <?php if(in_array(CONTROLLER_NAME, ['UsersRelease'])): ?>class="current"<?php endif; ?>>
                                <span>会员投稿</span>
                            </a>
                        </li>
                    <?php endif; endif; if($php_servicemeal > 1): if(is_check_access('UsersScore@conf') == '1'): ?>
                    <li>
                        <a href="<?php echo url('UsersScore/conf'); ?>" <?php if(in_array(CONTROLLER_NAME, ['UsersScore'])): ?>class="current"<?php endif; ?>>
                            <span>积分管理</span>
                        </a>
                    </li>
                    <?php endif; endif; ?>
            </ul>
        </div>
    </div>

    <div class="flexigrid">

        <div class="mDiv">

            <div class="ftitle">

                 <!--    <?php if(is_check_access('UsersScore@conf') == '1'): ?>
    <div class=" addartbtn fl">
        <input type="button" class="btn <?php if(!in_array(\think\Request::instance()->action(), ['conf'])): ?>current<?php else: ?>selected<?php endif; ?>" value="积分设置" onclick="window.location.href='<?php echo url("UsersScore/conf"); ?>';">
    </div>
    <?php endif; ?>
    <div class=" addartbtn fl"><span class="sprt col-padding-1">|</span></div>
    <?php if(is_check_access('UsersScore@index') == '1'): ?>
    <div class=" addartbtn fl">
        <input type="button" class="btn <?php if(!in_array(\think\Request::instance()->action(), ['index'])): ?>current<?php else: ?>selected<?php endif; ?>" value="积分明细" onclick="window.location.href='<?php echo url("UsersScore/index"); ?>';">
    </div>
    <?php endif; ?>
	 -->
	
	<div class="member-nav-group">
		<?php if(is_check_access('UsersScore@conf') == '1'): ?>
			<label class="member-nav-item">
				<input type="button" class="btn <?php if(!in_array(\think\Request::instance()->action(), ['conf'])): ?>current<?php else: ?>selected<?php endif; ?>" value="积分设置" onclick="window.location.href='<?php echo url("UsersScore/conf"); ?>';">
			</label>
		<?php endif; if(is_check_access('UsersScore@index') == '1'): ?>
			<label class="member-nav-item">
				<input type="button" class="btn <?php if(!in_array(\think\Request::instance()->action(), ['index'])): ?>current<?php else: ?>selected<?php endif; ?>" value="积分明细" onclick="window.location.href='<?php echo url("UsersScore/index"); ?>';">
			</label>
		<?php endif; ?>	 

	</div>


            </div>

        </div>

        <form class="form-horizontal" id="post_form" action="<?php echo url('UsersScore/conf'); ?>" method="post">

            <div class="hDiv htitx">

                <div class="hDivBox">

                    <table cellspacing="0" cellpadding="0" style="width: 100%">

                        <thead>

                        <tr>

                            <th class="sign w10" axis="col0">

                                <div class="tc"></div>

                            </th>

                            <th abbr="article_title" axis="col3" class="w10">

                                <div class="tc">积分设置</div>

                            </th>

                            <th abbr="ac_id" axis="col4">

                                <div class=""></div>

                            </th>

                        </tr>

                        </thead>

                    </table>

                </div>

            </div>

            <div class="ncap-form-default">

                <dl class="row">

                    <dt class="tit">

                        <label><em>*</em>积分名称</label>

                    </dt>

                    <dd class="opt">

                        <input type="text" name="score_name" value="<?php echo (isset($score['score_name']) && ($score['score_name'] !== '')?$score['score_name']:'积分'); ?>" class="w100" placeholder="前台积分文案">

                        <span class="err"></span>

                        <p class="notic">注：修改积分名称后，在买家端的所有页面里，看到的都是自定义的名称

                            例：商家使用自定义的积分名称来做品牌运营。如京东把积分称为“京豆”，淘宝把积分称为“淘金币”</p>

                    </dd>

                </dl>

                <dl class="row none">

                    <dt class="tit">

                        <label for="score_intro">积分说明</label>

                    </dt>

                    <dd class="opt ui-web_description ui-text">

                        <textarea rows="5" cols="100" id="score_intro" name="score_intro" style="height:100px;"

                                  class="ui-input" data-num="200"><?php echo (isset($score['score_intro']) && ($score['score_intro'] !== '')?$score['score_intro']:'a) 积分不可兑现、不可转让,仅可在本平台使用;

b) 您在本平台参加特定活动也可使用积分,详细使用规则以具体活动时的规则为准;

c) 积分的数值精确到个位(小数点后全部舍弃,不进行四舍五入)

d) 买家在完成该笔交易(订单状态为“已签收”)后才能得到此笔交易的相应积分,如购买商品参加店铺其他优惠,则优惠的金额部分不享受积分获取;'); ?></textarea>

                        <p class="notic"></p>

                        <p class="ui-big-text none"></p>

                    </dd>

                </dl>

            </div>

            <div class="hDiv htitx">

                <div class="hDivBox">

                    <table cellspacing="0" cellpadding="0" style="width: 100%">

                        <thead>

                        <tr>

                            <th class="sign w10" axis="col0">

                                <div class="tc"></div>

                            </th>

                            <th abbr="article_title" axis="col3" class="w10">

                                <div class="tc">积分赠送</div>

                            </th>

                            <th abbr="ac_id" axis="col4">

                                <div class=""></div>

                            </th>

                        </tr>

                        </thead>

                    </table>

                </div>

            </div>

            <div class="ncap-form-default">
                <dl class="row">
                    <dt class="tit" style="width: auto">
                        <label><b>签到模块</b></label>
                    </dt>
                </dl>
                <dl class="row">
                    <dt class="tit">
                        <label>签到送积分</label>
                    </dt>
                    <dd class="opt">
                        <div class="onoff">
                            <label for="score_signin_status1" class="cb-enable <?php if(isset($score['score_signin_status']) AND $score['score_signin_status'] == 1): ?>selected<?php endif; ?>" onclick="changeSigninStatus('control_signin_status',1);">开启</label>
                            <label for="score_signin_status0" class="cb-disable <?php if(!isset($score['score_signin_status']) OR empty($score['score_signin_status'])): ?>selected<?php endif; ?>" onclick="changeSigninStatus('control_signin_status',0);">关闭</label>
                            <input id="score_signin_status1" name="score_signin_status" value="1" type="radio" <?php if(isset($score['score_signin_status']) AND $score['score_signin_status'] == 1): ?> checked="checked" <?php endif; ?>>
                            <input id="score_signin_status0" name="score_signin_status" value="0" type="radio" <?php if(!isset($score['score_signin_status']) OR empty($score['score_signin_status'])): ?> checked="checked" <?php endif; ?>>
                        </div>
                        <p class="notic"></p>
                    </dd>
                </dl>

                <div id="control_signin_status" <?php if(empty($score['score_signin_status']) || (($score['score_signin_status'] instanceof \think\Collection || $score['score_signin_status'] instanceof \think\Paginator ) && $score['score_signin_status']->isEmpty())): ?>style="display:none;"<?php endif; ?>>
                    <dl class="row">
                        <dt class="tit">
                            <label>签到加积分</label>
                        </dt>
                        <dd class="opt">
                            <span class="err"></span>
                            <p class="notic"></p>
                            <table class="c-jf-table">
                                <thead>
                                    <tr>
                                        <th>会员</th>
                                        <th>签到积分</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(is_array($level) || $level instanceof \think\Collection || $level instanceof \think\Paginator): if( count($level)==0 ) : echo "" ;else: foreach($level as $k=>$vo): ?>
                                    <tr>
                                        <td><?php echo $vo['level_name']; ?></td>
                                        <td><input name="check_points[]" value="<?php echo $vo['check_points']; ?>"  type="text"></td>
                                    </tr>
                                     <td><input name="level_name[]" value="<?php echo $vo['level_name']; ?>"  type="hidden"></td>
                                    <?php endforeach; endif; else: echo "" ;endif; ?>
                                </tbody>
                            </table>
                        </dd>

                    </dl>

                </div>
            </div>

            <div class="ncap-form-default">
                <dl class="row">
                    <dt class="tit" style="width: auto">
                        <label><b>购物模块</b></label>
                    </dt>
                </dl>
                <dl class="row">
                    <dt class="tit">
                        <label>购物送积分</label>
                    </dt>
                    <dd class="opt">
                        <div class="onoff">
                            <label for="score_signin_status1" class="cb-enable <?php if(isset($score['score_signin_status']) AND $score['score_signin_status'] == 1): ?>selected<?php endif; ?>" onclick="changeSigninStatus('control_signin_status2',1);">开启</label>
                            <label for="score_signin_status0" class="cb-disable <?php if(!isset($score['score_signin_status']) OR empty($score['score_signin_status'])): ?>selected<?php endif; ?>" onclick="changeSigninStatus('control_signin_status2',0);">关闭</label>
                            <input id="score_signin_status1" name="score_signin_status" value="1" type="radio" <?php if(isset($score['score_signin_status']) AND $score['score_signin_status'] == 1): ?> checked="checked" <?php endif; ?>>
                            <input id="score_signin_status0" name="score_signin_status" value="0" type="radio" <?php if(!isset($score['score_signin_status']) OR empty($score['score_signin_status'])): ?> checked="checked" <?php endif; ?>>
                        </div>
                        <p class="notic"></p>
                    </dd>
                </dl>

                <div id="control_signin_status2" <?php if(empty($score['score_signin_status']) || (($score['score_signin_status'] instanceof \think\Collection || $score['score_signin_status'] instanceof \think\Paginator ) && $score['score_signin_status']->isEmpty())): ?>style="display:none;"<?php endif; ?>>
                    <dl class="row">
                        <dt class="tit">
                            <label>购物加积分</label>
                        </dt>
                        <dd class="opt">
                            ￥<input type="text" name="score_signin_score" value="<?php echo (isset($score['score_signin_score']) && ($score['score_signin_score'] !== '')?$score['score_signin_score']:3); ?>" class="w80">&nbsp;消费金额获得&nbsp;
                            <input type="text" name="score_signin_score" value="<?php echo (isset($score['score_signin_score']) && ($score['score_signin_score'] !== '')?$score['score_signin_score']:3); ?>" class="w80">&nbsp;积分
                            <span class="err"></span>
                            <p class="notic"></p>
                        </dd>
                    </dl>
                </div>
            </div>


            <div class="hDiv htitx">

                <div class="hDivBox">

                    <table cellspacing="0" cellpadding="0" style="width: 100%">

                        <thead>

                        <tr>

                            <th class="sign w10" axis="col0">

                                <div class="tc"></div>

                            </th>

                            <th abbr="article_title" axis="col3" class="w10">

                                <div class="tc">使用模块</div>

                            </th>

                            <th abbr="ac_id" axis="col4">

                                <div class=""></div>

                            </th>

                        </tr>

                        </thead>

                    </table>

                </div>

            </div>
            <div class="ncap-form-default">
                <dl class="row">
                    <dt class="tit">
                        <label>使用积分</label>
                    </dt>
                    <dd class="opt">
                        <div class="onoff">
                            <label for="score_signin_status1" class="cb-enable <?php if(isset($score['score_signin_status']) AND $score['score_signin_status'] == 1): ?>selected<?php endif; ?>" onclick="changeSigninStatus('control_signin_status3',1);">开启</label>
                            <label for="score_signin_status0" class="cb-disable <?php if(!isset($score['score_signin_status']) OR empty($score['score_signin_status'])): ?>selected<?php endif; ?>" onclick="changeSigninStatus('control_signin_status3',0);">关闭</label>
                            <input id="score_signin_status1" name="score_signin_status" value="1" type="radio" <?php if(isset($score['score_signin_status']) AND $score['score_signin_status'] == 1): ?> checked="checked" <?php endif; ?>>
                            <input id="score_signin_status0" name="score_signin_status" value="0" type="radio" <?php if(!isset($score['score_signin_status']) OR empty($score['score_signin_status'])): ?> checked="checked" <?php endif; ?>>
                        </div>
                        <p class="notic"></p>
                    </dd>
                </dl>

                <div id="control_signin_status3" <?php if(empty($score['score_signin_status']) || (($score['score_signin_status'] instanceof \think\Collection || $score['score_signin_status'] instanceof \think\Paginator ) && $score['score_signin_status']->isEmpty())): ?>style="display:none;"<?php endif; ?>>
                    <dl class="row">
                        <dt class="tit">
                            <label>积分冲抵</label>
                        </dt>
                        <dd class="opt">
                            <input type="text" name="score_signin_score" value="<?php echo (isset($score['score_signin_score']) && ($score['score_signin_score'] !== '')?$score['score_signin_score']:3); ?>" class="w80">&nbsp;积分冲抵&nbsp;
                            ￥<input type="text" name="score_signin_score" value="<?php echo (isset($score['score_signin_score']) && ($score['score_signin_score'] !== '')?$score['score_signin_score']:3); ?>" class="w80">&nbsp;消费金额
                            <span class="err"></span>
                            <p class="notic"></p>
                        </dd>
                    </dl>
                </div>
            </div>

            <div class="ncap-form-default">

                <dl class="row">

                    <div class="bot">

                    <a href="JavaScript:void(0);" onclick="check_submit();" class="ncap-btn-big ncap-btn-green"

                       id="submitBtn">确认提交</a>

                    </div>

                </dl>

            </div>

        </form>

    </div>

</div>
<style>
    .c-jf-table th,.c-jf-table td{
        padding: 10px;
        border: 1px solid #eee;
    }
</style>
<script type="text/javascript">

    function check_submit() {

        if($('input[name=score_name]').val() == ''){

            showErrorMsg('积分名称不能为空！');

            $('input[name=score_name]').focus();

            return false;

        }

        layer_loading('正在处理');

        $('#post_form').submit();

    }



    function changeStatus(id,val) {

        if (1 == val) {

            $("#"+id).show();

        }else{

            $("#"+id).hide();

        }

    }



    function changeSigninStatus(id,val) {

        if (1 == val) {

            $("#"+id).show();

        }else{

            $("#"+id).hide();

        }

    }

</script>

<br/>
<div id="goTop">
    <a href="JavaScript:void(0);" id="btntop">
        <i class="fa fa-angle-up"></i>
    </a>
    <a href="JavaScript:void(0);" id="btnbottom">
        <i class="fa fa-angle-down"></i>
    </a>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#think_page_trace_open').css('z-index', 99999);
    });
</script>
</body>
</html>