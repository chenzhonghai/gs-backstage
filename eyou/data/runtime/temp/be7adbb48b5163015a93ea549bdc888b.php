<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:33:"./weapp/Messages/template/add.htm";i:1639205418;s:44:"/mnt/eyou/weapp/Messages/template/header.htm";i:1639205418;s:41:"/mnt/eyou/weapp/Messages/template/bar.htm";i:1639205418;s:44:"/mnt/eyou/weapp/Messages/template/footer.htm";i:1639205418;}*/ ?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link href="/public/static/admin/css/main.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/public/static/admin/css/page.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/public/static/admin/font/css/font-awesome.min.css?v=<?php echo $version; ?>" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="/public/static/admin/font/css/font-awesome-ie7.min.css?v=<?php echo $version; ?>">
<![endif]-->
<link href="/public/static/admin/js/jquery-ui/jquery-ui.min.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css"/>
<link href="/public/static/admin/css/perfect-scrollbar.min.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css"/>
<style type="text/css">html, body { overflow: visible;}</style>
<script type="text/javascript">
    // 入口路径
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
    var module_name = "<?php echo MODULE_NAME; ?>";
    var GetUploadify_url = "<?php echo url('Uploadify/upload'); ?>";
</script>  
<script type="text/javascript" src="/public/static/admin/js/jquery.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/public/static/admin/js/jquery-ui/jquery-ui.min.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/public/plugins/layer-v3.1.0/layer.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/weapp/Messages/template/skin/js/admin.js?v=<?php echo (isset($weappInfo['version']) && ($weappInfo['version'] !== '')?$weappInfo['version']:'v1.0.0'); ?>"></script>
<script type="text/javascript" src="/public/static/admin/js/perfect-scrollbar.min.js?v=<?php echo $version; ?>"></script>
<script src="/weapp/Messages/template/skin/js/global.js?v=<?php echo (isset($weappInfo['version']) && ($weappInfo['version'] !== '')?$weappInfo['version']:'v1.0.0'); ?>"></script>
</head>
<style>
.ncap-form-default dl.row {position: unset;}
.comboTreeDropDownContainer {width: 388px !important;}
.multiplesFilter {border: 0 !important;border-bottom: solid 1px #eee !important;outline: none !important; }
.comboTreeDropDownContainer {box-shadow: 0 0 8px rgba(0,0,0,0.2);border-radius: 4px!important;background-color: #FFF !important;border: solid 1px #eee !important;}
.multiplesFilter{height: 30px !important;}
</style>
<body style="background-color: #FFF; overflow: auto;">
<div id="toolTipLayer" style="position: absolute; z-index: 9999; display: none; visibility: visible; left: 95px; top: 573px;"></div>
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="page">
    
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="<?php if('Messages' == \think\Request::instance()->param('sc') && 'index' == \think\Request::instance()->param('sa')): ?><?php echo url("Weapp/index"); else: ?><?php echo weapp_url("Messages/Messages/index"); endif; ?>" title="返回列表"><i class="fa fa-chevron-left"></i></a>
            <div class="subject">
                <h3><?php echo $weappInfo['name']; ?></h3>
                <h5></h5>
            </div>
            <ul class="tab-base nc-row">
                <li>
                <?php if(\think\Request::instance()->param('sa') == 'index'): ?>
                    <a href="javascript:void(0);" class="tab current"><span>文档列表</span></a>
                <?php else: ?>
                    <a href="<?php echo weapp_url("Messages/Messages/index"); ?>" class="tab"><span>文档列表</span></a>
                <?php endif; ?>
                </li>

                <li>
                <?php if(in_array((\think\Request::instance()->param('sa')), explode(',',"doc"))): ?>
                    <a href="javascript:void(0);" class="tab current"><span>插件介绍</span></a>
                <?php else: ?>
                    <a href="<?php echo weapp_url("Messages/Messages/doc"); ?>" class="tab"><span>插件介绍</span></a>
                <?php endif; ?>
                </li>
            </ul>
        </div>
    </div>
    <form class="form-horizontal" id="post_form" action="<?php echo weapp_url('Messages/Messages/add'); ?>" method="post">
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label for="title"><em>*</em>通知标题</label>
                </dt>
                <dd class="opt">
                    <input type="text" name="title" value="" id="title" class="input-txt">
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="users_id">通知某个会员</label>
                </dt>
                <dd class="opt">
                    <input type="text" name="users_id" placeholder="可以多选，全站通知不填" id="justAnInputBox" class="input-txt" autocomplete="off">
                    <span class="err"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="remark"><em>*</em>通知内容</label>
                </dt>
                <dd class="opt">
                    <textarea rows="5" cols="80" id="remark" name="remark" style="height:80px;" placeholder="通知会员内容"></textarea>
                    <p class="notic"></p>
                </dd>
            </dl>
            <div class="bot">
                <a href="JavaScript:void(0);" onclick="checkForm();" class="ncap-btn-big ncap-btn-green" id="submitBtn">确认提交</a>
            </div>
        </div>
    </form>
</div>
<link href="/weapp/Messages/template/skin/css/Messages.css?v=<?php echo (isset($weappInfo['version']) && ($weappInfo['version'] !== '')?$weappInfo['version']:'v1.0.0'); ?>" rel="stylesheet" type="text/css">
<script src="/weapp/Messages/template/skin/js/icontains.js?v=<?php echo (isset($weappInfo['version']) && ($weappInfo['version'] !== '')?$weappInfo['version']:'v1.0.0'); ?>"></script>
<script src="/weapp/Messages/template/skin/js/comboTreePlugin.js?v=<?php echo (isset($weappInfo['version']) && ($weappInfo['version'] !== '')?$weappInfo['version']:'v1.0.0'); ?>"></script>
<script type="text/javascript">
    var SampleJSONData = [
	    <?php if(empty($listname) || (($listname instanceof \think\Collection || $listname instanceof \think\Paginator ) && $listname->isEmpty())): ?>
        {
            id: 0,
            title: '没有符合条件的记录'
        }
		<?php else: if(is_array($listname) || $listname instanceof \think\Collection || $listname instanceof \think\Paginator): $i = 0; $__LIST__ = $listname;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
		        {
                    id: <?php echo $key; ?>,
                    title: '<?php echo $vo['username']; ?>'
                },
		    <?php endforeach; endif; else: echo "" ;endif; endif; ?>
    ];
    var comboTree1, comboTree2;
    jQuery(document).ready(function($) {
		comboTree1 = $('#justAnInputBox').comboTree({
			source : SampleJSONData,
			isMultiple: true
		});
    });
    // 判断输入框是否为空
    function checkForm(){
        if($('input[name=title]').val() == ''){
            showErrorMsg('网站名称不能为空！');
            $('input[name=title]').focus();
            return false;
        }
		if($('textarea[name=remark]').val() == ''){
            showErrorMsg('通知内容不能为空！');
            $('textarea[name=remark]').focus();
            return false;
        }
        layer_loading('正在处理');
        $('#post_form').submit();
    }
    
</script>
<br/>
<div id="goTop">
    <a href="JavaScript:void(0);" id="btntop">
        <i class="fa fa-angle-up"></i>
    </a>
    <a href="JavaScript:void(0);" id="btnbottom">
        <i class="fa fa-angle-down"></i>
    </a>
</div>

<script type="text/javascript">
    $(document).ready(function(){
		// 调试信息
        $('#think_page_trace_open').css('z-index', 99999);
    });
</script>
</body>
</html>