<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:30:"./weapp/Zan/template/index.htm";i:1634548463;s:39:"/mnt/eyou/weapp/Zan/template/header.htm";i:1596289902;s:36:"/mnt/eyou/weapp/Zan/template/bar.htm";i:1596289902;s:39:"/mnt/eyou/weapp/Zan/template/footer.htm";i:1596289902;}*/ ?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link href="/public/static/admin/css/main.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/public/static/admin/css/page.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/public/static/admin/font/css/font-awesome.min.css?v=<?php echo $version; ?>" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="/public/static/admin/font/css/font-awesome-ie7.min.css?v=<?php echo $version; ?>">
<![endif]-->
<link href="/public/static/admin/js/jquery-ui/jquery-ui.min.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css"/>
<link href="/public/static/admin/css/perfect-scrollbar.min.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css"/>
<style type="text/css">html, body { overflow: visible;}</style>
<script type="text/javascript">
    // 入口路径
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
    var module_name = "<?php echo MODULE_NAME; ?>";
    var GetUploadify_url = "<?php echo url('Uploadify/upload'); ?>";
</script>  
<script type="text/javascript" src="/public/static/admin/js/jquery.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/public/static/admin/js/jquery-ui/jquery-ui.min.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/public/plugins/layer-v3.1.0/layer.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/weapp/Zan/template/skin/js/admin.js?v=<?php echo (isset($weappInfo['version']) && ($weappInfo['version'] !== '')?$weappInfo['version']:'v1.0.0'); ?>"></script>
<script type="text/javascript" src="/public/static/admin/js/perfect-scrollbar.min.js?v=<?php echo $version; ?>"></script>
<script src="/weapp/Zan/template/skin/js/global.js?v=<?php echo (isset($weappInfo['version']) && ($weappInfo['version'] !== '')?$weappInfo['version']:'v1.0.0'); ?>"></script>
</head>
<body style="background-color: rgb(255, 255, 255); overflow: auto; cursor: default; -moz-user-select: inherit;">
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="page">
    
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="<?php if('Zan' == \think\Request::instance()->param('sc') && 'index' == \think\Request::instance()->param('sa')): ?><?php echo url("Weapp/index"); else: ?><?php echo weapp_url("Zan/Zan/index"); endif; ?>" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3><?php echo $weappInfo['name']; ?></h3>
                <h5></h5>
            </div>
            <ul class="tab-base nc-row">
                <li>
                <?php if(\think\Request::instance()->param('sa') == 'index'): ?>
                    <a href="javascript:void(0);" class="tab current"><span>点赞介绍</span></a>
                <?php else: ?>
                    <a href="<?php echo weapp_url("Zan/Zan/index"); ?>" class="tab"><span>点赞介绍</span></a>
                <?php endif; ?>
                </li>

            </ul>
        </div>
    </div>
    <div class="flexigrid">
        <div class="mDiv" style="display: flex;">
            <div class="ftitle" style="height: auto;">
                <h3>点赞详细介绍：</h3>
				<span style="font-size: 14px;color: #333;display:block;text-indent: 8px;padding: 10px 0px;line-height: 10px;">一：本点赞插件七月二十号更新支持游客点赞，支持会员点赞！</span>
				<span style="font-size: 14px;color: #333;display:block;text-indent: 8px;padding: 10px 0px;line-height: 10px;">二：本点赞插件十一月十五号更新列表显示点赞！</span>
				<span style="font-size:14px;color: #333;display:block;text-indent:8px;padding:10px 0px;line-height: 10px;">三：内容页点赞复制以下代码到{$eyou.field.content}下方即可，请勿粘贴到首页和列表页！</span>
				<span style="font-size:14px;color: #333;display:block;text-indent:8px;padding:10px 0px;line-height: 10px;">四：列表页点赞复制以下代码到{eyou:list pagesize='10' titlelen='30' infolen='160' orderby='add_time'}
代码
{/eyou:list}需要放的位置即可，支持列表和首页</span>
				<span style="font-size:14px;color: #333;display:block;text-indent:8px;padding:10px 0px;line-height: 10px;">五：感谢大家的支持！新增了列表页面显示点赞数量</span>
            </div>
        </div>
		<div class="mDiv">
            <div class="ftitle" style="height: auto;">
                <h3>内容页点赞调用代码：{eyou:include file="/template/plugins/zan/pc/index.htm" /} </h3></br>
				<h3>存在mobile文件夹内容页点赞调用代码：{eyou:include file="/template/plugins/zan/mobile/index.htm" /} </h3></br>
				<h3>列表页点赞调用代码：{eyou:include file="/template/plugins/zan/pc/index_list.htm" /} </h3></br>
				<h3>存在mobile文件夹列表页点赞调用代码：{eyou:include file="/template/plugins/zan/mobile/index_list.htm" /} </h3>
            </div>
        </div>
        
		
    </div>
</div>

<br/>
<div id="goTop">
    <a href="JavaScript:void(0);" id="btntop">
        <i class="fa fa-angle-up"></i>
    </a>
    <a href="JavaScript:void(0);" id="btnbottom">
        <i class="fa fa-angle-down"></i>
    </a>
</div>

<script type="text/javascript">
    $(document).ready(function(){
		// 调试信息
        $('#think_page_trace_open').css('z-index', 99999);
    });
</script>
</body>
</html>
