<?php if (!defined('THINK_PATH')) exit(); /*a:7:{s:59:"./application/admin/template/shop_service/after_service.htm";i:1646138839;s:57:"/mnt/api/api/application/admin/template/public/layout.htm";i:1640913882;s:60:"/mnt/api/api/application/admin/template/public/theme_css.htm";i:1640913882;s:53:"/mnt/api/api/application/admin/template/shop/left.htm";i:1640913887;s:57:"/mnt/api/api/application/admin/template/shop/shop_bar.htm";i:1640913887;s:55:"/mnt/api/api/application/admin/template/public/page.htm";i:1640913882;s:57:"/mnt/api/api/application/admin/template/public/footer.htm";i:1640913882;}*/ ?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link href="/api/public/plugins/layui/css/layui.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/main.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/page.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/font/css/font-awesome.min.css" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="/api/public/static/admin/font/css/font-awesome-ie7.min.css">
<![endif]-->
<script type="text/javascript">
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
    var module_name = "<?php echo MODULE_NAME; ?>";
    var GetUploadify_url = "<?php echo url('Uploadify/upload'); ?>";
    var __root_dir__ = "/api";
    var __lang__ = "<?php echo $admin_lang; ?>";
</script>  
<link href="/api/public/static/admin/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="/api/public/static/admin/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
<!-- <link type="text/css" rel="stylesheet" href="/api/public/plugins/tags_input/css/jquery.tagsinput.css?v=<?php echo $version; ?>"> -->
<style type="text/css">html, body { overflow: visible;}</style>
<link href="/api/public/static/admin/css/diy_style.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css" />

<!-- 官方内置样式表，升级会覆盖变动，请勿修改，否则后果自负 -->

<style type="text/css">
	/*左侧收缩图标*/
	#foldSidebar i { font-size: 24px;color:<?php echo $global['web_theme_color']; ?>; }
    /*左侧菜单*/
    .eycms_cont_left{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl dd a:hover,.eycms_cont_left dl dd a.on,.eycms_cont_left dl dt.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl dd a:active{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist dd{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl.jslist dd a:hover,.eycms_cont_left dl.jslist dd a.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist:hover{ background:<?php echo $global['web_assist_color']; ?>; }
    /*栏目操作*/
    .cate-dropup .cate-dropup-con a:hover{ background-color: <?php echo $global['web_theme_color']; ?>; }
    /*按钮*/
    a.ncap-btn-green { background-color: <?php echo $global['web_theme_color']; ?>; }
    a:hover.ncap-btn-green { background-color: <?php echo $global['web_assist_color']; ?>; }
    .flexigrid .sDiv2 .btn:hover { background-color: <?php echo $global['web_theme_color']; ?>; }
    .flexigrid .mDiv .fbutton div.add{background-color: <?php echo $global['web_theme_color']; ?>; border: none;}
    .flexigrid .mDiv .fbutton div.add:hover{ background-color: <?php echo $global['web_assist_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds{color:<?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds:hover{ background-color: <?php echo $global['web_theme_color']; ?>;}
    /*选项卡字体*/
    .tab-base a.current,
    .tab-base a:hover.current { border-bottom: solid 2px <?php echo $global['web_theme_color']; ?> !important;color: <?php echo $global['web_theme_color']; ?> !important;}
    .addartbtn input.btn:hover{ border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>; }
    .addartbtn input.btn.selected{ color: <?php echo $global['web_theme_color']; ?>;border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>;}
	/*会员导航*/
	.member-nav-group .member-nav-item .btn.selected{background: <?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;box-shadow: -1px 0 0 0 <?php echo $global['web_theme_color']; ?>;}
	.member-nav-group .member-nav-item:first-child .btn.selected{border-left: 1px solid <?php echo $global['web_theme_color']; ?> !important;}
	/*搜索按钮图标*/
	.flexigrid .sDiv2 .fa-search{}
        
    /* 商品订单列表标题 */
   .flexigrid .mDiv .ftitle h3 {border-left: 3px solid <?php echo $global['web_theme_color']; ?>;} 
	
    /*分页*/
    .pagination > .active > a, .pagination > .active > a:focus, 
	.pagination > .active > a:hover, 
	.pagination > .active > span, 
	.pagination > .active > span:focus, 
	.pagination > .active > span:hover { border-color: <?php echo $global['web_theme_color']; ?>;color:<?php echo $global['web_theme_color']; ?>; }
    
    .layui-form-onswitch {border-color: <?php echo $global['web_theme_color']; ?> !important;background-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-enable.selected { background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-disable.selected {background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    input[type="text"]:focus,
    input[type="text"]:hover,
    input[type="text"]:active,
    input[type="password"]:focus,
    input[type="password"]:hover,
    input[type="password"]:active,
    textarea:hover,
    textarea:focus,
    textarea:active { border-color:<?php echo hex2rgba($global['web_theme_color'],0.8); ?>;box-shadow: 0 0 0 2px <?php echo hex2rgba($global['web_theme_color'],0.15); ?> !important;}
    .input-file-show:hover .type-file-button {background-color:<?php echo $global['web_theme_color']; ?> !important; }
    .input-file-show:hover {border-color: <?php echo $global['web_theme_color']; ?> !important;box-shadow: 0 0 5px <?php echo hex2rgba($global['web_theme_color'],0.5); ?> !important;}
    .input-file-show:hover span.show a,
    .input-file-show span.show a:hover { color: <?php echo $global['web_theme_color']; ?> !important;}
    .input-file-show:hover .type-file-button {background-color: <?php echo $global['web_theme_color']; ?> !important; }
    .color_z { color: <?php echo $global['web_theme_color']; ?> !important;}
    a.imgupload{
        background-color: <?php echo $global['web_theme_color']; ?> !important;
        border-color: <?php echo $global['web_theme_color']; ?> !important;
    }
    /*专题节点按钮*/
    .ncap-form-default .special-add{background-color:<?php echo $global['web_theme_color']; ?>;border-color:<?php echo $global['web_theme_color']; ?>;}
    .ncap-form-default .special-add:hover{background-color:<?php echo $global['web_assist_color']; ?>;border-color:<?php echo $global['web_assist_color']; ?>;}
    
    /*更多功能标题*/
    .on-off_panel .title::before {background-color:<?php echo $global['web_theme_color']; ?>;}

</style>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.js"></script>
<!-- <script type="text/javascript" src="/api/public/plugins/tags_input/js/jquery.tagsinput.js?v=<?php echo $version; ?>"></script> -->
<script type="text/javascript" src="/api/public/static/admin/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/api/public/plugins/layer-v3.1.0/layer.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/admin.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/common.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/api/public/plugins/layui/layui.js"></script>
<script src="/api/public/static/admin/js/myFormValidate.js"></script>
<script src="/api/public/static/admin/js/myAjax2.js?v=<?php echo $version; ?>"></script>
<script src="/api/public/static/admin/js/global.js?v=<?php echo $version; ?>"></script>
</head>

<body class="bodystyle" style="overflow-y: scroll; cursor: default; -moz-user-select: inherit;min-width:auto;">
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="sidebar-second ">
    <ul>
        <li class="sidebar-second-title">商城中心</li>
        <li>
            <a <?php if('Statistics' == CONTROLLER_NAME): ?>class="active"<?php endif; ?> href='<?php echo url("Statistics/index"); ?>'>数据统计</a>
        </li>
        <li>
            <a <?php if(('ShopService' == CONTROLLER_NAME) OR ('Shop' == CONTROLLER_NAME and in_array(ACTION_NAME, ['index', 'order_details']))): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/index"); ?>'>所有订单</a>
        </li>
        <li>
            <a <?php if('ShopProduct' == CONTROLLER_NAME and in_array(ACTION_NAME, ['index', 'add', 'edit'])): ?>class="active"<?php endif; ?> href='<?php echo url("ShopProduct/index"); ?>'>商品管理</a>
        </li>
        <li>
            <a <?php if('ShopProduct' == CONTROLLER_NAME and in_array(ACTION_NAME, ['attrlist_index', 'attribute_index'])): ?>class="active"<?php endif; ?> href='<?php echo url("ShopProduct/attrlist_index"); ?>'>商品参数</a>
        </li>
        <?php 
            $shop_open_spec = getUsersConfigData('shop.shop_open_spec');
         if($shop_open_spec == '1'): ?>
        <li>
            <a <?php if('Shop' == CONTROLLER_NAME and in_array(ACTION_NAME, ['spec_index'])): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/spec_index"); ?>'>商品规格</a>
        </li>
        <?php endif; ?>
        <li>
            <a <?php if('Shop' == CONTROLLER_NAME and 'conf' == ACTION_NAME): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/conf"); ?>'>商城配置</a>
        </li>
        <?php if($php_servicemeal >= 2): ?>
        <li>
            <a <?php if('ShopComment' == CONTROLLER_NAME): ?>class="active"<?php endif; ?> href='<?php echo url("ShopComment/comment_index"); ?>'>商品评价</a>
        </li>
        <li>
            <a <?php if((in_array(CONTROLLER_NAME, ['Sharp','Coupon'])) OR ('Shop' == CONTROLLER_NAME and in_array(ACTION_NAME, ['market_index']))): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/market_index"); ?>'>营销功能</a>
        </li>
        <?php endif; ?>
    </ul>
</div>
<div class="page" style="min-width:auto;margin-left:98px;">
    <div class="fixed-bar">
    <div class="item-title">
        <!-- <a class="back" href="<?php echo url("Shop/index"); ?>" title="返回列表"><i class="fa fa-angle-double-left"></i>返回</a></a> -->
        <ul class="tab-base nc-row">
            <li>
                <a <?php if('index' == ACTION_NAME and '' == \think\Request::instance()->param('order_status')): ?>class="current"<?php endif; ?> href='<?php echo url("Shop/index"); ?>'>
                    <span>全部</span>
                </a>
            </li>

            <li>
                <a <?php if(\think\Request::instance()->param('order_status') == '10'): ?>class="current"<?php endif; ?> href='<?php echo url("Shop/index", ["order_status"=>10]); ?>'>
                    <span>待付款</span>
                </a>
            </li>

            <li>
                <a <?php if(\think\Request::instance()->param('order_status') == '1'): ?>class="current"<?php endif; ?> href='<?php echo url("Shop/index", ["order_status"=>1]); ?>' href="<?php echo url('Shop/conf'); ?>">
                    <span>待发货</span>
                </a>
            </li>

            <li>
                <a <?php if(\think\Request::instance()->param('order_status') == '2'): ?>class="current"<?php endif; ?> href='<?php echo url("Shop/index", ["order_status"=>2]); ?>' href="<?php echo url('Shop/conf'); ?>">
                    <span>已发货</span>
                </a>
            </li>

            <li>
                <a <?php if(\think\Request::instance()->param('order_status') == '3'): ?>class="current"<?php endif; ?> href='<?php echo url("Shop/index", ["order_status"=>3]); ?>' href="<?php echo url('Shop/conf'); ?>">
                    <span>已完成</span>
                </a>
            </li>
            
            <?php if($php_servicemeal > 1): ?>
            <li>
                <a <?php if('ShopService' == CONTROLLER_NAME): ?>class="current"<?php endif; ?> href='<?php echo url("ShopService/after_service"); ?>'>
                    <span>售后</span>
                </a>
            </li>
            <?php endif; ?>
            
        </ul>
    </div>
</div>
    <div class="flexigrid">
        <div class="mDiv">
            <div class="ftitle"> <h3>售后列表</h3> <h5>(共<?php echo $pager->totalRows; ?>条数据)</h5> </div>
            <form class="navbar-form form-inline" id="postForm" action="<?php echo url('ShopService/after_service'); ?>" method="get" onsubmit="layer_loading('正在处理');">
                <?php echo (isset($searchform['hidden']) && ($searchform['hidden'] !== '')?$searchform['hidden']:''); ?>
                <div class="sDiv">
                    <div class="sDiv2">
                        <input type="text" size="50" name="order_code" value="<?php echo \think\Request::instance()->param('order_code'); ?>" class="qsbox" style="width: 200px;" placeholder="搜索订单号..."> <input type="submit" class="btn" value="搜索"> <i class="fa fa-search"></i>
                    </div>

                </div>
            </form>
        </div>

        <div class="hDiv">
            <div class="hDivBox">
                <table cellspacing="0" cellpadding="0" style="width: 100%">
                    <thead>
                    <tr>
                        <th class="sign w40" axis="col0">
                            <div class="tc"><input type="checkbox" autocomplete="off" class="checkAll"></div>
                        </th>
                        <th align="center" abbr="article_title" axis="col3" class="w60">
                            <div class="tc">缩略图</div>
                        </th>
                        <th abbr="article_title" axis="col3" class="">
                            <div style="text-align: left; padding-left: 10px;" class="">订单号</div>
                        </th>
                        <th abbr="article_time" axis="col6" class="w100">
                            <div class="tc">用户名</div>
                        </th>
                        <th abbr="article_time" axis="col6" class="w100">
                            <div class="tc">商品金额</div>
                        </th>
                        <th abbr="article_time" axis="col6" class="w100">
                            <div class="tc">退还金额</div>
                        </th>
                        <th abbr="article_time" axis="col6" class="w100">
                            <div class="tc">售后类型</div>
                        </th>
                        <th abbr="article_time" axis="col6" class="w100">
                            <div class="tc">处理状态</div>
                        </th>
                        <th abbr="article_time" axis="col6" class="w160">
                            <div class="tc">申请时间</div>
                        </th>
                        <th axis="col1" class="w200">
                            <div class="tc">操作</div>
                        </th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="bDiv" style="height: auto;">
            <div id="flexigrid" cellpadding="0" cellspacing="0" border="0">
                <table style="width: 100%">
                    <tbody>
                    <?php if(empty($Service) || (($Service instanceof \think\Collection || $Service instanceof \think\Paginator ) && $Service->isEmpty())): ?>
                        <tr>
                            <td class="no-data" align="center" axis="col0" colspan="50">
                                <i class="fa fa-exclamation-circle"></i>没有符合条件的记录
                            </td>
                        </tr>
                    <?php else: if(is_array($Service) || $Service instanceof \think\Collection || $Service instanceof \think\Paginator): $i = 0; $__LIST__ = $Service;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                        <tr>
                            <td class="sign">
                                <div class="w40 tc"> <input type="checkbox" autocomplete="off" name="ids[]" value="<?php echo $vo['service_id']; ?>"> </div>
                            </td>

                            <td class="w60">
                                <div class="tc">
                                    <a href="<?php echo $vo['arcurl']; ?>" target="_blank"><img width="60" height="60" src="<?php echo $vo['product_img']; ?>"></a>
                                </div>
                            </td>

                            <td class="" style="width: 100%;">
                                <div class="tl" style="padding-left: 10px;">
                                    <a href="javascript:void(0);" data-href="<?php echo url('Shop/order_details', array('order_id' => $vo['order_id'])); ?>" onclick="openFullframe(this, '订单详情', '100%', '100%');"> <?php echo $vo['order_code']; ?> </a>
                                </div>
                            </td>

                            <td class="">
                                <div class="w100 tc">
                                    <a href="javascript:void(0);" data-href="<?php echo url('Member/users_edit', array('id' => $vo['users_id'])); ?>" onclick="openFullframe(this, '用户资料', '100%', '100%');"> <?php echo $vo['username']; ?> </a>
                                </div>
                            </td>

                            <td class="">
                                <?php $ser = M('shop_order_service')->where(array('service_id' => $vo['service_id']))->find();if($vo['service_type'] == '1'): ?>￥<?php echo $vo['refund_price']; endif; if($vo['service_type'] == '2'): ?>￥<?php echo $vo['product_total_price']; endif; ?>
                            </td>

                            <td class="">
                                <?php $ser['refund_price'] =  sprintf("%.2f", $ser['refund_price'] - $ser['coupon_price']);if($vo['service_type'] == '1'): ?>￥<?php echo $ser['refund_price']; endif; if($vo['service_type'] == '2'): ?>￥<?php echo $ser['refund_price']; endif; ?>
                            </td>

                            <td class="">
                                <?php if($vo['service_type'] == '1'): ?><div class="w100 tc blue">换货</div><?php endif; if($vo['service_type'] == '2'): ?><div class="w100 tc red">退货</div><?php endif; if($vo['service_type'] == '3'): ?><div class="w100 tc">维修</div><?php endif; ?>
                            </td>

                            <td class=""> <div class="w100 tc"> <?php echo (isset($ServiceStatus[$vo['status']]) && ($ServiceStatus[$vo['status']] !== '')?$ServiceStatus[$vo['status']]:''); ?> </div> </td>

                            <td class=""> <div class="w160 tc"> <?php echo MyDate('Y-m-d H:i:s',$vo['add_time']); ?> </div> </td>

                            <td>
                                <div class="w200 tc">
                                    <a href="javascript:void(0);" data-href="<?php echo url('ShopService/after_service_details', array('service_id' => $vo['service_id'])); ?>" onclick="openFullframe(this, '售后详情', '100%', '100%');" class="btn blue"><i class="fa fa-pencil-square-o"></i> 详情 </a>
                                    <?php if($vo['status'] == '1'): ?>
                                    <a href="JavaScript:void(0);" onclick="AuditOpinion('<?php echo $vo['service_id']; ?>', '<?php echo $vo['users_id']; ?>', '<?php echo $vo['order_id']; ?>', '<?php echo $vo['service_type']; ?>');" class="btn blue"> 审核 </a>
                                    <?php endif; if($vo['service_type'] == 1 && in_array($vo['status'], [2, 4, 5])): ?>
                                    <a href="JavaScript:void(0);" onclick="CarryOut('<?php echo $vo['service_id']; ?>', '<?php echo $vo['users_id']; ?>', '<?php echo $vo['order_id']; ?>', '<?php echo $vo['service_type']; ?>');" class="btn blue"> 完成 </a>
                                    <?php endif; if($vo['service_type'] == 2 && in_array($vo['status'], [2, 4, 5])): ?>
                                    <a href="JavaScript:void(0);" onclick="Refund('<?php echo $vo['service_id']; ?>', '<?php echo $vo['users_id']; ?>', '<?php echo $vo['order_id']; ?>', '<?php echo $vo['refund_price']; ?>');" class="btn blue"> 退款 </a>
                                    <?php endif; ?>
                                    <a href="javascript:void(0);" data-url="<?php echo url('ShopService/after_service_del'); ?>" data-id="<?php echo $vo['service_id']; ?>" onclick="delfun(this);" class="btn blue"> 删除 </a>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; endif; else: echo "" ;endif; endif; ?>
                    </tbody>
                </table>
            </div>
            <div class="iDiv" style="display: none;"></div>
        </div>

        <div class="tDiv">
            <div class="tDiv2">
                <div class="fbutton checkboxall">
                    <input type="checkbox" autocomplete="off" class="checkAll">
                </div>
                <div class="fbutton">
                    <a onclick="batch_del(this, 'ids');" data-url="<?php echo url('ShopService/after_service_del'); ?>" class="layui-btn layui-btn-primary"><span>批量删除</span></a>
                </div>
                <div class="fbuttonr">
    <div class="pages">
       <?php echo $page; ?>
    </div>
</div>
<div class="fbuttonr">
    <div class="total">
        <h5>共有<?php echo $pager->totalRows; ?>条,每页显示
            <select name="pagesize" style="width: 60px;" onchange="ey_selectPagesize(this);">
                <option <?php if($pager->listRows == 20): ?> selected <?php endif; ?> value="20">20</option>
                <option <?php if($pager->listRows == 50): ?> selected <?php endif; ?> value="50">50</option>
                <option <?php if($pager->listRows == 100): ?> selected <?php endif; ?> value="100">100</option>
                <option <?php if($pager->listRows == 200): ?> selected <?php endif; ?> value="200">200</option>
            </select>
        </h5>
    </div>
</div>
            </div>
            <div style="clear:both"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $('input[name*=ids]').click(function(){
            if ($('input[name*=ids]').length == $('input[name*=ids]:checked').length) {
                $('.checkAll').prop('checked','checked');
            } else {
                $('.checkAll').prop('checked', false);
            }
        });
        $('input[type=checkbox].checkAll').click(function(){
            $('input[type=checkbox]').prop('checked',this.checked);
        });
    });

    $(document).ready(function(){
        // 表格行点击选中切换
        $('#flexigrid > table>tbody >tr').click(function(){
            $(this).toggleClass('trSelected');
        });

        // 点击刷新数据
        $('.fa-refresh').click(function(){
            location.href = location.href;
        });
    });

    // 确认审核意见
    function AuditOpinion(service_id, users_id, order_id, service_type) {
        var type = 1 == service_type ? "<span style='color: blue;'> 换货 </span>" : "<span style='color: red;'> 退货 </span>";
        layer.confirm('是否同意'+type+'申请？', {
            title: false,
            btn: ['同意', '拒绝'],
        }, function() {
            // 同意申请
            PerformOperation(service_id, users_id, order_id, 2, 0, 0);
        }, function() {
            // 拒绝申请
            PerformOperation(service_id, users_id, order_id, 3, 0, 0);
        });
    }

    // 确认完成服务
    function CarryOut(service_id, users_id, order_id, service_type) {
        var type = 1 == service_type ? "<span style='color: blue;'> 换货 </span>" : "<span style='color: red;'> 退货 </span>";
        layer.confirm('确认完成'+type+'服务？', {
            title: false,
            btn: ['确认', '取消'],
        }, function() {
            // 确认
            if (1 == service_type) {
                // 换货结束
                PerformOperation(service_id, users_id, order_id, 6, 0, 0);
            } else if (2 == service_type) {
                // 退货结束
                PerformOperation(service_id, users_id, order_id, 7, 0, 0);
            }
        }, function() {

        });
    }

    // 确认退款操作
    function Refund(service_id, users_id, order_id, refund_price) {
        layer.confirm("请选择<span style='color: red;'> 退货 </span>操作", {
            title: false,
            btn: ['直接完成'],
        }, function() {
            // 确认
            PerformOperation2(service_id, users_id, order_id, 7, 1, refund_price);
        }, function() {
            PerformOperation2(service_id, users_id, order_id, 7, 0, refund_price);
        });
    }

    // 执行审核操作
    function PerformOperation(service_id, users_id, order_id, status, is_refund, refund_price) {
        layer_loading('正在处理');
        $.ajax({
            url: "<?php echo url('ShopService/after_service_deal_with'); ?>",
            data: {
                service_id: service_id,
                users_id: users_id,
                order_id: order_id,
                status: status,
                is_refund: is_refund,
                refund_price: refund_price,
                _ajax: 1
            },
            type:'post',
            dataType:'json',
            success:function(res) {
                layer.closeAll();
                if (1 == res.code) {
                    // layer.msg(res.msg, {time: 1500}, function() {
                    //     window.location.reload();
                    // });
                } else {
                    layer.msg(res.msg, {time: 1500});
                }
            }
        });
    }
    
    function PerformOperation2(service_id, users_id, order_id, status, is_refund, refund_price) {
        layer_loading('正在处理');
        $.ajax({
            url: "<?php echo url('ShopService/after_service_deal_with'); ?>",
            data: {
                service_id: service_id,
                users_id: users_id,
                order_id: order_id,
                status: status,
                is_refund: is_refund,
                refund_price: refund_price,
                _ajax: 1
            },
            type:'post',
            dataType:'json',
            success:function(res) {
                layer.closeAll();
                if (1 == res.code) {
                    jifen(service_id,users_id);
                    // layer.msg(res.msg, {time: 1500}, function() {
                    //     window.location.reload();
                    // });
                } else {
                    layer.msg(res.msg, {time: 1500});
                }
            }
        });
    }
    function jifen(service_id,users_id){
        $.ajax({
            url: "<?php echo url('Shop/after_service_deal_with'); ?>",
            data: {
                service_id: service_id,
                users_id: users_id,
            },
            type:'post',
            dataType:'json',
            success:function(res) {
                layer.closeAll();
                if (1 == res.code) {
                    layer.msg(res.msg, {time: 1500}, function() {
                        window.location.reload();
                    });
                } else {
                    layer.msg(res.msg, {time: 1500});
                }
            }
        });
    }
</script>

<br/>
<div id="goTop">
    <a href="JavaScript:void(0);" id="btntop">
        <i class="fa fa-angle-up"></i>
    </a>
    <a href="JavaScript:void(0);" id="btnbottom">
        <i class="fa fa-angle-down"></i>
    </a>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#think_page_trace_open').css('z-index', 99999);
    });
</script>
</body>
</html>