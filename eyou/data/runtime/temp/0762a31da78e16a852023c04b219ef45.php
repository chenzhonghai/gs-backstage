<?php if (!defined('THINK_PATH')) exit(); /*a:9:{s:36:"./template/pc/users/users_centre.htm";i:1622085092;s:48:"/mnt/eyou/template/pc/users/skin/css/diy_css.htm";i:1622085090;s:44:"/mnt/eyou/template/pc/users/users_header.htm";i:1622085092;s:42:"/mnt/eyou/template/pc/users/users_left.htm";i:1622085092;s:52:"./public/static/template/users_v2/users_leftmenu.htm";i:1616142504;s:50:"/mnt/eyou/template/pc/users/users_centre_field.htm";i:1622085078;s:63:"./public/static/template/users_v2/users_centre_field_mobile.htm";i:1616029664;s:63:"./public/static/template/users_v2/users_centre_field_extend.htm";i:1616142498;s:44:"/mnt/eyou/template/pc/users/users_footer.htm";i:1622085092;}*/ ?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8" />
    <title>我的信息-<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_name"); echo $__VALUE__; ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <link href="<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_cmspath"); echo $__VALUE__; ?>/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/css/basic.css","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/css/eyoucms.css","","",""); echo $__VALUE__; ?>
    <!-- 新样式 2020-11-25 -->
    <?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/css/element/index.css","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/css/e-user.css","","",""); echo $__VALUE__; ?>
    
<!-- 官方内置样式表，升级会覆盖变动，请勿修改，否则后果自负 -->

<style>
/*
<?php echo $theme_color; ?>
<?php echo hex2rgba($theme_color,0.8); ?> */
    .ey-container .ey-nav ul .active a {
        border-left: <?php echo $theme_color; ?> 3px solid;
        background-color:<?php echo hex2rgba($theme_color,0.1); ?>;
        color:<?php echo $theme_color; ?>;
    }
    .ey-container .ey-nav ul li a:hover {
        color: <?php echo $theme_color; ?>;
    }
    .ey-container .user-box .user-box-text{
        background-color:<?php echo $theme_color; ?>;
    }
    .ey-container .user-box-r .user-box-top .user-top-r .more {
        color:<?php echo $theme_color; ?>;
    }
    .ey-container .user-box-r .user-box-bottom .data-info .link a {
        color: <?php echo $theme_color; ?>;
    }
    .ey-container .column-title .column-name {
        color: <?php echo $theme_color; ?>;
    }
    a:hover {
        color: <?php echo $theme_color; ?>;
    }
    .el-button:focus, .el-button:hover {
        color: <?php echo $theme_color; ?>;
        border-color: <?php echo hex2rgba($theme_color,0.2); ?>;
        background-color:<?php echo hex2rgba($theme_color,0.1); ?>;
    }
    .el-button--primary {
        background-color: <?php echo $theme_color; ?>;
        border-color: <?php echo $theme_color; ?>;
    }
    .ey-container .item-from-row .err a {
        color: <?php echo $theme_color; ?>;
    }
    .ey-container .checkbox-label .checkbox:checked+.check-mark {
        background-color: <?php echo $theme_color; ?>;
        border: 1px solid <?php echo $theme_color; ?>;
    }
    .ey-container .checkbox-label .checkbox:checked+.check-mark:after {
        background:<?php echo $theme_color; ?>;
    }
    .ey-container .radio-label .radio:checked+.check-mark {
        border: 1px solid <?php echo $theme_color; ?>;
        background-color: <?php echo $theme_color; ?>;
    }
    .ey-container .radio-label .radio:checked+.check-mark:after {
        background: <?php echo $theme_color; ?>;
    }
    .el-button--primary.is-plain {
        color: <?php echo $theme_color; ?>;
        border-color: <?php echo hex2rgba($theme_color,0.2); ?>;
        background-color:<?php echo hex2rgba($theme_color,0.1); ?>;
    }
    .el-button--primary.is-plain:focus, .el-button--primary.is-plain:hover {
        background: <?php echo $theme_color; ?>;
        border-color: <?php echo $theme_color; ?>;
    }
    .el-button--primary:hover, .el-button--primary:focus {
        background: <?php echo hex2rgba($theme_color,0.9); ?>;
        border-color: <?php echo hex2rgba($theme_color,0.9); ?>;
        color: #fff;
    }
    .el-input-number__decrease:hover, .el-input-number__increase:hover {
        color: <?php echo $theme_color; ?>;
    }
    .el-input-number__decrease:hover:not(.is-disabled)~.el-input .el-input__inner:not(.is-disabled), .el-input-number__increase:hover:not(.is-disabled)~.el-input .el-input__inner:not(.is-disabled) {
        border-color: <?php echo $theme_color; ?>;
    }
    .ey-container .address-con .address-item.cur {
        border: 1px solid <?php echo hex2rgba($theme_color,0.6); ?>;
    }
    .ey-container .address-con .address-item.cur:before {
        color: <?php echo $theme_color; ?>;
        border-top: 50px solid <?php echo $theme_color; ?>;
    }
    .ey-container .ey-con .shop-oper .shop-oper-l .el-button.active {
        color: #FFF;
        background-color: <?php echo $theme_color; ?>;
        border-color: <?php echo $theme_color; ?>;
    }
    .ey-container .goods-con .goods-item .goods-item-r .view a {
        color: <?php echo $theme_color; ?>;
    }
    .ey-container div.dataTables_paginate .paginate_button.active>a, .ey-containerdiv.dataTables_paginate .paginate_button.active>a:focus, .ey-containerdiv.dataTables_paginate .paginate_button.active>a:hover {
        background: <?php echo $theme_color; ?> !important;
        border-color: <?php echo $theme_color; ?> !important;
    }
    .ey-container .pagination>li>a:focus, .ey-container .pagination>li>a:hover, .ey-container .pagination>li>span:focus, .ey-container .pagination>li>span:hover {
        color:<?php echo $theme_color; ?>;
    }
    .ey-container .pagination>li>a, .ey-container .pagination>li>span {
        color:<?php echo $theme_color; ?>;
    }
    .el-button--danger {
        background-color: <?php echo $theme_color; ?>;
        border-color: <?php echo $theme_color; ?>;
    }
    .recharge .pc-vip-list.active {
        border: <?php echo $theme_color; ?> 2px solid;
        /*background-color:<?php echo hex2rgba($theme_color,0.1); ?>;*/
    }
    .recharge .pc-vip-list:hover {
    	border: <?php echo $theme_color; ?> 2px solid;
    }
    .recharge .pc-vip-list .icon-recomd {
    	background: <?php echo $theme_color; ?>;
    }
    .el-input.is-active .el-input__inner, .el-input__inner:focus {
        border-color: <?php echo $theme_color; ?>;
    }
    .ey-container .address-con .selected .address-item:before {
        color:<?php echo $theme_color; ?>;
        border-top: 50px solid <?php echo $theme_color; ?>;
    }
    .ey-container .address-con .selected .address-item {
        border: 1px solid <?php echo hex2rgba($theme_color,0.4); ?>;
    }
    .ey-container .pay-type .payTag a {
        color: <?php echo $theme_color; ?>;
        border-bottom: 1px solid <?php echo $theme_color; ?>;
    }
    .ey-container .pay-type .pay-con .pay-type-item.active {
        border-color: <?php echo hex2rgba($theme_color,0.4); ?>;
    }
    .ey-container .pay-type .pay-con .pay-type-item.active:before {
        color: <?php echo $theme_color; ?>;
        border-bottom: 30px solid <?php echo $theme_color; ?>;
    }
    .ey-header-nav .nav li ul li a:hover {
        background: <?php echo $theme_color; ?>;
    }
    .btn-primary {
        background-color: <?php echo $theme_color; ?>;
    }
    .btn-primary.focus, .btn-primary:focus, .btn-primary:hover {
        border-color: <?php echo $theme_color; ?>;
        background-color: <?php echo $theme_color; ?>;
    }
    .btn-primary.active.focus, .btn-primary.active:focus, .btn-primary.active:hover, .btn-primary:active.focus, .btn-primary:active:focus, .btn-primary:active:hover, .open>.btn-primary.dropdown-toggle.focus, .open>.btn-primary.dropdown-toggle:focus, .open>.btn-primary.dropdown-toggle:hover {
    	border-color:<?php echo $theme_color; ?>;
    	background-color:<?php echo $theme_color; ?>;
    }
    .login-link a {
        color: <?php echo $theme_color; ?>;
    }
    .register_index .login_link .login_link_register {
        color: <?php echo $theme_color; ?>;
    }

</style>

<script type="text/javascript">
    var __root_dir__ = "";
    var __version__ = "<?php echo $version; ?>";
    var __lang__ = "<?php echo $home_lang; ?>";
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
</script>

    <?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/static/common/js/jquery.min.js","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/js/bootstrap.min.js","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/js/global.js","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/plugins/layer-v3.1.0/layer.js","","",""); echo $__VALUE__; ?>
</head>

<body class="centre">
<!-- 头部 -->
<!-- 头部信息 -->
<div class="ey-header">
    <div class="ey-logo">
        <a href="<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_cmsurl"); echo $__VALUE__; ?>"><img src="<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_logo"); echo $__VALUE__; ?>" /></a>
    </div>
    <div class="ey-header-nav w1200">
        <ul class="nav nav-menu nav-inline">
            <li><a href="<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_cmsurl"); echo $__VALUE__; ?>"> 首页 </a></li>
            <?php  if(isset($ui_typeid) && !empty($ui_typeid)) : $typeid = $ui_typeid; else: $typeid = ""; endif; if(empty($typeid) && isset($channelartlist["id"]) && !empty($channelartlist["id"])) : $typeid = intval($channelartlist["id"]); endif;  if(isset($ui_row) && !empty($ui_row)) : $row = $ui_row; else: $row = 60; endif; $tagChannel = new \think\template\taglib\eyou\TagChannel; $_result = $tagChannel->getChannel($typeid, "top", "", ""); if(is_array($_result) || $_result instanceof \think\Collection || $_result instanceof \think\Paginator): $i = 0; $e = 1;$__LIST__ = is_array($_result) ? array_slice($_result,0, $row, true) : $_result->slice(0, $row, true); if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$field): $field["typename"] = text_msubstr($field["typename"], 0, 100, false); $__LIST__[$key] = $_result[$key] = $field;$i= intval($key) + 1;$mod = ($i % 2 ); ?>
            <li>
                <a href="<?php echo $field['typeurl']; ?>"> <?php echo $field['typename']; if(!(empty($field['children']) || (($field['children'] instanceof \think\Collection || $field['children'] instanceof \think\Paginator ) && $field['children']->isEmpty()))): ?><i class="fa fa-angle-down margin-small-left"></i><?php endif; ?></a>
                <?php if(!(empty($field['children']) || (($field['children'] instanceof \think\Collection || $field['children'] instanceof \think\Paginator ) && $field['children']->isEmpty()))): ?>
                <ul class="drop-menu">
                    <?php  if(isset($ui_typeid) && !empty($ui_typeid)) : $typeid = $ui_typeid; else: $typeid = ""; endif; if(empty($typeid) && isset($channelartlist["id"]) && !empty($channelartlist["id"])) : $typeid = intval($channelartlist["id"]); endif;  if(isset($ui_row) && !empty($ui_row)) : $row = $ui_row; else: $row = 100; endif;if(is_array($field['children']) || $field['children'] instanceof \think\Collection || $field['children'] instanceof \think\Paginator): $i = 0; $e = 1;$__LIST__ = is_array($field['children']) ? array_slice($field['children'],0,100, true) : $field['children']->slice(0,100, true); if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$field2): $field2["typename"] = text_msubstr($field2["typename"], 0, 100, false); $__LIST__[$key] = $_result[$key] = $field2;$i= intval($key) + 1;$mod = ($i % 2 ); ?>
                    <li><a href="<?php echo $field2['typeurl']; ?>"><?php echo $field2['typename']; ?></a></li>
                    <?php ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $field2 = []; ?>
                </ul>
                <?php endif; ?>
            
            </li>
            <?php ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $field = []; ?>
         </ul>
    </div>
    <div class="ey-header-r">
        <?php if($php_servicemeal >= '1'): ?>
        <div class="right-item user-news">
            <a href="<?php echo url('user/UsersNotice/index'); ?>">
                <?php if($unread_num > '0'): ?>
                <span class="num" id="users_unread_num"><?php echo $unread_num; ?></span>
                <?php endif; ?>
                <span class="icon"><i class="el-icon-bell"></i></span>
            </a>
        </div>
        <?php endif; ?>
        <div class="right-item user-photo">
            <img src="<?php echo get_head_pic($users['head_pic']); ?>"/>
            <div class="user-drop">
                <ul>
                    <li><a href="<?php  $tagDiyurl = new \think\template\taglib\eyou\TagDiyurl; $__VALUE__ = $tagDiyurl->getDiyurl("", "user/Users/index", "", "", "", "", "", "", "ey_active"); echo $__VALUE__; ?>">个人中心</a></li>
                    <?php if(getUsersConfigData('shop.shop_open') == 1): ?>
                    <li><a href="<?php  $tagDiyurl = new \think\template\taglib\eyou\TagDiyurl; $__VALUE__ = $tagDiyurl->getDiyurl("", "user/Shop/shop_centre", "", "", "", "", "", "", "ey_active"); echo $__VALUE__; ?>">我的订单</a></li>
                    <?php endif; ?>
                    <li><a href="<?php  $tagDiyurl = new \think\template\taglib\eyou\TagDiyurl; $__VALUE__ = $tagDiyurl->getDiyurl("", "user/Users/collection_index", "", "", "", "", "", "", "ey_active"); echo $__VALUE__; ?>">我的收藏</a></li>
                    <li><a href="<?php  $tagDiyurl = new \think\template\taglib\eyou\TagDiyurl; $__VALUE__ = $tagDiyurl->getDiyurl("", "user/Users/logout", "", "", "", "", "", "", "ey_active"); echo $__VALUE__; ?>">安全退出</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- 头部信息结束 -->

<script type="text/javascript">
    //头像下拉
    $(".user-photo").mouseover(function(){
       $(".user-drop").show();
    });
    $(".user-photo").mouseout(function(){
       $(".user-drop").hide();
    });

    var GetUploadify_url = "<?php echo url('Uploadify/upload'); ?>";
</script>
<!-- 头部结束 -->

<div class="ey-body-bg">
    <div class="ey-body">
        <div class="ey-container w1200">
            <!-- 侧边 -->
            <div class="ey-nav fl">
    <div class="sidebar-box">
        <ul> 
    <li>
        <div class="title">会员中心</div>
    </li>
    <?php  $tagUsermenu = new \think\template\taglib\eyou\TagUsermenu; $_result = $tagUsermenu->getUsermenu("active", ""); if(is_array($_result) || $_result instanceof \think\Collection || $_result instanceof \think\Paginator): $i = 0; $e = 1; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$field): $i= intval($key) + 1;$mod = ($i % 2 ); ?>
    <li class="<?php echo $field['currentstyle']; ?>">
        <a href="<?php echo $field['url']; ?>"><?php echo $field['title']; ?></a>
    </li>
    <?php ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $field = []; ?>
</ul>

<?php if(isset($usersConfig['shop_open']) && $usersConfig['shop_open'] == 1 && $php_servicemeal > 1): ?>
<ul>
    <li>
        <div class="title">商城中心</div>
    </li>
    <li <?php if(MODULE_NAME == 'user' && CONTROLLER_NAME == 'Shop' && ACTION_NAME == 'shop_cart_list'): ?> class="active" <?php endif; ?>>
        <a href="<?php echo url("user/Shop/shop_cart_list","",true,false,null,null,null);?>">购物车</a>
    </li>
    <li <?php if(MODULE_NAME == 'user' && CONTROLLER_NAME == 'Shop' && ACTION_NAME == 'shop_address_list'): ?> class="active" <?php endif; ?>>
        <a href="<?php echo url("user/Shop/shop_address_list","",true,false,null,null,null);?>">收货地址</a>
    </li>
    <li <?php if(MODULE_NAME == 'user' && (CONTROLLER_NAME == 'Shop' && in_array(ACTION_NAME, ['shop_centre', 'shop_order_details', 'after_service', 'after_service_details', 'after_service_apply']) || (CONTROLLER_NAME == 'ShopComment' && in_array(ACTION_NAME, ['product'])))): ?> class="active" <?php endif; ?>>
        <a href="<?php echo url("user/Shop/shop_centre","",true,false,null,null,null);?>">我的订单</a>
    </li>
    <?php if($php_servicemeal >= '2'): ?>
    <li <?php if(MODULE_NAME == 'user' && CONTROLLER_NAME == 'ShopComment' && in_array(ACTION_NAME, ['index'])): ?> class="active" <?php endif; ?>>
        <a href="<?php echo url("user/ShopComment/index","",true,false,null,null,null);?>"> 我的评价</a>
    </li>
    <?php endif; ?>
</ul>
<?php endif; if(isset($usersConfig['users_open_release']) && $usersConfig['users_open_release'] == 1): ?>
<ul>
    <li>
        <div class="title">投稿中心</div>
    </li>
    <li <?php if(MODULE_NAME == 'user' && CONTROLLER_NAME == 'UsersRelease' && ACTION_NAME == 'release_centre'): ?> class="active" <?php endif; ?>>
        <a href="<?php echo url('user/UsersRelease/release_centre', ['list'=>1]); ?>">投稿列表</a>
    </li>
    <li <?php if(MODULE_NAME == 'user' && CONTROLLER_NAME == 'UsersRelease' && (ACTION_NAME == 'article_add' || ACTION_NAME == 'article_edit')): ?> class="active" <?php endif; ?>>
        <a href="<?php echo url("user/UsersRelease/article_add","",true,false,null,null,null);?>">我要投稿</a>
    </li>
</ul>
<?php endif; ?>
    </div>
</div>
            <!-- 侧边结束 -->
            <!-- 中部 -->
            <div class="ey-con fr" >
                <div class="el-main main-bg">
                    <div class="column-title mb20">
                        <div class="column-name"><?php echo $eyou['field']['title']; ?></div>
                    </div>
                    <div class="user-info">
                        <div class="face fl">
                            <div tabindex="0" class="el-upload el-upload--text" onclick="$('#ey_head_pic').trigger('click');">
                                <img id="ey_head_pic_a" class="img-fluid rounded-circle" src="<?php echo get_head_pic($users['head_pic']); ?>"/>
                                <span title="编辑头像" class="el-icon-edit-outline"></span>
                            </div>
                            <input type="file" name="ey_head_pic" id="ey_head_pic" data-max_file_size="2" onchange="upload_head_pic(this)" style="display: none;">
                        </div> 
                        <div class="info fr">
                            <div class="item-from-row">
                                <div class="from-row-l">
                                    用户名：
                                </div>
                                <div class="from-row-r">
                                    <?php echo $users['username']; ?>
                                </div>
                            </div>
                            <form name='theForm' id="theForm">
                                <?php if(empty($users['thirdparty']) || (($users['thirdparty'] instanceof \think\Collection || $users['thirdparty'] instanceof \think\Paginator ) && $users['thirdparty']->isEmpty())): ?>
                                <div class="item-from-row">
                                    <div class="from-row-l">
                                        修改密码：
                                    </div>
                                    <div class="from-row-r">
                                        <input type="text" name="password_edit" id="password_edit" autocomplete="off" class="el-input__inner w300" placeholder="留空时默认不修改密码">
                                    </div>
                                </div>
                                <?php endif; if(empty($users['password']) || (($users['password'] instanceof \think\Collection || $users['password'] instanceof \think\Paginator ) && $users['password']->isEmpty())): ?>
                                <div class="item-from-row">
                                    <div class="from-row-l">
                                        设置密码：
                                    </div>
                                    <div class="from-row-r">
                                        <input type="text" name="password" id="password" autocomplete="off" class="el-input__inner w300"> 微信注册用户，请设置密码。
                                    </div>
                                </div>
                                <?php endif; ?>
                                <div class="item-from-row">
                                    <div class="from-row-l">
                                        昵称：
                                    </div>
                                    <div class="from-row-r">
                                         <input type="text" name="nickname" value="<?php echo $users['nickname']; ?>" autocomplete="off" class="el-input__inner w300">
                                    </div>
                                </div>
                                
                                <div class="item-from-row">
                                    <div class="from-row-l">
                                        会员等级：
                                    </div>
                                    <div class="from-row-r">
                                        <?php echo $users['level_name']; ?>
                                         
                                    </div>
                                </div>
                                
                                <!-- 更多资料中的会员属性 -->
                                <?php if(is_array($users_para) || $users_para instanceof \think\Collection || $users_para instanceof \think\Paginator): $i = 0; $e = 1; $__LIST__ = $users_para;if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$vo): $i= intval($key) + 1;$mod = ($i % 2 ); switch($vo['dtype']): case "hidden": ?>
            <!-- 隐藏域 start -->
            <div class="item-from-row" style="display: none;">
                <dt class="tit">
                    &nbsp;&nbsp;<label><?php echo $vo['title']; ?></label>
                </dt>
                <dd class="opt">
                    <input type="hidden" class="input-txt" id="<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>" name="<?php echo $vo['fieldArr']; ?>[<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>]" value="<?php echo (isset($vo['dfvalue']) && ($vo['dfvalue'] !== '')?$vo['dfvalue']:''); ?>">
                    <span class="err"></span>
                    <p class="notic"><?php echo (isset($vo['remark']) && ($vo['remark'] !== '')?$vo['remark']:''); ?></p>
                </dd>
            </div>
            <!-- 隐藏域 start -->
        <?php break; case "mobile": ?>
            <!-- 手机文本框 start -->
            <div class="item-from-row">
    <div class="from-row-l">
		<?php if(1 == $vo['is_required']): ?>
			<span class="red">*</span>
		<?php else: ?>
			<span class="red"></span>
		<?php endif; ?>
		<?php echo $vo['title']; ?>：
	</div>
    <div class="from-row-r">
        <?php echo (isset($vo['dfvalue']) && ($vo['dfvalue'] !== '')?$vo['dfvalue']:''); if($users['is_mobile'] == '1'): ?>
            <span class="err"><a href="JavaScript:void(0);" onclick="get_<?php echo $vo['fieldArr']; ?><?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>_mobile_code('更改手机');">（更改手机）</a></span>
        <?php else: ?>
            <span class="err"><a href="JavaScript:void(0);" onclick="get_<?php echo $vo['fieldArr']; ?><?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>_mobile_code('绑定手机');">（绑定手机）</a></span>
        <?php endif; ?>
        <script type="text/javascript">
            function get_<?php echo $vo['fieldArr']; ?><?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>_mobile_code(title) {
                var url = '<?php echo $RootDir; ?>/index.php?m=user&c=Users&a=bind_mobile';
                if (url.indexOf('?') > -1) {
                    url += '&';
                } else {
                    url += '?';
                }
                url += 'title=' + title;
                //iframe窗
                layer.open({
                    type: 2,
                    title: title,
                    shadeClose: false,
                    maxmin: false, //开启最大化最小化按钮
                    area: ['350px', '300px'],
                    content: url
                });
            }
        </script> 
        <p class="notic"><?php echo (isset($vo['remark']) && ($vo['remark'] !== '')?$vo['remark']:''); ?></p>
    </div>
</div>
            <!-- 手机文本框 end -->
        <?php break; case "email": ?>
            <!-- 邮箱文本框 start -->
            <div class="item-from-row">
            <div class="from-row-l"><?php echo $vo['title']; ?>：</div>
                <div class="from-row-r">
                    <?php echo (isset($vo['dfvalue']) && ($vo['dfvalue'] !== '')?$vo['dfvalue']:''); if($users['is_email'] == '1'): ?>
                        <span class="err"><a href="JavaScript:void(0);" onclick="get_<?php echo $vo['fieldArr']; ?><?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>_email_code('更改邮箱');">（更改邮箱）</a></span>
                    <?php else: ?>
                        <span class="err"><a href="JavaScript:void(0);" onclick="get_<?php echo $vo['fieldArr']; ?><?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>_email_code('绑定邮箱');">（绑定邮箱）</a></span>
                    <?php endif; ?>
                    <p class="notic"><?php echo (isset($vo['remark']) && ($vo['remark'] !== '')?$vo['remark']:''); ?></p>
                </div>
            </div>
            <script type="text/javascript">
                function get_<?php echo $vo['fieldArr']; ?><?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>_email_code(title)
                {
                    var url = "<?php echo url("user/Users/bind_email","",true,false,null,null,null);?>";
                    if (url.indexOf('?') > -1) {
                        url += '&';
                    } else {
                        url += '?';
                    }
                    url += 'title='+title;
                    //iframe窗
                    layer.open({
                        type: 2,
                        title: title,
                        shadeClose: false,
                        maxmin: false, //开启最大化最小化按钮
                        area: ['350px', '300px'],
                        content: url
                    });
                }
            </script>
            <!-- 邮箱文本框 end -->
        <?php break; case "text": ?>
            <!-- 单行文本框 start -->
            <div class="item-from-row">
                <div class="from-row-l"><?php echo $vo['title']; ?>：
					<?php if(1 == $vo['is_required']): ?>
						<span class="red">*</span>
					<?php else: ?>
						<span class="red"></span>
					<?php endif; ?>
				</div>
                <div class="from-row-r">
                    <input type="text" class="input-txt" id="<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>" name="<?php echo $vo['fieldArr']; ?>[<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>]" value="<?php echo (isset($vo['dfvalue']) && ($vo['dfvalue'] !== '')?$vo['dfvalue']:''); ?>" autocomplete="off"><?php echo (isset($vo['dfvalue_unit']) && ($vo['dfvalue_unit'] !== '')?$vo['dfvalue_unit']:''); ?>
                    <span class="err"></span>
                    <p class="notic"><?php echo (isset($vo['remark']) && ($vo['remark'] !== '')?$vo['remark']:''); ?></p>
                </div>
            </div>
            <!-- 单行文本框 end -->
        <?php break; case "multitext": ?>
            <!-- 多行文本框 start -->
            <div class="item-from-row">
                <div class="from-row-l">
					<?php if(1 == $vo['is_required']): ?>
					    <span class="red">*</span>
					<?php else: ?>
					    <span class="red"></span>
					<?php endif; ?>
					<?php echo $vo['title']; ?>：
				</div>
                <div class="from-row-r">
                    <textarea id="<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>" name="<?php echo $vo['fieldArr']; ?>[<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>]" class="w300" autocomplete="off"><?php echo (isset($vo['dfvalue']) && ($vo['dfvalue'] !== '')?$vo['dfvalue']:''); ?></textarea>
                    <span class="err"></span>
                    <p class="notic"><?php echo (isset($vo['remark']) && ($vo['remark'] !== '')?$vo['remark']:''); ?></p>
                </div>
            </div>
            <!-- 多行文本框 end -->
        <?php break; case "checkbox": ?>
            <!-- 复选框 start -->
            <div class="item-from-row">
                <div class="from-row-l"><?php echo $vo['title']; ?>：</div>
                <div class="from-row-r">
                    <?php if(is_array($vo['dfvalue']) || $vo['dfvalue'] instanceof \think\Collection || $vo['dfvalue'] instanceof \think\Paginator): $i = 0; $e = 1; $__LIST__ = $vo['dfvalue'];if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$v2): $i= intval($key) + 1;$mod = ($i % 2 ); ?>
					<label class="checkbox-label">
						<span><?php echo $v2; ?></span>
						<input type="checkbox" class="checkbox" name="<?php echo $vo['fieldArr']; ?>[<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>][]" value="<?php echo $v2; ?>" <?php if(isset($vo['trueValue']) AND in_array($v2, $vo['trueValue'])): ?>checked="checked"<?php endif; ?>>
					    <span class="check-mark"></span>
					</label>
                    <?php echo isset($v2["ey_1563185380"])?$v2["ey_1563185380"]:""; ?><?php echo (1 == $e && isset($v2["ey_1563185376"]))?$v2["ey_1563185376"]:""; ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $v2 = []; if(1 == $vo['is_required']): ?>
                        （必选）
                    <?php endif; ?>
                    <span class="err"></span>
                    <p class="notic"><?php echo (isset($vo['remark']) && ($vo['remark'] !== '')?$vo['remark']:''); ?></p>
                    
                </div>
            </div>
            <!-- 复选框 end -->
        <?php break; case "radio": ?>
            <!-- 单选项 start -->
            <div class="item-from-row">
                <div class="from-row-l"><?php echo $vo['title']; ?>：</div>
                <div class="from-row-r">
                    <?php if(is_array($vo['dfvalue']) || $vo['dfvalue'] instanceof \think\Collection || $vo['dfvalue'] instanceof \think\Paginator): $i = 0; $e = 1; $__LIST__ = $vo['dfvalue'];if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$v2): $i= intval($key) + 1;$mod = ($i % 2 ); ?>
					<label class="radio-label">
						<span><?php echo $v2; ?></span>
						<input type="radio" class="radio" name="<?php echo $vo['fieldArr']; ?>[<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>]" value="<?php echo $v2; ?>" <?php if(isset($vo['trueValue']) AND in_array($v2, $vo['trueValue'])): ?>checked="checked"<?php endif; ?>>
					    <span class="check-mark"></span>
					</label>
                    <?php echo isset($v2["ey_1563185380"])?$v2["ey_1563185380"]:""; ?><?php echo (1 == $e && isset($v2["ey_1563185376"]))?$v2["ey_1563185376"]:""; ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $v2 = []; if(1 == $vo['is_required']): ?>
                        （必选）
                    <?php endif; ?>
                    <span class="err"></span>
                    <p class="notic"><?php echo (isset($vo['remark']) && ($vo['remark'] !== '')?$vo['remark']:''); ?></p>
                </div>
            </div>
            <!-- 单选项 end -->
        <?php break; case "select": ?>
            <!-- 下拉框 start -->
            <div class="item-from-row">
                <div class="from-row-l">
					<?php if(1 == $vo['is_required']): ?>
						<span class="red">*</span>
					<?php endif; ?>
					<?php echo $vo['title']; ?>：
				</div>
                <div class="from-row-r">
					<div class="w300 select">
						<select name="<?php echo $vo['fieldArr']; ?>[<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>]" id="<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>">
						    <option value="">请选择</option>
						    <?php if(is_array($vo['dfvalue']) || $vo['dfvalue'] instanceof \think\Collection || $vo['dfvalue'] instanceof \think\Paginator): $i = 0; $e = 1; $__LIST__ = $vo['dfvalue'];if( count($__LIST__)==0 ) : echo htmlspecialchars_decode("");else: foreach($__LIST__ as $key=>$v2): $i= intval($key) + 1;$mod = ($i % 2 ); ?>
						        <option value="<?php echo $v2; ?>" <?php if(isset($vo['trueValue']) AND in_array($v2, $vo['trueValue'])): ?>selected<?php endif; ?>><?php echo $v2; ?></option>
						    <?php echo isset($v2["ey_1563185380"])?$v2["ey_1563185380"]:""; ?><?php echo (1 == $e && isset($v2["ey_1563185376"]))?$v2["ey_1563185376"]:""; ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $v2 = []; ?>
						</select>
					</div>
                    <span class="err"></span>
                    <p class="notic"><?php echo (isset($vo['remark']) && ($vo['remark'] !== '')?$vo['remark']:''); ?></p>
                </div>
            </div>
            <!-- 下拉框 end -->
        <?php break; ?>
        <!-- 扩展 start -->
        <!-- 扩展 -->
<?php case "img": ?>
    <!-- 单张图 start -->
    <div class="item-from-row">
        <div class="from-row-l">
            <?php echo $vo['title']; ?>：
        </div>
        <div class="from-row-r">
            <div tabindex="0" class="input-file-show"  onclick="$('#upload_single_pic_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>').trigger('click');" style="cursor: pointer;">
                <img id="single_img_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>" class="img-fluid img1_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>" src="<?php echo (isset($vo['info']) && ($vo['info'] !== '')?$vo['info']:'/public/static/common/images/not_upload_pic.png'); ?>"/>
            </div>
        </div>
        <input type="file" name="upload_single_pic" id="upload_single_pic_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>" onchange="upload_single_pic_1609837252(this,'<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>')" style="display: none;">
        <input type="hidden" class="type-file-text" id="single_pic_input_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>" name="<?php echo $vo['fieldArr']; ?>[<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>]" value="<?php echo (isset($vo['info']) && ($vo['info'] !== '')?$vo['info']:''); ?>">
    </div>
    <!-- 单张图 end -->
<?php break; case "file":  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/plugins/layui/css/layui.css","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/plugins/layui/layui.js","","",""); echo $__VALUE__; ?>
    <!-- 单个文件 start -->
    <div class="item-from-row">
        <div class="from-row-l">
            <?php echo $vo['title']; ?>：
        </div>
        <div class="from-row-r" >
            <a id="download_file_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>" style="margin-right: 30px;text-decoration: underline;<?php if(!(empty($vo['info']) || (($vo['info'] instanceof \think\Collection || $vo['info'] instanceof \think\Paginator ) && $vo['info']->isEmpty()))): ?>display: '';<?php else: ?>display: none;<?php endif; ?>"
               <?php if(!(empty($vo['info']) || (($vo['info'] instanceof \think\Collection || $vo['info'] instanceof \think\Paginator ) && $vo['info']->isEmpty()))): ?> href="<?php echo (isset($vo['info']) && ($vo['info'] !== '')?$vo['info']:''); ?>" download="<?php echo get_filename($vo['info']); ?>" <?php endif; ?>>
            <img src="/public/static/common/images/file.png" alt="" style="width: 16px;height:  16px;">下载附件</a>
            <input type="text" id="<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>" name="<?php echo $vo['fieldArr']; ?>[<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>]" value="<?php echo (isset($vo['info']) && ($vo['info'] !== '')?$vo['info']:''); ?>" style="display: none;">
            <button type="button" class="el-button el-button--primary is-plain el-button--small" id="upload_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>">上传文件</button>
        </div>
    </div>
    <script type="text/javascript">
        $(function(){
            layui.use('upload', function(){
                var upload = layui.upload,
                    layer = layui.layer;

                //执行实例
                var uploadInst = upload.render({
                    elem: "#upload_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>" //绑定元素
                    ,url: "<?php echo url('user/Uploadify/DownloadUploadFileAjax'); ?>"
                    ,accept: 'file' //普通文件
                    ,exts: '<?php echo $vo['ext']; ?>'
                    ,size: <?php echo $vo['filesize']; ?> //限制文件大小，单位 KB
                    ,done: function(res){
                        if (res.state=="SUCCESS"){
                            layer.msg('上传成功!')
                            $("#<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>").val(res.url);
                            $("#download_file_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>").css('display','');
                            $("#download_file_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>").attr('href',res.url);
                            var arr = res.url.split("/");
                            var download = arr[arr.length-1];
                            $("#download_file_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>").attr('download',download);
                            $("#download_file_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>").css('color','#000');
                            $("#download_file_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>").html('<img src="/public/static/common/images/file.png" alt="" style="width: 16px;height:  16px;">下载附件');
                        }else {
                            $("#download_file_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>").css('display','');
                            $("#download_file_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>").css('color','red');
                            $("#download_file_<?php echo $vo['fieldArr']; ?>_<?php echo (isset($vo['name']) && ($vo['name'] !== '')?$vo['name']:''); ?>").text(res.state);
                        }
                    }
                    ,error: function(){
                        //请求异常回调
                    }
                });
            });
        })
    </script>
    <!-- 单个文件 end -->
<?php break; ?>
        <!-- 扩展 end -->
    <?php endswitch; ?>
<?php echo isset($vo["ey_1563185380"])?$vo["ey_1563185380"]:""; ?><?php echo (1 == $e && isset($vo["ey_1563185376"]))?$vo["ey_1563185376"]:""; ++$e; endforeach; endif; else: echo htmlspecialchars_decode("");endif; $vo = []; ?>
                                <!-- 结束 -->
                                <div class="item-from-row">
                                    <div class="from-row-l">
                                        &nbsp;
                                    </div>
                                    <div class="from-row-r">
                                        <input type="button" onclick="UpdateUsersData();" class="el-button el-button--primary el-button--medium" value="保存资料"/>
                                    </div>
                                </div>
                            </form>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
            <!-- 中部结束 -->
        </div>
    </div>
</div>

<?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("users/skin/js/users_centre.js","","",""); echo $__VALUE__; ?>

<!-- 底部 -->
	<div class="ey-footer">
    <footer class="container">
        <p><?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_copyright"); echo $__VALUE__; ?></p>
    </footer>
</body>
</html>