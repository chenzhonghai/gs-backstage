<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:45:"./application/admin/template/shop/voucher.htm";i:1642402222;s:57:"/mnt/api/api/application/admin/template/public/layout.htm";i:1640913882;s:60:"/mnt/api/api/application/admin/template/public/theme_css.htm";i:1640913882;s:57:"/mnt/api/api/application/admin/template/public/footer.htm";i:1640913882;}*/ ?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link href="/api/public/plugins/layui/css/layui.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/main.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/page.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/font/css/font-awesome.min.css" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="/api/public/static/admin/font/css/font-awesome-ie7.min.css">
<![endif]-->
<script type="text/javascript">
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
    var module_name = "<?php echo MODULE_NAME; ?>";
    var GetUploadify_url = "<?php echo url('Uploadify/upload'); ?>";
    var __root_dir__ = "/api";
    var __lang__ = "<?php echo $admin_lang; ?>";
</script>  
<link href="/api/public/static/admin/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="/api/public/static/admin/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
<!-- <link type="text/css" rel="stylesheet" href="/api/public/plugins/tags_input/css/jquery.tagsinput.css?v=<?php echo $version; ?>"> -->
<style type="text/css">html, body { overflow: visible;}</style>
<link href="/api/public/static/admin/css/diy_style.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css" />

<!-- 官方内置样式表，升级会覆盖变动，请勿修改，否则后果自负 -->

<style type="text/css">
	/*左侧收缩图标*/
	#foldSidebar i { font-size: 24px;color:<?php echo $global['web_theme_color']; ?>; }
    /*左侧菜单*/
    .eycms_cont_left{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl dd a:hover,.eycms_cont_left dl dd a.on,.eycms_cont_left dl dt.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl dd a:active{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist dd{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl.jslist dd a:hover,.eycms_cont_left dl.jslist dd a.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist:hover{ background:<?php echo $global['web_assist_color']; ?>; }
    /*栏目操作*/
    .cate-dropup .cate-dropup-con a:hover{ background-color: <?php echo $global['web_theme_color']; ?>; }
    /*按钮*/
    a.ncap-btn-green { background-color: <?php echo $global['web_theme_color']; ?>; }
    a:hover.ncap-btn-green { background-color: <?php echo $global['web_assist_color']; ?>; }
    .flexigrid .sDiv2 .btn:hover { background-color: <?php echo $global['web_theme_color']; ?>; }
    .flexigrid .mDiv .fbutton div.add{background-color: <?php echo $global['web_theme_color']; ?>; border: none;}
    .flexigrid .mDiv .fbutton div.add:hover{ background-color: <?php echo $global['web_assist_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds{color:<?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds:hover{ background-color: <?php echo $global['web_theme_color']; ?>;}
    /*选项卡字体*/
    .tab-base a.current,
    .tab-base a:hover.current { border-bottom: solid 2px <?php echo $global['web_theme_color']; ?> !important;color: <?php echo $global['web_theme_color']; ?> !important;}
    .addartbtn input.btn:hover{ border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>; }
    .addartbtn input.btn.selected{ color: <?php echo $global['web_theme_color']; ?>;border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>;}
	/*会员导航*/
	.member-nav-group .member-nav-item .btn.selected{background: <?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;box-shadow: -1px 0 0 0 <?php echo $global['web_theme_color']; ?>;}
	.member-nav-group .member-nav-item:first-child .btn.selected{border-left: 1px solid <?php echo $global['web_theme_color']; ?> !important;}
	/*搜索按钮图标*/
	.flexigrid .sDiv2 .fa-search{}
        
    /* 商品订单列表标题 */
   .flexigrid .mDiv .ftitle h3 {border-left: 3px solid <?php echo $global['web_theme_color']; ?>;} 
	
    /*分页*/
    .pagination > .active > a, .pagination > .active > a:focus, 
	.pagination > .active > a:hover, 
	.pagination > .active > span, 
	.pagination > .active > span:focus, 
	.pagination > .active > span:hover { border-color: <?php echo $global['web_theme_color']; ?>;color:<?php echo $global['web_theme_color']; ?>; }
    
    .layui-form-onswitch {border-color: <?php echo $global['web_theme_color']; ?> !important;background-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-enable.selected { background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-disable.selected {background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    input[type="text"]:focus,
    input[type="text"]:hover,
    input[type="text"]:active,
    input[type="password"]:focus,
    input[type="password"]:hover,
    input[type="password"]:active,
    textarea:hover,
    textarea:focus,
    textarea:active { border-color:<?php echo hex2rgba($global['web_theme_color'],0.8); ?>;box-shadow: 0 0 0 2px <?php echo hex2rgba($global['web_theme_color'],0.15); ?> !important;}
    .input-file-show:hover .type-file-button {background-color:<?php echo $global['web_theme_color']; ?> !important; }
    .input-file-show:hover {border-color: <?php echo $global['web_theme_color']; ?> !important;box-shadow: 0 0 5px <?php echo hex2rgba($global['web_theme_color'],0.5); ?> !important;}
    .input-file-show:hover span.show a,
    .input-file-show span.show a:hover { color: <?php echo $global['web_theme_color']; ?> !important;}
    .input-file-show:hover .type-file-button {background-color: <?php echo $global['web_theme_color']; ?> !important; }
    .color_z { color: <?php echo $global['web_theme_color']; ?> !important;}
    a.imgupload{
        background-color: <?php echo $global['web_theme_color']; ?> !important;
        border-color: <?php echo $global['web_theme_color']; ?> !important;
    }
    /*专题节点按钮*/
    .ncap-form-default .special-add{background-color:<?php echo $global['web_theme_color']; ?>;border-color:<?php echo $global['web_theme_color']; ?>;}
    .ncap-form-default .special-add:hover{background-color:<?php echo $global['web_assist_color']; ?>;border-color:<?php echo $global['web_assist_color']; ?>;}
    
    /*更多功能标题*/
    .on-off_panel .title::before {background-color:<?php echo $global['web_theme_color']; ?>;}

</style>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.js"></script>
<!-- <script type="text/javascript" src="/api/public/plugins/tags_input/js/jquery.tagsinput.js?v=<?php echo $version; ?>"></script> -->
<script type="text/javascript" src="/api/public/static/admin/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/api/public/plugins/layer-v3.1.0/layer.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/admin.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/common.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/api/public/plugins/layui/layui.js"></script>
<script src="/api/public/static/admin/js/myFormValidate.js"></script>
<script src="/api/public/static/admin/js/myAjax2.js?v=<?php echo $version; ?>"></script>
<script src="/api/public/static/admin/js/global.js?v=<?php echo $version; ?>"></script>
</head>
<body class="bodystyle" style="overflow-y: scroll; cursor: default; -moz-user-select: inherit;">
<style type="text/css">
    .system_table{ border:1px solid #dcdcdc; width:100%;}
    .system_table td{ height:40px; line-height:40px; font-size:12px; color:#454545; border-bottom:1px solid #dcdcdc; border-right:1px solid #dcdcdc; width:35%; padding-left:1%;}
    .system_table td.gray_bg{ background:#f7f7f7; width:15%;}
</style>
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="page">
    <div class="flexigrid">
        
        <form class="form-horizontal" id="postForm" action="<?php echo url('Shop/order_mark_status'); ?>" method="post">
            <input type="hidden" name="order_id" id="order_id" value="<?php echo $OrderData['order_id']; ?>">
            <input type="hidden" name="order_code" value="<?php echo $OrderData['order_code']; ?>">
            <input type="hidden" name="users_id" value="<?php echo $OrderData['users_id']; ?>">
            <input type="hidden" name="consignee" value="<?php echo $OrderData['consignee']; ?>">
            <div class="hDiv htitx">
                <div class="hDivBox">
                    <table cellspacing="0" cellpadding="0" style="width: 100%">
                        <thead>
                            <tr>
                                <th class="sign w10" axis="col0">
                                    <div class="tc"></div>
                                </th>
                                <th abbr="article_title" axis="col3" class="w10">
                                    <div class="tc">凭证详情</div>
                                </th>
                                <th abbr="ac_id" axis="col4">
                                    <div class=""></div>
                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

            <div class="hDiv" style="margin-top: 5px;">
                <div class="hDivBox">
                    <table cellspacing="0" cellpadding="0" style="width: 100%">
                        <thead>
                            <tr>
                                
                                
                               <th abbr="article_title" axis="col3">
                                    <div class="tl text-l10" style="width: 100%">订单号</div>
                                </th>
    
                                <th abbr="article_title" axis="col3" class="w100">
                                    <div class="tc">金额</div>
                                </th>
                               <th abbr="article_title" axis="col3" class="w100">
                                    <div class="tc">公司名称</div>
                                </th>
                               <th abbr="article_title" axis="col3" class="w100">
                                    <div class="tc">币种</div>
                                </th>
                               <th abbr="article_title" axis="col3" class="w100">
                                    <div class="tc">备注</div>
                                </th>
                                 </th>
                               <th abbr="article_title" axis="col3" class="w100">
                                    <div class="tc">上传时间</div>
                                </th>
    
                                <th abbr="article_title" axis="col3" class="w120">
                                    <div class="tc">上传凭证</div>
                                </th>
    
                                <th axis="col1" class="w180">
                                    <div class="tc">操作</div>
                                </th>
                                
                                <!--<th abbr="article_time" axis="col6" class="w210">-->
                                <!--    <div class="tc ">上传凭证</div>-->
                                <!--</th>-->
                                <!--<th abbr="article_time" axis="col6" class="w150">-->
                                <!--    <div class="tc">订单号</div>-->
                                <!--</th>-->
                                <!--<th abbr="article_time" axis="col6" class="w150">-->
                                <!--    <div class="tc">金额</div>-->
                                <!--</th>-->
                                <!--<th abbr="article_title" axis="col3" class="w300">-->
                                <!--    <div class="tc">公司名称</div>-->
                                <!--</th>-->
                                <!--<th abbr="article_title" axis="col3" class="w300">-->
                                <!--    <div class="tc">币种</div>-->
                                <!--</th>-->
                                <!--<th abbr="article_title" axis="col3" class="w300">-->
                                <!--    <div class="tc">备注</div>-->
                                <!--</th>-->
                                <!--<th abbr="article_title" axis="col3" class="w300">-->
                                <!--    <div class="tc">上传时间</div>-->
                                <!--</th>-->
                                <!--<th axis="col1" class="w160">-->
                                <!--    <div class="tc">操作</div>-->
                                <!--</th>-->
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="bDiv" style="height: auto;">
                <div id="flexigrid" cellpadding="0" cellspacing="0" border="0">
                    <table style="width: 100%;">
                        <tbody>
                        <?php if(empty($voucher) || (($voucher instanceof \think\Collection || $voucher instanceof \think\Paginator ) && $voucher->isEmpty())): ?>
                            <tr>
                                <td class="no-data" align="center" axis="col0" colspan="50">
                                    <i class="fa fa-exclamation-circle"></i>没有符合条件的记录
                                </td>
                            </tr>
                        <?php else: ?>
                        
                            <tr>
                                
                            <td>
                            </td>

                                <td align="left" style="width:100%;">
                                    <div class="tl text-l10">
                                        <?php if(is_check_access(CONTROLLER_NAME.'@edit') == '1'): ?>
                                            <a href="javascript:void(0);" data-href="<?php echo url('AdPosition/edit',array('id'=>$vo['id'])); ?>" data-closereload="1" onclick="openFullframe(this, '编辑广告', '100%', '100%');"><?php echo $voucher['order_code']; ?></a>
                                        <?php else: ?>
                                            <?php echo $voucher['order_code']; endif; ?>
                                    </div>
                                </td>
                                <td>
                                    <div class="tc w100">  <?php echo $voucher['money']; ?> </div>
                                </td>
                               <td>
                                    <div class="tc w100"> <?php echo $voucher['company']; ?> </div>
                                </td>
                                 <td>
                                    <div class="tc w100">  <?php echo $voucher['currency']; ?> </div>
                                </td>
                               <td>
                                    <div class="tc w100"> <?php echo $voucher['remarks']; ?> </div>
                                </td>
                                <td>
                                    <div class="tc w100">   <?php echo MyDate('Y-m-d H:i:s',$voucher['add_time']); ?> </div>
                                </td>
                                <td>
                                    <div class="tc w120">
                                        <ul class="adpic">
                                            <li>
                                                <img src="<?php echo $voucher['voucher']; ?>" data-img-src="<?php echo $voucher['voucher']; ?>">
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                                <td class="operation">
                                    <div class="w180 tc">
                                 		<?php if($voucher['voucher_status'] == 0): ?>
                                             <a href="JavaScript:void(0);" onclick="OrderMark1('ysh','<?php echo $voucher['id']; ?>','<?php echo $voucher['order_id']; ?>');" class="btn blue">
                                                审核
                                            </a>
                                            <?php endif; if($voucher['voucher_status'] == 1): ?>
                                                审核通过
                                        <?php endif; ?>
                                    </div>
                                </td>
                                
                                
                               <!--   <td class="" style="width: 100%; text-align: center;">-->
                               <!--     <div class="tl text-l10">-->
                               <!--         <a target="_blank">-->
                               <!--             <img src="<?php echo $voucher['voucher']; ?>" style="width: 60px;height: 60px;">-->
                               <!--         </a>-->
                               <!--     </div>-->
                               <!-- </td>-->
                                
                               <!--<td class="sort">-->
                               <!--     <div class="tc w210">-->
                               <!--         <?php echo $voucher['order_code']; ?>-->
                               <!--     </div>-->
                               <!-- </td>-->
                               <!--<td class="sort">-->
                               <!--     <div class="tc w210">-->
                               <!--         <?php echo $voucher['money']; ?>-->
                               <!--     </div>-->
                               <!-- </td>-->
                               <!-- <td class="sort">-->
                               <!--     <div class="tc w150">-->
                               <!--         <?php echo $voucher['company']; ?>-->
                               <!--     </div>-->
                               <!-- </td>-->
                               <!-- <td class="sort">-->
                               <!--     <div class="tc w300">-->
                               <!--         <?php echo $voucher['currency']; ?>-->
                               <!--     </div>-->
                               <!-- </td>-->
                               <!--  <td class="sort">-->
                               <!--     <div class="tc w300">-->
                               <!--         <?php echo $voucher['remarks']; ?>-->
                               <!--     </div>-->
                               <!-- </td>-->
                               <!--  <td class="sort">-->
                               <!--     <div class="tc w300">-->
                               <!--         <?php echo $voucher['add_time']; ?>-->
                               <!--     </div>-->
                               <!-- </td>-->
                               <!--  <td class="operation">-->
                               <!--     <div class="w160 tc">-->
                               <!-- 		<?php if($voucher['voucher_status'] == 0): ?>-->
                               <!--          <a href="JavaScript:void(0);" onclick="OrderMark1('ysh','<?php echo $voucher['id']; ?>','<?php echo $voucher['order_id']; ?>');" class="btn blue">-->
                               <!--             审核-->
                               <!--         </a>-->
                               <!--         <?php endif; ?>-->
                               <!--     	<?php if($voucher['voucher_status'] == 1): ?>-->
                               <!--             审核通过-->
                               <!--         <?php endif; ?>-->
                               <!--     </div>-->
                               <!--  </td>-->
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <div class="iDiv" style="display: none;"></div>
            </div>

        </form>
    </div>
</div>


<div id="hi-img-pop">
    <div href="javascript:;" class="hi-close"></div>
    <img src="" alt="">
</div>
<style>
    /* 图片弹窗 */
        
        #hi-img-pop {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 1000;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.5);
            text-align: center;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        
        #hi-img-pop * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        
        #hi-img-pop .hi-close {
            position: fixed;
            top: 86%;
            left: 50%;
            margin-left: -25px;
            width: 50px;
            height: 50px;
            color: #fff;
            background: #fff;
            border-radius: 50%;
            transition: .5s;
            cursor: pointer;
        }
        
        #hi-img-pop .hi-close:after,
        #hi-img-pop .hi-close:before {
            content: "";
            position: absolute;
            bottom: 24px;
            left: 10px;
            width: 30px;
            height: 2px;
            background: #333;
            transition: .5s;
        }
        
        #hi-img-pop .hi-close:hover {
            background: #019dee;
        }
        
        #hi-img-pop .hi-close:hover:after,
        #hi-img-pop .hi-close:hover:before {
            background: #fff;
        }
        
        #hi-img-pop .hi-close:after {
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg);
        }
        
        #hi-img-pop .hi-close:before {
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg);
        }
        
        #hi-img-pop img {
            position: fixed;
            left: 50%;
            top: 50%;
            max-width: 90%;
            max-height: 70%;
            -webkit-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
        }
</style>
<script type="text/javascript">
    // 判断输入框是否为空
 function OrderMark1(status_name, order_id, users_id, admin_note, update_time) {
        if('yfk' == status_name){
            var msg = '确认订单已付款？';
        }else if('ysh' == status_name){
            var msg = '确认审核？';
        }else if('gbdd' == status_name){
            var msg = '确认关闭订单？';
        }else if('ddbz' == status_name){
            layer.prompt({
                formType: 2,
                value: admin_note,
                title: false,
                closeBtn: 0,
                id: 'AdminNote',
                area: ['300px', '100px'],
                success: function(layero, index) {
                    if (admin_note) {
                        var msg = '<br/><span style="color: #999; font-size: 12px;">最后更新：'+update_time+'</span>';
                        $("#AdminNote").append(msg);
                    }
                }
            }, function(value, index, elem) {
                UpNote(order_id,value);
                layer.close(index);
            });
            return false;
        }else if('ddsc' == status_name){
            var msg = '确认删除订单？';
        }

        layer.confirm(msg, {
            title: false,
            closeBtn: 0,
            btn: ['确定','取消'],
        },function(){
            $.ajax({
                url: "<?php echo url('Shop/examine'); ?>",
                data: {order_id:users_id,id:order_id, _ajax:1},
                type:'post',
                dataType:'json',
                success:function(res){
                    layer.closeAll();
                    if (1 == res.code) {
                        layer.msg(res.msg, {time: 1500},function(){
                            window.location.reload();
                        });
                    }else{
                        layer.msg(res.msg, {time: 1500});
                    }
                }
            });
        },function(index){
            layer.closeAll(index);
        });
    }

    // 更新管理员备注
    function UpNote(order_id){
        var admin_note = $('#admin_note').val();
        $.ajax({
            url: "<?php echo url('Shop/update_note', ['_ajax'=>1]); ?>",
            data: {order_id:order_id,admin_note:admin_note},
            type:'post',
            dataType:'json',
            success:function(res){
                layer.closeAll();
                if ('1' == res.code) {
                    layer.msg(res.msg, {time: 1500});
                }else{
                    layer.msg(res.msg, {time: 1500});
                }
            }
        });
    }

    $(document).ready(function(){
        // 表格行点击选中切换
        $('#flexigrid > table>tbody >tr').click(function(){
            $(this).toggleClass('trSelected');
        });

        // 点击刷新数据
        $('.fa-refresh').click(function(){
            location.href = location.href;
        });
    });
    
    
    function HiImgPop(obj) {
        var oImgBox = $("#hi-img-pop"); //弹窗
        var oClose = oImgBox.find(".hi-close"); //关闭按钮
        var oImg = oImgBox.find("img"); //图片标签
        // 点击显示弹窗，设置图片src
        obj.each(function() {
            $(this).click(function() {
                oImgBox.stop().fadeIn();
                oImg.attr("src", $(this).attr("data-img-src"));
            });
        });
        // 点击关闭弹窗
        oClose.click(function() {
            oImgBox.stop().fadeOut();
        });
    }
    // 调用
    HiImgPop($(".adpic li img"));
</script>
<br/>
<div id="goTop">
    <a href="JavaScript:void(0);" id="btntop">
        <i class="fa fa-angle-up"></i>
    </a>
    <a href="JavaScript:void(0);" id="btnbottom">
        <i class="fa fa-angle-down"></i>
    </a>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#think_page_trace_open').css('z-index', 99999);
    });
</script>
</body>
</html>