<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:35:"./weapp/Messages/template/index.htm";i:1639205418;s:44:"/mnt/eyou/weapp/Messages/template/header.htm";i:1639205418;s:41:"/mnt/eyou/weapp/Messages/template/bar.htm";i:1639205418;s:44:"/mnt/eyou/weapp/Messages/template/footer.htm";i:1639205418;}*/ ?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link href="/public/static/admin/css/main.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/public/static/admin/css/page.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/public/static/admin/font/css/font-awesome.min.css?v=<?php echo $version; ?>" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="/public/static/admin/font/css/font-awesome-ie7.min.css?v=<?php echo $version; ?>">
<![endif]-->
<link href="/public/static/admin/js/jquery-ui/jquery-ui.min.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css"/>
<link href="/public/static/admin/css/perfect-scrollbar.min.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css"/>
<style type="text/css">html, body { overflow: visible;}</style>
<script type="text/javascript">
    // 入口路径
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
    var module_name = "<?php echo MODULE_NAME; ?>";
    var GetUploadify_url = "<?php echo url('Uploadify/upload'); ?>";
</script>  
<script type="text/javascript" src="/public/static/admin/js/jquery.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/public/static/admin/js/jquery-ui/jquery-ui.min.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/public/plugins/layer-v3.1.0/layer.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/weapp/Messages/template/skin/js/admin.js?v=<?php echo (isset($weappInfo['version']) && ($weappInfo['version'] !== '')?$weappInfo['version']:'v1.0.0'); ?>"></script>
<script type="text/javascript" src="/public/static/admin/js/perfect-scrollbar.min.js?v=<?php echo $version; ?>"></script>
<script src="/weapp/Messages/template/skin/js/global.js?v=<?php echo (isset($weappInfo['version']) && ($weappInfo['version'] !== '')?$weappInfo['version']:'v1.0.0'); ?>"></script>
</head>
<body style="background-color: rgb(255, 255, 255); overflow: auto; cursor: default; -moz-user-select: inherit;">
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="page">
    
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="<?php if('Messages' == \think\Request::instance()->param('sc') && 'index' == \think\Request::instance()->param('sa')): ?><?php echo url("Weapp/index"); else: ?><?php echo weapp_url("Messages/Messages/index"); endif; ?>" title="返回列表"><i class="fa fa-chevron-left"></i></a>
            <div class="subject">
                <h3><?php echo $weappInfo['name']; ?></h3>
                <h5></h5>
            </div>
            <ul class="tab-base nc-row">
                <li>
                <?php if(\think\Request::instance()->param('sa') == 'index'): ?>
                    <a href="javascript:void(0);" class="tab current"><span>文档列表</span></a>
                <?php else: ?>
                    <a href="<?php echo weapp_url("Messages/Messages/index"); ?>" class="tab"><span>文档列表</span></a>
                <?php endif; ?>
                </li>

                <li>
                <?php if(in_array((\think\Request::instance()->param('sa')), explode(',',"doc"))): ?>
                    <a href="javascript:void(0);" class="tab current"><span>插件介绍</span></a>
                <?php else: ?>
                    <a href="<?php echo weapp_url("Messages/Messages/doc"); ?>" class="tab"><span>插件介绍</span></a>
                <?php endif; ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="flexigrid">
        <div class="mDiv">
            <div class="ftitle">
                <h3>文档列表</h3>
                <h5>(共<?php echo $pageObj->totalRows; ?>条数据)</h5>
            </div>
			<div class="fbutton" style="float: left;">
                <a href="<?php echo weapp_url('Messages/Messages/add'); ?>">
                    <div class="add" title="添加通知">
                        <span class="red" style="color: white;"><i class="fa fa-plus"></i>添加通知</span>
                    </div>
                </a>
            </div>
            <div title="刷新数据" class="pReload"><i class="fa fa-refresh"></i></div>
            <form class="navbar-form form-inline" action="<?php echo weapp_url('Messages/Messages/index'); ?>" method="get" onsubmit="layer_loading('正在处理');">
                <?php echo (isset($searchform['hidden']) && ($searchform['hidden'] !== '')?$searchform['hidden']:''); ?>
                <div class="sDiv">
                    <div class="sDiv2">
                        <input type="text" size="30" name="keywords" class="qsbox" placeholder="搜索相关数据...">
                        <input type="submit" class="btn" value="搜索">
                    </div>
                    <div class="sDiv2">
                        <input type="button" class="btn" value="重置" onClick="window.location.href='<?php echo weapp_url('Messages/Messages/index'); ?>';">
                    </div>
                </div>
            </form>
        </div>
        <div class="hDiv">
            <div class="hDivBox">
                <table cellspacing="0" cellpadding="0" style="width: 100%">
                    <thead>
                    <tr>
                        <th class="sign w40" axis="col0">
                            <div class="tc">选择</div>
                        </th>
                        <th abbr="article_show" axis="col5" class="w40">
                            <div class="tc">ID</div>
                        </th>
                        <th abbr="article_title" axis="col3" class="w600">
                            <div class="">通知标题</div>
                        </th>
                        <th abbr="ac_id" axis="col4">
                            <div class="">通知会员</div>
                        </th>
                        <th abbr="article_time" axis="col6" class="w160">
                            <div class="tc">通知时间</div>
                        </th>
                        <th axis="col1" class="w120">
                            <div class="tc">操作</div>
                        </th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="bDiv" style="height: auto;">
            <div id="flexigrid" cellpadding="0" cellspacing="0" border="0">
                <table style="width: 100%">
                    <tbody>
                    <?php if(empty($list) || (($list instanceof \think\Collection || $list instanceof \think\Paginator ) && $list->isEmpty())): ?>
                        <tr>
                            <td class="no-data" align="center" axis="col0" colspan="50">
                                <i class="fa fa-exclamation-circle"></i>没有符合条件的记录
                            </td>
                        </tr>
                    <?php else: if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                        <tr>
                            <td class="sign">
                                <div class="w40 tc"><input type="checkbox" name="ids[]" value="<?php echo $vo['id']; ?>"></div>
                            </td>
                            <td class="sort">
                                <div class="w40 tc">
                                    <?php echo $vo['id']; ?>
                                </div>
                            </td>
                            <td class="">
                                <div class="w600">
                                    <a href="<?php echo weapp_url('Messages/Messages/edit',array('id'=>$vo['id'])); ?>"><?php echo $vo['title']; ?></a>
                                </div>
                            </td>
                            <td style="width: 100%">
                                <div style="">
								    <?php if(empty($vo['users_id']) || (($vo['users_id'] instanceof \think\Collection || $vo['users_id'] instanceof \think\Paginator ) && $vo['users_id']->isEmpty())): ?>
									    全站会员
									<?php else: ?>
                                        <?php echo $vo['users_id']; endif; ?>
                                </div>
                            </td>
                            <td class="">
                                <div class="w160 tc">
                                    <?php echo MyDate('Y-m-d H:i:s',$vo['add_time']); ?>
                                </div>
                            </td>
                            <td>
                                <div class="w120 tc">
                                    <a href="<?php echo weapp_url('Messages/Messages/edit',array('id'=>$vo['id'])); ?>" class="btn blue"><i class="fa fa-pencil-square-o"></i>编辑</a>
                                    <a class="btn red"  href="javascript:void(0)" data-url="<?php echo weapp_url('Messages/Messages/del'); ?>" data-id="<?php echo $vo['id']; ?>" onClick="delfun(this);"><i class="fa fa-trash-o"></i>删除</a>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; endif; else: echo "" ;endif; endif; ?>
                    </tbody>
                </table>
            </div>
            <div class="iDiv" style="display: none;"></div>
        </div>
        <div class="tDiv">
            <div class="tDiv2">
                <div class="fbutton checkboxall">
                    <input type="checkbox" onclick="javascript:$('input[name*=ids]').prop('checked',this.checked);">
                </div>
                <div class="fbutton">
                    <a onclick="batch_del(this, 'ids');" data-url="<?php echo weapp_url('Messages/Messages/del'); ?>">
                        <div class="add" title="批量删除">
                            <span><i class="fa fa-close"></i>批量删除</span>
                        </div>
                    </a>
                </div>
            </div>
            <div style="clear:both"></div>
        </div>
        <!--分页位置-->
        <?php echo $pageStr; ?>
    </div>
</div>
<script>
    $(document).ready(function(){
        // 表格行点击选中切换
        $('#flexigrid > table>tbody >tr').click(function(){
            $(this).toggleClass('trSelected');
        });

        // 点击刷新数据
        $('.fa-refresh').click(function(){
            location.href = location.href;
        });
    });
</script>
<br/>
<div id="goTop">
    <a href="JavaScript:void(0);" id="btntop">
        <i class="fa fa-angle-up"></i>
    </a>
    <a href="JavaScript:void(0);" id="btnbottom">
        <i class="fa fa-angle-down"></i>
    </a>
</div>

<script type="text/javascript">
    $(document).ready(function(){
		// 调试信息
        $('#think_page_trace_open').css('z-index', 99999);
    });
</script>
</body>
</html>
