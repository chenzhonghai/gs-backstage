<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:51:"./application/admin/template/shop/order_details.htm";i:1642400510;s:57:"/mnt/api/api/application/admin/template/public/layout.htm";i:1640913882;s:60:"/mnt/api/api/application/admin/template/public/theme_css.htm";i:1640913882;s:57:"/mnt/api/api/application/admin/template/public/footer.htm";i:1640913882;}*/ ?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link href="/api/public/plugins/layui/css/layui.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/main.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/page.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/font/css/font-awesome.min.css" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="/api/public/static/admin/font/css/font-awesome-ie7.min.css">
<![endif]-->
<script type="text/javascript">
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
    var module_name = "<?php echo MODULE_NAME; ?>";
    var GetUploadify_url = "<?php echo url('Uploadify/upload'); ?>";
    var __root_dir__ = "/api";
    var __lang__ = "<?php echo $admin_lang; ?>";
</script>  
<link href="/api/public/static/admin/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="/api/public/static/admin/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
<!-- <link type="text/css" rel="stylesheet" href="/api/public/plugins/tags_input/css/jquery.tagsinput.css?v=<?php echo $version; ?>"> -->
<style type="text/css">html, body { overflow: visible;}</style>
<link href="/api/public/static/admin/css/diy_style.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css" />

<!-- 官方内置样式表，升级会覆盖变动，请勿修改，否则后果自负 -->

<style type="text/css">
	/*左侧收缩图标*/
	#foldSidebar i { font-size: 24px;color:<?php echo $global['web_theme_color']; ?>; }
    /*左侧菜单*/
    .eycms_cont_left{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl dd a:hover,.eycms_cont_left dl dd a.on,.eycms_cont_left dl dt.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl dd a:active{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist dd{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl.jslist dd a:hover,.eycms_cont_left dl.jslist dd a.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist:hover{ background:<?php echo $global['web_assist_color']; ?>; }
    /*栏目操作*/
    .cate-dropup .cate-dropup-con a:hover{ background-color: <?php echo $global['web_theme_color']; ?>; }
    /*按钮*/
    a.ncap-btn-green { background-color: <?php echo $global['web_theme_color']; ?>; }
    a:hover.ncap-btn-green { background-color: <?php echo $global['web_assist_color']; ?>; }
    .flexigrid .sDiv2 .btn:hover { background-color: <?php echo $global['web_theme_color']; ?>; }
    .flexigrid .mDiv .fbutton div.add{background-color: <?php echo $global['web_theme_color']; ?>; border: none;}
    .flexigrid .mDiv .fbutton div.add:hover{ background-color: <?php echo $global['web_assist_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds{color:<?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds:hover{ background-color: <?php echo $global['web_theme_color']; ?>;}
    /*选项卡字体*/
    .tab-base a.current,
    .tab-base a:hover.current { border-bottom: solid 2px <?php echo $global['web_theme_color']; ?> !important;color: <?php echo $global['web_theme_color']; ?> !important;}
    .addartbtn input.btn:hover{ border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>; }
    .addartbtn input.btn.selected{ color: <?php echo $global['web_theme_color']; ?>;border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>;}
	/*会员导航*/
	.member-nav-group .member-nav-item .btn.selected{background: <?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;box-shadow: -1px 0 0 0 <?php echo $global['web_theme_color']; ?>;}
	.member-nav-group .member-nav-item:first-child .btn.selected{border-left: 1px solid <?php echo $global['web_theme_color']; ?> !important;}
	/*搜索按钮图标*/
	.flexigrid .sDiv2 .fa-search{}
        
    /* 商品订单列表标题 */
   .flexigrid .mDiv .ftitle h3 {border-left: 3px solid <?php echo $global['web_theme_color']; ?>;} 
	
    /*分页*/
    .pagination > .active > a, .pagination > .active > a:focus, 
	.pagination > .active > a:hover, 
	.pagination > .active > span, 
	.pagination > .active > span:focus, 
	.pagination > .active > span:hover { border-color: <?php echo $global['web_theme_color']; ?>;color:<?php echo $global['web_theme_color']; ?>; }
    
    .layui-form-onswitch {border-color: <?php echo $global['web_theme_color']; ?> !important;background-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-enable.selected { background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-disable.selected {background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    input[type="text"]:focus,
    input[type="text"]:hover,
    input[type="text"]:active,
    input[type="password"]:focus,
    input[type="password"]:hover,
    input[type="password"]:active,
    textarea:hover,
    textarea:focus,
    textarea:active { border-color:<?php echo hex2rgba($global['web_theme_color'],0.8); ?>;box-shadow: 0 0 0 2px <?php echo hex2rgba($global['web_theme_color'],0.15); ?> !important;}
    .input-file-show:hover .type-file-button {background-color:<?php echo $global['web_theme_color']; ?> !important; }
    .input-file-show:hover {border-color: <?php echo $global['web_theme_color']; ?> !important;box-shadow: 0 0 5px <?php echo hex2rgba($global['web_theme_color'],0.5); ?> !important;}
    .input-file-show:hover span.show a,
    .input-file-show span.show a:hover { color: <?php echo $global['web_theme_color']; ?> !important;}
    .input-file-show:hover .type-file-button {background-color: <?php echo $global['web_theme_color']; ?> !important; }
    .color_z { color: <?php echo $global['web_theme_color']; ?> !important;}
    a.imgupload{
        background-color: <?php echo $global['web_theme_color']; ?> !important;
        border-color: <?php echo $global['web_theme_color']; ?> !important;
    }
    /*专题节点按钮*/
    .ncap-form-default .special-add{background-color:<?php echo $global['web_theme_color']; ?>;border-color:<?php echo $global['web_theme_color']; ?>;}
    .ncap-form-default .special-add:hover{background-color:<?php echo $global['web_assist_color']; ?>;border-color:<?php echo $global['web_assist_color']; ?>;}
    
    /*更多功能标题*/
    .on-off_panel .title::before {background-color:<?php echo $global['web_theme_color']; ?>;}

</style>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.js"></script>
<!-- <script type="text/javascript" src="/api/public/plugins/tags_input/js/jquery.tagsinput.js?v=<?php echo $version; ?>"></script> -->
<script type="text/javascript" src="/api/public/static/admin/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/api/public/plugins/layer-v3.1.0/layer.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/admin.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/common.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/api/public/plugins/layui/layui.js"></script>
<script src="/api/public/static/admin/js/myFormValidate.js"></script>
<script src="/api/public/static/admin/js/myAjax2.js?v=<?php echo $version; ?>"></script>
<script src="/api/public/static/admin/js/global.js?v=<?php echo $version; ?>"></script>
</head>
<body class="bodystyle" style="overflow-y: scroll; cursor: default; -moz-user-select: inherit;">
<style type="text/css">
    .system_table{ border:1px solid #dcdcdc; width:100%;}
    .system_table td{ height:40px; line-height:40px; font-size:12px; color:#454545; border-bottom:1px solid #dcdcdc; border-right:1px solid #dcdcdc; width:35%; padding-left:1%;}
    .system_table td.gray_bg{ background:#f7f7f7; width:15%;}
</style>
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="page">
    <div class="flexigrid">
        
        <form class="form-horizontal" id="postForm" action="<?php echo url('Shop/order_mark_status'); ?>" method="post">
            <input type="hidden" name="order_id" id="order_id" value="<?php echo $OrderData['order_id']; ?>">
            <input type="hidden" name="order_code" value="<?php echo $OrderData['order_code']; ?>">
            <input type="hidden" name="users_id" value="<?php echo $OrderData['users_id']; ?>">
            <input type="hidden" name="consignee" value="<?php echo $OrderData['consignee']; ?>">
            <div class="hDiv htitx">
                <div class="hDivBox">
                    <table cellspacing="0" cellpadding="0" style="width: 100%">
                        <thead>
                            <tr>
                                <th class="sign w10" axis="col0">
                                    <div class="tc"></div>
                                </th>
                                <th abbr="article_title" axis="col3" class="w10">
                                    <div class="tc">订单信息</div>
                                </th>
                                <th abbr="ac_id" axis="col4">
                                    <div class=""></div>
                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

            <div class="ncap-form-default">
                <table cellpadding="0" cellspacing="0" class="system_table">
                    <tbody>
                        <tr>
                            <td class="gray_bg">订单ID：</td>
                            <td><?php echo $OrderData['order_id']; ?></td>
                            <td class="gray_bg">订单编号：</td>
                            <td><?php echo $OrderData['order_code']; ?></td>
                        </tr>
                        <tr>
                            <td class="gray_bg">用户名：</td>
                            <td><?php echo $UsersData['username']; ?></td>
                            <td class="gray_bg">邮箱地址：</td>
                            <td><?php echo $UsersData['email']; ?></td>
                        </tr>
                        <tr>
                            <td class="gray_bg">手机号码：</td>
                            <td><?php echo $UsersData['mobile']; ?></td>
                            <td class="gray_bg">应付金额：</td>
                            <td>
                                <input type="hidden" id="order_amount_old" value="<?php echo $OrderData['order_amount']; ?>">
                                <span id="order_amount">￥<?php echo $OrderData['order_amount']; ?></span>
                                <?php if($OrderData['shipping_fee'] > '0'): ?>(含运费:<?php echo $OrderData['shipping_fee']; ?>)<?php endif; if($OrderData['order_status'] == '0'): ?>
                                    <a href="JavaScript:void(0);" style="color: red;" onclick="PromptChangePrice();">[改价]</a>
                                <?php else: if($OrderData['order_total_amount'] > $OrderData['order_amount']): ?>
                                        <span style="color: red;" title="应付金额已被更改过">[已改价]</span>
                                    <?php endif; endif; ?>
                            </td>
                        </tr>
                        <script type="text/javascript">
                            // 弹出改价输入框
                            function PromptChangePrice() {
                                layer.prompt({
                                    title: false, 
                                    formType: 3,
                                    id: 'BulkSetPrice',
                                    btn: ['确定', '关闭'],
                                    closeBtn: 0,
                                    success: function(layero, index) {
                                        $("#BulkSetPrice").find('input').attr('placeholder', '请输入应付金额');
                                        $("#BulkSetPrice").find('input').attr('value', $('#order_amount_old').val());
                                        $("#BulkSetPrice").find('input').attr('onkeyup', "this.value=this.value.replace(/[^\\d.]/g,'')");
                                        $("#BulkSetPrice").find('input').attr('onpaste', "this.value=this.value.replace(/[^\\d.]/g,'')");
                                        var msg = '<span style="color: red;">改价后让用户重新进入订单列表点击支付</span>';
                                        $("#BulkSetPrice").append(msg);
                                        // 修改订单金额后为保证支付金额准确有效<br/>请让用户刷新订单列表页后重新点击支付
                                    }
                                }, function(price, index) {
                                    if (0 < price) {
                                        layer.close(index);
                                        OrderChangePrice(price);
                                    } else {
                                        layer.msg('应付金额不允许为0', {time: 1500});
                                    }
                                });
                            }

                            // 提交改价数据并追加一条订单操作记录
                            function OrderChangePrice(Price) {
                                if (0 >= Price) layer.msg('应付金额不允许为0', {time: 1500});
                                layer_loading('正在处理');
                                $.ajax({
                                    type : 'post',
                                    url  : "<?php echo url('Shop/order_change_price'); ?>",
                                    data : {
                                        order_id: $('#order_id').val(),
                                        order_amount_old: $('#order_amount_old').val(),
                                        order_amount: Price,
                                        _ajax: 1
                                    },
                                    dataType : 'json',
                                    success : function(res) {
                                        layer.closeAll();
                                        if (1 == res.code) {
                                            layer.msg(res.msg, {time: 1500}, function() {
                                                window.location.reload();
                                            });
                                        } else {
                                            layer.alert(res.msg, {title: false, closeBtn: 0}, function() {
                                                window.location.reload();
                                            });
                                        }
                                    }
                                });
                            }
                        </script>
                        <tr>
                            <td class="gray_bg">支付方式：</td>
                            <td>
                                <?php if($OrderData['payment_method'] == '1'): ?>
                                    货到付款
                                <?php endif; if($OrderData['payment_method'] == '0'): ?>
                                   在线支付
                                <?php endif; if($OrderData['payment_method'] == '2'): ?>
                                    上传凭证支付
                                <?php endif; ?>
                            </td>
                            <td class="gray_bg">订单状态：</td>
                            <td>
                                <?php echo (isset($admin_order_status_arr[$OrderData['order_status']]) && ($admin_order_status_arr[$OrderData['order_status']] !== '')?$admin_order_status_arr[$OrderData['order_status']]:''); if($OrderData['order_status'] == '2'): ?>
                                    &nbsp;<a href="<?php echo url('Shop/order_send',array('order_id'=>$OrderData['order_id'])); ?>" class="ncap-btn ncap-btn-green" >
                                        发货详情
                                    </a>
                                    <?php if($OrderData['prom_type'] == '0'): ?>
                                        &nbsp;<a href="javascript:void(0);" data-url="<?php echo $MobileExpressUrl; ?>" onclick="LogisticsInquiry(this);" class="ncap-btn ncap-btn-green" >
                                            物流查询
                                        </a>
                                        <script type="text/javascript">
                                            // 发货、标记未付款
                                            function LogisticsInquiry(obj){
                                                var url = $(obj).attr('data-url');
                                                //iframe窗
                                                var iframes = layer.open({
                                                    type: 2,
                                                    title: '物流查询',
                                                    shadeClose: false,
                                                    maxmin: false, //开启最大化最小化按钮
                                                    area: ['60%', '80%'],
                                                    content: url
                                                });
                                            }
                                        </script>
                                    <?php endif; endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="gray_bg">下单时间：</td>
                            <td><?php echo MyDate('Y-m-d H:i:s',$OrderData['add_time']); ?></td>
                            <td class="gray_bg">付款时间：</td>
                            <td><?php if($OrderData['pay_time'] != '0'): ?><?php echo MyDate('Y-m-d H:i:s',$OrderData['pay_time']); endif; ?></td>
                        </tr>
                        <tr>
                            <td class="gray_bg">付款方式：</td>
                            <td>
                                <?php if($OrderData['payment_method'] == '1'): ?>
                                    快递代收
                                <?php else: ?>
                                    <?php echo (isset($pay_method_arr[$OrderData['pay_name']]) && ($pay_method_arr[$OrderData['pay_name']] !== '')?$pay_method_arr[$OrderData['pay_name']]:'未付款'); endif; ?>
                            </td>
                            <td class="gray_bg">订单类型：</td>
                            <td><?php echo $OrderData['prom_type_name']; ?></td>
                        </tr>
                        <?php if(empty($OrderData['prom_type']) || (($OrderData['prom_type'] instanceof \think\Collection || $OrderData['prom_type'] instanceof \think\Paginator ) && $OrderData['prom_type']->isEmpty())): ?>
                            <tr>
                                <td class="gray_bg">物流公司：</td>
                                <td><?php echo $OrderData['express_name']; ?></td>
                                <td class="gray_bg">配送单号：</td>
                                <td><?php echo $OrderData['express_order']; ?></td>
                            </tr>
                        <?php endif; ?>
                        
                        <tr>
                            <td class="gray_bg">积分抵扣：</td>
                            <td><?php echo $OrderData['integral'] * 1000;?></td>
                            <td class="gray_bg">优惠劵：</td>
                            <td><?php echo $coupon_name; ?></td>
                        </tr>
                    
                    </tbody>
                </table>
            </div>

            <div class="hDiv htitx">
                <div class="hDivBox">
                    <table cellspacing="0" cellpadding="0" style="width: 100%">
                        <thead>
                            <tr>
                                <th class="sign w10" axis="col0">
                                    <div class="tc"></div>
                                </th>
                                <th abbr="article_title" axis="col3" class="w10">
                                    <div class="tc">收货信息</div>
                                </th>
                                <th abbr="ac_id" axis="col4">
                                    <div class=""></div>
                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="ncap-form-default">
                <?php if(empty($OrderData['prom_type']) || (($OrderData['prom_type'] instanceof \think\Collection || $OrderData['prom_type'] instanceof \think\Paginator ) && $OrderData['prom_type']->isEmpty())): ?>
                <dl class="row">
                    <dt class="tit pl15">
                        <label>收货人</label>
                    </dt>
                    <dd class="opt">          
                        <?php echo $OrderData['consignee']; ?>
                    </dd>
                </dl>
                <dl class="row">
                    <dt class="tit pl15">
                        <label>联系方式</label>
                    </dt>
                    <dd class="opt">          
                        <?php echo $OrderData['mobile']; ?>
                    </dd>
                </dl>
                <dl class="row">
                    <dt class="tit pl15">
                        <label>收货地址</label>
                    </dt>
                    <dd class="opt">          
                        <?php echo $OrderData['country']; ?> <?php echo $OrderData['province']; ?> <?php echo $OrderData['city']; ?> <?php echo $OrderData['district']; ?> <?php echo $OrderData['address']; ?>
                    </dd>
                </dl>
               
                <?php endif; ?>
                <dl class="row">
                    <dt class="tit pl15">
                        <label>订单留言</label>
                    </dt>
                    <dd class="opt">          
                        <?php echo $OrderData['user_note']; ?>
                    </dd>
                </dl>
                <dl class="row">
                    <dt class="tit pl15">
                        <label>管理员备注</label>
                    </dt>
                    <dd class="opt">          
                        <textarea rows="5" cols="60" id="admin_note" name="admin_note" style="height:60px;"><?php echo $OrderData['admin_note']; ?></textarea>
                        <span class="err"></span>
                        <p class="notic"></p>
                    </dd>
                </dl>
                <div class="bot pl120" style="padding-bottom:0px;">
                    <input type="hidden" name="gourl" value="<?php echo (isset($gourl) && ($gourl !== '')?$gourl:''); ?>">
                    <a href="JavaScript:void(0);" onclick="UpNote('<?php echo $OrderData['order_id']; ?>');" class="ncap-btn-big ncap-btn-green" id="submitBtn">保存</a>
                </div>
            </div>
            
            
                        <div class="hDiv htitx">
                <div class="hDivBox">
                    <table cellspacing="0" cellpadding="0" style="width: 100%">
                        <thead>
                            <tr>
                                <th class="sign w10" axis="col0">
                                    <div class="tc"></div>
                                </th>
                                <th abbr="article_title" axis="col3" class="w10">
                                    <div class="tc">发票信息</div>
                                </th>
                                <th abbr="ac_id" axis="col4">
                                    <div class=""></div>
                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            
            <?php if($OrderData['stat'] == 3): ?>
               <div class="ncap-form-default">
                    不需要发票信息
              </div>
            <?php endif; if($OrderData['stat'] == 1): ?>
                <div class="ncap-form-default">
                   <dl class="row">
                        <dt class="tit pl15">
                            <label>发票类型</label>
                        </dt>
                        <dd class="opt">
                          普票
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="tit pl15">
                            <label>公司名称</label>
                        </dt>
                        <dd class="opt">          
                            <?php echo $invoice['company']; ?>
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="tit pl15">
                            <label>纳税识别号</label>
                        </dt>
                        <dd class="opt">          
                            <?php echo $invoice['identification_number']; ?>
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="tit pl15">
                            <label>邮箱</label>
                        </dt>
                        <dd class="opt">          
                            <?php echo $invoice['email']; ?>
                        </dd>
                    </dl>
                </div>
            <?php endif; if($OrderData['stat'] == 2): ?>
                <div class="ncap-form-default">
                   <dl class="row">
                        <dt class="tit pl15">
                            <label>发票类型</label>
                        </dt>
                        <dd class="opt">
                          专票
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="tit pl15">
                            <label>公司名称</label>
                        </dt>
                        <dd class="opt">          
                            <?php echo $invoice['company']; ?>
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="tit pl15">
                            <label>纳税识别号</label>
                        </dt>
                        <dd class="opt">          
                            <?php echo $invoice['identification_number']; ?>
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="tit pl15">
                            <label>邮箱</label>
                        </dt>
                        <dd class="opt">          
                            <?php echo $invoice['email']; ?>
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="tit pl15">
                            <label>注册地址</label>
                        </dt>
                        <dd class="opt">          
                            <?php echo $invoice['address']; ?>
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="tit pl15">
                            <label>注册电话</label>
                        </dt>
                        <dd class="opt">          
                            <?php echo $invoice['phone']; ?>
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="tit pl15">
                            <label>开户银行名称</label>
                        </dt>
                        <dd class="opt">          
                            <?php echo $invoice['bank_name']; ?>
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="tit pl15">
                            <label>银行账号</label>
                        </dt>
                        <dd class="opt">          
                            <?php echo $invoice['bank_account']; ?>
                        </dd>
                    </dl>
                </div>
            <?php endif; ?>

            

            

            <div class="hDiv" style="margin-top: 5px;">
                <div class="hDivBox">
                    <table cellspacing="0" cellpadding="0" style="width: 100%">
                        <thead>
                            <tr>
                                
                                <th abbr="article_title" axis="col3" class="">
                                    <div class="tl text-l10">商品名称</div>
                                </th>
                                <th abbr="article_time" axis="col6" class="w210">
                                    <div class="tc ">规格属性</div>
                                </th>
                                <th abbr="article_time" axis="col6" class="w210">
                                    <div class="tc">数量</div>
                                </th>
                                <th abbr="article_time" axis="col6" class="w150">
                                    <div class="tc">会员价格</div>
                                </th>
                                <th abbr="article_title" axis="col3" class="w300">
                                    <div class="tc">单品原价</div>
                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="bDiv" style="height: auto;">
                <div id="flexigrid" cellpadding="0" cellspacing="0" border="0">
                    <table style="width: 100%;">
                        <tbody>
                        <?php if(empty($DetailsData) || (($DetailsData instanceof \think\Collection || $DetailsData instanceof \think\Paginator ) && $DetailsData->isEmpty())): ?>
                            <tr>
                                <td class="no-data" align="center" axis="col0" colspan="50">
                                    <i class="fa fa-exclamation-circle"></i>没有符合条件的记录
                                </td>
                            </tr>
                        <?php else: if(is_array($DetailsData) || $DetailsData instanceof \think\Collection || $DetailsData instanceof \think\Paginator): if( count($DetailsData)==0 ) : echo "" ;else: foreach($DetailsData as $k=>$vo): ?>
                            <tr>
                                
                                <td class="" style="width: 100%;">
                                    <div class="tl text-l10">
                                        <a href="<?php echo $vo['arcurl']; ?>" target="_blank">
                                            <img src="<?php echo $vo['litpic']; ?>" style="width: 60px;height: 60px;"> <?php echo $vo['product_name']; ?>
                                        </a>
                                    </div>
                                </td>
                                <td class="sort">
                                    <div class="tc w210 text-l10">
                                        <?php echo $vo['data']; ?>
                                    </div>
                                </td>
                                <td class="sort">
                                    <div class="tc w210">
                                        <?php echo $vo['num']; ?>
                                    </div>
                                </td>
                                <td class="sort">
                                    <div class="tc w150">
                                        
                                        <?php echo  sprintf("%.2f", $vo['product_price'] * $discount); ?>
                                        
                                    </div>
                                </td>
                                <td class="sort">
                                    <div class="tc w300">
                                        ￥<?php echo $vo['subtotal']; ?>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; endif; else: echo "" ;endif; endif; ?>
                        </tbody>
                    </table>
                </div>
                <div class="iDiv" style="display: none;"></div>
            </div>

            <!-- <div class="hDiv">
                <div class="hDivBox">
                    <table cellspacing="0" cellpadding="0" style="width: 100%">
                        <thead>
                        <tr>
                            <th class="sign w10" axis="col0">
                                <div class="tc"></div>
                            </th>
                            <th abbr="article_title" axis="col3" class="w10">
                                <div class="tc">操作信息</div>
                            </th>
                            <th abbr="ac_id" axis="col4">
                                <div class=""></div>
                            </th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="ncap-form-default">
                <input type="hidden" name="status_name" id="status_name">
                <dl class="row">
                    <dt class="tit">
                        <label for="uname">可执行操作</label>
                    </dt>
                    <dd class="opt">
                        <?php if($OrderData['order_status'] == '0'): ?>
                            <a href="JavaScript:void(0);" onclick="OrderMark('yfk');" class="ncap-btn-big ncap-btn-green" id="submitBtn">
                                标记已付款
                            </a>
                        <?php endif; if($OrderData['order_status'] == '2'): ?>
                            <a href="JavaScript:void(0);" onclick="OrderMark('ysh');" class="ncap-btn-big ncap-btn-green" id="submitBtn">
                                标记已收货
                            </a>
                        <?php endif; if($OrderData['order_status'] != '-1'): ?>
                            <a href="JavaScript:void(0);" onclick="OrderMark('wx');" class="ncap-btn-big ncap-btn-green" id="submitBtn">
                                标记为无效
                            </a>
                        <?php endif; ?>
                    </dd>
                </dl>
            </div> -->

            
            <div class="hDiv" style="margin-top: 5px;">
                <div class="hDivBox">
                    <table cellspacing="0" cellpadding="0" style="width: 100%">
                        <thead>
                        <tr>
                            
                            <th abbr="article_time" axis="col6">
                                <div class="tl text-l10">操作记录</div>
                            </th>
                            <th class="sign w210" axis="col0">
                                <div class="tc">操作者</div>
                            </th>
                            <th abbr="article_title" axis="col3" class="w210">
                                <div class="tc">操作时间</div>
                            </th>
                            <th abbr="article_time" axis="col6" class="w150">
                                <div class="tc">订单状态</div>
                            </th>
                            <th abbr="article_title" axis="col3" class="w300">
                                <div class="tc">操作备注</div>
                            </th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="bDiv" style="height: auto;">
                <div id="flexigrid" cellpadding="0" cellspacing="0" border="0">
                    <table style="width: 100%;">
                        <tbody>
                        <?php if(empty($Action) || (($Action instanceof \think\Collection || $Action instanceof \think\Paginator ) && $Action->isEmpty())): ?>
                            <tr>
                                <td class="no-data" align="center" axis="col0" colspan="50">
                                    <i class="fa fa-exclamation-circle"></i>没有符合条件的记录
                                </td>
                            </tr>
                        <?php else: if(is_array($Action) || $Action instanceof \think\Collection || $Action instanceof \think\Paginator): if( count($Action)==0 ) : echo "" ;else: foreach($Action as $k=>$vo): ?>
                            <tr>
                                
                                <td class="sort" style="width: 100%;">
                                    <div class="tl text-l10" >
                                        <?php echo $vo['action_desc']; ?>
                                    </div>
                                </td>
                                <td class="sort">
                                    <div class="tc w210">
                                        <?php echo $vo['username']; ?>
                                    </div>
                                </td>
                                <td class="sort">
                                    <div class="tc w210">
                                        <?php echo MyDate('Y-m-d H:i:s',$vo['add_time']); ?>
                                    </div>
                                </td>
                               
                                <td class="sort">
                                    <div class="tc w150">
                                        
                                        <?php echo (isset($admin_order_status_arr[$vo['order_status']]) && ($admin_order_status_arr[$vo['order_status']] !== '')?$admin_order_status_arr[$vo['order_status']]:''); ?>
                                    </div>
                                </td>
                                <td class="sort" >
                                    <div class="tc w300" >
                                        <?php echo $vo['action_note']; ?>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; endif; else: echo "" ;endif; endif; ?>
                        </tbody>
                    </table>
                </div>
                <div class="iDiv" style="display: none;"></div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    // 判断输入框是否为空
    function OrderMark(status_name){
        if('qfh' == status_name){
            var order_id = '<?php echo $OrderData['order_id']; ?>';
            var url = "<?php echo url('Shop/order_send'); ?>";
            if (url.indexOf('?') > -1) {
                url += '&';
            } else {
                url += '?';
            }
            url += 'order_id='+order_id;
            window.location.href = url;
            return false;
        }else if('yfk' == status_name){
            var msg = '确认订单已付款？';
        }else if('ysh' == status_name){
            var msg = '确认订单已收货？';
        }else if('wfk' == status_name){
            var msg = '确认订单未付款？';
        }else if('wx' == status_name){
            var msg = '确认订单无效？';
        }
        
        layer.confirm(msg, {
            title: false,
            btn: ['确定','取消']
        },function(){
            $('#status_name').val(status_name);
            layer_loading('正在处理');
            $('#postForm').submit();
        },function(index){
            layer.closeAll(index);
        });
    }

    // 更新管理员备注
    function UpNote(order_id){
        var admin_note = $('#admin_note').val();
        $.ajax({
            url: "<?php echo url('Shop/update_note', ['_ajax'=>1]); ?>",
            data: {order_id:order_id,admin_note:admin_note},
            type:'post',
            dataType:'json',
            success:function(res){
                layer.closeAll();
                if ('1' == res.code) {
                    layer.msg(res.msg, {time: 1500});
                }else{
                    layer.msg(res.msg, {time: 1500});
                }
            }
        });
    }

    $(document).ready(function(){
        // 表格行点击选中切换
        $('#flexigrid > table>tbody >tr').click(function(){
            $(this).toggleClass('trSelected');
        });

        // 点击刷新数据
        $('.fa-refresh').click(function(){
            location.href = location.href;
        });
    });
</script>
<br/>
<div id="goTop">
    <a href="JavaScript:void(0);" id="btntop">
        <i class="fa fa-angle-up"></i>
    </a>
    <a href="JavaScript:void(0);" id="btnbottom">
        <i class="fa fa-angle-down"></i>
    </a>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#think_page_trace_open').css('z-index', 99999);
    });
</script>
</body>
</html>