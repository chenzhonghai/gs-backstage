<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:67:"./application/admin/template/shop_service/after_service_details.htm";i:1646138934;s:57:"/mnt/api/api/application/admin/template/public/layout.htm";i:1640913882;s:60:"/mnt/api/api/application/admin/template/public/theme_css.htm";i:1640913882;s:57:"/mnt/api/api/application/admin/template/public/footer.htm";i:1640913882;}*/ ?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link href="/api/public/plugins/layui/css/layui.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/main.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/page.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/font/css/font-awesome.min.css" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="/api/public/static/admin/font/css/font-awesome-ie7.min.css">
<![endif]-->
<script type="text/javascript">
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
    var module_name = "<?php echo MODULE_NAME; ?>";
    var GetUploadify_url = "<?php echo url('Uploadify/upload'); ?>";
    var __root_dir__ = "/api";
    var __lang__ = "<?php echo $admin_lang; ?>";
</script>  
<link href="/api/public/static/admin/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="/api/public/static/admin/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
<!-- <link type="text/css" rel="stylesheet" href="/api/public/plugins/tags_input/css/jquery.tagsinput.css?v=<?php echo $version; ?>"> -->
<style type="text/css">html, body { overflow: visible;}</style>
<link href="/api/public/static/admin/css/diy_style.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css" />

<!-- 官方内置样式表，升级会覆盖变动，请勿修改，否则后果自负 -->

<style type="text/css">
	/*左侧收缩图标*/
	#foldSidebar i { font-size: 24px;color:<?php echo $global['web_theme_color']; ?>; }
    /*左侧菜单*/
    .eycms_cont_left{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl dd a:hover,.eycms_cont_left dl dd a.on,.eycms_cont_left dl dt.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl dd a:active{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist dd{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl.jslist dd a:hover,.eycms_cont_left dl.jslist dd a.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist:hover{ background:<?php echo $global['web_assist_color']; ?>; }
    /*栏目操作*/
    .cate-dropup .cate-dropup-con a:hover{ background-color: <?php echo $global['web_theme_color']; ?>; }
    /*按钮*/
    a.ncap-btn-green { background-color: <?php echo $global['web_theme_color']; ?>; }
    a:hover.ncap-btn-green { background-color: <?php echo $global['web_assist_color']; ?>; }
    .flexigrid .sDiv2 .btn:hover { background-color: <?php echo $global['web_theme_color']; ?>; }
    .flexigrid .mDiv .fbutton div.add{background-color: <?php echo $global['web_theme_color']; ?>; border: none;}
    .flexigrid .mDiv .fbutton div.add:hover{ background-color: <?php echo $global['web_assist_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds{color:<?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds:hover{ background-color: <?php echo $global['web_theme_color']; ?>;}
    /*选项卡字体*/
    .tab-base a.current,
    .tab-base a:hover.current { border-bottom: solid 2px <?php echo $global['web_theme_color']; ?> !important;color: <?php echo $global['web_theme_color']; ?> !important;}
    .addartbtn input.btn:hover{ border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>; }
    .addartbtn input.btn.selected{ color: <?php echo $global['web_theme_color']; ?>;border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>;}
	/*会员导航*/
	.member-nav-group .member-nav-item .btn.selected{background: <?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;box-shadow: -1px 0 0 0 <?php echo $global['web_theme_color']; ?>;}
	.member-nav-group .member-nav-item:first-child .btn.selected{border-left: 1px solid <?php echo $global['web_theme_color']; ?> !important;}
	/*搜索按钮图标*/
	.flexigrid .sDiv2 .fa-search{}
        
    /* 商品订单列表标题 */
   .flexigrid .mDiv .ftitle h3 {border-left: 3px solid <?php echo $global['web_theme_color']; ?>;} 
	
    /*分页*/
    .pagination > .active > a, .pagination > .active > a:focus, 
	.pagination > .active > a:hover, 
	.pagination > .active > span, 
	.pagination > .active > span:focus, 
	.pagination > .active > span:hover { border-color: <?php echo $global['web_theme_color']; ?>;color:<?php echo $global['web_theme_color']; ?>; }
    
    .layui-form-onswitch {border-color: <?php echo $global['web_theme_color']; ?> !important;background-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-enable.selected { background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-disable.selected {background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    input[type="text"]:focus,
    input[type="text"]:hover,
    input[type="text"]:active,
    input[type="password"]:focus,
    input[type="password"]:hover,
    input[type="password"]:active,
    textarea:hover,
    textarea:focus,
    textarea:active { border-color:<?php echo hex2rgba($global['web_theme_color'],0.8); ?>;box-shadow: 0 0 0 2px <?php echo hex2rgba($global['web_theme_color'],0.15); ?> !important;}
    .input-file-show:hover .type-file-button {background-color:<?php echo $global['web_theme_color']; ?> !important; }
    .input-file-show:hover {border-color: <?php echo $global['web_theme_color']; ?> !important;box-shadow: 0 0 5px <?php echo hex2rgba($global['web_theme_color'],0.5); ?> !important;}
    .input-file-show:hover span.show a,
    .input-file-show span.show a:hover { color: <?php echo $global['web_theme_color']; ?> !important;}
    .input-file-show:hover .type-file-button {background-color: <?php echo $global['web_theme_color']; ?> !important; }
    .color_z { color: <?php echo $global['web_theme_color']; ?> !important;}
    a.imgupload{
        background-color: <?php echo $global['web_theme_color']; ?> !important;
        border-color: <?php echo $global['web_theme_color']; ?> !important;
    }
    /*专题节点按钮*/
    .ncap-form-default .special-add{background-color:<?php echo $global['web_theme_color']; ?>;border-color:<?php echo $global['web_theme_color']; ?>;}
    .ncap-form-default .special-add:hover{background-color:<?php echo $global['web_assist_color']; ?>;border-color:<?php echo $global['web_assist_color']; ?>;}
    
    /*更多功能标题*/
    .on-off_panel .title::before {background-color:<?php echo $global['web_theme_color']; ?>;}

</style>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.js"></script>
<!-- <script type="text/javascript" src="/api/public/plugins/tags_input/js/jquery.tagsinput.js?v=<?php echo $version; ?>"></script> -->
<script type="text/javascript" src="/api/public/static/admin/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/api/public/plugins/layer-v3.1.0/layer.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/admin.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/common.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/api/public/plugins/layui/layui.js"></script>
<script src="/api/public/static/admin/js/myFormValidate.js"></script>
<script src="/api/public/static/admin/js/myAjax2.js?v=<?php echo $version; ?>"></script>
<script src="/api/public/static/admin/js/global.js?v=<?php echo $version; ?>"></script>
</head>
<body class="bodystyle" style="overflow-y: scroll; cursor: default; -moz-user-select: inherit;">
<style type="text/css">
    .system_table{ border:1px solid #dcdcdc; width:100%;}
    .system_table td{ height:40px; line-height:40px; font-size:12px; color:#454545; border-bottom:1px solid #dcdcdc; border-right:1px solid #dcdcdc; width:35%; padding-left:1%;}
    .system_table td.gray_bg{ background:#f7f7f7; width:15%;}
</style>
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>

<div class="page">
    <div class="flexigrid">
        <form class="form-horizontal" id="postForm">
            <input type="hidden" name="order_id" value="<?php echo $Service['order_id']; ?>">
            <input type="hidden" name="service_id" value="<?php echo $Service['service_id']; ?>">
            <input type="hidden" name="order_code" value="<?php echo $Service['order_code']; ?>">
            <input type="hidden" name="users_id" value="<?php echo $Users['users_id']; ?>">
            <input type="hidden" name="shipping_fee" value="<?php echo $Service['shipping_fee']; ?>">

            <!-- 会员信息 -->
            <div class="hDiv htitx">
                <div class="hDivBox">
                    <table cellspacing="0" cellpadding="0" style="width: 100%">
                        <thead>
                            <tr>
                                
                                <th abbr="article_title" axis="col3" class="w10">
                                    <div class="tl text-l10">会员信息</div>
                                </th>
                                <th abbr="ac_id" axis="col4">
                                    <div class=""></div>
                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="ncap-form-default">
                <table cellpadding="0" cellspacing="0" class="system_table">
                    <tbody>
                        <tr>
                            <td class="gray_bg">会员名：</td>
                            <td><a href="<?php echo url('Member/users_edit', ['id'=>$Users['users_id']]); ?>"><?php echo $Users['nickname']; ?></a></td>
                            <td class="gray_bg">手机号码：</td>
                            <td><?php echo $Users['mobile']; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- END -->

            <!-- 订单信息 -->
            <div class="hDiv htitx">
                <div class="hDivBox">
                    <table cellspacing="0" cellpadding="0" style="width: 100%">
                        <thead>
                            <tr>
                                <th abbr="article_title" axis="col3" class="w10">
                                    <div class="tl text-l10">订单信息</div>
                                </th>
                                <th abbr="ac_id" axis="col4">
                                    <div class=""></div>
                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="ncap-form-default">
                <table cellpadding="0" cellspacing="0" class="system_table">
                    <tbody>
                        <tr>
                            <td class="gray_bg">订单ID：</td>
                            <td>
                                <a href="<?php echo url('Shop/order_details', ['order_id'=>$Service['order_id']]); ?>"><?php echo $Service['order_id']; ?></a>
                            </td>
                            <td class="gray_bg">订单编号：</td>
                            <td>
                                <a href="<?php echo url('Shop/order_details', ['order_id'=>$Service['order_id']]); ?>"><?php echo $Service['order_code']; ?></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- END -->

            <!-- 商品信息 -->
            <div class="hDiv htitx">
                <div class="hDivBox">
                    <table cellspacing="0" cellpadding="0" style="width: 100%">
                        <thead>
                        <tr>
                            <th abbr="article_title" axis="col3" class="w10">
                                <div class="tl text-l10">商品信息</div>
                            </th>
                            <th abbr="ac_id" axis="col4">
                                <div class=""></div>
                            </th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="ncap-form-default">
                <table cellpadding="0" cellspacing="0" class="system_table">
                    <tbody>
                        <tr>
                            <td class="gray_bg">商品ID：</td>
                            <td><?php echo $Service['product_id']; ?></td>
                            <td class="gray_bg">退换数量：</td>
                            <td><?php echo $Service['product_num']; ?></td>
                        </tr>
                        <tr>
                            <td class="gray_bg">商品名称：</td>
                            <td>
                                <a href="<?php echo $Service['arcurl']; ?>" target="_blank">
                                    <img src="<?php echo $Service['product_img']; ?>" style="width: 60px;height: 60px;"> <?php echo $Service['product_name']; ?>
                                </a>
                            </td>
                            <!--<td class="gray_bg">商品规格：</td>-->
                            <!--<td><?php echo $Service['product_spec']; ?></td>-->
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- END -->

            <!-- 会员收货信息 -->
            <div class="hDiv htitx">
                <div class="hDivBox">
                    <table cellspacing="0" cellpadding="0" style="width: 100%">
                        <thead>
                            <tr>
                                <th abbr="article_title" axis="col3" class="w10">
                                    <div class="tl text-l10">会员收货信息</div>
                                </th>
                                <th abbr="ac_id" axis="col4">
                                    <div class=""></div>
                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="ncap-form-default">
                <table cellpadding="0" cellspacing="0" class="system_table">
                    <tbody>
                        <tr>
                            <td class="gray_bg">会员收货地址：</td>
                            <td><?php echo $Service['address']; ?></td>
                            <td class="gray_bg"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="gray_bg">收货人：</td>
                            <td><?php echo $Service['consignee']; ?></td>
                            <td class="gray_bg">收货人手机：</td>
                            <td><?php echo $Service['mobile']; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- END -->

            <!-- 服务详情 -->
            <div class="hDiv htitx">
                <div class="hDivBox">
                    <table cellspacing="0" cellpadding="0" style="width: 100%">
                        <thead>
                            <tr>
                                <th abbr="article_title" axis="col3" class="w10">
                                    <div class="tl text-l10">服务详情</div>
                                </th>
                                <th abbr="ac_id" axis="col4">
                                    <div class=""></div>
                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="ncap-form-default">
                <table cellpadding="0" cellspacing="0" class="system_table">
                    <tbody>
                        <tr>
                            <td class="gray_bg">服务类型：</td>
                            <td><?php echo $Service['TypeName']; ?></td>
                            <td class="gray_bg">当前状态：</td>
                            <td><?php echo $Service['StatusName']; ?></td>
                        </tr>
                        <tr>
                            <td class="gray_bg">申请时间：</td>
                            <td><?php echo MyDate('Y-m-d H:i:s',$Service['add_time']); ?></td>
                            <td class="gray_bg">问题描述：</td>
                            <td><?php echo $Service['content']; ?></td>
                        </tr>
                        <tr>
                            <!--<td class="gray_bg">图片信息：</td>-->
                            <!--<td>-->
                            <!--    <?php if(is_array($Service['upload_img']) || $Service['upload_img'] instanceof \think\Collection || $Service['upload_img'] instanceof \think\Paginator): $i = 0; $__LIST__ = $Service['upload_img'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>-->
                            <!--        <?php if(!(empty($vo) || (($vo instanceof \think\Collection || $vo instanceof \think\Paginator ) && $vo->isEmpty()))): ?><img src="<?php echo $vo; ?>" width="60" height="60"><?php endif; ?>-->
                            <!--    <?php endforeach; endif; else: echo "" ;endif; ?>-->
                            <!--</td>-->
                            <!--<td class="gray_bg">问题描述：</td>-->
                            <!--<td><?php echo $Service['content']; ?></td>-->
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- END -->

            <?php if($Service['status'] != '8'): if(!(empty($Service['users_delivery']) || (($Service['users_delivery'] instanceof \think\Collection || $Service['users_delivery'] instanceof \think\Paginator ) && $Service['users_delivery']->isEmpty()))): ?>
                <!-- 会员发货信息 -->
                <div class="hDiv htitx">
                    <div class="hDivBox">
                        <table cellspacing="0" cellpadding="0" style="width: 100%">
                            <thead>
                                <tr>
                                    <th abbr="article_title" axis="col3" class="w10">
                                        <div class="tl text-l10">会员发货信息</div>
                                    </th>
                                    <th abbr="ac_id" axis="col4">
                                        <div class=""></div>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="ncap-form-default">
                    <table cellpadding="0" cellspacing="0" class="system_table">
                        <tbody>
                            <tr>
                                <td class="gray_bg">快递公司：</td>
                                <td><?php echo $Service['users_delivery']['name']; ?></td>
                                <td class="gray_bg">快递单号：</td>
                                <td><?php echo $Service['users_delivery']['code']; ?></td>
                            </tr>
                            <tr>
                                <td class="gray_bg">快递费用：</td>
                                <td>
                                    <?php echo $Service['users_delivery']['cost']; ?>
                                </td>
                                <td class="gray_bg">发货时间：</td>
                                <td><?php echo $Service['users_delivery']['time']; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- END -->
                <?php endif; if($Service['service_type'] == '2'): ?>
                <!-- 退款详情 -->
                <div class="hDiv htitx">
                    <div class="hDivBox" id="fzdiv" data-usersid="<?php echo $Service['users_id']; ?>" data-servicid="<?php echo $Service['service_id']; ?>">
                        <table cellspacing="0" cellpadding="0" style="width: 100%">
                            <thead>
                                <tr>
                                    <th abbr="article_title" axis="col3" class="w10">
                                        <div class="tl text-l10">退款详情</div>
                                    </th>
                                    <th abbr="ac_id" axis="col4">
                                        <div class=""></div>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="ncap-form-default">
                    <table cellpadding="0" cellspacing="0" class="system_table">
                        <tbody>
                            <tr>
                                <td class="gray_bg">需退还金额：</td>
                                <td>
                                      <?php $ser = M('shop_order_service')->where(array('service_id' => $Service['service_id']))->find();  $ser['refund_price'] =  sprintf("%.2f", $ser['refund_price'] - $ser['coupon_price']);?>
                                    <input type="text" name="refund_price" value="<?php echo $ser['refund_price']; ?>">
                                    <span style="color: red;">已扣除 <?php echo $Service['ShippingFee']; ?> 运费</span>
                                </td>
                                <td class="gray_bg">退还说明：</td>
                                <td>
                                    <a style="color: #3b639f; font-size: 12px;" href="javascript:void(0);" onmouseover="layer_tips = layer.tips($('#Manual').html(), this, {time:100000});" onmouseout="layer.close(layer_tips);"> 查看说明 </a>
                                    <span style="display: none;" id="Manual">
                                        <div>
                                            1.暂时不支持金额原路退还，需自行将<span style="color: red;">需退还金额</span>通过转账方式退还会员后将数据设为<span style="color: red;">0</span>，再进行确认退款
                                        </div>
                                        <div>
                                            2.如不将<span style="color: red;">需退还金额</span>设为<span style="color: red;">0</span>则默认将<span style="color: red;">需退还金额</span>退还到<span style="color: red;">会员余额</span>中
                                        </div>
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- END -->
                <?php endif; ?>

                <!-- 审核处理 -->
                <div class="hDiv">
                    <div class="hDivBox">
                        <table cellspacing="0" cellpadding="0" style="width: 100%">
                            <thead>
                                <tr>
                                    <th abbr="article_title" axis="col3" class="w10">
                                        <div class="tl text-l10">审核处理</div>
                                    </th>
                                    <th abbr="ac_id" axis="col4">
                                        <div class=""></div>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="ncap-form-default">
                    <table cellpadding="0" cellspacing="0" class="system_table">
                        <tbody>
                            <?php if($Service['status'] == '1'): ?>
                            <tr>
                                <td class="gray_bg">审核意见：</td>
                                <td>
                                    <label><input type="radio" name="status" value="2">审核通过</label>
                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    <label><input type="radio" name="status" value="3">拒绝通过</label>
                                </td>
                                <td class="gray_bg">处理备注：</td>
                                <td>
                                    <textarea name="admin_note" style="width:300px; height:60px;" placeholder="管理员描述" class="tarea"><?php echo $Service['admin_note']; ?></textarea>
                                </td>
                            </tr>
                            <?php else: if($Service['status'] == '2'): ?>
                                <tr>
                                    <td class="gray_bg">可操作</td>
                                    <td>等待会员发货</td>
                                <?php endif; if($Service['status'] == '3'): ?>
                                <tr>
                                    <td class="gray_bg">可操作</td>
                                    <td>申请已被拒接</td>
                                <?php endif; if($Service['status'] == '4'): ?>
                                <tr>
                                    <td class="gray_bg">可操作</td>
                                    <td>
                                        <input type="hidden" name="status" value="5">
                                        <a href="JavaScript:;" onclick="ConfirmSubmit('确认收到货物？');" class="ncap-btn-big ncap-btn-green submitBtn">确认收货</a>
                                    </td>
                                <?php endif; if($Service['status'] == '5'): ?>
                                <tr>
                                    <?php if($Service['service_type'] == '2'): ?>
                                        <td class="gray_bg">可操作</td>
                                        <td>
                                            <input type="hidden" name="status" value="7">
                                            <a href="JavaScript:;" onclick="ConfirmSubmit2('确认退款？');" id="tui" class="ncap-btn-big ncap-btn-green submitBtn">确认退款</a>
                                        </td>
                                    <?php else: ?>
                                        <td class="gray_bg">可操作</td>
                                        <td>
                                            <input type="hidden" name="status" value="6">
                                            <div>快递公司：<input type="text" name="delivery[name]"></div>
                                            <div>快递单号：<input type="text" name="delivery[code]"></div>
                                            <a href="JavaScript:;" onclick="ConfirmSubmit('确认发货？');" class="ncap-btn-big ncap-btn-green submitBtn">确认发货</a>
                                        </td>
                                    <?php endif; endif; if($Service['status'] == '6'): ?>
                                <tr>
                                    <td class="gray_bg">可操作</td>
                                    <td>换货完成，服务结束</td>
                                <?php endif; if($Service['status'] == '7'): ?>
                                <tr>
                                    <td class="gray_bg">可操作</td>
                                    <td>退款完成，服务结束</td>
                                <?php endif; ?>
                                    <td class="gray_bg">处理备注：</td>
                                    <td><?php echo $Service['admin_note']; ?></td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <!-- END -->

                <?php if($Service['status'] == '6'): ?>
                <!-- 卖家发货信息 -->
                <div class="hDiv htitx">
                    <div class="hDivBox">
                        <table cellspacing="0" cellpadding="0" style="width: 100%">
                            <thead>
                                <tr>
                                    <th abbr="article_title" axis="col3" class="w10">
                                        <div class="tl text-l10">卖家发货信息</div>
                                    </th>
                                    <th abbr="ac_id" axis="col4">
                                        <div class=""></div>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="ncap-form-default">
                    <table cellpadding="0" cellspacing="0" class="system_table">
                        <tbody>
                            <tr>
                                <td class="gray_bg">快递公司：</td>
                                <td><?php echo $Service['admin_delivery']['name']; ?></td>
                                <td class="gray_bg">快递单号：</td>
                                <td><?php echo $Service['admin_delivery']['code']; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- END -->
                <?php endif; ?>

                <input type="hidden" id="url" value="<?php echo url('ShopService/after_service_deal_with', ['_ajax'=>1]); ?>">
                <?php if($Service['status'] == '1'): ?>
                    <div class="bot" style="text-align: center;">
                        <a href="JavaScript:void(0);" onclick="ConfirmSubmit();" class="ncap-btn-big ncap-btn-green submitBtn">确认提交</a>
                    </div>
                <?php endif; ?>
                <br/>
                <script type="text/javascript">
                    function ConfirmSubmit(title) {
                        if (!title) {
                            // 无需二次确认
                            Submit();
                        } else {
                            // 需要二次确认
                            layer.confirm(title, {
                                title:false,
                                closeBtn: 0
                            },function(){
                                Submit();
                            },function(index){
                                layer.closeAll(index);
                            });
                        }
                    }
                    
                    function ConfirmSubmit2(title) {
                        if (!title) {
                            // 无需二次确认
                            Submit2();
                        } else {
                            // 需要二次确认
                            layer.confirm(title, {
                                title:false,
                                closeBtn: 0
                            },function(){
                                Submit2();
                            },function(index){
                                layer.closeAll(index);
                            });
                        }
                    }
                    
                    
                    function Submit2() {
                        var service_id = $("#fzdiv").attr("data-servicid");
                        var users_id = $("#fzdiv").attr("data-usersid");
                        alert(service_id);
                        alert(users_id);
                        console.log(service_id,users_id);
                        layer_loading('正在处理');
                        $.ajax({
                            url: $('#url').val(),
                            data: $('#postForm').serialize(),
                            type:'post',
                            dataType:'json',
                            success:function(res){
                                if (1 == res.code) {
                                    if (7 == res.data.status) res.msg = '退款完成';
                                    layer.closeAll();
                                    jifen(service_id,users_id);
                                    // if($("")){
                                        
                                    // }
                                    
                                    // layer.msg(res.msg, {time: 1500}, function() {
                                    //     window.location.reload();
                                    // });
                                } else {
                                    layer.closeAll();
                                    layer.msg(res.msg, {time: 1500});
                                    if (res.data.id) $("input[name='delivery["+res.data.id+"]']").focus();
                                }
                            },
                            error : function() {
                                layer.closeAll();
                                layer.alert('未知错误！', {title:false, icon:5, closeBtn: 0});
                            }
                        });
                    }
                    

                    function Submit() {
                        var service_id = $("#fzdiv").attr("data-servicid");
                        var users_id = $("#fzdiv").attr("data-usersid");
                        console.log(service_id,users_id);
                        layer_loading('正在处理');
                        $.ajax({
                            url: $('#url').val(),
                            data: $('#postForm').serialize(),
                            type:'post',
                            dataType:'json',
                            success:function(res){
                                if (1 == res.code) {
                                    if (7 == res.data.status) res.msg = '退款完成';
                                    layer.closeAll();
                        
                                    // if($("")){
                                        
                                    // }
                                    
                                    // layer.msg(res.msg, {time: 1500}, function() {
                                    //     window.location.reload();
                                    // });
                                } else {
                                    layer.closeAll();
                                    layer.msg(res.msg, {time: 1500});
                                    if (res.data.id) $("input[name='delivery["+res.data.id+"]']").focus();
                                }
                            },
                            error : function() {
                                layer.closeAll();
                                layer.alert('未知错误！', {title:false, icon:5, closeBtn: 0});
                            }
                        });
                    }
                    
                    function jifen(service_id,users_id){
                        $.ajax({
                            url: "<?php echo url('Shop/after_service_deal_with'); ?>",
                            data: {
                                service_id: service_id,
                                users_id: users_id,
                            },
                            type:'post',
                            dataType:'json',
                            success:function(res) {
                                layer.closeAll();
                                if (1 == res.code) {
                                    layer.msg(res.msg, {time: 1500}, function() {
                                        window.location.reload();
                                    });
                                } else {
                                    layer.msg(res.msg, {time: 1500});
                                }
                            }
                        });
                    }
                    
                </script>
            <?php endif; ?>

            <!-- 操作记录 -->
            <div class="hDiv htitx">
                <div class="hDivBox">
                    <table cellspacing="0" cellpadding="0" style="width: 100%">
                        <thead>
                        <tr>
                            <th abbr="article_title" axis="col3" class="w10">
                                <div class="tl text-l10">操作记录</div>
                            </th>
                            <th abbr="ac_id" axis="col4">
                                <div class=""></div>
                            </th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="hDiv" style="margin-top: 5px;">
                <div class="hDivBox">
                    <table cellspacing="0" cellpadding="0" style="width: 100%">
                        <thead>
                        <tr>
                            <th class="sign" axis="col0">
                                <div class="tl text-l10">操作者</div>
                            </th>
                            <th abbr="article_title" axis="col3" class="w210">
                                <div class="tc">操作时间</div>
                            </th>
                            <th abbr="article_title" axis="col3" class="w300">
                                <div style="text-align: left; padding-left: 10px;" class="">操作备注</div>
                            </th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="bDiv" style="height: auto;">
                <div id="flexigrid" cellpadding="0" cellspacing="0" border="0">
                    <table style="width: 100%;">
                        <tbody>
                           
                        <?php if(empty($Log) || (($Log instanceof \think\Collection || $Log instanceof \think\Paginator ) && $Log->isEmpty())): ?>
                            <tr>
                                <td class="no-data" align="center" axis="col0" colspan="50">
                                    <i class="fa fa-exclamation-circle"></i>没有符合条件的记录
                                </td>
                            </tr>
                        <?php else: if(is_array($Log) || $Log instanceof \think\Collection || $Log instanceof \think\Paginator): $i = 0; $__LIST__ = $Log;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                            <tr>
                                <td class="sort">
                                    <div class="tl text-l10">
                                        <?php echo $vo['name']; ?>
                                    </div>
                                </td>
                                <td class="sort w210">
                                    <div class="tc">
                                        <?php echo MyDate('Y-m-d H:i:s',$vo['add_time']); ?>
                                    </div>
                                </td>
                                <td class="w300">
                                    <div class="tl" style="padding-left: 10px;">
                                        <?php if($vo['log_note'] == ''): ?>
                                        会员已将货物发出，等待商家收货！
                                        <?php else: ?>
                                         <?php echo $vo['log_note']; endif; ?>
                                       
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; endif; else: echo "" ;endif; endif; ?>
                        </tbody>
                    </table>
                </div>
                <div class="iDiv" style="display: none;"></div>
            </div>
            <!-- END -->
        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        // 表格行点击选中切换
        $('#flexigrid > table>tbody >tr').click(function(){
            $(this).toggleClass('trSelected');
        });

        // 点击刷新数据
        $('.fa-refresh').click(function(){
            location.href = location.href;
        });
    });
</script>

<br/>
<div id="goTop">
    <a href="JavaScript:void(0);" id="btntop">
        <i class="fa fa-angle-up"></i>
    </a>
    <a href="JavaScript:void(0);" id="btnbottom">
        <i class="fa fa-angle-down"></i>
    </a>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#think_page_trace_open').css('z-index', 99999);
    });
</script>
</body>
</html>