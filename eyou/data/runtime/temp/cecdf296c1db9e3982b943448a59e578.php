<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:41:"./template/plugins/ask/pc/edit_answer.htm";i:1634539380;s:48:"/mnt/eyou/template/plugins/ask/pc/ask_header.htm";i:1634539380;}*/ ?>
<!DOCTYPE html>
<html>
<head> 
    <title>编辑回答-问答中心</title> 
    <meta name="renderer" content="webkit" /> 
    <meta charset="utf-8" /> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=0,minimal-ui" /> 
    <link href="<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_cmspath"); echo $__VALUE__; ?>/favicon.ico" rel="shortcut icon" type="image/x-icon" /> 
    <?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("skin/css/eyoucms.css","","","ask"); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("skin/css/ask.css","","","ask"); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/static/common/js/jquery.min.js","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/plugins/layer-v3.1.0/layer.js","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("skin/js/ask.js","","","ask"); echo $__VALUE__; ?>
</head>

<body>
<!-- 头部 -->
<nav class="navbar navbar-default met-nav navbar-fixed-top" role="navigation">
    <div class="ey-head">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-sm-6 logo">
                    <ul class="list-none">
                        <li><a href="<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_cmsurl"); echo $__VALUE__; ?>" class="ey-logo"><img src="<?php  $tagGlobal = new \think\template\taglib\eyou\TagGlobal; $__VALUE__ = $tagGlobal->getGlobal("web_logo"); echo $__VALUE__; ?>" style="max-height: 60px;" /></a></li>
                        <li>问答中心</li>
                    </ul>
                </div>
                <div class="col-xs-6 col-sm-6 user-info">
                    <ol class="breadcrumb pull-right">
                        <?php if($users['users_id'] == '0'): ?>
                        <li><a href="<?php echo url("user/Users/login","",true,false,null,null,null);?>" title="登录">登录</a></li>
                        <li><a href="<?php echo url("user/Users/reg","",true,false,null,null,null);?>" title="注册">注册</a></li>
                        <?php else: ?>
                        <li><a href="<?php echo url("user/Users/centre","",true,false,null,null,null);?>"><?php echo $users['nickname']; ?></a></li>
                        <li><a href="<?php echo url("user/Users/logout","",true,false,null,null,null);?>" title="退出登录">退出登录</a></li>
                        <?php endif; ?>
                    </ol>
                    <button type="button" class="hamburger animated fadeInLeft is-closed" data-toggle="offcanvas">
                    <span class="hamb-top"></span>
                    <span class="hamb-middle"></span>
                    <span class="hamb-bottom"></span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</nav>
<!-- END -->

<main class="mian-body container" style="">
    <div class="main_box main_box_a" style="">
        <div class="tiwen_box mar_t38" style="">
            <form id="AnswerFormData" method="post" action="<?php echo $eyou['field']['EditAnswerUrl']; ?>">
                <input type="hidden" name="m" value="plugins">
                <input type="hidden" name="c" value="Ask">
                <input type="hidden" name="a" value="ajax_edit_answer">
                <input type="hidden" name="ask_id" value="<?php echo $eyou['field']['Info']['ask_id']; ?>">
                <p class="tt-img">
                    <img src="/template/plugins/ask/pc/skin/images/twtop.png">
                </p>

                <div class="tiwen_main mar_t20" style="">
                    <p class="ti">
                        <big><strong>问题标题</strong></big>
                    </p>
                    <div class="mar_t10 ts">
                        <?php echo $eyou['field']['Info']['ask_title']; ?>
                    </div>

                    <p class="hui_font mar_t10 ti">
                        <big><strong>问题回答</strong></big>
                        <font class="font12 jiu_font mar_l10"><?php echo $eyou['field']['Info']['nickname']; ?>，您要修改的答复如下</font>
                    </p>
                    <div class="mar_t10 da" style="">
                        <script type="text/javascript">
                            window.UEDITOR_HOME_URL = "/public/plugins/Ueditor/";
                        </script> 
                        <?php  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/plugins/Ueditor/ueditor.config.js","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/plugins/Ueditor/ueditor.all.min.js","","",""); echo $__VALUE__;  $tagStatic = new \think\template\taglib\eyou\TagStatic; $__VALUE__ = $tagStatic->getStatic("/public/plugins/Ueditor/lang/zh-cn/zh-cn.js","","",""); echo $__VALUE__; ?>
                        <!-- HTML文本 start -->
                        <div class="row">
                            <div class="col-xs-9">
                                <textarea class="span12 ckeditor" id="content" name="content"><?php echo $eyou['field']['Info']['content']; ?></textarea>
                            </div>
                        </div>
                        <script type="text/javascript">
                            UE.getEditor('content',{
                                serverUrl :"<?php echo url('user/Uploadify/index',array('savepath'=>'weapp')); ?>",
                                zIndex: 999,
                                initialFrameWidth: "135%", //初化宽度
                                initialFrameHeight: 300, //初化高度            
                                focus: false, //初始化时，是否让编辑器获得焦点true或false
                                maximumWords: 99999,
                                removeFormatAttributes: 'class,style,lang,width,height,align,hspace,valign',//允许的最大字符数 'fullscreen',
                                pasteplain:false, //是否默认为纯文本粘贴。false为不使用纯文本粘贴，true为使用纯文本粘贴
                                autoHeightEnabled: false,
                                toolbars: [["forecolor", "backcolor", "removeformat", "|", "simpleupload", "unlink"]],
                            });

                            //必须在提交前渲染编辑器；
                            function content() {
                                //判断编辑模式状态:0表示【源代码】HTML视图；1是【设计】视图,即可见即所得；-1表示不可用
                                if(UE.getEditor("content").queryCommandState('source') != 0) {
                                    UE.getEditor("content").execCommand('source'); //切换到【设计】视图
                                }
                            }
                        </script>
                    </div>

                    <div class="width100 mar_t10 et da">
                        <span class="el"></span>
                        <span class="rc">
                            <font id="errorTips"></font>
                            <button type="button" class="sz_button float_r" data-url="<?php echo $eyou['field']['EditAnswerUrl']; ?>" onclick="SubmitData(this);">提交</button>
                        </span>
                    </div>
                </div>
            </form>
        </div>
    </div>
</main>

<script type="text/javascript">
    function SubmitData(obj) {
        if (!$('textarea[name="content"]').val()) {
            layer.msg('请填写问题描述！', {time: 1500, icon: 2});
            return false;
        }

        layer_loading('正在处理');
        $.ajax({
            url: $(obj).data('url'),
            data: $('#AnswerFormData').serialize(),
            type:'post',
            dataType:'json',
            success:function(res){
                layer.closeAll();
                if (1 == res.code) {
                    if (res.data.review) {
                        var times = 2500;
                    }else{
                        var times = 1000;
                    }
                    layer.msg(res.msg, {time: times},function(){
                        window.location.href = res.url;
                    });
                } else {
                    layer.msg(res.msg, {time: 1500, icon: 2});
                }
            },
            error : function() {
                layer.closeAll();
                layer.alert('网络失败，请刷新页面后重试', {icon: 5});
            }
        });
    }
</script>
</body>
</html>