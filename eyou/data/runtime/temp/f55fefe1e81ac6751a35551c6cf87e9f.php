<?php if (!defined('THINK_PATH')) exit(); /*a:5:{s:49:"./application/admin/template/statistics/index.htm";i:1640913891;s:57:"/mnt/api/api/application/admin/template/public/layout.htm";i:1640913882;s:60:"/mnt/api/api/application/admin/template/public/theme_css.htm";i:1640913882;s:53:"/mnt/api/api/application/admin/template/shop/left.htm";i:1640913887;s:57:"/mnt/api/api/application/admin/template/public/footer.htm";i:1640913882;}*/ ?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link href="/api/public/plugins/layui/css/layui.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/main.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/css/page.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<link href="/api/public/static/admin/font/css/font-awesome.min.css" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="/api/public/static/admin/font/css/font-awesome-ie7.min.css">
<![endif]-->
<script type="text/javascript">
    var eyou_basefile = "<?php echo \think\Request::instance()->baseFile(); ?>";
    var module_name = "<?php echo MODULE_NAME; ?>";
    var GetUploadify_url = "<?php echo url('Uploadify/upload'); ?>";
    var __root_dir__ = "/api";
    var __lang__ = "<?php echo $admin_lang; ?>";
</script>  
<link href="/api/public/static/admin/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="/api/public/static/admin/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
<!-- <link type="text/css" rel="stylesheet" href="/api/public/plugins/tags_input/css/jquery.tagsinput.css?v=<?php echo $version; ?>"> -->
<style type="text/css">html, body { overflow: visible;}</style>
<link href="/api/public/static/admin/css/diy_style.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css" />

<!-- 官方内置样式表，升级会覆盖变动，请勿修改，否则后果自负 -->

<style type="text/css">
	/*左侧收缩图标*/
	#foldSidebar i { font-size: 24px;color:<?php echo $global['web_theme_color']; ?>; }
    /*左侧菜单*/
    .eycms_cont_left{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl dd a:hover,.eycms_cont_left dl dd a.on,.eycms_cont_left dl dt.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl dd a:active{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist dd{ background:<?php echo $global['web_theme_color']; ?>; }
    .eycms_cont_left dl.jslist dd a:hover,.eycms_cont_left dl.jslist dd a.on{ background:<?php echo $global['web_assist_color']; ?>; }
    .eycms_cont_left dl.jslist:hover{ background:<?php echo $global['web_assist_color']; ?>; }
    /*栏目操作*/
    .cate-dropup .cate-dropup-con a:hover{ background-color: <?php echo $global['web_theme_color']; ?>; }
    /*按钮*/
    a.ncap-btn-green { background-color: <?php echo $global['web_theme_color']; ?>; }
    a:hover.ncap-btn-green { background-color: <?php echo $global['web_assist_color']; ?>; }
    .flexigrid .sDiv2 .btn:hover { background-color: <?php echo $global['web_theme_color']; ?>; }
    .flexigrid .mDiv .fbutton div.add{background-color: <?php echo $global['web_theme_color']; ?>; border: none;}
    .flexigrid .mDiv .fbutton div.add:hover{ background-color: <?php echo $global['web_assist_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds{color:<?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;}
	.flexigrid .mDiv .fbutton div.adds:hover{ background-color: <?php echo $global['web_theme_color']; ?>;}
    /*选项卡字体*/
    .tab-base a.current,
    .tab-base a:hover.current { border-bottom: solid 2px <?php echo $global['web_theme_color']; ?> !important;color: <?php echo $global['web_theme_color']; ?> !important;}
    .addartbtn input.btn:hover{ border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>; }
    .addartbtn input.btn.selected{ color: <?php echo $global['web_theme_color']; ?>;border-bottom: 1px solid <?php echo $global['web_theme_color']; ?>;}
	/*会员导航*/
	.member-nav-group .member-nav-item .btn.selected{background: <?php echo $global['web_theme_color']; ?>;border: 1px solid <?php echo $global['web_theme_color']; ?>;box-shadow: -1px 0 0 0 <?php echo $global['web_theme_color']; ?>;}
	.member-nav-group .member-nav-item:first-child .btn.selected{border-left: 1px solid <?php echo $global['web_theme_color']; ?> !important;}
	/*搜索按钮图标*/
	.flexigrid .sDiv2 .fa-search{}
        
    /* 商品订单列表标题 */
   .flexigrid .mDiv .ftitle h3 {border-left: 3px solid <?php echo $global['web_theme_color']; ?>;} 
	
    /*分页*/
    .pagination > .active > a, .pagination > .active > a:focus, 
	.pagination > .active > a:hover, 
	.pagination > .active > span, 
	.pagination > .active > span:focus, 
	.pagination > .active > span:hover { border-color: <?php echo $global['web_theme_color']; ?>;color:<?php echo $global['web_theme_color']; ?>; }
    
    .layui-form-onswitch {border-color: <?php echo $global['web_theme_color']; ?> !important;background-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-enable.selected { background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    .onoff .cb-disable.selected {background-color: <?php echo $global['web_theme_color']; ?> !important;border-color: <?php echo $global['web_theme_color']; ?> !important;}
    input[type="text"]:focus,
    input[type="text"]:hover,
    input[type="text"]:active,
    input[type="password"]:focus,
    input[type="password"]:hover,
    input[type="password"]:active,
    textarea:hover,
    textarea:focus,
    textarea:active { border-color:<?php echo hex2rgba($global['web_theme_color'],0.8); ?>;box-shadow: 0 0 0 2px <?php echo hex2rgba($global['web_theme_color'],0.15); ?> !important;}
    .input-file-show:hover .type-file-button {background-color:<?php echo $global['web_theme_color']; ?> !important; }
    .input-file-show:hover {border-color: <?php echo $global['web_theme_color']; ?> !important;box-shadow: 0 0 5px <?php echo hex2rgba($global['web_theme_color'],0.5); ?> !important;}
    .input-file-show:hover span.show a,
    .input-file-show span.show a:hover { color: <?php echo $global['web_theme_color']; ?> !important;}
    .input-file-show:hover .type-file-button {background-color: <?php echo $global['web_theme_color']; ?> !important; }
    .color_z { color: <?php echo $global['web_theme_color']; ?> !important;}
    a.imgupload{
        background-color: <?php echo $global['web_theme_color']; ?> !important;
        border-color: <?php echo $global['web_theme_color']; ?> !important;
    }
    /*专题节点按钮*/
    .ncap-form-default .special-add{background-color:<?php echo $global['web_theme_color']; ?>;border-color:<?php echo $global['web_theme_color']; ?>;}
    .ncap-form-default .special-add:hover{background-color:<?php echo $global['web_assist_color']; ?>;border-color:<?php echo $global['web_assist_color']; ?>;}
    
    /*更多功能标题*/
    .on-off_panel .title::before {background-color:<?php echo $global['web_theme_color']; ?>;}

</style>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.js"></script>
<!-- <script type="text/javascript" src="/api/public/plugins/tags_input/js/jquery.tagsinput.js?v=<?php echo $version; ?>"></script> -->
<script type="text/javascript" src="/api/public/static/admin/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/api/public/plugins/layer-v3.1.0/layer.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/admin.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/common.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="/api/public/static/admin/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/api/public/static/admin/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/api/public/plugins/layui/layui.js"></script>
<script src="/api/public/static/admin/js/myFormValidate.js"></script>
<script src="/api/public/static/admin/js/myAjax2.js?v=<?php echo $version; ?>"></script>
<script src="/api/public/static/admin/js/global.js?v=<?php echo $version; ?>"></script>
</head>
<script type="text/javascript" src="/api/public/plugins/Echarts/echarts.min.js"></script>
<link href="/api/public/static/admin/css/main_new.css?v=<?php echo $version; ?>" rel="stylesheet" type="text/css">
<body style="overflow: auto; cursor: default; -moz-user-select: inherit;background-color:#F4F4F4; padding: 20px; ">
	<div id="append_parent"></div>
	<div id="ajaxwaitid"></div>
	<div class="sidebar-second ">
    <ul>
        <li class="sidebar-second-title">商城中心</li>
        <li>
            <a <?php if('Statistics' == CONTROLLER_NAME): ?>class="active"<?php endif; ?> href='<?php echo url("Statistics/index"); ?>'>数据统计</a>
        </li>
        <li>
            <a <?php if(('ShopService' == CONTROLLER_NAME) OR ('Shop' == CONTROLLER_NAME and in_array(ACTION_NAME, ['index', 'order_details']))): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/index"); ?>'>所有订单</a>
        </li>
        <li>
            <a <?php if('ShopProduct' == CONTROLLER_NAME and in_array(ACTION_NAME, ['index', 'add', 'edit'])): ?>class="active"<?php endif; ?> href='<?php echo url("ShopProduct/index"); ?>'>商品管理</a>
        </li>
        <li>
            <a <?php if('ShopProduct' == CONTROLLER_NAME and in_array(ACTION_NAME, ['attrlist_index', 'attribute_index'])): ?>class="active"<?php endif; ?> href='<?php echo url("ShopProduct/attrlist_index"); ?>'>商品参数</a>
        </li>
        <?php 
            $shop_open_spec = getUsersConfigData('shop.shop_open_spec');
         if($shop_open_spec == '1'): ?>
        <li>
            <a <?php if('Shop' == CONTROLLER_NAME and in_array(ACTION_NAME, ['spec_index'])): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/spec_index"); ?>'>商品规格</a>
        </li>
        <?php endif; ?>
        <li>
            <a <?php if('Shop' == CONTROLLER_NAME and 'conf' == ACTION_NAME): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/conf"); ?>'>商城配置</a>
        </li>
        <?php if($php_servicemeal >= 2): ?>
        <li>
            <a <?php if('ShopComment' == CONTROLLER_NAME): ?>class="active"<?php endif; ?> href='<?php echo url("ShopComment/comment_index"); ?>'>商品评价</a>
        </li>
        <li>
            <a <?php if((in_array(CONTROLLER_NAME, ['Sharp','Coupon'])) OR ('Shop' == CONTROLLER_NAME and in_array(ACTION_NAME, ['market_index']))): ?>class="active"<?php endif; ?> href='<?php echo url("Shop/market_index"); ?>'>营销功能</a>
        </li>
        <?php endif; ?>
    </ul>
</div>
	<div class="page" style="min-width:auto;margin-left:98px;">
		<div class="flexigrid">
			<div class="mDiv">
				<div class="ftitle">
					<h3>数据统计</h3>
				</div>
			</div>
			<div class="list-stats">
				<div class="stats-con">
					开始日期 &nbsp;<input type="text" class="input-txt" id="add_time" name="add_time" value="<?php echo date('Y-m-d H:i:s',$StartTime); ?>" autocomplete="off">
					结束日期 &nbsp;<input type="text" class="input-txt" id="add_time2" name="add_time" value="<?php echo date('Y-m-d H:i:s',$EndTime); ?>" autocomplete="off">
					&nbsp; &nbsp;
					<a href="javascript:void(0);" onclick="TimeProcessing(this, null, null, 1);">一个月</a>
					&nbsp; &nbsp;
					<a href="javascript:void(0);" onclick="TimeProcessing(this, null, null, 3);">三个月</a>
					&nbsp; &nbsp;
					<a href="javascript:void(0);" onclick="TimeProcessing(this, null, null, 6);">六个月</a>
					&nbsp; &nbsp;
					<a href="javascript:void(0);" onclick="TimeProcessing(this, null, null, 9);">九个月</a>
					&nbsp; &nbsp;
					<a href="javascript:void(0);" onclick="TimeProcessing(this, null, null, 12);">一年</a>
				</div>
				<script type="text/javascript">
					layui.use('laydate', function() {
					  	var laydate = layui.laydate;
					  	laydate.render({
						  	elem: '#add_time', type: 'datetime',
						  	change: function(value, date, endDate) {
						  		// TimeProcessing(value);
						  	},
						  	done: function(value, date, endDate){
						    	TimeProcessing(null, value);
						  	}
						});

						laydate.render({
						  	elem: '#add_time2', type: 'datetime',
						  	change: function(value, date, endDate) {
						  		// TimeProcessing(null, null, value);
						  	},
						  	done: function(value, date, endDate){
						    	TimeProcessing(null, null, value);
						  	}
						});
					});

					// 处理时间并查询数据
					function TimeProcessing(obj, Start, End ,Year) {
						Start = Start ? Start : $('#add_time').val();
						End   = End ? End : $('#add_time2').val();
						if (!Year && Start && End) {
							if (End < Start) {
								$('body').click();
								layer.msg('结束日期不可早于开始日期', {icon: 2,time: 2000});
								return false;
							}
							Year = 0;
						}
						if ((!End || !Start) && !Year) return false;
						
						var Url = "<?php echo url('Statistics/GetTimeCycletData'); ?>";
						$.ajax({
                            url  : Url,
                            type : 'post',
                            data : {StartNew:Start, EndNew:End, Year:Year, _ajax:1},
                            dataType : 'json',
                            success : function(res){
                                layer.closeAll();
                                $('#UsersNum').empty().html(res.data.UsersNum);
                                $('#PayOrderNum').empty().html(res.data.PayOrderNum);
                                $('#OrderSales').empty().html(res.data.OrderSales);
                                $('#ProductNum').empty().html(res.data.ProductNum);
                                $('#OrderUsersNum').empty().html(res.data.OrderUsersNum);
                                $('#UsersRecharge').empty().html(res.data.UsersRecharge);
                                if (res.data.Start) $('#add_time').val(res.data.Start);
                                if (res.data.End) $('#add_time2').val(res.data.End);
                                if (obj) {
	                                $('.stats-con a').css('color', '');
	                                $(obj).css('color', 'red');
                                }
                            }
                        });
					}
				</script>

				<ul class="stats2">
					<li>
						<div class="card_box">
							<div class="car_box_l fl">
								<i class="card-icon fa fa-user"></i>
							</div>
							<div class="car_box_r fl">
								<h1>用户数量</h1>
								<p><cite id="UsersNum"><?php echo $CycletData['UsersNum']; ?></cite></p>
							</div>
						</div>
					</li>
					<li>
						<div class="card_box">
							<div class="car_box_l fl">
								<i class="card-icon fa fa-shopping-cart"></i>
							</div>
							<div class="car_box_r fl">
								<h1>付款订单数</h1>
								<p><cite id="PayOrderNum"><?php echo $CycletData['PayOrderNum']; ?></cite></p>
							</div>
						</div>
					</li>
					<li>
						<div class="card_box">
							<div class="car_box_l fl">
								<i class="card-icon fa fa-cubes"></i>
							</div>
							<div class="car_box_r fl">
								<h1>商品数量</h1>
								<p><cite id="ProductNum"><?php echo $CycletData['ProductNum']; ?></cite></p>
							</div>
						</div>
					</li>
					<li>
						<div class="card_box">
							<div class="car_box_l fl">
								<i class="card-icon fa fa-group"></i>
							</div>
							<div class="car_box_r fl">
								<h1>消费人数</h1>
								<p><cite id="OrderUsersNum"><?php echo $CycletData['OrderUsersNum']; ?></cite></p>
							</div>
						</div>
					</li>
					<li>
						<div class="card_box">
							<div class="car_box_l fl">
								<i class="card-icon fa fa-rmb"></i>
							</div>
							<div class="car_box_r fl">
								<h1>付款订单总额</h1>
								<p><cite id="OrderSales"><?php echo $CycletData['OrderSales']; ?></cite></p>
							</div>
						</div>
					</li>
					<li>
						<div class="card_box">
							<div class="car_box_l fl">
								<i class="card-icon fa fa-rmb"></i>
							</div>
							<div class="car_box_r fl">
								<h1>用户充值总额</h1>
								<p><cite id="UsersRecharge"><?php echo $CycletData['UsersRecharge']; ?></cite></p>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="page mt20" style="min-width:auto;margin-left:98px;">
		<div class="flexigrid">
			<div class="mDiv">
				<div class="ftitle">
					<h3>近七日交易走势</h3>
				</div>
			</div>
			<div class="list-stats">
				<div class="stats-con">
					<div id="stats-box" style="width:90%;height:400px;margin: 0 auto;"></div>

				</div>
			</div>
		</div>
	</div>

	<div class="stats-content" style="min-width:auto;margin-left:98px;width:calc(100% - 98px); ">
		<div class="stats-content-l fl">
			<div class="flexigrid">
				<div class="mDiv">
					<div class="ftitle">
						<h3>商品销售榜</h3>
					</div>
				</div>
				<div class="list-stats">
					<div class="stats-con">
						<table border="" cellspacing="" cellpadding="" style="width: 100%;">
							<thead>
								<tr>
									<th class="w100 tc">
										<div>排名</div>
									</th>
									<th class="">
										<div>商品</div>
									</th>
									<th class="w100 tc">
										<div>销量</div>
									</th>
									<th class="w100 tc">
										<div>销售额</div>
									</th>
								</tr>
							</thead>
							<tbody>
								<?php if(is_array($OrderSalesList) || $OrderSalesList instanceof \think\Collection || $OrderSalesList instanceof \think\Paginator): $i = 0; $__LIST__ = $OrderSalesList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
									<tr>
										<td>
											<div class="tc">
												<?php if($i < '4'): ?>
													<img src="/api/public/static/admin/images/0<?php echo $i; ?>.png">
												<?php else: ?>
													<?php echo $i; endif; ?>
											</div>
										</td>
										<td>
											<div>
												<a href="<?php echo url('ShopProduct/edit', ['id'=>$vo['aid']]); ?>"><?php echo $vo['title']; ?></a>
												<a href="<?php echo $vo['arcurl']; ?>" target="_blank" style="color: red;">[预览]</a>
											</div>
										</td>
										<td>
											<div class="tc"><?php echo $vo['sales_num']; ?></div>
										</td>
										<td>
											<div class="tc"><?php echo $vo['sales_amount']; ?></div>
										</td>
									</tr>
								<?php endforeach; endif; else: echo "" ;endif; ?>
							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>
		<div class="stats-content-r fr">
			<div class="flexigrid">
				<div class="mDiv">
					<div class="ftitle">
						<h3>用户消费榜</h3>
					</div>
				</div>
				<div class="list-stats">
					<div class="stats-con">
						<table border="" cellspacing="" cellpadding="" style="width: 100%;">
							<thead>
								<tr>
									<th class="w100 tc">
										<div>排名</div>
									</th>
									<th class="">
										<div>用户昵称</div>
									</th>
									<th class="w100 tc">
										<div>实际消费金额</div>
									</th>
								</tr>
							</thead>
							<tbody>
								<?php if(is_array($UserConsumption) || $UserConsumption instanceof \think\Collection || $UserConsumption instanceof \think\Paginator): $i = 0; $__LIST__ = $UserConsumption;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
									<tr>
										<td>
											<div class="tc"><?php if($i < '4'): ?><img src="/api/public/static/admin/images/0<?php echo $i; ?>.png"><?php else: ?><?php echo $i; endif; ?></div>
										</td>
										<td>
											<div><?php echo $vo['nickname']; ?></div>
										</td>
										<td>
											<div class="tc"><?php echo $vo['amount']; ?></div>
										</td>
									</tr>
								<?php endforeach; endif; else: echo "" ;endif; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		function getBeforeDate(n) {
	        var n = n;
	        var d = new Date();
	        var year = d.getFullYear();
	        var mon = d.getMonth() + 1;
	        var day = d.getDate();
	        if(day <= n) {
	            if(mon > 1) {
	                mon = mon - 1;
	            } else {
	                year = year - 1;
	                mon = 12;
	            }
	        }
	        d.setDate(d.getDate() - n);
	        year = d.getFullYear();
	        mon = d.getMonth() + 1;
	        day = d.getDate();
	        s = year + "-" + (mon < 10 ? ('0' + mon) : mon) + "-" + (day < 10 ? ('0' + day) : day);
	        return s;
	    }

		// 基于准备好的dom，初始化echarts实例
		var myChart = echarts.init(document.getElementById('stats-box'));

		// 指定图表的配置项和数据
		var option = {
			color: ['#3398DB', '#6be6c1'],
			title: {
				text: ''
			},
			tooltip: {
				trigger: 'axis',
			},
			legend: {
				data: ['成交量', '成交额']
			},
			grid: {
				left: '3%',
				right: '4%',
				bottom: '3%',
				containLabel: true
			},
			toolbox: {
				feature: {
					saveAsImage: {}
				}
			},
			xAxis: {
				type: 'category',
				boundaryGap: false,
				data: [getBeforeDate(6), getBeforeDate(5), getBeforeDate(4), getBeforeDate(3), getBeforeDate(2), getBeforeDate(1), getBeforeDate(0)],
				splitLine: {
					show: true,
					lineStyle: {
						color: ['#eee'],
						width: 1,
						type: 'solid'
					}
				}
			},
			yAxis: {
				type: 'value',
				splitLine: {
					show: true,
					lineStyle: {
						color: ['#eee'],
						width: 1,
						type: 'solid'
					}
				}
			},
			series: [{
					name: '成交量',
					type: 'line',
					data: ['<?php echo $DealNum['0']; ?>','<?php echo $DealNum['1']; ?>','<?php echo $DealNum['2']; ?>','<?php echo $DealNum['3']; ?>','<?php echo $DealNum['4']; ?>','<?php echo $DealNum['5']; ?>','<?php echo $DealNum['6']; ?>'],
				},
				{
					name: '成交额',
					type: 'line',
					data: ['<?php echo $DealAmount['0']; ?>','<?php echo $DealAmount['1']; ?>','<?php echo $DealAmount['2']; ?>','<?php echo $DealAmount['3']; ?>','<?php echo $DealAmount['4']; ?>','<?php echo $DealAmount['5']; ?>','<?php echo $DealAmount['6']; ?>'],
				},
			]
		};

		// 使用刚指定的配置项和数据显示图表。
		myChart.setOption(option);

	    $(document).ready(function(){
	        <?php if($is_syn_theme_shop == '1'): ?>
	            syn_theme_shop();
	        <?php endif; ?>
	        function syn_theme_shop()
	        {
	            layer_loading('商城初始化');
	            // 确定
	            $.ajax({
	                type : 'get',
	                url : "<?php echo url('Shop/ajax_syn_theme_shop'); ?>",
	                data : {_ajax:1},
	                dataType : 'json',
	                success : function(res){
	                    layer.closeAll();
	                    if(res.code == 1){
	                        layer.msg(res.msg, {icon: 1, time: 1000});
	                    }else{
	                        layer.alert(res.msg, {icon: 2, title:false}, function(){
	                            window.location.reload();
	                        });
	                    }
	                },
	                error: function(e){
	                    layer.closeAll();
	                    layer.alert(ey_unknown_error, {icon: 2, title:false}, function(){
	                        window.location.reload();
	                    });
	                }
	            })
	        }
	    });
	</script>
	<br/>
<div id="goTop">
    <a href="JavaScript:void(0);" id="btntop">
        <i class="fa fa-angle-up"></i>
    </a>
    <a href="JavaScript:void(0);" id="btnbottom">
        <i class="fa fa-angle-down"></i>
    </a>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#think_page_trace_open').css('z-index', 99999);
    });
</script>
</body>
</html>
</body>