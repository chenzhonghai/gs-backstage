<?php
/**
 * 易优CMS
 * ============================================================================
 * 版权所有 2016-2028 海南赞赞网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.eyoucms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小虎哥 <1105415366@qq.com>
 * Date: 2018-06-28
 */

namespace weapp\Collection\controller;

use think\Page;
use think\Db;
use app\common\controller\Weapp;

/**
 * 插件的控制器
 */
class Collection extends Weapp
{
    /**
     * 插件基本信息
     */
    private $weappInfo;

    /**
     * 构造方法
     */
    public function __construct(){
        parent::__construct();

        /*插件基本信息*/
        $this->weappInfo = $this->getWeappInfo();
        $this->assign('weappInfo', $this->weappInfo);
        /*--end*/
    }

    /**
     * 插件安装的前置操作
     * @return [type] [description]
     */
    public function beforeInstall()
    {
        Db::name('users_menu')->where(['mca'=>'user/Collection/index'])->delete();
    }

    /**
     * 插件安装的后置操作
     * @return [type] [description]
     */
    public function afterInstall()
    {
        $result = Db::name('users_menu')->where(['mca'=>'user/Collection/index'])->find();
        if (empty($result)) {
            $data = [
                'title' => '我的收藏',
                'mca'   => 'user/Collection/index',
                'sort_order'    => 100,
                'status'    => 1,
                'lang'  => $this->admin_lang,
                'add_time'  => getTime(),
            ];
            Db::name('users_menu')->save($data);
        }
        
        // 系统默认是自动调用，这里在安装完插件之后，更改为手工调用
        $savedata = [
            'tag_weapp' => 2,
            'update_time'   => getTime(),
        ];
        Db::name('weapp')->where(['code'=>'Collection'])->update($savedata);

        // 拷贝模板文件
        $this->recurse_copy(WEAPP_PATH.'Collection'.DS.'backup'.DS.'tpl', rtrim(ROOT_PATH, DS));
    }

    /**
     * 插件卸载的后置操作
     * @return [type] [description]
     */
    public function afterUninstall()
    {
        Db::name('users_menu')->where(['mca'=>'user/Collection/index'])->delete();
    }

    /**
     * 插件启用的后置操作（可无）
     * @return [type] [description]
     */
    public function afterEnable()
    {
        Db::name('users_menu')->where(['mca'=>'user/Collection/index'])->update([
            'status'    => 1,
            'update_time'   => getTime(),
        ]);
    }

    /**
     * 插件禁用的后置操作（可无）
     * @return [type] [description]
     */
    public function afterDisable()
    {
        Db::name('users_menu')->where(['mca'=>'user/Collection/index'])->update([
            'status'    => 0,
            'update_time'   => getTime(),
        ]);
    }

    /**
     * 系统内置钩子show方法（没用到这个方法，建议删掉）
     * 用于在前台模板显示片段的html代码，比如：QQ客服、对联广告等
     *
     * @param  mixed  $params 传入的参数
     */
    public function show($params = null){
        echo $this->fetch('show');
    }

    /**
     * 插件后台管理 - 列表
     */
    public function index()
    {
        return $this->fetch('index');
    }

    /**
     * 自定义函数递归的复制带有多级子目录的目录
     * 递归复制文件夹
     *
     * @param string $src 原目录
     * @param string $dst 复制到的目录
     * @return string
     */                        
    //参数说明：            
    //自定义函数递归的复制带有多级子目录的目录
    private function recurse_copy($src, $dst)
    {
        $planPath_pc = "template/".TPL_THEME."pc/";
        $planPath_m = "template/".TPL_THEME."mobile/";
        $dir = opendir($src);

        /*pc和mobile目录存在的情况下，才拷贝会员模板到相应的pc或mobile里*/
        $dst_tmp = str_replace('\\', '/', $dst);
        $dst_tmp = rtrim($dst_tmp, '/').'/';
        if (stristr($dst_tmp, $planPath_pc) && file_exists($planPath_pc)) {
            tp_mkdir($dst);
        } else if (stristr($dst_tmp, $planPath_m) && file_exists($planPath_m)) {
            tp_mkdir($dst);
        }
        /*--end*/

        while (false !== $file = readdir($dir)) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src . '/' . $file)) {
                    $needle = '/template/'.TPL_THEME;
                    $needle = rtrim($needle, '/');
                    $dstfile = $dst . '/' . $file;
                    if (!stristr($dstfile, $needle)) {
                        $dstfile = str_replace('/template', $needle, $dstfile);
                    }
                    $this->recurse_copy($src . '/' . $file, $dstfile);
                }
                else {
                    if (file_exists($src . DIRECTORY_SEPARATOR . $file)) {
                        /*pc和mobile目录存在的情况下，才拷贝会员模板到相应的pc或mobile里*/
                        $rs = true;
                        $src_tmp = str_replace('\\', '/', $src . DIRECTORY_SEPARATOR . $file);
                        if (stristr($src_tmp, $planPath_pc) && !file_exists($planPath_pc)) {
                            continue;
                        } else if (stristr($src_tmp, $planPath_m) && !file_exists($planPath_m)) {
                            continue;
                        }
                        /*--end*/
                        $rs = @copy($src . DIRECTORY_SEPARATOR . $file, $dst . DIRECTORY_SEPARATOR . $file);
                        if($rs) {
                            // @unlink($src . DIRECTORY_SEPARATOR . $file);
                        }
                    }
                }
            }
        }
        closedir($dir);
    }
}