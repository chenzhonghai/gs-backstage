<?php
/**
 * 易优CMS
 * ============================================================================
 * 版权所有 2016-2028 海南赞赞网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.eyoucms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小虎哥 <1105415366@qq.com>
 * Date: 2018-06-28
 */

namespace weapp\Zan\controller;

use think\Page;
use think\Db;
use app\common\controller\Weapp;
use weapp\Zan\model\ZanModel;
use weapp\Zan\logic\ZanLogic;

/**
 * 插件的控制器
 */
class Zan extends Weapp
{
	/**
     * 插件标识
     */
    private $code = 'Zan';

    /**
     * 插件基本信息
     */
    private $weappInfo;

    /**
     * 构造方法
     */
    public function __construct(){
        parent::__construct();

        $this->weappInfo = $this->getWeappInfo();
		$this->weappInfo = Db::name('weapp')->field('id')->where(['code'=>$this->code,'status'=>1])->find();
        $this->assign('weappInfo', $this->weappInfo);
    }
    /**
     * 插件后台管理 - 列表
     */
    public function index()
    {
		$list = array();
        $this->assign('list', $list);

        return $this->fetch('index');
    }
}