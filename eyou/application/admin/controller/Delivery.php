<?php

namespace app\admin\controller;

use think\Page;
use think\Db;
class Delivery extends Base
{
    public function index()
    {
        $sample =  M('product_delivery')->count();
        $Page = new Page($count, config('paginate.list_rows'));
        $limit = $count > config('paginate.list_rows') ? $Page->firstRow.','.$Page->listRows : $count;
        $pro = M('product_delivery')
        ->field('a.*,b.title as titles, b.litpic,c.name as names')
        ->alias('a')
        ->join('archives b','a.product_id=b.aid')
        ->join('product_brand c','c.id = b.brand_id')
        ->limit($limit)
        ->select();
        // dump($pro);die;
        $show = $Page->show();
        $this->assign('page',$show);
        $this->assign('pro',$pro);
        return $this->fetch();
    }
    
    public function edit()
    {
        
         if (IS_POST) {
            $post = input('post.');
            $post['status'] = 1;
            $post['processing_time'] = time();
            $res = M('product_delivery')->where(array('id' => $post['id']))->save($post);
            $ress = M('product_delivery')->where(array('id' => $post['id']))->find();
            if($res)
            {
                $notice['source'] = 9;
                $notice['admin_id'] = 1;
                $notice['users_id'] =  $ress['users_id'];
                $tpl = M('users_notice_tpl') -> where(array('tpl_id' => 9))->find();
                $notice['content_title'] = $tpl['tpl_title'];
                $notice['content'] = '订单询价编号：'.$ress['numbers'];
                $notice['is_read'] = 0;
                $notice['users_read'] = 0;
                $notice['add_time'] = time();
                $notice['users_title'] = '您的询价订单:'.$ress['numbers'].'已回复';
                M('users_notice_tpl_content')->add($notice);
                $this->success("回复成功!",url('Delivery/index'));
            }
         }
        $id = input('id/d');
        $delivery = M('product_delivery')->where(array('id' => $id))->find();
        $this->assign('field',$delivery);
        return $this->fetch();  
    }
}