<?php

namespace app\admin\controller;

use think\Page;
use think\Db;
class Inquiry extends Base
{
   public function index()
   {
        $sample =  M('product_sample')->count();
        $Page = new Page($count, config('paginate.list_rows'));
        $limit = $count > config('paginate.list_rows') ? $Page->firstRow.','.$Page->listRows : $count;
        $inquiry = M('product_inquiry')
       ->field('a.*,b.title as titles, b.litpic')
        ->alias('a')
        ->join('archives b','a.product_id=b.aid')
        ->order('add_time desc')
        ->limit($limit)
        ->select();
        foreach($inquiry as $k => $v)
        {
            if (empty($inquiry['users_id'])) {
               $inquiry[$k]['name'] = '游客';
            }else {
                
               $users = M('users')->where(array('users_id' => $v['users_id']))->find();
               $inquiry[$k]['name'] = $users['usersname'];
            }
        }
        $show = $Page->show();
        $this->assign('page',$show);
        $this->assign('inquiry',$inquiry);
        return $this->fetch();
   }
   
   public function edit()
   {
        return $this->fetch();
   }
}