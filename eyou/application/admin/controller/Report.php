<?php

namespace app\admin\controller;

use think\Page;
use think\Db;
class Report extends Base
{
    public function index()
    {
        $sample =  M('ask_report')->count();
        $Page = new Page($count, config('paginate.list_rows'));
        $limit = $count > config('paginate.list_rows') ? $Page->firstRow.','.$Page->listRows : $count;
        
        
        $pro = M('ask_report')
        ->field('a.*,b.ask_title,c.username')
        ->alias('a')
        ->join('weapp_ask b','a.ask_id=b.ask_id')
        ->join('users c','a.users_id=c.users_id')
        ->select();
        
        foreach ($pro as $k => $v) {
            $pro[$k]['content'] = htmlspecialchars_decode($v['content']);
        }
        // dump($pro);die;
        
        // // ->field('a.*,b.title as titles, b.litpic,c.name as names')
        // // ->alias('a')
        // // ->join('archives b','a.product_id=b.aid')
        // // ->join('product_brand c','c.id = b.brand_id')
        // // ->limit($limit)
        // // ->select();
        // // dump($pro);die;
        $show = $Page->show();
        $this->assign('page',$show);
        $this->assign('pro',$pro);
        return $this->fetch();
    }
    
    public function del()
    {
        $id_arr = input('del_id/a');
        $id_arr = eyIntval($id_arr);
       $ask = M('ask_report')->where(array('id' => array('in',$id_arr)))->delete();
       if($ask)
       {
               $this->success("删除成功!",url('Report/index'));
       }
        // dump($id_arr);die;
    }
}