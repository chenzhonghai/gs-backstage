<?php
/**
 * 易优CMS
 * ============================================================================
 * 版权所有 2016-2028 海南赞赞网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.eyoucms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 陈风任 <491085389@qq.com>
 * Date: 2019-1-7
 */

namespace app\admin\controller;

use think\Page;
use think\Db;

class Brand extends Base
{
    public function index()
    {
       $brand =  M('product_brand')->count();
       $Page = new Page($count, config('paginate.list_rows'));
       $limit = $count > config('paginate.list_rows') ? $Page->firstRow.','.$Page->listRows : $count;
        $brand =  M('product_brand')
        ->limit($limit)
        ->select();
        $this->assign('brand', $brand);
        return $this->fetch();
        // var_dump(111);die;
    }
    
    public function add()
    {
         if (IS_POST) {
            $post = input('post.');
            $post['litpic'] = $post['logo_local'];
            $post['status'] = 1;
            $post['add_time'] = time();
            $product = M('product_brand')->add($post);
            if($product)
            {
                 $this->success("添加成功!", null, $product);
            }
         }
        return $this->fetch();
    }
    
    public function edit()
    {
        
          if (IS_POST) {
            $post = input('post.'); 
            $post['litpic'] = $post['logo_local'];
            // dump($product)
            $product = M('product_brand')->where(array('id' => $post['id']))->save($post);
            // dump($product);die;
            if($product)
            {
                 $this->success("修改成功!", null, $product);
            }
          }
        
        
        $id = input('id/d');
        $brand = M('product_brand')->where(array('id' => $id))->find();
        // dump($brand);die;
        $this->assign('brand',$brand);
        return $this->fetch();
    }
    
    public function del()
    {
        // $post = input('post.');
        $id_arr = input('del_id/a');
        $del = M('product_brand')->where(array('id' => array('in',$id_arr))) ->delete();
        if($del)
        {
            $this->success('删除成功');
        }
        // dump($id_arr);die;
        
    }
}