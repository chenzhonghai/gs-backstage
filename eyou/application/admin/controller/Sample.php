<?php

namespace app\admin\controller;

use think\Page;
use think\Db;
class Sample extends Base
{
    public function index()
    {
        
        $param = input('param.');
        $condition['a.numbers'] = array('LIKE', "%{$param['keywords']}%");
        $sample =  M('product_sample')->count();
        $Page = new Page($count, config('paginate.list_rows'));
        $limit = $count > config('paginate.list_rows') ? $Page->firstRow.','.$Page->listRows : $count;
        $pro = M('product_sample')
        ->field('a.*,b.title as titles, b.litpic,a.status as statuss')
        ->alias('a')
        ->join('archives b','a.product_id=b.aid')
        ->where('a.status != 3')
        ->where($condition)
        ->order('a.status asc,add_time desc')
        ->limit($limit)
        ->select();
        
        // dump($pro);die;
        $show = $Page->show();
        $this->assign('page',$show);
        $this->assign('pro',$pro);
        return $this->fetch();
    }
    
    public function reject()
    {
         if (IS_POST) {
            $post = input('post.');
            // dump($post);die;
            $data['status'] = 2;
            $data['examine_time'] = time();
            // dump
            $res =M('product_sample')->where(array('id' => $post['del_id']))->save($data);
            $ress =M('product_sample')->where(array('id' => $post['del_id']))->find();
            
            // dump($ress);die;
            if($res)
            {
                $notice['source'] = 11;
                $notice['admin_id'] = 1;
                $notice['users_id'] =  $ress['users_id'];
                $tpl = M('users_notice_tpl') -> where(array('tpl_id' => 11))->find();
                $notice['content_title'] = $tpl['tpl_title'];
                $notice['content'] = '您驳回了该订单：'.$ress['numbers'];
                $notice['is_read'] = 0;
                $notice['users_read'] = 0;
                $notice['add_time'] = time();
                $notice['users_title'] = '您申请的样品'.$ress['numbers'].'被驳回了';
                M('users_notice_tpl_content')->add($notice);
                $this->success("驳回成功!");
            }
            // dump($post);die;
         }
    }
    
    public function examine()
    {
         if (IS_POST) {
            $post = input('post.');
            $data['status'] = 1;
            $data['examine_time'] = time();
            $ress =M('product_sample')->where(array('id' => $post['del_id']))->find();
            $res =M('product_sample')->where(array('id' => $post['del_id']))->save($data);
            if($res)
            {
                $notice['source'] = 8;
                $notice['admin_id'] = 1;
                $notice['users_id'] = $ress['users_id'];
                $tpl = M('users_notice_tpl') -> where(array('tpl_id' => 8))->find();
                $notice['content_title'] = $tpl['tpl_title'];
                $notice['content'] = '您审核通过了订单：'.$ress['numbers'];
                $notice['is_read'] = 0;
                $notice['users_read'] = 0;
                $notice['add_time'] = time();
                $notice['users_title'] = '您申请的样品'.$ress['numbers'].'审核通过';
                M('users_notice_tpl_content')->add($notice);
                $this->success("审核成功!");
            }
            // dump($post);die;
         } 
    }
    
    public function edit()
    {
        if(IS_POST)
        {
             $post = input('post.');
             if($post['reject'] == '')
             {
                  $this->error('请填写驳回理由！');
             }
             
             $post['status'] = 2;
            $post['examine_time'] = time();
            // dump
            $ress =M('product_sample')->where(array('id' => $post['id']))->find();
             
            $res = M('product_sample')->where(array('id' => $post['id']))->save($post);
            if($res)
            {
                $notice['source'] = 11;
                $notice['admin_id'] = 1;
                $notice['users_id'] =  $ress['users_id'];
                $tpl = M('users_notice_tpl') -> where(array('tpl_id' => 11))->find();
                $notice['content_title'] = $tpl['tpl_title'];
                $notice['content'] = '您驳回了该订单：'.$ress['numbers'];
                $notice['is_read'] = 0;
                $notice['users_read'] = 0;
                $notice['add_time'] = time();
                $notice['users_title'] = '您申请的样品'.$ress['numbers'].'被驳回了';
                M('users_notice_tpl_content')->add($notice);
                $this->success("驳回成功!",url('sample/index'));
            }
        }
        $post = input('param.');
        $sample = M('product_sample')->where(array('id' => $post['id']))->find();
        $this->assign('sample',$sample);
        return $this->fetch();
    }
    
    
    public function status()
    {
         $post = input('param.');
         $data['status'] = 1;
         $data['reject'] = '';
         $data['examine_time'] = time();
         $ress =M('product_sample')->where(array('id' => $post['id']))->find();
         $res =M('product_sample')->where(array('id' => $post['id']))->save($data);
        if($res)
        {
            $notice['source'] = 8;
            $notice['admin_id'] = 1;
            $notice['users_id'] = $ress['users_id'];
            $tpl = M('users_notice_tpl') -> where(array('tpl_id' => 8))->find();
            $notice['content_title'] = $tpl['tpl_title'];
            $notice['content'] = '您审核通过了订单：'.$ress['numbers'];
            $notice['is_read'] = 0;
            $notice['users_read'] = 0;
            $notice['add_time'] = time();
            $notice['users_title'] = '您申请的样品'.$ress['numbers'].'审核通过';
            M('users_notice_tpl_content')->add($notice);
            $this->success("修改成功!");
        }
    }
    
    
    public function order_mark_status()
    {
         if (IS_POST) {
            $post = input('post.');
            $data['sample_status'] = 3;
            $sample = M('product_sample')->where(array('id' => $post['order_id']))->save($data);
            if($sample)
            {
                  $this->success('完成！');
            }
            // dump($post);die;
         }
    }
    
    /**
     *  订单发货
     */
    public function order_send()
    {
        $order_id = input('param.order_id');
        // dump($order_id);die;
        if ($order_id) {
            // 查询订单信息
            $this->GetOrderData($order_id);
            return $this->fetch('order_send');
        }
    }
    
        /*
     *  查询会员订单数据并加载，无返回
     */
    function GetOrderData($order_id)
    {
        // 获取订单数据
       $OrderData = M('product_sample')->find($order_id);
       $rest = M('sample_express')->where(array('sample_id' => $OrderData['id']))->find();
       $OrderData['freight'] = $rest['freight'];
       $OrderData['numbers1'] = $rest['numbers'];
       
       $address = M('shop_address')->where(array('addr_id' => $OrderData['address_id']))->find();
       
        // DUMP($OrderData);DIE;

//         // 获取会员数据
        // $UsersData = $this->users_db->find($OrderData['users_id']);
//       $invoice = M('shop_invoice')->where(array('order_id' => $OrderData['order_id']))->find();
//       $OrderData['stat'] = $invoice['status'];
//         // 当前单条订单信息的会员ID，存入session，用于添加订单操作表
//         session('OrderUsersId',$OrderData['users_id']);

//         // 获取订单详细表数据
//         $DetailsData = $this->shop_order_details_db->where('order_id',$OrderData['order_id'])->select();

//         // 获取订单状态，后台专用
//         $admin_order_status_arr = Config::get('global.admin_order_status_arr');

//         // 获取订单方式名称
//         $pay_method_arr = Config::get('global.pay_method_arr');

//         // 处理订单主表的地址数据处理，显示中文名字
            $OrderData['country']  = '中国';
            $OrderData['province'] = get_province_name($address['province']);
            $OrderData['city']     = get_city_name($address['city']);
            $OrderData['district'] = get_area_name($address['district']);
            $OrderData['address_name'] = $address['address'];
//         $array_new = get_archives_data($DetailsData,'product_id');
//         $OrderData['prom_type_virtual'] = false;
//         // 处理订单详细表数据处理
//         foreach ($DetailsData as $key => $value) {
//             if ($value['prom_type'] == 1) {
//                 $OrderData['prom_type_virtual'] = true;
//             }
//             // 产品属性处理
//             $ValueData = unserialize($value['data']);
//             // 规制值
//             $spec_value = !empty($ValueData['spec_value']) ? htmlspecialchars_decode($ValueData['spec_value']) : '';
//             $spec_value = htmlspecialchars_decode($spec_value);
//             // 旧参数
//             $attr_value = !empty($ValueData['attr_value']) ? htmlspecialchars_decode($ValueData['attr_value']) : '';
//             $attr_value = htmlspecialchars_decode($attr_value);
//             // 新参数
//             $attr_value_new = !empty($ValueData['attr_value_new']) ? htmlspecialchars_decode($ValueData['attr_value_new']) : '';
//             $attr_value_new = htmlspecialchars_decode($attr_value_new);
//             // 优先显示新参数
//             $attr_value = !empty($attr_value_new) ? $attr_value_new : $attr_value;
//             $DetailsData[$key]['data'] = $spec_value . $attr_value;

//             // 产品内页地址
//             $DetailsData[$key]['arcurl'] = get_arcurl($array_new[$value['product_id']]);
            
//             // 小计
//             $DetailsData[$key]['subtotal'] = $value['product_price'] * $value['num'];
            
//             $DetailsData[$key]['litpic'] = handle_subdir_pic($DetailsData[$key]['litpic']); // 支持子目录
//         }

//         // 订单类型
//         if (empty($OrderData['prom_type'])) {
//             $OrderData['prom_type_name'] = '普通订单';
//         }else{
//             $OrderData['prom_type_name'] = '虚拟订单';
//         }

//         // 移动端查询物流链接
//         $MobileExpressUrl = "//m.kuaidi100.com/index_all.html?type=".$OrderData['express_code']."&postid=".$OrderData['express_order'];


// // dump($OrderData);die;
//         // 加载数据
//         $this->assign('invoice',$invoice);
//         $this->assign('MobileExpressUrl', $MobileExpressUrl);
        $this->assign('OrderData', $OrderData);
//         $this->assign('DetailsData', $DetailsData);
//         $this->assign('UsersData', $UsersData);
//         $this->assign('admin_order_status_arr',$admin_order_status_arr);;
//         $this->assign('pay_method_arr',$pay_method_arr);
    }
    
    
    public function order_send_operating()
    {
        $post = input('param.');
        $data['numbers'] = $post['numbers1'];
        $data['freight'] = $post['freight'];
        $data['express'] = $post['express_name'];
        $data['sample_id'] = $post['order_id'];
        $data['add_time'] = time();
       $rest = M('sample_express')->add($data);
       if($rest)
       {
           $res['sample_status'] = 2;
          $sample = M('product_sample')->where(array('id' =>$post['order_id']))->save($res);
          if($sample)
          {
            $this->success('发货成功！');
          }
       }
        // dump($post);die;
    }
    
}

?>