<?php
/**
 * 易优CMS
 * ============================================================================
 * 版权所有 2016-2028 海南赞赞网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.eyoucms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 陈风任 <491085389@qq.com>
 * Date: 2019-3-20
 */

namespace app\user\controller;

use think\Db;
use think\Config;
use think\Page;
use think\Cookie;
use app\common\logic\ShopCommonLogic;

class Shop extends Base
{
    // 初始化
    public function _initialize() {
        parent::_initialize();
        $this->users_db              = Db::name('users');               // 会员数据表
        $this->users_money_db        = Db::name('users_money');         // 会员金额明细表

        $this->shop_cart_db          = Db::name('shop_cart');           // 购物车表
        $this->shop_order_db         = Db::name('shop_order');          // 订单主表
        $this->shop_order_details_db = Db::name('shop_order_details');  // 订单明细表
        $this->shop_order_service_db = Db::name('shop_order_service');  // 订单售后服务表
        $this->shop_address_db       = Db::name('shop_address');        // 收货地址表

        $this->archives_db           = Db::name('archives');            // 产品表
        $this->product_attr_db       = Db::name('product_attr');        // 产品属性表
        $this->product_attribute_db  = Db::name('product_attribute');   // 产品属性标题表

        $this->region_db             = Db::name('region');                 // 三级联动地址总表
        $this->shipping_template_db  = Db::name('shop_shipping_template'); // 运费模板表

        $this->shop_model = model('Shop');  // 商城模型
        $this->shop_common = new ShopCommonLogic(); // common商城业务层，前后台共用

        // 订单中心是否开启
        $redirect_url = '';
        $shop_open = getUsersConfigData('shop.shop_open');
        $web_users_switch = tpCache('web.web_users_switch');
        if (empty($shop_open)) { 
            // 订单功能关闭，立马跳到会员中心
            $redirect_url = url('user/Users/index');
            $msg = '商城中心尚未开启！';
        } else if (empty($web_users_switch)) { 
            // 前台会员中心已关闭，跳到首页dum
            $redirect_url = ROOT_DIR.'/';
            $msg = '会员中心尚未开启！';
        }
        if (!empty($redirect_url)) {
            Db::name('users_menu')->where([
                    'mca'   => 'user/Shop/shop_centre',
                    'lang'  => $this->home_lang,
                ])->update([
                    'status'    => 0,
                    'update_time' => getTime(),
                ]);
            $this->error($msg, $redirect_url);
            exit;
        }
        // --end
    }

    // 购物车列表
    public function shop_cart_list()
    {
        // 数据由标签调取生成
        return $this->fetch('users/shop_cart_list');
    }

    // 订单管理列表，订单中心
    public function shop_centre()
    {
        $result = [];
        // 应用搜索条件
        $keywords      = input('param.keywords/s');
        // 订单状态搜索
        $select_status = input('param.select_status');
        // 查询订单是否为空
        $result['data'] = $this->shop_model->GetOrderIsEmpty($this->users_id,$keywords,$select_status);
        // dump($result);
        // dump($this->users_id);
        // dump($keywords);
        // dump($select_status);die;
        // 是否移动端，1表示手机端，0表示PC端
        $result['IsMobile'] = isMobile() ? 1 : 0;
        // 菜单名称
        $result['title'] = Db::name('users_menu')->where([
                'mca'   => 'user/Shop/shop_centre',
                'lang'  => $this->home_lang,
            ])->getField('title');
        // 加载数据
        $eyou = [
            'field' => $result,
        ];
        
        // dump($result);die;
        $data['status'] = 1;
        $data['mag'] = '';
        $data['res'] = $eyou;
        echo json_encode($data,JSON_UNESCAPED_UNICODE);
        exit();
    }

    // 订单数据详情
    public function shop_order_details()
    {
        if (IS_GET) {
           $get = input('get.');
        //   dump($get);die;
           $rest = $this->shop_order_details_db
           ->field('a.*,b.litpic')
           ->alias('a')
           ->join('__ARCHIVES__ b', 'a.product_id = b.aid', 'LEFT')
           ->where(array('a.order_id' => $get['order_id']))
           ->select();
           
        //   dump($rest);die;
           
          $shop_open = M('shop_order')   
           ->field('b.order_code,b.	consignee,b.province,b.country,b.city,b.district,b.	address,b.mobile,b.shipping_fee,b.order_total_amount,b.order_amount,b.order_total_num,b.order_status,b.payment_method,b.add_time,b.express_order,b.express_name,b.express_time,b.confirm_time,b.pay_time,b.pay_name,b.coupon_price')
           ->alias('b')
          ->where(array('b.order_id' => $get['order_id']))
           ->find();
            $shop_open['province'] = get_province_name($shop_open['province']);
            $shop_open['city']     = get_city_name($shop_open['city']);
            $shop_open['district'] = get_area_name($shop_open['district']);
           
            $order['invoice'] = M('shop_invoice')->where(array('order_id' => $get['order_id']))->find();
            if($shop_open['pay_name'] == 'voucher')
            {
               $vouche = M('pay_voucher')->where(array('order_code' => $shop_open['order_code']))->find();
               $shop_open['vouche'] = $vouche['voucher'];
               $shop_open['voucher_status'] = 1;
            }else
            {
                 $shop_open['voucher_status'] = 0;
            }
            
            // dump($order['invoice']);die;
            $order['shop'] = $shop_open;
            
            
            $comment = M('shop_order_comment')
            ->field('a.*,b.title,b.litpic')
            ->alias('a')
            ->join('__ARCHIVES__ b', 'a.product_id = b.aid', 'LEFT')
            ->where(array('a.order_id' => $get['order_id'],'a.users_id' => $this->users_id))
            ->select();
            // dump($comment);die;
            if(!empty($comment))
            {
                foreach ($comment as $k => $v)
                {
                    $order['comment'][$k]['total_score'] = GetScoreArray($v['total_score']);
                    $order['comment'][$k]['upload_img'] = !empty($v['upload_img']) ? explode(',', $v['upload_img']) : '';
                    $order['comment'][$k]['order_total_score'] = Config::get('global.order_total_score')[$v['total_score']];
                    $order['comment'][$k]['total_score'] = GetScoreArray($v['total_score']);
                    $order['comment'][$k]['score'] = $v['total_score'];
                    $order['comment'][$k]['content'] = !empty($v['content']) ? htmlspecialchars_decode($v['content']) : ''; 
                    $order['comment'][$k]['title'] = $v['title'];
                    $order['comment'][$k]['litpic'] = $v['litpic'];
                }
            
            }else
            {
                 $order['comment'] = [];
            }
        
        // dump($rest);die;
           
           foreach($rest as $k => $v)
           {
               $datae = unserialize($v['data']);
               $details =  M('shop_order_service')->where(array('details_id' => $v['details_id']))->find();
            //   dump($details);
               $rest[$k]['service_status'] = $details['status'];
               $rest[$k]['data'] = htmlspecialchars_decode($datae['attr_value_new']);
               $rest[$k]['status'] = 0;
           }
           
        //   dump($rest);die;
            $order['list'] = $rest;
            $data['status'] = 1;
            $data['mag'] = '';
            $data['res'] = $order;
            // dump($data);die;
            echo json_encode($data,JSON_UNESCAPED_UNICODE);
            exit();
        }else{
            $this->error('非法访问！');
        }
    }

    // 订单提交
    public function shop_under_order($error = true)
    {
        if (empty($error)) $this->error('您的购物车还没有商品！');
        // 获取当前页面URL，存入session，若操作添加地址后返回当前页面
        session($this->users_id.'_EyouShopOrderUrl', $this->request->url(true));
        // 数据由标签调取生成
        return $this->fetch('users/shop_under_order');
    }

    // 购物车库存检测
    public function cart_stock_detection()
    {
        if (IS_AJAX_POST) {
            // 购物车查询条件
            $CartWhere = [
                'a.users_id' => $this->users_id,
                'a.lang'     => $this->home_lang,
                'a.selected' => 1,
            ];
            $list = $this->shop_cart_db->field('a.product_num, b.stock_count, c.spec_value_id, c.spec_stock')
                ->alias('a')
                ->join('__ARCHIVES__ b', 'a.product_id = b.aid', 'LEFT')
                ->join('__PRODUCT_SPEC_VALUE__ c', 'a.spec_value_id = c.spec_value_id and a.product_id = c.aid', 'LEFT')
                ->where($CartWhere)
                ->order('a.add_time desc')
                ->select();
                // dump($list);die;
            if (empty($list)) $this->error('请选择要购买的商品！');
            
            $ExceedingStock = 0;
            // 处理商品库存检测
            foreach ($list as $key => $value) {
                // 购物车商品存在规格并且库存不为空，则覆盖商品原来的库存
                if (!empty($value['spec_value_id'])) {
                    $value['stock_count'] = $value['spec_stock'];
                }
                // 检测是否有超出库存的产品
                if (empty($value['product_num']) || $value['product_num'] > $value['stock_count']) {
                    $ExceedingStock = 1;
                    break;
                }
                // dump($value['stock_count']);die;
                // 若库存为空则清除这条数据
                if ($value['stock_count'] == 0) {
                    // dump(11);die;
                    unset($list[$key]);
                    continue;
                }
            }
            
            if($ExceedingStock == 1)
            {
                $datas['status'] = -1;
                $datas['msg'] = '库存不足请等待商家补货';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;   
            }else
            {
                $datas['status'] = 1;
                $datas['msg'] = '检测完成';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit; 
            }
            // dump($list);die;
            // $datas['status'] = 1;
            // $datas['msg'] = '检测完成';
            // $datas['res'] = '';
            // echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            // exit; 
            
            // $this->success('检测完成', null, $ExceedingStock);
        }
    }

    // 收货地址管理列表
    public function shop_address_list()
    {
        // 获取当前页面URL，存入session，若操作添加地址后返回当前页面
        session($this->users_id.'_EyouShopOrderUrl', $this->request->url(true));
        // 指定返回上一级URL
        $gourl = input('param.gourl/s');
        $gourl = urldecode($gourl);
        $this->assign('gourl', $gourl);
        // 数据由标签调取生成
        return $this->fetch('users/shop_address_list');
    }

    // 取消订单
    public function shop_order_cancel()
    {
        if (IS_POST) {
            $order_id = input('param.order_id');
            // DUMP($this->users);DIE;
            if (!empty($order_id)) {
                // 更新条件
                $Where = [
                    'order_id' => $order_id,
                    'users_id' => $this->users_id,
                    'lang'     => $this->home_lang,
                ];
                // 更新数据
                $Data  = [
                    'order_status' => '-1',
                    'update_time'  => getTime(),
                ];
                // 更新订单主表
                $return = $this->shop_order_db->where($Where)->update($Data);
                if (!empty($return)) {
                    // 订单取消，还原单内产品数量 
                    model('ProductSpecValue')->SaveProducSpecValueStock($order_id, $this->users_id);
                    
                    //订单取消，还原用户积分
                   $order = M('shop_order')->where(array('order_id' => $order_id))->find();
                //   dump($order['integral']);die;
                   
                        $confg = M('users_config')->select();
                      foreach ($confg as $k => $v)
                      {
                          if($v['name'] == 'score_signin_score')
                          {
                              $score_signin = $v['value'];
                          }
                          if($v['name'] == 'score_signin_score2')
                          {
                            //   dump($v);die;
                              $score_signin_score2 = $v['value'];
                          }
                      }
                        $score_signin = (int)$score_signin/(int)$score_signin_score2;
                               
                    if(!empty($order['integral']))
                    {
                        $users['scores'] = $this->users['scores'] + $order['integral']*$score_signin;
                        // dump($users);die;
                        M('users')->where(array('users_id' => $this->users['users_id']))->save($users);
                        $lists['symbol'] = '+';
                        $lists['type'] = 7;
                        $lists['users_id'] = $this->users['users_id'];
                        $lists['score'] = $order['integral']*$score_signin;
                        $lists['devote'] = $order['integral']*$score_signin;
                        $lists['add_time'] = time();
                        $lists['info'] = '取消订单';
                        $lists['users_score'] =  $users['scores'];
                        $ilst = M('users_score')->add($lists);
                        
                    }
                    if(!empty($order['coupon_id']))
                    { 
                        $coupon['use_status'] = 0;
                        M('shop_coupon_use')->where(array('coupon_id' => $order['coupon_id'],'users_id' => $this->users_id))->save($coupon);
                    }
                    
                    // 添加订单操作记录
                    AddOrderAction($order_id,$this->users_id,'0','0','0','0','订单取消！','会员关闭订单！');
                    
                $datas['status'] = 1;
                $datas['msg'] = '订单已取消';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;
                    // $this->success('订单已取消！');
                }else{
                    $datas['status'] = -1;
                    $datas['msg'] = '操作失败';
                    $datas['res'] = '';
                    echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                    exit;
                    // $this->error('操作失败！');
                }
            }
        }
    }

    // 立即购买
    public function shop_buy_now()
    {
        if (IS_AJAX_POST) {
            $param = input('param.');
            $param['aid'] = intval($param['aid']);
            $param['num'] = intval($param['num']);

            // 商品是否已售罄
            $this->IsSoldOut($param);

            // 数量不可为空
            if (empty($param['num']) || 0 > $param['num']) {
                $this->error('请选择数量！');
            }
            // 查询条件
            $archives_where = [
                'arcrank' => array('egt','0'), //带审核稿件不查询
                'aid'     => $param['aid'],
                'lang'    => $this->home_lang,
            ];
            $count = $this->archives_db->where($archives_where)->count();
            // 跳转下单页
            if (!empty($count)) {
                // 对ID和订单号加密，拼装url路径
                $querydata = [
                    'aid'         => $param['aid'],
                    'product_num' => $param['num'],
                ];

                /*若开启多规格则执行*/
                if (!empty($this->usersConfig['shop_open_spec']) && !empty($param['spec_value_id'])) {
                    $querydata['spec_value_id'] = $param['spec_value_id'];
                }
                /* END */

                /*特定场景专用*/
                $opencodetype = config('global.opencodetype');
                if (1 == $opencodetype) {
                    if (!empty($param['mini_id']) && intval($param['mini_id']) > 0) {
                        $querydata['mini_id'] = intval($param['mini_id']);
                    }
                }
                /*end*/

                // /*修复1.4.2漏洞 -- 加密防止利用序列化注入SQL*/
                // $querystr = '';
                // foreach($querydata as $_qk => $_qv)
                // {
                //     $querystr .= $querystr ? "&$_qk=$_qv" : "$_qk=$_qv";
                // }
                // $querystr = str_replace('=', '', mchStrCode($querystr));
                // $auth_code = tpCache('system.system_auth_code');
                // $hash = md5("payment".$querystr.$auth_code);
                // /*end*/
                // $url = urldecode(url('user/Shop/shop_under_order', ['querystr'=>$querystr,'hash'=>$hash]));

                // 先 json_encode 后 md5 加密信息
                $querystr = md5(json_encode($querydata));

                // 存入 cookie
                cookie($querystr, $querydata);

                // 跳转链接
                $url = urldecode(url('user/Shop/shop_under_order', ['querystr'=>$querystr]));
                $this->success('立即购买！',$url);
            }else{
                $this->error('该商品不存在或已下架！');
            }
        }else {
            $this->error('非法访问！');
        }
    }

    // 添加购物车数据
    public function shop_add_cart()
    {
        if (IS_POST) {
            
            $param = input('param.');
            $num = $param['num'];
            // dump($param);die;
            // 商品是否已售罄
            $this->IsSoldOut($param);
            
            if(!empty($param['specifications_id']))
            {
               $ress = M('specifications')->where(array('id' => $param['specifications_id']))->find();
               $param['num'] =  $param['num']*$ress['nume_num'];
               $param['weight'] = $num * $ress['weight'];
            }
            // dump($param);die;

            // 数量不可为空
            if (empty($param['num']) || 0 > $param['num']) {
                $datas['status'] = -1;
                $datas['msg'] = '请选择数量！';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit();
                // $this->error('请选择数量！');
            }
            // 查询条件
            $archives_where = [
                'arcrank' => array('egt','0'), //带审核稿件不查询
                'aid'     => $param['aid'],
                'lang'    => $this->home_lang,
            ];
            $count = $this->archives_db->where($archives_where)->count();
            // 加入购物车处理
            if (!empty($count)) {
                // 查询条件
                $cart_where = [
                    'users_id'   => $this->users_id,
                    'product_id' => $param['aid'],
                    'lang'       => $this->home_lang,
                ];

                /*若开启多规格则执行*/
                if (!empty($this->usersConfig['shop_open_spec']) && !empty($param['spec_value_id'])) {
                    $cart_where['spec_value_id'] = $param['spec_value_id'];
                }
                /* END */

                $cartInfo = $this->shop_cart_db->field('product_num')->where($cart_where)->find();
                // dump($param);die;
                if (!empty($cartInfo)) {
                    // 购物车内已有相同产品，进行数量更新。
                    $data['product_num'] = $param['num'] + intval($cartInfo['product_num']); //与购物车数量进行叠加
                    $data['update_time'] = getTime();
                    $cart_id = $this->shop_cart_db->where($cart_where)->update($data);
                }else{
                    // 购物车内还未有相同产品，进行添加。
                    $data['users_id']    = $this->users_id;
                    $data['product_id']  = $param['aid'];
                    $data['product_num'] = $param['num'];
                    $data['spec_value_id'] = $param['spec_value_id'];
                    $data['weight']      = $param['weight'];
                    // dump($param);die;
                    // $data['specifications_id'] = $param['specifications_id'];
                    $data['add_time']    = getTime();
                    $cart_id = $this->shop_cart_db->add($data);
                }
                
                // dump($data);die;
                if (!empty($cart_id)) {
                    $datas['status'] = 1;
                    $datas['msg'] = '加入购物车成功';
                    echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                    exit(); 
                }else{
                    $datas['status'] = -1;
                    $datas['msg'] = '加入购物车失败';
                    echo json_encode($data,JSON_UNESCAPED_UNICODE);
                    exit();
                }
            }else{
                    $data['status'] = -1;
                    $data['msg'] = '该商品不存在或已下架';
                    echo json_encode($data,JSON_UNESCAPED_UNICODE);
                    exit();
                // $this->error('该商品不存在或已下架！');
            }
        }else {
            $data['status'] = -1;
            $data['msg'] = '非法访问！';
            echo json_encode($data,JSON_UNESCAPED_UNICODE);
            exit();
            // $this->error('非法访问！');
        }
    }

    // 统一修改购物车数量
    // symbol 加或减数量或直接修改数量
    public function cart_unified_algorithm(){
        if (IS_POST) {
            $aid    = input('post.aid');
            $symbol = input('post.symbol');
            $num    = input('post.num');
            $cart_id = input('post.cart_id');
            
            if($num <1)
            {
                $datas['status'] = -1;
                $datas['msg'] = '商品数量最少为1';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;
            }
            // dump($num);die;

            // 查询条件
            $archives_where = [
                'arcrank' => array('egt','0'),
                'aid'     => $aid,
                'lang'    => $this->home_lang,
            ];
            $archives_count = $this->archives_db->where($archives_where)->count();
            if (!empty($archives_count)) {
                // 查询条件
                $cart_where = [
                    'users_id'    => $this->users_id,
                    'product_id'  => $aid,
                    'lang'        => $this->home_lang,
                    'cart_id'  =>    $cart_id,
                ];
                // 判断追加查询条件，当减数量时，商品数量最少为1
                if ('-' == $symbol) $cart_where['product_num'] = array('gt','1');
                $product_num = $this->shop_cart_db->where($cart_where)->getField('product_num');
                // 处理购物车产品数量
                if (!empty($product_num)) {
                    // 更新数组
                    if ('+' == $symbol) {
                        $data['product_num'] = $product_num + 1;
                    }else if ('-' == $symbol) {
                        $data['product_num'] = $product_num - 1;
                    }else if ('change' == $symbol) {
                        $data['product_num'] = $num;
                    }
                    
                    $data['update_time'] = getTime();
                    // 更新数据
                    // dump($data);die;
                    $cart_id = $this->shop_cart_db->where($cart_where)->update($data);
                    // 计算金额数量
                    $CaerWhere = [
                        'a.users_id' => $this->users_id,
                        'a.lang'     => $this->home_lang,
                        'a.selected' => 1,
                    ];
                    
                    $CartData = $this->shop_cart_db
                        ->field('a.product_num, b.users_price, c.spec_price')
                        ->alias('a') 
                        ->join('__ARCHIVES__ b', 'a.product_id = b.aid', 'LEFT')
                        ->join('__PRODUCT_SPEC_VALUE__ c', 'a.spec_value_id = c.spec_value_id and a.product_id = c.aid', 'LEFT')
                        ->where($CaerWhere)
                        ->select();
                    $level_discount = $this->users['level_discount'];
                    $discount_price = $level_discount / 100;
                    $spec_price = $users_price = $num_new = 0;
                    
                    // dump($CartData);DIE;
                    foreach($CartData as $k => $v)
                    {
                        $rest = $this->num($aid,$v['product_num']);
                        $CartData[$k]['spec_price'] =  sprintf("%.2f",$rest['spec_price']*$discount_price);
                        // dump($CartData);die;
                        $product_id  = sprintf("%.2f", $rest['spec_price']*$discount_price);
                        // dump($v['product_num']);
                        // dump($product_id);
                        $CartData['total_price'] += sprintf("%.2f",$v['product_num'] * $product_id);
                        $CartData['num']   += $v['product_num'];
                    }
                    // dump($CartData);die;
                    // dump($level_discount);
                    // dump($discount_price);
                    // dump($spec_price);die;
                    // foreach ($CartData as $key => $value) {
                    //     if (!empty($value['spec_price'])) {
                    //         if (!empty($level_discount)) $value['spec_price'] = $value['spec_price'] * $discount_price;
                    //         $spec_price += $value['product_num'] * $value['spec_price'];
                    //     } else {
                    //         if (!empty($level_discount)) $value['users_price'] = $value['users_price'] * $discount_price;
                    //         $users_price += $value['product_num'] * $value['users_price'];
                    //     }
                    //     $num_new += $value['product_num'];
                    // }
                    // $CartData['num']   = $num_new;
                    // $CartData['price'] = sprintf("%.2f", $spec_price + $users_price);
                    // if (empty($CartData['num']) && empty($CartData['price'])) {
                    //     $CartData['num']   = '0';
                    //     $CartData['price'] = '0.00';
                    // // }
                    // dump($CartData['total_price']);die;
                    if (!empty($cart_id)) {
                     $datas['status'] = 1;
                     $datas['msg'] = '操作成功';
                     $datas['res'] = ['NumberVal'=>$CartData['num'], 'AmountVal'=>$CartData['total_price']];
                     echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                     exit;
                        // $this->success('操作成功！', null, ['NumberVal'=>$CartData['num'], 'AmountVal'=>$CartData['total_price']]);
                    }
                } else {
                     $datas['status'] = -1;
                     $datas['msg'] = '商品数量最少为1';
                     $datas['res'] = '';
                     echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                     exit;
                    // $this->error('商品数量最少为1', null, ['error'=>'0']);
                }
            } else {
                 $datas['status'] = -1;
                 $datas['msg'] = '该商品不存在或已下架';
                 $datas['res'] = '';
                 echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                 exit;
                // $this->error('该商品不存在或已下架！');
            }
        }
    }

    // 删除购物车内的产品
    public function cart_del()
    {
        if (IS_POST) {
            $cart_id = input('post.cart_id');
            if (!empty($cart_id)) {
                // 删除条件
                $cart_where = [
                    'cart_id'  => $cart_id,
                    'users_id' => $this->users_id,
                    'lang'     => $this->home_lang,
                ];
                // 删除数据
                $return = $this->shop_cart_db->where($cart_where)->delete();
            }
            if (!empty($return)) {
                /*计算购物车选中商品的总数总额*/
                $CaerWhere = [
                    'a.users_id' => $this->users_id,
                    'a.lang'     => $this->home_lang,
                    'a.selected' => 1
                ];
                $CartData = $this->shop_cart_db
                    ->field('a.product_num, b.users_price, c.spec_price')
                    ->alias('a') 
                    ->join('__ARCHIVES__ b', 'a.product_id = b.aid', 'LEFT')
                    ->join('__PRODUCT_SPEC_VALUE__ c', 'a.spec_value_id = c.spec_value_id and a.product_id = c.aid', 'LEFT')
                    ->where($CaerWhere)
                    ->select();

                $level_discount = $this->users['level_discount'];
                $discount_price = $level_discount / 100;
                $spec_price = $users_price = $num_new = 0;
                foreach ($CartData as $key => $value) {
                    if (!empty($value['spec_price'])) {
                        if (!empty($level_discount)) $value['spec_price'] = $value['spec_price'] * $discount_price;
                        $spec_price += $value['product_num'] * $value['spec_price'];
                    } else {
                        if (!empty($level_discount)) $value['users_price'] = $value['users_price'] * $discount_price;
                        $users_price += $value['product_num'] * $value['users_price'];
                    }
                    $num_new += $value['product_num'];
                }
                $CartData['num']   = empty($num_new) ? 0 : $num_new;
                $CartData['price'] = sprintf("%.2f", $spec_price + $users_price);
                $CartData['price'] = empty($CartData['price']) ? '0.00' : $CartData['price'];
                /*END*/

                /*购物车是否还存在商品*/
                $CartCount = $this->shop_cart_db->where([
                        'users_id' => $this->users_id,
                        'lang'     => $this->home_lang,
                    ])->count();
                /*end*/

                $data = [
                    'NumberVal' => $CartData['num'],
                    'AmountVal' => $CartData['price'],
                    'CartCount' => $CartCount,
                ];
                $data['status'] = 1;
                $data['msg'] = '删除成功';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit(); 
               // $this->success('操作成功！', null, $data);
            } else {
                $data['status'] = 1;
                $data['msg'] = '删除失败';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit(); 
                //$this->error('删除失败！');
            }
        }
    }

    // 移入收藏
    public function move_to_collection()
    {
        if (IS_AJAX_POST) {
            $cart_id = input('post.cart_id');
            if (!empty($cart_id)) {
                // 删除条件
                $cart_where = [
                    'cart_id'  => $cart_id,
                    'users_id' => $this->users_id,
                    'lang'     => $this->home_lang,
                ];

                /*加入收藏*/
                $aid = $this->shop_cart_db->where($cart_where)->value('product_id');
                $row = Db::name('users_collection')->where([
                    'users_id'  => $this->users_id,
                    'aid'   => $aid,
                ])->find();
                if (empty($row)) {
                    $archivesInfo = Db::name('archives')->field('aid,title,litpic,channel,typeid')->find($aid);
                    if (!empty($archivesInfo)) {
                        Db::name('users_collection')->add([
                            'users_id'  => $this->users_id,
                            'title' => $archivesInfo['title'],
                            'aid' => $aid,
                            'litpic' => $archivesInfo['litpic'],
                            'channel' => $archivesInfo['channel'],
                            'typeid' => $archivesInfo['typeid'],
                            'lang'  => $this->home_lang,
                            'add_time'  => getTime(),
                            'update_time' => getTime(),
                        ]);
                    }
                }
                /*end*/

                // 删除数据
                $return = $this->shop_cart_db->where($cart_where)->delete();
            }
            if (!empty($return)) {
                /*计算购物车总数总额*/
                $CaerWhere = [
                    'a.users_id' => $this->users_id,
                    'a.lang'     => $this->home_lang,
                    'a.selected' => 1
                ];
                $CartData = $this->shop_cart_db
                    ->field('a.product_num, b.users_price, c.spec_price')
                    ->alias('a') 
                    ->join('__ARCHIVES__ b', 'a.product_id = b.aid', 'LEFT')
                    ->join('__PRODUCT_SPEC_VALUE__ c', 'a.spec_value_id = c.spec_value_id and a.product_id = c.aid', 'LEFT')
                    ->where($CaerWhere)
                    ->select();

                $level_discount = $this->users['level_discount'];
                $discount_price = $level_discount / 100;
                $spec_price = $users_price = $num_new = 0;
                foreach ($CartData as $key => $value) {
                    if (!empty($value['spec_price'])) {
                        if (!empty($level_discount)) $value['spec_price'] = $value['spec_price'] * $discount_price;
                        $spec_price += $value['product_num'] * $value['spec_price'];
                    } else {
                        if (!empty($level_discount)) $value['users_price'] = $value['users_price'] * $discount_price;
                        $users_price += $value['product_num'] * $value['users_price'];
                    }
                    $num_new += $value['product_num'];
                }
                $CartData['num']   = empty($num_new) ? 0 : $num_new;
                $CartData['price'] = sprintf("%.2f", $spec_price + $users_price);
                $CartData['price'] = empty($CartData['price']) ? '0.00' : $CartData['price'];
                /*END*/

                $this->success('操作成功！', null, ['NumberVal'=>$CartData['num'], 'AmountVal'=>$CartData['price']]);
            } else {
                $this->error('操作失败！');
            }
        }
    }

    // 选中产品
    public function cart_checked()
    {
        if (IS_POST) {
            $cart_id  = input('post.cart_id');
            $selected = input('post.selected');
            // dump($cart_id);
            // dump($selected);die;
            // 更新数组
            if (!empty($selected)) {
                $selected = 0;
            } else {
                $selected = 1;
            }
            // dump($selected);die;
            $data['selected'] = $selected;
            $data['update_time'] = getTime();
            // 更新条件
            if ('*' == $cart_id) {
                $cart_where = [
                    'product_num' => ['>', 0],
                    'users_id' => $this->users_id,
                    'lang'     => $this->home_lang,
                ];
            } else {
                $cart_where = [
                    'cart_id'  => $cart_id,
                    'product_num' => ['>', 0],
                    'users_id' => $this->users_id,
                    'lang'     => $this->home_lang,
                ];
            }
            // dump($data);
            // dump($cart_where);die;
            // 更新数据
            $return = $this->shop_cart_db->where($cart_where)->update($data);
            if (!empty($return)) {
                 $datas['status'] = 1;
                 $datas['msg'] = '操作成功';
                 $datas['res'] = '';
                 echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                 exit;
                // $this->success('操作成功！');
            }else{
                $datas['status'] = -1;
                $datas['msg'] = '选择失败';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;
                // $this->error('操作失败！');
            }
        }
    }

    public function shop_wechat_pay_select()
    {
        $ReturnOrderData = session($this->users_id.'_ReturnOrderData');
        if (empty($ReturnOrderData)) {
            $url = session($this->users_id.'_EyouShopOrderUrl');
            $this->error('订单支付异常，请刷新重新下单~',$url);
        }
        $eyou = [
            'field' => $ReturnOrderData,
        ];
        $this->assign('eyou',$eyou);
        return $this->fetch('users/shop_wechat_pay_select');
    }

    /**
     * 快速下单支付流程 - 添加商品信息及计算价格等
     * @return [type] [description]
     */
    public function fastSubmitOrder()
    {
        $aid = input('post.aid_1607507428/d');
        $pay_code = input('post.pay_code_1607507428/s'); // 支付方式

        if (IS_POST && !empty($aid) && !empty($pay_code)) {

            $OrderData = [];

            $mini_id = input('post.mini_id_1607507428/d');
            !empty($mini_id) && $OrderData['mini_id'] = $mini_id;

            // 规格值
            $spec_value_id = input('post.spec_value_id_1607507428/s');
            if (!empty($spec_value_id)) {
                $spec_value_id = preg_replace('/[^\d\_]/i', '', $spec_value_id);
                $spec_value_id = trim($spec_value_id, '_');
                $OrderData['spec_value_id'] = "_{$spec_value_id}_";
            }

            $ArchivesWhere = [
                'a.aid'  => $aid,
            ];
            if (!empty($spec_value_id)) $ArchivesWhere['b.spec_value_id'] = $spec_value_id;
            $field = 'a.aid, a.aid as product_id, a.title, a.litpic, a.users_price, a.stock_count, a.prom_type, a.attrlist_id, b.spec_price, b.spec_stock, b.spec_value_id, b.value_id';
            $list = Db::name('archives')->field($field)
                ->alias('a')
                ->join('__PRODUCT_SPEC_VALUE__ b', 'a.aid = b.aid', 'LEFT')
                ->where($ArchivesWhere)
                ->limit('0, 1')
                ->select();

            // 没有相应的产品
            if (empty($list[0])) $this->error('订单生成失败，没有相应的商品！');
            $list[0]['product_num']      = 1;
            $list[0]['under_order_type'] = 2; // 快速下单支付

            // 生成订单之前的产品数据整理
            $retData = $this->shop_model->handlerOrderData('fast', $OrderData, $list, $this->users);
            if (empty($retData['code'])) {
                $this->error($retData['msg']);
            }
            $OrderData = array_merge($OrderData, $retData['data']['OrderData']);
            $list = $retData['data']['list'];

            $OrderId = Db::name('shop_order')->insertGetId($OrderData);
            $OrderData['order_id'] = $OrderId;
            if (!empty($OrderId)) {

                // 生成订单之后的订单明细整理
                $retData = $this->shop_model->handlerDetailsData('fast', $OrderData, $list, $this->users);
                if (empty($retData['code'])) {
                    $this->error($retData['msg']);
                }
                $cart_ids = $retData['data']['cart_ids'];
                $OrderDetailsData = $retData['data']['OrderDetailsData'];
                $UpSpecValue = $retData['data']['UpSpecValue'];

                $DetailsId = Db::name('shop_order_details')->insertAll($OrderDetailsData);

                if (!empty($DetailsId)) {
                    // 清理购物车中已下单的ID
                    if (!empty($cart_ids)) Db::name('shop_cart')->where('cart_id', 'IN', $cart_ids)->delete();

                    // 产品库存、销量处理
                    $this->shop_model->ProductStockProcessing($UpSpecValue);

                    // 添加订单操作记录
                    AddOrderAction($OrderId, $this->users_id);

                    if (0 == $OrderData['payment_method']) {
                        // 选择在线付款并且在手机微信端、小程序中则返回订单ID，订单号，订单交易类型
                        if (isMobile() && isWeixin()) {
                            // $where = [
                            //     'pay_id' => 1,
                            //     'pay_mark' => 'wechat'
                            // ];
                            // $PayInfo = Db::name('pay_api_config')->where($where)->getField('pay_info');
                            // if (!empty($PayInfo)) $PayInfo = unserialize($PayInfo);

                            // if (!empty($this->users['open_id']) && !empty($PayInfo) && 0 == $PayInfo['is_open_wechat']) {
                            //     $ReturnOrderData = [
                            //         'unified_id'       => $OrderId,
                            //         'unified_number'   => $OrderData['order_code'],
                            //         'transaction_type' => 2, // 订单支付购买
                            //         'order_total_amount' => $TotalAmount,
                            //         'order_source'     => 1, // 提交订单页
                            //         'is_gourl'         => 1,
                            //     ];
                            //     if ($this->users['users_money'] <= '0.00') {
                            //         // 余额为0
                            //         $ReturnOrderData['is_gourl'] = 0;
                            //         $this->success('订单已生成！', null, $ReturnOrderData);
                            //     } else {
                            //         // 余额不为0
                            //         $url = url('user/Shop/shop_wechat_pay_select');
                            //         session($this->users_id.'_ReturnOrderData', $ReturnOrderData);
                            //         $this->success('订单已生成！', $url, $ReturnOrderData);
                            //     }
                            // } else {
                            //     // 如果会员没有openid则跳转到支付页面进行支付
                            //     // 在线付款时，跳转至付款页
                            //     // 对ID和订单号加密，拼装url路径
                            //     $Paydata = [
                            //         'order_id'   => $OrderId,
                            //         'order_code' => $OrderData['order_code'],
                            //     ];

                            //     // 先 json_encode 后 md5 加密信息
                            //     $Paystr = md5(json_encode($Paydata));

                            //     // 存入 cookie
                            //     cookie($Paystr, $Paydata);

                            //     // 跳转链接
                            //     $PaymentUrl = urldecode(url('user/Pay/pay_recharge_detail',['paystr'=>$Paystr]));

                            //     $this->success('订单已生成！', $PaymentUrl, ['is_gourl' => 1]);
                            // }
                        } else {
                            if ('alipay' == $pay_code) {
                                // 重要参数，支付宝配置信息
                                $PayInfo = Db::name('pay_api_config')->where([
                                        'pay_id' => 2,
                                        'pay_mark' => 'alipay'
                                    ])->getField('pay_info');
                                if (empty($PayInfo)) $this->error('请先配置支付宝！');
                                $alipay = unserialize($PayInfo);
                                $vars = [
                                    'unified_number'   => $OrderData['order_code'],
                                    'unified_amount'   => $OrderData['order_amount'],
                                    'transaction_type' => 2,
                                ];
                                $PayApiLogic = new \app\user\logic\PayApiLogic;
                                $retData = $PayApiLogic->UseAliPayPay(['transaction_type'=>2], $vars, $alipay, true);
                                if (!empty($retData['alipay_url'])) {
                                    $PaymentUrl = $retData['alipay_url'];
                                    $this->redirect($PaymentUrl);
                                } else {
                                    $this->error('调起支付宝页面失败！');
                                }
                            }
                            else if ('weipay' == $pay_code) {
                                $vars = [
                                    'unified_number' => $OrderData['order_code'],
                                    'transaction_type' => 2
                                ];
                                $data = [
                                    'url_qrcode'        => url('user/PayApi/pay_wechat_png', $vars),
                                    'pay_id'            => 1,
                                    'pay_mark'          => 'wechat',
                                    'unified_id'        => $OrderId,
                                    'unified_number'    => $OrderData['order_code'],
                                    'transaction_type'  => 2,
                                ];
                                $this->success('正在支付中', null, $data);
                            }
                        }
                    }
                }
            }
        }
        $this->error('操作失败！');
    }

    // 订单提交处理逻辑，添加商品信息及计算价格等
    public function shop_payment_page()
    {
        $hxf = 1;
        if ($hxf =1) {
            // 提交的订单信息判断
            $post = input('post.');
            // $post['addr_id'] = 5;
            // $post['payment_method'] = 0;
            // $post['payment_type'] = 'zxzf_wechat';
            // dump($post);die;
            if (empty($post['payment_type']))
            {
                $data['status'] = 1;
                $data['mag'] = '网站支付配置未完善，购买服务暂停使用';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit();
            }
            
            // $this->error('网站支付配置未完善，购买服务暂停使用');
            
            if (empty($post)) 
            {
                $data['status'] = 1;
                $data['mag'] = '订单生成失败，商品数据有误！';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit();
            }
            // $this->error('订单生成失败，商品数据有误！'); 
            $Md5Value = !empty($post['Md5Value']) ? $post['Md5Value'] : null;
            
            // if (!empty($post['aid'])) $aid  = intval(mchStrCode($post['aid'],'DECODE'));
            // if (!empty($post['num'])) $num  = intval(mchStrCode($post['num'],'DECODE'));
            // if (!empty($post['type'])) $type  = intval(mchStrCode($post['type'],'DECODE'));
            
            $OrderData = [];

            // 产品ID是否存在
            if (!empty($Md5Value)) {
                $querystr = cookie($Md5Value);
                $aid = !empty($querystr['aid']) ? intval($querystr['aid']) : 0;
                $num = !empty($querystr['product_num']) ? intval($querystr['product_num']) : 0;
                $mini_id = !empty($querystr['mini_id']) ? intval($querystr['mini_id']) : 0;
                !empty($mini_id) && $OrderData['mini_id'] = $mini_id;
                $spec_value_id = !empty($querystr['spec_value_id']) ? $querystr['spec_value_id'] : '';
                !empty($spec_value_id) && $OrderData['spec_value_id'] = $spec_value_id;
                $type = !empty($post['type']) ? intval($post['type']) : 0;
                $OrderData['order_md5'] = md5($aid.$spec_value_id);

                // if (!empty($post['spec_value_id'][0])) $spec_value_id  = mchStrCode($post['spec_value_id'][0],'DECODE');
                // 商品数量判断
                if ($num <= 0)
                {
                    $data['status'] = 1;
                    $data['mag'] = '订单生成失败，商品数量有误！';
                    $data['res'] = '';
                    echo json_encode($data,JSON_UNESCAPED_UNICODE);
                    exit();
                }
                
                // $this->error('订单生成失败，商品数量有误！');
                // 订单来源判断
                if ($type != 1)
                {
                    $data['status'] = 1;
                    $data['mag'] = '订单生成失败，提交来源有误！';
                    $data['res'] = '';
                    echo json_encode($data,JSON_UNESCAPED_UNICODE);
                    exit();
                }
                // $this->error('订单生成失败，提交来源有误！');
                
                // 立即购买查询条件
                $ArchivesWhere = [
                    'a.aid'  => $aid,
                ];
                if (!empty($spec_value_id)) $ArchivesWhere['b.spec_value_id'] = $spec_value_id;
                $field = 'a.aid, a.aid as product_id, a.title, a.litpic, a.users_price, a.stock_count, a.prom_type, a.attrlist_id, b.spec_price, b.spec_stock, b.spec_value_id, b.value_id, c.spec_is_select';
                $list = $this->archives_db->field($field)
                    ->alias('a')
                    ->join('__PRODUCT_SPEC_VALUE__ b', 'a.aid = b.aid', 'LEFT')
                    ->join('__PRODUCT_SPEC_DATA__ c', 'a.aid = c.aid', 'LEFT')
                    ->where($ArchivesWhere)
                    ->limit('0, 1')
                    ->select();
                $list[0]['product_num']      = $num;
                $list[0]['under_order_type'] = $type;
                if (empty($list[0]['spec_is_select'])) {
                    $list[0]['spec_price']    = '';
                    $list[0]['spec_stock']    = '';
                    $list[0]['spec_value_id'] = '';
                }
            }else{
                // 购物车查询条件
                $cart_where = [
                    'a.users_id' => $this->users_id,
                    'a.selected' => 1,
                ];
                $list = $this->shop_cart_db->field('a.*, b.aid, b.title, b.litpic, b.users_price, b.stock_count, b.prom_type, b.attrlist_id, c.spec_price, c.spec_stock, c.value_id,d.weight')
                    ->alias('a')
                    ->join('__ARCHIVES__ b', 'a.product_id = b.aid', 'LEFT')
                    ->join('__PRODUCT_SPEC_VALUE__ c', 'a.spec_value_id = c.spec_value_id and a.product_id = c.aid', 'LEFT')
                      ->join('product_content d','d.aid=b.aid')
                    ->where($cart_where)
                    ->select();
            }

            // 没有相应的产品
            if (empty($list))
            {
                 $data['status'] = 1;
                $data['mag'] = '订单生成失败，没有相应的产品！';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit(); 
            }
            
            // $this->error('订单生成失败，没有相应的产品！');

            // 生成订单之前的产品数据整理
            $retData = $this->shop_model->handlerOrderData('normal', $OrderData, $list, $this->users, $post);
            // dump($retData)
            if (empty($retData['code'])) $this->error($retData['msg']);
            
            // 合并订单数据
            $OrderData = array_merge($OrderData, $retData['data']['OrderData']);
            // DUMP($OrderData);DIE;
            $list = $retData['data']['list'];
            
            foreach ($list as $k => $v)
            {
               $rest = $this->num($v['product_id'],$v['product_num']);
               $list[$k]['users_price'] = $rest['spec_price'];
            }
            
            // dump($list);die;

            $AddrData = [];
            // DUMP($OrderData);DIE;
            // 非虚拟订单则查询运费信息
            if (empty($OrderData['prom_type'])) {
                // DUMP(113);DIE;
                // 没有选择收货地址
                if (empty($post['addr_id'])) {
                    // 在微信端并且不在小程序中
                    if (isWeixin() && !isWeixinApplets()) {
                        // 跳转至收货地址添加选择页
                        $get_addr_url = url('user/Shop/shop_get_wechat_addr');
                        $is_gourl['is_gourl'] = 1;
                        $this->success('101:选择添加地址方式', $get_addr_url, $is_gourl);
                    } else {
                        $paramNew['add_addr'] = 1;
                        $this->error('订单生成失败，请添加收货地址！', null, $paramNew);
                    }
                }

                // 查询收货地址
                $AddrWhere = [
                    'addr_id'  => $post['addr_id'],
                    'users_id' => $this->users_id,
                ];
                $AddressData = $this->shop_address_db->where($AddrWhere)->find();
                // dump($AddrWhere);die;
                // DUMP(1);DIE;
                if (empty($AddressData)) {
                    if (isWeixin() && !isWeixinApplets()) {
                        // 跳转至收货地址添加选择页
                        $get_addr_url = url('user/Shop/shop_get_wechat_addr');
                        $is_gourl['is_gourl'] = 1;
                        $this->success('102:选择添加地址方式', $get_addr_url, $is_gourl);
                    } else {
                        $paramNew['add_addr'] = 1;
                        $this->error('订单生成失败，请添加收货地址！', null, $paramNew);
                    }
                }

                $shop_open_shipping = getUsersConfigData('shop.shop_open_shipping');
                // dump($shop_open_shipping);die;
                
               $template = M('shop_shipping_template')->where('province_id', $AddressData['province'])->find();
            //   dump($OrderData);die;
             foreach($list as $k => $v)
             {
                  $total_weight += $v['product_num'] * $v['weight'];
             }
            // dump($total_weight);die;
            //   dump($template);
            
            if($shop_open_shipping == 0)
            {
                $freight = 0;
            }else
            {
                if($post['payment_method'] != 3)
                {
                     if((string)$OrderData['order_amount'] > $template['template_money'])
                    {
                      $freight = 0;
                    }
                   else
                   {

                       if($total_weight < 1)
                       {
                           $freight = $template['first_weight'];
                       }else
                       {
                             $total_weight -= 1;
                            //   dump($wergth);die;
                              $price = $total_weight * $template['continued_weight'];
                              $freight = $price + $template['first_weight'];
                       }
                    }
                }else
                {
                    $freight = 0; 
                }
            }
            //   DUMP($freight);DIE;
                
                // $template_money = '0.00';
                // if (!empty($shop_open_shipping)) {
                //     // 通过省份获取运费模板中的运费价格
                //     $template_money = $this->shipping_template_db->where('province_id', $AddressData['province'])->getField('template_money');
                //     if (0 >= $template_money) {
                //         // 省份运费价格为0时，使用统一的运费价格，固定ID为100000
                //         $template_money = $this->shipping_template_db->where('province_id', '100000')->getField('template_money');
                //     }
                //     // 合计金额加上运费价格
                //     $OrderData['order_total_amount'] += $template_money;
                //     $OrderData['order_amount'] += $template_money;
                // }
                // $OrderData
                 $OrderData['order_total_amount'] += $freight;
                $OrderData['order_amount'] += $freight;
// dump($OrderData);die;
                // 拼装数组
                $AddrData = [
                    'consignee'    => $AddressData['consignee'],
                    'country'      => $AddressData['country'],
                    'province'     => $AddressData['province'],
                    'city'         => $AddressData['city'],
                    'district'     => $AddressData['district'],
                    'address'      => $AddressData['address'],
                    'mobile'       => $AddressData['mobile'],
                    'shipping_fee' => $freight,
                ];
            }
            // dump(123);die;
            // 存在收货地址则追加合并到主表数组
            if (!empty($AddrData)) $OrderData = array_merge($OrderData, $AddrData);

            // 数据验证
            $rule = ['payment_method' => 'require|token'];
            $message = ['payment_method.require' => '不可为空！'];
            $validate = new \think\Validate($rule, $message);
            // DUMP($validate->check($post));DIE;
            // if (!$validate->check($post)) $this->error('不可连续提交订单！')
            // DUMP($post);DIE;
            
            // dump($OrderData);DIE;
            
            if(!empty($post['coupon']))
            {
               $coupon = M('shop_coupon')->where(array('coupon_id' => $post['coupon']))->find();
               if($coupon['coupon_form'] == 1)
               {
                   if($coupon['end_date'] > time())
                   {
                       if($coupon['conditions_use'] <= $OrderData['order_total_amount'])
                       {
                        //   dump(22);
                           $OrderData['order_total_amount'] -= $coupon['coupon_price'];
                           $OrderData['order_amount'] -= $coupon['coupon_price'];
                       }
                       
                    //   dump($OrderData['order_total_amount']);
                    //   dump($coupon['conditions_use']);die;
                       $coupon_use['use_status'] = 1;
                       $coupon_use['use_time'] = time();
                       M('shop_coupon_use')->where(array('coupon_code' => $coupon['coupon_code']))->save($coupon_use);
                       $OrderData['coupon_id'] = $coupon['coupon_id'];
                       $OrderData['coupon_price'] = $coupon['coupon_price'];
                   }else
                   {
                        $data['status'] = -1;
                        $data['mag'] = '该优惠劵已到期';
                        $data['res'] = '';
                        echo json_encode($data,JSON_UNESCAPED_UNICODE);
                        exit(); 
                   }
               }
            }
            
            // dump($post);die;
            if (!empty($post['integral'])) {
                
                // DUMP(11);die;
                if($post['integral'] > $this->users['scores'])
                {
                    $data['status'] = -1;
                    $data['mag'] = '您的积分不足,请重新输入';
                    $data['res'] = '';
                    echo json_encode($data,JSON_UNESCAPED_UNICODE);
                    exit();
                }
                
                       $confg = M('users_config')->select();
                      foreach ($confg as $k => $v)
                      {
                          if($v['name'] == 'score_signin_score')
                          {
                              $score_signin = $v['value'];
                          }
                          if($v['name'] == 'score_signin_score2')
                          {
                            //   dump($v);die;
                              $score_signin_score2 = $v['value'];
                          }
                      }
                        $score_signin = (int)$score_signin/(int)$score_signin_score2;
                
                
               $users['scores'] = $this->users['scores'] - $post['integral']*$score_signin;
               
            //   dump($users['scores']);die;
            //   DUMP($users);DIE;
              $res = M('users')->where(array('users_id' => $this->users['users_id']))->save($users);
               $lists['type'] = 6;
               $lists['symbol'] = '-';
               $lists['users_id'] = $this->users['users_id'];
               $lists['score'] = $post['integral']*$score_signin;
               $lists['devote'] = $post['integral']*$score_signin;
               $lists['add_time'] = time();
               $lists['info'] = '商品积分抵扣';
               $lists['users_score'] =  $users['scores'];
               $ilst = M('users_score')->add($lists);
               $integral = $post['integral'];
              if ($ilst) {
                  $OrderData['order_total_amount'] -= $integral;
                  $OrderData['order_amount'] -= $integral;
              }
            }
            
            // // dump(234);die;
            // dump($OrderData);DIE;
            
            $OrderId = $this->shop_order_db->insertGetId($OrderData);
            
            $invoice['order_id'] = $OrderId;
            $invoice['company'] = $post['company'];
            $invoice['identification_number'] = $post['identification_number'];
            $invoice['email'] = $post['email'];
            $invoice['status'] = $post['status'];
            $invoice['address'] = $post['address'];
            $invoice['phone'] = $post['phone'];
            $invoice['bank_name'] = $post['bank_name'];
            $invoice['add_time'] = time();
            $invoice['bank_account'] = $post['bank_account'];
            
            // DUMP($invoice);die;
             
           $invoices = M('shop_invoice')->add($invoice);
           
               
            // DUMP($OrderId);die;
            
            if (!empty($OrderId)) {
                $OrderData['order_id'] = $OrderId;

                // 生成订单之后的订单明细整理
                $retData = $this->shop_model->handlerDetailsData('normal', $OrderData, $list, $this->users);
                if (empty($retData['code'])) $this->error($retData['msg']);
                
                // 清理购物车中已下单的ID
                $cart_ids = $retData['data']['cart_ids'];

                // 产品库存、销量处理
                $UpSpecValue = $retData['data']['UpSpecValue'];

                // 添加订单明细表
                $OrderDetailsData = $retData['data']['OrderDetailsData'];
                
                // dump($OrderDetailsData);DIE;
                $DetailsId = $this->shop_order_details_db->insertAll($OrderDetailsData);

                if (!empty($DetailsId)) {
                    // 清理购物车中已下单的ID
                    if (!empty($cart_ids)) $this->shop_cart_db->where('cart_id', 'IN', $cart_ids)->delete();

                    // 产品库存、销量处理
                    $this->shop_model->ProductStockProcessing($UpSpecValue);

                    // 添加订单操作记录
                    AddOrderAction($OrderId, $this->users_id);

                    if (getUsersTplVersion() == 'v1') { // 第一版会员中心
                        /*清除下单的Cookie数据*/
                        if (!empty($Md5Value)) Cookie::delete($Md5Value);
                        /* END */
                    }
                    
                    // DUMP(11);DIE;
                    if (0 == $post['payment_method'] || $post['payment_method'] == 3) {
                        if (isMobile() && isWeixin()) {
                            // 选择在线付款并且在手机微信端、小程序中则返回订单ID，订单号，订单交易类型
                            $where = [
                                'pay_id' => 1,
                                'pay_mark' => 'wechat'
                            ];
                            $PayInfo = Db::name('pay_api_config')->where($where)->getField('pay_info');
                            if (!empty($PayInfo)) $PayInfo = unserialize($PayInfo);

                            if (!empty($this->users['open_id']) && !empty($PayInfo) && 0 == $PayInfo['is_open_wechat']) {
                                $payment_type = $post['payment_type'];
                                if ('yezf_balance' == $payment_type) {
                                    // 余额支付
                                    if ($this->users['users_money'] < $OrderData['order_amount']) {
                                        // 余额不足，支付失败
                                        $this->error('余额不足，支付失败！', null, ['url' => url('user/Shop/shop_centre')]);
                                    } else {
                                        // 余额充足，进行支付
                                        $ret = Db::name('users')->where(['users_id' => $this->users_id])->update([
                                            'users_money' => Db::raw('users_money-'.$OrderData['order_amount']),
                                            'update_time' => getTime(),
                                        ]);
                                        if (false !== $ret) {
                                            $pay_details = [
                                                'unified_id'        => $OrderData['order_id'],
                                                'unified_number'    => $OrderData['order_code'],
                                                'transaction_type'  => 2,
                                                'payment_amount'    => $OrderData['order_amount'],
                                                'payment_type'      => "余额支付",
                                            ];
                                            $returnData = pay_success_logic($this->users_id, $OrderData['order_code'], $pay_details, 'balance');
                                            if (is_array($returnData)) {
                                                if (1 == $returnData['code']) {
                                                    $this->success($returnData['msg'], $returnData['url'], $returnData['data']);
                                                } else {
                                                    $this->error($returnData['msg'], null, ['url'=>url('user/Shop/shop_centre')]);
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    // 微信内支付、小程序内支付 -- 微信端不存在支付宝支付
                                    $ReturnOrderData = [
                                        'unified_id'       => $OrderId,
                                        'unified_number'   => $OrderData['order_code'],
                                        'transaction_type' => 2, // 订单支付购买
                                        'order_source'     => 1, // 提交订单页
                                        'is_gourl'         => 1,
                                        'order_total_amount' => 0 // 好像没有用处了
                                    ];
                                    if ($this->users['users_money'] <= '0.00') {
                                        // 余额为0
                                        $ReturnOrderData['is_gourl'] = 0;
                                        $this->success('订单已生成22！', null, $ReturnOrderData);
                                    } else {
                                        // 余额不为0
                                        $url = url('user/Shop/shop_wechat_pay_select');
                                        session($this->users_id.'_ReturnOrderData', $ReturnOrderData);
                                        $this->success('订单已生成333！', $url, $ReturnOrderData);
                                    }
                                }
                            } else {
                                // 执行如果会员没有openid则跳转到支付页面进行支付
                                $Paydata = [
                                    'order_id'   => $OrderId,
                                    'order_code' => $OrderData['order_code'],
                                ];

                                // 先 json_encode 后 md5 加密信息
                                $Paystr = md5(json_encode($Paydata));

                                // 存入 cookie
                                cookie($Paystr, $Paydata);

                                // 跳转链接
                                $PaymentUrl = urldecode(url('user/Pay/pay_recharge_detail',['paystr'=>$Paystr]));
                                $this->success('订单已生成555！', $PaymentUrl, ['is_gourl' => 1]);
                            }
                        } else {
                            // PC端、手机浏览器端 -- 在线付款时执行
                            if (getUsersTplVersion() == 'v2' && isset($post['payment_type'])) {
                                // 第二套模板 -- 余额支付、微信支付、支付宝支付、其他第三方支付
                                $payment_type = $post['payment_type'];
                                if ('yezf_balance' == $payment_type) {
                                    // 余额支付
                                    if ($this->users['users_money'] < $OrderData['order_amount']) {
                                        $url = url('user/Shop/shop_centre');
                                        $this->error('余额不足，支付失败！', null, ['url'=>$url]);
                                    } else {
                                        $ret = Db::name('users')->where(['users_id'=>$this->users_id])->update([
                                            'users_money' => Db::raw('users_money-'.$OrderData['order_amount']),
                                            'update_time' => getTime(),
                                        ]);
                                        if (false !== $ret) {
                                            $pay_details = [
                                                'unified_id'        => $OrderData['order_id'],
                                                'unified_number'    => $OrderData['order_code'],
                                                'transaction_type'  => 2,
                                                'payment_amount'    => $OrderData['order_amount'],
                                                'payment_type'      => "余额支付",
                                            ];
                                            $returnData = pay_success_logic($this->users_id, $OrderData['order_code'], $pay_details, 'balance');
                                            if (is_array($returnData)) {
                                                if (1 == $returnData['code']) {
                                                    $this->success($returnData['msg'], $returnData['url'], $returnData['data']);
                                                } else {
                                                    $this->error($returnData['msg'], null, ['url'=>url('user/Shop/shop_centre')]);
                                                }
                                            }
                                        }
                                    }
                                } else if (in_array($payment_type, ['zxzf_wechat', 'zxzf_alipay'])) {
                                    // 内置第三方在线支付
                                    $payment_type_arr = explode('_', $payment_type);
                                    $pay_mark = !empty($payment_type_arr[1]) ? $payment_type_arr[1] : '';
                                    $payApiRow = Db::name('pay_api_config')->where(['pay_mark' => $pay_mark, 'lang' => $this->home_lang])->find();
                                    if (empty($payApiRow)) $this->error('请选择正确的支付方式！');

                                    // 返回支付所需参数
                                    $data = [
                                        'code'              => 'order_status_0',
                                        'pay_id'            => $payApiRow['pay_id'],
                                        'pay_mark'          => $pay_mark,
                                        'unified_id'        => $OrderData['order_id'],
                                        'unified_number'    => $OrderData['order_code'],
                                        'transaction_type'  => 2,
                                    ];
                                    // dump($data);die;
                                    
                                    $data['status'] = 1;
                                    $data['mag'] = '正在支付中';
                                    $data['res'] = $data;
                                    $data['url'] = url('user/Shop/shop_centre');
                                    echo json_encode($data,JSON_UNESCAPED_UNICODE);
                                    exit();
                                    // $this->success('正在支付中', url('user/Shop/shop_centre'), $data);
                                }
                            } else {
                                // 第一套模板 -- 执行跳转至订单支付页
                                $Paydata = [
                                    'order_id'   => $OrderId,
                                    'order_code' => $OrderData['order_code'],
                                ];

                                // 先 json_encode 后 md5 加密信息
                                $Paystr = md5(json_encode($Paydata));

                                // 存入 cookie
                                cookie($Paystr, $Paydata);

                                // 跳转链接
                                $PaymentUrl = urldecode(url('user/Pay/pay_recharge_detail',['paystr'=>$Paystr]));
                                
                                $this->success('订单已生成77！', $PaymentUrl);
                            }
                        }
                    } else {
                        // 货到付款 -- 无需跳转付款页，直接跳转订单列表页
                        $PaymentUrl = urldecode(url('user/Shop/shop_centre'));
                        
                        // 再次添加一条订单操作记录
                        AddOrderAction($OrderId, $this->users_id, 0, 1, 0, 1, '货到付款！', '会员选择货到付款，款项由快递代收！');

                        // 邮箱发送
                        $SmtpConfig = tpCache('smtp');
                        $ReturnData['email'] = GetEamilSendData($SmtpConfig, $this->users, $OrderData, 1, 'delivery_pay');
                            
                        // 手机发送
                        $SmsConfig = tpCache('sms');
                        $ReturnData['mobile'] = GetMobileSendData($SmsConfig, $this->users, $OrderData, 1, 'delivery_pay');

                        // 发送站内信给后台
                        $OrderData['pay_method'] = '货到付款';
                        SendNotifyMessage($OrderData, 5, 1, 0);

                        // 返回结束
                        $ReturnData['is_gourl'] = 1;
                        $this->success('订单已生成99！', $PaymentUrl, $ReturnData);
                    }
                } else {
                    
                $data['status'] = -1;
                $data['mag'] = '订单生成失败，商品数据有误！';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit(); 
                    
                    // $this->error('订单生成失败，商品数据有误！');
                }
            } else {
                $data['status'] = -1;
                $data['mag'] = '订单生成失败，商品数据有误！';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit(); 
                // $this->error('订单生成失败，商品数据有误！');
            }
        }
    }

    // 添加收货地址
    public function shop_add_address()
    {
        if (IS_POST) {
            $post = input('post.');
            if (empty($post['consignee'])) {
                $data['status'] = -1;
                $data['msg'] = '收货人姓名不可为空';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;  
                // $this->error('收货人姓名不可为空！');
            }
            if (empty($post['mobile'])) {
                
                $data['status'] = -1;
                $data['msg'] = '收货人手机不可为空';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit; 
                
                // $this->error('收货人手机不可为空！');
            }
            if (empty($post['province'])) {
                
                $data['status'] = -1;
                $data['msg'] = '收货省份不可为空';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit; 
                
                // $this->error('收货省份不可为空！');
            }
            if (empty($post['address'])) {
                
                $data['status'] = -1;
                $data['msg'] = '详细地址不可为空';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit; 
                
                // $this->error('详细地址不可为空！');
            }
            // 添加数据
            $post['users_id'] = $this->users_id;
            $post['add_time'] = getTime();
            $post['lang']     = $this->home_lang;
            if (isMobile() && isWeixin()) {
                // 在手机微信端、小程序中则把新增的收货地址设置为默认地址
                $post['is_default'] = 1;// 设置为默认地址
            } else {
                $post['is_default'] = !empty($post['is_default']) ? 1 : 0;
            }
            $addr_id = $this->shop_address_db->add($post);

            if (!empty($addr_id)) {
                // 把对应会员下的所有地址改为非默认
                if (1 == $post['is_default']) {
                    $AddressWhere = [
                        'addr_id'  => array('NEQ',$addr_id),
                        'users_id' => $this->users_id,
                        'lang'     => $this->home_lang,
                    ];
                    $data_new['is_default']  = 0;// 设置为非默认地址
                    $data_new['update_time'] = getTime();
                    $this->shop_address_db->where($AddressWhere)->update($data_new);
                }

                if (isMobile() && isWeixin()) {
                    $ResultD = [
                        'url' => session($this->users_id.'_EyouShopOrderUrl')
                    ];
                    $data['status'] = 1;
                    $data['msg'] = '添加成功';
                    $data['res'] = $ResultD;
                    echo json_encode($data,JSON_UNESCAPED_UNICODE);
                    exit; 
                    // $this->success('添加成功！', session($this->users_id.'_EyouShopOrderUrl'), $ResultD);
                }

                // 根据地址ID查询相应的中文名字
                $post['country']  = '中国';
                $post['province'] = get_province_name($post['province']);
                $post['city']     = get_city_name($post['city']);
                $post['district'] = get_area_name($post['district']);
                $post['addr_id'] = $addr_id;
                $post['is_mobile'] = $this->is_mobile;
                $data['status'] = 1;
                $data['msg'] = '添加成功';
                $data['res'] = $post;
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit; 
                // $this->success('添加成功！', null, $post);
            } else {
                $this->error('数据有误！');
            }
        }

        $types = input('param.type');
        if ('list' == $types || 'order' == $types || 'order_new' == $types) {
            $Where = [
                'users_id' => $this->users_id,
                'lang'     => $this->home_lang,
            ];
            $addr_num = $this->shop_address_db->where($Where)->count();

            $eyou = [
                'field'    => [
                    'Province' => get_province_list(),
                    'types'    => $types,
                    'addr_num' => $addr_num,
                ],
            ];
            $this->assign('eyou',$eyou);
        }else{
            $this->error('非法来源！');
        }

        return $this->fetch('users/shop_add_address');
    }

    // 更新收货地址
    public function shop_edit_address()
    {
        if (IS_POST) {
            $post = input('post.');
            if (empty($post['consignee'])) {
               $data['status'] = -1;
                $data['msg'] = '收货人姓名不可为空';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;  
                // $this->error('收货人姓名不可为空！');
            }
            if (empty($post['mobile'])) {
                $data['status'] = -1;
                $data['msg'] = '收货人手机不可为空';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;  
                // $this->error('收货人手机不可为空！');
            }
            if (empty($post['province'])) {
                 $data['status'] = -1;
                $data['msg'] = '收货省份不可为空';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;  
                // $this->error('收货省份不可为空！');
            }
            if (empty($post['address'])) {
                $data['status'] = -1;
                $data['msg'] = '详细地址不可为空';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;  
                // $this->error('详细地址不可为空！');
            }
            // 更新条件及数据
            $post['users_id'] = $this->users_id;
            $post['update_time'] = getTime();
            $post['lang']     = $this->home_lang;
            $post['country'] = (empty($post['country']) || $post['country'] == '中国') ? 0 : 1;
            $post['is_default'] = !empty($post['is_default']) ? 1 : 0;

            $AddrWhere = [
                'addr_id'  => $post['addr_id'],
                'users_id' => $this->users_id,
                'lang'     => $this->home_lang,
            ];
            
            // dump($post);die;

            $addr_id = $this->shop_address_db->where($AddrWhere)->update($post);

            if (!empty($addr_id)) {
                // 把对应会员下的所有地址改为非默认
                if (1 == $post['is_default']) {
                    $AddressWhere = [
                        'addr_id'  => array('NEQ',$post['addr_id']),
                        'users_id' => $this->users_id,
                        'lang'     => $this->home_lang,
                    ];
                    $data_new['is_default']  = 0;// 设置为非默认地址
                    $data_new['update_time'] = getTime();
                    $this->shop_address_db->where($AddressWhere)->update($data_new);
                }

                // 根据地址ID查询相应的中文名字
                $post['country']  = '中国';
                $post['province'] = get_province_name($post['province']);
                $post['city']     = get_city_name($post['city']);
                $post['district'] = get_area_name($post['district']);
                
                
                $data['status'] = 1;
                $data['msg'] = '修改成功';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;  
                // $this->success('修改成功！','',$post);
            }else{
                 $data['status'] = -1;
                $data['msg'] = '数据有误';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;  
                // $this->error('数据有误！');
            }
        }

        $AddrId   = input('param.addr_id');
        $AddrWhere = [
            'addr_id'  => $AddrId,
            'users_id' => $this->users_id,
            'lang'     => $this->home_lang,
        ];
        // 根据地址ID查询相应的中文名字
        $AddrData = $this->shop_address_db->where($AddrWhere)->find();
        if (empty($AddrData)) {
                $data['status'] = -1;
                $data['msg'] = '数据有误';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit; 
        }
        $AddrData['country']  = '中国'; //国家
        // $AddrData['Province'] = get_province_list(); // 省份
        // $AddrData['City']     = $this->region_db->where('parent_id',$AddrData['province'])->select(); // 城市
        // $AddrData['District'] = $this->region_db->where('parent_id',$AddrData['city'])->select(); // 县/区/镇
        $AddrData['onDelAddress'] = " onclick=\"DelAddress('{$AddrId}', this);\" ";
        $eyou = [
            'field' => $AddrData,
        ];
        
        $data['status'] = 1;
        $data['msg'] = '';
        $data['res'] = $eyou;
        echo json_encode($data,JSON_UNESCAPED_UNICODE);
        exit;  
        // dump($eyou);die;
        // $this->assign('eyou',$eyou);
        // return $this->fetch('users/shop_edit_address');
    }

    // 删除收货地址
    public function shop_del_address()
    {
        if (IS_POST) {
            $addr_id = input('post.addr_id/d');
          //  dump($addr_id);die;
            $Where = [
                'addr_id'  => $addr_id,
                'users_id' => $this->users_id,
                'lang'     => $this->home_lang,
            ];
            $return = $this->shop_address_db->where($Where)->delete();
            if ($return) {
                $data['status'] = 1;
                $data['msg'] = '删除成功';
                $data['res'] = $post;
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit; 
                // $this->success('删除成功！');
            }else{           
                $data['status'] = -1;
                $data['msg'] = '删除失败';
                $data['res'] = $post;
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit; 
                // $this->error('删除失败！');
            }
        }
    }

    // 更新收货地址，设置为默认地址
    public function shop_set_default_address()
    {
        if (IS_POST) {
            $post = input('post.');
            // 更新条件及数据
            $post['users_id']   = $this->users_id;
            $post['is_default'] = '1'; //设置为默认
            $post['add_time']   = getTime();
            $post['lang']       = $this->home_lang;

            $AddrWhere = [
                'addr_id'  => $post['addr_id'],
                'users_id' => $this->users_id,
                'lang'     => $this->home_lang,
            ];
            $addr_id = $this->shop_address_db->where($AddrWhere)->update($post);
            if (!empty($addr_id)) {
                // 把对应会员下的所有地址改为非默认
                $AddressWhere = [
                    'addr_id'  => array('NEQ',$post['addr_id']),
                    'users_id' => $this->users_id,
                    'lang'     => $this->home_lang,
                ];
                $data['is_default']  = '0';// 设置为非默认
                $data['update_time'] = getTime();
                $this->shop_address_db->where($AddressWhere)->update($data);
                
                $data['status'] = 1;
                $data['msg'] = '设置成功';
                $data['res'] = $post;
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit; 
                //$this->success('设置成功！');
            }else{
                $data['status'] = -1;
                $data['msg'] = '数据有误';
                $data['res'] = $post;
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit; 
                // $this->error('数据有误！');
            }
        }
    }

    // 查询运费
    public function shop_inquiry_shipping()
    {
        if (IS_AJAX_POST) {
            $shop_open_shipping = getUsersConfigData('shop.shop_open_shipping');
            if (empty($shop_open_shipping)) $this->success('未开启运费', '', 0);
            
            // 查询会员收货地址，获取省份
            $addr_id = input('post.addr_id');
            $where = [
                'addr_id'  => $addr_id,
                'users_id' => $this->users_id,
                'lang'     => $this->home_lang,
            ];
            $province = $this->shop_address_db->where($where)->getField('province');

            // 通过省份获取运费模板中的运费价格
            $template_money = $this->shipping_template_db->where('province_id', $province)->getField('template_money');
            if ('0.00' == $template_money) {
                // 省份运费价格为0时，使用统一的运费价格，固定ID为100000
                $template_money = $this->shipping_template_db->where('province_id', '100000')->getField('template_money');
            }
            $this->success('查询成功！', '', $template_money);
        } else {
            $this->error('订单号错误');
        }
    }
    
    public function get_region()
    {
        $RegionData = $this->region_db->where("parent_id",0)->select();
        $data['status'] = 1;
        $data['msg'] = '';
        $data['res'] = $RegionData;
        echo json_encode($data,JSON_UNESCAPED_UNICODE);
        exit;
    }

    // 联动地址获取
    public function get_region_data(){
        $parent_id  = input('param.parent_id/d');
        $RegionData = $this->region_db->where("parent_id",$parent_id)->select();
        echo json_encode($RegionData);
    }

    // 会员提醒收货
    public function shop_order_remind()
    {
        if (IS_AJAX_POST) {
            $post = input('post.');
            // 添加订单操作记录
            AddOrderAction($post['order_id'],$this->users_id,'0','1','0','1','提醒成功！','会员提醒管理员及时发货！');
            $this->success('提醒成功！');
        }else{
            $this->error('订单号错误');
        }
    }

    // 会员确认收货
    public function shop_member_confirm()
    {
        if (IS_POST) {
            $post = input('post.');
            // 更新条件
            $Where = [
                'order_id' => $post['order_id'],
                'users_id' => $this->users_id,
                'lang'     => $this->home_lang,
            ];
            // 更新数据
            $Data = [
                'order_status' => 3,
                'confirm_time' => getTime(),
                'update_time'  => getTime(),
            ];
            // 更新订单主表
            $return = $this->shop_order_db->where($Where)->update($Data);
            if (!empty($return)) {
                // 更新数据
                $Data = [
                    'update_time'  => getTime(),
                ];
                // 更新订单明细表
                $this->shop_order_details_db->where($Where)->update($Data);
                // 添加订单操作记录
                AddOrderAction($post['order_id'],$this->users_id,'0','3','1','1','确认收货！','会员已确认收到货物，订单完成！');
                
                $datas['status'] = 1;
                $datas['msg'] = '会员确认收货';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;  
                // $this->success('会员确认收货');
            }else{
                $datas['status'] = -1;
                $datas['msg'] = '订单号错误';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;  
                // $this->error('订单号错误');
            }
        }
    }
    
    // 获取微信收货地址
    public function shop_get_wechat_addr()
    {
        if (IS_AJAX_POST) {
            // 微信配置信息
            $WeChatLoginConfig = !empty($this->usersConfig['wechat_login_config']) ? unserialize($this->usersConfig['wechat_login_config']) : [];
            $appid     = !empty($WeChatLoginConfig['appid']) ? $WeChatLoginConfig['appid'] : '';
            $appsecret = !empty($WeChatLoginConfig['appsecret']) ? $WeChatLoginConfig['appsecret'] : '';
            
            if (empty($appid)) {
                $this->error('后台微信配置尚未配置AppId，不可以获取微信地址！');
            }else if (empty($appsecret)) {
                $this->error('后台微信配置尚未配置AppSecret，不可以获取微信地址！');
            }

            // 当前时间戳
            $time = getTime();
            // 微信access_token和jsapi_ticket信息
            $WechatData  = getUsersConfigData('wechat');
            // access_token信息判断
            $accesstoken = $WechatData['wechat_token_value'];
            if (empty($accesstoken)) {
                // 如果配置表中的accesstoken为空则执行
                // 获取公众号access_token，接口限制10万次/天
                $return = $this->shop_model->GetWeChatAccessToken($appid,$appsecret);
                if (empty($return['status'])) {
                    $this->error($return['prompt']);
                }else{
                    $accesstoken = $return['token'];
                }
            }else if ($time > ($WechatData['wechat_token_time']+7000)) {
                // 如果配置表中的时间超过过期时间则执行
                // 获取公众号access_token，接口限制10万次/天
                $return = $this->shop_model->GetWeChatAccessToken($appid,$appsecret);
                if (empty($return['status'])) {
                    $this->error($return['prompt']);
                }else{
                    $accesstoken = $return['token'];
                }
            }

            // jsapi_ticket信息判断
            $jsapi_ticket = $WechatData['wechat_ticket_value'];
            if (empty($jsapi_ticket)) {
                // 获取公众号jsapi_ticket，接口限制500万次/天
                $return = $this->shop_model->GetWeChatJsapiTicket($accesstoken);
                if (empty($return['status'])) {
                    $this->error($return['prompt']);
                }else{
                    $jsapi_ticket = $return['ticket'];
                }
            }else if ($time > ($WechatData['wechat_ticket_time']+7000)) {
                // 获取公众号jsapi_ticket，接口限制500万次/天
                $return = $this->shop_model->GetWeChatJsapiTicket($accesstoken);
                if (empty($return['status'])) {
                    $this->error($return['prompt']);
                }else{
                    $jsapi_ticket = $return['ticket'];
                }
            }

            // <---- 加密参数开始 
            // 微信公众号jsapi_ticket
            // $jsapi_ticket = $jsapi_ticket;
            // 随机字符串
            $noncestr  = $this->shop_model->GetRandomString('16');
            $noncestr  = "$noncestr";
            // 当前时间戳
            $timestamp = time();
            $timestamp = "$timestamp";
            // 当前访问接口URL
            $url       = $this->request->url(true);
            // 加密参数结束 ----->
            
            // 参数加密，顺序固定不可改变
            $string    = 'jsapi_ticket='.$jsapi_ticket.'&noncestr='.$noncestr.'&timestamp='.$timestamp.'&url='.$url;
            $signature = SHA1($string);

            // 返回结果
            $result = [
                // 用于调试，不影响正常业务(如不需要，可直接清理)
                'token'     => $accesstoken,
                'ticket'    => $jsapi_ticket,
                'url'       => $url, // 传入接口调用参数(必须返回)
                'appid'     => $appid,
                'timestamp' => $timestamp,
                'noncestr'  => $noncestr,
                'signature' => $signature,
            ];
            $this->success('数据获取！',null,$result);
        }

        $result = [
            'wechat_url'   => url("user/Shop/shop_get_wechat_addr"),
            'add_addr_url' => url("user/Shop/shop_add_address"),
        ];
        $eyou = array(
            'field' => $result,
        );
        $this->assign('eyou', $eyou);
        return $this->fetch('users/shop_get_wechat_addr');
    }

    // 添加微信的收货地址到数据库
    public function add_wechat_addr()
    {
        if (IS_AJAX_POST) {
            $post = input('post.');
            // 省
            $province = $this->region_db->where('name',$post['provinceName'])->getField('id');
            // 市
            $city     = $this->region_db->where('name',$post['cityName'])->getField('id');
            // 县
            $district = $this->region_db->where('name',$post['countryName'])->getField('id');

            // 查询这个收货地址是否存在
            $where  = [
                'users_id'   => $this->users_id,
                'consignee'  => $post['userName'],
                'mobile'     => $post['telNumber'],
                'province'   => $province,
                'city'       => $city,
                'district'   => $district,
                'address'    => $post['detailInfo'],
                'lang'       => $this->home_lang,
            ];
            $return = $this->shop_address_db->where($where)->find();
            if (!empty($return)) {
                $this->success('获取成功！',session($this->users_id.'_EyouShopOrderUrl'));
            }else{
                $data = [
                    'users_id'   => $this->users_id,
                    'consignee'  => $post['userName'],
                    'mobile'     => $post['telNumber'],
                    'province'   => $province,
                    'city'       => $city,
                    'district'   => $district,
                    'address'    => $post['detailInfo'],
                    'is_default' => 1, // 设置为默认地址
                    'lang'       => $this->home_lang,
                    'add_time'   => getTime(),
                ];
                $addr_id = $this->shop_address_db->add($data);
                if (!empty($addr_id)) {
                    // 把对应会员下的所有地址改为非默认
                    $AddressWhere = [
                        'addr_id'  => array('NEQ',$addr_id),
                        'users_id' => $this->users_id,
                        'lang'     => $this->home_lang,
                    ];
                    $data_new['is_default']  = '0';// 设置为非默认地址
                    $data_new['update_time'] = getTime();
                    $this->shop_address_db->where($AddressWhere)->update($data_new);
                    $this->success('获取成功！',session($this->users_id.'_EyouShopOrderUrl'));
                }else{
                    $this->success('获取失败，请刷新后重试！');
                }
            }
        }
    }

    // 判断商品是否库存为0
    private function IsSoldOut($param = array())
    {
       if (!empty($param['aid'])) {
            if (!empty($param['spec_value_id'])) {
                $SpecWhere = [
                    'aid'  => $param['aid'],
                    'lang' => $this->home_lang,
                    'spec_stock' => ['>',0],
                    'spec_value_id' => $param['spec_value_id'],
                ];
                $spec_stock = Db::name('product_spec_value')->where($SpecWhere)->getField('spec_stock');
                if (empty($spec_stock)) {
                    $data['code'] = -1; // 已售罄
                    $this->error('商品已售罄！', null, $data);
                }
                if ($spec_stock < $param['num']) {
                    $data['code'] = -1; // 库存不足
                    $this->error('商品最大库存：'.$spec_stock, null, $data);
                }
            } else {
                $archives_where = [
                    'arcrank' => array('egt','0'), //带审核稿件不查询
                    'aid'     => $param['aid'],
                    'lang'    => $this->home_lang,
                ];
                $stock_count = $this->archives_db->where($archives_where)->getField('stock_count');
                if (empty($stock_count)) {
                    $data['code'] = -1; // 已售罄
                    $this->error('商品已售罄！', null, $data);
                }
                if ($stock_count < $param['num']) {
                    $data['code'] = -1; // 库存不足
                    $this->error('商品最大库存：'.$stock_count, null, $data);
                }
            }
        }
    }
    
    public function comment()
    {
        
         if (IS_POST) {
            $post = input('post.');
                $post['add_time'] = time();
                $post['users_id'] = $this->users_id;
                $post['upload_img'] = implode(',',$post['upload_img']);
                $post['ip_address'] = clientIP();
                $comment = M('shop_order_comment')->where(array('product_id' => $post['product_id'],'details_id' => $post['details_id'],'order_code' => $post['order_code'],'users_id'=> $this->users_id))->find();
                
                if($post['content'] == '')
                {
                    $datas['status'] = -1;
                    $datas['msg'] = '请填写评论内容';
                    $datas['res'] = '';
                    echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                    exit; 
                }
               if($comment)
               {
                    $datas['status'] = -1;
                    $datas['msg'] = '您已评价过';
                    $datas['res'] = '';
                    echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                    exit; 
               }else
               {
                  $data['is_comment'] = 1;
                  $ress = M('shop_order_details')->where(array('details_id' => $post['details_id']))->save($data);
                //   dump($ress);die;
                  $res = M('shop_order_comment')->add($post);
               }
               if($res)
               {
                    $datas['status'] = 1;
                    $datas['msg'] = '评价成功';
                    $datas['res'] = '';
                    echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                    exit; 
               }
         }
        $details_id = input('param.details_id/d');
        // dump($details_id);die;
        if (empty($details_id)) $this->error('售后服务单不存在！'); 
          $data = $this->shop_model->GetOrderDetailsInfo($details_id, $this->users_id,$this->users);
        //   dump($data);die;
          $datas['status'] = 1;
            $datas['msg'] = '';
            $datas['res'] = $data;
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit; 
    }
    


    /*------陈风任---2021-1-12---售后服务(退换货)------开始------*/
    // 申请退换货服务
    public function after_service_apply()
    {
        if (IS_AJAX_POST) {
            $post = input('post.');
            if (empty($post))
            {
                $datas['status'] = -1;
                $datas['msg'] = '提交数据有误';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit; 
            }
            if (empty($post['service_type']))
            {
                $datas['status'] = -1;
                $datas['msg'] = '请选择服务类型';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit; 
            }
            if (empty($post['content']))
            {
                $datas['status'] = -1;
                $datas['msg'] = '请填写问题描述';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit; 
            }
            
            if ($post['service_type'] == 1) {
               if (empty($post['address']))
                {
                    $datas['status'] = -1;
                    $datas['msg'] = '请填写您的收货地址';
                    $datas['res'] = '';
                    echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                    exit;  
                }
                if (empty($post['consignee'])){
                    
                    $datas['status'] = -1;
                    $datas['msg'] = '请填写您的姓名';
                    $datas['res'] = '';
                    echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                    exit;  
                } 
                if (empty($post['mobile'])){
                    $datas['status'] = -1;
                    $datas['msg'] = '请填写您的手机号码';
                    $datas['res'] = '';
                    echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                    exit;  
                }
            }
            
            // dump($post);die;
            
            // $this->error('请填写您的手机号码！');
            $time = getTime();
            $data = [
                'users_id'    => $this->users_id,
                'address'     => $post['addrinfo'].' '.$post['address'],
                'upload_img'  => !empty($post['upload_img']) ? implode(',', $post['upload_img']) : '',
                'refund_balance' => '',
                'refund_point' => '',
                'refund_code' => 'HH' . $time . rand(10,100),
                'add_time'    => $time,
                'update_time' => $time,
            ];
            if (2 == $post['service_type'] && !empty($post['refund_price'])) $data['refund_code'] = 'TK' . $time . rand(10,100);
            $ServiceData = array_merge($post, $data);
            $ResultID = $this->shop_order_service_db->add($ServiceData);
            if (!empty($ResultID)) {
                /*更新订单明细表中对应商品为申请服务*/
                $update = [
                    'apply_service' => 1,
                    'update_time' => getTime()
                ];
                $this->shop_order_details_db->where('details_id', $post['details_id'])->update($update);
                /* END */

                /*添加订单服务记录*/
                $LogNote = 1 == $post['service_type'] ? '会员提交换货申请，待管理员审核！' : '会员提交退货申请，待管理员审核！';
                OrderServiceLog($ResultID, $post['order_id'], $this->users_id, 0, $LogNote);
                /* END */

                $url = url('user/Shop/after_service_details', ['service_id' => $ResultID]);
                
                $datas['status'] = 1;
                $datas['msg'] = '已申请，待审核';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit; 
                // $this->success('已申请，待审核', $url);
            } else {
                $this->error('申请失败！');
            }
        }

        $details_id = input('param.details_id/d');
        // dump($details_id);die;
        if (empty($details_id)) $this->error('售后服务单不存在！');

        // 查询订单中单个商品信息
        $data = $this->shop_model->GetOrderDetailsInfo($details_id, $this->users_id,$this->users);

        // 返回错误提示
        if (!empty($data['msg']) && isset($data['code']) && 0 == $data['code']) $this->error($data['msg']);
        
        // 商户收货信息
        $maddr  = getUsersConfigData('addr');
        
        // dump($maddr);die;
        
        $eyou = [
            'url'   => url('user/Shop/after_service_apply', ['_ajax'=>1]),
            'maddr' => $maddr,
            'field' => $data
        ];
        
        
        // dump($eyou);die;
            $datas['status'] = 1;
            $datas['msg'] = '';
            $datas['res'] = $eyou;
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit; 
        // dump($eyou);die;
        // $this->assign('eyou', $eyou);
        // return $this->fetch('users/shop_after_service_apply');
    }

    // 退货换服务列表
    public function after_service()
    {
        $order_code = input('param.order_code');
        $page = input('post.page');
        $limit = input('post.limit');
        $ServiceInfo = $this->shop_model->GetAllServiceInfo($this->users_id, $order_code,$page,$limit);
        $eyou = [
            'field' => [
               'service' => $ServiceInfo['Service'],
               'count' => $ServiceInfo['count'],
               'total_page' => $ServiceInfo['total_page'],
            ],
        ];
        // dump($eyou);die;
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $eyou;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit; 
        // dump($eyou);die;
        // $this->assign('eyou', $eyou);
        // return $this->fetch('users/shop_after_service');
    }

    // 申请服务详情
    public function after_service_details() {
        /*取消服务单*/
        if (IS_POST) {
            $param = input('param.');
            // dump($param);die;
            if (empty($param['service_id']))
            {
                   $data['status'] = -1;
                    $data['msg'] = '请选择需要取消的服务单';
                    $data['res'] = '';
                    echo json_encode($data,JSON_UNESCAPED_UNICODE);
                    exit;
            }
            // $this->error('请选择需要取消的服务单！');
            /*取消服务单*/
            $where = [
                'users_id'   => $this->users_id,
                'service_id' => $param['service_id'],
            ];
            $update = [
                'status' => 8,
                'update_time' => getTime(),
            ];
            $ResultID = $this->shop_order_service_db->where($where)->update($update);
            /* END */

            if (!empty($ResultID)) {
                /*更新订单明细表中对应商品为未申请服务*/
                $where = [
                    'users_id'   => $this->users_id,
                    'details_id' => $param['details_id']
                ];
                $update = [
                    'apply_service' => 0,
                    'update_time' => getTime()
                ];
                $this->shop_order_details_db->where($where)->update($update);
                /* END */

                /*添加记录单*/
                $param['users_id'] = $this->users_id;
                $param['status'] = 8;
                $this->shop_common->AddOrderServiceLog($param, 1);
                /* END */
                     $data['status'] = 1;
                    $data['msg'] = '取消成功';
                    $data['res'] = '';
                    echo json_encode($data,JSON_UNESCAPED_UNICODE);
                    exit;
                // $this->success('取消成功！');
            } else {
                   $data['status'] = -1;
                    $data['msg'] = '取消失败';
                    $data['res'] = '';
                    echo json_encode($data,JSON_UNESCAPED_UNICODE);
                    exit;
                // $this->error('取消失败！');
            }
        }
        /* END */

        $service_id = input('param.service_id/d');

        // 商户收货信息
        $maddr  = getUsersConfigData('addr');

        // 加载模板
        $eyou = [
            'maddr'      => $maddr,
            'field'      => $this->shop_model->GetServiceDetailsInfo($service_id, $this->users_id),
            'ServiceUrl' => url('user/Shop/after_service_update', ['_ajax' => 1]),
            'StatusArr'  => [6, 7, 8],
            'StatusLog'  => $this->shop_model->GetOrderServiceLog($service_id, $this->users),
        ];

            $data['status'] = 1;
            $data['msg'] = '';
            $data['res'] = $eyou;
            echo json_encode($data,JSON_UNESCAPED_UNICODE);
            exit;
        // $this->assign('eyou', $eyou);
        // return $this->fetch('users/shop_after_service_details');
    }

    // 更新服务单状态
    public function after_service_update()
    {
        if (IS_POST) {
            if (empty($this->users_id)) $this->redirect('user/Login/login');
            $post = input('post.');
            if (empty($post['delivery']['cost'])) $post['delivery']['cost'] = 0;
            $post['delivery']['time'] = date('Y-m-d h:i:s',time());

            /*查询条件*/
            $Where = [
                'users_id' => $this->users_id,
                'service_id' => $post['service_id'],
            ];
            /* END */

            /*更新数据*/
            $UpDate = [
                'status' => 4,
                'users_delivery' => serialize($post['delivery']),
                'update_time' => getTime(),
                'service_id' => $post['service_id'],
            ];
            
            // dump($UpDate);DIE;
            /* END */
            
            $ResultID = $this->shop_order_service_db->where($Where)->update($UpDate);
            if (!empty($ResultID)) {
                /*添加记录单*/
                $post['users_id'] = $this->users_id;
                $this->shop_common->AddOrderServiceLog($post, 1);
                /* END */

               $datas['status'] = 1;
               $datas['msg'] = '操作成功！';
               $datas['res'] =  '';
               echo json_encode($datas,JSON_UNESCAPED_UNICODE);
               exit;
                // $this->success('操作成功！');
            } else {
                   $datas['status'] = -1;
                   $datas['msg'] = '操作失败！';
                   $datas['res'] =  '';
                   echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                   exit;
                // $this->error('操作失败！');
            }
        }
    }
    
    
    //购物车列表
    public function cart_list()
    {
        
        $param = input('post.');
        // dump($param);die;
      $count = M('shop_cart')
        ->field('a.*,b.title,b.litpic,c.name as brand_name,c.litpic as brand_litpic,a.add_time,d.number,d.sample,d.delivery')
        ->alias('a')
        ->join('archives b','a.product_id=b.aid')
        ->join('product_brand c','b.brand_id=c.id')
        ->join('product_content d','d.aid=b.aid')
        ->where(array('a.users_id' => $this->users_id))
        // ->page($param['page'],$param['limit'])
        ->count();
        // dump($param);
       $data['total_page'] = ceil($count/$param['limit']);
      $data['count'] = $count;
      $shop_cart = M('shop_cart')
        ->field('a.*,b.title,b.litpic,c.name as brand_name,c.litpic as brand_litpic,a.add_time,d.number,d.sample,d.delivery')
        ->alias('a')
        ->join('archives b','a.product_id=b.aid')
        ->join('product_brand c','b.brand_id=c.id')
        ->join('product_content d','d.aid=b.aid')
        ->where(array('a.users_id' => $this->users_id))
        ->page($param['page'],$param['limit'])
        ->select();
        
        $level_discount = $this->users['level_discount'];
        $discount_price = $level_discount / 100;
        $spec_price = $users_price = $num_new = 0;
       foreach($shop_cart as $k => $v)
        {
        //   $shop_cart[$k]['total_price'] = $v['num']*$v['users_price'];
           $attr = M('shop_product_attr')
          ->field('a.attr_value,b.attr_name')
          ->alias('a')
          ->join('shop_product_attribute b','b.attr_id=a.attr_id', 'LEFT')
          ->where(array('a.aid' => $v['product_id']))
          ->select();
          foreach($attr as $kk => $vv)
          {
              if($vv['attr_name'] == '参考封装')
              {
                  $shop_cart[$k]['package1'] = $vv['attr_value'];
              }
             if($vv['attr_name'] == '封装/规格')
              {
                  $shop_cart[$k]['specifications1'] = $vv['attr_value'];
              }
          }
          $rest = $this->num($v['product_id'],$v['product_num']);
        //   DUMP($rest);DIE;
          $shop_cart[$k]['spec_price'] =  sprintf("%.2f",$rest['spec_price']*$discount_price);
          $product_id  = sprintf("%.2f", $rest['spec_price']*$discount_price);
          $shop_cart[$k]['total_price'] = sprintf("%.2f",$v['product_num'] * $product_id);
          
        }
        
        // dump($discount_price);die;
            $data['status'] = 1;
            $data['msg'] = '';
            $data['res'] = $shop_cart;
            // dump($data);die;
            echo json_encode($data,JSON_UNESCAPED_UNICODE);
            exit(); 
    }
    
    
    
    
    //收货地址
    public function addlist()
    {
        $address = M('shop_address')->where(array('users_id' =>$this->users_id))->select();
        foreach($address as $k => $v)
        {
            $address[$k]['province'] = get_province_name($v['province']);
            $address[$k]['city']     = get_city_name($v['city']);
            $address[$k]['district'] = get_area_name($v['district']); 
        }
        
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $address;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit; 
    }
    
    //订单列表页面
    public function order_list()
    {
        $param = input('post.');
        if(empty($param['address']))
        {
            $data['status'] = -1;
            $data['msg'] = '请选择地址';
            $data['res'] = '';
            echo json_encode($data,JSON_UNESCAPED_UNICODE);
            exit(); 
        }
       $shop_cart = M('shop_cart')
        ->field('a.*,b.title,b.litpic,c.name as brand_name,c.litpic as brand_litpic,a.add_time,d.number,d.sample,d.delivery,d.weight')
        ->alias('a')
        ->join('archives b','a.product_id=b.aid')
        ->join('product_brand c','b.brand_id=c.id')
        ->join('product_content d','d.aid=b.aid')
        ->where(array('a.users_id' => $this->users_id,'a.selected' => 1))
        ->select();
       if(empty($shop_cart))
       {
            $data['status'] = -1;
            $data['msg'] = '您购物车没有选中的商品';
            $data['res'] = '';
            echo json_encode($data,JSON_UNESCAPED_UNICODE);
            exit();  
       }
       
       $shop_address = M('shop_address')->where(array('addr_id' => $param['address']))->find();
       
       $shop_address['country']  = '中国';
       $shop_address['province'] = get_province_name($shop_address['province']);
       $shop_address['city']     = get_city_name($shop_address['city']);
       $shop_address['district'] = get_area_name($shop_address['district']);
       
        $level_discount = $this->users['level_discount'];
        $discount_price = $level_discount / 100;
        $spec_price = $users_price = $num_new = 0;
        $wergth = 0;
        
        // dump($shop_cart);die;
       foreach($shop_cart as $k => $v)
        {
        //   $shop_cart[$k]['total_price'] = $v['num']*$v['users_price'];
           $attr = M('shop_product_attr')
          ->field('a.attr_value,b.attr_name')
          ->alias('a')
          ->join('shop_product_attribute b','b.attr_id=a.attr_id', 'LEFT')
          ->where(array('a.aid' => $v['product_id']))
          ->select();
          foreach($attr as $kk => $vv)
          {
              if($vv['attr_name'] == '参考封装')
              {
                  $shop_cart[$k]['package1'] = $vv['attr_value'];
              }
             if($vv['attr_name'] == '封装/规格')
              {
                  $shop_cart[$k]['specifications1'] = $vv['attr_value'];
              }
          }
          $rest = $this->num($v['product_id'],$v['product_num']);
          $shop_cart[$k]['spec_price'] =  sprintf("%.2f",$rest['spec_price']*$discount_price);
          $product_id  = sprintf("%.2f", $rest['spec_price']*$discount_price);
          $shop_cart[$k]['total_price'] = sprintf("%.2f",$v['product_num'] * $product_id);
          $wergth += $v['weight'];
          $total_weight += $v['product_num'] * $v['weight'];
          
        //   dump($v);die;
        //   dump($total_weight);die;
          $total_price +=  sprintf("%.2f",$v['product_num'] * $product_id);
        }
        
       $template = M('shop_shipping_template')->where(array('template_region' => $shop_address['province']))->find();
       
       $shop_open_shipping = getUsersConfigData('shop.shop_open_shipping');
    //   dump($shop_open_shipping);die;
       
       if($shop_open_shipping == 0)
       {
           $freight = 0;
       }else
       {
        //   dump($total_price);
        //   dump($template['template_money']);die;
            if((string)$total_price > $template['template_money'])
           {
              $freight = 0;
           }
           else
           {
               
            //   dump($total_weight);die;
               if($total_weight < 1)
               {
                   $freight = $template['first_weight'];
               }else
               {
                   $total_weight -= 1;
                //   dump($wergth);die;
                  $price = $total_weight * $template['continued_weight'];
                  $freight = $price + $template['first_weight'];
               }
           }  
       }
       
       $data['freight'] = $freight;   //运费
       $data['shop_address'] = $shop_address;   //地址
       $data['shop_cart'] = $shop_cart;   //产品
       
    //   DUMP($data);die;
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $data;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit; 
    }
    
    //订单列表优惠劵
    public function order_coupon()
    {
       $time = date('Y-m-d h:i:s',time());
    //   dump($time);die;
       $coupon = M('shop_coupon_use')
       ->field('b.*')
       ->alias('a')
       ->join('shop_coupon b','b.coupon_code= a.coupon_code')
       ->where(array('a.users_id' => $this->users_id,'a.use_status' => 0))
       ->where('a.end_time > '.time())
       ->select();
    //   dump($coupon);die;
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $coupon;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit; 
    }
    
    public function use_coupon()
    {
        $post = input('post.');
        $res = M('shop_coupon')->where(array('coupon_id' => $post['coupon_id']))->find();
        if(!empty($res))
        {
            if($res['coupon_type'] == 1)
            {
                $datas['status'] = 1;
                $datas['msg'] = '成功';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;   
            }
            if($res['coupon_type'] == 2)
            {
                if(in_array($res['product_id'],$post['product_id']))
                {
                    $datas['status'] = 1;
                    $datas['msg'] = '成功';
                    $datas['res'] = '';
                    echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                    exit;  
                }
            }
            
            if($res['coupon_type'] == 3)
            {
               $arct = M('arctype')->where(array('topid' => $res['arctype_id']))->getField('id', true);
               $arctes = M('archives') -> where(array('aid' => array('in',$post['product_id'])))->getField('typeid', true);
               $result = array_intersect($arct, $arctes);
               if(!empty($result))
               {
                    $datas['status'] = 1;
                    $datas['msg'] = '成功';
                    $datas['res'] = '';
                    echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                    exit;  
               }else
               {
                    $datas['status'] = -1;
                    $datas['msg'] = '该系列产品无法使用此优惠劵';
                    $datas['res'] = '';
                    echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                    exit;  
               }
            
            }
            
        }else
        {
            $datas['status'] = -1;
            $datas['msg'] = '没有该优惠劵';
            $datas['res'] = '';
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit; 
        }

    }
    
    public function recommend()
    {
       $order_code = input('post.order_code');
       $order = M('shop_order')
       ->field('b.*,a.order_id')
       ->alias('a')
       ->join('shop_order_details b','b.order_id = a.order_id')
       ->where(array('a.users_id' => $this->users_id,'a.order_code' => $order_code))
       ->whereor('a.order_status = 1 or a.order_status = 2 or a.order_status = 3')
       ->order('a.order_id desc')
       ->find();
     $product = M('archives')->where(array('aid' => $order['product_id']))->find();
     $arc = M('archives')->where(array('aid' => array('in',$product['like_product'])))->limit(0,3)->select();
     
     foreach($arc as $k => $v)
     {
           $arc[$k]['specifications_id'] = '';
           $arc[$k]['product_num'] = '1';
           $arc[$k]['product_type'] = '个';
           $collection = M('product_collection')->where(array('users_id' =>  $this->users_id,'product_id' => $v['aid']))->find();
           
           if(empty($collection))
           {
               $arc[$k]['collection'] = 0;
           }else
           {
                $arc[$k]['collection'] = 1;
           }
           
          $spe =  M('specifications')->where(array('product_id' => $v['aid']))->select();
          foreach ($spe as $kk => $vv)
          {
              $spe[$kk]['status'] = '';
          }
          $arc[$k]['specifications'] = $spe;
          $product_spec = M('product_spec_value')
          ->field('b.spec_value,a.spec_price,a.value_id,a.aid')
          ->alias('a')
          ->join('product_spec_data b','b.aid=a.aid and a.spec_value_id = b.spec_value_id', 'LEFT')
          ->where(array('a.aid' => $v['aid']))
          ->order('a.spec_price desc')
          ->select();
          $arc[$k]['product_spec'] = $product_spec;
          
         $attr = M('shop_product_attr')
          ->field('a.attr_value,b.attr_name')
          ->alias('a')
          ->join('shop_product_attribute b','b.attr_id=a.attr_id', 'LEFT')
          ->where(array('a.aid' => $v['aid']))
          ->select();
          foreach($attr as $kk => $vv)
          {
              if($vv['attr_name'] == '参考封装')
              {
                  $arc[$k]['package1'] = $vv['attr_value'];
              }
             if($vv['attr_name'] == '封装/规格')
              {
                  $arc[$k]['specifications1'] = $vv['attr_value'];
              }
               if($vv['attr_name'] == '包装/方式')
              {
                  $arc[$k]['packing1'] = $vv['attr_value'];
              }
          }
        }
        
        $datas['product_title'] = $product['title'];
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $arc;
        // dump($datas);die;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit;
    }
    
    public function voucher()
    {
       $post = input('post.');
    //   dump($post);die;
       $order = M('shop_order')->where(array('order_id' => $post['order_id'],'users_id' => $this->users_id))->find();
    //   dump($order);die;
       if(empty($order))
       {
            $datas['status'] = -1;
            $datas['msg'] = '该订单不存在';
            $datas['res'] = '';
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit; 
       }
       if($order['order_status'] == 4)
       {
            $datas['status'] = -1;
            $datas['msg'] = '该订单已过期';
            $datas['res'] = '';
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit; 
       }
      if($order['order_status'] == -1)
       {
            $datas['status'] = -1;
            $datas['msg'] = '改订单已取消';
            $datas['res'] = '';
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit; 
       }
       
      if($order['order_status'] == 1 || $order['order_status'] == 2 || $order['order_status'] == 3)
       {
            $datas['status'] = -1;
            $datas['msg'] = '该订单已付款';
            $datas['res'] = '';
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit; 
       }
       
       if(empty($post['money']))
       {
            $datas['status'] = -1;
            $datas['msg'] = '您未填写金额';
            $datas['res'] = '';
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit; 
       }
       if(empty($post['voucher']))
       {
            $datas['status'] = -1;
            $datas['msg'] = '您还未上传支付凭证';
            $datas['res'] = '';
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit; 
       }
       
       $data = array(
           'users_id' => $this->users_id,
           'order_id' => $post['order_id'],
           'order_code' => $order['order_code'],
           'money' => $post['money'],
           'company' => $post['company'],
           'currency' => $post['currency'],
           'remarks' => $post['remarks'],
           'voucher' => $post['voucher'],
           'add_time' => time()
           );
       $voucher = M('pay_voucher')->add($data);
       if($voucher)
       {
        //   $orders['pay_time'] = time();
          $orders['pay_name'] = 'voucher';
          $orders['payment_method'] = 2;
        //   $orders['pay_voucher_id'] = $voucher;
          $shop_order = M('shop_order')->where(array('order_id' => $post['order_id']))->save($orders);
        //   dump($shop_order);die;
           if($shop_order)
           {
                $datas['status'] = 1;
                $datas['msg'] = '提交成功';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;  
           }
       }
       
    }
    
    
    public function num($product_id,$num)
    {
        if(empty($num))
        {
            $data['code'] = -1;
            $data['smg'] = '商品数量最少为1';
            echo json_encode($data,JSON_UNESCAPED_UNICODE);
            exit(); 
        }
        if($num>=1 && $num<10)
        {
           $res = M('product_spec_data')->where(array('aid' => $product_id,'spec_value' => '1+'))->find();
        }
        if($num>=10 && $num<100)
        {
           $res = M('product_spec_data')->where(array('aid' => $product_id,'spec_value' => '10+'))->find();
        }
       if($num>=100 && $num<500)
        {
           $res = M('product_spec_data')->where(array('aid' => $product_id,'spec_value' => '100+'))->find();
        }
       if($num>=500 && $num<1000)
        {
           $res = M('product_spec_data')->where(array('aid' => $product_id,'spec_value' => '500+'))->find();
        }
       if($num>=1000 && $num<5000)
        {
           $res = M('product_spec_data')->where(array('aid' => $product_id,'spec_value' => '1000+'))->find();
        }
       if($num>=5000 && $num<10000)
        {
           $res = M('product_spec_data')->where(array('aid' => $product_id,'spec_value' => '5000+'))->find();
        }
        if($num>=10000 && $num<20000)
        {
           $res = M('product_spec_data')->where(array('aid' => $product_id,'spec_value' => '10000+'))->find();
        }
        if($num>=20000 && $num<50000)
        {
           $res = M('product_spec_data')->where(array('aid' => $product_id,'spec_value' => '20000+'))->find();
        }
        if($num>=50000 && $num<100000)
        {
           $res = M('product_spec_data')->where(array('aid' => $product_id,'spec_value' => '50000+'))->find();
        }
        if($num>=100000 && $num<1000000000)
        {
           $res = M('product_spec_data')->where(array('aid' => $product_id,'spec_value' => '100000+'))->find();
        }
        if(empty($res))
        {
           $rest = M('product_spec_value')->where(array('aid' => $product_id))->order('spec_price asc')->find();
            // dump($spec);die;
        }else
        {
           $rest =  M('product_spec_value')->where(array('spec_value_id' => $res['spec_value_id'],'aid' => $product_id))->find();
            // dump($res);die;
        }
        return $rest;
    }
    
    
        /**
     * 获取订单列表数据
     */
    public function getSporderlist($pagesize = '10')
    {
        // dump(11);die;
        $pagesize = input('post.limit');
        // 基础查询条件
        $OrderWhere = [
            'users_id' => $this->users_id,
            'lang'     => $this->home_lang,
        ];

// dump($this->users_id);die;
        // 应用搜索条件
        $keywords = input('param.keywords/s');
        // dump(11);die;
        if (!empty($keywords)) $OrderWhere['order_code'] = ['LIKE', "%{$keywords}%"];

        // 订单状态搜索
        $select_status = input('param.select_status');
        if (!empty($select_status)) {
            if ('daifukuan' === $select_status) $select_status = 0;
            if (3 == $select_status) $OrderWhere['is_comment'] = 0;
            $OrderWhere['order_status'] = $select_status;
        }

        // 分页查询逻辑
        $paginate_type = isMobile() ? 'usersmobile' : 'userseyou';
        $query_get = input('get.');
        $paginate = array(
            'type'     => $paginate_type,
            'var_page' => config('paginate.var_page'),
            'query'    => $query_get,
        );

        $pages = Db::name('shop_order')
            ->field("*")
            ->where($OrderWhere)
            ->order('add_time desc')
            ->paginate($pagesize, false, $paginate);
        $result['list']  = $pages->items();
        $result['pages'] = $pages;

        // 搜索名称时，查询订单明细表商品名称
        if (empty($result['list']) && !empty($keywords)) {
            $Data = model('Shop')->QueryOrderList($pagesize, $this->users_id, $keywords, $query_get);
            $result['list']  = $Data['list'];
            $result['pages'] = $Data['pages'];
        }

        /*规格值ID预处理*/
        $SpecValueArray = Db::name('product_spec_value')->field('aid,spec_value_id')->select();
        $SpecValueArray = group_same_key($SpecValueArray, 'aid');
        $ReturnData  = [];
        foreach ($SpecValueArray as $key => $value) {
            $ReturnData[$key] = [];
            foreach ($value as $kk => $vv) {
                array_push($ReturnData[$key], $vv['spec_value_id']);
            }
        }
        /* END */
        
        $level_discount = $this->users['level_discount'];
        $discount_price = $level_discount / 100;
        $spec_price = $users_price = $num_new = 0;

        if (!empty($result['list'])) {
            // 订单数据处理
            $controller_name = 'Product';
            // 获取当前链接及参数，用于手机端查询快递时返回页面
            $OrderIds = [];
            $ReturnUrl = request()->url(true);
            foreach ($result['list'] as $key => $value) {
                $DetailsWhere['a.users_id'] = $value['users_id'];
                $DetailsWhere['a.order_id'] = $value['order_id'];
                // 查询订单明细表数据
                $result['list'][$key]['details'] = Db::name('shop_order_details')->alias('a')
                    ->field('a.*, b.service_id, c.is_del,b.status as service_status')
                    ->join('__SHOP_ORDER_SERVICE__ b', 'a.details_id = b.details_id', 'LEFT')
                    ->join('__ARCHIVES__ c', 'a.product_id = c.aid', 'LEFT')
                    ->order('a.product_price desc, a.product_name desc')
                    ->where($DetailsWhere)
                    ->select();
                    // dump($result['list'][$key]['details']);
                $array_new = get_archives_data($result['list'][$key]['details'], 'product_id');
                
                if($value['pay_name'] == 'voucher')
                {
                   $rest = M('pay_voucher')->where(array('order_code' => $value['order_code']))->find();
                   if(!empty($rest))
                   {
                       $result['list'][$key]['voucher_status'] = 1;
                   }
                }else
                {
                    $result['list'][$key]['voucher_status'] = 0;
                }
                
                // dump($value);die;
                

                foreach ($result['list'][$key]['details'] as $kk => $vv) {
                    
                     $rest = $this->num($vv['product_id'],$vv['num']);
                     $product_id  = sprintf("%.2f", $rest['spec_price']*$discount_price);
                    //  dump($product_id);die;
                     $result['list'][$key]['details'][$kk]['product_price'] = $product_id;
                     
                     $result['list'][$key]['details'][$kk]['total_price'] =  sprintf("%.2f",$vv['num'] * $product_id);
                     
                     $result['list'][$key]['details'][$kk]['service_status'] =  Config::get('global.order_service_status')[$vv['service_status']];
                    // dump($vv);die;
                    // 产品规格处理
                    if (!in_array($vv['order_id'], $OrderIds) && 0 == $value['order_status']) {
                        $spec_value_id = unserialize($vv['data'])['spec_value_id'];
                        if (!empty($spec_value_id)) {
                            if (!in_array($spec_value_id, $ReturnData[$vv['product_id']])) {
                                // 用于更新订单数据
                                array_push($OrderIds, $vv['order_id']);
                                // 修改循环内的订单状态进行逻辑计算
                                $value['order_status'] = 4;
                                // 修改主表数据，确保输出数据正确
                                $result['list'][$key]['order_status'] = 4;
                                // 用于追加订单操作记录
                                $OrderIds_[]['order_id'] = $vv['order_id'];
                            }
                        }
                    }

                    // 产品内页地址
                    if (!empty($array_new[$vv['product_id']]) && 0 == $vv['is_del']) {
                        // 商品存在
                        $arcurl = urldecode(arcurl('home/'.$controller_name.'/view', $array_new[$vv['product_id']]));
                        $has_deleted = 0;
                        $msg_deleted = '';
                    } else {
                        // 商品不存在
                        $arcurl = urldecode(url('home/View/index', ['aid'=>$vv['product_id']]));
                        $has_deleted = 1;
                        $msg_deleted = '[商品已停售]';
                    }
                    $result['list'][$key]['details'][$kk]['arcurl'] = $arcurl;
                    $result['list'][$key]['details'][$kk]['has_deleted'] = $has_deleted;
                    $result['list'][$key]['details'][$kk]['msg_deleted'] = $msg_deleted;

                    // 图片处理
                    $result['list'][$key]['details'][$kk]['litpic'] = handle_subdir_pic(get_default_pic($vv['litpic']));

                    // 申请退换货
                    $result['list'][$key]['details'][$kk]['ApplyService'] = urldecode(url('user/Shop/after_service_apply', ['details_id' => $vv['details_id']]));

                    // 查看售后详情单
                    $result['list'][$key]['details'][$kk]['ViewAfterSale'] = urldecode(url('user/Shop/after_service_details', ['service_id' => $vv['service_id']]));

                    // 商品评价
                    $result['list'][$key]['details'][$kk]['CommentProduct'] = urldecode(url('user/ShopComment/product', ['details_id' => $vv['details_id']]));
                    
                    if(empty($vv['service_id']))
                    {
                        $result['list'][$key]['apply_service'] = 0;
                    }else
                    {
                        // dump(11);die;
                         $result['list'][$key]['service_status'] =  Config::get('global.order_service_status')[$vv['service_status']];
                         $result['list'][$key]['service_id'] = $vv['service_id'];
                         $result['list'][$key]['apply_service'] = 1;
                    }
                    
                }

                if (empty($value['order_status'])) {
                    // 付款地址处理，对ID和订单号加密，拼装url路径
                    // $querydata = [
                    //     'order_id'   => $value['order_id'],
                    //     'order_code' => $value['order_code']
                    // ];
                    // /*修复1.4.2漏洞 -- 加密防止利用序列化注入SQL*/
                    // $querystr = '';
                    // foreach($querydata as $_qk => $_qv)
                    // {
                    //     $querystr .= $querystr ? "&$_qk=$_qv" : "$_qk=$_qv";
                    // }
                    // $querystr = str_replace('=', '', mchStrCode($querystr));
                    // $auth_code = tpCache('system.system_auth_code');
                    // $hash = md5("payment".$querystr.$auth_code);
                    // /*end*/
                    // $result['list'][$key]['PaymentUrl'] = urldecode(url('user/Pay/pay_recharge_detail', ['querystr'=>$querystr,'hash'=>$hash]));

                    // 付款地址处理，对ID和订单号加密，拼装url路径
                    $Paydata = [
                        'order_id'   => $value['order_id'],
                        'order_code' => $value['order_code']
                    ];

                    // 先 json_encode 后 md5 加密信息
                    $Paystr = md5(json_encode($Paydata));

                    // 清除之前的 cookie
                    Cookie::delete($Paystr);

                    // 存入 cookie
                    cookie($Paystr, $Paydata);

                    // 跳转链接
                    $result['list'][$key]['PaymentUrl'] = urldecode(url('user/Pay/pay_recharge_detail',['paystr'=>$Paystr]));
                }
                
                    $order_status_arr = Config::get('global.order_status_arr');
                        if(empty($result['list'][$key]['details'][0]['service_id']))
                        {
                               $result['list'][$key]['order_status_name'] = $order_status_arr[$value['order_status']];
                        }else
                        {
                            
                            $result['list'][$key]['order_status_name'] = $order_status_arr[$value['order_status']];
                        //   $service_id = M('shop_order_service')->where(array('service_id' => $result['list'][$key]['details'][0]['service_id']))->find();
                        //   if($service_id['status'] == 1)
                        //   {
                        //         $result['list'][$key]['order_status_name'] = '换货审核中';
                        //   }elseif($service_id['status'] == 2)
                        //   {
                        //         $result['list'][$key]['order_status_name'] = '换货审核通过';
                        //   }elseif($service_id['status'] == 3)
                        //   {
                        //         $result['list'][$key]['order_status_name'] = '换货审核不通过';
                        //   }elseif($service_id['status'] == 4)
                        //   {
                        //         $result['list'][$key]['order_status_name'] = '订单发货中';
                        //   }elseif($service_id['status'] == 5)
                        //   {
                        //         $result['list'][$key]['order_status_name'] = '订单收货';
                        //   }elseif($service_id['status'] == 6)
                        //   {
                        //         $result['list'][$key]['order_status_name'] = '退货完成';
                        //   }
                            // dump($result['list'][$key]['details'][0]['service_id']);die;
                            //   $result['list'][$key]['order_status_name'] = '退换货';
                        }
                        
                        if($value['order_status'] == 3)
                        {
                              $result['list'][$key]['order_status_name'] = $order_status_arr[$value['order_status']];
                        }
                // dump($value);die;
                // 获取订单状态
                

                // 获取订单支付方式名称
                $pay_method_arr = Config::get('global.pay_method_arr');
                if (!empty($value['payment_method']) && !empty($value['pay_name'])) {
                    $result['list'][$key]['pay_name'] = !empty($pay_method_arr[$value['pay_name']]) ? $pay_method_arr[$value['pay_name']] : '第三方支付';
                } else {
                    if (!empty($value['pay_name'])) {
                        $result['list'][$key]['pay_name'] = !empty($pay_method_arr[$value['pay_name']]) ? $pay_method_arr[$value['pay_name']] : '第三方支付';
                    } else {
                        $result['list'][$key]['pay_name'] = '在线支付';
                    }
                }

                // 封装订单查询详情链接
                $result['list'][$key]['OrderDetailsUrl'] = urldecode(url('user/Shop/shop_order_details',['order_id'=>$value['order_id']]));

                // 封装订单催发货JS
                $result['list'][$key]['OrderRemind'] = " onclick=\"OrderRemind('{$value['order_id']}','{$value['order_code']}');\" ";
                 
                // 封装确认收货JS
                $result['list'][$key]['Confirm'] = " onclick=\"Confirm('{$value['order_id']}','{$value['order_code']}');\" ";

                // 封装查询物流链接
                $result['list'][$key]['LogisticsInquiry'] = $MobileExpressUrl = '';
                if (('2' == $value['order_status'] || '3' == $value['order_status']) && empty($value['prom_type'])) {
                    // 物流查询接口
                    if (isMobile()) {
                        $ExpressUrl = "https://m.kuaidi100.com/index_all.html?type=".$value['express_code']."&postid=".$value['express_order']."&callbackurl=".$ReturnUrl;
                    } else {
                        $ExpressUrl = "https://www.kuaidi100.com/chaxun?com=".$value['express_code']."&nu=".$value['express_order'];
                    }
                    // 微信端、小程序使用跳转方式进行物流查询
                    $result['list'][$key]['MobileExpressUrl'] = $ExpressUrl;
                    // PC端，手机浏览器使用弹框方式进行物流查询
                    $result['list'][$key]['LogisticsInquiry'] = " onclick=\"LogisticsInquiry('{$ExpressUrl}');\" ";
                }
                
                $users = M('users')->where(array('users_id' => $this->users_id))->find();
                $level = M('users_level')->where(array('level_id' => $users['level']))->find();
                $result['list'][$key]['level_discount'] = $level['discount'];

                // 默认为空
                $result['list'][$key]['hidden'] = '';
            }

            // 更新产品规格异常的订单，更新为订单过期
            if (!empty($OrderIds)) {
                // 更新订单
                $UpData = [
                    'order_status' => 4,
                    'update_time'  => getTime()
                ];
                Db::name('shop_order')->where('order_id', 'IN', $OrderIds)->update($UpData);
                // 追加订单操作记录
                AddOrderAction($OrderIds_, $this->users_id, 0, 4, 0, 0, '订单过期！', '规格更新后部分产品规格不存在，订单过期！');
            }

// dump($result);die;
            // 传入JS参数
            $data['shop_member_confirm'] = url('user/Shop/shop_member_confirm');
            $data['shop_order_remind']   = url('user/Shop/shop_order_remind');
            $data['status'] = 1;
            $data['mag'] = '';
            $data['res'] = $result;
            echo json_encode($result,JSON_UNESCAPED_UNICODE);
            exit();
        }
    }
    
    
    
    
    
    
    /*------陈风任---2021-1-12---售后服务(退换货)------结束------*/
}