<?php
/**
 * 易优CMS
 * ============================================================================
 * 版权所有 2016-2028 海南赞赞网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.eyoucms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 陈风任 <491085389@qq.com>
 * Date: 2019-1-25
 */

namespace app\user\controller;
use weapp\Ask\model\AskTypeModel;

use think\Db;
use think\Config;
use think\Page;
use think\Verify;
use app\user\logic\SmtpmailLogic;
use app\plugins\logic\AskLogic;

class Users extends Base
{
    public $smtpmailLogic;

    public function _initialize()
    {
        parent::_initialize();
        $this->smtpmailLogic      = new SmtpmailLogic;
        $this->users_db           = Db::name('users');      // 会员数据表
        $this->users_level_db     = Db::name('users_level'); // 会员等级表
        $this->users_parameter_db = Db::name('users_parameter'); // 会员属性表
        $this->users_list_db      = Db::name('users_list'); // 会员属性信息表
        $this->users_config_db    = Db::name('users_config');// 会员配置表
        $this->users_money_db     = Db::name('users_money');// 会员金额明细表
        $this->smtp_record_db     = Db::name('smtp_record');// 发送邮箱记录表
        $this->sms_log_db         = Db::name('sms_log');// 发送手机记录表
        
        $this->weapp_ask_db = Db::name('weapp_ask'); // 问题表
        $this->weapp_ask_answer_db = Db::name('weapp_ask_answer'); // 答案表
        $this->weapp_ask_type_db = Db::name('weapp_ask_type'); // 问题栏目分类表
        $this->weapp_ask_answer_like_db = Db::name('weapp_ask_answer_like'); // 问题回答点赞表
        $this->AskLogic = new AskLogic;
        
        // 微信配置信息
        $this->pay_wechat_config = unserialize(getUsersConfigData('pay.pay_wechat_config'));
    }
    
    // 会员中心首页
    public function index()
    {
        if (1 == config('global.opencodetype')) {
            return action('user/Users/index2');
        }
        
        if (getUsersTplVersion() == 'v1') {
            return action('user/Users/info');
        }

        $result = [];
        // 资料信息
        $result['users_para'] = model('Users')->getDataParaList($this->users_id);
        //  $users = M('users')->where(array('users_id' => $this->users_id))->find();
        //  $result['users'] = array(
        //      'password'  => '******',
        //      'phone'     => $users['mobile'],
        //      'nickname'  => $users['nickname'],
        //      'head_pic'  => $users['head_pic'],
        //      'email'     => $users['email'],
        //      'username'  => $users['username'],
        //      'users_id'  => $users['users_id'],
        //      );
             
        //      $result['address'] = M('shop_address')->where(array('users_id' => $this->users_id))->select();
        //      foreach ($address as $k => $v)
        //      {     
        //         $result['address'][$k]['country']  = '中国';
        //         $result['address'][$k]['province'] = get_province_name($address['province']);
        //         $result['address'][$k]['city']     = get_city_name($address['city']);
        //         $result['address'][$k]['district'] = get_area_name($address['district']);
        //      }
             
        //     $result['identity'] = M('users_identity')->where(array('users_id' => $this->users_id))->find();
        //     foreach($result['identity'] as $kk => $vv)
        //     {
        //         $result['identity'][$k]['picture'] = explode(",", $vv['picture']);
        //     }
         
       // dump($address);die;
        // $this->assign('users_para', $result['users_para']);

        $eyou = array(
            'field' => $result,
        );
        $this->assign('eyou', $eyou);

        //其他数据
        $others = array();
        $users_id = $this->users_id;
        //收藏数
        $others['collect_num'] = Db::name('users_collection')->where(['users_id'=>$users_id])->count("id");
        //足迹
        $others['footprint_num'] = Db::name('users_footprint')->where(['users_id'=>$users_id])->count('id');
        //今日签到信息
        $others['signin_conf'] = getUsersConfigData('score');
        if ($others['signin_conf'] && isset($others['signin_conf']['score_signin_status']) && $others['signin_conf']['score_signin_status'] == 1) {
            $now_time = time();
            $today_start = mktime(0,0,0,date("m",$now_time),date("d",$now_time),date("Y",$now_time));
            $today_end = mktime(23,59,59,date("m",$now_time),date("d",$now_time),date("Y",$now_time));
            $others['signin_info'] = Db::name('users_signin')->where(['users_id'=>$users_id,'add_time'=>['BETWEEN',[$today_start,$today_end]]])->value("id");
        }

        //查询插件信息
        $weapp_menu_info = Db::name('users_menu')->field("id,title,version,mca")->where(['version'=>'weapp','status'=>1])->select();
        $others['weapp_menu_info'] = [];
        if ($weapp_menu_info) {
            foreach ($weapp_menu_info as $k=>$v) {
                preg_match_all('/\/(\w+)\//i', $v['mca'],$preg_res);
                if (!empty($preg_res[1])) {
                    $code_str = $preg_res[1][0];
                    $weapp_info = Db::name('weapp')->field("code,name,config")->where(['code'=>$code_str])->find();
                    if ($weapp_info) {
                        $weapp_menu_info[$k]['litpic'] = json_decode($weapp_info['config'],true)['litpic'];
                    }else{
                        unset($weapp_menu_info[$k]);
                    }
                }
            }
            $others['weapp_menu_info'] = $weapp_menu_info;
        }
        $this->assign('others', $others);

        //查询部分模型开启信息  下载 视频 问答
        $part_channel = Db::name('channeltype')
            ->where('nid','in',['ask','download','media','article'])
            ->field('nid,status,data')
            ->getAllWithIndex('nid');
        if (!empty($part_channel['article']['data'])){
            $part_channel['article']['data'] = json_decode($part_channel['article']['data'], true);
        }
        $this->assign('part_channel', $part_channel);

        // 多语言
        $condition_bottom['a.lang'] = array('eq', $this->admin_lang);
        $condition_bottom['a.status'] = array('eq', 1);
        $condition_bottom['a.display'] = array('eq', 1);
        $bottom_menu_list = Db::name('users_bottom_menu')->field('a.*')
            ->alias('a')
            ->where($condition_bottom)
            ->order('a.sort_order asc, a.id asc')
            ->limit(4)
            ->select();

        $this->assign('bottom_menu_list', $bottom_menu_list);

        $clear_session_url = $this->root_dir."/index.php?m=api&c=Ajax&a=clear_session";
        $replace = <<<EOF
    <script type="text/javascript">
        clear_session();
        function clear_session()
        {
            $.ajax({
                url: "{$clear_session_url}",
                type: 'post',
                dataType: 'JSON',
                data: {_ajax: 1},
                success: function(res){
                }
            });
        }
    </script>
</body>
EOF;
        $html = $this->fetch('users_welcome');
        $html = str_ireplace('</body>', $replace, $html);
        
        return $html;
    }
    
    public function users_list()
    {
          if (1 == config('global.opencodetype')) {
            return action('user/Users/index2');
        }
        
        if (getUsersTplVersion() == 'v1') {
            return action('user/Users/info');
        }

        $result = [];
        // 资料信息
       //$result['users_para'] = model('Users')->getDataParaList($this->users_id);
         $users = M('users')->where(array('users_id' => $this->users_id))->find();
         $result['users'] = array(
             'password'  => '******',
             'phone'     => $users['mobile'],
             'nickname'  => $users['nickname'],
             'head_pic'  => $users['head_pic'],
             'email'     => $users['email'],
             'username'  => $users['username'],
             'users_id'  => $users['users_id'],
             'pid'       => $users['pid']
             );
            //  dump($this->users_id);die;
            //  $this->users_id = 8;
             $result['address'] = M('shop_address')->where(array('users_id' => $this->users_id))->select();
             foreach ($address as $k => $v)
             {     
                $result['address'][$k]['country']  = '中国';
                $result['address'][$k]['province'] = get_province_name($address['province']);
                $result['address'][$k]['city']     = get_city_name($address['city']);
                $result['address'][$k]['district'] = get_area_name($address['district']);
             }
             
            $result['identity'] = M('users_identity')->where(array('users_id' => $this->users_id))->find();
            
            if(empty($result['identity']))
            {
                $result['identity']['username'] = '';
                $result['identity']['company'] = '';
                $result['identity']['identity_id'] = '';
                $result['identity']['pay_taxes'] = '';
                $result['identity']['position'] = '';
                $result['identity']['picture'] = '';
                $result['identity']['email'] = '';
                $result['identity']['examine'] = '';
            }else
            {
                $result['identity']['picture'] = explode(",", $result['identity']['picture']);
            }
            $beginToday=mktime(0,0,0,date('m'),date('d'),date('Y'));
            $endToday=mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
            $wheres['add_time'] = array('between', array($beginToday,$endToday));
            
           $score = M('users_score')->where($wheres)->where(array('users_id' => $this->users_id,'type' => 5))->find();
        //   dump($score);die;
           if(!empty($score))
           {
              $result['users']['scores'] = 1;
           }else
           {
               $result['users']['scores'] = 0;
           }
            
            // dump($result);die;
            $data['status'] = 1;
            $data['msg'] = '';
            $data['res'] = $result;
            echo json_encode($result,JSON_UNESCAPED_UNICODE);
            
    }
    
    
    

    // 个人信息
    public function info()
    {
        $result = [];
        // 资料信息
        $result['users_para'] = model('Users')->getDataParaList($this->users_id);
        $this->assign('users_para', $result['users_para']);

        // 菜单名称
        $result['title'] = Db::name('users_menu')->where([
            'mca'  => 'user/Users/index',
            'lang' => $this->home_lang,
        ])->getField('title');

        $eyou = array(
            'field' => $result,
        );
        $this->assign('eyou', $eyou);

        $html = $this->fetch('users_centre');

        // 会员模板版本号
        if (getUsersTplVersion() == 'v1') {
            /*第三方注册的用户，无需修改登录密码*/
            if (!empty($this->users['thirdparty'])) {
                $html = str_ireplace('onclick="ChangePwdMobile();"', 'onclick="ChangePwdMobile();" style="display: none;"', $html);
                $html = str_ireplace('onclick="ChangePwd();"', 'onclick="ChangePwd();" style="display: none;"', $html);
            }
            /*end*/

            // 美化昵称输入框
            $html = str_ireplace('type="text" name="nickname"', 'type="text" name="nickname" class="input-txt"', $html);
        }

        return $html;
    }

    // 会员选择登陆方式界面
    public function users_select_login()
    {
        // 若存在则调转至会员中心
        if ($this->users_id > 0) {
            $this->redirect('user/Users/centre');
            exit;
        }
        // 跳转链接
        $referurl = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : url("user/Users/centre");
        session('eyou_referurl', $referurl);

        // 拼装url
        $result = [
            'wechat_url'  => url("user/Users/ajax_wechat_login"),
            'website_url' => $this->root_dir . "/index.php?m=user&c=Users&a=login&website=website",
        ];

        // 若为微信端并且开启微商城模式则重定向
        if (isWeixin() && !empty($this->usersConfig['shop_micro'])) {
            $WeChatLoginConfig = !empty($this->usersConfig['wechat_login_config']) ? unserialize($this->usersConfig['wechat_login_config']) : [];
            if (!empty($WeChatLoginConfig)) {
                $this->redirect($result['wechat_url']);
            }
        }

        // 若后台功能设置-登录设置中，微信端本站登录为关闭状态，则直接跳转到微信授权页面
        if (isset($this->usersConfig['users_open_website_login']) && empty($this->usersConfig['users_open_website_login'])) {
            $this->redirect($result['wechat_url']);
            exit;
        }

        // 数据加载
        $eyou = array(
            'field' => $result,
        );
        $this->assign('eyou', $eyou);
        return $this->fetch('users_select_login');
    }

    // 使用ajax微信授权登陆
    public function ajax_wechat_login()
    {
        $WeChatLoginConfig = !empty($this->usersConfig['wechat_login_config']) ? unserialize($this->usersConfig['wechat_login_config']) : [];
        // 微信授权登陆
        if (!empty($WeChatLoginConfig['appid']) && !empty($WeChatLoginConfig['appsecret'])) {
            if (isMobile() && isWeixin()) {
                // 判断登陆成功跳转的链接，若为空则默认会员中心链接并存入session
                $referurl = session('eyou_referurl');
                if (empty($referurl)) {
                    $referurl = url('user/Users/index', '', true, true);
                    session('eyou_referurl', $referurl);
                }

                // 获取微信配置授权登陆
                $appid     = $WeChatLoginConfig['appid'];
                $NewUrl    = urlencode(url('user/Users/get_wechat_info', '', true, true));
                $ReturnUrl = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" . $appid . "&redirect_uri=" . $NewUrl . "&response_type=code&scope=snsapi_userinfo&state=eyoucms&#wechat_redirect";

                if (isset($this->usersConfig['users_open_website_login']) && empty($this->usersConfig['users_open_website_login'])) {
                    $this->redirect($ReturnUrl);
                } else {
                    if (IS_AJAX_POST) {
                        $this->success('授权成功！', $ReturnUrl);
                    } else {
                        $this->redirect($ReturnUrl);
                    }
                }
            }
            $this->error('非手机端微信、小程序，不可以使用微信登陆，请选择本站登陆！');
        }
        $this->error('后台微信配置尚未配置AppSecret，不可以微信登陆，请选择本站登陆！');

    }

    // 授权之后，获取会员信息
    public function get_wechat_info()
    {
        $WeChatLoginConfig = !empty($this->usersConfig['wechat_login_config']) ? unserialize($this->usersConfig['wechat_login_config']) : [];

        // 微信配置信息
        $appid  = $WeChatLoginConfig['appid'];
        $secret = $WeChatLoginConfig['appsecret'];
        $code   = input('param.code/s');

        // 获取到会员openid
        $get_token_url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=' . $appid . '&secret=' . $secret . '&code=' . $code . '&grant_type=authorization_code';
        $data          = httpRequest($get_token_url);
        $WeChatData    = json_decode($data, true);
        if (empty($WeChatData) || (!empty($WeChatData['errcode']) && !empty($WeChatData['errmsg']))) {
            $this->error('AppSecret错误或已过期', $this->root_dir.'/');
        }
        
        // 查询这个openid是否已注册
        $where = [
            'open_id' => $WeChatData['openid'],
            'lang'    => $this->home_lang,
        ];
        $Users = $this->users_db->where($where)->find();
        if (!empty($Users)) {
            // 已注册
            $eyou_referurl = session('eyou_referurl');
            session('users_id', $Users['users_id']);
            session('users', $Users);
            session('eyou_referurl', '');
            cookie('users_id', $Users['users_id']);
            $this->redirect($eyou_referurl);
        } else {
            // 未注册
            $username = substr($WeChatData['openid'], 6, 8);
            // 查询用户名是否已存在
            $result = $this->users_db->where('username', $username)->count();
            if (!empty($result)) {
                $username = $username . rand('100,999');
            }
            // 获取会员信息
            $get_userinfo = 'https://api.weixin.qq.com/sns/userinfo?access_token=' . $WeChatData["access_token"] . '&openid=' . $WeChatData["openid"] . '&lang=zh_CN';
            $UserInfo     = httpRequest($get_userinfo);
            $UserInfo     = json_decode($UserInfo, true);
            if (empty($UserInfo['nickname']) && empty($UserInfo['headimgurl'])) {
                $this->error('用户授权异常，建议清理手机缓存再进行登录', $this->root_dir.'/');
            }

            // 新增会员和微信绑定
            $UsersData = [
                'username'       => $username,
                'nickname'       => $UserInfo['nickname'],
                'open_id'        => $WeChatData['openid'],
                'password'       => '', // 密码默认为空
                'last_ip'        => clientIP(),
                'reg_time'       => getTime(),
                'last_login'     => getTime(),
                'is_activation'  => 1, // 微信注册会员，默认开启激活
                'register_place' => 2, // 前台微信注册会员
                'login_count'    => Db::raw('login_count+1'),
                'head_pic'       => $UserInfo['headimgurl'],
                'lang'           => $this->home_lang,
            ];
            // 查询默认会员级别，存入会员表
            $level_id           = $this->users_level_db->where([
                'is_system' => 1,
                'lang'      => $this->home_lang,
            ])->getField('level_id');
            $UsersData['level'] = $level_id;

            $users_id = $this->users_db->add($UsersData);
            if (!empty($users_id)) {
                // 新增成功，将会员信息存入session
                $eyou_referurl = session('eyou_referurl');
                $GetUsers = $this->users_db->where('users_id', $users_id)->find();
                session('users_id', $GetUsers['users_id']);
                session('users', $GetUsers);
                session('eyou_referurl', '');
                cookie('users_id', $GetUsers['users_id']);
                $this->redirect($eyou_referurl);
            } else {
                $this->error('未知错误，无法继续！');
            }
        }
    }
    
    
    public function code()
    {
        // dump(11);die;
        if (IS_POST) {
            $param =  input('post.');
            
			$ip = clientIP();
			if($this->limitoftimes($ip,30,600) === false)
            {
                $data['status'] = -1;
                $data['msg'] = '您访问的次数过多';
                echo json_encode($data);
                exit();
            }
			if($this->limitoftimes($param['phone'],1,60)===false)
            {
                $data['status'] = -1;
                $data['msg'] = '一分钟内给您发送过了';
                echo json_encode($data);
                exit();
            }

            if($this->limitoftimes($param['phone'],3,600)===false)
            {
                $data['status'] = -1;
                $data['msg'] = '10分钟内最多只能发送3次';
                echo json_encode($data);
                exit();
			    //$this->error('10分钟内最多只能发送3次');
            }
            if($this->limitoftimes($param['phone'],20,'oneday') === false)
            {
                $data['status'] = -1;
                $data['msg'] = '今日验证码获取次数超过20次，请明日再试！';
                echo json_encode($data);
                exit();
            }
            
            if(preg_match('#^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,6,7,8]{1}\d{8}$|^18[\d]{9}$#', $param['phone'])){
                $param['model'] = 1;
            }else
            {
                $param['model'] = 4;
            }
            if(!empty($param['status']))
            {
                // dump($param);die;
                if($param['status'] == 6)
                {
                  $users =  M('users')->where(array('mobile' => $param['phone']))->find();
                  if (!empty($users))
                  {
                        $data['status'] = -1;
                        $data['msg'] = '该手机号码已绑定';
                        echo json_encode($data);
                        exit;
                  }
                   $param['model'] = 6;
                }
                if ($param['status'] == 4) 
                {
                    $users =  M('users')->where(array('mobile' => $param['phone']))->find();
                    if(empty($users))
                    {
                        $data['status'] = -1;
                        $data['msg'] = '该手机号码未注册';
                        echo json_encode($data);
                        exit;
                    }
                     $param['model'] = 4;
                }
                if ($param['status'] == 5) 
                {
                    $users =  M('users')->where(array('mobile' => $param['phone']))->find();
                    if(empty($users))
                    {
                        $data['status'] = -1;
                        $data['msg'] = '该手机号码未注册,请重新发送';
                        echo json_encode($data);
                        exit;
                    }
                     $param['model'] = 6;
                }
            }
            // dump($param);die;
            
            $res = sendSms($param['model'], $param['phone'], array('content' => mt_rand(1000, 9999)));
            if (intval($res['status']) == 1) {
                /*多语言*/
                if (is_language()) {
                    $langRow = \think\Db::name('language')->order('id asc')
                        ->cache(true, EYOUCMS_CACHE_TIME, 'language')
                        ->select();
                    foreach ($langRow as $key => $val) {
                        tpCache('sms', $param, $val['mark']);
                    }
                } else {
                    tpCache('sms', $param);
                }
                /*--end*/
              if (intval($res['status']) == 1) {
                  $data['status'] = 1;
                  $data['msg'] = '发送成功';
                 echo json_encode($data);
                 exit;
                $this->success('发送成功！');
                } else {
                    $this->error($res['msg']);
                }
            } else {
                $this->error($res['msg']);
            }
        }
    }
    
    
    public function users_password()
    {
         if (IS_POST) {
            $post  = input('post.');
            $ListWhere = array(
                'mobile' => array('eq', $post['phone']),
                'lang' => array('eq', $this->home_lang)
            );
        //     $ListData  = $this->users_db->where($ListWhere)->field('users_id')->find();
        //   // dump($ListData);die;
        //     if (empty($ListData))
        //     {
        //         $data['status'] = -1;
        //         $data['msg'] = '手机号码不存在，不能找回密码！';
        //         $data['res'] = '';
        //         echo json_encode($data);
        //         exit;  
        //     }
             $UsersWhere = array(
                'mobile' => array('eq', $post['phone']),
                'lang'   => array('eq', $this->home_lang)
            );
            $UsersData  = $this->users_db->where($UsersWhere)->field('mobile')->find();
            // dump($UsersData);die;
            if (empty($UsersData))
            {
                // dump(11);die;
                $data['status'] = -1;
                $data['msg'] = '手机号码未绑定，不能找回密码！';
                $data['res'] = '';
                echo json_encode($data);
                exit;  
            }
           $RecordWhere = [
                'mobile' => $post['phone'],
                'code'   => $post['code'],
                'lang'   => $this->home_lang
            ];
            $RecordData = $this->sms_log_db->where($RecordWhere)->field('is_use, add_time')->order('id desc')->find();
            // dump($RecordData);die;
            if (!empty($RecordData)) {
                $time = getTime();
                $RecordData['add_time'] += Config::get('global.mobile_default_time_out');
                if (1 == $RecordData['is_use'] || $RecordData['add_time'] <= $time) {
                    $data['status'] = -1;
                    $data['msg'] = '手机验证码已被使用或超时，请重新发送！';
                    $data['res'] = '';
                    echo json_encode($data);
                    exit; 
                    // $this->error('手机验证码已被使用或超时，请重新发送！');
                } else {
                    // 处理手机验证码
                    $RecordWhere = [
                        'source'   => 4,
                        'mobile'   => $post['phone'],
                        'is_use'   => 0,
                        'lang'     => $this->home_lang
                    ];
                    // 更新数据
                    $RecordData = [
                        'is_use'      => 1,
                        'update_time' => $time
                    ];
                    // dump($RecordData);die;
                    $this->sms_log_db->where($RecordWhere)->update($RecordData);
                    session('users_retrieve_password_mobile', $post['mobile']);
                    $data['status'] = 1;
                    $data['msg'] = '验证通过';
                    $data['res'] = '';
                    echo json_encode($data);
                    exit;
                    // $this->success('验证通过', url('user/Users/reset_password_mobile'));
                }
                //  $this->error('手机验证码不正确，请重新输入！');
            }else
            {
                $data['status'] = -1;
                $data['msg'] = '手机验证码不正确，请重新输入！';
                $data['res'] = '';
                echo json_encode($data);
                exit;  
            }

         }
    }
    
    public function users_pass()
    {
        if (IS_POST) {
            $post  = input('post.');
             $UsersWhere = array(
                'mobile' => array('eq', $post['phone']),
                'lang'   => array('eq', $this->home_lang)
            );
            $UsersData  = $this->users_db->where($UsersWhere)->field('mobile')->find();
            
            if (empty($UsersData))
            {
                $data['status'] = -1;
                $data['msg'] = '手机号码未绑定，不能找回密码！';
                $data['res'] = '';
               echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;  
            }
            $RecordWhere = [
                'mobile' => $post['phone'],
                'code'   => $post['code'],
                'lang'   => $this->home_lang
            ];
            $RecordData = $this->sms_log_db->where($RecordWhere)->field('is_use, add_time')->order('id desc')->find();
             if (empty($RecordData))
             {
                $data['status'] = -1;
                $data['msg'] = '手机验证码不正确，请重新输入！';
                $data['res'] = '';
               echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;
             }
             $time = getTime();
             $RecordData['add_time'] += Config::get('global.mobile_default_time_out');
             if (1 == $RecordData['is_use'] || $RecordData['add_time'] <= $time) {
                    $data['status'] = -1;
                    $data['msg'] = '手机验证码已被使用或超时，请重新发送！';
                    $data['res'] = '';
                   echo json_encode($data,JSON_UNESCAPED_UNICODE);
                    exit; 
              }
              
            $res = preg_match('/^(\w*(?=\w*\d)(?=\w*[A-Za-z])\w*){8,16}$/', $post['password']);
           if(!$res)
           {
                $data['status'] = -1;
                $data['msg'] = '密码格式不正确';
                 echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;
           }
            if (empty($post['password'])) {
                
                $data['status'] = -1;
                $data['msg'] = '新密码不能为空';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;
            } else if ($post['password'] != $post['password2']) {
                
                $data['status'] = -1;
                $data['msg'] = '重复密码与新密码不一致';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;
            }
             $users = $this->users_db->field('password')->where([
                'mobile' => $post['phone'],
                'lang'     => $this->home_lang,
            ])->find();
            if (!empty($users)) {
                $r = $this->users_db->where([
                    'mobile' => $post['phone'],
                    'lang'     => $this->home_lang,
                ])->update([
                    'password'    => func_encrypt($post['password']),
                    'update_time' => getTime(),
                ]);
                if ($r) {
                    $data['status'] = 1;
                    $data['msg'] = '修改成功';
                   echo json_encode($data,JSON_UNESCAPED_UNICODE);
                    exit;
                }
                    $data['status'] = -1;
                    $data['msg'] = '修改失败';
                    echo json_encode($data,JSON_UNESCAPED_UNICODE);
                    exit;
            }
            
        }
    }
    
    
    public function login3()
    {
        if ($this->users_id > 0) {
            $this->redirect('user/Users/centre');
            exit;
        }
        if (IS_POST) {
            $post  = input('post.');
            
            // dump($post);die;
            $post['username'] = trim($post['phone']);

            if (empty($post['username'])) {
                $data['status'] = -1;
                $data['msg'] = '手机号码不能为空';
                echo json_encode($data);
                exit;
                // $this->error('手机号码不能为空！', null, ['status' => 1]);
            }
           $login =  M('sms_log')->where(array('mobile' => $post['phone']))->order('id desc')->find();
           if ($login['status'] == 1) {
               if ($post['code'] != $login['code']) {
                   
                    $data['status'] = -1;
                    $data['msg'] = '您输入的验证码错误';
                    echo json_encode($data);
                    exit;
                   
                //   $this->error('您输入的验证码错误！', null, ['status' => -1]);
               }
           if ($login['add_time'] + 1800 < time())
            {
                $data['status'] = -1;
                $data['msg'] = '验证码已过期,请重新发送！';
                echo json_encode($data);
                exit;
                // $this->error('验证码已过期,请重新发送！', null, ['status' => 1]);
            }
           }else
           {
                $data['status'] = -1;
                $data['msg'] = '您输入的验证码错误！';
                echo json_encode($data);
                exit;
                // $this->error('您输入的验证码错误！', null, ['status' => -1]);
           }
            $users = $this->users_db->where([
                'mobile' => $post['phone'],
                'is_del'   => 0,
                'lang'     => $this->home_lang,
            ])->find();
            // dump($users);die;
            if (!empty($users)) {
                if (!empty($users['admin_id'])) {
                    // 后台账号不允许在前台通过账号密码登录，只能后台登录时同步到前台
                    $data['status'] = -1;
                    $data['msg'] = '前台禁止管理员登录！';
                    echo json_encode($data);
                    exit;
                    // $this->error('前台禁止管理员登录！', null, ['status' => 1]);
                }
                if (empty($users['is_activation'])) {
                    
                    $data['status'] = -1;
                    $data['msg'] = '该会员尚未激活，请联系管理员！';
                    echo json_encode($data);
                    exit;
                    
                    // $this->error('该会员尚未激活，请联系管理员！', null, ['status' => 1]);
                }

                $users_id = $users['users_id'];
                // dump($_SESSION['think']['codes']);die;
                if ($post['code'] === $login['code']) {
                    // 判断是前台还是后台注册的会员，后台注册不受注册验证影响，1为后台注册，2为前台注册。
                    if (2 == $users['register_place']) {
                        $usersVerificationRow = M('users_config')->where([
                            'name' => 'users_verification',
                            'lang' => $this->home_lang,
                        ])->find();
                        if ($usersVerificationRow['update_time'] <= $users['reg_time']) {
                            // 判断是否需要后台审核
                            if ($usersVerificationRow['value'] == 1 && $users['is_activation'] == 0) {
                                $this->error('管理员审核中，请稍等！', null, ['status' => 2]);
                            }
                        }
                    }
                    // 会员users_id存入session
                    model('EyouUsers')->loginAfter($users);

                    // 回跳路径
                    $url = input('post.referurl/s', null, 'htmlspecialchars_decode,urldecode');
                    
                    $data['status'] = 1;
                    $data['msg'] = '登录成功';
                    $data['url'] = $url;
                    echo json_encode($data);
                    exit;
                    
                    // $this->success('登录成功', $url);
                } else {
                    $data['status'] = -1;
                    $data['msg'] = '密码不正确';
                    echo json_encode($data);
                    exit;
                    // $data['url'] = $url;
                    // $this->error('密码不正确！', null, ['status' => 1]);
                }
            }else
            {
                $post['password'] = 'abc123456';
                $data['username']       =   $post['phone'];
                $data['nickname']       = !empty($post['nickname']) ? $post['nickname'] : $post['username'];
                $data['password']       = func_encrypt($post['password']);
                $data['mobile']      =   $post['phone'];
                $data['is_email']       = !empty($ParaData['email_2']) ? 1 : 0;
                $data['last_ip']        = clientIP();
                $data['head_pic']       = ROOT_DIR . '/public/static/common/images/dfboy.png';
                $data['reg_time']       = getTime();
                $data['last_login']     = getTime();
                $data['source']         = $post['source'];
                $data['register_place'] = 2;  // 注册位置，后台注册不受注册验证影响，1为后台注册，2为前台注册。
                $data['lang']           = $this->home_lang;
                $level_id      = $this->users_level_db->where([
                    'is_system' => 1,
                    'lang'      => $this->home_lang,
                ])->getField('level_id');
                $data['level'] = $level_id;
                $users_id = M('users')->add($data);
                if (!empty($users_id)) {
                    $pid = $users_id.str_pad(mt_rand(10, 999999), 6, "0", STR_PAD_BOTH);
                    $ret['pid'] = $pid;
                     M('users')->where(array('users_id' =>$users_id))->save($ret);
                    // 批量添加会员属性到属性信息表
                    if (!empty($ParaData)) {
                        $betchData    = [];
                        $usersparaRow = $this->users_parameter_db->where([
                            'lang'      => $this->home_lang,
                            'is_hidden' => 0,
                        ])->getAllWithIndex('name');
                        foreach ($ParaData as $key => $value) {
                            if (preg_match('/_code$/i', $key)) {
                                continue;
                            }

                            // 若为数组，则拆分成字符串
                            if (is_array($value)) $value = implode(',', $value);
                            $para_id     = intval($usersparaRow[$key]['para_id']);
                            $betchData[] = [
                                'users_id' => $users_id,
                                'para_id'  => $para_id,
                                'info'     => $value,
                                'lang'     => $this->home_lang,
                                'add_time' => getTime(),
                            ];
                        }
                        $this->users_list_db->insertAll($betchData);
                    }
                    session('users_id', $users_id);
                    if (session('users_id')) {
                        setcookie('users_id', $users_id, null);
                        
                        // dump($users_verification);die;
                        if (empty($users_verification)) {
                            // 无需审核，直接登陆
                            // dump(11);die;
                            $url = url('user/Users/centre');
                            
                                $data['status'] = 1;
                                $data['msg'] = '登录成功';
                                $data['url'] = $url;
                                echo json_encode($data);
                                exit;
                            
                            // $this->success('登入成功！', $url, ['status' => 3]);
                        } else if (1 == $users_verification) {
                            // 需要后台审核
                            session('users_id', null);
                            //  dump(22);die;
                            $url = url('user/Users/login');
                            $this->success('注册成功，等管理员激活才能登录！', $url, ['status' => 2]);
                        } else if (2 == $users_verification) {
                            // 注册成功
                            //  dump(14);die;
                            $url = url('user/Users/centre');
                            $this->success('注册成功，邮箱绑定成功，跳转至会员中心！', $url, ['status' => 0]);
                        } else if (3 == $users_verification) {
                            // 注册成功
                            //  dump(33);die;
                            $url = url('user/Users/centre');
                            $this->success('注册成功，手机绑定成功，跳转至会员中心！', $url, ['status' => 0]);
                        }
                    }
                }
            }
        }
        
        $data['status'] = -1;
        $data['msg'] = '请输入相关的账号密码';
        $data['url'] = $url;
        echo json_encode($data,JSON_UNESCAPED_UNICODE);
        exit;
    }
    
    

    // 登陆
    public function login()
    {
        
        // dump($_SESSION);die;
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");
        header('Access-Control-Allow-Methods: GET,POST,PUT,DELETE,OPTIONS,PATCH');
        // 若已登录则重定向
        if ($this->users_id > 0) $this->redirect('user/Users/centre');

        // 回跳路径
        $referurl = input('param.referurl/s', null, 'htmlspecialchars_decode,urldecode');
        if (empty($referurl)) {
            if (isset($_SERVER['HTTP_REFERER']) && stristr($_SERVER['HTTP_REFERER'], $this->request->host())) {
                $referurl = $_SERVER['HTTP_REFERER'];
            } else {
                $referurl = url("user/Users/centre");
            }
        }
        session('eyou_referurl', $referurl);

        // 若为微信端并且开启微商城模式则重定向直接使用微信登录
        if (isWeixin() && !empty($this->usersConfig['shop_micro'])) {
            $WeChatLoginConfig = !empty($this->usersConfig['wechat_login_config']) ? unserialize($this->usersConfig['wechat_login_config']) : [];
            if (!empty($WeChatLoginConfig)) {
                $this->redirect('user/Users/ajax_wechat_login');
            }
        }

        // 若为微信端并且没有开启微商城模式则重定向到登录选择页
        $website = input('param.website/s');
        if (isWeixin() && empty($website)) $this->redirect('user/Users/users_select_login');

        // 默认开启验证码
        $is_vertify          = 1;
        $users_login_captcha = config('captcha.users_login');
        if (!function_exists('imagettftext') || empty($users_login_captcha['is_on'])) {
            $is_vertify = 0; // 函数不存在，不符合开启的条件
        }
        $this->assign('is_vertify', $is_vertify);

        if (IS_POST) {
            $post = input('post.');
            
    		$ip = clientIP();
			if($this->limitoftimes($ip,30,600) === false)
            {
               $data['status'] = -1;
               $data['msg'] = '您访问的次数过多';
               echo json_encode($data);
               exit();
            }
            
            $post['username'] = trim($post['username']);

            if (empty($post['username'])) {
                $data['status'] = -1;
                $data['msg'] = '用户名不能为空';
                echo json_encode($data);
                exit;
                // $this->error('用户名不能为空！', null, ['status' => 1]);
            } else if (!preg_match("/^[\x{4e00}-\x{9fa5}\w\-\_\@\#]{2,30}$/u", $post['username'])) {
                
                $data['status'] = -1;
                $data['msg'] = '用户名不正确';
                echo json_encode($data);
                exit;
                
                // $this->error('用户名不正确！', null, ['status' => 1]);
            }

            if (empty($post['password'])) {
                
                $data['status'] = -1;
                $data['msg'] = '密码不能为空';
                echo json_encode($data);
                exit;
            }
            
            if($this->limitoftimes($post['username'],10,600)===false)
            {
                $data['status'] = -1;
                $data['msg'] = '10分钟内最多只能登入10';
                echo json_encode($data);
                exit;
			 //   $this->error('10分钟内最多只能登入10');
            }
            if($this->limitoftimes($post['username'] ,30,'oneday') === false)
            {
                $data['status'] = -1;
                $data['msg'] = '今日登入超过30次，请明日再试！';
                echo json_encode($data);
                exit;
				// $this->error('今日登入超过30次，请明日再试！');
            }
            

            if (1 == $is_vertify) {
                if (empty($post['vertify'])) {
                    $this->error('图片验证码不能为空！', null, ['status' => 1]);
                }
            }

            $users = $this->users_db->where([
                'username' => $post['username'],
                'is_del'   => 0,
                'lang'     => $this->home_lang,
            ])->find();
            if (!empty($users)) {
                if (!empty($users['admin_id'])) {
                    // 后台账号不允许在前台通过账号密码登录，只能后台登录时同步到前台
                    
                    $data['status'] = -1;
                    $data['msg'] = '前台禁止管理员登录';
                    echo json_encode($data);
                    exit;
                    // $this->error('前台禁止管理员登录！', null, ['status' => 1]);
                }

                if (empty($users['is_activation'])) {
                    $data['status'] = -1;
                    $data['msg'] = '该会员尚未激活，请联系管理员！';
                    echo json_encode($data);
                    exit;
                    // $this->error('该会员尚未激活，请联系管理员！', null, ['status' => 1]);
                }

                $users_id = $users['users_id'];
                if (strval($users['password']) === strval(func_encrypt($post['password']))) {

                    // 处理判断验证码
                    if (1 == $is_vertify) {
                        $verify = new Verify();
                        if (!$verify->check($post['vertify'], "users_login")) {
                            $this->error('验证码错误', null, ['status' => 'vertify']);
                        }
                    }

                    // 判断是前台还是后台注册的会员，后台注册不受注册验证影响，1为后台注册，2为前台注册。
                    if (2 == $users['register_place']) {
                        $usersVerificationRow = M('users_config')->where([
                            'name' => 'users_verification',
                            'lang' => $this->home_lang,
                        ])->find();
                        if ($usersVerificationRow['update_time'] <= $users['reg_time']) {
                            // 判断是否需要后台审核
                            if ($usersVerificationRow['value'] == 1 && $users['is_activation'] == 0) {
                                $this->error('管理员审核中，请稍等！', null, ['status' => 2]);
                            }
                        }
                    }
                    
                    if($users['pid'] == '')
                    {
                        $rett['pid'] = $users_id.str_pad(mt_rand(10, 999999), 6, "0", STR_PAD_BOTH);
                        
                        if(strlen($rett['pid']) == 7)
                        {
                            $rett['pid'] = '1'.$rett['pid'];
                        }
                        M('users')->where(array('users_id' => $users['users_id']))->save($rett);
                    }

                    // 会员users_id存入session
                    model('EyouUsers')->loginAfter($users);
                    $data['status'] = 1;
                    $data['token'] = $this->users_id;
                    $data['msg'] = '登录成功';
                    echo json_encode($data);
                    exit;
                    // $this->success('登录成功', $referurl);
                } else {
                    $data['status'] = -1;
                    $data['msg'] = '密码不正确';
                    echo json_encode($data);
                    exit;
                    // $this->error('密码不正确！', null, ['status' => 1]);
                }
            } else {
                    $data['status'] = -1;
                    $data['msg'] = '该用户名不存在，请注册！';
                    echo json_encode($data);
                    exit;
                // $this->error('该用户名不存在，请注册！', null, ['status' => 1]);
            }
        }

        /*微信登录插件 - 判断是否显示微信登录按钮*/
        $weapp_wxlogin = 0;
        if (is_dir('./weapp/WxLogin/')) {
            $wx         = Db::name('weapp')->field('data,status,config')->where(['code' => 'WxLogin'])->find();
            if ($wx) {
                $wx['data'] = unserialize($wx['data']);
                if ($wx['status'] == 1 && $wx['data']['login_show'] == 1) {
                    $weapp_wxlogin = 1;
                }
                // 使用场景 0 PC+手机 1 手机 2 PC
                $wx['config'] = json_decode($wx['config'], true);
                if (isMobile() && !in_array($wx['config']['scene'], [0,1])) {
                    $weapp_wxlogin = 0;
                } else if (!isMobile() && !in_array($wx['config']['scene'], [0,2])) {
                    $weapp_wxlogin = 0;
                }
            }
        }
        $this->assign('weapp_wxlogin', $weapp_wxlogin);
        /*end*/

        /*QQ登录插件 - 判断是否显示QQ登录按钮*/
        $weapp_qqlogin = 0;
        if (is_dir('./weapp/QqLogin/')) {
            $qq         = Db::name('weapp')->field('data,status,config')->where(['code' => 'QqLogin'])->find();
            if (!empty($qq)) {
                $qq['data'] = unserialize($qq['data']);
                if ($qq['status'] == 1 && $qq['data']['login_show'] == 1) {
                    $weapp_qqlogin = 1;
                }
                // 使用场景 0 PC+手机 1 手机 2 PC
                $qq['config'] = json_decode($qq['config'], true);
                if (isMobile() && !in_array($qq['config']['scene'], [0,1])) {
                    $weapp_qqlogin = 0;
                } else if (!isMobile() && !in_array($qq['config']['scene'], [0,2])) {
                    $weapp_qqlogin = 0;
                }
            }
        }
        $this->assign('weapp_qqlogin', $weapp_qqlogin);
        /*end*/

        /*微博插件 - 判断是否显示微博按钮*/
        $weapp_wblogin = 0;
        if (is_dir('./weapp/Wblogin/')) {
            $wb         = Db::name('weapp')->field('data,status,config')->where(['code' => 'Wblogin'])->find();
            if (!empty($wb)) {
                $wb['data'] = unserialize($wb['data']);
                if ($wb['status'] == 1 && $wb['data']['login_show'] == 1) {
                    $weapp_wblogin = 1;
                }
                // 使用场景 0 PC+手机 1 手机 2 PC
                $wb['config'] = json_decode($wb['config'], true);
                if (isMobile() && !in_array($wb['config']['scene'], [0,1])) {
                    $weapp_wblogin = 0;
                } else if (!isMobile() && !in_array($wb['config']['scene'], [0,2])) {
                    $weapp_wblogin = 0;
                }
            }
        }
        $this->assign('weapp_wblogin', $weapp_wblogin);
        /*end*/

        if (1 == config('global.opencodetype')) {
            $type = input('param.type/s');
            $this->assign('type', $type);
        }

        cookie('referurl', $referurl);
        
    //     $datas['status'] = -1;
    //   $datas['msg'] = '请登入';
    //   $datas['res'] = '';
    //   echo json_encode($datas,JSON_UNESCAPED_UNICODE);
    //     exit;
        
        $this->assign('referurl', $referurl);
        return $this->fetch('users_login');
    }

    // 会员注册
    public function reg()
    {
        if ($this->users_id > 0) {
            $this->redirect('user/Users/centre');
            exit;
        }

        $is_vertify        = 1; // 默认开启验证码
        $users_reg_captcha = config('captcha.users_reg');
        if (!function_exists('imagettftext') || empty($users_reg_captcha['is_on'])) {
            $is_vertify = 0; // 函数不存在，不符合开启的条件
        }
        $this->assign('is_vertify', $is_vertify);

        if (IS_AJAX_POST) {
            $post             = input('post.');
            if (isset($post['username'])) {
                $post['username'] = trim($post['username']);

                $users_reg_notallow = explode(',', getUsersConfigData('users.users_reg_notallow'));
                if (!empty($users_reg_notallow)) {
                    if (in_array($post['username'], $users_reg_notallow)) {
                        $this->error('用户名为系统禁止注册！', null, ['status' => 1]);
                    }
                }

                if (empty($post['username'])) {
                    $this->error('用户名不能为空！', null, ['status' => 1]);
                } else if (!preg_match("/^[\x{4e00}-\x{9fa5}\w\-\_\@\#]{2,30}$/u", $post['username'])) {
                    $this->error('请输入2-30位的汉字、英文、数字、下划线等组合', null, ['status' => 1]);
                }
            }

            if (isset($post['password'])) {
                if (empty($post['password'])) {
                    $this->error('登录密码不能为空！', null, ['status' => 1]);
                }

                if (empty($post['password2'])) {
                    $this->error('重复密码不能为空！', null, ['status' => 1]);
                }
            }

            if (1 == $is_vertify) {
                if (empty($post['vertify'])) {
                    $this->error('图片验证码不能为空！', null, ['status' => 1]);
                }
            }

            if (isset($post['username'])) {
                $count = $this->users_db->where([
                    'username' => $post['username'],
                    'lang'     => $this->home_lang,
                ])->count();
                if (!empty($count)) {
                    $this->error('用户名已存在！', null, ['status' => 1]);
                }
            }

            if (isset($post['password'])) {
                if (empty($post['password']) && empty($post['password2'])) {
                    $this->error('登录密码不能为空！', null, ['status' => 1]);
                } else {
                    if ($post['password'] != $post['password2']) {
                        $this->error('两次密码输入不一致！', null, ['status' => 1]);
                    }
                }
            }

            // 处理会员属性数据
            $ParaData = [];
            if (is_array($post['users_'])) {
                $ParaData = $post['users_'];
            }
            unset($post['users_']);

            // 处理提交的会员属性中必填项是否为空
            // 必须传入提交的会员属性数组
            $EmptyData = model('Users')->isEmpty($ParaData, 'reg', 'array');
            if (!empty($EmptyData)) {
                if (is_array($EmptyData)) {
                    $this->error($EmptyData['msg'], null, ['status' => 1, 'field'=>$EmptyData['field']]);
                } else {
                    $this->error($EmptyData, null, ['status' => 1]);
                }
            }

            // 处理提交的会员属性中邮箱和手机是否已存在
            // IsRequired方法传入的参数有2个
            // 第一个必须传入提交的会员属性数组
            // 第二个users_id，注册时不需要传入，修改时需要传入。
            $RequiredData = model('Users')->isRequired($ParaData, 'reg');
            if (!empty($RequiredData) && !is_array($RequiredData)) {
                $this->error($RequiredData, null, ['status' => 1]);
            }

            // 处理判断验证码
            if (1 == $is_vertify) {
                $verify = new Verify();
                if (!$verify->check($post['vertify'], "users_reg")) {
                    $this->error('图片验证码错误', null, ['status' => 'vertify']);
                }
            }

            if (!empty($RequiredData['email'])) {
                // 查询会员输入的邮箱并且为找回密码来源的所有验证码
                $RecordWhere = [
                    'source'   => 2,
                    'email'    => $RequiredData['email'],
                    'users_id' => 0,
                    'status'   => 0,
                    'lang'     => $this->home_lang,
                ];
                $RecordData  = [
                    'status'      => 1,
                    'update_time' => getTime(),
                ];
                // 更新数据
                $this->smtp_record_db->where($RecordWhere)->update($RecordData);
            }

            if (!empty($RequiredData['mobile'])) {
                // 查询会员输入的邮箱并且为找回密码来源的所有验证码
                $RecordWhere = [
                    'source' => 0,
                    'mobile' => $RequiredData['mobile'],
                    'is_use' => 0,
                    'lang'   => $this->home_lang
                ];
                $RecordData  = [
                    'is_use' => 1,
                    'update_time' => getTime()
                ];
                // 更新数据
                $this->sms_log_db->where($RecordWhere)->update($RecordData);
            }

            // 会员设置
            $users_verification = !empty($this->usersConfig['users_verification']) ? $this->usersConfig['users_verification'] : 0;

            // 处理判断是否为后台审核，verification=1为后台审核。
            if (1 == $users_verification) $data['is_activation'] = 0;

            // 添加会员到会员表
            $data['username']       = !empty($post['username']) ? trim($post['username']) : 'yun'.getTime().rand(0,100);
            $data['nickname']       = !empty($post['nickname']) ? $post['nickname'] : $data['username'];
            if (0 == config('global.opencodetype')) {
                $data['password']       = func_encrypt($post['password']);
            }
            $data['is_mobile']      = !empty($ParaData['mobile_1']) ? 1 : 0;
            $data['is_email']       = !empty($ParaData['email_2']) ? 1 : 0;
            $data['last_ip']        = clientIP();
            $data['head_pic']       = ROOT_DIR . '/public/static/common/images/dfboy.png';
            $data['reg_time']       = getTime();
            $data['last_login']     = getTime();
            $data['register_place'] = 2;  // 注册位置，后台注册不受注册验证影响，1为后台注册，2为前台注册。
            $data['lang']           = $this->home_lang;

            $level_id      = $this->users_level_db->where([
                'is_system' => 1,
                'lang'      => $this->home_lang,
            ])->getField('level_id');
            $data['level'] = $level_id;

            /*特定场景专用*/
            $opencodetype = config('global.opencodetype');
            if (1 == $opencodetype) {
                $origin_mid = cookie('origin_mid');
                if (!empty($origin_mid)) {
                    $data['origin_mid']          = intval($origin_mid);
                }
                $origin_type = cookie('origin_type');
                if (!empty($origin_type)) {
                    $data['origin_type']         = intval($origin_type);
                }
            }
            /*end*/

            $users_id = $this->users_db->add($data);

            // 判断会员是否添加成功
            if (!empty($users_id)) {
                // 批量添加会员属性到属性信息表
                if (!empty($ParaData)) {
                    $betchData    = [];
                    $usersparaRow = $this->users_parameter_db->where([
                        'lang'      => $this->home_lang,
                        'is_hidden' => 0,
                    ])->getAllWithIndex('name');
                    foreach ($ParaData as $key => $value) {
                        if (preg_match('/(_code|_vertify)$/i', $key)) {
                            continue;
                        }

                        // 若为数组，则拆分成字符串
                        if (is_array($value)) $value = implode(',', $value);
                        
                        $para_id     = intval($usersparaRow[$key]['para_id']);
                        $betchData[] = [
                            'users_id' => $users_id,
                            'para_id'  => $para_id,
                            'info'     => $value,
                            'lang'     => $this->home_lang,
                            'add_time' => getTime(),
                        ];
                    }
                    $this->users_list_db->insertAll($betchData);
                }

                // 查询属性表的手机号码和邮箱地址,拼装数组$UsersListData
                $UsersListData                = model('Users')->getUsersListData('*', $users_id);
                $UsersListData['login_count'] = 1;
                $UsersListData['update_time'] = getTime();
                if (2 == $users_verification) {
                    // 若开启邮箱验证并且通过邮箱验证则绑定到会员
                    $UsersListData['is_email'] = 1;
                    if (!isset($post['username'])) {
                        $username = rand_username();
                        $UsersListData['username']       = $username;
                        $UsersListData['nickname']       = $username;
                    }
                } else if (3 == $users_verification) {
                    // 若开启手机验证并且通过手机验证则绑定到会员
                    $UsersListData['is_mobile'] = 1;
                    if (!isset($post['username'])) {
                        $new_username = 'yun'.substr($UsersListData['mobile'], -6);
                        $username = rand_username($new_username);
                        $UsersListData['username']       = $username;
                        $UsersListData['nickname']       = $username;
                    }
                }
                // 同步修改会员信息
                $this->users_db->where('users_id', $users_id)->update($UsersListData);

                // 回跳路径
                $referurl = input('post.referurl/s', null, 'htmlspecialchars_decode,urldecode');

                if (1 == config('global.opencodetype')) {
                    cookie('origin_type', null);
                    cookie('origin_mid', null);
                }

                session('users_id', $users_id);
                if (session('users_id')) {
                    cookie('users_id', $users_id);
                    if (empty($users_verification)) {
                        // 无需审核，直接登陆
                        $url = !empty($referurl) ? $referurl : url('user/Users/centre');
                        $this->success('注册成功，正在跳转中……', $url, ['status' => 3]);
                    } else if (1 == $users_verification) {
                        // 需要后台审核
                        session('users_id', null);
                        $url = url('user/Users/login');
                        $this->success('注册成功，等管理员激活才能登录！', $url, ['status' => 2]);
                    } else if (2 == $users_verification) {
                        // 注册成功
                        $url = !empty($referurl) ? $referurl : url('user/Users/centre');
                        $this->success('注册成功，正在跳转中……', $url, ['status' => 0]);
                    } else if (3 == $users_verification) {
                        // 注册成功
                        $url = !empty($referurl) ? $referurl : url('user/Users/centre');
                        $this->success('注册成功，正在跳转中……', $url, ['status' => 0]);
                    }
                } else {
                    $url = url('user/Users/login');
                    $this->success('注册成功，请登录！', $url, ['status' => 2]);
                }
            }
            $this->error('注册失败', null, ['status' => 4]);
        }

        // 会员属性资料信息
        $users_para = model('Users')->getDataPara('reg');
        $this->assign('users_para', $users_para);

        // 跳转链接
        $referurl = input('param.referurl/s');
        if (empty($referurl)) {
            if (isset($_SERVER['HTTP_REFERER']) && stristr($_SERVER['HTTP_REFERER'], $this->request->host())) {
                $referurl = $_SERVER['HTTP_REFERER'];
            } else {
                $referurl = url("user/Users/centre");
            }
        } else {
            $referurl = urldecode($referurl);
        }
        cookie('referurl', $referurl);
        $this->assign('referurl', $referurl);

        /*微信登录插件 - 判断是否显示微信登录按钮*/
        $weapp_wxlogin = 0;
        if (is_dir('./weapp/WxLogin/')) {
            $wx         = Db::name('weapp')->field('data,status,config')->where(['code' => 'WxLogin'])->find();
            $wx['data'] = unserialize($wx['data']);
            if ($wx['status'] == 1 && $wx['data']['login_show'] == 1) {
                $weapp_wxlogin = 1;
            }
            // 使用场景 0 PC+手机 1 手机 2 PC
            $wx['config'] = json_decode($wx['config'], true);
            if (isMobile() && !in_array($wx['config']['scene'], [0,1])) {
                $weapp_wxlogin = 0;
            } else if (!isMobile() && !in_array($wx['config']['scene'], [0,2])) {
                $weapp_wxlogin = 0;
            }
        }
        $this->assign('weapp_wxlogin', $weapp_wxlogin);
        /*end*/

        /*QQ登录插件 - 判断是否显示QQ登录按钮*/
        $weapp_qqlogin = 0;
        if (is_dir('./weapp/QqLogin/')) {
            $qq         = Db::name('weapp')->field('data,status,config')->where(['code' => 'QqLogin'])->find();
            $qq['data'] = unserialize($qq['data']);
            if ($qq['status'] == 1 && $qq['data']['login_show'] == 1) {
                $weapp_qqlogin = 1;
            }
            // 使用场景 0 PC+手机 1 手机 2 PC
            $qq['config'] = json_decode($qq['config'], true);
            if (isMobile() && !in_array($qq['config']['scene'], [0,1])) {
                $weapp_qqlogin = 0;
            } else if (!isMobile() && !in_array($qq['config']['scene'], [0,2])) {
                $weapp_qqlogin = 0;
            }
        }
        $this->assign('weapp_qqlogin', $weapp_qqlogin);
        /*end*/

        /*微博插件 - 判断是否显示微博按钮*/
        $weapp_wblogin = 0;
        if (is_dir('./weapp/Wblogin/')) {
            $wb         = Db::name('weapp')->field('data,status,config')->where(['code' => 'Wblogin'])->find();
            $wb['data'] = unserialize($wb['data']);
            if ($wb['status'] == 1 && $wb['data']['login_show'] == 1) {
                $weapp_wblogin = 1;
            }
            // 使用场景 0 PC+手机 1 手机 2 PC
            $wb['config'] = json_decode($wb['config'], true);
            if (isMobile() && !in_array($wb['config']['scene'], [0,1])) {
                $weapp_wblogin = 0;
            } else if (!isMobile() && !in_array($wb['config']['scene'], [0,2])) {
                $weapp_wblogin = 0;
            }
        }
        $this->assign('weapp_wblogin', $weapp_wblogin);
        /*end*/

        $html = $this->fetch('users_reg');
        if (isMobile()) {
            $str = <<<EOF
<div id="update_mobile_file" style="display: none;">
    <form id="form1" style="text-align: center;" >
        <input type="button" value="点击上传" onclick="up_f.click();" class="btn btn-primary form-control"/><br>
        <p><input type="file" id="up_f" name="up_f" onchange="MobileHeadPic();" style="display:none"/></p>
    </form>
</div>
</body>
EOF;
            $html = str_ireplace('</body>', $str, $html);
        }

        return $html;
    }

    // 会员中心
    public function centre()
    {
    //   $datas['status'] = 1;
    //   $datas['msg'] = '您已登入';
    //   $datas['res'] = '';
    //   echo json_encode($datas,JSON_UNESCAPED_UNICODE);
    //   exit;
        $mca = Db::name('users_menu')->where(['is_userpage' => 1, 'lang' => $this->home_lang])->getField('mca');
        $mca = !empty($mca) ? $mca : 'user/Users/index';
        $this->redirect($mca);
    }

    // 修改资料
    public function centre_update()
    {
        if (IS_AJAX_POST) {
            $post = input('post.');
            if (getUsersTplVersion() != 'v1') {
                if (isset($post['password_edit']) && !empty($post['password_edit'])) {
                    $password_new = func_encrypt($post['password_edit']);
                }
            }
/*            if (empty($this->users['password'])) {
                // 密码为空则表示第三方注册会员，强制设置密码
                if (empty($post['password'])) {
                    $this->error('第三方注册会员，为确保账号安全，请设置密码。');
                } else {
                    $password_new = func_encrypt($post['password']);
                }
            }*/

            $nickname = trim($post['nickname']);
            if (!empty($post['nickname']) && empty($nickname)) {
                $this->error('昵称不可为纯空格！');
            }

            $ParaData = [];
            if (is_array($post['users_'])) {
                $ParaData = $post['users_'];
            }
            unset($post['users_']);

            // 处理提交的会员属性中必填项是否为空
            // 必须传入提交的会员属性数组
            $EmptyData = model('Users')->isEmpty($ParaData);
            if ($EmptyData) {
                $this->error($EmptyData);
            }

            // 处理提交的会员属性中邮箱和手机是否已存在
            // IsRequired方法传入的参数有2个
            // 第一个必须传入提交的会员属性数组
            // 第二个users_id，注册时不需要传入，修改时需要传入。
            $RequiredData = model('Users')->isRequired($ParaData, $this->users_id);
            if ($RequiredData) {
                $this->error($RequiredData);
            }

            /*处理属性表的数据修改添加*/
            $row2 = $this->users_parameter_db->field('para_id,name')->getAllWithIndex('name');
            if (!empty($row2)) {
                foreach ($ParaData as $key => $value) {
                    if (!isset($row2[$key])) {
                        continue;
                    }

                    // 若为数组，则拆分成字符串
                    if (is_array($value)) {
                        $value = implode(',', $value);
                    }

                    $data                = [];
                    $para_id             = intval($row2[$key]['para_id']);
                    $where               = [
                        'users_id' => $this->users_id,
                        'para_id'  => $para_id,
                        'lang'     => $this->home_lang,
                    ];
                    $data['info']        = $value;
                    $data['update_time'] = getTime();

                    // 若信息表中无数据则添加
                    $row = $this->users_list_db->where($where)->count();
                    if (empty($row)) {
                        $data['users_id'] = $this->users_id;
                        $data['para_id']  = $para_id;
                        $data['lang']     = $this->home_lang;
                        $data['add_time'] = getTime();
                        $this->users_list_db->add($data);
                    } else {
                        $this->users_list_db->where($where)->update($data);
                    }
                }
            }

            // 查询属性表的手机和邮箱信息，同步修改会员信息
            $usersData             = model('Users')->getUsersListData('*', $this->users_id);
            $usersData['nickname'] = trim($post['nickname']);
            if (!empty($password_new)) {
                $usersData['password'] = $password_new;
            }
            $usersData['update_time'] = getTime();
            $return                   = $this->users_db->where('users_id', $this->users_id)->update($usersData);
            if ($return) {
                \think\Cache::clear('users_list');
                $this->success('操作成功');
            }
            $this->error('操作失败');
        }
        $this->error('访问错误！');
    }

    // 更改密码
    public function change_pwd()
    {
        if (IS_POST) {
            $post = input('post.');
            //   dump($post);die;
            
           $res = preg_match('/^(\w*(?=\w*\d)(?=\w*[A-Za-z])\w*){8,16}$/', $post['password']);
        //   dump($res);die;
           if(!$res)
           {
                $data['status'] = -1;
                $data['msg'] = '密码格式不正确';
                echo json_encode($data);
                exit;
                //$this->error('密码格式不正确！');
           }
        //   dump($res);die;
            if (empty($post['password'])) {
                
                $data['status'] = -1;
                $data['msg'] = '新密码不能为空';
                echo json_encode($data);
                exit;
                
                // $this->error('新密码不能为空！');
            } else if ($post['password'] != $post['password2']) {
                
                $data['status'] = -1;
                $data['msg'] = '重复密码与新密码不一致';
                echo json_encode($data);
                exit;
                
                //$this->error('重复密码与新密码不一致！');
            }

            $users = $this->users_db->field('password')->where([
                'mobile' => $post['phone'],
                'lang'     => $this->home_lang,
            ])->find();
            if (!empty($users)) {
                $r = $this->users_db->where([
                    'mobile' => $post['phone'],
                    'lang'     => $this->home_lang,
                ])->update([
                    'password'    => func_encrypt($post['password']),
                    'update_time' => getTime(),
                ]);
                if ($r) {
                    $data['status'] = 1;
                    $data['msg'] = '修改成功';
                    echo json_encode($data);
                    exit;
                    // $this->success('修改成功');
                }
                    $data['status'] = -1;
                    $data['msg'] = '修改失败';
                    echo json_encode($data);
                    exit;
                //$this->error('修改失败');
            }
            $data['status'] = -1;
            $data['msg'] = '登录失效，请重新登录！';
            echo json_encode($data);
            exit;
            //$this->error('登录失效，请重新登录！');
        }
        // return $this->fetch('users_change_pwd');
    }

    // 找回密码
    public function retrieve_password()
    {
        if ($this->users_id > 0) $this->redirect('user/Users/centre');

        if (!empty($this->usersConfig['users_retrieve_password']) && 2 == $this->usersConfig['users_retrieve_password']) {
            $this->redirect('user/Users/retrieve_password_mobile');
        }

        $is_vertify                 = 1; // 默认开启验证码
        $users_retrieve_pwd_captcha = config('captcha.users_retrieve_password');
        if (!function_exists('imagettftext') || empty($users_retrieve_pwd_captcha['is_on'])) {
            $is_vertify = 0; // 函数不存在，不符合开启的条件
        }
        $this->assign('is_vertify', $is_vertify);

        if (IS_AJAX_POST) {
            $post = input('post.');
            // POST数据基础判断
            if (empty($post['email'])) {
                $this->error('邮箱地址不能为空！');
            }
            if (1 == $is_vertify) {
                if (empty($post['vertify'])) {
                    $this->error('图片验证码不能为空！');
                }
            }
            if (empty($post['email_code'])) {
                $this->error('邮箱验证码不能为空！');
            }

            // 判断会员输入的邮箱是否存在
            $ListWhere = array(
                'info' => array('eq', $post['email']),
                'lang' => array('eq', $this->home_lang),
            );
            $ListData  = $this->users_list_db->where($ListWhere)->field('users_id')->find();
            if (empty($ListData)) {
                $this->error('邮箱不存在，不能找回密码！');
            }

            // 判断会员输入的邮箱是否已绑定
            $UsersWhere = array(
                'email' => array('eq', $post['email']),
                'lang'  => array('eq', $this->home_lang),
            );
            $UsersData  = $this->users_db->where($UsersWhere)->field('is_email')->find();
            if (empty($UsersData['is_email'])) {
                $this->error('邮箱未绑定，不能找回密码！');
            }

            // 查询会员输入的邮箱验证码是否存在
            $RecordWhere = [
                'code' => $post['email_code'],
                'lang' => $this->home_lang,
            ];
            $RecordData  = $this->smtp_record_db->where($RecordWhere)->field('status,add_time,email')->find();
            if (!empty($RecordData)) {
                // 邮箱验证码是否超时
                $time                   = getTime();
                $RecordData['add_time'] += Config::get('global.email_default_time_out');
                if ('1' == $RecordData['status'] || $RecordData['add_time'] <= $time) {
                    $this->error('邮箱验证码已被使用或超时，请重新发送！');
                } else {
                    // 图形验证码判断
                    if (1 == $is_vertify) {
                        $verify = new Verify();
                        if (!$verify->check($post['vertify'], "users_retrieve_password")) {
                            $this->error('图形验证码错误，请重新输入！');
                        }
                    }

                    session('users_retrieve_password_email', $post['email']); // 标识邮箱验证通过
                    $em  = rand(10, 99) . base64_encode($post['email']) . '/=';
                    $url = url('user/Users/reset_password', ['em' => base64_encode($em)]);
                    $this->success('操作成功', $url);
                }

            } else {
                $this->error('邮箱验证码不正确，请重新输入！');
            }
        }

        session('users_retrieve_password_email', null); // 标识邮箱验证通过

        /*检测会员邮箱属性是否开启*/
        $usersparamRow = $this->users_parameter_db->where([
            'name'      => ['LIKE', 'email_%'],
            'is_hidden' => 1,
            'lang'      => $this->home_lang,
        ])->find();
        if (!empty($usersparamRow)) {
            $this->error('会员邮箱属性已关闭，请联系网站管理员 ！');
        }
        /*--end*/

        return $this->fetch();
    }

    // 重置密码
    public function reset_password()
    {
        if (IS_AJAX_POST) {
            $post = input('post.');
            if (empty($post['password'])) {
                $this->error('新密码不能为空！');
            }
            if ($post['password'] != $post['password_']) {
                $this->error('两次密码输入不一致！');
            }

            $email = session('users_retrieve_password_email');
            if (!empty($email)) {
                $data   = [
                    'password'    => func_encrypt($post['password']),
                    'update_time' => getTime(),
                ];
                $return = $this->users_db->where([
                    'email' => $email,
                    'lang'  => $this->home_lang,
                ])->update($data);
                if ($return) {
                    session('users_retrieve_password_email', null); // 标识邮箱验证通过
                    $url = url('user/Users/login');
                    $this->success('重置成功！', $url);
                }
            }
            $this->error('重置失败！');
        }

        // 没有传入邮箱，重定向至找回密码页面
        $em    = input('param.em/s');
        $em    = base64_decode(input('param.em/s'));
        $em    = base64_decode(msubstr($em, 2, -2));
        $email = session('users_retrieve_password_email');
        if (empty($email) || !check_email($em) || $em != $email) {
            $this->redirect('user/Users/retrieve_password');
            exit;
        }
        $users = $this->users_db->where([
            'email' => $email,
            'lang'  => $this->home_lang,
        ])->find();

        if (!empty($users)) {
            // 查询会员输入的邮箱并且为找回密码来源的所有验证码
            $RecordWhere = [
                'source'   => 4,
                'email'    => $email,
                'users_id' => 0,
                'status'   => 0,
                'lang'     => $this->home_lang,
            ];
            // 更新数据
            $RecordData = [
                'status'      => 1,
                'update_time' => getTime(),
            ];
            $this->smtp_record_db->where($RecordWhere)->update($RecordData);
        }
        $this->assign('users', $users);
        return $this->fetch();
    }

    // 手机找回密码
    public function retrieve_password_mobile()
    {
        if ($this->users_id > 0) $this->redirect('user/Users/centre');

        if (!empty($this->usersConfig['users_retrieve_password']) && 1 == $this->usersConfig['users_retrieve_password']) {
            $this->redirect('user/Users/retrieve_password');
        }

        $is_vertify = 1; // 默认开启验证码
        $users_retrieve_pwd_captcha = config('captcha.users_retrieve_password');
        if (!function_exists('imagettftext') || empty($users_retrieve_pwd_captcha['is_on'])) {
            $is_vertify = 0; // 函数不存在，不符合开启的条件
        }
        $this->assign('is_vertify', $is_vertify);

        if (IS_AJAX_POST) {
            $post = input('post.');

            // POST数据基础判断
            if (empty($post['mobile'])) $this->error('请输入手机号码');
            if (empty($post['mobile_code'])) $this->error('请输入手机验证码');
            if (1 == $is_vertify) {
                if (empty($post['vertify'])) $this->error('请输入图片验证码');
            }

            // 判断会员输入的手机是否存在
            $ListWhere = array(
                'info' => array('eq', $post['mobile']),
                'lang' => array('eq', $this->home_lang)
            );
            $ListData  = $this->users_list_db->where($ListWhere)->field('users_id')->find();
            if (empty($ListData)) $this->error('手机号码不存在，不能找回密码！');

            // 判断会员输入的手机是否已绑定
            $UsersWhere = array(
                'mobile' => array('eq', $post['mobile']),
                'lang'   => array('eq', $this->home_lang)
            );
            $UsersData  = $this->users_db->where($UsersWhere)->field('is_mobile')->find();
            if (empty($UsersData['is_mobile'])) $this->error('手机号码未绑定，不能找回密码！');

            // 判断验证码是否存在并且是否可用
            $RecordWhere = [
                'mobile' => $post['mobile'],
                'code'   => $post['mobile_code'],
                'lang'   => $this->home_lang
            ];
            $RecordData = $this->sms_log_db->where($RecordWhere)->field('is_use, add_time')->order('id desc')->find();
            if (!empty($RecordData)) {
                // 验证码存在
                $time = getTime();
                $RecordData['add_time'] += Config::get('global.mobile_default_time_out');
                if (1 == $RecordData['is_use'] || $RecordData['add_time'] <= $time) {
                    $this->error('手机验证码已被使用或超时，请重新发送！');
                } else {
                    // 处理手机验证码
                    $RecordWhere = [
                        'source'   => 4,
                        'mobile'   => $post['mobile'],
                        'is_use'   => 0,
                        'lang'     => $this->home_lang
                    ];
                    // 更新数据
                    $RecordData = [
                        'is_use'      => 1,
                        'update_time' => $time
                    ];
                    $this->sms_log_db->where($RecordWhere)->update($RecordData);
                    session('users_retrieve_password_mobile', $post['mobile']);
                    $this->success('验证通过', url('user/Users/reset_password_mobile'));
                }
            } else {
                $this->error('手机验证码不正确，请重新输入！');
            }
        }

        session('users_retrieve_password_mobile', null);

        /*检测会员邮箱属性是否开启*/
        $usersparamRow = $this->users_parameter_db->where([
            'name'      => ['LIKE', 'mobile_%'],
            'is_hidden' => 1,
            'lang'      => $this->home_lang
        ])->find();
        if (!empty($usersparamRow)) $this->error('会员手机属性已关闭，请联系网站管理员！');
        /*--end*/

        return $this->fetch();
    }

    public function reset_password_mobile()
    {
        if (IS_AJAX_POST) {
            $post = input('post.');
            if (empty($post['password'])) $this->error('请输入新密码');
            if (empty($post['password_'])) $this->error('请输入确认新密码');
            if ($post['password'] != $post['password_']) $this->error('两次密码输入不一致！');

            $mobile = session('users_retrieve_password_mobile');
            if (!empty($mobile)) {
                $data   = [
                    'password'    => func_encrypt($post['password']),
                    'update_time' => getTime()
                ];
                $return = $this->users_db->where(['mobile'=>$mobile, 'lang'=>$this->home_lang])->update($data);
                if ($return) {
                    session('users_retrieve_password_mobile', null);
                    $url = url('user/Users/login');
                    $this->success('重置成功！', $url);
                }
            }
            $this->error('重置失败！');
        }

        // 没有手机号则重定向至找回密码页面
        $mobile = session('users_retrieve_password_mobile');
        if (empty($mobile)) $this->redirect('user/Users/retrieve_password_mobile');
        
        // 查询会员信息
        $username = $this->users_db->where(['mobile'=>$mobile, 'lang'=>$this->home_lang])->getField('username');
        $this->assign('username', $username);
        return $this->fetch();   
    }

    /**
     * 上传头像
     * @return [type] [description]
     */
    public function edit_users_head_pic()
    {
        if (IS_POST) {
            $head_pic_url = input('param.filename/s', '');
//            dump($head_pic_url);;die;
            if (!empty($head_pic_url) && !is_http_url($head_pic_url)) {
//                dump($head_pic_url);;die;
                $usersData['head_pic']    = $head_pic_url;
                $usersData['update_time'] = getTime();
                $return                   = $this->users_db->where([
                    'users_id' => $this->users_id,
                    'lang'     => $this->home_lang,
                ])->update($usersData);
                if (false !== $return) {
                    /*同步头像到管理员表对应的管理员*/
                    if (!empty($this->users['admin_id'])) {
                        Db::name('admin')->where(['admin_id'=>$this->users['admin_id']])->update([
                            'head_pic'  => $head_pic_url,
                            'update_time'   => getTime(),
                        ]);
                    }
                    /*end*/

                    /*删除之前的头像文件*/
                    if (!stristr($this->users['head_pic'], '/public/static/common/images/')) {
                        @unlink('.'.handle_subdir_pic($this->users['head_pic'], 'img', false, true));
                    }
                    /*end*/

                    // $head_pic_url = func_thumb_img($head_pic_url, 200, 200);
                    // dump($head_pic_url);die;

                    $this->success('上传成功', null, ['head_pic'=>$head_pic_url]);
                }
            }
        }
        $this->error('上传失败');
    }

    public function bind_email()
    {
        if (IS_AJAX_POST) {
            $post = input('post.');
            if (!empty($post['email']) && !empty($post['email_code'])) {
                // 邮箱格式验证是否正确
                if (!check_email($post['email'])) {
                    $this->error('邮箱格式不正确！');
                }

                // 是否已存在相同邮箱地址
                $ListWhere = [
                    'users_id' => ['NEQ', $this->users_id],
                    'info'     => $post['email'],
                    'lang'     => $this->home_lang,
                ];
                $ListData  = $this->users_list_db->where($ListWhere)->count();
                if (!empty($ListData)) {
                    $this->error('该邮箱已存在，不可绑定！');
                }

                // 判断验证码是否存在并且是否可用
                $RecordWhere = [
                    'email'    => $post['email'],
                    'code'     => $post['email_code'],
                    'users_id' => $this->users_id,
                    'lang'     => $this->home_lang,
                ];
                $RecordData  = $this->smtp_record_db->where($RecordWhere)->field('record_id,email,status,add_time')->find();
                if (!empty($RecordData)) {
                    // 验证码存在
                    $time                   = getTime();
                    $RecordData['add_time'] += Config::get('global.email_default_time_out');
                    if (1 == $RecordData['status'] || $RecordData['add_time'] <= $time) {
                        // 验证码不可用
                        $this->error('邮箱验证码已被使用或超时，请重新发送！');
                    } else {
                        // 查询会员输入的邮箱并且为绑定邮箱来源的所有验证码
                        $RecordWhere = [
                            'source'   => 3,
                            'email'    => $RecordData['email'],
                            'users_id' => $this->users_id,
                            'status'   => 0,
                            'lang'     => $this->home_lang,
                        ];

                        // 更新数据
                        $RecordData = [
                            'status'      => 1,
                            'update_time' => $time,
                        ];
                        $this->smtp_record_db->where($RecordWhere)->update($RecordData);

                        // 匹配查询邮箱
                        $ParaWhere = [
                            'name'      => ['LIKE', "email_%"],
                            'is_system' => 1,
                            'lang'      => $this->home_lang,
                        ];
                        $ParaData  = $this->users_parameter_db->where($ParaWhere)->field('para_id')->find();

                        // 修改会员属性表信息
                        $listCount = $this->users_list_db->where([
                            'para_id'  => $ParaData['para_id'],
                            'users_id' => ['EQ', $this->users_id],
                            'lang'     => $this->home_lang,
                        ])->count();
                        if (empty($listCount)) { // 后台新增会员，没有会员属性记录的情况
                            $ListData = [
                                'users_id' => $this->users_id,
                                'para_id'  => $ParaData['para_id'],
                                'info'     => $post['email'],
                                'lang'     => $this->home_lang,
                                'add_time' => $time,
                            ];
                            $IsList   = $this->users_list_db->where($ListWhere)->add($ListData);
                        } else {
                            $ListWhere = [
                                'users_id' => $this->users_id,
                                'para_id'  => $ParaData['para_id'],
                                'lang'     => $this->home_lang,
                            ];
                            $ListData  = [
                                'info'        => $post['email'],
                                'update_time' => $time,
                            ];
                            $IsList    = $this->users_list_db->where($ListWhere)->update($ListData);
                        }

                        if (!empty($IsList)) {
                            // 同步修改会员表邮箱地址，并绑定邮箱地址到会员账号
                            $UsersData = [
                                'users_id'    => $this->users_id,
                                'is_email'    => '1',
                                'email'       => $post['email'],
                                'update_time' => $time,
                            ];
                            $this->users_db->update($UsersData);
                            \think\Cache::clear('users_list');
                            $this->success('操作成功！');
                        } else {
                            $this->error('未知错误，邮箱地址修改失败，请重新获取验证码！');
                        }
                    }
                } else {
                    $this->error('输入的邮箱地址和邮箱验证码不一致，请重新输入！');
                }
            }
        }
        $title = input('param.title/s');
        $this->assign('title', $title);
        return $this->fetch();
    }

    // 绑定手机
    public function bind_mobile()
    {
        if (IS_AJAX_POST) {
            $post = input('post.');
            if (!empty($post['mobile']) && !empty($post['mobile_code'])) {
                // 手机格式验证是否正确
                if (!check_mobile($post['mobile'])) $this->error('手机格式不正确！');
                
                // 是否已存在相同手机号码
                $ListWhere = [
                    'users_id' => ['NEQ', $this->users_id],
                    'info'     => $post['mobile'],
                    'lang'     => $this->home_lang
                ];
                $ListData  = $this->users_list_db->where($ListWhere)->count();
                if (!empty($ListData)) $this->error('手机号码已存在，不可绑定！');

                // 判断验证码是否存在并且是否可用
                $RecordWhere = [
                    'mobile'   => $post['mobile'],
                    'code'     => $post['mobile_code'],
                    'lang'     => $this->home_lang
                ];
                $RecordData = $this->sms_log_db->where($RecordWhere)->field('is_use, add_time')->order('id desc')->find();
                if (!empty($RecordData)) {
                    // 验证码存在
                    $time = getTime();
                    $RecordData['add_time'] += Config::get('global.mobile_default_time_out');
                    if (1 == $RecordData['is_use'] || $RecordData['add_time'] <= $time) {
                        // 验证码不可用
                        $this->error('手机验证码已被使用或超时，请重新发送！');
                    } else {
                        // 查询会员输入的邮箱并且为绑定邮箱来源的所有验证码
                        $RecordWhere = [
                            'source'   => 1,
                            'mobile'   => $post['mobile'],
                            'is_use'   => 0,
                            'lang'     => $this->home_lang
                        ];
                        // 更新数据
                        $RecordData = [
                            'is_use'      => 1,
                            'update_time' => $time
                        ];
                        $this->sms_log_db->where($RecordWhere)->update($RecordData);

                        // 匹配查询手机
                        $ParaWhere = [
                            'name'      => ['LIKE', "mobile_%"],
                            'is_system' => 1,
                            'lang'      => $this->home_lang
                        ];
                        $ParaData  = $this->users_parameter_db->where($ParaWhere)->field('para_id')->find();

                        // 修改会员属性表信息
                        $listCount = $this->users_list_db->where([
                            'para_id'  => $ParaData['para_id'],
                            'users_id' => ['EQ', $this->users_id],
                            'lang'     => $this->home_lang
                        ])->count();

                        if (empty($listCount)) {
                            // 后台新增会员，没有会员属性记录的情况
                            $ListData = [
                                'users_id' => $this->users_id,
                                'para_id'  => $ParaData['para_id'],
                                'info'     => $post['mobile'],
                                'lang'     => $this->home_lang,
                                'add_time' => $time
                            ];
                            $IsList = $this->users_list_db->where($ListWhere)->add($ListData);
                        } else {
                            $ListWhere = [
                                'users_id' => $this->users_id,
                                'para_id'  => $ParaData['para_id'],
                                'lang'     => $this->home_lang
                            ];
                            $ListData  = [
                                'info' => $post['mobile'],
                                'update_time' => $time
                            ];
                            $IsList = $this->users_list_db->where($ListWhere)->update($ListData);
                        }

                        if (!empty($IsList)) {
                            // 同步修改会员表邮箱地址，并绑定邮箱地址到会员账号
                            $UsersData = [
                                'users_id'    => $this->users_id,
                                'is_mobile'   => 1,
                                'mobile'      => $post['mobile'],
                                'update_time' => $time
                            ];
                            $this->users_db->update($UsersData);
                            \think\Cache::clear('users_list');
                            $this->success('操作成功！');
                        } else {
                            $this->error('未知错误，手机号码修改失败，请重新获取验证码！');
                        }
                    }
                } else {
                    $this->error('输入的手机号码和手机验证码不一致，请重新输入！');
                }
            }
        }

        if (1 == config('global.opencodetype')) {
            $opt = input('param.opt/s');
            $this->assign('opt', $opt);
        }

        $title = input('param.title/s');
        $this->assign('title', $title);
        
        return $this->fetch();
    }

    // 退出登陆
    public function logout()
    {
        
        // dump($_SESSION);die;
        session('users_id', null);
        session('users', null);
        cookie('users_id', null);

        // 跳转链接
        $referurl = input('param.referurl/s');
        $data['status'] = 1;
        $data['msg'] = '退出成功';
        $data['res'] = '';
        echo json_encode($data,JSON_UNESCAPED_UNICODE);
        exit;
        // dump($referurl);die;
        // if (empty($referurl)) {
        //     $referurl = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ROOT_DIR . '/';
        // }

        // // 开启微站点模式，强制退出到网站首页
        // if (!empty($this->usersConfig['shop_micro']) && 1 == $this->usersConfig['shop_micro']) {
        //     $referurl = ROOT_DIR . '/';
        // }
        
        // $this->redirect($referurl);
    }

    /**
     * 我的足迹首页
     * @return mixed
     */
    public function footprint_index()
    {
        $allow_release_channel_list = $this->get_allow_release_channel_list();

        $params = array();

        $keywords = input('keywords/s');
        // dump($keywords);die;

        $condition = array();
        if (!empty($keywords)) {
            $condition['a.title'] = array('LIKE', "%{$keywords}%");
        }

        //所属模型
        if (!empty($allow_release_channel_list)) {
            $channel_id = input('channel_id/d');
            if ($channel_id) {
                $condition['a.channel'] = $channel_id;
                $params['channel_id'] = $condition['a.channel'];
            }
        }

        $condition['a.users_id'] = $this->users_id;

        $count = Db::name('users_footprint')->alias('a')->where($condition)->count('id');// 查询满足要求的总记录数
        $Page  = $pager = new Page($count, config('paginate.list_rows'));// 实例化分页类 传入总记录数和每页显示的记录数
        $result['data'] = Db::name('users_footprint')
            ->field('d.*,a.*')
            ->alias('a')
            ->join('__ARCTYPE__ d', 'a.typeid = d.id')
            ->where($condition)
            ->order('a.update_time desc')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();

        foreach ($result['data'] as $key => $value) {
            $result['data'][$key]['litpic'] = handle_subdir_pic($result['data'][$key]['litpic']); // 支持子目录
            $result['data'][$key]['arcurl']  = arcurl('home/View/index', $value);
        }
        $result['delurl']  = url('user/Users/footprint_del');

        $eyou = array(
            'field' => $result,
        );

        //导航栏url
        $params['barurl'] = MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME;

        $show = $Page->show();// 分页显示输出
        $this->assign('page', $show);// 赋值分页输出
        $this->assign('eyou', $eyou);// 赋值数据集
        $this->assign('allow_release_channel_list', $allow_release_channel_list);
        $this->assign('params', $params);
        $this->assign('pager', $pager);// 赋值分页对象
        return $this->fetch('users_footprint_index');
    }

    //获取启用的模型
    protected function get_allow_release_channel_list()
    {
        $allow_release_channel_list = cache('extra_global_channeltype');
        $res = array();
        if ($allow_release_channel_list) {
            foreach ($allow_release_channel_list as $item) {
                if ($item['status'] == 1) {
                    $value = array();
                    $value['id'] = $item['id'];
                    $value['nid'] = $item['nid'];
                    $value['title'] = $item['title'];
                    $value['ntitle'] = $item['ntitle'];
                    $res[] = $value;
                }
            }
        }
        return $res;
    }

    /**
     * 删除
     */
    public function footprint_del()
    {
        if (IS_POST) {
            $id_arr = input('del_id/a');
            $id_arr = eyIntval($id_arr);
            if (!empty($id_arr)) {
                $r = Db::name('users_footprint')->where([
                    'id'       => ['IN', $id_arr],
                    'users_id' => $this->users_id,
                ])->delete();
                if (!empty($r)) {
                    $this->success('删除成功');
                }
            }
        }
        $this->error('删除失败');
    }

    /*
     * 收藏
     */
    public function collection_index()
    {
        $allow_release_channel_list = $this->get_allow_release_channel_list();

        $params = array();

        $keywords = input('keywords/s');

        $condition = array();
        if (!empty($keywords)) {
            $condition['a.title'] = array('LIKE', "%{$keywords}%");
        }

        $condition['a.users_id'] = $this->users_id;

        //所属模型
        if (!empty($allow_release_channel_list)) {
            $channel_id = input('channel_id/d');
            if ($channel_id) {
                $condition['a.channel'] = $channel_id;
                $params['channel_id'] = $condition['a.channel'];
            }
        }

        // 多语言
        $condition['a.lang'] = $this->home_lang;

        $count = Db::name('users_collection')->alias('a')->where($condition)->count('id');// 查询满足要求的总记录数
        $Page = $pager = new Page($count, config('paginate.list_rows'));// 实例化分页类 传入总记录数和每页显示的记录数

        $result['data'] = Db::name('users_collection')
            ->field('d.*, a.*')
            ->alias('a')
            ->join('__ARCTYPE__ d', 'a.typeid = d.id', 'LEFT')
            ->where($condition)
            ->order('a.id desc')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();
        foreach ($result['data'] as $key => $value) {
            $result['data'][$key]['litpic'] = handle_subdir_pic($result['data'][$key]['litpic']); // 支持子目录
            $result['data'][$key]['arcurl']  = arcurl('home/View/index', $value);
        }

        $result['delurl']  = url('user/Users/collection_del');

        $eyou = array(
            'field' => $result,
        );

        //导航栏url
        $params['barurl'] = MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME;

        $show = $Page->show();// 分页显示输出
        $this->assign('page',$show);// 赋值分页输出
        // 数据
        $this->assign('eyou', $eyou);
        $this->assign('allow_release_channel_list', $allow_release_channel_list);
        $this->assign('params', $params);
        $this->assign('pager',$pager);// 赋值分页对象
        return $this->fetch('users_collection_index');
    }

    /**
     * 删除
     */
    public function collection_del()
    {
        if (IS_POST) {
            $id_arr = input('del_id/a');
            $id_arr = eyIntval($id_arr);
            if(!empty($id_arr)){
                $r = Db::name('users_collection')->where([
                    'id'    => ['IN', $id_arr],
                    'users_id'  => $this->users_id,
                    'lang'  => $this->home_lang,
                ])
                    ->delete();
                if(!empty($r)){
                    $this->success('删除成功');
                }
            }
        }
        $this->error('删除失败');
    }

    //我的视频
    public function media_index()
    {
        $keywords = input('keywords/s');

        $condition = array();
        $order_code = input('order_code/s');
        if (!empty($order_code)) $condition['a.order_code'] = ['LIKE', "%{$order_code}%"];

        $condition['a.users_id'] = $this->users_id;
        $condition['a.lang'] = $this->home_lang;

        $order_status = input('order_status/s');
        $this->assign('order_status', $order_status);
        if (!empty($order_status)) {
            if (-1 == $order_status) $order_status = 0;
            $condition['a.order_status'] = $order_status;
        }else{
            $condition['a.order_status'] = 1;//默认查询已购买
        }

        $count = Db::name('media_order')->alias('a')->where($condition)->count('order_id');
        $Page = $pager = new Page($count, config('paginate.list_rows'));

        $result['data'] = Db::name('media_order')->where($condition)
            ->field('a.*,c.aid,c.typeid,c.channel,d.*,a.add_time as order_add_time')
            ->alias('a')
            ->join('__ARCHIVES__ c', 'a.product_id = c.aid', 'LEFT')
            ->join('__ARCTYPE__ d', 'c.typeid = d.id', 'LEFT')
            ->order('a.order_id desc')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();

        $array_new = get_archives_data($result['data'], 'product_id');
        foreach ($result['data'] as $key => $value) {
            $arcurl = '';
            $vars = !empty($array_new[$value['product_id']]) ? $array_new[$value['product_id']] : [];
            if (!empty($vars)) {
                $arcurl = urldecode(arcurl('home/Media/view', $vars));
            }
            $result['data'][$key]['arcurl']  = $arcurl;
        }

        $result['delurl']  = url('user/Users/collection_del');

        $eyou = array(
            'field' => $result,
        );
        $show = $Page->show();
        $this->assign('page',$show);
        // 数据
        $this->assign('eyou', $eyou);
        $this->assign('pager',$pager);
        return $this->fetch('users_media_index');
    }

    // 视频订单详情页
    public function media_order_details()
    {
        $order_id = input('param.order_id');
        if (!empty($order_id)) {
            // 查询订单信息
            $OrderData = Db::name('media_order')
                ->field('a.*, product_id,c.aid,c.typeid,c.channel,d.*')
                ->alias('a')
                ->join('__ARCHIVES__ c', 'a.product_id = c.aid', 'LEFT')
                ->join('__ARCTYPE__ d', 'c.typeid = d.id', 'LEFT')
                ->find($order_id);

            // 查询会员数据
            $UsersData = $this->users_db->find($OrderData['users_id']);
            // 用于点击视频文档跳转到前台
            $array_new = get_archives_data([$OrderData], 'product_id');
            // 内页地址
            $arcurl = '';
            $vars = !empty($array_new[$OrderData['product_id']]) ? $array_new[$OrderData['product_id']] : [];
            if (!empty($vars)) {
                $arcurl = urldecode(arcurl('home/Media/view', $vars));
            }
            $OrderData['arcurl'] = $arcurl;
            // 支持子目录
            $OrderData['product_litpic'] = get_default_pic($OrderData['product_litpic']);
            // 加载数据
            $this->assign('OrderData', $OrderData);
            $this->assign('UsersData', $UsersData);
            return $this->fetch();
        } else {
            $this->error('非法访问！');
        }
    }

    /*
     * 积分明细
     */
    public function score_index()
    {
        //积分类型
        $score_type_arr = config('global.score_type');
        if (!$score_type_arr) {
            $score_type_arr = [1=>'提问',2=>'回答',3=>'最佳答案',4=>'悬赏退回',5=>'每日签到'];
            config('global.score_type',$score_type_arr);
        }
        $this->assign('score_type_arr',$score_type_arr);

        $condition = array();
        $condition['a.users_id'] = $this->users_id;

        // 多语言
        $condition['a.lang'] = $this->home_lang;

        $count = Db::name('users_score')->alias('a')->where($condition)->count('id');
        $Page = $pager = new Page($count, config('paginate.list_rows'));

        $result['data'] = Db::name('users_score')
            ->field('a.*')
            ->alias('a')
            ->where($condition)
            ->order('a.id desc')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();

        $eyou = array(
            'field' => $result,
        );

        $show = $Page->show();
        $this->assign('page',$show);
        $this->assign('eyou', $eyou);
        $this->assign('pager',$pager);
        return $this->fetch('users_score_index');
    }

    //我的文章
    public function article_index()
    {
        $keywords = input('keywords/s');

        $condition = array();
        $order_code = input('order_code/s');
        if (!empty($order_code)) $condition['a.order_code'] = ['LIKE', "%{$order_code}%"];

        $condition['a.users_id'] = $this->users_id;
        $condition['a.lang'] = $this->home_lang;

        $order_status = input('order_status/s');
        $this->assign('order_status', $order_status);
        if (!empty($order_status)) {
            if (-1 == $order_status) $order_status = 0;
            $condition['a.order_status'] = $order_status;
        }else{
            $condition['a.order_status'] = 1;//默认查询已购买
        }

        $count = Db::name('article_order')->alias('a')->where($condition)->count('order_id');
        $Page = $pager = new Page($count, config('paginate.list_rows'));

        $result['data'] = Db::name('article_order')->where($condition)
            ->field('a.*,c.aid,c.typeid,c.channel,d.*,a.add_time as order_add_time')
            ->alias('a')
            ->join('__ARCHIVES__ c', 'a.product_id = c.aid', 'LEFT')
            ->join('__ARCTYPE__ d', 'c.typeid = d.id', 'LEFT')
            ->order('a.order_id desc')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();

        $array_new = get_archives_data($result['data'], 'product_id');
        foreach ($result['data'] as $key => $value) {
            $arcurl = '';
            $vars = !empty($array_new[$value['product_id']]) ? $array_new[$value['product_id']] : [];
            if (!empty($vars)) {
                $arcurl = urldecode(arcurl('home/Article/view', $vars));
            }
            $result['data'][$key]['arcurl']  = $arcurl;
        }

        $result['delurl']  = url('user/Users/collection_del');

        $eyou = array(
            'field' => $result,
        );
        $show = $Page->show();
        $this->assign('page',$show);
        // 数据
        $this->assign('eyou', $eyou);
        $this->assign('pager',$pager);
        return $this->fetch('users_article_index');
    }

    // 文章订单详情页
    public function article_order_details()
    {
        $order_id = input('param.order_id');
        if (!empty($order_id)) {
            // 查询订单信息
            $OrderData = Db::name('article_order')
                ->field('a.*, product_id,c.aid,c.typeid,c.channel,d.*')
                ->alias('a')
                ->join('__ARCHIVES__ c', 'a.product_id = c.aid', 'LEFT')
                ->join('__ARCTYPE__ d', 'c.typeid = d.id', 'LEFT')
                ->find($order_id);

            // 查询会员数据
            $UsersData = $this->users_db->find($OrderData['users_id']);
            // 用于点击视频文档跳转到前台
            $array_new = get_archives_data([$OrderData], 'product_id');
            // 内页地址
            $arcurl = '';
            $vars = !empty($array_new[$OrderData['product_id']]) ? $array_new[$OrderData['product_id']] : [];
            if (!empty($vars)) {
                $arcurl = urldecode(arcurl('home/Article/view', $vars));
            }
            $OrderData['arcurl'] = $arcurl;
            // 支持子目录
            $OrderData['product_litpic'] = get_default_pic($OrderData['product_litpic']);
            // 加载数据
            $this->assign('OrderData', $OrderData);
            $this->assign('UsersData', $UsersData);
            return $this->fetch();
        } else {
            $this->error('非法访问！');
        }
    }
    
    
    //关注用户
    public function follow()
    {
        $follow_id = input('param.follow_id/d');
        $data['users_id'] = $this->users_id;//关注用户
        $data['follow_id'] = $follow_id;//被关注用户
        if ($this->users_id == $follow_id) {
            $datas['status'] = -1;
            $datas['msg'] = '您不能关注自身';
            $datas['res'] = '';
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit;
        }
        
        if(M('follow')->where(array('users_id' => $this->users_id,'follow_id' => $follow_id))->find())
        {
             $follow = M('follow')->where(array('users_id' => $this->users_id,'follow_id' => $follow_id))->delete();
            //  dump(11);die;
             if($follow)
             {
                $datas['status'] = 1;
                $datas['msg'] = '取消关注';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;
             }
        }else
        {
        $data['add_time'] = time();
        $follow = M('follow')->save($data);
        // dump($follow);die;
        if ($follow) {
                $datas['status'] = 1;
                $datas['msg'] = '关注成功';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;
            //  $this->success('关注成功');
        }
        }
    }
    
    //follow=1关注用户列表
    //follow=2关注我的用户列表
    public function Users_follow()
    {
        $follow = input('param.follow');
        $page =  input('param.page');
        $limit =  input('param.limit');
        $post = input('post.');
        if($follow == 1)
        {
            
            $count = M('follow')
            ->field('a.*,b.nickname,b.quotations,b.head_pic')
    		->alias('a')
    	    ->join('users b', 'a.follow_id = b.users_id', 'LEFT')
    	    ->where(array('a.users_id' => $this->users_id))
    	    ->count();
    	     $datas['total_page'] = ceil($count/$post['limit']);
    	     $datas['count'] = $count;
            
            $follow = M('follow')
            ->field('a.*,b.nickname,b.quotations,b.head_pic')
    		->alias('a')
    	    ->join('users b', 'a.follow_id = b.users_id', 'LEFT')
    	    ->where(array('a.users_id' => $this->users_id))
    	    ->page($page,$limit)
    	    ->select();
    	   // dump($follow);die;
            $datas['status'] = 1;
            $datas['msg'] = '';
            $datas['res'] = $follow;
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit;  
        }else
        {
            
            $count = M('follow')
            ->field('a.*,b.nickname,b.quotations,b.head_pic')
    		->alias('a')
    		->join('users b', 'a.users_id = b.users_id', 'LEFT')
    	    ->where(array('a.follow_id' => $this->users_id))
            ->count();
              $datas['total_page'] = ceil($count/$post['limit']);
              $datas['count'] = $count;
            $follow = M('follow')
            ->field('a.*,b.nickname,b.quotations,b.head_pic')
    		->alias('a')
    		->join('users b', 'a.users_id = b.users_id', 'LEFT')
    	    ->where(array('a.follow_id' => $this->users_id))
    	    ->page($page,$limit)
    	    ->select();
	        $datas['status'] = 1;
            $datas['msg'] = '';
            $datas['res'] = $follow;
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit; 
        }
    }
    
    //关注我的用户列表
    
    
    
    //作者详情页
    
    public function author()
    {
         $users_id = input('param.users_id/d');
         $type_id = input('param.type_id/d');
         $page = input('param.page/d');
         $limit = input('param.limit/d');
         $post = input('post.');
        //  dump($users_id);die;
         $user = M('users')->where(array('users_id' => $users_id))->find();
         $users['users']['nickname'] = $user['nickname'];
         $users['users']['users_id'] = $user['users_id'];
         $users['users']['head_pic'] = $user['head_pic'];
         $users['users']['head_pic'] = $user['head_pic'];
         $users['users']['quotations'] = $user['quotations'];
          //作者发布的帖子/文章
          
         $count = M('weapp_ask')
         ->where(array('users_id' => $users_id,'type_id' => $type_id))
         ->count();
        //  dump($ask);die;
         
         
         $ask['ask']['ask'] = M('weapp_ask')
         ->where(array('users_id' => $users_id,'type_id' => $type_id))
         ->page($page,$limit)
         ->select();
        //  dump($ask['ask']);die;
         foreach ( $ask['ask']['ask'] as $k => $v)
         {
            if($this->users_id == 0)
            {
                 $ask['ask']['ask'][$k]['zan_content'] = M('weapp_zan')->where(array('aid'=> $v['ask_id'],'zan_count' => 1))->count();
                 $ask['ask']['ask'][$k]['zan'] = 0;
                 $ask['ask']['ask'][$k]['collection'] = 0;
            }else
            {
                 $ask['ask']['ask'][$k]['zan_content'] = M('weapp_zan')->where(array('aid'=> $v['ask_id'],'zan_count' => 1))->count();
                $zan = M('weapp_zan')->where(array('users_id'=> $this->users_id,'aid' => $v['ask_id'],'zan_count' => 1))->find();
                if($zan)
                {
                     $ask['ask']['ask'][$k]['zan'] = 1;
                }else
                {
                    $ask['ask']['ask'][$k]['zan'] = 0;
                }
               $collection = M('weapp_collection')->where(array('users_id'=> $this->users_id,'ask_id' => $v['ask_id']))->find();
               if($collection)
               {
                    $ask['ask']['ask'][$k]['collection'] = 1;
               }else
               {
                    $ask['ask']['ask'][$k]['collection'] = 0;
               }
                
            }
            $ask['ask']['ask'][$k]['content'] = htmlspecialchars_decode($v['content']);
            $ask['ask_id'] = $v['ask_id'];
            $ask['ask']['ask'][$k]['fild'] = $this->GetAskReplyData($ask,'-1');
            
           
         }
         $ask['ask']['total_page'] = ceil($ask['count']/$post['limit']);
         $ask['ask']['count'] = $count;
         
         //是否关注该用户
         
         
        //  dump($ask['ask']);die;
         
         
        //  dump($ask);die;
         
         //作者的评论
         
        $counts = M('weapp_ask_answer')
          ->field('a.*,b.ask_title')
          ->alias('a')
          ->join('weapp_ask b', 'a.ask_id= b.ask_id', 'LEFT')
          ->where(array('a.users_id' => $users_id))
          ->count();
        
        $answer['answer']['answer'] = M('weapp_ask_answer')
          ->field('a.*,b.ask_title')
          ->alias('a')
          ->join('weapp_ask b', 'a.ask_id= b.ask_id', 'LEFT')
          ->where(array('a.users_id' => $users_id))
          ->page($page,$limit)
          ->select();
          foreach( $answer['answer']['answer'] as $k => $v)
          {
               if($this->users_id == 0)
            {
                 $answer['answer']['answer'][$k]['zan_content'] = M('weapp_ask_answer_like')->where(array('answer_id' => $v['answer_id']))->count();
                  $answer['answer']['answer'][$k]['zan'] = 0;
            }else
            {
                $answer['answer']['answer'][$k]['zan_content'] = M('weapp_ask_answer_like')->where(array('answer_id' => $v['answer_id']))->count();
                $zan1 = M('weapp_ask_answer_like')->where(array('users_id'=> $this->users_id,'answer_id' => $v['answer_id']))->find();
                if($zan1)
                {
                       $answer['answer']['answer'][$k]['zan'] = 1;
                }else
                {
                    $answer['answer']['answer'][$k]['zan'] = 0;
                }
                
            }
            $answer['answer']['answer'][$k]['content'] = htmlspecialchars_decode($v['content']);
            
          }
          
          $answer['answer']['total_page'] = ceil($counts/$post['limit']);
          $answer['answer']['count'] = $counts;
          
          //dump($answer);die;
        //   $ask['ask_id'] = 
        //   foreach($answer['answer'] as $k => $v)
        //   {
        //       $ask['ask_id'] = $v['ask_id'];
        //       $answer['answer'][$k]['fild'] = $this->GetAskReplyData($ask,'-1');
        //   }
          
        //   dump($ask);die;
          
          //作者关注的数量
         $follow['follow'] = M('follow')->where(array('users_id' => $users_id))->count();
         
         //用户是否关注该作者
         
        $follow_ids = M('follow')->where(array('users_id' => $this->users_id,'follow_id' => $users_id))->find();
        if(!empty($follow_ids))
        {
            $users['users']['is_follow'] = 1;
        }else
        {
            $users['users']['is_follow'] = 0;
        }
         
         //关注作者的数量
         $follow['author'] = M('follow')->where(array('follow_id' => $users_id))->count();
         
         
        //  dump($answer);die;
        $result = array_merge($users, $ask, $answer, $follow);
        
        $data['status'] = 1;
        $data['msg'] = '';
        $data['res'] = $result;
        echo json_encode($data);
    }
    
    public function users_center()
    {
         $users_id = $this->users_id;
         $type_id = input('param.type_id/d');
         $page = input('param.page/d');
         $limit = input('param.limit/d');
         $post = input('post.');
        //  
         $user = M('users')->where(array('users_id' => $users_id))->find();
         $users['users']['nickname'] = $user['nickname'];
         $users['users']['users_id'] = $user['users_id'];
         $users['users']['head_pic'] = $user['head_pic'];
         $users['users']['head_pic'] = $user['head_pic'];
         
        $experience = M('users_experience')->where(array('users_id' => $user['users_id']))->find();
        $users['users']['background_map'] = $experience['background_map'];
         $users['users']['quotations'] = $experience['introduce'];
        //作者的评论
        // $answer['answer'] = M('weapp_ask_answer')
        //   ->field('a.*,b.ask_title')
        //   ->alias('a')
        //   ->join('weapp_ask b', 'a.ask_id= b.ask_id', 'LEFT')
        //   ->where(array('a.users_id' => $users_id))
        //   ->page($page,$limit)
        //   ->select();
        //   foreach($answer['answer'] as $k => $v)
        //   {
        //       $ask['ask_id'] = $v['ask_id'];
        //       $answer['answer'][$k]['fild'] = $this->GetAskReplyData($ask,'-1');
        //   }
        
        
        
        
         //作者的评论
         
        $answer['count'] = M('weapp_ask_answer')
          ->field('a.*,b.ask_title')
          ->alias('a')
          ->join('weapp_ask b', 'a.ask_id= b.ask_id', 'LEFT')
          ->where(array('a.users_id' => $this->users_id))
          ->count();
        
        $answer['answer'] = 
            M('weapp_ask_answer')
          ->field('a.*,b.ask_title')
          ->alias('a')
          ->join('weapp_ask b', 'a.ask_id= b.ask_id', 'LEFT')
          ->where(array('a.users_id' => $this->users_id))
          ->page($page,$limit)
          ->select();
        //   dump($this->users_id);die;
          foreach($answer['answer'] as $k => $v)
          {
               if($this->users_id == 0)
            {
                 $answer['answer'][$k]['zan_content'] = M('weapp_ask_answer_like')->where(array('answer_id' => $v['answer_id']))->count();
                 $answer['answer'][$k]['zan'] = 0;
            }else
            {
                $answer['answer'][$k]['zan_content'] = M('weapp_ask_answer_like')->where(array('answer_id' => $v['answer_id']))->count();
                $zan1 = M('weapp_ask_answer_like')->where(array('users_id'=> $this->users_id,'answer_id' => $v['answer_id']))->find();
                if($zan1)
                {
                      $answer['answer'][$k]['zan'] = 1;
                }else
                {
                    $answer['answer'][$k]['zan'] = 0;
                }
                
            }
           
                $answer['answer'][$k]['content'] = htmlspecialchars_decode($v['content']);
          }
          
    //   $answer['total_page'] = ceil($answer['count']/$post['limit']);
        
    //     $answer['count'] = $answer['count'];
          
          
         $count = M('weapp_ask')
         ->where(array('users_id' => $this->users_id,'type_id' => $type_id))
         ->count();
          
         $ask['ask']['ask']  = M('weapp_ask')
         ->where(array('users_id' => $this->users_id,'type_id' => $type_id))
         ->page($page,$limit)
         ->select();
         
         
         
         foreach ($ask['ask']['ask']  as $k => $v)
         {
            if($this->users_id == 0)
            {
                 $ask['ask']['ask'] [$k]['zan_content'] = M('weapp_zan')->where(array('aid'=> $v['ask_id'],'zan_count' => 1))->count();
                 $ask['ask']['ask'] [$k]['zan'] = 0;
                  $ask['ask']['ask'] [$k]['collection'] = 0;
            }else
            {
                $ask['ask']['ask'] [$k]['zan_content'] = M('weapp_zan')->where(array('aid'=> $v['ask_id'],'zan_count' => 1))->count();
                $zan = M('weapp_zan')->where(array('users_id'=> $this->users_id,'aid' => $v['ask_id'],'zan_count' => 1))->find();
                if($zan)
                {
                     $ask['ask']['ask'] [$k]['zan'] = 1;
                }else
                {
                    $ask['ask']['ask'] [$k]['zan'] = 0;
                }
               $collections = M('weapp_collection')->where(array('users_id'=> $this->users_id,'ask_id' => $v['ask_id']))->find();
               if($collections)
               {
                    $ask['ask']['ask'] [$k]['collection'] = 1;
               }else
               {
                   $ask['ask']['ask'] [$k]['collection'] = 0;
               }
                
            }
            $ask['ask_id'] = $v['ask_id'];
            $ask['ask']['ask'] [$k]['fild'] = $this->GetAskReplyData($ask,'-1');
            $ask['ask']['ask'] [$k]['content'] = htmlspecialchars_decode($v['content']);
            // dump($this->users_id);die;
         }
         
          $ask['ask']['total_page'] = ceil($count/$post['limit']);
          $ask['ask']['count'] = $count;
          
        
          
          //用户关注的数量
         $follow['follow'] = M('follow')->where(array('users_id' => $users_id))->count();
         
         //关注用户的数量
         $follow['author'] = M('follow')->where(array('follow_id' => $users_id))->count();
         
         //用户收藏个数
         $collection['collection_num'] = M('weapp_collection')->where(array('users_id' => $users_id))->count();
         //用户收藏文章列表
         $collection['collection'] = 
         M('weapp_collection')
         ->field('a.*,a.title as ask_title')
         ->alias('a')
         ->where(array('users_id' => $users_id))
         ->select();
         foreach($collection['collection'] as $k => $v)
         {
            $collection['collection'][$k]['zan'] = 
            M('weapp_zan')
             ->where(array('aid' => $v['ask_id']))
             ->count();
              $collection['collection'][$k]['collection'] = 1;
         }
         
         
         
         //用户点赞个数
         $ask1 = M('weapp_ask')
    		->where(array('users_id' => $this->users_id))
    		->getField('ask_id', true);
    		
    // 		dump($collection);die;
    		
    		
    		
         $zan['zan_num'] =M('weapp_zan')
		  ->field('a.*,b.ask_title,b.content,b.ask_id,c.nickname,c.head_pic')
		  ->alias('a')
		  ->join('weapp_ask b', 'a.aid = b.ask_id', 'LEFT')
		  ->join('users c','a.users_id=c.users_id','LEFT')
		  ->where(array('a.aid' => array('in',$ask1),'b.type_id' => $type_id,'zan_count' => 1))
		  ->count();
	    $zan['total_page'] = ceil($zan['zan_num']/$post['limit']);
	     $zan['count'] =  $zan['zan_num'];
         //用户点赞列表
  		$zan['zan_list'] = M('weapp_zan')
		  ->field('a.*,b.ask_title,b.content,b.ask_id,c.nickname,c.head_pic')
		  ->alias('a')
		  ->join('weapp_ask b', 'a.aid = b.ask_id', 'LEFT')
		  ->join('users c','a.users_id=c.users_id','LEFT')
		  ->where(array('a.aid' => array('in',$ask1),'b.type_id' => $type_id,'zan_count' => 1))
		  ->page($page,$limit)
		  ->select();
		  
		  foreach($zan['zan_list'] as $k => $v)
		  {
		        $collectiont = M('weapp_collection')->where(array('users_id'=> $this->users_id,'ask_id' => $v['ask_id']))->find();
               if($collectiont)
               {
                    $zan['zan_list'][$k]['collection'] = 1;
               }else
               {
                   $zan['zan_list'][$k]['collection'] = 0;
               }
		  }
		  
		  //dump($zan);die;
          
          
          //用户关注的用户
          
           $counte = M('follow')
           ->field('a.*,b.nickname,b.head_pic')
           ->alias('a')
           ->join('users b', 'a.follow_id= b.users_id', 'LEFT')
           ->where(array('a.users_id' => $users_id))
           ->page($page,$limit)
           ->count();
          
         $follow['follow_list']['follow_list'] = M('follow')
           ->field('a.*,b.nickname,b.head_pic')
           ->alias('a')
           ->join('users b', 'a.follow_id= b.users_id', 'LEFT')
           ->where(array('a.users_id' => $users_id))
           ->page($page,$limit)
           ->select();
           
	     $follow['follow_list']['total_page'] = ceil($counte/$post['limit']);
	     $follow['follow_list']['count'] =  $counte;
           
        //   dump($follow);die;
           
           //当前用户的文章
          $users_ask = M('weapp_ask')->where(array('users_id' => 1,'is_review' =>1,'type_id' => $type_id))->getField('ask_id', true);
        //   dump($users_ask);die;
          
          foreach($users_ask as $k => $v)
          {
             $rest['ask_id'] = $v;
             $users_wenzhang[]=  $this->GetAskDetailsData($rest,session('admin_info.parent_id'),$this->users_id);
            //  $users_wenzhang[]['comment'] = $this->GetAskReplyData($rest,'-1');
          }
          foreach($users_wenzhang as $k => $v)
          {
               $info['ask_id'] = $v['info']['ask_id'];
               $v['info']['comment'] = $this->GetAskReplyData($info,'-1');
               $users_wenzhang[$k]['info']['comment'] = $v['info']['comment'];
          }
            // dump($collection);die;
          $result = array_merge($users, $ask, $answer, $follow,$collection,$zan,$users_wenzhang);
        //   dump($ask);die;
        $data['status'] = 1;
        $data['msg'] = '';
        $data['res'] = $result;
        echo json_encode($data);
        exit;
    }
    
    
    
     public function GetAskReplyData($param = array(), $parent_id = null)
    {
        // dump($param);
        // dump($parent_id);
    	/*查询条件*/
    	$bestanswer_id = $this->weapp_ask_db->where('ask_id', $param['ask_id'])->getField('bestanswer_id');
		$RepliesWhere = ['ask_id' => $param['ask_id'], 'is_review' => 1];
        $WhereOr = [];
        if (!empty($param['answer_id'])) {
            $RepliesWhere = ['answer_id' => $param['answer_id'], 'is_review' => 1];
            $WhereOr = ['answer_pid' => $param['answer_id']];
        }

        // 若为则创始人则去除仅查询已审核评论这个条件，$parent_id = 0 表示为创始人
        if (0 == $parent_id) unset($RepliesWhere['is_review']);
    	/* END */

        /*评论读取条数*/
        $firstRow = !empty($param['firstRow']) ? $param['firstRow'] : 0;
        $listRows = !empty($param['listRows']) ? $param['listRows'] : 5;
        $result['firstRow'] = $firstRow;
        $result['listRows'] = $listRows;
        /* END */

    	/*排序*/
        $OrderBy = !empty($param['click_like']) ? 'a.click_like '.$param['click_like'].', a.add_time asc' : 'a.add_time asc';
        $click_like = isset($param['click_like']) ? $param['click_like'] : '';
        $result['SortOrder'] = 'desc' == $click_like ? 'asc' : 'desc';
		/* END */

    	/*评论回答*/
    	$RepliesData = $this->weapp_ask_answer_db->field('a.*, b.head_pic, b.nickname, c.nickname as `at_usersname`')
            ->alias('a')
            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
            ->join('__USERS__ c', 'a.at_users_id = c.users_id', 'LEFT')
            ->order($OrderBy)
            ->where($RepliesWhere)
            ->WhereOr($WhereOr)
            ->select();
        if (empty($RepliesData)) return [];
        /* END */

        /*点赞数据*/
        $AnswerIds = get_arr_column($RepliesData, 'answer_id');
        $AnswerLikeData = $this->weapp_ask_answer_like_db->field('a.*, b.nickname')
            ->alias('a')
            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
            ->order('like_id desc')
            ->where('answer_id','IN',$AnswerIds)
            ->select();
		$AnswerLikeData = group_same_key($AnswerLikeData, 'answer_id');
		/* END */
		
        /*回答处理*/
        $PidData = $AnswerData = [];
        foreach ($RepliesData as $key => $value) {
        	// 友好显示时间
        	$value['add_time'] = friend_date($value['add_time']);
            // 处理格式
            $value['content'] = htmlspecialchars_decode($value['content']);
            // 头像处理
            $value['head_pic'] = get_head_pic($value['head_pic']);
            // 会员昵称
            $value['nickname'] = !empty($value['nickname']) ? $value['nickname'] : $value['username'];
            
             $value['content'] =  htmlspecialchars_decode($value['content']);

        	// 是否上一级回答
            if($value['answer_pid'] == 0){
                $PidData[]	  = $value;
            }else{
                $AnswerData[] = $value;
            }
        }
        /* END */

        /*一级回答*/
        foreach($PidData as $key => $PidValue){
            $result['AnswerData'][] = $PidValue;
            // 子回答
            $result['AnswerData'][$key]['AnswerSubData'] = [];
            // 点赞数据
            $result['AnswerData'][$key]['AnswerLike'] = [];

            /*所属子回答处理*/
            foreach($AnswerData as $AnswerValue){
                if($AnswerValue['answer_pid'] == $PidValue['answer_id']){
                     $LikeNums = M('weapp_ask_answer_like')->where(array('answer_id' => $AnswerValue['answer_id']))->count();
    	             $AnswerValue['AnswerLike']['LikeNums'] = $LikeNums;
                    array_push($result['AnswerData'][$key]['AnswerSubData'], $AnswerValue);
                }
            }
            /* END */

            /*读取指定数据*/
            // 以是否审核排序，审核的优先
            array_multisort(get_arr_column($result['AnswerData'][$key]['AnswerSubData'],'is_review'), SORT_DESC, $result['AnswerData'][$key]['AnswerSubData']);
            // 读取指定条数
            $result['AnswerData'][$key]['AnswerSubData'] = array_slice($result['AnswerData'][$key]['AnswerSubData'], $firstRow, $listRows);
            
            /* END */

            $result['AnswerData'][$key]['AnswerLike']['LikeNum']  = null;
            $result['AnswerData'][$key]['AnswerLike']['LikeName'] = null;
            /*点赞处理*/
            foreach ($AnswerLikeData as $LikeKey => $LikeValue) {
            	if($PidValue['answer_id'] == $LikeKey){
            		// 点赞总数
            		$LikeNum = count($LikeValue);
            		$result['AnswerData'][$key]['AnswerLike']['LikeNum'] = $LikeNum;
            		for ($i=0; $i < $LikeNum; $i++) {
            			// 获取前三个点赞人处理后退出本次for
            			if ($i > 2) break;
            			// 点赞人用户名\昵称
            			$LikeName = $LikeValue[$i]['nickname'];
            			// 在第二个数据前加入顿号，拼装a链接
            			if ($i != 0) {
            				$LikeName = ' 、<a href="javascript:void(0);">'. $LikeName .'</a>';
            			}else{
            				$LikeName = '<a href="javascript:void(0);">'. $LikeName .'</a>';
            			}
            			$result['AnswerData'][$key]['AnswerLike']['LikeName'] .= $LikeName;
            		}
            	}
            }
            /* END */
        }
        /* END */

        /*最佳答案数据*/
        foreach ($result['AnswerData'] as $key => $value) {
        	if ($bestanswer_id == $value['answer_id']) {
        		$result['BestAnswer'][$key] = $value;
        	}
        }
        /* NED */
        
        // 统计回答数
        $result['AnswerCount'] = count($RepliesData);
        return $result;
    }
    
    
    public function GetAskDetailsData($param = array(),  $parent_id = null, $users_id = null)
    {
        // dump($param);die;
    	$ResultData['code'] = 1;
    	$ResultData['info'] = $this->weapp_ask_db->field('a.*, b.username, b.nickname, b.head_pic, c.type_name')
            ->alias('a')
            ->join('__WEAPP_ASK_TYPE__ c', 'c.type_id = a.type_id', 'LEFT')
            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
            ->where('ask_id', $param['ask_id'])
            ->find();
		if (empty($ResultData['info'])) return ['code'=>0,'msg'=>'浏览的问题不存在！'];
		if (0 !== $parent_id) {
			if (0 == $ResultData['info']['is_review'] && $ResultData['info']['users_id'] !== $users_id){
				return ['code'=>0,'msg'=>'问题未审核通过，暂时不可浏览！'];
			}
		}

        // 头像处理
        $ResultData['info']['head_pic'] = get_head_pic($ResultData['info']['head_pic']);
        
        //用户点赞的数量
        $ResultData['info']['LikeNum'] = M('weapp_zan')->where(array('aid' => $ResultData['info']['ask_id'],'zan_count' => 1))->count();
        
        //用户发帖总数
        $ResultData['info']['usersNum'] = M('weapp_ask')->where(array('users_id' => $ResultData['info']['users_id']))->count();
        
        //用户粉丝数量
         $ResultData['info']['followNum'] = M('follow')->where(array('follow_id' => $ResultData['info']['users_id']))->count();
         
         //用户是否关注了该篇文章
         
        $collection = M('weapp_collection')->where(array('ask_id' => $ResultData['info']['ask_id'],'users_id' => $users_id))->find();
        
        if($collection)
        {
             $ResultData['info']['$collection'] = 1;
        }else
        {
             $ResultData['info']['collection'] = 0;
        }
         
         
         //当前访问用户是否关注
       $follow = M('follow')->where(array('follow_id' => $ResultData['info']['users_id'],'users_id' => $users_id))->find();
       if($follow)
       {
            $ResultData['info']['follow'] = 1;
       }else
       {
            $ResultData['info']['follow'] = 0;
       }
        //用户是否点赞
        $res = M('weapp_zan')->where(array('aid' => $	$ResultData['info']['ask_id'],'users_id' => $users_id,'zan_count' => 1))->find();
        if($res)
        {
             $ResultData['info']['zan'] = 1;
        }else
        {
             $ResultData['info']['zan'] = 0;
        }

        // 时间友好显示处理
        $ResultData['info']['add_time'] = $ResultData['info']['add_time'];
        $ResultData['IsUsers'] = 0;
        if ($ResultData['info']['users_id'] == session('users_id')) $ResultData['IsUsers'] = 1;

        // 处理格式
        $ResultData['info']['content'] = htmlspecialchars_decode($ResultData['info']['content']);

        $ResultData['SearchName'] = null;
        // seo信息
        $ResultData['info']['seo_title'] = $ResultData['info']['ask_title'] . ' - ' . $ResultData['info']['type_name'];
        $ResultData['info']['seo_keywords'] = $ResultData['info']['ask_title'];
        $ResultData['info']['seo_description'] = @msubstr(checkStrHtml($ResultData['info']['content']), 0, config('global.arc_seo_description_length'), false);
        return $ResultData;
    }
    
    //个人信息
    public function users_details()
    {
        $users_id = $this->users_id;
        // dump($users_id);die;
        
        $users = M('users')
          ->field('b.*,a.nickname,a.province,a.city,a.district,a.head_pic,a.users_id')
          ->alias('a')
          ->join('users_experience b', 'a.users_id= b.users_id', 'LEFT')
          ->where(array('a.users_id' => $users_id))
           ->find();
           
        $users['province_id'] = $users['province'];
        $users['city_id']     = $users['city'];
        $users['district_id'] = $users['district'];
           
        $users['province'] = get_province_name($users['province']);
        $users['city']     = get_city_name($users['city']);
        $users['district'] = get_area_name($users['district']);
        if(empty($users['occupation']))
        {
            $users['occupation'] = array(
                array(
                'name' => '',
                'job' => ''
        )   
                );
        }else
        {
            $users['occupation'] = json_decode($users['occupation'], true);
        }
        if(empty($users['education']))
        {
            $users['education'] = array(
              array(  'school' => '',
                'professional' => '',
                'education' => '')
                );
        }else
        {
            $users['education'] = json_decode($users['education'], true);
        }
        if(empty($users['industry']))
        {
            $users['industry'] = array(
                array(
                'industry' => ''
                )
                );
        }else
        {
            $users['industry'] = json_decode($users['industry'], true);
        }
        
        // dump($users);die;
        
        $beginToday=mktime(0,0,0,date('m'),date('d'),date('Y'));
        $endToday=mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
        $wheres['add_time'] = array('between', array($beginToday,$endToday));
        $data['status'] = 1;
        $data['msg'] = '';
        $data['res'] = $users;
        echo json_encode($data);
        exit;
        // $this->success('','',$users);
    }
    
    
    
    
    //身份认证
    
    public function authentication()
    {
        $users_id = input('param.users_id/d');
        
        $users = M('users_authentication')
           ->field('a.*,b.nickname,b.province,b.city,b.district,b.head_pic,c.background_map,c.personal_description')
           ->alias('a')
           ->join('users b', 'a.users_id= b.users_id', 'LEFT')
           ->join('users_experience c', 'a.users_id= c.users_id', 'LEFT')
           ->where(array('a.users_id' => $users_id))
           ->find();
        $data['status'] = 1;
        $data['msg'] = '';
        $data['res'] = $users;
        echo json_encode($data);
        exit;
        //   dump($users);die;
        //  $this->success('','',$users);
    }
    
    public function edit_authentication()
    {
        if (IS_POST) {
             $param =  input('post.');
             $param['users_id'] = $this->users_id;
             $users['users_id'] = $this->users_id;
             $users['nickname'] = $param['nickname'];
             $users_id = M('users')->where(array('users_id' => $this->users_id))->save($users);
             $data['users_id'] = $param['users_id'];
             $data['background_map'] = $param['background_map'];
             $data['personal_description'] = $param['personal_description'];
             $res = M('users_experience')->where(array('users_id' => $this->users_id))->save($data);
             $param['add_time'] = time();
             if(!empty($param['id']))
             {
               $res = M('users_authentication')->where(array('id' => $param['id']))->save($param);
             }else
             {
                  $res = M('users_authentication')->save($param);
             }
             if($res)
             {
                $data['status'] = 1;
                $data['msg'] = '提交成功';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;
                //  $this->success('提交成功','','');
             }
            
        }
    }
    
    //编辑个人信息
     //$users_id = input('param.users_id/d');
    public function edit_users()
    {
         if (IS_POST) {
                $param =  input('post.');
                $users['nickname'] = $param['nickname'];
                $users['users_id'] = $this->users_id;
                $users['province'] = $param['province'];
                $users['city'] = $param['city'];
                $users['district'] = $param['district'];
                $users['head_pic'] = $param['head_pic'];
                if(!empty($param['userna']))
                {
                     $users['username'] = $param['userna'];
                }
               
                // dump(mb_strlen($users['nickname'],'UTF8'));die;
                if (mb_strlen($users['nickname'],'UTF8') > 15) {
                        $data['status'] = 1;
                        $data['msg'] = '您的昵称长度超过15个字符,请重新填写';
                        $data['res'] = '';
                        echo json_encode($data,JSON_UNESCAPED_UNICODE);
                        exit; 
                }
              if (mb_strlen($users['username'],'UTF8') > 15) {
                    $data['status'] = 1;
                    $data['msg'] = '您的真实姓名长度超过15个字符,请重新填写';
                    $data['res'] = '';
                    echo json_encode($data,JSON_UNESCAPED_UNICODE);
                    exit; 
                }
                // dump($users);die;
                if($param['status'] == 2)
                {
                     $users_id = M('users')->where(array('users_id' => $this->users_id))->save($users);
                     if($users_id)
                     {
                        $data['status'] = 1;
                        $data['msg'] = '修改成功';
                        $data['res'] = '';
                        echo json_encode($data,JSON_UNESCAPED_UNICODE);
                        exit; 
                     }
                }else
                {
                    // dump($users);die;
                     $users_id = M('users')->where(array('users_id' => $this->users_id))->save($users);
                }
               
                // dump($users_id);die;
                $param['add_time'] = time();
                
                if(empty($param['status']))
                {
                    $param['occupation'] = json_encode($param['occupation'],JSON_UNESCAPED_UNICODE);
                    $param['education'] = json_encode($param['education'],JSON_UNESCAPED_UNICODE);
                    $param['industry'] = json_encode($param['industry'],JSON_UNESCAPED_UNICODE);
                }
                

                // dump($param);die;
                if(!empty($param['id']))
                {
                    $res = M('users_experience')->where(array('id' => $param['id']))->save($param);
                }else
                {
                    if($param['status'] == 1)
                    {
                        $ret['background_map'] = $param['background_map'];
                        // dump($this->users_id);die;
                         $res = M('users_experience')->where(array('users_id' => $this->users_id))->find();
                         if(empty($res))
                         {
                            //  dump(11);die;
                             $ret['users_id'] = $this->users_id;
                              $res = M('users_experience')->add($ret);
                         }else
                         {
                             $res = M('users_experience')->where(array('users_id' => $this->users_id))->save($ret); 
                         }
                         
                        //  dump($res);die;
                    }else
                    {
                        
                        $res = M('users_experience')->where(array('users_id' => $this->users_id))->find();
                         if(empty($res))
                         {
                             unset($param['id']);
                              $res = M('users_experience')->add($param);
                         }else
                         {
                            M('users_experience')->where(array('users_id' => $this->users_id))->save($param);  
                         }
                    }
                }
                    $data['status'] = 1;
                    $data['msg'] = '修改成功';
                    $data['res'] = '';
                    echo json_encode($data,JSON_UNESCAPED_UNICODE);
                    exit;
            }
    }
    
    //更换手机号码
    public function users_phone()
    {
         if (IS_POST) {
            $param =  input('post.');
            
            if(empty($param['phone']))
            {
                $data['status'] = -1;
                $data['msg'] = '请输入手机号码';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;  
            }
           if(empty($param['code']))
            {
                $data['status'] = -1;
                $data['msg'] = '请输入验证码';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;  
            }
            $users =  M('users')->where(array('mobile' => $param['phone']))->find();
            //  dump($users);
            if (!empty($users))
            {
                $data['status'] = -1;
                $data['msg'] = '该手机号已绑定';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;
            }
            $res['mobile'] =$param['phone'];
           $login =  M('sms_log')->where(array('mobile' => $param['phone'],'source' =>6,'is_use' => 0))->order('id desc')->find();
        //   dump($login);die;
           if(empty($login))
           {
                $data['status'] = -1;
                $data['msg'] = '您输入正确的验证码';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit; 
           }
        //   dump($login);die;
           if ($login['status'] == 1) {
                 
                //  dump($login);die;
                if ($param['code'] != $login['code']) {
                   
                    $data['status'] = -1;
                    $data['msg'] = '您输入的验证码错误';
                    echo json_encode($data,JSON_UNESCAPED_UNICODE);
                    exit;
                   
                //   $this->error('您输入的验证码错误！', null, ['status' => -1]);
               }
               if ($login['add_time'] + 1800 < time())
                {
                    $data['status'] = -1;
                    $data['msg'] = '验证码已过期,请重新发送！';
                    echo json_encode($data,JSON_UNESCAPED_UNICODE);
                    exit;
                    // $this->error('验证码已过期,请重新发送！', null, ['status' => 1]);
                }
            }
            $users_id = M('users')->where(array('users_id' => $this->users_id))->save($res);
           if($users_id)
           {
                $use['is_use'] = 1;
                M('sms_log')->where(array('id' => $login['id']))->save($use);
                $data['status'] = 1;
                $data['msg'] = '修改成功';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit; 
           }else
           {
                $data['status'] = -1;
                $data['msg'] = '修改失败';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;  
           }
            
         }
    }
    
    public function email_code()
    {
        if (IS_POST) {
            $param =  input('post.');
        //   dump($this->users_id);die;
          $email = M('users')->where(array('email' => $param['email']))->find(); 
          if(!empty($email))
          {
               $data['status'] = -1;
                $data['msg'] = '该邮箱已绑定';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;
          }
          
            if (!filter_var($param['email'], FILTER_VALIDATE_EMAIL)) {
               $data['status'] = -1;
                $data['msg'] = '请输入正确的邮箱';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;
            }
                        
            $title = '绑定邮箱验证码';
            $code = mt_rand(1000,9999);
            $content = '邮箱验证码：' .$code.',有效期5分钟';
            $smtp_config =  array(
                'smtp_server' => "smtp.qq.com",
                'smtp_port'   => "465",
                'smtp_user'   => "vietrue.jie@qq.com",
                'smtp_pwd'    => "xgktcfyifeoghdha",
                'smtp_from_eamil' => "352628580@qq.com",
                );
            // dump($content);die;
            $res = send_email($param['email'], $title, $content, 0, $smtp_config);
            // dump($res);die;
            if($res)
            {
                
                // dump()
                $datas['users_id'] = $this->users_id;
                $datas['email'] = $param['email'];
                $datas['source'] = 3;
                $datas['code'] = $code;
                $datas['status'] = 0;
                $datas['add_time'] = time();
                
                 $this->smtp_record_db->add($datas);
                
                $data['status'] = 1;
                $data['msg'] = '发送成功';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;  
            }
            // dump($res);die;
        } 
    }
    
    //更换绑定邮箱
    
    public function users_email()
    {
        if (IS_POST) {
            $param =  input('post.');
            if(empty($param['email']))
            {
                $data['status'] = -1;
                $data['msg'] = '请输入邮箱';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;  
            }
           if(empty($param['email_code']))
            {
                $data['status'] = -1;
                $data['msg'] = '请输入邮箱验证码';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;  
            }
            
             $users =  M('users')->where(array('email' => $param['email']))->find();
            //  dump($users);die;
            if (!empty($users))
            {
                $data['status'] = -1;
                $data['msg'] = '该邮箱已绑定';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;
            }
            
          $login =  M('smtp_record')->where(array('email' => $param['email'],'source' =>3,'status' => 0,'users_id' => $this->users_id))->order('record_id desc')->find();
          if(empty($login))
          {
                $data['status'] = -1;
                $data['msg'] = '您没有发送验证码,请发送！';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit; 
          }
        //   dump($login);die;
             if ($login['status'] == 0) {
                 
                //  dump($login);die;
                if ($param['email_code'] != $login['code']) {
                   
                    $data['status'] = -1;
                    $data['msg'] = '您输入的验证码错误';
                    echo json_encode($data,JSON_UNESCAPED_UNICODE);
                    exit;
                   
                //   $this->error('您输入的验证码错误！', null, ['status' => -1]);
               }
               if ($login['add_time'] + 1800 < time())
                {
                    $data['status'] = -1;
                    $data['msg'] = '验证码已过期,请重新发送！';
                    echo json_encode($data,JSON_UNESCAPED_UNICODE);
                    exit;
                    // $this->error('验证码已过期,请重新发送！', null, ['status' => 1]);
                }
            }
          
            
           $res['email'] =$param['email'];
            
           $users_id = M('users')->where(array('users_id' => $this->users_id))->save($res);
           if($users_id)
           {
               $rett['status'] = 1;
                $login =  M('smtp_record')->where(array('email' => $param['email'],'source' =>3,'status' => 0,'users_id' => $this->users_id))->save($rett);
                $data['status'] = 1;
                $data['msg'] = '绑定成功';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit; 
           }else
           {
                $data['status'] = -1;
                $data['msg'] = '绑定失败';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit;  
           }
        }
    }
    
    public function resources()
    {
        
        // dump($this->users_id);die;
        $param = input('post.');
        $users = M('users')->where(array('users_id' => $this->users_id))->find();
        //总积分
        $data['scores'] = $users['scores'];
        //签到总天数
        $data['scores_day'] = M('users_score')->where(array('users_id' => $this->users_id,'type' => 5))->count();
       
       //本周的日期+周几
       $this_week_num = date('w');
 
        $timestamp = time();
        //如果获取到的日期是周日，需要把时间戳换成上一周的时间戳
        //英语国家 一周的开始时间是周日
        if($this_week_num == 0){
            $timestamp = $timestamp - 86400;
        }
     
        $this_week_arr =  [
            [
                'is_sign'=>0,
                'this_week'=>1,
                'week_name'=>'星期一',
                'week_time'=>strtotime(date('Y-m-d', strtotime("this week Monday", $timestamp))),
                'date'  => $dt=date('Y-m-d',strtotime(date('Y-m-d', strtotime("this week Monday", $timestamp))))
            ],
            [
                'is_sign'=>0,
                'this_week'=>2,
                'week_name'=>'星期二',
                'week_time'=>strtotime(date('Y-m-d', strtotime("this week Tuesday", $timestamp))),
                'date'  => $dt=date('Y-m-d',strtotime(date('Y-m-d', strtotime("this week Tuesday", $timestamp))))
            ],
            [
                'is_sign'=>0,
                'this_week'=>3,
                'week_name'=>'星期三',
                'week_time'=>strtotime(date('Y-m-d', strtotime("this week Wednesday", $timestamp))),
                'date'  => $dt=date('Y-m-d',strtotime(date('Y-m-d', strtotime("this week Wednesday", $timestamp))))
            ],
            [
                'is_sign'=>0,
                'this_week'=>4,
                'week_name'=>'星期四',
                'week_time'=>strtotime(date('Y-m-d', strtotime("this week Thursday", $timestamp))),
                'date'  => $dt=date('Y-m-d',strtotime(date('Y-m-d', strtotime("this week Thursday", $timestamp))))
            ],
            [
                'is_sign'=>0,
                'this_week'=>5,
                'week_name'=>'星期五',
                'week_time'=>strtotime(date('Y-m-d', strtotime("this week Friday", $timestamp))),
                'date'  => $dt=date('Y-m-d',strtotime(date('Y-m-d', strtotime("this week Friday", $timestamp))))
            ],
            [
                'is_sign'=>0,
                'this_week'=>6,
                'week_name'=>'星期六',
                'week_time'=>strtotime(date('Y-m-d', strtotime("this week Saturday", $timestamp))),
                'date'  => $dt=date('Y-m-d',strtotime(date('Y-m-d', strtotime("this week Saturday", $timestamp))))
            ],
            [
                'is_sign'=>0,
                'this_week'=>7,
                'week_name'=>'星期天',
                'week_time'=>strtotime(date('Y-m-d', strtotime("this week Sunday", $timestamp))),
                'date'  => $dt=date('Y-m-d',strtotime(date('Y-m-d', strtotime("this week Sunday", $timestamp))))
            ],
        ];
        $score = M('users_score')->where(array('users_id' => $this->users_id,'type' => 5))->whereTime('add_time','>','week')->select();
        
        // dump($score);die;
        foreach($score as $k => $v)
        {
            $score[$k]['time'] = date('Y-m-d',$v['add_time']);
        }
        foreach($this_week_arr as $kk => $vv )
        {
            foreach($score as $kv => $vk)
            {
                if($vv['date'] == $vk['time'])
                {
                    $this_week_arr[$kk]['sign'] = 1;
                }
            }
        }
        
        
        
        // dump($this_week_arr);die;
        
        $a = 0;
        
        foreach ($this_week_arr as $k => $v)
        {
            if($v['week_time'] < time())
            {
                if(empty($v['sign']))
                {
                    $a = 0;
                }else
                {
                   $a = $a+1;
                }
                // dump($v);
            }
        }
        
        $data['tian'] = $a;
        // dump($a);die;
        $data['this_week_arr'] = $this_week_arr;
        
        $count =  M('users_score')
        ->where(array('users_id' => $this->users_id))
        ->count();
        
      $datas['total_page'] = ceil($count/$param['limit']);
      $datas['count'] = $count;
        
        $users_score =  M('users_score')
        ->where(array('users_id' => $this->users_id))
        ->order('id desc')
        ->page($param['page'],$param['limit'])
        ->select();
        
        
        foreach($users_score as $k => $v)
        {
            $users_score[$k]['add_time'] = date('Y-m-d',$v['add_time']);
        }
        
        
        
        $data['users_score'] = $users_score;
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $data;
        
        // dump($datas);die;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit; 
    }
    
    public function users_identity()
    {
        $param = input('post.');
        if(empty($param['username']))
        {
            $datas['status'] = -1;
            $datas['msg'] = '请填写您的姓名';
            $datas['res'] = $data;
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit; 
        }
        if(empty($param['email']))
        {
            $datas['status'] = -1;
            $datas['msg'] = '请填写邮箱';
            $datas['res'] = $data;
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit; 
        }
         if(empty($param['identity_id']))
        {
            $datas['status'] = -1;
            $datas['msg'] = '请输入身份证号';
            $datas['res'] = $data;
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit; 
        }
         if(empty($param['pay_taxes']))
        {
            $datas['status'] = -1;
            $datas['msg'] = '请输入纳税识别号';
            $datas['res'] = $data;
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit; 
        }
        
        $param['add_time'] = time();
        $param['users_id'] = $this->users_id;
        
       $users = M('users_identity')->where(array('users_id' => $this->users_id))->find();
       $param['examine'] = 0;
    //   dump($param);die;
        if(empty($users))
        {
            $param['picture'] = implode(',',$param['picture']);
            $res = M('users_identity')->add($param);
        }else
        {
            $param['picture'] = implode(',',$param['picture']);
            // dump($param['picture']);die;
           $res = M('users_identity')->where(array('users_id' => $this->users_id))->save($param);
        }
        if($res)
        {
            $datas['status'] = 1;
            $datas['msg'] = '提交成功，正在审核';
            $datas['res'] = $data;
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit; 
        }
    }
    
    
    
    //优惠劵列表
    
    public function coupon()
    {
        
        $cou['use_status'] = 2;
        $shop_coupon_use = M('shop_coupon_use')->where(array('use_status' => 0))->where('end_time < '.time())->save($cou);
        // dump($shop_coupon_use);die;
        
        $coupon = M('shop_coupon')->where(array('is_del' =>0))->select();
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $coupon;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit; 
    //   dump($coupon);die;
    }
    
    //用户领取优惠劵
    public function receive_coupon()
    {
         $param =  input('post.');
         $param['get_ip'] = clientIP();
         $param['get_time'] = time();
         $param['use_status'] = 0;
         $param['add_time'] = time();
         $param['update_time'] = time();
         $param['users_id'] = $this->users_id;
         if( M('shop_coupon_use')->where(array('users_id' => $this->users_id,'coupon_code' => $param['coupon_code']))->find())
         {
            $datas['status'] = -1;
            $datas['msg'] = '该优惠劵您已领取';
            $datas['res'] = $data;
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit; 
         }else
         {
             $coupon = M('shop_coupon_use')->add($param);
             if($coupon)
             {
                $shop_coupon = M('shop_coupon')->where(array('coupon_id' => $param['coupon_id']))->find();
                $rest['coupon_stock'] = $shop_coupon['coupon_stock'] -1;
                M('shop_coupon')->where(array('coupon_id' => $param['coupon_id']))->save($rest);
                $datas['status'] = 1;
                $datas['msg'] = '领取成功';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit; 
             }  
         }
    }
    
    public function users_coupon()
    {
        
       $coupon = M('shop_coupon_use')->where(array('users_id' => $this->users_id))->select();
       foreach($coupon as $k => $v)
       {
        //   dump($v);die;
           if($v['end_time']<time())
           {
               $res['use_status'] = 2;
               M('shop_coupon_use')->where(array('use_id' => $v['use_id']))->save($res);
           }
       }
       
    //   M('shop_coupon_use')
        
       $coupon_use = M('shop_coupon_use')
        ->field('b.*,a.use_status,a.get_time,a.use_time,a.use_id')
        ->alias('a')
        ->join('shop_coupon b', 'b.coupon_id = a.coupon_id', 'LEFT')
        ->where(array('a.users_id' => $this->users_id))
        ->select();
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $coupon_use;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit; 
    }
    
    
    //会有等级
    
    public function level()
    {
        $level = M('users_level')->select();
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $level;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit; 
    }
    
    //用户会员等级
    
    public function interests()
    {
       $users = M('users')
        ->field('a.nickname,a.head_pic,b.level_name,a.total_amount')
        ->alias('a')
        ->join('users_level b','b.level_id=a.level', 'LEFT')
        ->where(array('a.users_id' => $this->users_id))
        ->find();
        $users['total_amount'] = $users['total_amount']*10;
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $users;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit; 
    }
    
    //用户享受权益
    public function users_interests()
    {
        // $post = input('post.');
        
        
        $archives =
        M('archives')
        ->field('a.title,a.litpic,b.explain,b.discount,c.level_name,c.level_value')
        ->alias('a')
        ->join('interests_content b','b.aid=a.aid', 'LEFT')
         ->join('users_level c','c.level_value=a.arcrank', 'LEFT')
        ->where(array('typeid' => 75))
        ->select();
        
        // dump($archives);
        
          $users = M('users')
        ->field('b.*')
        ->alias('a')
        ->join('users_level b','b.level_id=a.level', 'LEFT')
        ->where(array('a.users_id' => $this->users_id))
        ->find();
        // dump($users);die;
        
        foreach($archives as $k => $v)
        {
            if($v['level_value'] >= $users['level_value'])
            {
                $archives[$k]['status'] = 1;
            }else
            {
                 $archives[$k]['status'] = 0;
            }
        }
        // dump($archives);die;
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $archives;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit; 
    }
    
    //技术服务
    public function technical_service()
    {
         $param =  input('post.');
         
         $param['attr_28'] = implode(',',$param['attr_28']);
         $this->channel = !empty($channeltype_list['guestbook']) ? $channeltype_list['guestbook'] : 8;
         
         if(mb_strlen($param['attr_26']) > 50)
         {
            $datas['status'] = -1;
            $datas['msg'] = '您输入的问题类型过长请调整';
            $datas['res'] = '';
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit;  
         }
         
        //  dump(mb_strlen($param['attr_27']));die;
        if(mb_strlen($param['attr_27']) > 300)
         {
            $datas['status'] = -1;
            $datas['msg'] = '您描述的问题过长，请重新输入';
            $datas['res'] = '';
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit;  
         }
         
          if(!is_numeric($param['attr_29']))
         {
            $datas['status'] = -1;
            $datas['msg'] = '请输入正确的价格';
            $datas['res'] = '';
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit;  
         }
         
         
        //  mb_strlen($str,‘UTF8‘);
         
         $ip = clientIP();
         $newData = array(
                'channel'     => $this->channel,
                'ip'          => $ip,
                'lang'        => $this->home_lang,
                'add_time'    => getTime(),
                'update_time' => getTime(),
            );
            $data    = array_merge($param, $newData);
            $md5data         = md5(serialize($data));
            $data['md5data'] = $md5data;
            $rest['typeid'] = $data['typeid'];
            $rest['channel'] = $data['channel'];
            $rest['md5data'] = $data['md5data'];
            $rest['ip'] = $ip;
            $rest['is_read'] = 0;
            $rest['lang'] = $this->home_lang;
            $rest['add_time'] = time();
            
            $res = M('guestbook')->add($rest);
            if($res)
            {
               foreach ($param as $key => $value) {
                    if (stripos($key, "attr_") !== false) {
                        //处理得到自定义属性id
                        $attr_id = substr($key, 5);
                        $attr['attr_id'] = intval($attr_id);
                        $attr['aid'] = $res;
                        $attr['attr_value'] = $value;
                        $attr['lang'] = $this->home_lang;
                        $attr['add_time'] = time();
                        M('guestbook_attr')->add($attr);
                    }
                }
                 $datas['status'] = 1;
                $datas['msg'] = '提交成功';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit; 
            }else
            {
                $datas['status'] = 1;
                $datas['msg'] = '提交失败';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;  
            }
    }
    
    
    //私信
    public function private_list()
    {
        $post = input('post.');
        
        // $this->users_id =
        
        $res  = M('users_private')
        ->field('a.*,b.username,b.head_pic')
        ->alias('a')
        ->join('users b','b.users_id=a.users_id', 'LEFT')
        ->where(array('a.users_id' => $this->users_id,'a.private_id' => $post['users_id']))
        ->select();
        // dump($res);die;
        $ress  = M('users_private')
        ->field('a.*,b.username,b.head_pic')
        ->alias('a')
        ->join('users b','b.users_id=a.users_id', 'LEFT')
        ->where(array('a.users_id' => $post['users_id'],'a.private_id' => $this->users_id))
        ->select();
        // dump($res);
        // dump($ress);die;
        $private = array_merge($res,$ress);
        foreach($private as $k => $v)
        {
            if($v['users_id'] == $this->users_id)
            {
                $private[$k]['zj'] = 0;
            }else
            {
                $private[$k]['zj'] = 1; 
            }
            $private[$k]['time'] = friend_date($v['add_time']);
            $private[$k]['content'] = htmlspecialchars_decode($v['content']);
        }
        foreach($private as $kk => $vv)
        {
            $datetime[] = $vv['add_time'];
        }
        
        array_multisort($datetime,SORT_ASC,$private);
        // dump($private);die;
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $private;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit;  
    }
    
    //回复
    public function private_letter()
    {
         if (IS_POST) {
             $post = input('post.');
             
             
             $match = "/[\x{4e00}-\x{9fa5}A-Za-z0-9]/u";
             
             if(mb_strlen($post['content'],'UTF8') > 1000)
             {
                $datas['status'] = -1;
                $datas['msg'] = '您输入的内容过长';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit; 
             }

            if(!preg_match($match, $post['content'])){
            
                $datas['status'] = -1;
                $datas['msg'] = '内容必须包含中文，英文，数字字符';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;  
            }
             
            $wordfilter = M('weapp_wordfilter')->where(array('status' => 1))->select();
            foreach ($wordfilter as $k => $v)
            {
                $wordfilters[$k] = $v['title'];
            }
            $result = $this->sensitive($wordfilters, $post['content']);
            //  dump($result);
             $post['content'] = $result;
             $post['users_id'] = $this->users_id;
             $post['add_time'] = time();
             $private = M('users_private')->add($post);
             $datas['status'] = 1;
             $datas['msg'] = '回复成功';
             $datas['res'] = '';
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit; 
         }
    }
    
    /**
 * @todo 敏感词过滤，返回结果
 * @param array $list  定义敏感词一维数组
 * @param string $string 要过滤的内容
 * @return string $log 处理结果
 */
function sensitive($list, $string){
  $count = 0; //违规词的个数
  $sensitiveWord = '';  //违规词
  $stringAfter = $string;  //替换后的内容
  $pattern = "/".implode("|",$list)."/i"; //定义正则表达式
  if(preg_match_all($pattern, $string, $matches)){ //匹配到了结果
    $patternList = $matches[0];  //匹配到的数组
    $count = count($patternList);
    $sensitiveWord = implode(',', $patternList); //敏感词数组转字符串
    $replaceArray = array_combine($patternList,array_fill(0,count($patternList),'*')); //把匹配到的数组进行合并，替换使用
    $stringAfter = strtr($string, $replaceArray); //结果替换
  }
  return $stringAfter;
}
    
    // 已读
    
    public function news()
    {
        if (IS_POST) {
            $post = input('post.');
            $datae['news'] = 1;
            // dump($post);die;
            $private = M('users_private')->where(array('private_id' => $this->users_id,'users_id' => $post['private_id']))->save($datae);
            // dump($private);die;
            if($private)
            {
                $datas['status'] = 1;
                $datas['msg'] = '已读';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;  
            }
        }
    }
    
    
    public function users_lists()
    {
       $param = input('post.');
    //   $privates = M('users_private')
    //      ->field('a.*,b.username,b.head_pic')
    //      ->alias('a')
    //      ->join('users b','b.users_id=a.users_id', 'LEFT')
    //      ->where(array('a.private_id' => $param['users_id']))
    //      ->order('news asc and add_time desc')
    //      ->getField('a.users_id', true);
        $users = M('users')->where(array('users_id' => $param['users_id']))->find();
    //     // if(empty())
    //     dump($privates);die;
    //     foreach($users as $kk => $vv)
    //     {
    
    // dump($this->users_id);die;
          $list['username'] = $users['username'];
          $list['head_pic'] = $users['head_pic'];
          $list['users_id'] = $users['users_id'];
         $prc = M('users_private')
         ->field('a.*,b.username,b.head_pic')
         ->alias('a')
         ->join('users b','b.users_id=a.users_id', 'LEFT')
         ->where(array('a.private_id' => $param['users_id'],'a.users_id' => $this->users_id))
         ->order('add_time desc')
         ->limit(0,1)
         ->find();
         if(!empty($prc))
         {
              $prc['title'] = htmlspecialchars_decode($prc['content']);
         }
        
         $list['xinxi'] = $prc;
             
             
            //   $list['title'] = htmlspecialchars_decode($prc['content']);
            //   $list['add_time'] = $prc['add_time'];
            //   $list['status'] = 0;
    // //     }
        
        
    //     if(empty($list))
    //     {
    //       $list['username'] = $vv['username'];
    //       $list['head_pic'] = $vv['head_pic'];
    //       $list['users_id'] = $vv['users_id'];
    //     }
        
        // dump($list);die;
    //     $data['count'] = $private;
    //     $data['list'] = $list;
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $list;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit;
        // dump($datas);die;
        //  dump($privates);die;
    }
    
    
    //
    public function list_letter()
    {
        $users_id = $this->users_id;
        
        $private = M('users_private')
         ->field('a.*,b.nickname,b.head_pic')
         ->alias('a')
         ->join('users b','b.users_id=a.users_id', 'LEFT')
         ->where(array('a.users_id' => $users_id,'a.news' => 0))
         ->order('news asc and add_time desc')
         ->count();
         
         
       $privates = M('users_private')
         ->field('a.*,b.nickname,b.head_pic')
         ->alias('a')
         ->join('users b','b.users_id=a.users_id', 'LEFT')
         ->where(array('a.users_id' => $users_id))
         ->order('news asc and add_time desc')
         ->getField('a.private_id', true);
         
     $privatess = M('users_private')
         ->field('a.*,b.nickname,b.head_pic')
         ->alias('a')
         ->join('users b','b.users_id=a.users_id', 'LEFT')
         ->where(array('a.private_id' => $users_id))
         ->order('news asc and add_time desc')
         ->getField('a.users_id', true);
         
        //  dump($privates);
        //  dump($privatess);die;
        if(empty($privates))
        {
            $privates = [];
        }
        if(empty($privatess))
        {
            $privatess = [];
        }
         $privates = array_merge($privates,$privatess);
         
        $users = M('users')->where(array('users_id' => array('in',array_unique($privates))))->select();
        // dump($users);die;
        foreach($users as $kk => $vv)
        {
           $list[$kk]['nickname'] = $vv['nickname'];
           $list[$kk]['head_pic'] = $vv['head_pic'];
           $list[$kk]['users_id'] = $vv['users_id'];
           $list[$kk]['count'] = M('users_private')
             ->field('a.*,b.username,b.head_pic')
             ->alias('a')
             ->join('users b','b.users_id=a.users_id', 'LEFT')
             ->where(array('a.private_id' => $users_id,'a.users_id' => $vv['users_id']))
             ->order('a.news asc and a.add_time desc')
             ->count();
             
            $prc = M('users_private')
             ->field('a.*,b.username,b.head_pic')
             ->alias('a')
             ->join('users b','b.users_id=a.users_id', 'LEFT')
             ->where(array('a.private_id' => $users_id,'a.users_id' => $vv['users_id']))
             ->order('add_time desc')
             ->limit(0,1)
             ->find();
             
            $prcs = M('users_private')
             ->field('a.*,b.nickname,b.head_pic')
             ->alias('a')
             ->join('users b','b.users_id=a.users_id', 'LEFT')
             ->where(array('a.users_id' => $users_id,'a.private_id' => $vv['users_id']))
             ->order('add_time desc')
             ->limit(0,1)
             ->find();
             if($prc['add_time'] > $prcs['add_time'])
             {
                 $list[$kk]['title'] = htmlspecialchars_decode($prc['content']); 
                   $list[$kk]['add_time'] = $prc['add_time'];
             }else
             {
                 $list[$kk]['title'] = htmlspecialchars_decode($prcs['content']);  
                   $list[$kk]['add_time'] = $prcs['add_time'];
             }
            //   $list[$kk]['add_time'] = $prc['add_time'];
              $list[$kk]['status'] = 0;
        }
        $post = input('post.');
        
        if(!in_array($post['users_id'],array_unique($privates)))
        {
            if(!empty($post['users_id']))
            {
                  $us = M('users')->where(array('users_id' => $post['users_id']))->find();
                  $ret['nickname'] = $us['nickname'];
                  $ret['head_pic'] = $us['head_pic'];
                  $ret['users_id'] = $us['users_id'];
                  $prc = M('users_private')
                 ->field('a.*,b.nickname,b.head_pic')
                 ->alias('a')
                 ->join('users b','b.users_id=a.users_id', 'LEFT')
                 ->where(array('a.private_id' => $users_id,'a.users_id' => $vv['users_id']))
                 ->order('add_time desc')
                 ->limit(0,1)
                 ->find();
                 
                $prcs = M('users_private')
                 ->field('a.*,b.nickname,b.head_pic')
                 ->alias('a')
                 ->join('users b','b.users_id=a.users_id', 'LEFT')
                 ->where(array('a.users_id' => $users_id,'a.private_id' => $vv['users_id']))
                 ->order('add_time desc')
                 ->limit(0,1)
                 ->find();
                //  dump(11);die;
                 
                 if($prc['add_time'] > $prcs['add_time'])
                 {
                     $list[$kk]['title'] = htmlspecialchars_decode($prc['content']); 
                     $list[$kk]['add_time'] = $prc['add_time'];
                 }else
                 {
                     $list[$kk]['title'] = htmlspecialchars_decode($prcs['content']);  
                     $list[$kk]['add_time'] = $prcs['add_time'];
                 }
            
                    $rest[] = array(
                        'nickname' => $us['nickname'],
                        'head_pic' => $us['head_pic'],
                        'users_id' => $us['users_id'],
                        'add_time' => time(),
                        'prc' => $prc
                        );
            }
            // dump($rest);die;
        if(!empty($rest))
        {
           $list = array_merge($list,$rest);
          $keys = 'nickname';
          $list = $this->second_array_unique_bykey($list,$keys);
        }
        }
        
        // dump($list);die
        // dump($list);die;
        $data['count'] = $private;
        $data['list'] = $list;
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $data;
        // dump($datas);die;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit;
    }
    
    //搜索聊天
    public function search_letter()
    {
        $post = input('post.');
        // dump($post);die;
        if(empty($post['nickname']))
        {
            $datas['status'] = -1;
            $datas['msg'] = '请输入您要搜索的用户名';
            $datas['res'] = '';
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit;
        }
       $users =  M('users')->where(array('nickname' => $post['nickname']))->find();
       if(empty($users))
       {
            $datas['status'] = -1;
            $datas['msg'] = '您搜索的用户不存在';
            $datas['res'] = '';
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit;
       }else
       {
        //   $this->users_id = 15;
        //   dump($this->users_id);die;
               $prc = M('users_private')
             ->field('a.*,b.username,b.head_pic')
             ->alias('a')
             ->join('users b','b.users_id=a.users_id', 'LEFT')
             ->where(array('a.private_id' => $this->users_id,'a.users_id' => $users['users_id']))
             ->order('add_time desc')
             ->limit(0,1)
             ->find();
             
            $prcs = M('users_private')
             ->field('a.*,b.username,b.head_pic')
             ->alias('a')
             ->join('users b','b.users_id=a.users_id', 'LEFT')
             ->where(array('a.users_id' => $this->users_id,'a.private_id' => $users['users_id']))
             ->order('add_time desc')
             ->limit(0,1)
             ->find();
             
             if(empty($prc) || empty($prcs))
             {
                $datas['status'] = -1;
                $datas['msg'] = '该用户您没有聊过';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;
             }else
             {
                  if($prc['add_time'] > $prcs['add_time'])
                 {
                     $list['title'] = htmlspecialchars_decode($prc['content']); 
                 }else
                 {
                     $list['title'] = htmlspecialchars_decode($prcs['content']);  
                 }
            
                $rest = array(
                    'nickname' => $users['nickname'],
                    'head_pic' => $users['head_pic'],
                    'users_id' => $users['users_id'],
                    'add_time' => time(),
                    'title' => $list['title']
                    );
                $datas['status'] = 1;
                $datas['msg'] = '';
                $datas['res'] = $rest;
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;
             }
            //  dump($prc);
            //  dump($prcs);die;
             
        //   dump($rest);die;
       }
       
    }
    
    
   public function second_array_unique_bykey($arr, $key){
    $tmp_arr = array();
    foreach($arr as $k => $v)
    {
        if(in_array($v[$key], $tmp_arr))   //搜索$v[$key]是否在$tmp_arr数组中存在，若存在返回true
        {
            unset($arr[$k]); //销毁一个变量  如果$tmp_arr中已存在相同的值就删除该值
        }
        else {
            $tmp_arr[$k] = $v[$key];  //将不同的值放在该数组中保存
        }
   }
   //ksort($arr); //ksort函数对数组进行排序(保留原键值key)  sort为不保留key值
    return $arr;
   
}
    
    
    
    //加入样品清单
    
    public function add_sample()
    {
        $post = input('post.');
        $post['status'] = 3;
        $post['users_id'] = $this->users_id;
        $post['add_time'] = time();
        
        $product = M('product_content')->where(array('aid' => $post['product_id']))->find();
        if($product['is_sample'] != '是')
        {
             $datas['status'] = -2;
             $datas['msg'] = '该商品禁止申请样品';
             $datas['res'] = '';
             echo json_encode($datas,JSON_UNESCAPED_UNICODE);
             exit; 
        }
        
        $sample = M('product_sample')->where(array('users_id' => $this->users_id,'product_id' => $post['product_id'],'status' => 3))->find();
        if($sample)
        {
            $sample['num'] = $sample['num'] + $post['num'];
            $res = M('product_sample')->where(array('id' => $sample['id']))->save($sample);
        }else
        {
           $res = M('product_sample')->add($post);
        }
        if($res)
        {
             $datas['status'] = 1;
             $datas['msg'] = '加入成功';
             $datas['res'] = '';
             echo json_encode($datas,JSON_UNESCAPED_UNICODE);
             exit;  
        }
    }
    
    //样品清单列表
    public function lists_sample()
    {
        $post = input('post.');
        $count = M('product_sample')
        ->field('b.title,b.litpic,b.aid,b.users_price,c.name as brand_name,d.number,d.sample,d.delivery,a.num,a.id,a.selected')
        ->alias('a')
        ->join('archives b','a.product_id=b.aid')
        ->join('product_brand c','b.brand_id=c.id')
        ->join('product_content d','d.aid=b.aid')
        ->where(array('a.users_id' => $this->users_id,'a.status' => 3))
        ->count();
        
        $datas['total_page'] = ceil($count/$post['limit']);
         $datas['count'] = $count;
        $product = M('product_sample')
        ->field('b.title,b.litpic,b.aid,b.users_price,c.name as brand_name,d.number,d.sample,d.delivery,a.num,a.id,a.selected')
        ->alias('a')
        ->join('archives b','a.product_id=b.aid')
        ->join('product_brand c','b.brand_id=c.id')
        ->join('product_content d','d.aid=b.aid')
        ->where(array('a.users_id' => $this->users_id,'a.status' => 3))
        ->page($post['page'],$post['limit'])
        ->select();
        // dump($product);die;
        
        $level_discount = $this->users['level_discount'];
        $discount_price = $level_discount / 100;
        $spec_price = $users_price = $num_new = 0;
        foreach($product as $k => $v)
        {
            // dump($v);die;
          $rest = $this->num($v['aid'],$v['num']);
        //   dump($discount_price);die;
        //   $shop_cart[$k]['spec_price'] =  sprintf("%.2f",$rest['spec_price']*$discount_price);
          $product_id  = sprintf("%.2f", $rest['spec_price']*$discount_price);
        //   dump($v['num']);
        //   dump($rest);
        //   dump($product_id);die;
        $product[$k]['users_price'] = $product_id;
          $product[$k]['total_price'] = sprintf("%.2f",$v['num'] * $product_id);
            
        //   $product[$k]['total_price'] = sprintf("%.2f",$v['num']*$v['users_price']);
           $attr = M('shop_product_attr')
          ->field('a.attr_value,b.attr_name')
          ->alias('a')
          ->join('shop_product_attribute b','b.attr_id=a.attr_id', 'LEFT')
          ->where(array('a.aid' => $v['aid']))
          ->select();
          
          foreach($attr as $kk => $vv)
          {
              if($vv['attr_name'] == '参考封装')
              {
                  $product[$k]['package1'] = $vv['attr_value'];
              }
             if($vv['attr_name'] == '封装/规格')
              {
                  $product[$k]['specifications1'] = $vv['attr_value'];
              }
               if($vv['attr_name'] == '包装/方式')
              {
                  $product[$k]['packing1'] = $vv['attr_value'];
              }
          }
        }
        // dump($product);die;
       
        
         $datas['status'] = 1;
         $datas['msg'] = '';
         $datas['res'] = $product;
         echo json_encode($datas,JSON_UNESCAPED_UNICODE);
         exit; 
    }
    
    
    //修改样品列表数量
    public function sample_unified_algorithm()
    {
        if (IS_POST) {
            $aid    = input('post.aid');
            $symbol = input('post.symbol');
            $num    = input('post.num');
            $id = input('post.id');
           if($num <1)
            {
                $datas['status'] = -1;
                $datas['msg'] = '商品数量最少为1';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;
            }
            
           $arc = M('archives')->where(array('aid' => $aid))->find();
           if($arc['stock_count'] < $num)
           {
                $datas['status'] = -1;
                $datas['msg'] = '数量过多请重新填写';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;  
           }
            
            $archives_where = [
                'arcrank' => array('egt','0'),
                'aid'     => $aid,
                'lang'    => $this->home_lang,
            ];
            $archives_count = M('archives')->where($archives_where)->count();
            // dump($archives_count);die;
             if (!empty($archives_count)) {
                
               $where = [
                    'users_id'    => $this->users_id,
                    'product_id'  => $aid,
                    'id'  =>    $id,
                ];
                
                if ('-' == $symbol) $where['num'] = array('gt','1');
                $nums = M('product_sample')->where($where)->getField('num');
                if (!empty($nums)) {
                   if ('+' == $symbol) {
                        $data['num'] = $nums + 1;
                    }else if ('-' == $symbol) {
                        $data['num'] = $nums - 1;
                    }else if ('change' == $symbol) {
                        $data['num'] = $num;
                    }
                    
                    // dump($data);die;
                    $ids =  M('product_sample')->where($where)->update($data);
                    
                     if (!empty($ids)) {
                     $datas['status'] = 1;
                     $datas['msg'] = '操作成功';
                     $datas['res'] = '';
                     echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                     exit;
                        // $this->success('操作成功！', null, ['NumberVal'=>$CartData['num'], 'AmountVal'=>$CartData['total_price']]);
                    }
                    
                }else
                {
                    $datas['status'] = -1;
                    $datas['msg'] = '商品数量最少为1';
                    $datas['res'] = '';
                    echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                    exit;
                }
                // dump($nums);die;
                
             }else
             {
                $datas['status'] = -1;
                $datas['msg'] = '该商品不存在或已下架';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;
             }
        }
    }
    
    
    //批量提交样品清单列表
    public function sample_check()
    {
        $post = input('post.');
        
       $identity = M('users_identity')->where(array('users_id' => $this->users_id))->find();
        if(empty($identity))
        {
             $datas['status'] = -2;
             $datas['msg'] = '您还未实名认证不能申请样品清单,请前往认证';
             $datas['res'] = '';
             echo json_encode($datas,JSON_UNESCAPED_UNICODE);
             exit;  
        }elseif($identity['examine'] == 0)
        {
             $datas['status'] = -2;
             $datas['msg'] = '您的实名认证正在审核，请耐心等待';
             $datas['res'] = '';
             echo json_encode($datas,JSON_UNESCAPED_UNICODE);
             exit;  
        }
        
        $count = M('product_sample')
        ->field('b.title,b.litpic,b.aid,b.users_price,c.name as brand_name,d.number,d.sample,d.delivery,a.num,a.id')
        ->alias('a')
        ->join('archives b','a.product_id=b.aid')
        ->join('product_brand c','b.brand_id=c.id')
        ->join('product_content d','d.aid=b.aid')
        ->where(array('a.users_id' => $this->users_id,'a.status' => 3,'product_id' => array('in',$post['product_id'])))
        ->count();
        //  $datas['total_page'] = ceil($count/$post['limit']);
        //   $datas['count'] = $count;
         $product = M('product_sample')
        ->field('b.title,b.litpic,b.aid,b.users_price,c.name as brand_name,d.number,d.sample,d.delivery,a.num,a.id')
        ->alias('a')
        ->join('archives b','a.product_id=b.aid')
        ->join('product_brand c','b.brand_id=c.id')
        ->join('product_content d','d.aid=b.aid')
        ->where(array('a.users_id' => $this->users_id,'a.status' => 3,'product_id' => array('in',$post['product_id'])))
        // ->page($post['page'],$post['limit'])
        ->select();
        
        // dump($product);die;
        
        // $product
        foreach($product as $k => $v)
        {
          $product[$k]['total_price'] = sprintf("%.2f",$v['num']*$v['users_price']);
           $attr = M('shop_product_attr')
          ->field('a.attr_value,b.attr_name')
          ->alias('a')
          ->join('shop_product_attribute b','b.attr_id=a.attr_id', 'LEFT')
          ->where(array('a.aid' => $v['aid']))
          ->select();
          foreach($attr as $kk => $vv)
          {
              if($vv['attr_name'] == '参考封装')
              {
                  $product[$k]['package1'] = $vv['attr_value'];
              }
             if($vv['attr_name'] == '封装/规格')
              {
                  $product[$k]['specifications1'] = $vv['attr_value'];
              }
               if($vv['attr_name'] == '包装/方式')
              {
                  $product[$k]['packing1'] = $vv['attr_value'];
              }
          }
        }
        // dump($product);die;
         $datas['status'] = 1;
         $datas['msg'] = '';
         $datas['res'] = $product;
         echo json_encode($datas,JSON_UNESCAPED_UNICODE);
         exit;
    }
    
    //批量申请样品清单
    public function sample_check_add()
    {
       $post = input('post.');
       $post['status'] = 0;
       $id = $post['id'];
       $post['add_time'] = time();
       $time = time();
       $post['numbers'] = date('Y') . $time . rand(10,100);
       
       unset ($post['id']);
       $res = M('product_sample')->where(array('id' => array('in',$id),'users_id' => $this->users_id))->save($post);
       if($res)
       {
           $notice['source'] = 7;
           $notice['admin_id'] = 1;
           $notice['users_id'] = $this->users_id;
           $tpl = M('users_notice_tpl') -> where(array('tpl_id' => 7))->find();
           $notice['content_title'] = $tpl['tpl_title'];
           $notice['content'] = '订单样品清单编号：'.$post['numbers'];
           $notice['is_read'] = 0;
           $notice['users_read'] = 0;
           $notice['add_time'] = time();
           $notice['users_title'] = '您申请了样品清单,订单号编号：'.$post['numbers'].',请耐心等待审核';
           M('users_notice_tpl_content')->add($notice);
            $datas['status'] = 1;
            $datas['msg'] = '提交成功,请等待审核';
            
            $product['numbers'] = $post['numbers'];
            
            $product['id'] = $id;
            
            $datas['res'] = $product;
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit; 
       }
    }
    
    //样品选中
    
    public function selected()
    {
        $id  = input('post.id');
        $selected = input('post.selected');
        
        if (!empty($selected)) {
            $selected = 0;
        } else {
            $selected = 1;
        }
        
        // dump($selected);die;
         $data['selected'] = $selected;
        // 更新条件
        if ('*' == $id) {
            $cart_where = [
                'num' => ['>', 0],
                'users_id' => $this->users_id,
                'status' => 3
            ];
        } else {
            $cart_where = [
                'id'  => $id,
                'num' => ['>', 0],
                'users_id' => $this->users_id,
                 'status' => 3
            ];
        }
        
        // dump($cart_where);die;
        // dump($data);die;
        
        $return =  M('product_sample')->where($cart_where)->update($data);
        if (!empty($return)) {
             $datas['status'] = 1;
             $datas['msg'] = '操作成功';
             $datas['res'] = '';
             echo json_encode($datas,JSON_UNESCAPED_UNICODE);
             exit;
            // $this->success('操作成功！');
        }else{
            $datas['status'] = -1;
            $datas['msg'] = '选择失败';
            $datas['res'] = '';
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit;
            // $this->error('操作失败！');
        }
        
    }
    
    
    // //样品清单详情列表
    
    public function info_list()
    {
    
    $post = input('post.');
        $post['product_id'] = M('product_sample')->where(array('selected' => 1,'users_id' => $this->users_id))->getField('product_id', true);
        //dump($post['product_id']);die;

        $count = M('product_sample')
        ->field('b.title,b.litpic,b.aid,b.users_price,c.name as brand_name,d.number,d.sample,d.delivery,a.num,a.id')
        ->alias('a')
        ->join('archives b','a.product_id=b.aid')
        ->join('product_brand c','b.brand_id=c.id')
        ->join('product_content d','d.aid=b.aid')
        ->where(array('a.users_id' => $this->users_id,'a.status' => 3,'product_id' => array('in',$post['product_id'])))
        ->count();
        // dump($count);die;
        
         $datas['total_page'] = ceil($count/$post['limit']);
         $datas['count'] = $count;
         
         $product = M('product_sample')
        ->field('b.title,b.litpic,b.aid,b.users_price,c.name as brand_name,d.number,d.sample,d.delivery,a.num,a.id')
        ->alias('a')
        ->join('archives b','a.product_id=b.aid')
        ->join('product_brand c','b.brand_id=c.id')
        ->join('product_content d','d.aid=b.aid')
        ->where(array('a.users_id' => $this->users_id,'a.status' => 3,'product_id' => array('in',$post['product_id'])))
        ->page($post['page'],$post['limit'])
        ->select();
        
        // $product
        foreach($product as $k => $v)
        {
          $product[$k]['total_price'] = sprintf("%.2f",$v['num']*$v['users_price']);
          $attr = M('shop_product_attr')
          ->field('a.attr_value,b.attr_name')
          ->alias('a')
          ->join('shop_product_attribute b','b.attr_id=a.attr_id', 'LEFT')
          ->where(array('a.aid' => $v['aid']))
          ->select();
          
          foreach($attr as $kk => $vv)
          {
              if($vv['attr_name'] == '参考封装')
              {
                  $product[$k]['package1'] = $vv['attr_value'];
              }
             if($vv['attr_name'] == '封装/规格')
              {
                  $product[$k]['specifications1'] = $vv['attr_value'];
              }
              if($vv['attr_name'] == '包装/方式')
              {
                  $product[$k]['packing1'] = $vv['attr_value'];
              }
          }
            $rest = $this->num($v['aid'],$v['num']);
            // dump($rest)
            $product[$k]['total_price'] = $rest['spec_price'];
            $product[$k]['users_price'] = $rest['spec_price'];
        }
        // dump($product);die;
         $datas['status'] = 1;
         $datas['msg'] = '';
         $datas['res'] = $product;
         echo json_encode($datas,JSON_UNESCAPED_UNICODE);
         exit;
    }
    
    //删除样品清单
    public function sample_del()
    {
        $post = input('post.');
        $del = M('product_sample')->where(array('id' =>$post['id']))->delete();
        if($del)
        {
            $datas['status'] = 1;
            $datas['msg'] = '删除成功';
            $datas['res'] = '';
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit;  
        }
    }
    
    //我的样品列表
    
    public function users_sample()
    {
        
        // $this->users_id = 5;
       $post = input('post.');
       
       if(empty($post['limit']))
       {
           $post['limit'] = 10;
       }
      if(empty($post['page']))
       {
           $post['page'] = 1;
       }
       
      $count = M('product_sample')
        ->field('b.title,b.litpic,b.aid,b.users_price,a.numbers,a.num,a.status,a.id,a.add_time,a.selected')
        ->alias('a')
        ->join('archives b','a.product_id=b.aid')
        ->where(array('a.users_id' => $this->users_id))
        ->where('a.status != 3')
        ->count();
        // dump($count);die;
       
       $sample = M('product_sample')
        ->field('b.title,b.litpic,b.aid,b.users_price,a.numbers,a.num,a.status,a.id,a.add_time,a.selected,a.sample_status,a.address')
        ->alias('a')
        ->join('archives b','a.product_id=b.aid')
        ->where(array('a.users_id' => $this->users_id))
        ->where('a.status != 3')
        ->order('add_time asc')
        ->page($post['page'],$post['limit'])
        ->select();
        
        foreach ($sample as $k => $v)
        {
              if($v['status'] == 0)
              {
                 $sample[$k]['statuse'] = '待审核';
              }
               if($v['status'] == 1 && $v['sample_status'] == 1)
              {
                  $sample[$k]['statuse'] = '待发货';
              }
              if($v['status'] == 1 && $v['sample_status'] == 2)
              {
                 $sample[$k]['statuse'] = '已发货';
              }
              if($v['status'] == 1 && $v['sample_status'] == 3)
              {
                 $sample[$k]['statuse'] = '已完成';
              }
              
              if($v['status'] == 2)
              {
                   $sample[$k]['statuse'] = '驳回';
              }
            
            if($v['sample_status'] == 2)
            {
              $sample[$k]['express'] = M('sample_express')->where(array('sample_id' => $v['id']))->find();
            }
            $address = M('shop_address')->where(array('addr_id' => $OrderData['address_id']))->find();
            
            $sample[$k]['country']  = '中国';
            $sample[$k]['province'] = get_province_name($address['province']);
            $sample[$k]['city']     = get_city_name($address['city']);
            $sample[$k]['district'] = get_area_name($address['district']);
            $sample[$k]['address_name'] = $address['address'];
        }
        
        // dump($sample);die;
        
        $data['total_page'] = ceil($count/$post['limit']);
         $data['count'] = $count;
        foreach ($sample as $k => $v)
        {
            $sample[$k]['total_price'] = $v['users_price'] * $v['num'];
        }
        $data['status'] = 1;
        $data['msg'] = '';
        $data['res'] = $sample;
        echo json_encode($data,JSON_UNESCAPED_UNICODE);
        exit(); 
    }
    
    
    //样品完成
    
    public function order_mark_status()
    {
        $post = input('post.');
        
        $data['sample_status'] = 3;
        $sample = M('product_sample')->where(array('id' => $post['id']))->save($data);
        if($sample)
        {
            $data['status'] = 1;
            $data['msg'] = 完成;
            $data['res'] = '';
            echo json_encode($data,JSON_UNESCAPED_UNICODE);
            exit(); 
        }
    }
    
    public function users_view_sample()
    {
         $post = input('post.');
         
        //  dump($post);die;
         $sample = M('product_sample')
         ->field('c.name as brand_name,a.*,b.title as product_name')
         ->alias('a')
         ->join('archives b','a.product_id=b.aid')
         ->join('product_brand c','b.brand_id=c.id')
         ->where(array('a.id' => array('in',$post['id'])))
         ->find();
         
         $address = M('shop_address')->where(array('addr_id' => $sample['address_id']))->find();
         
          $sample['country']  = '中国';
          $sample['province'] = get_province_name($address['province']);
          $sample['city']     = get_city_name($address['city']);
          $sample['district'] = get_area_name($address['district']);
          $sample['address_name'] = $address['address'];
          
          if($sample['status'] == 0)
          {
              $sample['statuse'] = '待审核';
          }
           if($sample['status'] == 1 && $sample['sample_status'] == 1)
          {
              $sample['statuse'] = '待发货';
          }
          if($sample['status'] == 1 && $sample['sample_status'] == 2)
          {
              $sample['statuse'] = '已发货';
          }
          if($sample['status'] == 1 && $sample['sample_status'] == 3)
          {
              $sample['statuse'] = '已完成';
          }
          
        if($sample['sample_status'] == 2)
        {
           $sample['express'] = M('sample_express')->where(array('sample_id' => $v['id']))->find();
        }
         
        //  dump($sample);DIE;
         $data['status'] = 1;
         $data['msg'] = '';
         $data['res'] = $sample;
         echo json_encode($data,JSON_UNESCAPED_UNICODE);
         exit(); 
        //  dump($sample);die;
    }
    
    //提交询交价
    public function delivery()
    {
       $post = input('post.');
       $post['users_id'] = $_SESSION['think']['users_id'];
       $post['add_time'] = time();
       $time = time();
       $post['numbers'] = date('Y') . $time . rand(10,100);
       $ip = clientIP();
    //   dump($post);die;
       if($this->limitoftimes($ip,1,60)===false)
       {
            $data['status'] = -1;
            $data['msg'] = '一分钟内以提交过';
            echo json_encode($data,JSON_UNESCAPED_UNICODE);
            exit();
       }
      $delivery = M('product_delivery')->where(array('users_id' => $this->users_id,'product_id' => $post['product_id']))->find();
      if($delivery)
      {
            $data['status'] = -1;
            $data['msg'] = '您以询价过，请联系客服';
            echo json_encode($data,JSON_UNESCAPED_UNICODE);
            exit(); 
      }else
      {
          $res = M('product_delivery')->add($post);
      }
      if($res)
      {
            $notice['source'] = 9;
            $notice['admin_id'] = 1;
            $notice['users_id'] = $this->users_id;
            $tpl = M('users_notice_tpl') -> where(array('tpl_id' => 9))->find();
            $notice['content_title'] = $tpl['tpl_title'];
            $notice['content'] = '订单询价编号：'.$post['numbers'];
            $notice['is_read'] = 0;
            $notice['users_read'] = 0;
            $notice['add_time'] = time();
            $notice['users_title'] = '您已提交询价,询价编号：'.$post['numbers'].',请耐心等待回复';
            M('users_notice_tpl_content')->add($notice);
            $data['status'] = 1;
            $data['msg'] = '询价成功，请等待客服回复';
            echo json_encode($data,JSON_UNESCAPED_UNICODE);
            exit(); 
      }
    }
    
    //个人中心询交
    public function delivery_list()
    {
         $post = input('post.');
        
         $count = M('product_delivery')
        ->field('a.*,b.title,b.litpic,c.name as brand_name,c.litpic as brand_litpic,a.add_time')
        ->alias('a')
        ->join('archives b','a.product_id=b.aid')
        ->join('product_brand c','b.brand_id=c.id')
        ->where(array('a.users_id' => $this->users_id))
        ->count();
        
        $data['total_page'] = ceil($count/$post['limit']);
         $data['count'] = $count;
        $product = M('product_delivery')
        ->field('a.*,b.title,b.litpic,c.name as brand_name,c.litpic as brand_litpic,a.add_time')
        ->alias('a')
        ->join('archives b','a.product_id=b.aid')
        ->join('product_brand c','b.brand_id=c.id')
          ->where(array('a.users_id' => $this->users_id))
        ->page($post['page'],$post['limit'])
        ->order('add_time desc')
        ->select();
        
        
        
        foreach($product as $k => $v)
        {
            $product[$k]['open'] = 0;
        }
        
        // dump($product);die;
        
        $data['status'] = 1;
        $data['mag'] = '';
        $data['res'] = $product;
        // dump($data);die;
        echo json_encode($data,JSON_UNESCAPED_UNICODE);
        exit(); 
        // dump($product);die;
    }
    
    //个人购物车物品数量
    public function cart_num()
    {
        // dump(11);die;
        $care_num = M('shop_cart')->where(array('users_id' => $this->users_id))->count();
        $data['status'] = 1;
        $data['mag'] = '';
        $data['res'] = $care_num;
        echo json_encode($data,JSON_UNESCAPED_UNICODE);
        exit();
    }
    
    public function footprint()
    {
        $param = input('post.');
        $param['add_time'] = time();
        $param['channel'] = 2;
        $param['users_id'] = $this->users_id;
        
        $footprint = M('users_footprint')->add($param);
        $data['status'] = 1;
        $data['mag'] = '成功';
        $data['res'] = '';
        echo json_encode($data,JSON_UNESCAPED_UNICODE);
        exit();
    }
    
    //样品清单数量
    public function sample_num()
    {
        $sample = M('product_sample')->where(array('status' => 3,'users_id' => $this->users_id))->count();
        $data['status'] = 1;
        $data['mag'] = '';
        $data['res'] = $sample;
        echo json_encode($data,JSON_UNESCAPED_UNICODE);
        exit();
    }
    
    //支付成功后调用接口
    public function payment()
    {
      $GetContentArr = input('post.');
       $Notice = M('users_notice_tpl')->where(array('source' => 5))->find();
       $ContentArr = [
            '订单编号：' . $GetContentArr['order_code'],
            '订单总额：' . $GetContentArr['order_amount'],
            '支付方式：' . $GetContentArr['pay_method'],
            '手机号：' . $GetContentArr['mobile']
        ];
        $Content = implode('<br/>', $ContentArr);
        $ContentData = [
            'source'      => $SendScene,
            'admin_id'    => 1,
            'users_id'    => $this->users_id,
            'content_title' => $Notice['tpl_title'],
            'content'     => !empty($Content) ? $Content : '',
            'is_read'     => 0,
            'lang'        => get_home_lang(),
            'add_time'    => getTime(),
            'update_time' => getTime(),
            'users_read'  => 0,
            'users_title' =>'订单信息',
        ];
        M('users_notice_tpl_content')->add($ContentData);
    }
    
    //用户全站信息
    
    public function notice_tpl()
    {
        // $this->users_id = 5;
        // dump($this->users_id);die;
        $param = input('post.');
        
            $liste['order'] = M('users_notice_tpl_content')
            ->field('a.*,b.tpl_name')
            ->alias('a')
            ->join('users_notice_tpl b','a.source=b.send_scene')
            ->where(array('a.users_id' => $this->users_id,'a.source' => 5,'a.is_del' => 0,'users_read' => 0))
            ->count();
           $liste['deliver_goods'] = M('users_notice_tpl_content')
            ->field('a.*,b.tpl_name')
            ->alias('a')
            ->join('users_notice_tpl b','a.source=b.send_scene')
            ->where(array('a.users_id' => $this->users_id,'a.source' => 6,'a.is_del' => 0,'users_read' => 0))
            ->count();
           $liste['sample'] = M('users_notice_tpl_content')
            ->field('a.*,b.tpl_name')
            ->alias('a')
            ->join('users_notice_tpl b','a.source=b.send_scene')
            ->where(array('a.users_id' => $this->users_id,'a.source' => 7,'a.is_del' => 0,'users_read' => 0))
            ->count();
           $liste['sample_status'] = M('users_notice_tpl_content')
            ->field('a.*,b.tpl_name')
            ->alias('a')
            ->join('users_notice_tpl b','a.source=b.send_scene')
            ->where(array('a.users_id' => $this->users_id,'a.source' => 8,'a.is_del' => 0,'users_read' => 0))
            ->count();
           $liste['inquiry'] = M('users_notice_tpl_content')
            ->field('a.*,b.tpl_name')
            ->alias('a')
            ->join('users_notice_tpl b','a.source=b.send_scene')
            ->where(array('a.users_id' => $this->users_id,'a.source' => 9,'a.is_del' => 0,'users_read' => 0))
            ->count();
           $liste['reject'] = M('users_notice_tpl_content')
            ->field('a.*,b.tpl_name')
            ->alias('a')
            ->join('users_notice_tpl b','a.source=b.send_scene')
            ->where(array('a.users_id' => $this->users_id,'a.source' => 11,'a.is_del' => 0,'users_read' => 0))
            ->count();
        
        
        if(empty($param['source']))
        {
            $count = M('users_notice_tpl_content')
            ->field('a.*,b.tpl_name')
            ->alias('a')
            ->join('users_notice_tpl b','a.source=b.send_scene')
            ->where(array('a.users_id' => $this->users_id,'a.is_del' => 0))
            ->count();
             $data['total_page'] = ceil($count/$param['limit']);
              $data['count'] = $count;
             $notice_tpl = M('users_notice_tpl_content')
            ->field('a.*,b.tpl_name')
            ->alias('a')
            ->join('users_notice_tpl b','a.source=b.send_scene')
            ->where(array('a.users_id' => $this->users_id,'a.is_del' => 0))
            ->order('a.users_read asc')
            ->page($param['page'],$param['limit'])
            ->select();
        }else
        {
            $count = M('users_notice_tpl_content')
            ->field('a.*,b.tpl_name')
            ->alias('a')
            ->join('users_notice_tpl b','a.source=b.send_scene')
            ->where(array('a.users_id' => $this->users_id,'a.source' => $param['source'],'a.is_del' => 0))
            ->count();
             $data['total_page'] = ceil($count/$param['limit']);
              $data['count'] = $count;
             $notice_tpl = M('users_notice_tpl_content')
            ->field('a.*,b.tpl_name')
            ->alias('a')
            ->join('users_notice_tpl b','a.source=b.send_scene')
            ->where(array('a.users_id' => $this->users_id,'a.is_del' => 0,'a.source' => $param['source']))
             ->order('a.users_read asc')
            ->page($param['page'],$param['limit'])
            ->select();
        }
        // dump($notice_tpl);die;
        
        $data['list'] = $liste;
        $data['status'] = 1;
        $data['mag'] = '';
        $data['res'] = $notice_tpl;
        echo json_encode($data,JSON_UNESCAPED_UNICODE);
        exit();
    }
    
    //用户信息未读条数
    
    public function unread()
    {
       $res = M('users_notice_tpl_content')->where(array('users_id' => $this->users_id,'users_read' => 0,'is_del' => 0))->count();
        $data['status'] = 1;
        $data['mag'] = '';
        $data['res'] = $res;
        echo json_encode($data,JSON_UNESCAPED_UNICODE);
        exit();
    }
    
    //已读
    public function read()
    {
        $param = input('post.');
        if($param['users_read'] == 0)
        {
            $res['users_read'] = 1;
            $notice =  M('users_notice_tpl_content')->where(array('content_id' => $param['content_id']))->save($res);
            if($notice)
            {
                $data['mag'] = '查看成功';
                $data['res'] = '';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit();
            }
        }
       
    }
    
    //删除信息
    public function del_notice()
    {
        $param = input('post.');
        
        $res['is_del'] = 1;
        $notice =  M('users_notice_tpl_content')->where(array('content_id' => $param['content_id']))->save($res);
        // dump($notice);die;
       if($notice)
        {
            $data['mag'] = '删除成功';
            $data['res'] = '';
            echo json_encode($data,JSON_UNESCAPED_UNICODE);
            exit();
        }
        
    }
    
    public function newss()
    {
        $users_id = $this->users_id;
        //dump($_SESSION['think']['users']['username']);die;
    //   $users_count = M('weapp_messages')->where(array('users_id' => $this->users_id,'read' => 0))->count();
    // dump($this->users_id);die;
    // $where[]=['exp','FIND_IN_SET(14,read_user)'];
    $username = $_SESSION['think']['users']['username'];
   // dump($username);die;
       $count = M('weapp_messages')->where(Db::raw("FIND_IN_SET('$users_id',users_ids)"))->where(Db::raw("FIND_IN_SET('$users_id',read_user)"))->count();
       $count1 = M('weapp_messages')->where(Db::raw("FIND_IN_SET('$users_id',users_ids)"))->count();
       $count2 = M('weapp_messages')->where(Db::raw("FIND_IN_SET('$users_id',read_user)"))->count();
       $count3 = $count1+$count2-$count;
       
       //dump
       $count4 = M('weapp_messages')->where(Db::raw("FIND_IN_SET('$username',users_id)"))->count();
        $count5 = M('weapp_messages')->where(array('users_id' =>''))->count();
        $count6 = $count4+$count5;
        $data['count'] = $count6-$count3;
       
       $data['users_private'] = M('users_private')->where(array('private_id' => $this->users_id,'news' => 0))->count();
       $datas['status'] = 1;
       $datas['msg'] = '';
       $datas['res'] = $data;
       
    //   dump($datas);die;
       echo json_encode($datas,JSON_UNESCAPED_UNICODE);
       exit;    
    }
    
    
    //站内信息已看
    public function read_users()
    {
        $users_id = $this->users_id;
        $count5 = M('weapp_messages')->where(array('users_id' =>''))->where(Db::raw("not FIND_IN_SET('$users_id',read_user)"))->select();
        $username = $_SESSION['think']['users']['username'];
         $count6 = M('weapp_messages')->where(Db::raw("FIND_IN_SET('$username',users_id)"))->select();
        // dump($count6);die;
        foreach ($count5 as $k => $v)
        {
            $rest['read_user'] = $v['read_user'].$users_id.',';
            $res = M('weapp_messages')->where(array('id' => $v['id']))->save($rest);
            // dump($res);die;
        }
        foreach ($count6 as $kk => $vv)
        {
            $use = explode(",", $vv['read_user']);
            // dump($users_id);
            // dump($use);
            if(!in_array($users_id,$use))
            {
                // dump(11);die;
                $rest['read_user'] = $vv['read_user'].$users_id.',';
                $res = M('weapp_messages')->where(array('id' => $vv['id']))->save($rest);
            }
        }
        if($res)
        {
            $datas['status'] = 1;
            $datas['msg'] = '查看成功';
            $datas['res'] = '';
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit;   
        }
    }
    
    public function del_read()  
    {
        $id = input('post.id');
        $res = M('weapp_messages')->where(array('id' => $id))->find();
        $data['users_ids'] = $res['users_ids'].$this->users_id.',';
         $use = explode(",", $res['read_user']);
         if(!in_array($this->users_id,$use))
         {
           $res = M('weapp_messages')->where(array('id' => $id))->save($data);
           if($res)
            {
                $datas['status'] = 1;
                $datas['msg'] = '删除成功';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;   
            } 
         }else
         {
               $datas['status'] = -1;
                $datas['msg'] = '该数据已删除';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;  
         }
        
    }
    //推荐关注
    public function recommend()
    {
        // dump($this->users_id);die;
        $follow_id = M('follow')->where(array('users_id' => $this->users_id))->getField('follow_id', true);
         array_push($follow_id,$this->users_id);
         array_push($follow_id,1);
        // dump($follow_id);die;
        $use = M('users')->where(array('users_id' => array('not in',$follow_id)))->order(rand(0,2))->limit(0,2)->select();
        // dump($use);die;
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $use;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit; 
    }
    
    
    public function limitoftimes($unique_id,$numberoftimes=10,$timespant="oneday",$verification=false){
        // dump($numberoftimes);die;
		$cacheneme = request()->url().$numberoftimes.'-'.$unique_id;
// 		dump($unique_id);die;
		$nowtimes = cache($cacheneme);//获取缓存中的当前次数
// 		dump( cache());die;
		if($nowtimes>=$numberoftimes)return false;
		if($verification)return true;
		if($timespant==="oneday"){
			$expires_in = strtotime(date('Y-m-d',strtotime('+1 day'))) - time();
		}else{
			$expires_inname = $cacheneme."-".$timespant;//设置时间缓存名称
			$expires_intime = cache($expires_inname);
			if($expires_intime){
				$expires_in = $timespant-(request()->time() - $expires_intime);
			}else{
				$expires_in = $timespant;
				cache($expires_inname,request()->time(),$expires_in);
			}
		}
// 		dump($nowtimes);die;
		$nowtimes = $nowtimes+1;//不可用++ 因为有false的情况
		cache($cacheneme,$nowtimes,$expires_in);
		
// 		dump($nowtimes);die;
		return $numberoftimes-$nowtimes;
	}
	
	    public function num($product_id,$num)
    {
        if(empty($num))
        {
            $data['code'] = -1;
            $data['smg'] = '商品数量最少为1';
            echo json_encode($data,JSON_UNESCAPED_UNICODE);
            exit(); 
        }
        if($num>=1 && $num<10)
        {
           $res = M('product_spec_data')->where(array('aid' => $product_id,'spec_value' => '1+'))->find();
        }
        if($num>=10 && $num<100)
        {
           $res = M('product_spec_data')->where(array('aid' => $product_id,'spec_value' => '10+'))->find();
        }
       if($num>=100 && $num<500)
        {
           $res = M('product_spec_data')->where(array('aid' => $product_id,'spec_value' => '100+'))->find();
        }
       if($num>=500 && $num<1000)
        {
           $res = M('product_spec_data')->where(array('aid' => $product_id,'spec_value' => '500+'))->find();
        }
       if($num>=1000 && $num<5000)
        {
           $res = M('product_spec_data')->where(array('aid' => $product_id,'spec_value' => '1000+'))->find();
        }
       if($num>=5000 && $num<10000)
        {
           $res = M('product_spec_data')->where(array('aid' => $product_id,'spec_value' => '5000+'))->find();
        }
        if($num>=10000 && $num<20000)
        {
           $res = M('product_spec_data')->where(array('aid' => $product_id,'spec_value' => '10000+'))->find();
        }
        if($num>=20000 && $num<50000)
        {
           $res = M('product_spec_data')->where(array('aid' => $product_id,'spec_value' => '20000+'))->find();
        }
        if($num>=50000 && $num<100000)
        {
           $res = M('product_spec_data')->where(array('aid' => $product_id,'spec_value' => '50000+'))->find();
        }
        if($num>=100000 && $num<1000000000)
        {
           $res = M('product_spec_data')->where(array('aid' => $product_id,'spec_value' => '100000+'))->find();
        }
        if(empty($res))
        {
           $rest = M('product_spec_value')->where(array('aid' => $product_id))->order('spec_price asc')->find();
            // dump($spec);die;
        }else
        {
           $rest =  M('product_spec_value')->where(array('spec_value_id' => $res['spec_value_id'],'aid' => $product_id))->find();
            // dump($res);die;
        }
        return $rest;
    }
    
    
    
}