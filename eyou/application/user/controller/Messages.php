<?php
/**
 * 易优CMS
 * ============================================================================
 * 版权所有 2016-2028 海南赞赞网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.eyoucms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 陈风任 <491085389@qq.com>
 * Date: 2019-7-3
 */

namespace app\user\controller;

use think\Db;
use think\Page;
/**
 * 会员收藏夹
 */
class Messages extends Base
{
    public function _initialize() {
        parent::_initialize();
		
		// 当前访问的方法名
        $Method = request()->action();
        $this->assign('Method',$Method);

    }

    public function index()
    {
        $list = array();
        $keywords = input('keywords/s');
        $limit = input('post.limit');
        $page = input('post.page');
        
        $condition = array();
        if (!empty($keywords)) {
            $condition['title|url'] = array('LIKE', "%{$keywords}%");
        }

		$users_id = session('users_id');
        $condition['users_id'] = 8;
        // 多语言
        $username = Db::name('users')->where($condition)->find();
//         $count = Db::name('weapp_messages')
// 	    ->field('a.*')
// 		->alias('a')
// 		->join('__WEAPP_MESSAGES_READ__ b', 'a.id = b.id', 'LEFT')
// 	    ->where(['b.users_id' => $username[username]])
// 		->whereOr(['a.users_id' => 0])
// 		->order('a.id desc')
//         ->count();// 查询满足要求的总记录数
//         // $Page = $pager = new Page($count, config('paginate.list_rows'));// 实例化分页类 传入总记录数和每页显示的记录数
//         $datas['total_page'] = ceil($count/$limit);
//         $datas['count'] = $count;
        
        $users_id = $username['users_id'];
        // dump($users_id);
        $list = Db::name('weapp_messages')
		    ->field('a.*')
			->alias('a')
			->join('__WEAPP_MESSAGES_READ__ b', 'a.id = b.id', 'LEFT')
			->where(Db::raw("not FIND_IN_SET($users_id,a.users_ids)"))
// 			->where('b.users_id = '.$username['username'])
			->where(['b.users_id' => $username[username]])
// 			->whereor(['a.users_id' => 0])
			->order('a.id desc')
// 			->page($page,$limit)
			->select();
// 			dump($list);
			
			
        $lists = Db::name('weapp_messages')
		    ->field('a.*')
			->alias('a')
			->join('__WEAPP_MESSAGES_READ__ b', 'a.id = b.id', 'LEFT')
			->where(Db::raw("not FIND_IN_SET($users_id,a.users_ids)"))
// 			->where('b.users_id = '.$username['username'])
// 			->where(['b.users_id' => $username[username]])
			->where(['a.users_id' => 0])
			->order('a.id desc')
// 			->page($page,$limit)
			->select();
// 			dump($lists);die;
			
// 			$page = $page + $limit;
            if($page == 1)
            {
              	$page = $page  -1;
            }else {
                if($limit != 1)
                {
                    $page = $limit;
                }else
                {
                	$page = $page  -1;
                }
                
            }
            // dump($page);
			
			
			$li = array_merge($list,$lists);
// 			dump($li)
			
			 $datas['total_page'] = ceil(count($li)/$limit);
			
			$datas['count'] = count($li);
			
		$li = array_slice($li,$page,$limit);
			
// 			dump($li);die;
// 			dump($lists);die;
			
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $li;
        // dump($datas);die;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit;
    }
	
}