<?php
/**
 * 易优CMS
 * ============================================================================
 * 版权所有 2016-2028 海南赞赞网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.eyoucms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小虎哥 <1105415366@qq.com>
 * Date: 2018-4-3
 */

namespace app\home\controller;

use think\Db;
use think\Verify;
use think\Config;

class Lists extends Base
{
    // 模型标识
    public $nid = '';
    // 模型ID
    public $channel = '';

    public function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 栏目列表
     */
    public function index($tid = '')
    {
        $param = input('param.');

        /*获取当前栏目ID以及模型ID*/
        $page_tmp = input('param.page/s', 0);
        if (empty($tid) || !is_numeric($page_tmp)) {
            abort(404, '页面不存在');
        }

        $map = [];
        /*URL上参数的校验*/
/*        $seo_pseudo = config('ey_config.seo_pseudo');
        $url_screen_var = config('global.url_screen_var');
        if (!isset($param[$url_screen_var]) && 3 == $seo_pseudo)
        {
            if (stristr($this->request->url(), '&c=Lists&a=index&')) {
                abort(404,'页面不存在');
            }
            $map = array('a.dirname'=>$tid);
        }
        else if (isset($param[$url_screen_var]) || 1 == $seo_pseudo || (2 == $seo_pseudo && isMobile()))
        {
            $seo_dynamic_format = config('ey_config.seo_dynamic_format');
            if (1 == $seo_pseudo && 2 == $seo_dynamic_format && stristr($this->request->url(), '&c=Lists&a=index&')) {
                abort(404,'页面不存在');
            } else if (!is_numeric($tid) || strval(intval($tid)) !== strval($tid)) {
                abort(404,'页面不存在');
            }
            $map = array('a.id'=>$tid);
            
        }else if (2 == $seo_pseudo){ // 生成静态页面代码
            
            $map = array('a.id'=>$tid);
        }*/
        /*--end*/
        if (!is_numeric($tid) || strval(intval($tid)) !== strval($tid)) {
            $map = array('a.dirname' => $tid);
        } else {
            $map = array('a.id' => intval($tid));
        }
        $map['a.is_del'] = 0; // 回收站功能
        $map['a.lang']   = $this->home_lang; // 多语言
        $row             = Db::name('arctype')->field('a.id, a.current_channel, b.nid')
            ->alias('a')
            ->join('__CHANNELTYPE__ b', 'a.current_channel = b.id', 'LEFT')
            ->where($map)
            ->find();
        if (empty($row)) {
            abort(404, '页面不存在');
        }
        $tid           = $row['id'];
        $this->nid     = $row['nid'];
        $this->channel = intval($row['current_channel']);
        /*--end*/

        $result = $this->logic($tid); // 模型对应逻辑

        $eyou       = array(
            'field' => $result,
        );
        $this->eyou = array_merge($this->eyou, $eyou);
        $this->assign('eyou', $this->eyou);

        /*模板文件*/
        $viewfile = !empty($result['templist'])
            ? str_replace('.' . $this->view_suffix, '', $result['templist'])
            : 'lists_' . $this->nid;
        /*--end*/

        /*多语言内置模板文件名*/
        if (!empty($this->home_lang)) {
            $viewfilepath = TEMPLATE_PATH . $this->theme_style_path . DS . $viewfile . "_{$this->home_lang}." . $this->view_suffix;
            if (file_exists($viewfilepath)) {
                $viewfile .= "_{$this->home_lang}";
            }
        }
        /*--end*/

        // /*模板文件*/
        // $viewfile = $filename = !empty($result['templist'])
        // ? str_replace('.'.$this->view_suffix, '',$result['templist'])
        // : 'lists_'.$this->nid;
        // /*--end*/

        // /*每个栏目内置模板文件名*/
        // $viewfilepath = TEMPLATE_PATH.$this->theme_style_path.DS.$filename."_{$result['id']}.".$this->view_suffix;
        // if (file_exists($viewfilepath)) {
        //     $viewfile = $filename."_{$result['id']}";
        // }
        // /*--end*/

        // /*多语言内置模板文件名*/
        // if (!empty($this->home_lang)) {
        //     $viewfilepath = TEMPLATE_PATH.$this->theme_style_path.DS.$filename."_{$this->home_lang}.".$this->view_suffix;
        //     if (file_exists($viewfilepath)) {
        //         $viewfile = $filename."_{$this->home_lang}";
        //     }
        //     /*每个栏目内置模板文件名*/
        //     $viewfilepath = TEMPLATE_PATH.$this->theme_style_path.DS.$filename."_{$result['id']}_{$this->home_lang}.".$this->view_suffix;
        //     if (file_exists($viewfilepath)) {
        //         $viewfile = $filename."_{$result['id']}_{$this->home_lang}";
        //     }
        //     /*--end*/
        // }
        // /*--end*/

        $view = ":{$viewfile}";
        if (51 == $this->channel) { // 问答模型
            $Ask = new \app\home\controller\Ask;
            return $Ask->index();
        }else{
            return $this->fetch($view);
        }
    }

    /**
     * 模型对应逻辑
     * @param intval $tid 栏目ID
     * @return array
     */
    private function logic($tid = '')
    {
        $result = array();

        if (empty($tid)) {
            return $result;
        }

        switch ($this->channel) {
            case '6': // 单页模型
            {
                $arctype_info = model('Arctype')->getInfo($tid);
                if ($arctype_info) {
                    // 读取当前栏目的内容，否则读取每一级第一个子栏目的内容，直到有内容或者最后一级栏目为止。
                    $archivesModel = new \app\home\model\Archives();
                    $result_new = $archivesModel->readContentFirst($tid);
                    // 阅读权限
                    if ($result_new['arcrank'] == -1) {
                        $this->success('待审核稿件，你没有权限阅读！');
                        exit;
                    }
                    // 外部链接跳转
                    if ($result_new['is_part'] == 1) {
                        $result_new['typelink'] = htmlspecialchars_decode($result_new['typelink']);
                        if (!is_http_url($result_new['typelink'])) {
                            $typeurl = '//'.$this->request->host();
                            if (!preg_match('#^'.ROOT_DIR.'(.*)$#i', $result_new['typelink'])) {
                                $typeurl .= ROOT_DIR;
                            }
                            $typeurl .= '/'.trim($result_new['typelink'], '/');
                            $result_new['typelink'] = $typeurl;
                        }
                        $this->redirect($result_new['typelink']);
                        exit;
                    }
                    /*自定义字段的数据格式处理*/
                    $result_new = $this->fieldLogic->getChannelFieldList($result_new, $this->channel);
                    /*--end*/
                    $result = array_merge($arctype_info, $result_new);

                    $result['templist'] = !empty($arctype_info['templist']) ? $arctype_info['templist'] : 'lists_'. $arctype_info['nid'];
                    $result['dirpath'] = $arctype_info['dirpath'];
                    $result['typeid'] = $arctype_info['typeid'];
                }
                break;
            }

            default:
            {
                $result = model('Arctype')->getInfo($tid);
                /*外部链接跳转*/
                if ($result['is_part'] == 1) {
                    $result['typelink'] = htmlspecialchars_decode($result['typelink']);
                    if (!is_http_url($result['typelink'])) {
                        $result['typelink'] = '//'.$this->request->host().ROOT_DIR.'/'.trim($result['typelink'], '/');
                    }
                    $this->redirect($result['typelink']);
                    exit;
                }
                /*end*/
                break;
            }
        }

        if (!empty($result)) {
            /*自定义字段的数据格式处理*/
            $result = $this->fieldLogic->getTableFieldList($result, config('global.arctype_channel_id'));
            /*--end*/
        }

        /*是否有子栏目，用于标记【全部】选中状态*/
        $result['has_children'] = model('Arctype')->hasChildren($tid);
        /*--end*/

        // seo
        $result['seo_title'] = set_typeseotitle($result['typename'], $result['seo_title']);

        /*获取当前页面URL*/
        $result['pageurl'] = $this->request->url(true);
        /*--end*/

        /*给没有type前缀的字段新增一个带前缀的字段，并赋予相同的值*/
        foreach ($result as $key => $val) {
            if (!preg_match('/^type/i', $key)) {
                $key_new = 'type' . $key;
                !array_key_exists($key_new, $result) && $result[$key_new] = $val;
            }
        }
        /*--end*/

        return $result;
    }

    /**
     * 留言提交
     */
    public function gbook_submit()
    {
        $typeid = input('post.typeid/d');

        if (IS_POST && !empty($typeid)) {
            $channel_guestbook_gourl = tpSetting('channel_guestbook.channel_guestbook_gourl');
            if (!empty($channel_guestbook_gourl)) {
                $gourl = $channel_guestbook_gourl;
            } else {
                $gourl = input('post.gourl/s');
            }
            $post = input('post.');
            unset($post['gourl']);

            $token = '__token__';
            foreach ($post as $key => $val) {
                if (preg_match('/^__token__/i', $key)) {
                    $token = $key;
                    continue;
                }
            }
            $ip = clientIP();

            /*留言间隔限制*/
            $channel_guestbook_interval = tpSetting('channel_guestbook.channel_guestbook_interval');
            $channel_guestbook_interval = is_numeric($channel_guestbook_interval) ? intval($channel_guestbook_interval) : 60;
            if (0 < $channel_guestbook_interval) {
                $map   = array(
                    'ip'       => $ip,
                    'typeid'   => $typeid,
                    'lang'     => $this->home_lang,
                    'add_time' => array('gt', getTime() - $channel_guestbook_interval),
                );
                $count = Db::name('guestbook')->where($map)->count('aid');
                if ($count > 0) {
                    if ($this->home_lang == 'cn') {
                        $msg = '同一个IP在'.$channel_guestbook_interval.'秒之内不能重复提交！';
                    } else if ($this->home_lang == 'zh') {
                        $msg = '同一個IP在'.$channel_guestbook_interval.'秒之內不能重複提交！';
                    } else {
                        $msg = 'The same IP cannot be submitted repeatedly within '.$channel_guestbook_interval.' seconds!';
                    }
                    $this->error($msg);
                }
            }
            /*end*/

            //判断必填项            
            $ContentArr = []; // 添加站内信所需参数
            foreach ($post as $key => $value) {
                if (stripos($key, "attr_") !== false) {
                    //处理得到自定义属性id
                    $attr_id = substr($key, 5);
                    $attr_id = intval($attr_id);
                    $ga_data = Db::name('guestbook_attribute')->where([
                        'attr_id'   => $attr_id,
                        'lang'      => $this->home_lang,
                    ])->find();
                    if ($ga_data['required'] == 1) {
                        if (empty($value)) {
                            if ($this->home_lang == 'cn') {
                                $msg = $ga_data['attr_name'] . '不能为空！';
                            } else if ($this->home_lang == 'zh') {
                                $msg = $ga_data['attr_name'] . '不能為空！';
                            } else {
                                $msg = $ga_data['attr_name'] . 'Cannot be empty!';
                            }
                            $this->error($msg);
                        } else {
                            if ($ga_data['validate_type'] == 6) {
                                $pattern  = "/^1\d{10}$/";
                                if (!preg_match($pattern, $value)) {
                                    if ($this->home_lang == 'cn') {
                                        $msg = $ga_data['attr_name'] . '格式不正确！';
                                    } else if ($this->home_lang == 'zh') {
                                        $msg = $ga_data['attr_name'] . '格式不正確！';
                                    } else {
                                        $msg = $ga_data['attr_name'] . 'Incorrect format!';
                                    }
                                    $this->error($msg);
                                }
                            } elseif ($ga_data['validate_type'] == 7) {
                                $pattern  = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i";
                                if (preg_match($pattern, $value) == false) {
                                    if ($this->home_lang == 'cn') {
                                        $msg = $ga_data['attr_name'] . '格式不正确！';
                                    } else if ($this->home_lang == 'zh') {
                                        $msg = $ga_data['attr_name'] . '格式不正確！';
                                    } else {
                                        $msg = $ga_data['attr_name'] . 'Incorrect format!';
                                    }
                                    $this->error($msg);
                                }
                            }
                        }
                    }

                    // 添加站内信所需参数
                    array_push($ContentArr, $value);
                }
            }

            /* 处理判断验证码 */
            $is_vertify        = 1; // 默认开启验证码
            $guestbook_captcha = config('captcha.guestbook');
            if (!function_exists('imagettftext') || empty($guestbook_captcha['is_on'])) {
                $is_vertify = 0; // 函数不存在，不符合开启的条件
            }
            if (1 == $is_vertify) {
                if (empty($post['vertify'])) {
                    if ($this->home_lang == 'cn') {
                        $msg = '图片验证码不能为空！';
                    } else if ($this->home_lang == 'zh') {
                        $msg = '圖片驗證碼不能為空！';
                    } else {
                        $msg = 'Picture verification code cannot be empty!';
                    }
                    $this->error($msg);
                }

                $verify = new Verify();
                if (!$verify->check($post['vertify'], $token)) {
                    if ($this->home_lang == 'cn') {
                        $msg = '图片验证码不正确！';
                    } else if ($this->home_lang == 'zh') {
                        $msg = '圖片驗證碼不正確！';
                    } else {
                        $msg = 'The picture verification code is incorrect!';
                    }
                    $this->error($msg);
                }
            }
            /* END */

            $channeltype_list = config('global.channeltype_list');
            $this->channel = !empty($channeltype_list['guestbook']) ? $channeltype_list['guestbook'] : 8;

            $newData = array(
                'typeid'      => $typeid,
                'channel'     => $this->channel,
                'ip'          => $ip,
                'lang'        => $this->home_lang,
                'add_time'    => getTime(),
                'update_time' => getTime(),
            );
            $data    = array_merge($post, $newData);

            // 数据验证
            $rule     = [
                'typeid' => 'require|token:' . $token,
            ];
            $message  = [
                'typeid.require' => '表单缺少标签属性{$field.hidden}',
            ];
            $validate = new \think\Validate($rule, $message);
            if (!$validate->batch()->check($data)) {
                $error     = $validate->getError();
                $error_msg = array_values($error);
                $this->error($error_msg[0]);
            } else {
                $guestbookRow = [];
                /*处理是否重复表单数据的提交*/
                $formdata = $data;
                foreach ($formdata as $key => $val) {
                    if (in_array($key, ['typeid', 'lang']) || preg_match('/^attr_(\d+)$/i', $key)) {
                        continue;
                    }
                    unset($formdata[$key]);
                }
                $md5data         = md5(serialize($formdata));
                $data['md5data'] = $md5data;
                $guestbookRow    = Db::name('guestbook')->field('aid')->where(['md5data' => $md5data])->find();
                /*--end*/
                $dataStr = '';
                if (empty($guestbookRow)) { // 非重复表单的才能写入数据库
                    $aid = Db::name('guestbook')->insertGetId($data);
                    if ($aid > 0) {
                        $res = $this->saveGuestbookAttr($aid, $typeid);
                        if ($res){
                            $this->error($res);
                        }
                    }
                    /*插件 - 邮箱发送*/
                    $data    = [
                        'gbook_submit',
                        $typeid,
                        $aid,
                    ];
                    $dataStr = implode('|', $data);
                    /*--end*/

                    /*发送站内信给后台*/
                    SendNotifyMessage($ContentArr, 1, 1, 0);
                    /* END */
                } else {
                    // 存在重复数据的表单，将在后台显示在最前面
                    Db::name('guestbook')->where('aid', $guestbookRow['aid'])->update([
                        'add_time' => getTime(),
                        'update_time' => getTime(),
                    ]);
                }
                
                if ($this->home_lang == 'cn') {
                    $msg = '操作成功';
                } else if ($this->home_lang == 'zh') {
                    $msg = '操作成功';
                } else {
                    $msg = 'success';
                }
                $channel_guestbook_time = tpSetting('channel_guestbook.channel_guestbook_time');
                $channel_guestbook_time = !empty($channel_guestbook_time) ? intval($channel_guestbook_time) : 5;
                $this->success($msg, $gourl, $dataStr, $channel_guestbook_time);
            }
        }

        $this->error('表单缺少标签属性{$field.hidden}');
    }

    /**
     *  给指定留言添加表单值到 guestbook_attr
     * @param int $aid 留言id
     * @param int $typeid 留言栏目id
     */
    private function saveGuestbookAttr($aid, $typeid)
    {
        // post 提交的属性  以 attr_id _ 和值的 组合为键名    
        $post = input("post.");
        $arr = explode(',', config('global.image_ext'));
        /*上传图片或附件*/
        foreach ($_FILES as $fileElementId => $file) {
            try {
                if (!empty($file['name']) && !is_array($file['name'])) {
                    $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
                    if (in_array($ext,$arr)){
                        $uplaod_data = func_common($fileElementId, 'allimg');
                    }else{
                        $uplaod_data = func_common_doc($fileElementId, 'files');
                    }
                    if (0 == $uplaod_data['errcode']) {
                        $post[$fileElementId] = $uplaod_data['img_url'];
                    } else {
                        return $uplaod_data['errmsg'];
//                        $post[$fileElementId] = '';
                    }
                }
            } catch (\Exception $e) {}
        }
        /*end*/

        $attrArr = [];

        /*多语言*/
        if (is_language()) {
            foreach ($post as $key => $val) {
                if (preg_match_all('/^attr_(\d+)$/i', $key, $matchs)) {
                    $attr_value           = intval($matchs[1][0]);
                    $attrArr[$attr_value] = [
                        'attr_id' => $attr_value,
                    ];
                }
            }
            $attrArr = model('LanguageAttr')->getBindValue($attrArr, 'guestbook_attribute'); // 多语言
        }
        /*--end*/

        foreach ($post as $k => $v) {
            if (!strstr($k, 'attr_')) continue;
            $attr_id = str_replace('attr_', '', $k);
            is_array($v) && $v = implode(PHP_EOL, $v);

            /*多语言*/
            if (!empty($attrArr)) {
                $attr_id = $attrArr[$attr_id]['attr_id'];
            }
            /*--end*/

            //$v = str_replace('_', '', $v); // 替换特殊字符
            //$v = str_replace('@', '', $v); // 替换特殊字符
            $v       = trim($v);
            $adddata = array(
                'aid'         => $aid,
                'attr_id'     => $attr_id,
                'attr_value'  => $v,
                'lang'        => $this->home_lang,
                'add_time'    => getTime(),
                'update_time' => getTime(),
            );
            Db::name('GuestbookAttr')->add($adddata);
        }
    }
    
    
    //首页产品代理
    public function brand()
    {
       $brand =  M('product_brand')->where(array('status' => 1))->order('sort_order desc')->select();
    //   dump($brand);die;
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $brand;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit; 
    //   dump($brand);die;
    }
    
    //首页全部产品分类
    public function product_fenlei()
    {
       $product = M('arctype')->where(array('current_channel' => 2,'parent_id' => 0))->where('id != 3')->order('sort_order asc,id asc')->select();
       
      foreach($product as $k => $v)
      {
          $product[$k]['gift'] = M('arctype')->where(array('parent_id' => $v['id']))->select();
         foreach ($product[$k]['gift'] as $kk => $vv)
           {
              $product[$k]['gift'][$kk]['gift'] = M('arctype')->where(array('parent_id' => $vv['id']))->order('sort_order asc,id asc')->select();
          }
      }
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $product;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
    }
    
    public function product()
    {
         $product_count = M('archives')->where(array('channel' => 2))->count();
         $product = M('arctype')->where(array('current_channel' => 2,'grade' => 1))->where('id != 3 and parent_id !=0 and parent_id != 3')->order('id asc')->select();
         foreach($product as $k => $v)
         {
            $product[$k]['gift'] = M('arctype')->where(array('parent_id' => $v['id']))->select();
            foreach ($product[$k]['gift'] as $kk => $vv)
           {
                $product[$k]['ids'][] = $vv['id'];
                $product[$k]['gift'][$kk]['product_count'] = M('archives')->where(array('typeid' => $vv['id']))->count();
           }
          $product[$k]['counts'] =  M('archives')->where(array('typeid' => array('in',$product[$k]['ids'])))->count();
          $datas['count'] +=  $product[$k]['counts'];
         }
        //  dump($product);die;
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $product;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit;
    }
    
    
    //二级列表页筛选参数
    public function option()
    {
        $post = input('post.');
        $rest = M('archives')->where(array('typeid' => $post['typeid']))->getField('aid', true);
        $brand_id = M('archives')->where(array('typeid' => $post['typeid']))->getField('brand_id', true);
        // dump($brand_id);die;
        // dump($rest);die;
        
        $attr = M('shop_product_attribute')->where(array('attr_name' => '参考封装'))->getField('attr_id', true);
        $attrs = M('shop_product_attribute')->where(array('attr_name' => '封装/规格'))->getField('attr_id', true);
        // $attrs = M('shop_product_attribute')->where(array('attr_name' => '品牌/产地'))->select();
        $brand = M('product_brand')->where(array('id' => array('in',$brand_id)))->select();
        $attrss = M('shop_product_attribute')->where(array('attr_name' => '包装/方式'))->getField('attr_id', true);
        
        $sarray =  M('shop_product_attr')->where(array('aid' => array('in',$rest),'attr_id' => array('in',$attr)))->getField('attr_value', true);
        $sarrays =  M('shop_product_attr')->where(array('aid' => array('in',$rest),'attr_id' => array('in',$attrs)))->getField('attr_value', true);
        $sarrayss =  M('shop_product_attr')->where(array('aid' => array('in',$rest),'attr_id' => array('in',$attrss)))->getField('attr_value', true);
        $dats['brand'] = $brand;
        $dats['specifications'] = array_unique($sarray);
        $dats['encapsulation'] = array_unique($sarrays);
        $dats['packing'] =  array_unique($sarrayss);
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $dats;
        // dump($dats);die;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit;
    }
    
    //一级列表页筛选参数
    public function option_level()
    {
        $post = input('post.');
        
        
        if($post['typeid'] != 0)
        {
            $arctype = M('arctype')->where(array('id' => $post['typeid']))->find();
            // dump($arctype);die;
            if($arctype['grade'] == 1)
            {
                 $id = M('arctype')->where(array('parent_id' => array('in',$post['typeid'])))->getField('id', true);
            }else
            {
                $id = $post['typeid'];
            }
        }else
        {
           $id = M('arctype')->where(array('current_channel' => 2,'grade' => 2))->getField('id', true); 
        }
        
        // dump($id);die;
  
        
        $brand_id = M('archives')->where(array('typeid' => array('in',$id)))->getField('brand_id', true);
        
        $rest = M('archives')->where(array('typeid' => array('in',$id)))->getField('aid', true);
        
        $attr = M('shop_product_attribute')->where(array('attr_name' => '参考封装'))->getField('attr_id', true);
        $attrs = M('shop_product_attribute')->where(array('attr_name' => '封装/规格'))->getField('attr_id', true);
        // $attrs = M('shop_product_attribute')->where(array('attr_name' => '品牌/产地'))->select();
        $brand = M('product_brand')->where(array('id' => array('in',$brand_id)))->select();
        
        $attrss = M('shop_product_attribute')->where(array('attr_name' => '包装/方式'))->getField('attr_id', true);
        
        $sarray =  M('shop_product_attr')->where(array('aid' => array('in',$rest),'attr_id' => array('in',$attr)))->getField('attr_value', true);
        $sarrays =  M('shop_product_attr')->where(array('aid' => array('in',$rest),'attr_id' => array('in',$attrs)))->getField('attr_value', true);
        $sarrayss =  M('shop_product_attr')->where(array('aid' => array('in',$rest),'attr_id' => array('in',$attrss)))->getField('attr_value', true);
        
       $arc = M('arctype')->where(array('id' => array('in',$id)))->select();
    //   dump($arc);die;
       foreach ($arc as $k => $v)
       {
           $dats['arc']['specifications'][] = $v['typename'];
           $dats['arc']['aid'][] = $v['id'];
       }
           $dats['arc']['title'] = '分类';
            $dats['arc']['value'] = '';
        // dump($brand);die;
        foreach($brand as $k => $v)
        {
            $dats['brand']['specifications'][] = $v['name'];
        }
            $dats['brand']['title'] = '品牌';
            $dats['brand']['value'] = '';
        // $dats['brand'] = $brand;
        
        $dats['res'] = array(
            array('title' => '参考封装','specifications' => array_unique($sarray),'value' => ''),
            array('title' => '封装/规格','specifications' => array_unique($sarrays),'value' => ''),
            array('title' => '包装/方式','specifications' => array_unique($sarrayss),'value' => ''),
            );
            
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $dats;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit;
    }
    
    
    public function product_list_level()
    {
        $post = input("post.");
        
        if($post['typeid'] == 0)
        {
           $id = M('arctype')->where(array('current_channel' => 2,'grade' => 2))->getField('id', true);
            
        }else
        {
            // dump($post);die;
             $arctype = M('arctype')->where(array('id' => $post['typeid']))->find();
            if($arctype['grade'] == 1)
            {
                $id = M('arctype')->where(array('parent_id' => array('in',$post['typeid'])))->getField('id', true);
            }else
            {
                $id = $post['typeid'];
            }
        }
        if(!empty($post['aid']))
        {
           $id = $post['aid'];
        }
        
        // dump($id);die;
        
        // $id = M('arctype')->where(array('parent_id' => array('in',$post['typeid'])))->getField('id', true);
   
        
        // dump($id);die;
        // $rest = M('archives')->where(array('typeid' => array('in',$id)))->getField('aid', true);
        // dump($rest);die;
        // $count = count(explode(',',$post['attr_value']));
        
        // dump(explode(',',$post['attr_value']));die;
        if(!empty($post['attr_value']))
        {
            $rest = M('shop_product_attr')
            ->where(array('attr_value' => array('in',$post['attr_value'])))
            ->getField('aid', true);
            // dump($rest);die;
            $rest = array_count_values($rest);
            // dump($rest);die;
            foreach ($rest as $k => $v)
            {
                if($v == $post['count'])
                {
                    $selts[] = $k;
                }
            }
            // dump($selts);die;
             $wheress = array('a.aid' => array('in',array_unique($selts)));
        }else
        {
            $wheress = [];
        }
   
        
        $order = '';
        $lists = [];
       if(!empty($post['sales_num']))
        {
            $order .= 'a.sales_num '.$post['sales_num'];
        }
        if(empty($post['sales_num']))
        {
            if(!empty($post['users_price']))
            {
                $order .= 'a.users_price '.$post['users_price'];
            }
        }else
        {
            if(!empty($post['users_price']))
            {
                $order .= ' and a.users_price '.$post['users_price'];
            } 
        }
        // dump($post);die;
    //     if(!empty($post['package1']))
    //     {
    //         $selt = M('shop_product_attr')->where(array('attr_value' => $post['package1'],'aid' => array('in',$rest)))->getField('aid', true);
    //         if(empty($selt))
    //         {
    //             $selt = ['hxf'];
    //         }
    //     }
    //     if(!empty($post['specifications1']))
    //     {
    //          $selt1 = M('shop_product_attr')->where(array('attr_value' => $post['specifications1'],'aid' => array('in',$rest)))->getField('aid', true);
    //         if(empty($selt1))
    //         {
    //             $selt1 = ['hxf'];
    //         }
    //         //  array_push($lists,$post['specifications1']);
    //     }
    //     if(!empty($post['packing1']))
    //     {
    //          $selt2 = M('shop_product_attr')->where(array('attr_value' => $post['packing1'],'aid' => array('in',$rest)))->getField('aid', true);
    //       if(empty($selt2))
    //         {
    //             $selt2[] = 'hxf';
    //         }
    //     }
    //     if(!empty($post['brand']))
    //     {
    //           $brand = M('product_brand')->where(array('name' =>$post['brand']))->getField('id', true);
    //           $brand_id = M('archives')->where(array('typeid' => array('in',$id),'brand_id' => array('in',$brand)))->getField('aid', true);
    //           if(empty($brand_id))
    //           {
    //               $brand_id = ['hxf'];
    //           }
    //     }
     
       
       
    //   if(!empty($selt) && !empty($selt1) && !empty($selt2) && !empty($brand_id))
    //   {
    //       $selts = array_intersect($selt,$selt1,$selt2,$brand_id);
    //   }elseif (!empty($selt) && !empty($selt1) && !empty($selt2)) {
    //       $selts = array_intersect($selt,$selt1,$selt2);
    //   }elseif(!empty($selt) && !empty($selt1) && !empty($brand_id))
    //   {
    //       $selts = array_intersect($selt,$selt1,$brand_id); 
    //   }elseif(!empty($selt) && !empty($selt2) && !empty($brand_id))
    //   {
    //         $selts = array_intersect($selt,$selt2,$brand_id); 
    //   }elseif(!empty($selt1) && !empty($selt2) && !empty($brand_id))
    //   {
    //     //   dump(11)
    //         $selts = array_intersect($selt1,$selt2,$brand_id); 
    //   }elseif(!empty($selt) && !empty($selt2))
    //   {
    //       $selts = array_intersect($selt,$selt2); 
    //   }elseif(!empty($selt) && !empty($selt1))
    //   {
    //       $selts = array_intersect($selt,$selt1); 
    //   }elseif(!empty($selt) && !empty($brand_id))
    //   {
    //       $selts = array_intersect($selt,$brand_id); 
    //   }elseif(!empty($selt1) && !empty($brand_id))
    //   {
    //       $selts = array_intersect($selt1,$brand_id); 
    //   }elseif(!empty($selt1) && !empty($selt2))
    //   {
    //       $selts = array_intersect($selt2,$selt1); 
    //   }elseif(!empty($selt2) && !empty($brand_id))
    //   {
    //       $selts = array_intersect($selt2,$brand_id); 
    //   }elseif(!empty($selt))
    //   {
    //       $selts = $selt; 
    //   }elseif(!empty($selt1))
    //   {
    //       $selts = $selt1; 
    //   }elseif(!empty($selt2))
    //   {
    //       $selts = $selt2; 
    //   }elseif(!empty($brand_id))
    //   {
    //       $selts = $brand_id; 
    //   }
       
       
       if($post['brand'] != '')
       {
           $wheres = array('c.name' => array('in',$post['brand']));
       }else
       {
           $wheres = []; 
       }
       
       
    //   dump($wheress);die;
       if(!empty($post['brand']) || !empty($post['attr_value']))
       {
           
            $count = M('archives')
            ->field('a.*,b.weight,b.number,c.name as brand')
            ->alias('a')
            ->join('product_content b','a.aid=b.aid')
            ->join('product_brand c' , 'c.id = a.brand_id')
            ->where(array('a.typeid' => array('in',$id)))
            ->where($wheres)
            ->where($wheress)
            ->where(array('arcrank' => 0))
            ->order($order)
            ->count();
           
           
             $arc = M('archives')
            ->field('a.*,b.weight,b.number,c.name as brand')
            ->alias('a')
            ->join('product_content b','a.aid=b.aid')
            ->join('product_brand c' , 'c.id = a.brand_id')
            ->where(array('a.typeid' => array('in',$id)))
            ->where($wheres)
            ->where($wheress)
             ->where(array('arcrank' => 0))
            ->page($post['page'],$post['limit'])
            ->order($order)
            ->select();
            // dump($arc);die;
       }else
       {
            $count = M('archives')
            ->field('a.*,b.weight,b.number,c.name as brand')
            ->alias('a')
            ->join('product_content b','a.aid=b.aid')
            ->join('product_brand c' , 'c.id = a.brand_id')
            ->where(array('a.typeid' => array('in',$id)))
             ->where(array('arcrank' => 0))
            ->order($order)
            ->count();
            // dump($count);die;
           
           $arc = M('archives')
            ->field('a.*,b.weight,b.number,c.name as brand')
            ->alias('a')
            ->join('product_content b','a.aid=b.aid')
            ->join('product_brand c' , 'c.id = a.brand_id')
            ->where(array('a.typeid' => array('in',$id)))
             ->where(array('arcrank' => 0))
            ->page($post['page'],$post['limit'])
            ->order($order)
            ->select();
       }
       
    //   $_SESSION['think']['users_id']
       
        foreach($arc as $k => $v)
        {
           $arc[$k]['specifications_id'] = '';
           $arc[$k]['product_num'] = '1';
           $arc[$k]['product_type'] = '个';
           $collection = M('product_collection')->where(array('users_id' =>  $_SESSION['think']['users_id'],'product_id' => $v['aid']))->find();
           
           if(empty($collection))
           {
               $arc[$k]['collection'] = 0;
           }else
           {
                $arc[$k]['collection'] = 1;
           }
           
          $spe =  M('specifications')->where(array('product_id' => $v['aid']))->select();
          foreach ($spe as $kk => $vv)
          {
              $spe[$kk]['status'] = '';
          }
          $arc[$k]['specifications'] = $spe;
          $product_spec = M('product_spec_value')
          ->field('b.spec_value,a.spec_price,a.value_id,a.aid')
          ->alias('a')
          ->join('product_spec_data b','b.aid=a.aid and a.spec_value_id = b.spec_value_id', 'LEFT')
          ->where(array('a.aid' => $v['aid']))
          ->orderRaw("LENGTH(b.spec_value) asc,b.spec_value asc")
          ->select();
          $arc[$k]['product_spec'] = $product_spec;
          
         $attr = M('shop_product_attr')
          ->field('a.attr_value,b.attr_name')
          ->alias('a')
          ->join('shop_product_attribute b','b.attr_id=a.attr_id', 'LEFT')
          ->where(array('a.aid' => $v['aid']))
          ->select();
          foreach($attr as $kk => $vv)
          {
              if($vv['attr_name'] == '参考封装')
              {
                  $arc[$k]['package1'] = $vv['attr_value'];
              }
             if($vv['attr_name'] == '封装/规格')
              {
                  $arc[$k]['specifications1'] = $vv['attr_value'];
              }
               if($vv['attr_name'] == '包装/方式')
              {
                  $arc[$k]['packing1'] = $vv['attr_value'];
              }
          }
          
            $shop = M('shop_order_details')
           ->alias('a')
           ->join('shop_order b','b.order_id=a.order_id')
           ->where('b.order_status = 1 or b.order_status = 3 or b.order_status = 2')
           ->where(array('a.product_id' => $v['aid']))
           ->count();
           
         $shope = M('shop_order_details')
           ->alias('a')
           ->join('shop_order b','b.order_id=a.order_id')
           ->whereTime('a.add_time', 'month')
           ->where('b.order_status = 1 or b.order_status = 3 or b.order_status = 2')
           ->where(array('a.product_id' => $v['aid']))
           ->count();
         $arc[$k]['sales_num'] = $shop;//总单
         $arc[$k]['near_num'] = $shope;//近期
         
        }
        
    $sort = array(
        'direction' => 'SORT_DESC', //排序顺序标志 SORT_DESC 降序；SORT_ASC 升序
        'field'     => 'sales_num',       //排序字段
        );
        
        $arrSort = array();
        foreach($arc AS $uniqid => $row){
            foreach($row AS $key=>$value){
                $arrSort[$key][$uniqid] = $value;
            }
        }
        if($sort['direction']){
            array_multisort($arrSort[$sort['field']], constant($sort['direction']), $arc);
        }

        
        
        
        $datas['count'] = $count;
        $datas['total_page'] = ceil($count/$post['limit']);
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $arc;
        // dump($datas);die;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit;
    }

    
    
    
    //品牌检索
    
    public function brand_search()
    {
         $post = input('post.');
         $brand = M('product_brand')->where(array('name' => $post['name']))->find();
         $order = '';
        // $lists = [];
       if(!empty($post['sales_num']))
        {
            $order .= 'a.sales_num '.$post['sales_num'];
        }
        if(empty($post['sales_num']))
        {
            if(!empty($post['users_price']))
            {
                $order .= 'a.users_price '.$post['users_price'];
            }
        }else
        {
            if(!empty($post['users_price']))
            {
                $order .= ' and a.users_price '.$post['users_price'];
            } 
        }
         
        $count = M('archives')
            ->field('a.*,b.weight,b.number,c.name as brand')
            ->alias('a')
            ->join('product_content b','a.aid=b.aid')
            ->join('product_brand c' , 'c.id = a.brand_id')
            ->where(array('brand_id' => array('in',$brand['id'])))
            ->page($post['page'],$post['limit'])
            ->count();
         $datas['count'] = $count;
         $datas['total_page'] = ceil($count/$post['limit']);
         
         $arc = M('archives')
            ->field('a.*,b.weight,b.number,c.name as brand')
            ->alias('a')
            ->join('product_content b','a.aid=b.aid')
            ->join('product_brand c' , 'c.id = a.brand_id')
            ->where(array('brand_id' => array('in',$brand['id'])))
            ->page($post['page'],$post['limit'])
            ->order($order)
            ->select();
            
        foreach($arc as $k => $v)
        {
           $arc[$k]['specifications_id'] = '';
           $arc[$k]['product_num'] = '1';
           $arc[$k]['product_type'] = '个'; 
           $collection = M('product_collection')->where(array('users_id' =>  $_SESSION['think']['users_id'],'product_id' => $v['aid']))->find();
           if(empty($collection))
           {
               $arc[$k]['collection'] = 0;
           }else
           {
                $arc[$k]['collection'] = 1;
           }
          $spe =  M('specifications')->where(array('product_id' => $v['aid']))->select();
          foreach ($spe as $kk => $vv)
          {
              $spe[$kk]['status'] = '';
          }
          $arc[$k]['specifications'] = $spe;
          $product_spec = M('product_spec_value')
          ->field('b.spec_value,a.spec_price,a.value_id,a.aid')
          ->alias('a')
          ->join('product_spec_data b','b.aid=a.aid and a.spec_value_id = b.spec_value_id', 'LEFT')
          ->where(array('a.aid' => $v['aid']))
          ->order('a.spec_price desc')
          ->select();
          
          $arc[$k]['product_spec'] = $product_spec;
          
         $attr = M('shop_product_attr')
          ->field('a.attr_value,b.attr_name')
          ->alias('a')
          ->join('shop_product_attribute b','b.attr_id=a.attr_id', 'LEFT')
          ->where(array('a.aid' => $v['aid']))
          ->select();
          foreach($attr as $kk => $vv)
          {
              if($vv['attr_name'] == '参考封装')
              {
                  $arc[$k]['package1'] = $vv['attr_value'];
              }
             if($vv['attr_name'] == '封装/规格')
              {
                  $arc[$k]['specifications1'] = $vv['attr_value'];
              }
               if($vv['attr_name'] == '包装/方式')
              {
                  $arc[$k]['packing1'] = $vv['attr_value'];
              }
          }
          
        $shop = M('shop_order_details')
           ->alias('a')
           ->join('shop_order b','b.order_id=a.order_id')
           ->where('b.order_status = 1 or b.order_status = 3 or b.order_status = 2')
           ->where(array('a.product_id' => $v['aid']))
           ->count();
           
         $shope = M('shop_order_details')
           ->alias('a')
           ->join('shop_order b','b.order_id=a.order_id')
           ->whereTime('a.add_time', 'month')
           ->where('b.order_status = 1 or b.order_status = 3 or b.order_status = 2')
           ->where(array('a.product_id' => $v['aid']))
           ->count();
         $arc[$k]['sales_num'] = $shop;//总单
         $arc[$k]['near_num'] = $shope;//近期
        }
        
        $sort = array(
        'direction' => 'SORT_DESC', //排序顺序标志 SORT_DESC 降序；SORT_ASC 升序
        'field'     => 'sales_num',       //排序字段
        );
        
        $arrSort = array();
        foreach($arc AS $uniqid => $row){
            foreach($row AS $key=>$value){
                $arrSort[$key][$uniqid] = $value;
            }
        }
        if($sort['direction']){
            array_multisort($arrSort[$sort['field']], constant($sort['direction']), $arc);
        }

        
        
        // dump($arc);die;
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $arc;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit;
            
    }
    
    
    //二级商品列表
    // public function product_list()
    // {
    //     $post = input("post.");
    //     $order = '';
    //     $lists = [];
        
    //   $count = count(explode(',',$post['attr_value']));
        
    //     // dump
    //     $rest = M('shop_product_attr')
    //     ->where(array('attr_value' => array('in',$post['attr_value'])))
    //     ->getField('aid', true);
    //     $rest = array_count_values($rest);
    //     // dump($rest);
    //     foreach ($rest as $k => $v)
    //     {
    //         if($v == $count)
    //         {
    //             $selts[] = $k;
    //         }
    //     }
        
        
    //     // dump($selts);die;
    //     if(!empty($post['sales_num']))
    //     {
    //         $order .= 'a.sales_num '.$post['sales_num'];
    //     }
    //     if(empty($post['sales_num']))
    //     {
    //         if(!empty($post['users_price']))
    //         {
    //             $order .= 'a.users_price '.$post['users_price'];
    //         }
    //     }else
    //     {
    //         if(!empty($post['users_price']))
    //         {
    //             $order .= ' and a.users_price '.$post['users_price'];
    //         } 
    //     }
    //     //       if(!empty($post['package1']))
    //     // {
    //     //     $selt = M('shop_product_attr')->where(array('attr_value' => $post['package1'],'aid' => array('in',$rest)))->getField('aid', true);
    //     //     if(empty($selt))
    //     //     {
    //     //         $selt = ['hxf'];
    //     //     }
    //     // }
    //     // if(!empty($post['specifications1']))
    //     // {
    //     //      $selt1 = M('shop_product_attr')->where(array('attr_value' => $post['specifications1'],'aid' => array('in',$rest)))->getField('aid', true);
    //     //     if(empty($selt1))
    //     //     {
    //     //         $selt1 = ['hxf'];
    //     //     }
    //     //     //  array_push($lists,$post['specifications1']);
    //     // }
    //     // if(!empty($post['packing1']))
    //     // {
    //     //      $selt2 = M('shop_product_attr')->where(array('attr_value' => $post['packing1'],'aid' => array('in',$rest)))->getField('aid', true);
    //     //   if(empty($selt2))
    //     //     {
    //     //         $selt2[] = 'hxf';
    //     //     }
    //     // }
    //     // if(!empty($post['brand']))
    //     // {
    //     //       $brand = M('product_brand')->where(array('name' =>$post['brand']))->getField('id', true);
    //     //     //   dump($brand);die;
    //     //       $brand_id = M('archives')->where(array('aid' => array('in',$rest),'brand_id' => array('in',$brand)))->getField('aid', true);
    //     //     //   dump($brand_id);die;
    //     //       if(empty($brand_id))
    //     //       {
    //     //           $brand_id = ['hxf'];
    //     //       }
    //     // }
    // //   if(!empty($selt) && !empty($selt1) && !empty($selt2) && !empty($brand_id))
    // //   {
    // //       $selts = array_intersect($selt,$selt1,$selt2,$brand_id);
    // //   }elseif (!empty($selt) && !empty($selt1) && !empty($selt2)) {
    // //       $selts = array_intersect($selt,$selt1,$selt2);
    // //   }elseif(!empty($selt) && !empty($selt1) && !empty($brand_id))
    // //   {
    // //       $selts = array_intersect($selt,$selt1,$brand_id); 
    // //   }elseif(!empty($selt) && !empty($selt2) && !empty($brand_id))
    // //   {
    // //         $selts = array_intersect($selt,$selt2,$brand_id); 
    // //   }elseif(!empty($selt1) && !empty($selt2) && !empty($brand_id))
    // //   {
    // //     //   dump(11)
    // //         $selts = array_intersect($selt1,$selt2,$brand_id); 
    // //   }elseif(!empty($selt) && !empty($selt2))
    // //   {
    // //       $selts = array_intersect($selt,$selt2); 
    // //   }elseif(!empty($selt) && !empty($selt1))
    // //   {
    // //       $selts = array_intersect($selt,$selt1); 
    // //   }elseif(!empty($selt) && !empty($brand_id))
    // //   {
    // //       $selts = array_intersect($selt,$brand_id); 
    // //   }elseif(!empty($selt1) && !empty($brand_id))
    // //   {
    // //       $selts = array_intersect($selt1,$brand_id); 
    // //   }elseif(!empty($selt1) && !empty($selt2))
    // //   {
    // //       $selts = array_intersect($selt2,$selt1); 
    // //   }elseif(!empty($selt2) && !empty($brand_id))
    // //   {
    // //       $selts = array_intersect($selt2,$brand_id); 
    // //   }elseif(!empty($selt))
    // //   {
    // //       $selts = $selt; 
    // //   }elseif(!empty($selt1))
    // //   {
    // //       $selts = $selt1; 
    // //   }elseif(!empty($selt2))
    // //   {
    // //       $selts = $selt2; 
    // //   }elseif(!empty($brand_id))
    // //   {
    // //       $selts = $brand_id; 
    // //   }
       
    //   if(!empty($post['attr_value']))
    //   {
    //          $arc = M('archives')
    //         ->field('a.*,b.weight,b.number,c.name as brand')
    //         ->alias('a')
    //         ->join('product_content b','a.aid=b.aid')
    //         ->join('product_brand c' , 'c.id = a.brand_id')
    //         ->where(array('a.typeid' => $post['typeid'],'a.aid' => array('in',array_unique($selts))))
    //         ->where(array('c.name' => array('in',$post['brand'])))
    //         ->page($post['page'],$post['limit'])
    //         ->order($order)
    //         ->select();
    //         dump($arc);die;
    //   }else
    //   {
    //       $arc = M('archives')
    //         ->field('a.*,b.weight,b.number,c.name as brand')
    //         ->alias('a')
    //         ->join('product_content b','a.aid=b.aid')
    //         ->join('product_brand c' , 'c.id = a.brand_id')
    //         ->where(array('a.typeid' => $post['typeid']))
    //         ->page($post['page'],$post['limit'])
    //         ->order($order)
    //         ->select();
    //   }
    //     foreach($arc as $k => $v)
    //     {
    //       $spe =  M('specifications')->where(array('product_id' => $v['aid']))->select();
    //       foreach ($spe as $kk => $vv)
    //       {
    //           $spe[$kk]['status'] = '';
    //       }
    //       $arc[$k]['specifications'] = $spe;
    //       $product_spec = M('product_spec_value')
    //       ->field('b.spec_value,a.spec_price,a.value_id,a.aid')
    //       ->alias('a')
    //       ->join('product_spec_data b','b.aid=a.aid and a.spec_value_id = b.spec_value_id', 'LEFT')
    //       ->where(array('a.aid' => $v['aid']))
    //       ->order('a.spec_price desc')
    //       ->select();
    //       $arc[$k]['product_spec'] = $product_spec;
          
    //      $attr = M('shop_product_attr')
    //       ->field('a.attr_value,b.attr_name')
    //       ->alias('a')
    //       ->join('shop_product_attribute b','b.attr_id=a.attr_id', 'LEFT')
    //       ->where(array('a.aid' => $v['aid']))
    //       ->select();
    //       foreach($attr as $kk => $vv)
    //       {
    //           if($vv['attr_name'] == '参考封装')
    //           {
    //               $arc[$k]['package1'] = $vv['attr_value'];
    //           }
    //          if($vv['attr_name'] == '封装/规格')
    //           {
    //               $arc[$k]['specifications1'] = $vv['attr_value'];
    //           }
    //           if($vv['attr_name'] == '包装/方式')
    //           {
    //               $arc[$k]['packing1'] = $vv['attr_value'];
    //           }
    //       }
    //     }
        
    //     // dump($arc);die;
    //     $datas['status'] = 1;
    //     $datas['msg'] = '';
    //     $datas['res'] = $arc;
    //     echo json_encode($datas,JSON_UNESCAPED_UNICODE);
    //     exit;
    // }
    
    //添加对比
    public function add_contrast()
    {
        $post = input('post.');
        $post['users_id'] = $_SESSION['think']['users_id'];
        
        if($post['users_id'] == '')
        {
            $datas['status'] = -2;
            $datas['msg'] = '请先登入';
            $datas['res'] = $product;
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit;  
        }
        
        $post['add_time'] = time();
        // dump($_SESSION['think']['users_id']);die;
        $pro = M('product_contrast')->where(array('product_id' => $post['product_id'],'users_id' => $_SESSION['think']['users_id']))->find();
        if($pro)
        {
            $datas['status'] = -1;
            $datas['msg'] = '该商品你已加入过';
            $datas['res'] = '';
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit;
        }else
        {
           $prod = M('product_contrast')->where(array('users_id' => $_SESSION['think']['users_id']))->count();
           if($prod > 4)
           {
                $datas['status'] = -1;
                $datas['msg'] = '您加入的产品超过上限';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;  
           }else
           {
               $pros = M('product_contrast')->add($post);
               if($pros)
               {
                    $datas['status'] = 1;
                    $datas['msg'] = '添加成功';
                    $datas['res'] = '';
                    echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                    exit; 
               }
           }

        }
    }
    
    //商品侧边对比
    public function list_contrast()
    {
        
        $post['users_id'] = $_SESSION['think']['users_id'];
        if($post['users_id'] == '')
        {
            $datas['status'] = -2;
            $datas['msg'] = '请先登入';
            $datas['res'] = $product;
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit;  
        }
        
       $product = M('product_contrast')
        ->field('b.title,b.litpic,b.aid')
        ->alias('a')
        ->join('archives b','a.product_id=b.aid')
        ->where(array('a.users_id' => $_SESSION['think']['users_id']))
        ->select();
        // dump($product);die;
        
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $product;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit; 
    }
    
    public function compared_del()
    {
         $post['users_id'] = $_SESSION['think']['users_id'];
        if($post['users_id'] == '')
        {
            $datas['status'] = -2;
            $datas['msg'] = '请先登入';
            $datas['res'] = $product;
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit;  
        }
        $param = input('post.');
        if($param['symbol'] == 'all')
        {
            $product = M('product_contrast')->where(array('users_id' => $_SESSION['think']['users_id']))->delete();
        }else
        {
             $product = M('product_contrast')->where(array('users_id' => $_SESSION['think']['users_id'],'product_id' => $param['product_id']))->delete();
        //   $count = M('product_contrast')->where(array('users_id' => $_SESSION['think']['users_id'],'product_id' => $param['product_id']))->count();
          
        }
        if($product)
        {
            $datas['status'] = 1;
            $datas['msg'] = '移除成功';
            $datas['res'] = '';
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit; 
        }
    }
    
    
    //对比详情
    public function contrast()
    {
        $post['users_id'] = $_SESSION['think']['users_id'];
        if($post['users_id'] == '')
        {
            $datas['status'] = -2;
            $datas['msg'] = '请先登入';
            $datas['res'] = $product;
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit;  
        }
        $count = M('product_contrast')->where(array('users_id' => $_SESSION['think']['users_id']))
        ->count();
        if($count > 1)
        {
             $product = M('product_contrast')
            ->field('b.title,b.litpic,a.product_id,b.stock_count')
            ->alias('a')
            ->join('archives b','a.product_id=b.aid')
            ->where(array('a.users_id' => $_SESSION['think']['users_id']))
            ->select();
            
            foreach($product as $k => $v)
            {
              $product_spec = M('product_spec_value')
              ->field('b.spec_value,a.spec_price,a.value_id,a.aid')
              ->alias('a')
              ->join('product_spec_data b','b.aid=a.aid and a.spec_value_id = b.spec_value_id', 'LEFT')
              ->where(array('a.aid' => $v['product_id']))
              ->order('a.spec_price desc')
              ->select();
              if(empty($product_spec))
              {
                  $product[$k]['product_spec'] = '-';
              }else
              {
                   $product[$k]['product_spec'] = $product_spec;
              }
             
              
              $attr = M('shop_product_attr')
              ->field('a.attr_value,b.attr_name')
              ->alias('a')
              ->join('shop_product_attribute b','b.attr_id=a.attr_id', 'LEFT')
              ->where(array('a.aid' => $v['product_id']))
              ->select();
              
            $attrs[] = M('shop_product_attr')
              ->alias('a')
              ->join('shop_product_attribute b','b.attr_id=a.attr_id', 'LEFT')
              ->where(array('a.aid' => $v['product_id']))
              ->getField('b.attr_name', true);
              
              $product[$k]['case'] = $attr;
              
            }
        }else
        {
              $datas['status'] = -1;
              $datas['msg'] = '产品数量小于等于1不能对比';
              $datas['res'] = '';
              echo json_encode($datas,JSON_UNESCAPED_UNICODE);
              exit;  
        }
        
        if($count > 2)
        {
           $ress = array_unique(array_merge($attrs[0],$attrs[1],$attrs[2]));
        }else
        {
            $ress = array_unique(array_merge($attrs[0],$attrs[1])); 
        }
        
        $a = 0;
        foreach($ress as $k => $v)
        {
            foreach($product[0]['case'] as $kk => $vv)
            {
                if(in_array($v,$vv))
                {
                    $a = 1;
                }
            }
            // dump($a);
            if($a == 0)
            {
                $product[0]['case'][$kk+1]['attr_name'] = $v;
                $product[0]['case'][$kk+1]['attr_value'] = '-';
            }
             $a = 0;
        }
        
        $b = 0;
        foreach($ress as $k => $v)
        {
            foreach($product[1]['case'] as $kk => $vv)
            {
                if(in_array($v,$vv))
                {
                    $b = 1;
                }
                // $b = 0;
            }
            // dump($b);
            if($b == 0)
            {
                $product[1]['case'][$kk+1]['attr_name'] = $v;
                $product[1]['case'][$kk+1]['attr_value'] = '-';
            }
            $b = 0;
        }
        
        if(!empty($product[2]['case']))
        {
             $c = 0;
            foreach($ress as $k => $v)
            {
                foreach($product[2]['case'] as $kk => $vv)
                {
                    if(in_array($v,$vv))
                    {
                        $c = 1;
                    }
                }
                if($c == 0)
                {
                    $product[2]['case'][$kk+1]['attr_name'] = $v;
                    $product[2]['case'][$kk+1]['attr_value'] = '-';
                }
                $c=0;
            }
        }
 
        
        if(!empty($product[2]['case']))
        {
            $sort = array_column($product[2]['case'], 'attr_name');      
    
            array_multisort($sort, SORT_DESC,  $product[2]['case']);  
        }
        
         $sort = array_column($product[1]['case'], 'attr_name');      

        array_multisort($sort, SORT_DESC,  $product[1]['case']); 
        
         $sort = array_column($product[0]['case'], 'attr_name');      

        array_multisort($sort, SORT_DESC,  $product[0]['case']); 
        
        
        // dump($product);die;
        // dump($attrs);
        // dump($ress);die;
        
        
        
        // dump($product);die;
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $product;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit; 
    }
    
    
    
    //添加收藏
    public function add_collection()
    {
       $post['users_id'] = $_SESSION['think']['users_id'];
        if($post['users_id'] == '')
        {
            $datas['status'] = -2;
            $datas['msg'] = '请先登入';
            $datas['res'] = $product;
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit;  
        }
        $post = input('post.');
        $post['users_id'] = $_SESSION['think']['users_id'];
        $post['add_time'] = time();
        $pro = M('product_collection')->where(array('product_id' => $post['product_id'],'users_id' => $_SESSION['think']['users_id']))->find();
        if($pro)
        {
            $pre = M('product_collection')->where(array('product_id' => $post['product_id'],'users_id' => $_SESSION['think']['users_id']))->delete();
            if($pre)
            {
                $datas['status'] = 1;
                $datas['msg'] = '取消收藏';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;     
            }
        }else
        {
            $pros = M('product_collection')->add($post);
            if($pros)
            {
                $datas['status'] = 1;
                $datas['msg'] = '收藏成功';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit; 
            } 
        }
    }
    
    //收藏列表
    public function list_collection()
    {
       $post['users_id'] = $_SESSION['think']['users_id'];
        if($post['users_id'] == '')
        {
            $datas['status'] = -2;
            $datas['msg'] = '请先登入';
            $datas['res'] = $product;
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit;  
        }
       $post = input('post.');
       
       
       $count = M('product_collection')
        ->field('b.title,b.litpic,b.aid,b.users_price,c.name,d.number')
        ->alias('a')
        ->join('archives b','a.product_id=b.aid')
        ->join('product_brand c','b.brand_id=c.id')
        ->join('product_content d','d.aid=b.aid')
        ->where(array('a.users_id' => $_SESSION['think']['users_id']))
        ->page($post['page'],$post['limit'])
        ->count();
        
       $datas['total_page'] = ceil($count/$post['limit']);
       $datas['count'] = $count;
        
       
       $product = M('product_collection')
        ->field('b.title,b.litpic,b.aid,b.users_price,c.name,d.number')
        ->alias('a')
        ->join('archives b','a.product_id=b.aid')
        ->join('product_brand c','b.brand_id=c.id')
        ->join('product_content d','d.aid=b.aid')
        ->where(array('a.users_id' => $_SESSION['think']['users_id']))
        ->order('a.add_time asc')
        ->page($post['page'],$post['limit'])
        ->select();
        // dump($product);die;
        foreach($product as $k => $v)
        {
            
           $collection = M('product_collection')->where(array('product_id' => $v['product_id'],'users_id' => $_SESSION['think']['users_id']))->find();
           if(empty($collection))
           {
                $product[$k]['status'] = 1;
           }
            
          $attr = M('shop_product_attr')
          ->field('a.attr_value,b.attr_name')
          ->alias('a')
          ->join('shop_product_attribute b','b.attr_id=a.attr_id', 'LEFT')
          ->where(array('a.aid' => $v['aid']))
          ->select();
          foreach($attr as $kk => $vv)
          {
              if($vv['attr_name'] == '参考封装')
              {
                  $product[$k]['package1'] = $vv['attr_value'];
              }
             if($vv['attr_name'] == '封装/规格')
              {
                  $product[$k]['specifications1'] = $vv['attr_value'];
              }
               if($vv['attr_name'] == '包装/方式')
              {
                  $product[$k]['packing1'] = $vv['attr_value'];
              }
          }
        }
        // dump($product);die;
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $product;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit;  
        // dump($product);die;
    }
    
    //用户/游客询价
    public function inquiry()
    {
       if (IS_POST)
       {
           $post = input('post.');
        //   dump($post);die;
           $post['users_id'] = $_SESSION['think']['users_id'];
           $post['add_time'] = time();
           $ip = clientIP();
           	if($this->limitoftimes($ip,1,60)===false)
            {
                $data['code'] = -1;
                $data['smg'] = '一分钟内以提交过';
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                exit();
            }
           $inquiry = M('product_inquiry')->add($post);
           if($inquiry)
           {
                $datas['status'] = 1;
                $datas['msg'] = '询价提交成功，请耐心等待';
                $datas['res'] = '';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;  
           }
        }
         $get = input('get.');
        //  dump($get);die;
        $product = M('archives')
           ->field('a.title,b.name as brand_name,a.aid')
           ->alias('a')
           ->join('product_brand b','b.id=a.brand_id')
           ->where(array('a.aid' => $get['product_id']))
           ->find();
        //   dump($product);die;
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $product;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit;  
    }
    
    //产品详情
    public function product_view()
    {
       $post = input('post.');
      $product = M('archives')
       ->field('a.users_price,a.sales_num,a.stock_count,b.name as brand_name ,b.litpic as brand_litpc,d.number,d.weight,a.aid,d.content,a.title as product_title,d.manual,d.manual2,a.typeid,d.product_subtitle,d.packing,d.specificationss,d.encapsulations')
       ->alias('a')
       ->join('product_brand b','b.id=a.brand_id')
       ->join('product_content d','d.aid=a.aid')
       ->where(array('a.aid' => $post['product_id']))
       ->find();
       
        $attr = M('shop_product_attr')
          ->field('a.attr_value,b.attr_name')
          ->alias('a')
          ->join('shop_product_attribute b','b.attr_id=a.attr_id', 'LEFT')
          ->where(array('a.aid' => $product['aid']))
          ->select();
        //   dump
          foreach($attr as $kk => $vv)
          {
              if($vv['attr_name'] == '参考封装')
              {
                  $product['package1'] = $vv['attr_value'];
              }
             if($vv['attr_name'] == '封装/规格')
              {
                  $product['specifications1'] = $vv['attr_value'];
              }
               if($vv['attr_name'] == '包装/方式')
              {
                  $product['packing1'] = $vv['attr_value'];
              }
          }
          
        $product_spec = M('product_spec_value')
          ->field('b.spec_value,a.spec_price,a.value_id,a.aid')
          ->alias('a')
          ->join('product_spec_data b','b.aid=a.aid and a.spec_value_id = b.spec_value_id', 'LEFT')
          ->where(array('a.aid' => $product['aid']))
         ->orderRaw("LENGTH(b.spec_value) asc,b.spec_value asc")
          ->select();
          $product['product_spec'] = $product_spec;
          
        $attr = M('shop_product_attr')
              ->field('a.attr_value,b.attr_name')
              ->alias('a')
              ->join('shop_product_attribute b','b.attr_id=a.attr_id', 'LEFT')
              ->where(array('a.aid' => $product['aid']))
              ->order('sort_order asc,a.attr_id asc')
              ->select();
            //   dump($attr);die;
              $product['case'] = $attr;
          $spe =  M('specifications')->where(array('product_id' => $product['aid']))->select();
          foreach ($spe as $kk => $vv)
          {
              $spe[$kk]['status'] = '';
          }
          $product['specifications'] = $spe;
          $res = M('product_img')
               ->field('a.image_url,a.intro')
               ->alias('a')
               ->where(array('aid' => $product['aid']))
               ->select();
           $product['image'] = $res;
           
          $shop = M('shop_order_details')
           ->alias('a')
           ->join('shop_order b','b.order_id=a.order_id')
           ->where('b.order_status = 1 or b.order_status = 3 or b.order_status = 2')
           ->where(array('a.product_id' => $post['product_id']))
           ->count();
           
         $shope = M('shop_order_details')
           ->alias('a')
           ->join('shop_order b','b.order_id=a.order_id')
           ->whereTime('a.add_time', 'month')
           ->where('b.order_status = 1 or b.order_status = 3 or b.order_status = 2')
           ->where(array('a.product_id' => $post['product_id']))
           ->count();
        //   dump($shop);die;
        
         $collection = M('product_collection')->where(array('users_id' =>  $_SESSION['think']['users_id'],'product_id' => $post['product_id']))->find();
           if(empty($collection))
           {
              $product['collection'] = 0;
           }else
           {
                $product['collection'] = 1;
           }
           
           $product['content'] = htmlspecialchars_decode($product['content']);
           
        //   $product['manual_title'] = cut_str($product['manual'],'/',-1);//输出 abc
           
           if(empty($product['manual']))
           {
               $product['manual_title'] = ''; 
           }else
           {
                $str = explode("/",$product['manual']);
                $product['manual_title'] = $str[(count($str)-1)];
           }
           
            if(empty($product['manual2']))
           {
               $product['manual1_title'] = ''; 
           }else
           {
                $str = explode("/",$product['manual2']);
                $product['manual1_title'] = $str[(count($str)-1)];
           }
      
           
           
           //商品评论
           
    //       $comment M('shop_order_comment')->where(array('product_id' => $post['product_id']))->select();
         $product['sales_num'] = $shop;//总单
         $product['near_num'] = $shope;//近期
    //   dump($shope);die;
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $product;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit;  
    }
    
    //产品优惠劵
    public function product_coupon()
    {
        $param = input('post.');
        $product_id = $param['product_id'];
        //只限产品使用的优惠劵
        $coupon = M('shop_coupon')->where(array('status' => 1,'coupon_type' => 2))->where(Db::raw("FIND_IN_SET($product_id,product_id)"))->where('end_date > '.time())->select();
        //全站通用的优惠劵
        $coupon1 = M('shop_coupon')->where(array('status' => 1,'coupon_type' => 1))->where('end_date > '.time())->select();
        //该系列通用优惠劵
    
        $arctype = M('arctype')->where(array('id' => $param['typeid']))->find();
        // $arctypes = M('arctype')->where(array('parent_id' => $arctype['parent_id']))->find();
        // dump($arctype);die;
        $coupon2 = M('shop_coupon')->where(array('status' => 1,'coupon_type' => 3,'arctype_id' => $arctype['topid']))->where('end_date > '.time())->select();
        
        if(empty($coupon))
        {
            $coupon = [];
        }
         if(empty($coupon1))
        {
            $coupon1 = [];
        }
         if(empty($coupon2))
        {
            $coupon2 = [];
        }
        $coupons = array_merge($coupon,$coupon1,$coupon2);
        
        // dump($coupons);die;
        
        foreach ($coupons as $k => $v)
        {
          $coupone = M('shop_coupon_use')->where(array('coupon_id' => $v['coupon_id'],'users_id' => $_SESSION['think']['users_id']))->find();
        //   dump($coupone);
           if(empty($coupone))
           {
               $coupons[$k]['receive'] = 0;
           }else
           {
               $coupons[$k]['receive'] = 1;
           }
        }
        
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $coupons;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit;  
    }
    
    
    //产品详情侧边推荐
    public function like_product()
    {
        $post = input('post.');
       $product = M('archives')
        ->where(array('aid' => $post['product_id']))
        ->find();
        
       $like = explode(',',$product['like_product']);
       $like_product_id = array_filter($like);
       $product = M('archives')
       ->field('a.title,a.litpic,a.users_price,b.name as brand_name,a.aid')
       ->alias('a')
       ->join('product_brand b','b.id=a.brand_id')
       ->where(array('a.aid' => array('in',$like_product_id)))
       ->select();
       
        foreach($product as $k => $v)
        {
          $attr = M('shop_product_attr')
          ->field('a.attr_value,b.attr_name')
          ->alias('a')
          ->join('shop_product_attribute b','b.attr_id=a.attr_id', 'LEFT')
          ->where(array('a.aid' => $v['aid']))
          ->select();
          foreach($attr as $kk => $vv)
          {
              if($vv['attr_name'] == '参考封装')
              {
                  $product[$k]['package1'] = $vv['attr_value'];
              }
             if($vv['attr_name'] == '封装/规格')
              {
                  $product[$k]['specifications1'] = $vv['attr_value'];
              }
          }
        }
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $product;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit;
    }
    
    //商品评论
    public function comment()
    {
        $post = input('post.');
        
        $count = M('shop_order_comment')
        ->field('a.*, b.num as product_num,c.username,c.head_pic')
        ->alias('a')
        ->join('__SHOP_ORDER_DETAILS__ b', 'a.details_id = b.details_id', 'LEFT')
        ->join('users c','c.users_id=b.users_id')
        ->where(array('a.product_id' => $post['product_id'],'a.is_show' => 1))
        ->count();
        $order_comment['count'] = $count;
        $order_comment['total_page'] = ceil($count/$post['limit']);
        $order_comment = M('shop_order_comment')
        ->field('a.*, b.num as product_num,c.username,c.head_pic')
        ->alias('a')
        ->join('__SHOP_ORDER_DETAILS__ b', 'a.details_id = b.details_id', 'LEFT')
        ->join('users c','c.users_id=b.users_id')
        ->where(array('a.product_id' => $post['product_id'],'a.is_show' => 1))
        ->page($post['page'],$post['limit'])
        ->select();
        $wordfilter = M('weapp_wordfilter')->where(array('status' => 1))->select();
        // dump($wordfilter);die;
        foreach ($order_comment as $k => $v)
        {
            foreach ($wordfilter as $kk => $vv)
            {
                $wordfilters[$kk] = $vv['title'];
            }
            $order_comment[$k]['content'] = $this->sensitive($wordfilters, $v['content']);
            $order_comment[$k]['upload_img'] = !empty($v['upload_img']) ? explode(',', unserialize($v['upload_img'])) : '';
                  // 商品评价评分
            $order_comment[$k]['order_total_score'] = Config::get('global.order_total_score')[$v['total_score']];

            // 评价转换星级评分
            $order_comment[$k]['total_score'] = GetScoreArray($v['total_score']);

            // 评价的内容
            // $order_comment[$k]['content'] = !empty($v['content']) ? unserialize($v['content']) : '';
        }
        
        // dump($order_comment);die;
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $order_comment;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit;
    }
    
    function sensitive($list, $string){
      $count = 0; //违规词的个数
      $sensitiveWord = '';  //违规词
      $stringAfter = $string;  //替换后的内容
      $pattern = "/".implode("|",$list)."/i"; //定义正则表达式
      if(preg_match_all($pattern, $string, $matches)){ //匹配到了结果
        $patternList = $matches[0];  //匹配到的数组
        $count = count($patternList);
        $sensitiveWord = implode(',', $patternList); //敏感词数组转字符串
        $replaceArray = array_combine($patternList,array_fill(0,count($patternList),'*')); //把匹配到的数组进行合并，替换使用
        $stringAfter = strtr($string, $replaceArray); //结果替换
      }
    //   dump($stringAfter);
      return $stringAfter;
    }
    
    public function search()
    {
        $post = input('post.');
        
       $key = explode(' ',$post['key']);
        
        $where['title'] = array('or');
        foreach ($key as $value) {
            
           array_unshift($where['title'], array('like', '%'.$value.'%'));
        }
        
       if(!empty($post['sales_num']))
        {
            $order .= 'a.sales_num '.$post['sales_num'];
        }
        if(empty($post['sales_num']))
        {
            if(!empty($post['users_price']))
            {
                $order .= 'a.users_price '.$post['users_price'];
            }
        }else
        {
            if(!empty($post['users_price']))
            {
                $order .= ' and a.users_price '.$post['users_price'];
            } 
        }
        
        $res = M('archives')->where($where)->where(array('channel' => 2))->getField('aid', true);
        // dump($res);die;
        
           $count = M('archives')
            ->field('a.*,b.weight,b.number,c.name as brand')
            ->alias('a')
            ->join('product_content b','a.aid=b.aid')
            ->join('product_brand c' , 'c.id = a.brand_id')
            ->where(array('a.aid' => array('in',$res)))
            ->page($post['page'],$post['limit'])
            ->order($order)
            ->count();
        $datas['count'] = $count;
         $datas['total_page'] = ceil($count/$post['limit']);
        $arc = M('archives')
            ->field('a.*,b.weight,b.number,c.name as brand')
            ->alias('a')
            ->join('product_content b','a.aid=b.aid')
            ->join('product_brand c' , 'c.id = a.brand_id')
            ->where(array('a.aid' => array('in',$res)))
            ->page($post['page'],$post['limit'])
            ->order($order)
            ->select();
            
        foreach($arc as $k => $v)
        {
            
           $arc[$k]['specifications_id'] = '';
           $arc[$k]['product_num'] = '1';
           $arc[$k]['product_type'] = '个'; 
           $collection = M('product_collection')->where(array('users_id' =>  $_SESSION['think']['users_id'],'product_id' => $v['aid']))->find();
           
           if(empty($collection))
           {
               $arc[$k]['collection'] = 0;
           }else
           {
                $arc[$k]['collection'] = 1;
           }
            
          $spe =  M('specifications')->where(array('product_id' => $v['aid']))->select();
          foreach ($spe as $kk => $vv)
          {
              $spe[$kk]['status'] = '';
          }
          $arc[$k]['specifications'] = $spe;
          $product_spec = M('product_spec_value')
          ->field('b.spec_value,a.spec_price,a.value_id,a.aid')
          ->alias('a')
          ->join('product_spec_data b','b.aid=a.aid and a.spec_value_id = b.spec_value_id', 'LEFT')
          ->where(array('a.aid' => $v['aid']))
          ->orderRaw("LENGTH(b.spec_value) asc,b.spec_value asc")
          ->select();
          
        //   $last_names = array_column($product_spec,'spec_value');

        //     array_multisort($last_names,SORT_ASC,$product_spec);
          
          $arc[$k]['product_spec'] = $product_spec;
          
         $attr = M('shop_product_attr')
          ->field('a.attr_value,b.attr_name')
          ->alias('a')
          ->join('shop_product_attribute b','b.attr_id=a.attr_id', 'LEFT')
          ->where(array('a.aid' => $v['aid']))
          ->select();
          foreach($attr as $kk => $vv)
          {
              if($vv['attr_name'] == '参考封装')
              {
                  $arc[$k]['package1'] = $vv['attr_value'];
              }
             if($vv['attr_name'] == '封装/规格')
              {
                  $arc[$k]['specifications1'] = $vv['attr_value'];
              }
               if($vv['attr_name'] == '包装/方式')
              {
                  $arc[$k]['packing1'] = $vv['attr_value'];
              }
          }
          
          $shop = M('shop_order_details')
           ->alias('a')
           ->join('shop_order b','b.order_id=a.order_id')
           ->where('b.order_status = 1 or b.order_status = 3 or b.order_status = 2')
           ->where(array('a.product_id' => $v['aid']))
           ->count();
           
         $shope = M('shop_order_details')
           ->alias('a')
           ->join('shop_order b','b.order_id=a.order_id')
           ->whereTime('a.add_time', 'month')
           ->where('b.order_status = 1 or b.order_status = 3 or b.order_status = 2')
           ->where(array('a.product_id' => $v['aid']))
           ->count();
         $arc[$k]['sales_num'] = $shop;//总单
         $arc[$k]['near_num'] = $shope;//近期
        }
        
        $sort = array(
        'direction' => 'SORT_DESC', //排序顺序标志 SORT_DESC 降序；SORT_ASC 升序
        'field'     => 'sales_num',       //排序字段
        );
        
        $arrSort = array();
        foreach($arc AS $uniqid => $row){
            foreach($row AS $key=>$value){
                $arrSort[$key][$uniqid] = $value;
            }
        }
        if($sort['direction']){
            array_multisort($arrSort[$sort['field']], constant($sort['direction']), $arc);
        }

        
        
        // dump($arc);die;
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $arc;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit;
    }
    
    //首页最新产品
    public function new_product()
    {
       $rest = M('archives')
        ->field('a.title,a.litpic,a.users_price,c.name as brand_name,a.aid')
        ->alias('a')
        ->join('product_brand c' , 'c.id = a.brand_id')
        ->where(array('a.is_del' => 0,'a.channel' => 2,'is_recom' => 1))
        ->order('a.add_time desc,a.sort_order desc')
        ->limit(0,8)
        ->select();
        
        
        
        foreach($rest as $k => $v)
        {
          $spe =  M('specifications')->where(array('product_id' => $v['aid']))->select();
          foreach ($spe as $kk => $vv)
          {
              $spe[$kk]['status'] = '';
          }
          $rest[$k]['specifications'] = $spe;
          $product_spec = M('product_spec_value')
          ->field('b.spec_value,a.spec_price,a.value_id,a.aid')
          ->alias('a')
          ->join('product_spec_data b','b.aid=a.aid and a.spec_value_id = b.spec_value_id', 'LEFT')
          ->where(array('a.aid' => $v['aid']))
          ->order('a.spec_price desc')
          ->limit(0,8)
          ->select();
          $rest[$k]['product_spec'] = $product_spec;
        }
        
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $rest;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit;
    }
    
    //首页热销产品
    
    public function sale_prodct()
    {
        
        // dump(11);die;
        $rest = M('archives')
        ->field('a.title,a.litpic,a.users_price,c.name as brand_name,a.aid')
        ->alias('a')
        ->join('product_brand c' , 'c.id = a.brand_id')
        ->where(array('a.is_del' => 0,'a.channel' => 2,'is_b' => 1))
        ->order('a.sort_order desc')
        ->limit(0,8)
        ->select();
        
        foreach($rest as $k => $v)
        {
          $spe =  M('specifications')->where(array('product_id' => $v['aid']))->select();
           foreach ($spe as $kk => $vv)
          {
              $spe[$kk]['status'] = '';
          }
          $rest[$k]['specifications'] = $spe;
          $product_spec = M('product_spec_value')
          ->field('b.spec_value,a.spec_price,a.value_id,a.aid')
          ->alias('a')
          ->join('product_spec_data b','b.aid=a.aid and a.spec_value_id = b.spec_value_id', 'LEFT')
          ->where(array('a.aid' => $v['aid']))
          ->order('a.spec_price desc')
          ->limit(0,8)
          ->select();
          $rest[$k]['product_spec'] = $product_spec;
        }
        
        // dump($rest);die;
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $rest;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit;
    }
    
    //首页轮播图
    public function rotation_chart()
    {
        
       $url=isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:'';
       $ad = M('ad')->where(array('pid' => 7))->select();
       $datas['status'] = 1;
       $datas['msg'] = '';
        $datas['res'] = $ad;
        // dump($datas);die;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit;
    }
    
    //代理品牌
    public function product_brand()
    {
        $brand =  M('product_brand')
        ->where(array('is_tui' => 1))
        ->order('sort_order desc')
        ->limit(0,10)
        ->select();
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $brand;
        // dump($brand);die;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit;
    }
    
    public function details()
    {
        $post = input('param.');
        $details = M('archives')
        ->field('b.content,a.*')
        ->alias('a')
        ->join('article_content b','a.aid=b.aid')
        ->where(array('a.aid' => $post['aid']))
        ->find();
        $details['content'] = htmlspecialchars_decode($details['content']);
        echo json_encode($details,JSON_UNESCAPED_UNICODE);
        exit;

        
    }
    
    //商城首页自定义搜索热词
    public function hot_words ()
    {
        $art = M('archives')->where(array('typeid' => 105,'arcrank' => 0))->order('sort_order desc')->select();
        $datas['status'] = 1;
        $datas['msg'] = '';
        $datas['res'] = $art;
        echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        exit;
    }
    
    //首页我的足迹
    public function users_footprint()
    {
       $post['users_id'] = $_SESSION['think']['users_id'];
        if($post['users_id'] == '')
        {
            $datas['status'] = -2;
            $datas['msg'] = '请先登入';
            $datas['res'] = $product;
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit;  
        }
        
    $start=strtotime(date('Y-m-01 00:00:00'));
    $end = strtotime(date('Y-m-d H:i:s'));
    // $data['cid']=$cid;
    $where['add_time'] = array('between',array($start,$end));
    $footprint = M('users_footprint')->where($where)->where(array('users_id' => $post['users_id'],'channel' => 2))->order('id desc')->getField('aid', true);
    // dump($footprint);die;
    $aid = array_unique($footprint);
    // dump($aid);die;
    $aid = array_slice($aid,0,8);
    
    foreach ($aid as $k => $v)
    {
          $rest[] = M('archives')
        ->field('a.title,a.litpic,a.users_price,c.name as brand_name,a.aid')
        ->alias('a')
        ->join('product_brand c' , 'c.id = a.brand_id')
        ->where(array('a.is_del' => 0,'a.channel' => 2,'aid' => $v))
        ->order('a.sales_num desc')
        ->find();
          
    }
    // $rest = M('archives')
    //     ->field('a.title,a.litpic,a.users_price,c.name as brand_name,a.aid')
    //     ->alias('a')
    //     ->join('product_brand c' , 'c.id = a.brand_id')
    //     ->where(array('a.is_del' => 0,'a.channel' => 2,'aid' => array('in',$aid)))
    //     ->order('a.sales_num desc')
    //     ->select();
        
        // dump($rest);die;
        
    foreach($rest as $k => $v)
        {
          $spe =  M('specifications')->where(array('product_id' => $v['aid']))->select();
           foreach ($spe as $kk => $vv)
          {
              $spe[$kk]['status'] = '';
          }
          $rest[$k]['specifications'] = $spe;
          $product_spec = M('product_spec_value')
          ->field('b.spec_value,a.spec_price,a.value_id,a.aid')
          ->alias('a')
          ->join('product_spec_data b','b.aid=a.aid and a.spec_value_id = b.spec_value_id', 'LEFT')
          ->where(array('a.aid' => $v['aid']))
          ->order('a.spec_price desc')
          ->limit(0,8)
          ->select();
          $rest[$k]['product_spec'] = $product_spec;
        }
    // dump($rest);die;

    // dump($footprint);die;
    $datas['status'] = 1;
    $datas['msg'] = '';
    $datas['res'] = $rest;
    echo json_encode($datas,JSON_UNESCAPED_UNICODE);
    exit;
    }
    
    //判断是否登入
    
    public function users_login()
    {
        if(!empty($_SESSION['think']['users_id']))
        {
            $res['users_id'] = $_SESSION['think']['users']['users_id'];
            $res['username'] = $_SESSION['think']['users']['username'];
            $res['head_pic'] = $_SESSION['think']['users']['head_pic'];
            $datas['status'] = 1;
            $datas['msg'] = '已登入';
            $datas['res'] = $res;
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit;
        }else
        {
            $datas['status'] = -2;
            $datas['msg'] = '您还未登入，请登入';
            $datas['res'] = '';
            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            exit;  
        }
        
    }
    
    public function service()
    {
       $data['service'] = M('arctype')->where(array('parent_id' => 30))->order('id asc')->limit(2,4)->select();
       $data['news'] = M('archives')->where(array('typeid' => 107,'is_recom' => 1))->select();
       $data['notice'] =  M('archives')->where(array('typeid' => 108))->select();
       $datas['status'] = 1;
       $datas['msg'] = '成功';
       $datas['res'] = $data;
    //   dump($data);die;
       echo json_encode($datas,JSON_UNESCAPED_UNICODE);
       exit;
    }
    
    
    public function sevel()
    {
         $post = input('post.');
         $data['service'] = M('arctype')->where(array('id' => $post['id']))->find();
         $datas['status'] = 1;
         $datas['msg'] = '成功';
         $datas['res'] = $data['service'];
        //   dump($data);die;
         echo json_encode($datas,JSON_UNESCAPED_UNICODE);
         exit;
         
    }
    
    
    public function footer()
    {
        $data['web_copyright'] = config()['tpcache']['web_copyright'];
        $data['web_recordnum'] =  config()['tpcache']['web_recordnum'];
        $data['email'] = config()['tpcache']['web_attr_15'];
        $data['weixin'] = config()['tpcache']['web_attr_17'];
        $data['login'] = config()['tpcache']['web_logo'];
        $data['tel'] = config()['tpcache']['web_attr_1'];
        $data['footer_login'] = config()['tpcache']['web_attr_13'];
        $datas['status'] = 1;
        $datas['msg'] = '成功';
        $datas['res'] = $data;
        //   dump($data);die;
         echo json_encode($datas,JSON_UNESCAPED_UNICODE);
         exit;
    }
    
    public function head()
    {
      $head =  M('users_menu')->where(array('version' => 'v2','status' => 1))->select();
      foreach ($head as $k => $v)
      {
          $data[$k]['title'] = $v['title'];
          $data[$k]['mca'] = $v['mca'];
      }
      $datas['status'] = 1;
      $datas['msg'] = '成功';
      $datas['res'] = $data;
        //   dump($data);die;
      echo json_encode($datas,JSON_UNESCAPED_UNICODE);
      exit;
    }
    
    
    public function contracts()
    {
        
        //  dump($_SESSION['think']['users']['nickname']);DIE;
      $sign =  M('archives')->where(array('typeid' => array('in','117,118,119')))->select();
      $singles = M('single_content')->where(array('typeid' => array('in','117,118,119')))->select();
      
      foreach($singles as $k => $v)
      {
          if($v['typeid'] == 117)
          {
              $lists['shopping'] = $v;
          }
           if($v['typeid'] == 118)
          {
              $lists['forum'] = $v;
          }
         if($v['typeid'] == 119)
          {
              $lists['Privacy'] = $v;
          }
      }
      
     foreach($sign as $k => $v)
     {
           if($v['typeid'] == 117)
          {
              $lists['shopping']['title'] = $v['title'];
          }
           if($v['typeid'] == 118)
          {
              $lists['forum']['title'] = $v['title'];
          }
         if($v['typeid'] == 119)
          {
              $lists['Privacy']['title'] = $v['title'];
          }
     }
     
     foreach ($lists as $k => $v)
     {
         $lists[$k]['content'] =htmlspecialchars_decode($v['content']);
     }
      
    //   dump($lists);die;
    //   $singles['typename'] = $sign['title'];
    //   $singles['content'] = htmlspecialchars_decode($singles['content']);
    //   dump($singles);die;
       $datas['status'] = 1;
       $datas['msg'] = '成功';
       $datas['res'] = $lists;
       echo json_encode($datas,JSON_UNESCAPED_UNICODE);
       exit;
    //   dump($single);die;
    }
    
    //论坛文章侧板广告
    
    public function advertisement()
    {
       $ad = M('ad')
        ->field('a.litpic,a.links,a.title,a.pid')
        ->alias('a')
        ->where(array('a.pid' => array('in','8,9')))
        ->select();
        
        foreach ($ad as $k => $v)
        {
            if($v['pid'] == 8)
            {
                $list['wenzhang'][$k] = $v;
            }
            if($v['pid'] == 9)
            {
                $list['luntan'][$k] = $v;
            }
        }
        
        // dump($list);die;
       $datas['status'] = 1;
       $datas['msg'] = '成功';
       $datas['res'] = $list;
       echo json_encode($datas,JSON_UNESCAPED_UNICODE);
       exit;
    }
    
    public function head_banner()
    {
        // dump(11);die;
       $ad = M('ad')
        ->field('a.litpic,a.links,a.title,a.pid')
        ->alias('a')
        ->where(array('a.pid' => 10))
        ->select();
        // dump($ad)
      $datas['status'] = 1;
       $datas['msg'] = '成功';
       $datas['res'] = $ad;
       echo json_encode($datas,JSON_UNESCAPED_UNICODE);
       exit;
    }
    
    public function luntan_banner()
    {
        // dump(11);die;
       $ad = M('ad')
        ->field('a.litpic,a.links,a.title,a.pid')
        ->alias('a')
        ->where(array('a.pid' => 11))
        ->select();
        // dump($ad)
      $datas['status'] = 1;
       $datas['msg'] = '成功';
       $datas['res'] = $ad;
       echo json_encode($datas,JSON_UNESCAPED_UNICODE);
       exit;
    }
    
    
    public function limitoftimes($unique_id,$numberoftimes=10,$timespant="oneday",$verification=false){
        // dump($numberoftimes);die;
		$cacheneme = request()->url().$numberoftimes.'-'.$unique_id;
// 		dump($unique_id);die;
		$nowtimes = cache($cacheneme);//获取缓存中的当前次数
// 		dump( cache());die;
		if($nowtimes>=$numberoftimes)return false;
		if($verification)return true;
		if($timespant==="oneday"){
			$expires_in = strtotime(date('Y-m-d',strtotime('+1 day'))) - time();
		}else{
			$expires_inname = $cacheneme."-".$timespant;//设置时间缓存名称
			$expires_intime = cache($expires_inname);
			if($expires_intime){
				$expires_in = $timespant-(request()->time() - $expires_intime);
			}else{
				$expires_in = $timespant;
				cache($expires_inname,request()->time(),$expires_in);
			}
		}
// 		dump($nowtimes);die;
		$nowtimes = $nowtimes+1;//不可用++ 因为有false的情况
		cache($cacheneme,$nowtimes,$expires_in);
		
// 		dump($nowtimes);die;
		return $numberoftimes-$nowtimes;
	}

    
    
    
    
}