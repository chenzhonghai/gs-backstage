<?php
/**
 * 易优CMS
 * ============================================================================
 * 版权所有 2016-2028 海南赞赞网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.eyoucms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小虎哥 <1105415366@qq.com>
 * Date: 2018-4-3
 */

namespace app\plugins\controller;

use think\Db;

class Zan extends Base
{
	public $code = 'zan';
	public $weappInfo = [];
	public function _initialize() {
		parent::_initialize();
		$this->weapp_zan_db = Db::name('weapp_zan');
		$this->Code = 'zan/';
		$this->users_id = session('users_id');
		$this->aid = session('aid');
		// 插件信息
        $this->weappInfo = Db::name('weapp')->field('id')->where(['code'=>'zan','status'=>1])->find();
	}
	// 点赞
	
	public function add()
    {
		if (empty($this->weappInfo)) {
            $data = [
                'isStatus' => 0,
            ];
              $datas['status'] = -1;
              $datas['msg'] = '点赞功能已禁用，请在后台插件中心开启！';
              echo json_encode($datas);
              exit;
            // $this->error('点赞功能已禁用，请在后台插件中心开启！', null, $data);
        }
		if (IS_POST) {
		
	    $param = input('param.');
		if (empty($this->users_id)) {
			
			$this->users_id = 0;
		} else {
			$this->users_id = session('users_id');
		}
		
		if ($this->users_id == 0) {
		      $datas['status'] = -1;
              $datas['msg'] = '请先登入！';
              echo json_encode($datas,JSON_UNESCAPED_UNICODE);
              exit;
		  //  $this->error('请先登入！');
		}
		$this->users_ip = clientIP();
	    $aid = input('param.aid/d');
		$this->aid = session('aid');
		$Where = [
                'aid'    => $aid,
                'users_id' => $this->users_id,
                'zan_count' => 1,
            ];
		$Wheres = [
                'aid'    => $aid,
                'users_id' => $this->users_id,
                'zan_count' => 0,
            ];
		if (empty($param['aid'])) $this->error('请在文章加入代码！');
		$IsCount = $this->weapp_zan_db->where($Where)->count();
		 if(!empty($param['zan_count']))
        {
    		$IsCounts = $this->weapp_zan_db->where($Wheres)->count();
    		if (!empty($IsCounts)){
    		     $r = $this->weapp_zan_db->where($Wheres)->delete();
                 if($r)
                 {
                    $datas['status'] = 1;
                    $datas['msg'] = '取消成功';
                    echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                    exit;  
                 }
    		}
        }
		if (!empty($IsCount)){
		    
             $r = $this->weapp_zan_db->where($Where)->delete();
             if($r)
             {
                $datas['status'] = 1;
                $datas['msg'] = '取消成功';
                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                exit;  
             }
		    }else
		    {
		        
		        if(empty($param['zan_count']))
		        {
            	    $AddData = [
                        'zan_count'  => 1,
                        'users_ip'    => clientIP(),
    					'users_id'    => $this->users_id,
                        'add_time'    => getTime(),
                        'update_time' => getTime(),
                    ];
		        }else
		        {
	           	    $AddData = [
                        'zan_count'  => 0,
                        'users_ip'    => clientIP(),
    					'users_id'    => $this->users_id,
                        'add_time'    => getTime(),
                        'update_time' => getTime(),
                    ]; 
		        }
		        //dump($AddData);die;
		
			    $AddData = array_merge($Where, $AddData);
			    $row = $this->weapp_zan_db->where($Where)->find();
			    if(empty($row))
			    {
			     //  dump(11);die;
        		    $ResultId = $this->weapp_zan_db->add($AddData);
        		  //  dump($ResultId);die;
        	        if (!empty($ResultId)) {
        				unset($Where['users_ip']);
        				if(!empty($param['zan_count']))
        				{
                            $LikeCount = $this->weapp_zan_db->where($Where)->count();
            				$data = [
                                // 点赞数
                                'LikeCount' => $LikeCount,
                            ];
            				$UpdataNew = [
                                'zan_count'  => $LikeCount,
                                'update_time' => getTime(),
                            ];
                            
            			    $this->weapp_zan_db->where($Where)->update($UpdataNew);
        				}
        			     
        	         if(empty($param['zan_count']))
        	        {
        		      $datas['status'] = 1;
                      $datas['msg'] = '会员点赞成功！';
                      $datas['res'] = $data;
                      echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                      exit;
        	        }else
        	        {
        	              $datas['status'] = 1;
                          $datas['msg'] = '会员👎成功！';
                          $datas['res'] = $data;
                          echo json_encode($datas,JSON_UNESCAPED_UNICODE);
                          exit;
        	        }
			        }
			    }
		    }
		}
    }
    
    
    
	
	public function zan()
	{	
		$aid = input('param.aid/d');
// 		dump($aid);die;
        $Zan = $this->getZanData($aid, $param);
        /*追加数据*/
        $plugins['zan'] = $Zan;
        /* END */
        $this->assign('plugins', $plugins);
        $html = $this->fetch($this->code.'/'.THEME_STYLE.'/zan');
        $data = [
            'html'  => $html,
            'plugins' => $plugins,
        ];
        $this->success('读取成功！', null, $data);
	}
	public function zan_list()
	{	
		$aid = input('param.aid/d');
        $Zan = $this->getZanData($aid, $param);
        /*追加数据*/
        $plugins['zan'] = $Zan;
        /* END */
        $this->assign('plugins', $plugins);
        $html = $this->fetch($this->code.'/'.THEME_STYLE.'/zan_list');
        $data = [
            'html'  => $html,
            'plugins' => $plugins,
        ];
        $this->success('读取成功！', null, $data);
	}
	
	private function getZanData($aid = null, $param = array())
	{
		$aid = intval($aid);
		$zanWhere = ['a.aid' => $aid];
		/*统计评论数*/
		/*$LikeCount = db('weapp_zan')->where('aid',$aid)->count();*/
        $LikeCount = Db::name('weapp_zan')->field('a.zan_id')
            ->alias('a')
            ->where($zanWhere)
            ->count();
        $result['LikeCount'] = $LikeCount;
		if (empty($LikeCount)) return [];
		/*评论
        $zanData = Db::name('weapp_zan')->field('a.*')
            ->alias('a')
            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
            ->where($zanWhere)
            ->order($OrderBy)
            ->limit($firstRow,$this->listRows)
            ->select();
        END */
		$result['zanData'] = $zanData;
		return $result;
	}
	
	
	//用户文章点赞列表
	public function users_zan()
	{
	   // dump(11);die;
	    $type_id = input('param.type_id/d');
	    $page = input('param.page/d');
	    $limit = input('param.limit/d');
		if (empty($this->users_id)) {
			$this->users_id = 0;
		} else {
			$this->users_id = session('users_id');
		}
// 		 $this->users_id = 5;
		
		$count = M('weapp_zan')
		  ->field('a.*,b.ask_title,b.content,b.ask_id,c.nickname,c.head_pic')
		  ->alias('a')
		  ->join('weapp_ask b', 'a.aid = b.ask_id', 'LEFT')
		  ->join('users c','a.users_id=c.users_id','LEFT')
		  ->where(array('a.users_id' => $this->users_id,'zan_count' => 1))
		  ->count();
		  
		 $data['total_page'] = ceil($count/$limit);
		 $data['count'] = $count;
		 $zan = M('weapp_zan')
		 ->field('a.*,b.ask_title,b.content,b.ask_id,c.nickname,c.head_pic')
		 ->alias('a')
		  ->join('weapp_ask b', 'a.aid = b.ask_id', 'LEFT')
		  ->join('users c','a.users_id=c.users_id','LEFT')
		  ->where(array('a.users_id' => $this->users_id,'zan_count' => 1))
		  ->page($page,$limit)
		  ->select();
		  foreach($zan as $k => $v)
		  {
		       $zan[$k]['url'] = url('plugins/Ask/details', ['ask_id'=>$v['ask_id']]);
		       $zan[$k]['content'] = htmlspecialchars_decode($v['content']);
		       
	         $zans = M('weapp_zan')->where(array('users_id' => $this->users_id,'aid' => $v['aid'],'zan_count' => 1))->find();
	       //  dump($zans);die;
            //踩
            $cai = M('weapp_zan')->where(array('users_id' => $this->users_id,'aid' => $v['aid'],'zan_count' => 0))->find();
           if(empty($zans))
            {
               $zan[$k]['zan'] = 0;
            }else
            {
                $zan[$k]['zan'] = 1;
            }
            
           if(empty($cai))
            {
                $zan[$k]['cai'] = 0;
            }else
            {
                $zan[$k]['cai'] = 1;
            }
            
            //是否收藏
              $collection = M('weapp_collection')->where(array('users_id' => $this->users_id,'ask_id' => $v['ask_id']))->find();
            if(empty($collection))
            {
               $zan[$k]['collection'] = 0;
            }else
            {
                $zan[$k]['collection'] = 1;
            }
            
            //评论条数
           $zan[$k]['comment'] = M('weapp_ask_answer')->where(array('ask_id' => $v['ask_id'],'answer_pid' => 0))->count();
		       
		  }
            $data['status'] = 1;
            $data['msg'] = '';
            $data['res'] = $zan;
            echo json_encode($data,JSON_UNESCAPED_UNICODE);
            exit;
	}
	
	
		//用户文章点赞列表
	public function users_zan2()
	{
	   // dump(11);die;
	    $type_id = input('param.type_id/d');
	    $page = input('param.page/d');
	    $limit = input('param.limit/d');
		if (empty($this->users_id)) {
			$this->users_id = 0;
		} else {
			$this->users_id = session('users_id');
		}
		 $this->users_id = $this->users_id;
		
		$count = M('weapp_zan')
		  ->field('a.*,b.ask_title,b.content,b.ask_id,c.nickname,c.head_pic')
		  ->alias('a')
		  ->join('weapp_ask b', 'a.aid = b.ask_id', 'LEFT')
		  ->join('users c','a.users_id=c.users_id','LEFT')
		  ->where(array('a.users_id' => $this->users_id,'zan_count' => 1,'type_id' => $type_id))
		  ->count();
		  
		 $data['total_page'] = ceil($count/$limit);
		 $data['count'] = $count;
		 $zan = M('weapp_zan')
		 ->field('a.*,b.ask_title,b.content,b.ask_id,c.nickname,c.head_pic')
		 ->alias('a')
		  ->join('weapp_ask b', 'a.aid = b.ask_id', 'LEFT')
		  ->join('users c','a.users_id=c.users_id','LEFT')
		  ->where(array('a.users_id' => $this->users_id,'zan_count' => 1,'type_id' => $type_id))
		  ->page($page,$limit)
		  ->select();
		  foreach($zan as $k => $v)
		  {
		       $zan[$k]['url'] = url('plugins/Ask/details', ['ask_id'=>$v['ask_id']]);
		       $zan[$k]['content'] = htmlspecialchars_decode($v['content']);
		       
	         $zans = M('weapp_zan')->where(array('users_id' => $this->users_id,'aid' => $v['aid'],'zan_count' => 1))->find();
	       //  dump($zans);die;
            //踩
            $cai = M('weapp_zan')->where(array('users_id' => $this->users_id,'aid' => $v['aid'],'zan_count' => 0))->find();
           if(empty($zans))
            {
               $zan[$k]['zan'] = 0;
            }else
            {
                $zan[$k]['zan'] = 1;
            }
            
           if(empty($cai))
            {
                $zan[$k]['cai'] = 0;
            }else
            {
                $zan[$k]['cai'] = 1;
            }
            
            //是否收藏
              $collection = M('weapp_collection')->where(array('users_id' => $this->users_id,'ask_id' => $v['ask_id']))->find();
            if(empty($collection))
            {
               $zan[$k]['collection'] = 0;
            }else
            {
                $zan[$k]['collection'] = 1;
            }
            
            //评论条数
           $zan[$k]['comment'] = M('weapp_ask_answer')->where(array('ask_id' => $v['ask_id'],'answer_pid' => 0))->count();
		       
		  }
            $data['status'] = 1;
            $data['msg'] = '';
            $data['res'] = $zan;
            echo json_encode($data,JSON_UNESCAPED_UNICODE);
            exit;
	}
	
   
}