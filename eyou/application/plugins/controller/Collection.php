<?php
/**
 * 易优CMS
 * ============================================================================
 * 版权所有 2016-2028 海南赞赞网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.eyoucms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小虎哥 <1105415366@qq.com>
 * Date: 2018-4-3
 */

namespace app\plugins\controller;

use think\Db;

class Collection extends Base
{
    /**
     * 构造方法
     */
    public function __construct(){
        parent::__construct();
    }

    /**
     * 收藏与取消
     * @return [type] [description]
     */
    public function ajax_save()
    {
        // dump($_SESSION);die;
        $aid =  input('param.ask_id/d');
        // $aid = 4;
        // dump($aid);die;
        if (IS_AJAX && !empty($aid)) {

            $users_id = session('users_id');
            // dump($users_id);die;
            if (empty($users_id)) {
                
                $data['status'] = -1;
                $data['msg'] = '请先登录';
                echo json_encode($data);
                exit;
        
                // $this->error('请先登录！');
            }

            $result = Db::name('weapp_ask')->field("a.*")
                ->alias('a')
                ->where('ask_id', $aid)
                ->find();
            $title = $result['ask_title'];
            // dump($result);die;
            $pageurl = url('plugins/Ask/details', ['ask_id'=>$result['ask_id']]);

            

            $row = Db::name('weapp_collection')->where([
                'ask_id'   => $aid,
                'users_id'  => $users_id,
                'url'   => $pageurl,
                'lang'  => $this->home_lang,
            ])->find();
            // dump($aid);
            // dump($users_id);die;
            if (empty($row)) {
                $r = Db::name('weapp_collection')->add([
                    'users_id'  => $users_id,
                    'title' => $title,
                    'url'   => $pageurl,
                    'ask_id' => $result['ask_id'],
                    'type_id' => $result['type_id'],
                    'lang'  => $this->home_lang,
                    'add_time'  => getTime(),
                    'update_time' => getTime(),
                ]);
                if (!empty($r)) {
                    
                    $data['status'] = 1;
                    $data['msg'] = '收藏成功';
                    echo json_encode($data);
                    exit;
                    
                    // $this->success('收藏成功', $pageurl, ['opt'=>'add', 'afterHtml'=>'取消收藏']);
                }
            } else {
                $r = Db::name('weapp_collection')->where([
                    'users_id'  => $users_id,
                    'url'   => $pageurl,
                    'lang'  => $this->home_lang,
                ])->delete();
                
                    $data['status'] = 1;
                    $data['msg'] = '取消收藏';
                    echo json_encode($data);
                    exit;
                
                // $this->success('取消成功', $pageurl, ['opt'=>'cancel', 'afterHtml'=>'加入收藏']);
            }
            
            // dump(11);
                $data['status'] = -1;
                $data['msg'] = '收藏失败';
                echo json_encode($data);
                exit;
            // $this->error('收藏失败', $pageurl, ['afterHtml'=>'加入收藏']);
        }
        abort(404);
    }

    /**
     * 判断是否收藏
     * @return [type] [description]
     */
    public function get_collection()
    {
        \think\Session::pause(); // 暂停session，防止session阻塞机制

        if (IS_AJAX) {
            $ask_id = input('param.ask_id/d');
            //$pageurl = urldecode($pageurl);

            $total = Db::name('weapp_collection')->where([
                    'url'   => $pageurl,
                    'lang'  => $this->home_lang,
                ])->count();

            $users_id = session('users_id');
            if (!empty($users_id)) {
                $count = Db::name('weapp_collection')->where([
                    'users_id'  => $users_id,
                    'ask_id'   => $ask_id,
                    'lang'  => $this->home_lang,
                ])->count();
                if (!empty($count)) {
                    $this->success('已收藏', null, ['total'=>$total]);
                }
            }
            $this->error('未收藏', null, ['total'=>$total]);
        }
        abort(404);
    }
    
    //用户收藏的文章/帖子
    
    public function users_collection()
    {
        // dump(11);
	    $type_id = input('param.type_id/d');    //栏目id
	    $page = input('param.page/d');          //页数
	    $limit = input('param.limit/d');        //显示的条数
        $users_id = $_SESSION['think']['users_id'];
        // dump($users_id);die;
        if (empty($users_id)) {
                $data['status'] = -1;
                $data['msg'] = '请先登录！';
                $data['res'] = '';
                echo json_encode($data);
                exit;
            // $this->error('请先登录！');
        }
        $count = $collection = M('weapp_collection')->where(array('users_id' => $users_id))->count();
        $data['total_page'] = ceil($count/$limit);
        $data['count'] = $count;
        $rest = $collection = M('weapp_collection')->where(array('users_id' => $users_id))->page($page,$limit)->select();
        foreach ($rest as $k => $v)
        {
            //赞
            $zan = M('weapp_zan')->where(array('users_id' => $users_id,'aid' => $v['ask_id'],'zan_count' => 1))->find();
            //踩
            $cai = M('weapp_zan')->where(array('users_id' => $users_id,'aid' => $v['ask_id'],'zan_count' => 0))->find();
                   if(empty($zan))
            {
               $rest[$k]['zan'] = 0;
            }else
            {
                $rest[$k]['zan'] = 1;
            }
            
           if(empty($cai))
            {
                $rest[$k]['cai'] = 0;
            }else
            {
                $rest[$k]['cai'] = 1;
            }
            
            //是否收藏
              $collection = M('weapp_collection')->where(array('users_id' => $users_id,'ask_id' => $v['ask_id']))->find();
            if(empty($collection))
            {
               $rest[$k]['collection'] = 0;
            }else
            {
                $rest[$k]['collection'] = 1;
            }
            
            //评论条数
           $rest[$k]['comment'] = M('weapp_ask_answer')->where(array('ask_id' => $v['ask_id'],'answer_pid' => 0))->count();
        }
        $data['status'] = 1;
        $data['msg'] = '';
        $data['res'] = $rest;
        echo json_encode($data);
        exit;
    }
    

    public function users_collection2()
    {
        // dump(11);
	    $type_id = input('param.type_id/d');    //栏目id
	    $page = input('param.page/d');          //页数
	    $limit = input('param.limit/d');        //显示的条数
        $users_id = $_SESSION['think']['users_id'];
        // dump($users_id);die;
        if (empty($users_id)) {
                $data['status'] = -1;
                $data['msg'] = '请先登录！';
                $data['res'] = '';
                echo json_encode($data);
                exit;
        }
        $count = $collection = M('weapp_collection')->where(array('users_id' => $users_id,'type_id' => $type_id))->count();
        $data['total_page'] = ceil($count/$limit);
        $data['count'] = $count;
        $rest = $collection = M('weapp_collection')->where(array('users_id' => $users_id,'type_id' => $type_id))->page($page,$limit)->select();
        foreach ($rest as $k => $v)
        {
            //赞
            $zan = M('weapp_zan')->where(array('users_id' => $users_id,'aid' => $v['ask_id'],'zan_count' => 1))->find();
            //踩
            $cai = M('weapp_zan')->where(array('users_id' => $users_id,'aid' => $v['ask_id'],'zan_count' => 0))->find();
                   if(empty($zan))
            {
               $rest[$k]['zan'] = 0;
            }else
            {
                $rest[$k]['zan'] = 1;
            }
            
           if(empty($cai))
            {
                $rest[$k]['cai'] = 0;
            }else
            {
                $rest[$k]['cai'] = 1;
            }
            
            //是否收藏
              $collection = M('weapp_collection')->where(array('users_id' => $users_id,'ask_id' => $v['ask_id']))->find();
            //   dump($value['ask_id']);
            //   dump($users_id);die;
            if(empty($collection))
            {
               $rest[$k]['collection'] = 0;
            }else
            {
                $rest[$k]['collection'] = 1;
            }
            
            //评论条数
           $rest[$k]['comment'] = M('weapp_ask_answer')->where(array('ask_id' => $v['ask_id'],'answer_pid' => 0))->count();
        }
        // dump($rest);die;
        $data['status'] = 1;
        $data['msg'] = '';
        $data['res'] = $rest;
        echo json_encode($data);
        exit;
        // dump($rest);die;
    }
    
}