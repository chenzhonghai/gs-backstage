<?php
/**
 * 易优CMS
 * ============================================================================
 * 版权所有 2016-2028 海南赞赞网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.eyoucms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 陈风任 <491085389@qq.com>
 * Date: 2019-7-30
 */

namespace app\plugins\model;

use think\Model;
use think\Db;
use think\Page;
use think\Config;
use app\plugins\logic\AskLogic;

/**
 * 模型
 */
class Ask extends Model
{
    //初始化
    protected function initialize()
    {
        // 需要调用`Model`的`initialize`方法
        parent::initialize();

        $this->users_db = Db::name('users'); // 会员表
        $this->weapp_ask_db = Db::name('weapp_ask'); // 问题表
        $this->weapp_ask_answer_db = Db::name('weapp_ask_answer'); // 答案表
        $this->weapp_ask_type_db = Db::name('weapp_ask_type'); // 问题栏目分类表
        $this->weapp_ask_answer_like_db = Db::name('weapp_ask_answer_like'); // 问题回答点赞表
        $this->AskLogic = new AskLogic;
    }

    // 用户信息及问题回答数据
    public function GetUsersAskCount($view_uid = null)
    {
    	// 返回参数
    	$result = [];

    	// 查询会员信息
    	$users = session('users');
    	if (!empty($users) && $users['users_id'] == $view_uid) {
			$result = [
    			'NickName' => $users['nickname'],
				'HeadPic'  => $users['head_pic'],
				'IsLogin'  => 1,
    		];
    	}else{
	    	$UsersInfo = $this->users_db->field('nickname,head_pic')->where('users_id', $view_uid)->find();
	    	if (!empty($UsersInfo)) {
	    		$result = [
	    			'NickName' => $UsersInfo['nickname'],
					'HeadPic'  => $UsersInfo['head_pic'],
					'IsLogin'  => 0,
	    		];
	    	}
    	}

    	// 问答数量
    	if (!empty($result['NickName'])) {
    		// 查询问题数量
			$result['AskCount'] = $this->weapp_ask_db->where('users_id', $view_uid)->count();
			// 查询回答数量
			$result['AnswerCount'] = $this->weapp_ask_answer_db->where('users_id', $view_uid)->count();
    	}

    	// 拼装URL
        $result['UsersAskUrl']    = url('plugins/Ask/ask_index', ['view_uid'=>$view_uid]);
        // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=ask_index&view_uid='.$view_uid;
        $result['UsersAnswerUrl'] = url('plugins/Ask/ask_index', ['view_uid'=>$view_uid, 'method'=>'answer']);
        // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=ask_index&view_uid='.$view_uid.'&method=answer';
    	return $result;
    }

    // 会员问题回答数据
    public function GetUsersAskData($view_uid = null, $is_ask = true)
    {
    	// 返回参数
    	$result = [];
    	$field = 'a.ask_id, a.ask_title, a.click, a.replies, a.add_time, a.is_review, b.users_id, b.nickname';
    	if (!empty($is_ask)) {
    		// 提问问题查询列表
    		$where = [
    			'a.status'   => ['IN',[0,1]],
    			'a.users_id' => $view_uid,
    		];

    		/* 分页 */
	        $count   = $this->weapp_ask_db->alias('a')->where($where)->count('ask_id');
	        $pageObj = new Page($count, 10);
	        $result['pageStr'] = $pageObj->show();
	        /* END */

	        /*问题表数据(问题表+会员表+问题分类表)*/
	        $result['AskData'] = $this->weapp_ask_db->field($field)
	            ->alias('a')
	            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
	            ->where($where)
	            ->order('a.add_time desc')
	            ->limit($pageObj->firstRow.','.$pageObj->listRows)
	            ->select();
	        /* END */

	        /*问题回答人查询*/
	        $ask_id = get_arr_column($result['AskData'], 'ask_id');
	        $RepliesData = $this->weapp_ask_answer_db->field('a.ask_id, a.users_id, b.nickname')
	            ->alias('a')
	            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
	            ->where('ask_id', 'IN', $ask_id)
	            ->select();
			/* END */
    	}else{
    		// 回答问题查询列表
    		/*问题回答人查询*/
    		$RepliesData = $this->weapp_ask_answer_db->field('a.ask_id, a.users_id, b.nickname')
	            ->alias('a')
	            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
	            ->select();
    		/* END */

    		/* 查询条件 */
    		$UsersIds = group_same_key($RepliesData, 'users_id');
    		$ask_ids  = get_arr_column($UsersIds[$view_uid], 'ask_id');
    		// 按主键去重
    		$ask_ids  = array_unique($ask_ids, SORT_REGULAR);
    		$where = [
    			'a.status'   => ['IN', [0, 1]],
    			'a.ask_id' 	 => ['IN', $ask_ids],
    		];
    		/* END */

    		/* 分页 */
	        $count 	 = $this->weapp_ask_db->alias('a')->where($where)->count('ask_id');
	        $pageObj = new Page($count, 10);
	        $result['pageStr'] = $pageObj->show();
	        /* END */

	        /*问题表数据(问题表+会员表+问题分类表)*/
	        $result['AskData'] = $this->weapp_ask_db->field($field)
	            ->alias('a')
	            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
	            ->where($where)
	            ->order('a.add_time desc')
	            ->limit($pageObj->firstRow.','.$pageObj->listRows)
	            ->select();
			/* END */
    	}

    	// 取出提问问题的ID作为主键
    	$RepliesData = group_same_key($RepliesData, 'ask_id');
    	
        /*数据处理*/
        foreach ($result['AskData'] as $key => $value) {
            // 时间友好显示处理
            $result['AskData'][$key]['add_time'] = friend_date($value['add_time']);
            // 问题内容Url
            $result['AskData'][$key]['AskUrl']   = url('plugins/Ask/details', ['ask_id'=>$value['ask_id']]);
            // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=details&ask_id='.$value['ask_id'];
            // 回复处理
            if (!empty($RepliesData[$value['ask_id']])) {
                $UsersConut = array_values(array_unique($RepliesData[$value['ask_id']], SORT_REGULAR));
                $result['AskData'][$key]['UsersConut'] = count($UsersConut);
            }else{
            	$UsersConut = ['0'=>['nickname'=>$value['nickname']]];
                $result['AskData'][$key]['UsersConut'] = $value['nickname'];
            }
            // 处理参与讨论者的A标签跳转
            foreach ($UsersConut as $kkk => $vvv) {
            	$nickname = $vvv['nickname'];
            	if (($kkk + 1) == count($UsersConut) || 2 == $kkk) {
            		$result['AskData'][$key]['NickName'] .= '<a href="javascript:void(0);">'. $nickname .'</a>';
            		break;
            	}else{
            		$result['AskData'][$key]['NickName'] .= '<a href="javascript:void(0);">'. $nickname .'</a>、';
            	}
            }
        }
        /* END */

    	return $result;
    }
    
    //推荐文章
    
    public function is_tuijian($where = array(), $limit = 20,$page=1, $field = null,$users,$users_id)
    {
    	// 返回参数
    	$result = [];
    	// 没有传入则默认查询这些字段
    	if (empty($field)) {
    		$field = 'a.*, b.nickname, b.head_pic, c.type_name';
    	}

    	$result['PendingAsk'] = '';
    	if (isset($where['a.replies']) && 0 == $where['a.replies']) {
    		$result['PendingAsk'] = '待回答';
    	}
    	
		if (!empty($where['SearchName']) || null == $where['SearchName']) {
        	$SearchName = $where['SearchName'];
        	unset($where['SearchName']);
            $result['SearchName']    = $SearchName;
            $result['SearchNameRed'] = "搜索 <font color='red'>".$SearchName."</font> 结果";
        }


    	if (!empty($where['a.type_id'])) {
    		// 存在栏目ID则执行
    		$TypeData = $this->weapp_ask_type_db->where('parent_id',$where['a.type_id'])->field('type_id')->select();
    		if (!empty($TypeData)) {
    			// 将顶级栏目ID合并到新条件中
    			$type_id = get_arr_column($TypeData, 'type_id');
    			$type_id = array_merge($type_id, array(0 => $where['a.type_id']));
    			$where['a.type_id'] = ['IN', $type_id];
    		}

    		// 查询满足要求的总记录数
	        $count = $this->weapp_ask_db->alias('a')->where($where)->where(array('a.is_hot_list' => 1))->count('ask_id');
	        
	       // $result['total_page'] = ceil($count/$limit);
        //     $result['count'] = $count;
	        // 实例化分页类 传入总记录数和每页显示的记录数
	        $pageObj = new \weapp\Ask\logic\PageLogic($count, 10);
	        /*问题表数据(问题表+会员表+问题分类表)*/
	        $result['AskData'] = $this->weapp_ask_db->field($field)
	            ->alias('a')
	            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
	            ->join('__WEAPP_ASK_TYPE__ c', 'a.type_id = c.type_id', 'LEFT')
	            ->where(array('a.is_hot_list' => 1,'a.so' => 0))
	            ->where($where)
	            ->order('a.ask_id desc')
	            ->select();
	           // dump($result);die;
	            
	             $askk = $this->weapp_ask_db->field($field)
	            ->alias('a')
	            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
	            ->join('__WEAPP_ASK_TYPE__ c', 'a.type_id = c.type_id', 'LEFT')
	            ->where('a.so != 0')
	            ->where($where)
	            ->order('a.ask_id desc')
	            ->select();
	            
            $follow = M('follow')->where(array('users_id' => $users_id))->getField('follow_id', true);
            foreach($askk as $ke => $ve)
            {
                if($ve['so'] == 1)
                {
                    if(!empty($follow))
	                {
	                    if(in_array($ve['users_id'],$follow))
	                    {
	                        $aske[] = $ve;
	                    }else
	                    {
	                        $aske = [];
	                    }
	                }else
	                {
	                    $aske = [];
	                }
                }
                
                if($ve['so'] == 2)
                {
                    if($ve['users_id'] == $users_id)
                    {
                        $ask[] = $ve;
                    }else
                    {
                        $ask = [];
                    }
                }
             
            }
            $result['AskData'] = array_merge($result['AskData'],$aske,$ask);
            $result['count'] = count($result['AskData']);
            
            $results['recommend']['count'] = count($result['AskData']);
            $results['recommend']['total_page'] = ceil( $results['AskData']['count']/$limit);
            
            $result['total_page'] = ceil($result['count']/$limit);
            $result['AskData'] = array_slice($result['AskData'],($page-1)*$limit,$limit);
	           // dump($result);die;
	            
	        // 分页显示输出
	        $result['pageStr'] = $pageObj->show();
	        
	       // dump(22);die;
    	}else{
    	    //dump($users);die;
    		/*问题表数据(问题表+会员表+问题分类表)*/
	    	$result['AskData'] = $this->weapp_ask_db->field($field)
	            ->alias('a')
	            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
	            ->join('__WEAPP_ASK_TYPE__ c', 'a.type_id = c.type_id', 'LEFT')
	            ->where(array('a.is_hot_list' => 1))
	            ->where($where)
	            ->where(array('a.ask_id' => array('not in',$users)))
	            ->order('a.ask_id desc')
	            ->page($page,$limit)
	            ->select();
	           // dump($result);die;
	        /* END */
            $result['pageStr'] = '';
    	}

        /*问题回答人查询*/
        $ask_id = get_arr_column($result['AskData'], 'ask_id');
        $RepliesData = $this->weapp_ask_answer_db->field('a.ask_id, a.users_id, b.head_pic, b.nickname')
            ->alias('a')
            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
            ->where('ask_id', 'IN', $ask_id)
            ->select();
        $RepliesDataNew = [];
        foreach ($RepliesData as $key => $value) {
            /*头像处理*/
            $value['head_pic'] = get_head_pic($value['head_pic']);
            /* END */
            // 将二维数组以ask_id值作为键，并归类数组，效果同等group_same_key
            $RepliesDataNew[$value['ask_id']][] = $value;
        }
        /* END */

        /*数据处理*/
        foreach ($result['AskData'] as $key => $value) {
            /*头像处理*/
            $value['head_pic'] = get_head_pic($value['head_pic']);
            $result['AskData'][$key]['head_pic'] = $value['head_pic'];
            /* END */
           $zan = M('weapp_zan')->where(array('users_id' => $users_id,'aid' => $value['ask_id'],'zan_count' => 1))->find();
           $cai = M('weapp_zan')->where(array('users_id' => $users_id,'aid' => $value['ask_id'],'zan_count' => 0))->find();
           $ask_count =  M('weapp_ask_answer')->where(array('ask_id' => $value['ask_id'],'answer_pid' => 0))->count();
        //   dump($ask_count);die;
           
        //   dump($result);die;
        //   dump($zan);
       // htmlspecialchars_decode($value['content']));die;
            
            $result['AskData'][$key]['content'] = htmlspecialchars_decode($value['content']);
            if(empty($zan))
            {
               $result['AskData'][$key]['zan'] = 0;
            }else
            {
               $result['AskData'][$key]['zan'] = 1;
            }
            
           if(empty($cai))
            {
               $result['AskData'][$key]['cai'] = 0;
            }else
            {
               $result['AskData'][$key]['cai'] = 1;
            }
            
            
              $collection = M('weapp_collection')->where(array('users_id' => $users_id,'ask_id' => $value['ask_id']))->find();
            //   dump($value['ask_id']);
            //   dump($users_id);die;
            if(empty($collection))
            {
               $result['AskData'][$key]['collection'] = 0;
            }else
            {
               $result['AskData'][$key]['collection'] = 1;
            }
            

        	// 若存在搜索关键词则标红关键词
        	if (isset($SearchName) && !empty($SearchName)) {
    			$result['AskData'][$key]['ask_title'] = $this->AskLogic->GetRedKeyWord($SearchName, $value['ask_title']);
        	}

            // 时间友好显示处理
            $result['AskData'][$key]['add_time'] = friend_date($value['add_time']);
            // 栏目分类Url
            $result['AskData'][$key]['TypeUrl'] = url('plugins/Ask/index', ['type_id'=>$value['type_id']]);
            // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=index&type_id='.$value['type_id'];
            // 问题内容Url
            $result['AskData'][$key]['AskUrl']  = url('plugins/Ask/details', ['ask_id'=>$value['ask_id']]);
            // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=details&ask_id='.$value['ask_id'];
            // 回复处理
            if (!empty($RepliesDataNew[$value['ask_id']])) {
                $UsersConut = array_unique($RepliesDataNew[$value['ask_id']], SORT_REGULAR);
                // 读取前三条数据
                $result['AskData'][$key]['HeadPic']    = array_slice($UsersConut, 0, 3);
                $result['AskData'][$key]['UsersConut'] = $ask_count;
            }else{
                $result['AskData'][$key]['HeadPic']    = ['0'=>['head_pic'=>$value['head_pic']]];
                $result['AskData'][$key]['UsersConut'] = $ask_count;
            }
        }
        // dump($result);die;
        /* END */

        // 是否推荐
        if (!empty($where['a.is_recom'])) {
        	$result['IsRecom'] = 1;
        }else{
        	$result['IsRecom'] = 0;
        }
        
        $results['recommend']['AskData'] = $result['AskData'];
        // $results['recommend']['total_page'] = ceil($count/$limit);
        // $results['recommend']['count'] = $count;
        
        //dump($results);die;
        
        return $results;
    }
    
    public function hot($where = array(), $limit = 20,$page=1, $field = null,$users,$users_id)
    {
    	// 返回参数
    	$result = [];
    	// 没有传入则默认查询这些字段
    	if (empty($field)) {
    		$field = 'a.*, b.nickname, b.head_pic, c.type_name';
    	}

    	$result['PendingAsk'] = '';
    	if (isset($where['a.replies']) && 0 == $where['a.replies']) {
    		$result['PendingAsk'] = '待回答';
    	}
    	
		if (!empty($where['SearchName']) || null == $where['SearchName']) {
        	$SearchName = $where['SearchName'];
        	unset($where['SearchName']);
            $result['SearchName']    = $SearchName;
            $result['SearchNameRed'] = "搜索 <font color='red'>".$SearchName."</font> 结果";
        }


    	if (!empty($where['a.type_id'])) {
    		// 存在栏目ID则执行
    		$TypeData = $this->weapp_ask_type_db->where('parent_id',$where['a.type_id'])->field('type_id')->select();
    		if (!empty($TypeData)) {
    			// 将顶级栏目ID合并到新条件中
    			$type_id = get_arr_column($TypeData, 'type_id');
    			$type_id = array_merge($type_id, array(0 => $where['a.type_id']));
    			$where['a.type_id'] = ['IN', $type_id];
    		}

    		// 查询满足要求的总记录数
	        $count = $this->weapp_ask_db->alias('a')->where($where)->where(array('a.is_recom' => 1))->count('ask_id');
	        
	       // $result['total_page'] = ceil($count/$limit);
        //     $result['count'] = $count;
	        // 实例化分页类 传入总记录数和每页显示的记录数
	        $pageObj = new \weapp\Ask\logic\PageLogic($count, 10);
	        /*问题表数据(问题表+会员表+问题分类表)*/
	        $result['AskData'] = $this->weapp_ask_db->field($field)
	            ->alias('a')
	            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
	            ->join('__WEAPP_ASK_TYPE__ c', 'a.type_id = c.type_id', 'LEFT')
	            ->where(array('a.is_recom' => 1,'a.so' => 0))
	            ->where($where)
	            ->order('a.ask_id desc')
	            ->select();
	           // dump($where);die;
	            
	             $askk = $this->weapp_ask_db->field($field)
	            ->alias('a')
	            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
	            ->join('__WEAPP_ASK_TYPE__ c', 'a.type_id = c.type_id', 'LEFT')
	            ->where('a.so != 0')
	            ->where($where)
	            ->order('a.ask_id desc')
	            ->select();
	            
	            
	            
	           // DUMP($askk);DIE;
	            
	            $follow = M('follow')->where(array('users_id' => $users_id))->getField('follow_id', true);
	            foreach($askk as $ke => $ve)
	            {
	                if($ve['so'] == 1)
	                {
                        if(!empty($follow))
    	                {
    	                    if(in_array($ve['users_id'],$follow))
    	                    {
    	                        $aske[] = $ve;
    	                    }else
    	                    {
    	                        $aske = [];
    	                    }
    	                }else
    	                {
    	                    $aske = [];
    	                }
	                }
	                
	                if($ve['so'] == 2)
	                {
	                    if($ve['users_id'] == $users_id)
	                    {
	                        $ask[] = $ve;
	                    }else
	                    {
	                        $ask = [];
	                    }
	                }
	             
	            }
	            $result['AskData'] = array_merge($result['AskData'],$aske,$ask);
                $result['count'] = count($result['AskData']);
                
                $results['hot']['count'] = count($result['AskData']);
                $results['hot']['total_page'] = ceil( $results['AskData']['count']/$limit);
                
                
                $result['total_page'] = ceil($result['count']/$limit);
              $result['AskData'] = array_slice($result['AskData'],($page-1)*$limit,$limit);
                // dump($result);die;
	            
	        // 分页显示输出
	        $result['pageStr'] = $pageObj->show();
	        
	       // dump(22);die;
    	}else{
    	    //dump($users);die;
    		/*问题表数据(问题表+会员表+问题分类表)*/
	    	$result['AskData'] = $this->weapp_ask_db->field($field)
	            ->alias('a')
	            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
	            ->join('__WEAPP_ASK_TYPE__ c', 'a.type_id = c.type_id', 'LEFT')
	            ->where(array('a.is_recom' => 1))
	            ->where($where)
	            ->where(array('a.ask_id' => array('not in',$users)))
	            ->order('a.ask_id desc')
	            ->page($page,$limit)
	            ->select();
	           // dump($result);die;
	        /* END */
            $result['pageStr'] = '';
    	}

        /*问题回答人查询*/
        $ask_id = get_arr_column($result['AskData'], 'ask_id');
        $RepliesData = $this->weapp_ask_answer_db->field('a.ask_id, a.users_id, b.head_pic, b.nickname')
            ->alias('a')
            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
            ->where('ask_id', 'IN', $ask_id)
            ->select();
        $RepliesDataNew = [];
        foreach ($RepliesData as $key => $value) {
            /*头像处理*/
            $value['head_pic'] = get_head_pic($value['head_pic']);
            /* END */
            // 将二维数组以ask_id值作为键，并归类数组，效果同等group_same_key
            $RepliesDataNew[$value['ask_id']][] = $value;
        }
        /* END */

        /*数据处理*/
        foreach ($result['AskData'] as $key => $value) {
            /*头像处理*/
            $value['head_pic'] = get_head_pic($value['head_pic']);
            $result['AskData'][$key]['head_pic'] = $value['head_pic'];
            /* END */
           $zan = M('weapp_zan')->where(array('users_id' => $users_id,'aid' => $value['ask_id'],'zan_count' => 1))->find();
           $cai = M('weapp_zan')->where(array('users_id' => $users_id,'aid' => $value['ask_id'],'zan_count' => 0))->find();
           $ask_count =  M('weapp_ask_answer')->where(array('ask_id' => $value['ask_id'],'answer_pid' => 0))->count();
        //   dump($ask_count);die;
           
        //   dump($result);die;
        //   dump($zan);
       // htmlspecialchars_decode($value['content']));die;
            
            $result['AskData'][$key]['content'] = htmlspecialchars_decode($value['content']);
            if(empty($zan))
            {
               $result['AskData'][$key]['zan'] = 0;
            }else
            {
               $result['AskData'][$key]['zan'] = 1;
            }
            
           if(empty($cai))
            {
               $result['AskData'][$key]['cai'] = 0;
            }else
            {
               $result['AskData'][$key]['cai'] = 1;
            }
            
            
              $collection = M('weapp_collection')->where(array('users_id' => $users_id,'ask_id' => $value['ask_id']))->find();
            //   dump($value['ask_id']);
            //   dump($users_id);die;
            if(empty($collection))
            {
               $result['AskData'][$key]['collection'] = 0;
            }else
            {
               $result['AskData'][$key]['collection'] = 1;
            }
            

        	// 若存在搜索关键词则标红关键词
        	if (isset($SearchName) && !empty($SearchName)) {
    			$result['AskData'][$key]['ask_title'] = $this->AskLogic->GetRedKeyWord($SearchName, $value['ask_title']);
        	}

            // 时间友好显示处理
            $result['AskData'][$key]['add_time'] = friend_date($value['add_time']);
            // 栏目分类Url
            $result['AskData'][$key]['TypeUrl'] = url('plugins/Ask/index', ['type_id'=>$value['type_id']]);
            // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=index&type_id='.$value['type_id'];
            // 问题内容Url
            $result['AskData'][$key]['AskUrl']  = url('plugins/Ask/details', ['ask_id'=>$value['ask_id']]);
            // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=details&ask_id='.$value['ask_id'];
            // 回复处理
            if (!empty($RepliesDataNew[$value['ask_id']])) {
                $UsersConut = array_unique($RepliesDataNew[$value['ask_id']], SORT_REGULAR);
                // 读取前三条数据
                $result['AskData'][$key]['HeadPic']    = array_slice($UsersConut, 0, 3);
                $result['AskData'][$key]['UsersConut'] = $ask_count;
            }else{
                $result['AskData'][$key]['HeadPic']    = ['0'=>['head_pic'=>$value['head_pic']]];
                $result['AskData'][$key]['UsersConut'] = $ask_count;
            }
        }
        // dump($result);die;
        /* END */

        // 是否推荐
        if (!empty($where['a.is_recom'])) {
        	$result['IsRecom'] = 1;
        }else{
        	$result['IsRecom'] = 0;
        }
        
        $results['hot']['AskData'] = $result['AskData'];
        
        
        // $results['hot']['total_page'] = ceil($count/$limit);
        // $results['hot']['count'] = $count;
        
        // dump($results);die;
        
        return $results;
    }

    // 问题数据
    public function GetNewAskData($where = array(), $limit = 20,$page=1, $field = null,$users,$users_id)
    {
        
        // dump($field);die;
    	// 返回参数
    	$result = [];
    	// 没有传入则默认查询这些字段
    	if (empty($field)) {
    		$field = 'a.*, b.nickname, b.head_pic, c.type_name';
    	}

    	$result['PendingAsk'] = '';
    	if (isset($where['a.replies']) && 0 == $where['a.replies']) {
    		$result['PendingAsk'] = '待回答';
    	}

    	// 提取搜索关键词
    	if (!empty($where['SearchName']) || null == $where['SearchName']) {
        	$SearchName = $where['SearchName'];
        	unset($where['SearchName']);
            $result['SearchName']    = $SearchName;
            $result['SearchNameRed'] = "搜索 <font color='red'>".$SearchName."</font> 结果";
        }

    	if (!empty($where['a.type_id'])) {
    		// 存在栏目ID则执行
    		$TypeData = $this->weapp_ask_type_db->where('parent_id',$where['a.type_id'])->field('type_id')->select();
    		if (!empty($TypeData)) {
    			// 将顶级栏目ID合并到新条件中
    			$type_id = get_arr_column($TypeData, 'type_id');
    			$type_id = array_merge($type_id, array(0 => $where['a.type_id']));
    			$where['a.type_id'] = ['IN', $type_id];
    		}

    		// 查询满足要求的总记录数
	        $count = $this->weapp_ask_db->alias('a')->where($where)->count('ask_id');
	        
	       // $result['total_page'] = ceil($count/$limit);
        //     $result['count'] = $count;
	        // 实例化分页类 传入总记录数和每页显示的记录数
	        $pageObj = new \weapp\Ask\logic\PageLogic($count, 10);
	        /*问题表数据(问题表+会员表+问题分类表)*/
	        $result['AskData'] = $this->weapp_ask_db->field($field)
	            ->alias('a')
	            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
	            ->join('__WEAPP_ASK_TYPE__ c', 'a.type_id = c.type_id', 'LEFT')
	            ->where(array('a.so' => 0))
	           // ->where(arry)
	            ->where($where)
	            ->order('a.ask_id desc')
	           // ->page($page,$limit)
	            ->select();
	           //  dump($result['AskData']);die;
	            
                $askk = $this->weapp_ask_db->field($field)
	            ->alias('a')
	            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
	            ->join('__WEAPP_ASK_TYPE__ c', 'a.type_id = c.type_id', 'LEFT')
	            ->where('a.so != 0')
	            ->where($where)
	            ->order('a.ask_id desc')
	            ->select();
	            
	            
	            
	           // DUMP($askk);DIE;
	            
	            $follow = M('follow')->where(array('users_id' => $users_id))->getField('follow_id', true);
	            foreach($askk as $ke => $ve)
	            {
	                if($ve['so'] == 1)
	                {
                        if(!empty($follow))
    	                {
    	                    if(in_array($ve['users_id'],$follow))
    	                    {
    	                        $aske[] = $ve;
    	                    }else
    	                    {
    	                        $aske[] = '';
    	                    }
    	                }else
    	                {
    	                    $aske = [];
    	                }
	                }
	                
	                if($ve['so'] == 2)
	                {
	                   //dump($users_id);die;
	                    if($ve['users_id'] == $users_id)
	                    {
	                       // dump($ve);die;
	                        $ask[] = $ve;
	                    }else
	                    {
	                        $ask[] = '';
	                    }
	                }
	             
	            }
	            
	            if(empty($ask))
	            {
	                $ask = [];
	            }
	             if(empty($aske))
	            {
	                $aske = [];
	            }
	            $result['AskData'] = array_filter(array_merge($result['AskData'],$aske,$ask));
	           // dump($result['AskData']);die;
	            $last_names = array_column($result['AskData'],'ask_id');
                array_multisort($last_names,SORT_DESC, $result['AskData']);

               $results['AskData']['count'] = count($result['AskData']);
               $results['AskData']['total_page'] = ceil( $results['AskData']['count']/$limit);
                
                $result['count'] = count($result['AskData']);
                $result['total_page'] = ceil($result['count']/$limit);
                // dump( $result['count']);
                // dump($result);die;
                $result['AskData'] = array_slice($result['AskData'],($page-1)*$limit,$limit);
                
                // dump($results['AskData']);die;
	        // 分页显示输出
	        $result['pageStr'] = $pageObj->show();
	        
	       // dump($result['count']);die;
    	}else{
    	   // dump($users);die;
    		/*问题表数据(问题表+会员表+问题分类表)*/
	    	$result['AskData'] = $this->weapp_ask_db->field($field)
	            ->alias('a')
	            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
	            ->join('__WEAPP_ASK_TYPE__ c', 'a.type_id = c.type_id', 'LEFT')
	            ->where($where)
	            ->where(array('a.ask_id' => array('not in',$users)))
	            ->order('a.ask_id desc')
	            ->page($page,$limit)
	            ->select();
	           // dump($result);die;
	        /* END */
            $result['pageStr'] = '';
    	}

        /*问题回答人查询*/
        $ask_id = get_arr_column($result['AskData'], 'ask_id');
        $RepliesData = $this->weapp_ask_answer_db->field('a.ask_id, a.users_id, b.head_pic, b.nickname')
            ->alias('a')
            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
            ->where('ask_id', 'IN', $ask_id)
            ->select();
        $RepliesDataNew = [];
        foreach ($RepliesData as $key => $value) {
            /*头像处理*/
            $value['head_pic'] = get_head_pic($value['head_pic']);
            /* END */
            // 将二维数组以ask_id值作为键，并归类数组，效果同等group_same_key
            $RepliesDataNew[$value['ask_id']][] = $value;
        }
        /* END */

        /*数据处理*/
        foreach ($result['AskData'] as $key => $value) {
            /*头像处理*/
            $value['head_pic'] = get_head_pic($value['head_pic']);
            $result['AskData'][$key]['head_pic'] = $value['head_pic'];
            /* END */
           $zan = M('weapp_zan')->where(array('users_id' => $users_id,'aid' => $value['ask_id'],'zan_count' => 1))->find();
           $cai = M('weapp_zan')->where(array('users_id' => $users_id,'aid' => $value['ask_id'],'zan_count' => 0))->find();
           $ask_count =  M('weapp_ask_answer')->where(array('ask_id' => $value['ask_id'],'answer_pid' => 0))->count();
        //   dump($ask_count);die;
           
        //   dump($result);die;
        //   dump($zan);
       // htmlspecialchars_decode($value['content']));die;
            
            $result['AskData'][$key]['content'] = htmlspecialchars_decode($value['content']);
            if(empty($zan))
            {
               $result['AskData'][$key]['zan'] = 0;
            }else
            {
               $result['AskData'][$key]['zan'] = 1;
            }
            
           if(empty($cai))
            {
               $result['AskData'][$key]['cai'] = 0;
            }else
            {
               $result['AskData'][$key]['cai'] = 1;
            }
            
            
              $collection = M('weapp_collection')->where(array('users_id' => $users_id,'ask_id' => $value['ask_id']))->find();
            //   dump($value['ask_id']);
            //   dump($users_id);die;
            if(empty($collection))
            {
               $result['AskData'][$key]['collection'] = 0;
            }else
            {
               $result['AskData'][$key]['collection'] = 1;
            }
            

        	// 若存在搜索关键词则标红关键词
    //     	if (isset($SearchName) && !empty($SearchName)) {
    // 			$result['AskData'][$key]['ask_title'] = $this->AskLogic->GetRedKeyWord($SearchName, $value['ask_title']);
    //     	}

            // 时间友好显示处理
            $result['AskData'][$key]['add_time'] = friend_date($value['add_time']);
            // 栏目分类Url
            $result['AskData'][$key]['TypeUrl'] = url('plugins/Ask/index', ['type_id'=>$value['type_id']]);
            // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=index&type_id='.$value['type_id'];
            // 问题内容Url
            $result['AskData'][$key]['AskUrl']  = url('plugins/Ask/details', ['ask_id'=>$value['ask_id']]);
            // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=details&ask_id='.$value['ask_id'];
            // 回复处理
            if (!empty($RepliesDataNew[$value['ask_id']])) {
                $UsersConut = array_unique($RepliesDataNew[$value['ask_id']], SORT_REGULAR);
                // 读取前三条数据
                $result['AskData'][$key]['HeadPic']    = array_slice($UsersConut, 0, 3);
                $result['AskData'][$key]['UsersConut'] = $ask_count;
            }else{
                $result['AskData'][$key]['HeadPic']    = ['0'=>['head_pic'=>$value['head_pic']]];
                $result['AskData'][$key]['UsersConut'] = $ask_count;
            }
        }
        // dump($result);die;
        /* END */

        // 是否推荐
        if (!empty($where['a.is_recom'])) {
        	$result['IsRecom'] = 1;
        }else{
        	$result['IsRecom'] = 0;
        }
        
        $results['AskData']['AskData'] = $result['AskData'];
        
        // $results['AskData']['count'] = count($result['AskData']);
        // $results['AskData']['total_page'] = ceil($result['count']/$limit);
        // $results['AskData']['total_page'] = ceil($count/$limit);
        // $results['AskData']['count'] = $count;
                //   $results['AskData']['count'] = count($result['AskData']);
                //  $results['AskData']['total_page'] = ceil($result['count']/$limit);
        
        // dump($results);die;
        
        return $results;
    }

    // 问题栏目分类数据
    public function GetAskTypeData($param = array(), $is_add = null)
    {   
        // 数据初始化
    	$result = [
            'IsTypeId' => 0,
            'ParentId' => 0,
            'TypeId'   => 0,
            'TypeName' => '',
            'HtmlCode' => '<span style="color: red;">请先让管理员在插件栏目列表中添加分类！</span>',
            'TypeData' => []
        ];

    	/*栏目处理*/
        $TypeData = $this->weapp_ask_type_db->order('sort_order asc, type_id asc')->select();
        if (empty($TypeData)) return [];

        foreach ($TypeData as $key => $value) {
        	/*若存在分类ID且和数据中的值相等则执行*/
	        if (!empty($param['type_id']) && $value['type_id'] == $param['type_id']) {
	            $result['TypeName'] = $value['type_name'];
	            $result['TypeId']   = $value['type_id'];
	            $result['ParentId'] = $value['parent_id'];
	            
	            // 描点标记焦点
	            if (!empty($value['parent_id'])) {
	            	$result['IsTypeId'] = $value['parent_id'];
	            }else{
	            	$result['IsTypeId'] = $value['type_id'];
	            }
	        }
	        /* END */

	        // 是否顶级栏目
            if($value['parent_id'] == 0){
        		$value['SelectType'] = 0;
                $PidData[] = $value;
            }else{
            	$value['SelectSubType'] = 0;
                $TidData[] = $value;
            }
        }

        // 调用来源
        if (isset($is_add) && 'add_ask' == $is_add) {
        	// 问题发布调用
        	$return['HtmlCode'] = $this->AskLogic->GetTypeHtmlCode($PidData, $TidData, $param['type_id']);
        	return $return;
        }else if (isset($is_add) && 'edit_ask' == $is_add) {
        	// 问题编辑调用
        	$return['HtmlCode'] = $this->AskLogic->GetTypeHtmlCode($PidData, $TidData, $param['type_id']);
        	return $return;
        }else{
        	// 列表或内容页调用
        	$TidData = group_same_key($TidData, 'parent_id');
        	// 一级栏目处理
	        foreach($PidData as $P_key => $PidValue){
	        	$result['TypeData'][] = $PidValue;
	            $result['TypeData'][$P_key]['Url'] = url('plugins/Ask/index', ['type_id'=>$PidValue['type_id']]);
                // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=index&type_id='.$PidValue['type_id'];

	            $result['TypeData'][$P_key]['SubType'] = [];

                if (!empty($TidData[$PidValue['type_id']])) {
                    // 所属子栏目处理
                    foreach($TidData[$PidValue['type_id']] as $T_key => $TidValue){
                        if($TidValue['parent_id'] == $PidValue['type_id']){
                            array_push($result['TypeData'][$P_key]['SubType'], $TidValue);
                            $result['TypeData'][$P_key]['SubType'][$T_key]['Url'] = url('plugins/Ask/index', ['type_id'=>$TidValue['type_id']]);
                            // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=index&type_id='.$TidValue['type_id'];
                        }
                    }
                }
	        }
        }
        /* END */
        
    	return $result;
        
    }

    // 周榜
    public function GetAskWeekListData($users_id,$limit,$page,$type_id)
    {
    	$time1 = strtotime(date('Y-m-d H:i:s', time()));
        $time2 = $time1 - (86400 * 7);
        $where = [
            'a.add_time' => ['between time', [$time2, $time1]],
            'a.is_review'=> 1,
            'a.type_id'  => $type_id,
        ];
        // dump($type_id);die;
        $result['WeekList'] = [];
    	$count = $this->weapp_ask_db->field('a.*, b.head_pic')
            ->alias('a')
            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
            ->order('click desc, replies desc')
            ->where($where)
            ->count();
            // dump($WeekList)
        
    	$WeekList = $this->weapp_ask_db->field('a.*, b.head_pic')
            ->alias('a')
            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
            ->order('click desc, replies desc')
            ->where($where)
            ->page($page,$limit)
            ->select();
            
		if (empty($WeekList)) {
            return $result;
        }

        foreach ($WeekList as $key => $value) {
            $zan = M('weapp_zan')->where(array('users_id' => $users_id,'aid' => $value['ask_id'],'zan_count' => 1))->find();
            $cai = M('weapp_zan')->where(array('users_id' => $users_id,'aid' => $value['ask_id'],'zan_count' => 0))->find();
           $WeekList[$key]['UsersConut'] = M('weapp_ask_answer')->where(array('ask_id' => $value['ask_id'],'answer_pid' => 0))->count();
            if(empty($zan))
            {
              $WeekList[$key]['zan'] = 0;
            }else
            {
              $WeekList[$key]['zan'] = 1;
            }
           if(empty($cai))
            {
               $WeekList[$key]['cai'] = 0;
            }else
            {
               $WeekList[$key]['cai'] = 1;
            }
            
              $collection = M('weapp_collection')->where(array('users_id' => $users_id,'ask_id' => $value['ask_id']))->find();
            if(empty($collection))
            {
               $WeekList[$key]['collection'] = 0;
            }else
            {
               $WeekList[$key]['collection'] = 1;
            }
        	$WeekList[$key]['AskUrl'] = url('plugins/Ask/details', ['ask_id'=>$value['ask_id']]);
	        $WeekList[$key]['content'] = htmlspecialchars_decode($value['content']);
        }
        
        // dump($WeekList);die;
         $result['WeekList']['WeekList'] = $WeekList;
         $result['WeekList']['total_page'] = ceil($count/$limit);
         $result['WeekList']['count'] = $count;

// dump($result);die;
        return $result;
    }
    
    //推荐
    
     public function GetAskRecommendData($users_id,$limit,$page,$type_id)
    {
        // dump($page);
        // dump($limit);
        // dump($type_id);
        $where = [
            'a.is_review'=> 1,
            'a.type_id'  => $type_id,
            'a.is_recom' => 1
        ];
        
    	$count = $this->weapp_ask_db->field('a.*, b.head_pic')
            ->alias('a')
            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
            ->order('click desc, replies desc')
            ->where($where)
            ->count();
            
        
    	$WeekList = $this->weapp_ask_db->field('a.*, b.head_pic')
            ->alias('a')
            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
            ->order('click desc, replies desc')
            ->where($where)
            ->page($page,$limit)
            ->select();
            // dump($WeekList);die;

        foreach ($WeekList as $key => $value) {
            $zan = M('weapp_zan')->where(array('users_id' => $users_id,'aid' => $value['ask_id'],'zan_count' => 1))->find();
            $WeekList[$key]['UsersConut'] = M('weapp_ask_answer')->where(array('ask_id' => $value['ask_id'],'answer_pid' => 0))->count();
            $WeekList[$key]['content'] = htmlspecialchars_decode($value['content']);
            if(empty($zan))
            {
              $WeekList[$key]['zan'] = 0;
            }else
            {
              $WeekList[$key]['zan'] = 1;
            }
            
              $collection = M('weapp_collection')->where(array('users_id' => $users_id,'ask_id' => $value['ask_id']))->find();
            if(empty($collection))
            {
               $WeekList[$key]['collection'] = 0;
            }else
            {
               $WeekList[$key]['collection'] = 1;
            }
        	$WeekList[$key]['AskUrl'] = url('plugins/Ask/details', ['ask_id'=>$value['ask_id']]);
        }
         $result['recommend']['recommend'] = $WeekList;
         $result['recommend']['total_page'] = ceil($count/$limit);
         $result['recommend']['count'] = $count;

// dump($result);die;
        return $result;
    }

    // 总榜
    public function GetAskTotalListData()
    {
        $result['TotalList'] = [];
    	$TotalList = $this->weapp_ask_db->field('a.ask_id, a.ask_title, a.click, a.replies, b.head_pic')
            ->alias('a')
            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
            ->order('click desc, replies desc')
            ->where('a.is_review', 1)
            ->page('0, 10')
            ->select();
		if (empty($TotalList)) {
            return $result;
        }

        foreach ($TotalList as $key => $value) {
        	$result['TotalList'][$key]['AskUrl']  = url('plugins/Ask/details', ['ask_id'=>$value['ask_id']]);
            // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=details&ask_id='.$value['ask_id'];
            $result['TotalList'][$key]['ask_title'] = mb_strimwidth($value['ask_title'], 0, 30, "...");
            $result['TotalList'][$key]['content'] = htmlspecialchars_decode($value['content']);
            // if(empty($zan))
        }

        return $result;
    }

    // 问题详情数据
    public function GetAskDetailsData($param = array(),  $parent_id = null, $users_id = null)
    {
    	$ResultData['code'] = 1;
    	$ResultData['info'] = $this->weapp_ask_db->field('a.*, b.username, b.nickname, b.head_pic, c.type_name')
            ->alias('a')
            ->join('__WEAPP_ASK_TYPE__ c', 'c.type_id = a.type_id', 'LEFT')
            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
            ->where('ask_id', $param['ask_id'])
            ->find();
		if (empty($ResultData['info'])) return ['code'=>0,'msg'=>'浏览的问题不存在！'];
		if (0 !== $parent_id) {
			if (0 == $ResultData['info']['is_review'] && $ResultData['info']['users_id'] !== $users_id){
				return ['code'=>0,'msg'=>'问题未审核通过，暂时不可浏览！'];
			}
		}
		
		$ResultData['info']['answer_count'] =  M('weapp_ask_answer')->where(array('ask_id' => $param['ask_id'],'answer_pid' => 0))->count();

        // 头像处理
        $ResultData['info']['head_pic'] = get_head_pic($ResultData['info']['head_pic']);
        
        //用户点赞的数量
        $ResultData['info']['LikeNum'] = M('weapp_zan')->where(array('aid' => $ResultData['info']['ask_id'],'zan_count' => 1))->count();
        
        //用户发帖总数
        $ResultData['info']['usersNum'] = M('weapp_ask')->where(array('users_id' => $ResultData['info']['users_id']))->count();
        
        //用户粉丝数量
         $ResultData['info']['followNum'] = M('follow')->where(array('follow_id' => $ResultData['info']['users_id']))->count();
         
         //用户是否关注了该篇文章
         
        $collection = M('weapp_collection')->where(array('ask_id' => $ResultData['info']['ask_id'],'users_id' => $users_id))->find();
        
        if($collection)
        {
             $ResultData['info']['$collection'] = 1;
        }else
        {
             $ResultData['info']['collection'] = 0;
        }
         
         
         //当前访问用户是否关注
       $follow = M('follow')->where(array('follow_id' => $ResultData['info']['users_id'],'users_id' => $users_id))->find();
       if($follow)
       {
            $ResultData['info']['follow'] = 1;
       }else
       {
            $ResultData['info']['follow'] = 0;
       }
        //用户是否点赞
        $res = M('weapp_zan')->where(array('aid' => $ResultData['info']['ask_id'],'users_id' => $users_id,'zan_count' => 1))->find();
        $cai = M('weapp_zan')->where(array('aid' => $ResultData['info']['ask_id'],'users_id' => $users_id,'zan_count' => -1))->find();
        // dump($ResultData['info']['ask_id']);die;
        // dump($res);die;
        
        if($cai)
        {
             $ResultData['info']['cai'] = 1;
        }else
        {
            $ResultData['info']['cai'] = 0;
        }

        
        if($res)
        {
             $ResultData['info']['zan'] = 1;
        }else
        {
            $ResultData['info']['zan'] = 0;
        }

        // 时间友好显示处理
        $ResultData['info']['add_time'] = $ResultData['info']['add_time'];
        $ResultData['IsUsers'] = 0;
        if ($ResultData['info']['users_id'] == session('users_id')) $ResultData['IsUsers'] = 1;

        // 处理格式
        $ResultData['info']['content'] = htmlspecialchars_decode($ResultData['info']['content']);

        $ResultData['SearchName'] = null;
        // seo信息
        $ResultData['info']['seo_title'] = $ResultData['info']['ask_title'] . ' - ' . $ResultData['info']['type_name'];
        $ResultData['info']['seo_keywords'] = $ResultData['info']['ask_title'];
        $ResultData['info']['seo_description'] = @msubstr(checkStrHtml($ResultData['info']['content']), 0, config('global.arc_seo_description_length'), false);
        return $ResultData;
    }

    // 问题回答数据
    public function GetAskReplyData($param = array(), $parent_id = null,$users_id)
    {
        
    	/*查询条件*/
    	$bestanswer_id = $this->weapp_ask_db->where('ask_id', $param['ask_id'])->getField('bestanswer_id');
		$RepliesWhere = ['ask_id' => $param['ask_id'], 'is_review' => 1];
        $WhereOr = [];
        if (!empty($param['answer_id'])) {
            $RepliesWhere = ['answer_id' => $param['answer_id'], 'is_review' => 1];
            $WhereOr = ['answer_pid' => $param['answer_id']];
        }

        // 若为则创始人则去除仅查询已审核评论这个条件，$parent_id = 0 表示为创始人
        if (0 == $parent_id) unset($RepliesWhere['is_review']);
    	/* END */

        /*评论读取条数*/
        $firstRow = !empty($param['firstRow']) ? $param['firstRow'] : 0;
        $listRows = !empty($param['listRows']) ? $param['listRows'] : 5;
        $result['firstRow'] = $firstRow;
        $result['listRows'] = $listRows;
        /* END */

    	/*排序*/
        $OrderBy = !empty($param['click_like']) ? 'a.click_like '.$param['click_like'].', a.add_time asc' : 'a.add_time asc';
        $click_like = isset($param['click_like']) ? $param['click_like'] : '';
        $result['SortOrder'] = 'desc' == $click_like ? 'asc' : 'desc';
		/* END */

    	/*评论回答*/
    	$RepliesData = $this->weapp_ask_answer_db->field('a.*, b.head_pic, b.nickname, c.nickname as `at_usersname`')
            ->alias('a')
            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
            ->join('__USERS__ c', 'a.at_users_id = c.users_id', 'LEFT')
            ->order($OrderBy)
            ->where($RepliesWhere)
            ->WhereOr($WhereOr)
            ->select();
        if (empty($RepliesData)) return [];
        /* END */

        /*点赞数据*/
        $AnswerIds = get_arr_column($RepliesData, 'answer_id');
        $AnswerLikeData = $this->weapp_ask_answer_like_db->field('a.*, b.nickname')
            ->alias('a')
            ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
            ->order('like_id desc')
            ->where('answer_id','IN',$AnswerIds)
            ->select();
		$AnswerLikeData = group_same_key($AnswerLikeData, 'answer_id');
		/* END */
		
        /*回答处理*/
        $PidData = $AnswerData = [];
        foreach ($RepliesData as $key => $value) {
        	// 友好显示时间
        	$value['add_time'] = friend_date($value['add_time']);
            // 处理格式
            $value['content'] = htmlspecialchars_decode($value['content']);
            // 头像处理
            $value['head_pic'] = get_head_pic($value['head_pic']);
            // 会员昵称
            $value['nickname'] = !empty($value['nickname']) ? $value['nickname'] : $value['username'];

        	// 是否上一级回答
            if($value['answer_pid'] == 0){
                $PidData[]	  = $value;
            }else{
                $AnswerData[] = $value;
            }
        }
        
    //   / dump($PidData);die;
        /* END */

        /*一级回答*/
        foreach($PidData as $key => $PidValue){
            // dump($users_id);die;
            if($users_id == 0)
            {
                $PidValue['zan'] = 0;
            }
            
             $res = M('weapp_ask_answer_like')->where(array('answer_id' =>$PidValue['answer_id'],'users_id' => $users_id))->find();
            if($res)
            {
                $PidValue['zan'] = 1;
            }else
            {
               $PidValue['zan'] = 0;
            }
            
            // dump($PidValue);die;
            $result['AnswerData'][] = $PidValue;
            // 子回答
            $result['AnswerData'][$key]['AnswerSubData'] = [];
            //  $result['AnswerData'][$key]['AnswerSubData']['AnswerSubData'] = [];
            // 点赞数据
            $result['AnswerData'][$key]['AnswerLike'] = [];
            $result['AnswerData'][$key]['ask_count'] = '';
            $result['AnswerData'][$key]['ask_reply'] = 0;
            /*所属子回答处理*/
            // dump($AnswerData);die;
            foreach($AnswerData as $k  => $AnswerValue){
                // dump($AnswerValue);
                if($AnswerValue['answer_pid'] == $PidValue['answer_id']){
                     $LikeNums = M('weapp_ask_answer_like')->where(array('answer_id' => $AnswerValue['answer_id']))->count();
    	             $AnswerValue['AnswerLike']['LikeNums'] = $LikeNums;
    	             $AnswerValue['ask_count'] = '';
    	             $AnswerValue['ask_reply'] = 0;
	                 $res1 = M('weapp_ask_answer_like')->where(array('answer_id' =>$AnswerValue['answer_id'],'users_id' => $users_id))->find();
                     $rest = M('weapp_ask_answer_like')->where(array('answer_id' =>$AnswerValue['answer_id'],'users_id' => $users_id))->count();
                     $AnswerValue['LikeNum'] = $rest;
                    if($res1)
                    {
                        $AnswerValue['zan'] = 1;
                    }else
                    {
                       $AnswerValue['zan'] = 0;
                    }
                    array_push($result['AnswerData'][$key]['AnswerSubData'], $AnswerValue);
                }
            }
            // dump(11);die;
            // dump($result['AnswerData'][$key]['AnswerSubData']);die;
            /* END */

            /*读取指定数据*/
            // 以是否审核排序，审核的优先
            array_multisort(get_arr_column($result['AnswerData'][$key]['AnswerSubData'],'is_review'), SORT_DESC, $result['AnswerData'][$key]['AnswerSubData']);
            // 读取指定条数
            $result['AnswerData'][$key]['AnswerSubData'] = array_slice($result['AnswerData'][$key]['AnswerSubData'], $firstRow, $listRows);
            
            // dump( $result['AnswerData'][$key]['AnswerSubData']);die;
            foreach ( $result['AnswerData'][$key]['AnswerSubData'] as $kk => $vv)
            {
                // dump($vv);
              $list = 
              M('weapp_ask_answer')
              ->field('a.*, b.nickname,b.head_pic')
              ->alias('a')
              ->join('__USERS__ b', 'a.users_id = b.users_id', 'LEFT')
              ->where(array('a.answer_pid' => $vv['answer_id']))
              ->order('answer_id desc')
              ->select();
              foreach ($list as $ke => $vs)
              {
                  
                   $re = M('weapp_ask_answer_like')->where(array('answer_id' =>$vs['answer_id'],'users_id' => $users_id))->find();
                //   dump($re);die;
                     $restr = M('weapp_ask_answer_like')->where(array('answer_id' =>$vs['answer_id'],'users_id' => $users_id))->count();
                     $list[$ke]['LikeNum'] = $restr;
                    if($re)
                    {
                        $list[$ke]['zan'] = 1;
                    }else
                    {
                       $list[$ke]['zan'] = 0;
                    }
              }
              
                $result['AnswerData'][$key]['AnswerSubData'][$kk]['AnswerSubData']['AnswerSubData'] = $list;
              
            //   dump( $result['AnswerData'][$key]['AnswerSubData'][$kk]['AnswerSubData']);die;
            }
            
            
            // foreach( $result['AnswerData'][$key]['AnswerSubData'][$kk]['AnswerSubData'] as $kv => $vk)
            // {
            //     // dump($vk);
            //     $result['AnswerData'][$key]['AnswerSubData'][$kk]['AnswerSubData'][$kv]['LikeNums'] = M('weapp_ask_answer_like')->where(array('answer_id' => $vk['answer_id']))->count();
            //     $result['AnswerData'][$key]['AnswerSubData'][$kk]['AnswerSubData'][$kv]['add_time'] = friend_date($vk['add_time']);
            //          $res2 = M('weapp_ask_answer_like')->where(array('answer_id' => $vk['answer_id'],'users_id' => $users_id))->find();
            //         if($res2)
            //         {
            //             $result['AnswerData'][$key]['AnswerSubData'][$kk]['AnswerSubData'][$kv]['zan'] = 1;
            //         }else
            //         {
            //           $result['AnswerData'][$key]['AnswerSubData'][$kk]['AnswerSubData'][$kv]['zan'] = 0;
            //         }
                    
            //         // dump($result['AnswerData'][$key]['AnswerSubData'][$kk]['AnswerSubData']);
            // }
            // dump($result);die;
            // dump($result['AnswerData'][$key]['AnswerSubData'][$kk]['AnswerSubData']);die;
            
            
            //     array_multisort(get_arr_column($result['AnswerData'][$key]['AnswerSubData'],'is_review'), SORT_DESC, $result['AnswerData'][$key]['AnswerSubData']);
            // // 读取指定条数
            // $result['AnswerData'][$key]['AnswerSubData'] = array_slice($result['AnswerData'][$key]['AnswerSubData'], $firstRow, $listRows);
            
            // dump($result['AnswerData'][$key]['AnswerSubData']);die;
            
            /* END */

            $result['AnswerData'][$key]['AnswerLike']['LikeNum']  = null;
            $result['AnswerData'][$key]['AnswerLike']['LikeName'] = null;
            /*点赞处理*/
            foreach ($AnswerLikeData as $LikeKey => $LikeValue) {
            	if($PidValue['answer_id'] == $LikeKey){
            		// 点赞总数
            		$LikeNum = count($LikeValue);
            		$result['AnswerData'][$key]['AnswerLike']['LikeNum'] = $LikeNum;
            		for ($i=0; $i < $LikeNum; $i++) {
            			// 获取前三个点赞人处理后退出本次for
            			if ($i > 2) break;
            			// 点赞人用户名\昵称
            			$LikeName = $LikeValue[$i]['nickname'];
            			// 在第二个数据前加入顿号，拼装a链接
            			if ($i != 0) {
            				$LikeName = ' 、<a href="javascript:void(0);">'. $LikeName .'</a>';
            			}else{
            				$LikeName = '<a href="javascript:void(0);">'. $LikeName .'</a>';
            			}
            			$result['AnswerData'][$key]['AnswerLike']['LikeName'] .= $LikeName;
            		}
            	}
            }
            /* END */
        }
        /* END */

        /*最佳答案数据*/
        foreach ($result['AnswerData'] as $key => $value) {
        	if ($bestanswer_id == $value['answer_id']) {
        		$result['BestAnswer'][$key] = $value;
        // 		unset($result['AnswerData'][$key]);
        	}
        }
        /* NED */
        
        // 统计回答数
        $result['AnswerCount'] = count($RepliesData);
        
        // dump($result);die;
        return $result;
    }

    // 操作问题表回复数
    public function UpdateAskReplies($ask_id = null, $IsAdd = true, $DelNum = 0)
    {
    	if (empty($ask_id)) return false;
    	if (!empty($IsAdd)) {
    		$Updata = [
    			'replies' => Db::raw('replies+1'),
    			'update_time' => getTime(),
    		];
    	}else{
    		$Updata = [
    			'replies' => Db::raw('replies-1'),
    			'update_time' => getTime(),
    		];
    		if ($DelNum > 0) $Updata['replies'] = Db::raw('replies-'.$DelNum);
    	}
    	$this->weapp_ask_db->where('ask_id', $ask_id)->update($Updata);
    }

    // 增加问题浏览点击量
    public function UpdateAskClick($ask_id = null)
    {
    	if (empty($ask_id)) return false;
    	$Updata = [
			'click' => Db::raw('click+1'),
			'update_time' => getTime(),
		];
    	$this->weapp_ask_db->where('ask_id', $ask_id)->update($Updata);
    }


    // 会员中心--问题中心--我的问题\回复
    public function GetUsersAskDataNew($view_uid = null, $is_ask = true,$type_id)
    {
        // 返回参数
        $result = [];
        if (!empty($is_ask)) {
            // 提问问题查询列表
            /*查询字段*/
            $field = 'a.ask_id, a.ask_title, a.click, a.replies, a.add_time, a.is_review, b.type_name';
            /* END */

            /*查询条件*/
            $where = [
                'a.status'   => ['IN', [0, 1]],
                'a.users_id' => $view_uid,
                'a.type_id' => $type_id,
            ];
            /* END */

            /* 分页 */
            $count   = $this->weapp_ask_db->alias('a')->where($where)->count('ask_id');
            $pageObj = new Page($count, 10);
            $result['pageStr'] = $pageObj->show();
            /* END */

            /*问题表数据(问题表+会员表+问题分类表)*/
            $result['AskData'] = $this->weapp_ask_db->field($field)
                ->alias('a')
                ->join('__WEAPP_ASK_TYPE__ b', 'a.type_id = b.type_id', 'LEFT')
                ->where($where)
                ->order('a.add_time desc')
                ->limit($pageObj->firstRow.','.$pageObj->listRows)
                ->select();
               // dump(11);die;
            /* END */
        }else{
            // dump(11);die;
            // 回答问题查询列表
            /*查询字段*/
            $field = 'a.*, b.ask_title';
            /* END */

            /*查询条件*/
            $where = [
                'a.users_id' => $view_uid,
                'a.type_id' => $type_id
            ];
            /* END */

            /* 分页 */
            $count   = $this->weapp_ask_answer_db->alias('a')->where($where)->count('answer_id');
            $pageObj = new Page($count, 5);
            $result['pageStr'] = $pageObj->show();
            /* END */

            /*问题回答人查询*/
            $result['AskData'] = $this->weapp_ask_answer_db->field($field)
                ->alias('a')
                ->join('__WEAPP_ASK__ b', 'a.ask_id = b.ask_id', 'LEFT')
                ->where($where)
                ->order('a.add_time desc')
                ->limit($pageObj->firstRow.','.$pageObj->listRows)
                ->select();
                // dump($result);die;
            /* END */
        }
        
        /*数据处理*/
        foreach ($result['AskData'] as $key => $value) {
            // 问题内容Url
            $result['AskData'][$key]['AskUrl'] = url('plugins/Ask/details', ['ask_id'=>$value['ask_id']]);
            // ROOT_DIR.'/index.php?m=plugins&c=Ask&a=details&ask_id='.$value['ask_id'];
            if (empty($is_ask)) {
                $result['AskData'][$key]['AskUrl'] .= !empty($value['answer_pid']) ? '#ul_div_li_'.$value['answer_pid'] : '#ul_div_li_'.$value['answer_id'];
            }

            if (isset($value['answer_id']) && !empty($value['answer_id'])) {
                $preg = '/<img.*?src=[\"|\']?(.*?)[\"|\']?\s.*?>/i';
                $value['content'] = htmlspecialchars_decode($value['content']);
                $value['content'] = preg_replace($preg,'[图片]',$value['content']);
                $value['content'] = strip_tags($value['content']);
                $result['AskData'][$key]['content'] = mb_strimwidth($value['content'], 0, 120, "...");
            }
        }
        /* END */

        return $result;
    }
}