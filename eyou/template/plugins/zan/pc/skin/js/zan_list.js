// 加载评论整体页面
function ajaxZan_1572491762(aid, root_dir)
{
    var before_display = document.getElementById('ey_zan'+aid).style.display;
    document.getElementById('ey_zan'+aid).style.display = 'none';
    var aid = parseInt(aid);
    //步骤一:创建异步对象
    var ajax = new XMLHttpRequest();
    //步骤二:设置请求的url参数,参数一是请求的类型,参数二是请求的url,可以带参数,动态的传递参数starName到服务端
    ajax.open("get", root_dir+'/index.php?m=plugins&c=Zan&a=zan_list&aid='+aid+'&_ajax=1', true);
    // 给头部添加ajax信息
    ajax.setRequestHeader("X-Requested-With","XMLHttpRequest");
    //步骤三:发送请求
    ajax.send();
    //步骤四:注册事件 onreadystatechange 状态改变就会调用
    ajax.onreadystatechange = function () {
        //步骤五 如果能够进到这个判断 说明 数据 完美的回来了,并且请求的页面是存在的
        if (ajax.readyState==4 && ajax.status==200) {
            var json = ajax.responseText;  
            var res = JSON.parse(json);
            if (1 == res.code) {
                try{
        　　　　    document.getElementById('ey_zan'+aid).innerHTML = res.data.html;
                    if (!before_display) {
                        document.getElementById('ey_zan'+aid).style.display = before_display;
                    }
                }catch(e){}
            } else {
                if (0 == res.data.isStatus) {
                    document.getElementById('ey_zan'+aid).innerHTML = '';
                    document.getElementById('ey_zan'+aid).style.display = 'none';
                }
            }
      　} else {
            try {
                if (!before_display) {
                    document.getElementById('ey_zan'+aid).style.display = before_display;
                }
                document.getElementById('ey_zan'+aid).innerHTML = '加载失败~';
            }
            catch(err){}
        }
    }
}

function ClickZan(obj, aid) {
    //if ($(obj).attr('data-is_zan')) {
		
       // layer.msg('您已赞过！', {time: 1500});
		
       // return false;
    //}
    $.ajax({
        url: $(obj).data('url'),
        type: 'POST',
        dataType: 'json',
        data: {aid:aid},
        success: function(res) {
            if (1 == res.code) {
                $('#click_like_'+aid).html(res.data.LikeCount);
				layer.msg('点赞成功！', {time: 1500});
				
            } else {
				
                layer.msg(res.msg, {time: 1500});
            }
        //$('#click_zan_'+aid).addClass("hover");
       // $(obj).attr('data-is_zan', true);
		},
    });
}